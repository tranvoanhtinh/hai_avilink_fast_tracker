function formatCurrencyInput(event) {
    let inputElement = event.target;
    let rawValue = inputElement.value.replace(/[,\.]/g, '');
    let amount = parseFloat(rawValue);
    if (!isNaN(amount)) {
        let [integerPart, decimalPart] = amount.toFixed(2).split(".");
        integerPart = integerPart.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        let formattedValue = integerPart + "." + decimalPart;
        inputElement.value = formattedValue;
    } else {
        inputElement.value = '';
    }
}
function formatCurrencyfocusInput(event) {
    var input = event.target;
    var value = input.value;
    if (value.endsWith('.00')) {
        input.value = value.slice(0, -3); // Xóa '.00' khỏi cuối chuỗi
    }
}
function formatCurrencyOnInput(event) {
    let inputElement = event.target;
    let rawValue = inputElement.value.replace(/,/g, '');
    if (!isNaN(rawValue)) {
        let formattedValue = rawValue.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        inputElement.value = formattedValue;
    } else {
        inputElement.value = '';
    }
}

const inputIds = ['exchange_rate', 'price','billingamount', 'unit_price_input'];
document.addEventListener('DOMContentLoaded', function() {
    inputIds.forEach(function(id) {
        let inputElement = document.getElementById(id);
        if (inputElement) {
            inputElement.addEventListener('blur', formatCurrencyInput);
            inputElement.addEventListener('focus', formatCurrencyfocusInput);
            inputElement.addEventListener('input', formatCurrencyOnInput);
        }
    });
});




////=============================
function initializeDropdown(input, columns,columns2,entryPoint, module, need) {

    var inputId = input;
    if(columns2[0] == true){

        var check = ['billing_account', 'billing_contact','contract_account','opportunity'];  // vùng id đã bị custom -> type2
        if (Array.isArray(inputId)) {
            type = 3;
            inputId = input[0];
        }else if (check.includes(inputId)) {
            type = 2;
            inputId = input;
        } else {
            type = 1;
            inputId = input;
        }
        var agent = document.getElementById(inputId);
        if (agent) {
            agent.setAttribute('id', 'custom_' + inputId);
            agent.setAttribute('name', 'custom_' + inputId);
            var hiddenInput = document.createElement('input');
            hiddenInput.type = 'hidden';
            hiddenInput.id = inputId; // Sử dụng lại id ban đầu
            hiddenInput.name = inputId; // Sử dụng lại name ban đầu
            hiddenInput.value = document.getElementById('custom_'+inputId).value;
            agent.parentNode.insertBefore(hiddenInput, agent.nextSibling);
        }
        // Tạo ul element chứa danh sách
        var divElement = document.querySelector(`div.col-xs-12.col-sm-8.edit-view-field[type="relate"][field="${inputId}"]`);
        if (divElement) {
            var ulElement = document.createElement('ul');
            ulElement.id = `data_${inputId}_list`;
            ulElement.classList.add('dropdown-list');
            ulElement.style.display = 'none'; // Mặc định ẩn đi
            divElement.appendChild(ulElement);
        }
        if(need){
            var id = document.getElementById(need).value;
        }
        // Fetch dữ liệu từ server
        function fetchAccountsData() {
            $.ajax({
                type: "POST",
                url: `index.php?entryPoint=${entryPoint}`,
                data: {
                    rows: JSON.stringify(columns),
                    module: module,
                    id: id,
                },
                success: function(data) {
                    var datas = JSON.parse(data);
                    populateResults2(datas, module);
                },
                error: function(error) {
                    console.error('Error fetching data:', error);
                }
            });
        }
        fetchAccountsData();
        function populateResults2(datas, module) {
            const ulElement = document.getElementById(`data_${inputId}_list`);
            if (!ulElement) {
                return;
            }
            ulElement.innerHTML = '';
            datas.forEach((item) => {
                const liElement = document.createElement('li');
                let content = `<span class="name">${item[columns[1]]}</span>`; // Use 'name' from columns array
                if (columns.length > 2 && item[columns[2]]) { // Check for 'tax_code' from columns array if available
                    content += ` - <span class="code">${item[columns[2]]}</span>`;
                }
                if (columns.length > 3 && item[columns[3]]) { // Check for 'account_code' from columns array if available
                    content += ` - <span class="code2">${item[columns[3]]}</span>`;
                }
                liElement.innerHTML = content;
                liElement.setAttribute('data-id', item[columns[0]]); // Use 'id' from columns array
                liElement.setAttribute('data-name', item[columns[1]]); // Use 'name' from columns array
                if (columns.length > 2 && item[columns[2]]) { // Check for 'tax_code' from columns array if available
                    liElement.setAttribute('data-tax-code', item[columns[2]]);
                }
                if (columns.length > 3 && item[columns[3]]) { // Check for 'account_code' from columns array if available
                    liElement.setAttribute('data-account-code', item[columns[3]]);
                }
                liElement.classList.add('dropdown-item');
                liElement.addEventListener('click', () => {
                    var main_id = inputId.slice(0, -5);

                    var inputIdField1 = document.getElementById('custom_' + inputId);
                    var hiddeninputIdField1 = document.getElementById(inputId);
                    var inputIdField2 = document.getElementById(columns2[2]);
                    var inputIdField3 =  document.getElementById(columns2[3]);
                    var inputId1 = document.getElementById(main_id + module + '_ida');
                    if  (inputId1){
                        inputId1.value = item[columns[0]];
                    }
                    var inputId2 = document.getElementById(inputId + '_id');
                    if (inputId2){
                        inputId2.value = item[columns[0]];
                    }
                    var inputId3 = document.getElementById(input[1]);
                    if  (inputId3) {
                        inputId3.value = item[columns[0]];
                    }
                    if (inputIdField1 && hiddeninputIdField1) {
                        inputIdField1.value = item[columns[1]];
                        hiddeninputIdField1.value = item[columns[1]];
                    }
                    if(inputIdField2){
                        inputIdField2.value = item[columns[2]];
                    }

                    if(inputIdField3){
                        inputIdField3.value = item[columns[3]];
                    }
                    ulElement.style.display = 'none'; // Hide the list after selection
                });
                ulElement.appendChild(liElement);
            });
        }
        const inputSearch = document.getElementById('custom_' + inputId);
        if (inputSearch) {

            inputSearch.addEventListener('input', function() {
                if (this.value.trim() !== '') { // Kiểm tra nếu input đã có giá trị
                    filterResults(normalizeString(this.value)); // Filter dữ liệu khi focus vào nếu đã có giá trị
                } else {
                    if (ulElement) {

                        ulElement.style.display = 'block';
                    }
                }
            });
            inputSearch.addEventListener('input', function() {
                const searchValue = normalizeString(this.value);
                filterResults(searchValue);
            });
        }
        // Ẩn danh sách khi nhấn ra ngoài
        document.addEventListener('click', function(event) {
            const isClickInside = inputSearch && inputSearch.contains(event.target) || (ulElement && ulElement.contains(event.target));
            if (!isClickInside && ulElement) {
                ulElement.style.display = 'none';
            }
        });
        function filterResults(searchValue) {
            const ulElement = document.getElementById(`data_${inputId}_list`);
            if (!ulElement) {
                return;
            }
            const lis = Array.from(ulElement.getElementsByTagName('li'));
            let hasVisibleItem = false;
            lis.forEach(li => {
                const name = normalizeString(li.getAttribute('data-name'));
                const taxCode = normalizeString(li.getAttribute('data-tax-code'));
                const accountCode = columns.length > 3 ? normalizeString(li.getAttribute('data-account-code')) : '';

                if (name.includes(searchValue) || (taxCode && taxCode.includes(searchValue)) || (accountCode && accountCode.includes(searchValue))) {
                    li.style.display = 'block';
                    hasVisibleItem = true;
                    highlightText(li, li.getAttribute('data-name'), li.getAttribute('data-tax-code'), searchValue, columns.length > 3 ? li.getAttribute('data-account-code') : ''); // Highlight text
                } else {
                    li.style.display = 'none';
                }
            });
            if (!hasVisibleItem) {
                ulElement.style.display = 'none';
            } else {
                ulElement.style.display = 'block';
            }
        }
        function normalizeString(str) {
            return str ? str.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase().trim() : '';
        }
        // Hàm đánh dấu văn bản phù hợp với màu nền
        function highlightText(element, name, taxCode, searchValue, accountCode = '') {
            const normalizedSearchValue = normalizeString(searchValue);
            const nameSpan = element.querySelector('.name');
            const taxCodeSpan = element.querySelector('.code');
            const accountCodeSpan = element.querySelector('.code2');
            if (nameSpan) {
                if (name && normalizeString(name).includes(normalizedSearchValue)) {
                    nameSpan.innerHTML = highlightSubstring(name, searchValue);
                } else {
                    nameSpan.innerHTML = name; // Bỏ đánh dấu nếu không khớp
                }
            }
            if (taxCodeSpan) {
                if (taxCode && normalizeString(taxCode).includes(normalizedSearchValue)) {
                    taxCodeSpan.innerHTML = highlightSubstring(taxCode, searchValue);
                } else {
                    taxCodeSpan.innerHTML = taxCode; // Bỏ đánh dấu nếu không khớp
                }
            }

            if (accountCodeSpan) {
                if (accountCode && normalizeString(accountCode).includes(normalizedSearchValue)) {
                    accountCodeSpan.innerHTML = highlightSubstring(accountCode, searchValue);
                } else {
                    accountCodeSpan.innerHTML = accountCode; // Bỏ đánh dấu nếu không khớp
                }
            }
        }
        function highlightSubstring(text, searchValue) {
            const normalizedText = normalizeString(text);
            const normalizedSearchValue = normalizeString(searchValue);
            const startIndex = normalizedText.indexOf(normalizedSearchValue);

            if (startIndex !== -1) {
                const beforeText = text.substring(0, startIndex);
                const highlightedText = text.substring(startIndex, startIndex + searchValue.length);
                const afterText = text.substring(startIndex + searchValue.length);
                return beforeText + '<span class="highlight">' + highlightedText + '</span>' + afterText;
            }
            return text;
        }
        const customInput = document.getElementById('custom_'+inputId);
        const originalInput = document.getElementById(inputId);

        if (customInput && originalInput) {
            customInput.addEventListener('input', function() {
                originalInput.value = this.value;
            });
        }
        const clearButton = document.getElementById('btn_clr_'+inputId);
        if (clearButton && customInput) {
            clearButton.addEventListener('click', function() {
                customInput.value = ''; // Xóa giá trị của input custom_accounts_aos_bookings_1_name
            });
        }
        var intervalId1;
        var field_id;
        var field_name = 'custom_' + inputId;
        if(type == 2){
            field_id = inputId+'_id';
        }else if(type == 1){
            field_id = inputId.slice(0, -5) + module + '_ida';
        }else if(type == 3){
            field_id = input[1];
        }
        var btn_choose = 'btn_' + inputId;
        var btn_choose_clear = 'btn_clr_' + inputId;
        var id2 = document.getElementById(field_id).value;
        var namex = [columns[1]];
        var getElement = (id) => document.getElementById(id);
        var field_idvalue = getElement(field_id).value;
        const handleClick = () => {
            intervalId1 = setInterval(check_hidden_id, 500);
        };
        const handleFocus = () => {
            intervalId1 = setInterval(check_hidden_id, 500);
        };
        const handleBlur = () => {
            intervalId1 = setInterval(check_hidden_id, 500);
        };
        const check_hidden_id = () => {
            var id = getElement(field_id);
            if (id.value !== id2) {
                id2 = id.value;
                get_fieldname(namex ,id2); // Sửa lỗi: Đã định nghĩa hàm get_fieldname
                clearInterval(intervalId1);
            }
        };
        getElement(btn_choose).addEventListener("click", handleClick);
        getElement(field_name).addEventListener('focus', handleFocus);
        getElement(field_name).addEventListener('blur', handleBlur);
        function get_fieldname(columns,id) {
        $.ajax({
            type: "POST",
            url: `index.php?entryPoint=${entryPoint}`,
            data: {
                rows: JSON.stringify(columns),
                module: module,
                where_id : id,

        },
            success: function(data) {
                datas = JSON.parse(data);
                document.getElementById('custom_'+inputId).value = datas[0][columns[0]];
            },
            error: function(error) {
                console.error(error);
            }
        });
    }
    }
}
// phần config 
var elementsToRemove = ['EditView_billing_account_results', 'EditView_billing_contact_results'];
elementsToRemove.forEach(function(elementId) {
    var element = document.getElementById(elementId);
    if (element) {
        element.remove();
    }
});
var form = document.getElementById('EditView');
var module_dir = form.querySelector('input[name="module"]').value;
var module_deleted = ['AOS_Quotes', 'AOS_Inventoryinput', 'AOS_Inventoryoutput', 'AOS_Warehouse_Transfer'];
if (module_deleted.includes(module_dir)) {
    var idsToRemove = [
        'billing_address_city_label',
        'billing_address_state_label',
        'billing_address_postalcode_label',
        'billing_address_country_label',
        'shipping_address_city_label',
        'shipping_address_city',
        'shipping_address_state_label',
        'shipping_address_postalcode_label',
        'shipping_address_country_label',
    ];
    idsToRemove.forEach(function(id) {
        var tdElement = document.getElementById(id);
        if (tdElement) {
            var trElement = tdElement.closest('tr');
            if (trElement) {
                trElement.remove();
            }
        }
    });
}
switch (module_dir) {
    // case 'Opportunities':
    //     columns12 = [true, 'name', 'part_number'];
    //     columns11 = [true, 'name'];
    //     columns13 = [true, 'name'];
    // case 'Accounts':
    //     columns9 = [true, 'name', 'part_number'];
    //     columns10 = [true, 'name'];
    //     // columns17 = [true, 'name'];  trường không thể xử lý
    //     break;
    // case 'AOS_Inventoryinput':
    //     columns1 = [true, 'name', 'mobileref'];
    //     columns2 = [true, 'name'];
    //     columns14 = [true, 'name'];
    //     columns15 = [true, 'name'];
    //     break;
    // case 'AOS_Inventoryoutput':
    //     columns1 = [true, 'name', 'mobileref'];
    //     columns2 = [true, 'name'];
    //     columns16 = [true, 'name'];
    //     columns15 = [true, 'name'];
    //     break;
    // case 'AOS_Warehouse_Transfer':
    //     columns2 = [true, 'name', 'mobileref'];
    //     columns14 = [true, 'name'];
    //     columns15 = [true, 'name'];
    //     columns16 = [true, 'name'];
    //     break;
    // case 'AOS_Quotes':
    //     columns1 = [true, 'name', 'phoneaccount'];
    //     columns2 = [true, 'name'];
    //     break;
    // case 'AOS_Invoices':
    //     columns1 = [true, 'name', 'mobileref'];
    //     columns2 = [true, 'name'];
    //     break;
    // case 'AOS_Contracts':
    //     columns3 = [true, 'name', 'mobileref'];
    //     columns17 = [true, 'name'];
    //     columns18 = [true, 'name'];

    //     break;
    // case 'Leads':
    //      columns4 = [true, 'name'];
    //      columns5 = [true,'name'];
    //     break;
    case 'spa_ActionService':
        // columns6 =[true, 'name', 'mobileref'];
        // columns7 = [true,'name'];
        // columns8 = [true,'name'];
        // columns19 = [true,'name'];
        columns24 = [true,'name'];
        columns25 = [true,'name'];
         break; 
    // case 'bill_Billing':
    //     columns20 = [true,'name'];
    //     columns21 = [true,'name'];    
    //     columns22 = [true,'name']; 
    //     columns23 = [true,'name']; 
    default:
        // Các giá trị mặc định hoặc xử lý cho các module_dir không khớp với các case trên
        break;
}
// Tìm và gọi hàm initializeDropdown cho từng id
// var element1 = document.getElementById('billing_account');
// if (element1) {
//     initializeDropdown('billing_account', ['id', 'name', 'phone_alternate','tax_code'], columns1, 'get_info_inputsearch', 'accounts');
// }

// var element2 = document.getElementById('billing_contact');
// if (element2) {
//     initializeDropdown('billing_contact', ['id', 'last_name', 'phone_mobile'], columns2, 'get_info_inputsearch', 'contacts');
// }

// var element3 = document.getElementById('contract_account');
// if (element3) {
//     initializeDropdown('contract_account', ['id', 'name', 'phone_alternate','tax_code'], columns3, 'get_info_inputsearch', 'accounts');
// }

// var element4 = document.getElementById('aos_product_categories_leads_1_name');
// if (element4) {
//     initializeDropdown('aos_product_categories_leads_1_name', ['id', 'name'], columns4, 'get_info_inputsearch', 'aos_product_categories');
// }

// var element5 = document.getElementById('aos_products_leads_1_name');
// if (element5) {
//     initializeDropdown('aos_products_leads_1_name', ['id', 'name','part_number'], columns5, 'get_info_inputsearch', 'aos_products');
// }

// var element6 = document.getElementById('accounts_spa_actionservice_1_name');
// if (element6) {
//     initializeDropdown('accounts_spa_actionservice_1_name',['id', 'name', 'phone_alternate','tax_code'], columns6, 'get_info_inputsearch', 'accounts');
// }
// var element7 = document.getElementById('aos_products_spa_actionservice_1_name');
// if (element7) {
//     initializeDropdown('aos_products_spa_actionservice_1_name',['id', 'name','part_number'], columns7, 'get_info_inputsearch', 'aos_products');
// }


// var element8 = document.getElementById('spa_actionservice_aos_invoices_name');
// if (element8) {
//     initializeDropdown('spa_actionservice_aos_invoices_name',['id', 'name'], columns8, 'get_info_inputsearch', 'aos_invoices');
// }

// var element9 = document.getElementById('aos_product_categories_accounts_1_name');
// if (element9) {
//     initializeDropdown('aos_product_categories_accounts_1_name', ['id', 'name'], columns9, 'get_info_inputsearch', 'aos_product_categories');
// }

// var element10 = document.getElementById('aos_products_accounts_1_name');
// if (element10) {
//     initializeDropdown('aos_products_accounts_1_name', ['id', 'name','part_number'], columns10, 'get_info_inputsearch', 'aos_products');
// }

// var element11 = document.getElementById('aos_product_categories_opportunities_1_name');
// if (element11) {
//     initializeDropdown('aos_product_categories_opportunities_1_name', ['id', 'name'], columns11, 'get_info_inputsearch', 'aos_product_categories');
// }

// var element12 = document.getElementById('aos_products_opportunities_1_name');
// if (element12) {
//     initializeDropdown('aos_products_opportunities_1_name', ['id', 'name','part_number'], columns12, 'get_info_inputsearch', 'aos_products');
// }

// var element13 = document.getElementById('account_name');
// if (element13) {
//     initializeDropdown(['account_name','account_id'], ['id', 'name', 'phone_alternate','tax_code'], columns13, 'get_info_inputsearch', 'accounts');
// }
// var element14 = document.getElementById('inventory_out');
// if (element14) {
//     initializeDropdown(['inventory_out','aos_inventoryoutput_id'], ['id', 'name'], columns14, 'get_info_inputsearch', 'aos_inventoryoutput');
// }

// var element15 = document.getElementById('invoice');
// if (element15) {
//     initializeDropdown(['invoice','aos_invoice_id'], ['id', 'name'], columns15, 'get_info_inputsearch', 'aos_invoices');
// }
// var element16 = document.getElementById('inventory_in');
// if (element16) {
//     initializeDropdown(['inventory_in','aos_inventoryinput_id'], ['id', 'name'], columns16, 'get_info_inputsearch', 'aos_inventoryinput');
// }

// var element17 = document.getElementById('contact');
// if (element17) {
//     initializeDropdown(['contact','contact_id'], ['id', 'last_name'], columns17, 'get_info_inputsearch', 'contacts');
// }

// var element18 = document.getElementById('opportunity');
// if (element18) {
//     initializeDropdown(['opportunity','opportunity_id'], ['id', 'name'], columns18, 'get_info_inputsearch', 'opportunities');
// }

// var element19 = document.getElementById('spa_actionservice_cs_cususer_name');
// if (element19) {
//     initializeDropdown('spa_actionservice_cs_cususer_name', ['id', 'username'], columns19, 'get_info_inputsearch', 'cs_cususer');
// }

// var element20 = document.getElementById('aos_products_bill_billing_1_name');
// if (element20) {
//     initializeDropdown('aos_products_bill_billing_1_name', ['id', 'name','part_number'], columns20, 'get_info_inputsearch', 'aos_products');
// }

// var element21 = document.getElementById('bill_billing_aos_invoices_name');
// if (element21) {
//     initializeDropdown('bill_billing_aos_invoices_name', ['id', 'name'], columns21, 'get_info_inputsearch', 'aos_invoices');
// }

// var element22 = document.getElementById('bill_billing_accounts_name');
// if (element22) {
//     initializeDropdown('bill_billing_accounts_name', ['id', 'name', 'phone_alternate','tax_code'], columns22, 'get_info_inputsearch', 'accounts');
// }

// var element23 = document.getElementById('cs_cususer_bill_billing_1_name');
// if (element23) {
//     initializeDropdown('cs_cususer_bill_billing_1_name', ['id', 'username'], columns23, 'get_info_inputsearch', 'cs_cususer');
// }


var element24 = document.getElementById('aos_bookings_spa_actionservice_1_name');
if (element24) {
    initializeDropdown('aos_bookings_spa_actionservice_1_name', ['id', 'name'], columns24, 'get_info_inputsearch', 'aos_bookings');
}
var element25 = document.getElementById('aos_passengers_spa_actionservice_1_name');
if (element25) {
    initializeDropdown('aos_passengers_spa_actionservice_1_name', ['id', 'name'], columns25, 'get_info_psg_search', 'aos_passengers','aos_bookings_spa_actionservice_1aos_bookings_ida');
}
