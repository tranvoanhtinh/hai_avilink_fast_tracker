var tdElements = [
    document.querySelectorAll('td[field="description"]'),
    document.querySelectorAll('td[field="description_goods_vn"]'),
    document.querySelectorAll('td[field="description_goods_en"]'),
    document.querySelectorAll('td[field="knitting_description"]'),
];

// Tạo một div bên ngoài để chứa modal và thanh cuộn
var outerContainerDiv = document.createElement('div');
outerContainerDiv.style.overflow = 'auto';
outerContainerDiv.style.maxHeight = '80vh'; // Đặt chiều cao tối đa cho thanh cuộn (80% chiều cao của màn hình)

// Thêm div ngoài vào body
document.body.appendChild(outerContainerDiv);

// Lặp qua từng nhóm td và thực hiện các hành động
tdElements.forEach(function (group) {
    group.forEach(function (td) {
        // Đặt kiểu CSS cho td
        td.style.color = 'blue';
        td.style.fontStyle = 'italic';

        // Tạo div mới và thêm nội dung
        var text = td.innerText;
        var div = document.createElement('div');
        div.style.overflow = 'hidden';
        div.style.width = '220px';
        div.style.height = '105px';
        div.style.position = 'relative';
        div.innerText = text;

        // Thêm sự kiện click cho td
        td.addEventListener('click', function () {
            var text = td.innerText.replace(/\. +/g, '<br>');

            // Tạo modal và thêm nội dung
        var modal = document.createElement('div');
        modal.style.position = 'fixed';
        modal.style.left = '0';
        modal.style.top = '10px';
        modal.style.width = '100%';
        modal.style.height = '100%';
        modal.style.backgroundColor = 'rgba(0, 0, 0, 0.5)';
        modal.style.display = 'flex';
        modal.style.alignItems = 'center';
        modal.style.justifyContent = 'center';
        modal.style.zIndex = '1000'; 


            // Tạo div bên ngoài để chứa table và thanh cuộn
            var innerContainerDiv = document.createElement('div');
            innerContainerDiv.style.overflowY = 'scroll';  // Đặt thuộc tính này để luôn hiển thị thanh cuộn dọc
            innerContainerDiv.style.maxHeight = '80vh'; // Đặt chiều cao tối đa cho thanh cuộn trong modal

            // Hiển thị nút tắt (X) ở trên cùng
            var trCloseButton = document.createElement('tr');
            var tdCloseButton = document.createElement('td');
            tdCloseButton.style.textAlign = 'right';
            tdCloseButton.colSpan = '2';

            var closeButton = document.createElement('button');
            closeButton.innerText = 'X';
            closeButton.style.backgroundColor = '#f16c6c';
            closeButton.style.color = 'white';

            // Thêm đường viền đỏ với góc cong 5px cho nút đóng
            closeButton.style.border = '1px solid red'; // Đặt đường viền đỏ
            closeButton.style.borderRadius = '5px'; // Đặt góc cong là 5px

            closeButton.addEventListener('click', function () {
                // Đóng modal khi người dùng click nút Đóng
                modal.remove();
            });

            tdCloseButton.appendChild(closeButton);
            trCloseButton.appendChild(tdCloseButton);

            // Tạo table
            var table = document.createElement('table');
            table.style.backgroundColor = 'white';
            table.style.padding = '20px';
            table.style.borderRadius = '2px';
            table.style.boxShadow = '0 4px 8px rgba(0, 0, 0, 0.1)';
            table.style.maxWidth = '500px';
            table.style.borderRadius = '7px';

            // Hiển thị nội dung của td.innerText trong bảng
            var trText = document.createElement('tr');
            var tdText = document.createElement('td');
            tdText.innerHTML = '<div style="margin: 10px;"><pre><b style="font-size: 17px; font-family: Times New Roman;">' + text + '<b></pre></div>';
            trText.appendChild(tdText);

            table.appendChild(trCloseButton);
            table.appendChild(trText);

            // Thêm table vào div bên trong
            innerContainerDiv.appendChild(table);

            // Thêm div bên trong vào modal
            modal.appendChild(innerContainerDiv);

            // Thêm modal vào div bên ngoài
            outerContainerDiv.appendChild(modal);
        });

        // Thay thế nội dung của td bằng div
        td.innerHTML = '';
        td.appendChild(div);
    });
});


    document.addEventListener('DOMContentLoaded', function() {
      // Lấy phần tử modal theo ID
      var modal = document.getElementById('searchDialog');
      var modalDialog = document.querySelector('.modal-dialog.modal-lg');
      
      // Xóa class 'modal' và 'fade'
      modal.classList.remove('modal', 'fade', 'modal-search');
       modalDialog.classList.remove('modal-dialog','modal-lg');

      // Thêm class 'in' để hiển thị modal
      modal.classList.add('in');

      // Cập nhật thuộc tính style để hiển thị modal
      modal.style.display = 'block';

      var firstModalContent = document.querySelector('.modal-content');

      // Kiểm tra xem có phần tử nào không
      if (firstModalContent) {
        // Xóa thuộc tính box-shadow
        firstModalContent.style.boxShadow = 'none';
         firstModalContent.style.backgroundColor='#f5f5f5';
         firstModalContent.style.border='none';
      }
    });

document.addEventListener('DOMContentLoaded', function() {
  // Lấy phần tử modal-content theo class
  var modalContent = document.querySelector('.modal-header');

  // Kiểm tra xem phần tử có tồn tại không
  if (modalContent) {
    // Thêm đoạn CSS vào phần tử
    modalContent.style.borderBottomWidth = '0px';
    modalContent.style.borderTopWidth = '0px';
    modalContent.style.paddingTop = '10px';
    modalContent.style.paddingBottom = '0px';
    modalContent.style.backgroundColor='#f5f5f5';
  }
});

    document.addEventListener('DOMContentLoaded', function() {
      // Lấy thẻ div cần di chuyển
      var modal = document.getElementById('searchDialog');

      // Lấy thẻ div mục tiêu
      var tabSearchForm = document.getElementById('tab_search_form');

      // Di chuyển thẻ div vào trong tab_search_form
      tabSearchForm.appendChild(modal);
    });

    // document.addEventListener('DOMContentLoaded', function() {
    //   // Lấy thẻ a cần thêm/xóa class "collapsed"
    //   var displayTabS = document.getElementById('display_tab_s');
    //   // Lấy thẻ div cần thêm/xóa class "in"
    //   var detailPanel = document.getElementById('detailpanel_0');

    //   // Thêm sự kiện click cho thẻ a
    //   displayTabS.addEventListener('click', function() {
    //     // Nếu thẻ a có class "collapsed"
    //     if (displayTabS.classList.contains('collapsed')) {
    //       // Xóa class "collapsed" và thêm class "in"
    //       displayTabS.classList.remove('collapsed');
    //       detailPanel.classList.add('in');
    //     } else {
    //       // Ngược lại, thêm class "collapsed" và xóa class "in"
    //       displayTabS.classList.add('collapsed');
    //       detailPanel.classList.remove('in');
    //     }
    //   });
    // });

    document.addEventListener('DOMContentLoaded', function() {
      // Lấy thẻ ul cần xóa
      var ulElement = document.querySelector('.clickMenu.selectmenu.searchLink.SugarActionMenu.listViewLinkButton.listViewLinkButton_top');

      // Kiểm tra xem thẻ ul có tồn tại hay không
      if (ulElement) {
        // Xóa thẻ ul
        ulElement.parentNode.removeChild(ulElement);
      }
    });


window.addEventListener('load', function() {


// Kiểm tra xem phần tử có tồn tại không

  // Lấy thẻ div có id="searchDialog"
  var searchDialog = document.getElementById('searchDialog');

  if (searchDialog) {
  // Lấy tất cả các phần tử có class "search_fields_basic" trong phần tử searchDialog
  var searchFields = searchDialog.querySelectorAll('.search_fields_basic');

  // Duyệt qua từng phần tử và thêm thuộc tính padding
  searchFields.forEach(function(field) {
    field.style.paddingTop = '0px';
    field.style.paddingBottom = '0px';
  });
}
  if (searchDialog) {
  // Lấy phần tử modal-body trong phần tử searchDialog
  var modalBody = searchDialog.querySelector('.modal-body');

  // Kiểm tra xem phần tử có tồn tại không
  if (modalBody) {
    // Thêm thuộc tính padding vào phần tử modal-body
    modalBody.style.paddingTop = '0px';
    modalBody.style.paddingBottom = '0px';
  }
}

  // Kiểm tra xem div có tồn tại không
  if (searchDialog) {
    // Lấy thẻ button.close bên trong div
    var closeButton = searchDialog.querySelector('button.close');

    // Kiểm tra xem nút có tồn tại không
    if (closeButton) {
      // Xóa nút button
      closeButton.parentNode.removeChild(closeButton);
    }

    // Lấy thẻ h4.modal-title bên trong div
    var modalTitle = searchDialog.querySelector('h4.modal-title');

    // Kiểm tra xem thẻ h4 có tồn tại không
    if (modalTitle) {
      // Xóa thẻ h4
      modalTitle.parentNode.removeChild(modalTitle);
    }
  }
});








document.addEventListener("DOMContentLoaded", function() {
    // Chỉ chạy khi trang đã được tải hoàn toàn
    checkHiddenClass();
});

function checkHiddenClass() {
    // Lấy thẻ <ul> có class "searchAppliedAlert"
    const searchAlertUl = document.querySelector('ul.searchAppliedAlert');

    if (searchAlertUl && searchAlertUl.classList.contains('hidden')) {
        // Nếu thẻ <ul> có class "searchAppliedAlert" không chứa class "hidden"

        // Kiểm tra nếu không có thẻ div nào có class "listViewEmpty"
        const listViewEmptyDiv = document.querySelector('div.listViewEmpty');
        if (!listViewEmptyDiv) {
            // Nếu không tìm thấy thẻ div nào có class "listViewEmpty", thực hiện hàm mong muốn
            listViewSearchIcon.toggleSearchDialog('basic');
        }
    }
}

var clearButton = document.getElementById('search_form_clear');

if (clearButton) {
    clearButton.addEventListener('click', function() {
        handleRemoveClick(this.form);
        return false; // Ngăn chặn hành động mặc định của nút
    });
} 

function handleRemoveClick(form) {
    // Lấy giá trị của tham số 'module' từ URL
    var urlParams = new URLSearchParams(window.location.search);
    var module = urlParams.get('module');

    // Kiểm tra xem giá trị 'module' có tồn tại hay không
    if (module) {
        // Gọi hàm với giá trị 'module' và form
        SUGAR.savedViews.shortcutDropdown('_none', module);

        // Bổ sung thêm logic xử lý cho việc xóa tìm kiếm nếu cần
        SUGAR.searchForm.clear_form(form);
    } 
}




// Chọn tất cả các thẻ <a> có class là "glyphicon glyphicon-filter parent-dropdown-handler"
var filterLinks = document.querySelectorAll('a.glyphicon.glyphicon-filter.parent-dropdown-handler');

// Chọn thẻ <ul> có class là "clickMenu selectmenu searchLink SugarActionMenu listViewLinkButton listViewLinkButton_bottom"
var ulElement = document.querySelector('ul.clickMenu.selectmenu.searchLink.SugarActionMenu.listViewLinkButton.listViewLinkButton_bottom');

// Kiểm tra xem thẻ <ul> đã được chọn thành công chưa
if (ulElement) {
    // Nếu đã chọn thành công, xóa thẻ <ul>
    ulElement.remove();
} 

// Kiểm tra xem có phần tử có id là "div-search" hay không
var existingDivSearch = document.getElementById('div-search');

if (!existingDivSearch) {
    // Nếu không tìm thấy, tạo phần tử mới và thêm vào ulList
    var newListItem = document.createElement("li");
    newListItem.setAttribute("id", "div-search");
    var submitButtonsDiv = document.querySelector(".submitButtons");
    var ulList = document.querySelector(".nav.nav-tabs[role='tablist']");
    while (submitButtonsDiv.firstChild) {
        newListItem.appendChild(submitButtonsDiv.firstChild);
    }
    ulList.appendChild(newListItem);
    submitButtonsDiv.parentNode.removeChild(submitButtonsDiv);
}

// Đặt style cho searchInput
var searchInput = document.getElementById('search_form_submit');
searchInput.style.marginTop = '0px';

// Gắn sự kiện click cho searchInput
var searchForm = document.getElementById('search_form');
searchInput.addEventListener('click', function(event) {
    event.preventDefault();
    searchForm.submit();
});

var sm_advanced = document.getElementById('search_form_submit_advanced');
var clear_advanced = document.getElementById('search_form_clear_advanced');

if (sm_advanced) {
    sm_advanced.remove();
}
if (clear_advanced) {
    clear_advanced.remove();
}

var idToCheck = 'search_form_clear';
var elements = document.querySelectorAll('#' + idToCheck);
var count = 0;
elements.forEach(function(element) {
    if (count === 1) {
        element.remove();
    }
    count++;
});

idToCheck = 'search_form_submit';
elements = document.querySelectorAll('#' + idToCheck);
count = 0;
elements.forEach(function(element) {
    if (count === 1) {
        element.remove();
    }
    count++;
});



var elementToRemove = document.querySelector("h5.searchTabHeader.mobileOnly.advanced.active");
if (elementToRemove) {
    elementToRemove.remove();
}

var elementToRemove2 = document.querySelector("h5.searchTabHeader.mobileOnly.basic.active");
if (elementToRemove2) {
    elementToRemove2.remove();
} 

function duplicateMenuItem(menuItemId, i) {
    if (!document.getElementById(menuItemId)) {
        var liElements = document.querySelectorAll('li.actionmenulinks');
        
        // Kiểm tra xem có đủ phần tử không
        if (i < liElements.length) {
            var liElement = liElements[i];
            var clonedLiElement = liElement.cloneNode(true);
            clonedLiElement.id = menuItemId;

            var ulElement = document.querySelector('ul.nav.nav-tabs');
            ulElement.appendChild(clonedLiElement);

            // Lấy sideBarIcon và actionMenuLink trong clonedLiElement
            var sideBarIcon = clonedLiElement.querySelector('.side-bar-action-icon');
            var actionMenuLink = clonedLiElement.querySelector('.actionmenulink');

            if (sideBarIcon) {
                sideBarIcon.style.display = 'inline-block';
            }

            if (actionMenuLink) {
                actionMenuLink.style.display = 'inline-block';
            }
        } 
    }
}

duplicateMenuItem('create_viewedit', 0); // Lấy thẻ thứ hai

