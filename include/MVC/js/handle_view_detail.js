const check = [
    ['AOS_Bookings', 'whole_subpanel_aos_bookings_spa_actionservice_1', 'unit_price_input'],
    ['AOS_Bookings', 'whole_subpanel_aos_bookings_bill_billing_1', 'billingamount'],
];

document.addEventListener('DOMContentLoaded', function() {
    var favoriteDiv = document.querySelector('div.favorite');
    if (favoriteDiv) {
        var module_dir = favoriteDiv.getAttribute('module');

        check.forEach(function(item) {
            var checkModule = item[0];
            var targetId = item[1];
            var inputId = item[2];

            if (module_dir === checkModule) {
                var targetDiv = document.getElementById(targetId);
                if (targetDiv) {
                    var observer = new MutationObserver(function(mutations) {
                        mutations.forEach(function(mutation) {
                            if (mutation.addedNodes.length > 0) {
                                var inputElement = document.getElementById(inputId);
                                if (inputElement) {
                                    inputElement.addEventListener('input', formatCurrencyOnInput);
                                    inputElement.addEventListener('blur', formatCurrencyInput);
                                    inputElement.addEventListener('focus', formatCurrencyfocusInput);
                                }
                            }
                        });
                    });

                    var config = { childList: true, subtree: true };
                    observer.observe(targetDiv, config);
                }
            }
        });
    }
});
function formatCurrencyfocusInput(event) {
    var input = event.target;
    var value = input.value;
    if (value.endsWith('.00')) {
        input.value = value.slice(0, -3); // Xóa '.00' khỏi cuối chuỗi
    }
}
function formatCurrencyInput(event) {
    let inputElement = event.target;
    let rawValue = inputElement.value.replace(/[,\.]/g, '');
    let amount = parseFloat(rawValue);
    if (!isNaN(amount)) {
        let [integerPart, decimalPart] = amount.toFixed(2).split(".");
        integerPart = integerPart.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        let formattedValue = integerPart + "." + decimalPart;
        inputElement.value = formattedValue;
    } else {
        inputElement.value = '';
    }
}

function formatCurrencyOnInput(event) {
    let inputElement = event.target;
    let rawValue = inputElement.value.replace(/,/g, '');
    if (!isNaN(rawValue)) {
        let formattedValue = rawValue.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        inputElement.value = formattedValue;
    } else {
        inputElement.value = '';
    }
}

document.addEventListener('DOMContentLoaded', function() {
    // Lấy nút tạo và gán sự kiện click cho nó
    var createButton = document.getElementById('aos_bookings_spa_actionservice_1_tạo_button');
    
    if (createButton) {
        createButton.addEventListener('click', function() {
            // Hàm tìm và click vào nút input
            function findAndClickButton() {
                var targetButton = document.getElementById('spa_ActionService_subpanel_full_form_button');
                if (targetButton) {
                    // Nút đã được tìm thấy, click vào nó
                    targetButton.click();
                    // Dừng việc kiểm tra
                    clearInterval(intervalId);
                }
            }
            
            // Chạy kiểm tra mỗi 0.5 giây
            var intervalId = setInterval(findAndClickButton, 100);
        });
    }
});
