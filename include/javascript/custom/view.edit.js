//-----HÀM kiểm tra MÃ tồn tại
//---check_voucher_code

// Hàm kiểm tra MÃ tồn tại
function check_voucher_code(fieldcode, module_table, oldValue,firstvalue) {
    var voucher_code = document.getElementById(fieldcode);
   
        var record = voucher_code.value;
        
        // Kiểm tra nếu giá trị mới khác giá trị cũ và không rỗng
        if (record !== oldValue && record.trim() !== ''&& record !== firstvalue) {
            if (/^[a-zA-Z0-9.\-_ ]*$/.test(record)) {
                $.ajax({
                    type: "POST",
                    url: "index.php?entryPoint=check_voucher_code",
                    data: {
                        record: record,
                        module_table: module_table,
                        col: fieldcode,
                    },
                    success: function(data) {
                        var remainingElement = document.getElementById('remaining');
                        if (data == -1) {
                            remainingElement.textContent = SUGAR.language.get(module_sugar_grp1, 'LBL_ALREADY-EXISTS');
                            remainingElement.style.color = 'red'; // Đổi màu chữ thành đỏ nếu mã đã tồn tại
                            disableSaveButton();
                        } else {
                            remainingElement.textContent = '';
                            voucher_code.value = data;
                            voucher_code.style.color = 'blue'; // Đổi màu chữ thành xanh nếu mã hợp lệ
                            voucher_code.style.fontWeight='bold';
                            enableSaveButton();
                        }
                    },
                    error: function(error) {
                        // Xử lý lỗi nếu có
                    },
                });
            } else {
                var remainingElement = document.getElementById('remaining');
                remainingElement.textContent = SUGAR.language.get(module_sugar_grp1, 'LBL_CHARACTER_ERROR');
                remainingElement.style.color = 'red'; // Đổi màu chữ thành đỏ nếu mã đã tồn tại
                disableSaveButton();
            }
        }

}




// Hàm vô hiệu hóa nút "Lưu"
function disableSaveButton() {
    var x = SUGAR.language.get(module_sugar_grp1, 'LBL_SAVE');
    var saveInputs = document.querySelectorAll('input[value="' + x + '"]');
    
    saveInputs.forEach(function(input) {
        input.disabled = true;
        input.style.setProperty('padding-top', '5px');
        input.style.setProperty('padding-bottom', '26px');
        input.style.setProperty('margin-top', '-10px');
    });
}

// Hàm kích hoạt lại nút "Lưu"
function enableSaveButton() {
    var x = SUGAR.language.get(module_sugar_grp1, 'LBL_SAVE');
    var saveInputs = document.querySelectorAll('input[value="' + x + '"]');
    
    saveInputs.forEach(function(input) {
        input.disabled = false;
        input.style.setProperty('padding-top', '0px');
        input.style.setProperty('padding-bottom', '0px');
        input.style.setProperty('margin-top', '0px');
    });
}
