<?php

function get_voucher_code($module, $voucher_date, $type_voucher = null) {
    global $db;
    $user_id =  $_SESSION['authenticated_user_id'];
    $type_sys_partner = $GLOBALS['app_list_strings']['type_sys_partner_list'];
    if(isset($type_sys_partner[$module])){
    $sql_check_user = "select tttt_worldfonepbx.* from tttt_worldfonepbx inner join tttt_worldfonepbx_users_1_c on tttt_worldfonepbx_users_1_c.tttt_worldfonepbx_users_1tttt_worldfonepbx_ida = tttt_worldfonepbx.id and tttt_worldfonepbx_users_1_c.deleted = '0' left join tttt_worldfonepbx_cstm on tttt_worldfonepbx.id = tttt_worldfonepbx_cstm.id_c where tttt_worldfonepbx_users_1_c.tttt_worldfonepbx_users_1users_idb = '".$user_id."' and tttt_worldfonepbx.is_status = '1' AND tttt_worldfonepbx.type_sys_app = '13' And tttt_worldfonepbx.deleted = '0' AND 
        tttt_worldfonepbx.type_sys_partner ='".$module."' ";
    }
    if($type_voucher !== null){
            $sql_check_user .= "AND swiftcode ='".$type_voucher."'";
    }
    $result_check_user = $db->query($sql_check_user);
    $num_rows = $result_check_user->num_rows;
    $date_parts = explode('/', $voucher_date);
    $day = $date_parts[0];
    $month = $date_parts[1];
    $year = $date_parts[2];
    $yyyy_mm_dd = $year . '-' . $month . '-' . $day;
    if ($num_rows > 0) {
        $row_worldfonepbx = $result_check_user->fetch_assoc();
        $set_data[$module] = array(
          'code' => $row_worldfonepbx['psw_callcenter'],
          'type_date' => $row_worldfonepbx['calluuid'],
        );
    }else {
        return;
    }    
    $col = 'name';
    $where_date = 'voucherdate';
    if($module =='accounts'){
        $col = 'account_code';
        $where_date = 'date_entered';
    }
    if($module =='aos_invoices'){
        $where_date = 'invoice_date';
    }
    if($module =='aos_contracts'){
        $where_date = 'start_date';
    }
    if($module =='aos_products'){
        $where_date = 'date_entered';
        $col = 'part_number';
    }
    if($module =='bill_billing'){
        $where_date = 'billingdate';
    }
    if($module =='aos_quotes'){
       $sql  = "SELECT *
        FROM $module
        JOIN aos_quotes_cstm ON $module.id = aos_quotes_cstm.id_c
        WHERE DATE(datequote_c) = '".$yyyy_mm_dd."'AND deleted = 0";
    }else {
    $sql = "SELECT $col FROM $module WHERE DATE(".$where_date.") = '".$yyyy_mm_dd."'AND deleted = 0";
    }
    $result = $db->query($sql);
    $largest_number = null;
    $start_code = $set_data[$module]['code'];
    while ($row = $db->fetchByAssoc($result)) {
        $name = $row[$col];
        $parts = explode('.', $name);
        $number = end($parts);
        if (strpos($name, $start_code) === 0) { // Kiểm tra xem $start_code có là một phần đầu của $name hay không
            if (ctype_digit($number)) {
                // Nếu $largest_number chưa được gán hoặc $number lớn hơn $largest_number
                if ($largest_number === null || $number > $largest_number) {
                    $largest_number = $number;
                }
            }
        }
    }
    $type_date = $set_data[$module]['type_date'];
    $new_number = sprintf('%04d', intval($largest_number) + 1);
    $format_date_code = DateTime::createFromFormat('Y-m-d', $yyyy_mm_dd);  
    if ($format_date_code !== false) {
        $format_date_code2 = $format_date_code->format($type_date);
        $date_code = str_replace(array("-", "/"), "", $format_date_code2);
        if($date_code !=''){
        $date_code .='.';
        }
        $code = $start_code . $date_code . $new_number;
        return $code;
    }
}


