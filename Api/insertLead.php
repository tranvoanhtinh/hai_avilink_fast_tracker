<?php
include('../config.php');
//include('../include/utils.php');
$servername = $sugar_config["dbconfig"]["db_host_name"];
$username   = $sugar_config["dbconfig"]["db_user_name"];
$password   = $sugar_config["dbconfig"]["db_password"];
$db			= $sugar_config["dbconfig"]["db_name"];
//require_once('include/entryPoint.php');
$data= json_decode(file_get_contents("php://input"));


$header=getallheaders();
function ensure_length(&$string, $length)
{
    $strlen = strlen($string);
    if ($strlen < $length) {
        $string = str_pad($string, $length, '0');
    } elseif ($strlen > $length) {
        $string = substr($string, 0, $length);
    }
}
function create_guid_section($characters)
{
    $return = '';
    for ($i = 0; $i < $characters; ++$i) {
        $return .= dechex(mt_rand(0, 15));
    }

    return $return;
}
function create_guid()
{
    $microTime = microtime();
    list($a_dec, $a_sec) = explode(' ', $microTime);

    $dec_hex = dechex($a_dec * 1000000);
    $sec_hex = dechex($a_sec);

    ensure_length($dec_hex, 5);
    ensure_length($sec_hex, 6);

    $guid = '';
    $guid .= $dec_hex;
    $guid .= create_guid_section(3);
    $guid .= '-';
    $guid .= create_guid_section(4);
    $guid .= '-';
    $guid .= create_guid_section(4);
    $guid .= '-';
    $guid .= create_guid_section(4);
    $guid .= '-';
    $guid .= $sec_hex;
    $guid .= create_guid_section(6);

    return $guid;
}
if($_SERVER['REQUEST_METHOD'] == 'POST'){
if($header['Authorization']=="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzM4NCJ9.eyJ1c2VyX2lkIjo1MzQ1MzQ1OSwidXNlcm5hbWUiOiJOYW1kZXB0cmFpIn0.rJxWLZRb-xZgNkyWU74z5Sg76VEDd-NJYaFdmXxVmyE"){

	$data1=array();
		
	foreach($data as $key=>$value){
		$data1[$key]=$value;
	}
	if(($data1['phoneMobile'] != null && $data1['phoneMobile'] != "") && ($data1['Name'] != null && $data1['Name'] != "") && ($data1['Company'] != null && $data1['Company'] != "") && ($data1['Note'] != null && $data1['Note'] != "") && ($data1['refId'] != null && $data1['refId'] != "") && ($data1['servicetype'] != null && $data1['servicetype'] != "") && ($data1['salutation'] != null && $data1['salutation'] != "")){
		date_default_timezone_set('Asia/Ho_Chi_Minh');
		$time=date('Y-m-d h:i:s', strtotime(date('Y-m-d h:i:s').' + 5 hours'));
		$time2=date('Y-m-d');
		$meeting=0;
		$conn=mysqli_connect($servername,$username,$password,$db);
		mysqli_set_charset( $conn, "utf8");
		$qr="select id,assigned_user_id from accounts where phone_alternate='".$data1['phoneMobile']."' and deleted='0'";
		$rs=mysqli_query($conn, $qr);
		$rw=mysqli_fetch_assoc($rs);
		if(empty($rw["id"])){
			$qr="select id,assigned_user_id from leads where phone_mobile='".$data1['phoneMobile']."' OR phone_other='".$data1['phoneMobile']."' OR phone_home='".$data1['phoneMobile']."' and deleted='0'";
			$rs=mysqli_query($conn, $qr);
			$rw=mysqli_fetch_assoc($rs);
			if(empty($rw["id"])){
			$meeting=0;	
			$module="Leads";	
			}else{
			$parentType="Leads";
			$parentId=$rw["id"];
			$meeting=1;	
			$assign=$rw["assigned_user_id"];
			}
		}
		else{
			$parentType="Accounts";
			$parentId=$rw["id"];
			$meeting=1;
			$assign=$rw["assigned_user_id"];
		}
		if($meeting == 0){
			$id=create_guid();
			$col='';
			$col.="'".$id."',";
			if($data1['phoneMobile'] != null && $data1['phoneMobile'] != "")
			{
				
				$col.="'".$data1['phoneMobile']."',";
				
				
			}
			
			
			
			if($data1['Name'] != null && $data1['Name'] != "")
			{
				
				$col.="'".$data1['Name']."',";
				
				
			}
			if($data1['Company'] != null && $data1['Company'] != "")
			{
				
				$col.="'".$data1['Company']."',";
				
				
			}
			if($data1['Note'] != null && $data1['Note'] != "" )
			{
				
				$col.="'".$data1['Note']."',";
				$col.="'".$data1['Note']."',";
				
				
			}
			if($time != null && $time != "" )
			{
				$col.="'".$time."',";
				$col.="'".$time."',";
			}
			
			if($data1['districtCode'] != null && $data1['districtCode'] != "" )
			{
				$district=$data1['districtCode'];
			}else{
				$data1['districtCode']=null;
				$district=$data1['districtCode'];
			}
			if($data1['provinceCode'] != null && $data1['provinceCode'] != "" )
			{
				$province=$data1['provinceCode'];
			}else{
				$data1['provinceCode']=null;
				$province=$data1['provinceCode'];
			}
			$query3="select id from users where user_name='leaddata' and deleted='0'";
			$result3=mysqli_query($conn, $query3);
			$row3=mysqli_fetch_assoc($result3);
			if(!empty($row3)){
				$assigned_user_id=$row3['id'];
			}
			else{
				$assigned_user_id="1";
			}
			if($data1['salutation'] == "Anh."){
				$salu = "Mr.";
			}else{
				$salu = "Ms.";
			}
				$query="insert into  leads (id,phone_mobile,last_name,account_name,description,status_description,date_entered,date_modified,assigned_user_id,created_by,modified_user_id,status,date_new,salutation) values (".$col."'".$assigned_user_id."','".$assigned_user_id."','".$assigned_user_id."','New','".$time2."','".$salu."')";
				$query=trim($query,",");
				$res=mysqli_query($conn, $query);
				$row=mysqli_affected_rows($conn);



				if($data1['month'] > 0){
					$leadType = "cloudcrm";
				}else{
					$leadType = "FromWeb";
				}
				$check_servicetype = implode(',', $data1['servicetype']);

				if($check_servicetype == "CRM cho dịch vụ cơ bản"){
					$servicetype = "1";
					
				}
				elseif($check_servicetype == "Phần mềm quản lý xưởng gara ô tô"){
					$servicetype = "2";
				}
				elseif($check_servicetype == "Phần mềm quản lý DN -ERP"){
					$servicetype = "3";
				}
				elseif($check_servicetype == "CRM cho vận chuyển-Logistics"){
					$servicetype = "4";
					
				}elseif($check_servicetype == "CRM cho bất động sản - Real Estate"){
					$servicetype = "5";
					
				}elseif($check_servicetype == "CRM cho đào tạo - Edu"){
					$servicetype = "6";
					
				}
				elseif($check_servicetype == "CRM Miễn Phí 12 Tháng"){
					$servicetype = "7";
				}
				elseif($check_servicetype == "App Mobile CRM"){
					$servicetype = "8";
				}
				elseif($check_servicetype == "CRM cho Spa"){
					$servicetype = "9";
				}
				else{
					$servicetype = "other";
				}



				$query1="insert into  leads_cstm (id_c,leadtype_c,provincecode_c,districtcode_c,lastdateactive_c,monthnumber_c,servicetype_c) values ('".$id."','".$leadType."','".$province."','".$district."','".$time."','".$data1['month']."','".$servicetype."')";
				$res1=mysqli_query($conn, $query1);
				$row1=mysqli_affected_rows($conn);
				if($data1['Email'] != null && $data1['Email'] != "")
				{
					$EUpper=strtoupper($data1['Email']);
					$eId=create_guid();
					$eRId=create_guid();
					$q="insert into  email_addresses (id,email_address,email_address_caps,date_created,date_modified) values
					('".$eId."','".$data1['Email']."','".$EUpper."','".$time."','".$time."')";
					$r1=mysqli_query($conn, $q);
					$ro1=mysqli_affected_rows($conn);
					$q="insert into  email_addr_bean_rel (id,email_address_id,bean_id,bean_module,date_created,date_modified) values
					('".$eRId."','".$eId."','".$id."','".$module."','".$time."','".$time."')";
					$r2=mysqli_query($conn, $q);
					$ro2=mysqli_affected_rows($conn);
				}
				if((($row != null) && ($row != 0) && ($row != -1)) &&(($row1 != null) && ($row1 != 0) && ($row1 != -1))){
					$array=array(
					"message"=>"LPCRM-001,insert successful",
					"data"=>array(
					"id"=>$id,
					"assigned_user_id"=>$assigned_user_id
					)
					);
					echo json_encode($array);
				}else{
					echo'{"message":"LPCRM-003,error occur"}';
				}
			
		}
		if($meeting==1){
		$id=create_guid();
		$name="[Auto] - Khách hàng đăng ký dịch vụ";
		$qr="insert into meetings (id,name,description,parent_type,parent_id,assigned_user_id,date_start,date_end,date_entered,date_modified) values('".$id."',
		'".$name."','[Auto]-".$data1['Note']."','".$parentType."','".$parentId."','".$assign."','".$time."','".$time."','".$time."','".$time."')";
		$rs=mysqli_query($conn, $qr);
		$rw=mysqli_affected_rows($conn);
		$ml_id=create_guid();
		$qr1="insert into meetings_leads (id,meeting_id,lead_id) values('".$ml_id."','".$id."','".$parentId."')";
		$rs1=mysqli_query($conn, $qr1);
		$rw1=mysqli_affected_rows($conn);
		if((($rw != null) && ($rw != 0) && ($rw != -1)) && (($rw1 != null) && ($rw1 != 0) && ($rw1 != -1))){
			echo'{"message":"LPCRM-002,create meetings successful"}';
		}else{
			echo'{"message":"LPCRM-004,cannot create meetings"}';
		}
		$ml_id=create_guid();
		$qr="insert into meetings_leads (id,meeting_id,lead_id) values('".$ml_id."','".$id."','".$parentId."')";
		
		}
	}
	else
	{
		echo'{"message":"phoneMobile,Name,Company,refId,salutation must have value"}';
	}
	
}else{
	
	echo'{"error":"wrong auth key"}';
	
}
}