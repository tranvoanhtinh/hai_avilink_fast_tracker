<?php
function utf8ize($d) {
    if (is_array($d)) {
        foreach ($d as $k => $v) {
            $d[$k] = utf8ize($v);
        }
    } else if (is_string ($d)) {
        return utf8_encode($d);
    }
    return $d;
}

require_once  '../config.php';
$userextension = $_POST["user"];
$calluuid = $_POST["calluuid"];
$servername = $sugar_config["dbconfig"]["db_host_name"];
$username   = $sugar_config["dbconfig"]["db_user_name"];
$password   = $sugar_config["dbconfig"]["db_password"];

// Create connection
$conn = new mysqli($servername, $username, $password);
$GLOBALS['conn'] =$conn ;
mysqli_select_db($GLOBALS['conn'],$sugar_config["dbconfig"]["db_name"]);
// $sql = "SELECT * FROM  tttt_history_calls WHERE `userextension`=".$userextension." AND (`workstatus` = 'On-Call' OR  `workstatus` = 'Ring' ) " ;

$sql = "SELECT * FROM  tttt_history_calls WHERE `calluuid`= '$calluuid' " ;

$result = mysqli_query($conn,$sql);
if(empty($result)) exit;
$row = mysqli_fetch_array($result,MYSQLI_ASSOC);
echo json_encode(utf8ize($row));
exit();