<?php
include('../config.php');
//include('../include/utils.php');
$servername = $sugar_config["dbconfig"]["db_host_name"];
$username   = $sugar_config["dbconfig"]["db_user_name"];
$password   = $sugar_config["dbconfig"]["db_password"];
$db			= $sugar_config["dbconfig"]["db_name"];
//require_once('include/entryPoint.php');
$data= json_decode(file_get_contents("php://input"));
$header=getallheaders();
function ensure_length(&$string, $length)
{
    $strlen = strlen($string);
    if ($strlen < $length) {
        $string = str_pad($string, $length, '0');
    } elseif ($strlen > $length) {
        $string = substr($string, 0, $length);
    }
}
function create_guid_section($characters)
{
    $return = '';
    for ($i = 0; $i < $characters; ++$i) {
        $return .= dechex(mt_rand(0, 15));
    }

    return $return;
}
function create_guid()
{
    $microTime = microtime();
    list($a_dec, $a_sec) = explode(' ', $microTime);

    $dec_hex = dechex($a_dec * 1000000);
    $sec_hex = dechex($a_sec);

    ensure_length($dec_hex, 5);
    ensure_length($sec_hex, 6);

    $guid = '';
    $guid .= $dec_hex;
    $guid .= create_guid_section(3);
    $guid .= '-';
    $guid .= create_guid_section(4);
    $guid .= '-';
    $guid .= create_guid_section(4);
    $guid .= '-';
    $guid .= create_guid_section(4);
    $guid .= '-';
    $guid .= $sec_hex;
    $guid .= create_guid_section(6);

    return $guid;
}
if($_SERVER['REQUEST_METHOD'] == 'POST'){
	if($header['Authorization']=="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzM4NCJ9.eyJ1c2VyX2lkIjoxMjM0NTY3ODksInVzZXJuYW1lIjoidmFuYW5oIn0.P849MupfQ4jzhfuk8vBQz8-c2cm4DB9luCu1PAKqNMo"){

		$data1=array();
			
		foreach($data as $key=>$value){
			$data1[$key]=$value;
		}
		if(($data1['subject'] != null && $data1['subject'] != "") && ($data1['description'] != null && $data1['description'] != "") && ($data1['refId'] != null && $data1['refId'] != "") && ($data1['refType'] != null && $data1['refType'] != "") && ($data1['refCompany'] != null && $data1['refCompany'] != "")){
			date_default_timezone_set('Asia/Ho_Chi_Minh');
			$time=date('Y-m-d h:i:s');
			$conn=mysqli_connect($servername,$username,$password,$db);
			mysqli_set_charset($conn, "utf8");
			$id=create_guid();
			$query="select id from users where user_name='casedata' and deleted='0'";
			$result=mysqli_query($conn, $query);
			$row=mysqli_fetch_assoc($result);
			if(empty($row)){
				$user_id='';
			}else{
				$user_id=$row["id"];
			}
				
			if($data1['phoneMobile'] != null && $data1['phoneMobile'] != ""){
				$qry="select id from accounts where phone_alternate='".$data1['phoneMobile']."' and deleted='0'";
				$rsult=mysqli_query($conn, $qry);
				$rww=mysqli_fetch_assoc($rsult);
				if(empty($rww["id"])){
					$qr="insert into cases (id,name,description,date_entered,date_modified,assigned_user_id,status) values ('".$id."','".$data1['subject']."','".$data1['description']."','".$time."','".$time."','".$user_id."','Open_New')";
					$rs=mysqli_query($conn, $qr);
					$rw=mysqli_affected_rows($conn);
					$qr2="insert into cases_cstm (id_c,refid_c,reftype_c,mobile_c,companyref_c) values ('".$id."','".$data1['refId']."','".$data1['refType']."','".$data1['phoneMobile']."','".$data1['refCompany']."')";
					$rs2=mysqli_query($conn, $qr2);
					$rw2=mysqli_affected_rows($conn);
					if((($rw != null) && ($rw != 0) && ($rw != -1)) && (($rw2 != null) && ($rw2 != 0) && ($rw2 != -1))){
						echo'{"message":"LPCRM-002,create cases successful"}';
					}else{
						echo'{"message":"LPCRM-004,cannot create cases"}';
					}
				}
				else{
					$qr="insert into cases (id,name,description,account_id,date_entered,date_modified,assigned_user_id,status) values ('".$id."','".$data1['subject']."','".$data1['description']."','".$rww["id"]."','".$time."','".$time."','".$user_id."','Open_New')";
					$rs=mysqli_query($conn, $qr);
					$rw=mysqli_affected_rows($conn);
					$qr2="insert into cases_cstm (id_c,refid_c,reftype_c,mobile_c,companyref_c) values ('".$id."','".$data1['refId']."','".$data1['refType']."','".$data1['phoneMobile']."','".$data1['refCompany']."')";
					$rs2=mysqli_query($conn, $qr2);
					$rw2=mysqli_affected_rows($conn);
					if((($rw != null) && ($rw != 0) && ($rw != -1)) && (($rw2 != null) && ($rw2 != 0) && ($rw2 != -1))){
						echo'{"message":"LPCRM-002,create cases successful"}';
					}else{
						echo'{"message":"LPCRM-004,cannot create cases"}';
					}
				}
			}else{
					$qr="insert into cases (id,name,description,date_entered,date_modified,assigned_user_id,status) values ('".$id."','".$data1['subject']."','".$data1['description']."','".$time."','".$time."','".$user_id."','Open_New')";
					$rs=mysqli_query($conn, $qr);
					$rw=mysqli_affected_rows($conn);
					$qr2="insert into cases_cstm (id_c,refid_c,reftype_c,companyref_c) values ('".$id."','".$data1['refId']."','".$data1['refType']."','".$data1['refCompany']."')";
					$rs2=mysqli_query($conn, $qr2);
					$rw2=mysqli_affected_rows($conn);
					if((($rw != null) && ($rw != 0) && ($rw != -1)) && (($rw2 != null) && ($rw2 != 0) && ($rw2 != -1))){
						echo'{"message":"LPCRM-002,create cases successful"}';
					}else{
						echo'{"message":"LPCRM-004,cannot create cases"}';
					}
			}
		}	
	}else{
		echo'{"error":"wrong auth key"}';
	}
}