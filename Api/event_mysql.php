<?php

//cấu trúc data mysql
/* CREATE TABLE `tttt_history_calls` (
  `worldfonepbxmanagerid` int(20) NOT NULL AUTO_INCREMENT,   -- primary key of table
  `direction` varchar(10) DEFAULT NULL,  -- direction of call (inboud or outbound)
  `callstatus` varchar(20) DEFAULT NULL, -- status of call (Start, Dialing, DialAnswer, HangUp)
  `workstatus` varchar(20) DEFAULT NULL,  -- status of work, define by external crm partner (New, Ring, On-Call, Completed, Missed...)
  `starttime` datetime DEFAULT NULL,  -- start time
  `answertime` datetime DEFAULT NULL,  -- answer time
  `endtime` datetime DEFAULT NULL,  -- end time
  `totalduration` int(11) DEFAULT NULL,  -- from start to end
  `billduration` int(11) DEFAULT NULL,  -- from answer to end
  `calluuid` varchar(100) DEFAULT NULL,  -- uuid of call
  `calluuid2` varchar(100) DEFAULT NULL,  -- uuid of call

  `user` varchar(100) DEFAULT NULL,      --  user id of current user
  `userextension` varchar(100) DEFAULT NULL,  -- extension of current user
  `customername` varchar(100) DEFAULT NULL,  -- Customer Name
  `customernumber` varchar(100) DEFAULT NULL,  -- Customer Number in current call
  `customertype` varchar(100) DEFAULT NULL,   -- Customer Type (Contact, Lead ...)
  `customercode` varchar(100) DEFAULT NULL, -- Customer Code  // version 3.0
  `calltype` varchar(100) DEFAULT NULL, -- Calltype (Inbound ACD/non-ACD, Outbound ACD/non-ACD)
  `disposition` varchar(255) DEFAULT NULL, -- NO_ANSWER, FAILED, BUSY, ANSWERED, UNKNOWN
  `activitydisposition` varchar(255) DEFAULT NULL, -- Disposition of activity (Promo1, Campaign1...)
  `callnote`   text  DEFAULT NULL,  -- Call note
  `causetxt` varchar(255) DEFAULT NULL, -- Cause of Hand-up
  PRIMARY KEY (`worldfonepbxmanagerid`)
  ) */

require   '../config.php';
$servername = $sugar_config["dbconfig"]["db_host_name"];
$username   = $sugar_config["dbconfig"]["db_user_name"];
$password   = $sugar_config["dbconfig"]["db_password"];

// Create connection
$conn = new mysqli($servername, $username, $password);
$GLOBALS['conn'] =$conn ;
mysqli_select_db($GLOBALS['conn'],$sugar_config["dbconfig"]["db_name"]);
$callstatus = $_GET['callstatus'];
$secret = $_GET['secret'];
$request = $_GET;
if($request['direction'] == 'outbound'){
	if($request['destinationnumber'][0] != '0'){
		$request['destinationnumber'] = '0' . $request['destinationnumber'];
	}	
}else{
	if($request['callernumber'][0] != '0'){
		$request['callernumber'] = '0' . $request['callernumber'];
	}
}




$myfile = fopen("install.log", "a+") ;
fwrite($myfile, $_GET['secret']."/");
fwrite($myfile, $_GET['calluuid']."/");
//fwrite($myfile, $_GET['starttime']."/");
fwrite($myfile, $_GET['callstatus']."/"."\r\n");
fclose($myfile);

require_once  '../config_override.php';
$secret_authen = $sugar_config['asterisk_port'] ; 
$appurl = $sugar_config['asterisk_host'] ;
// Config server PBX
// secret key Southtelecom gửi sang, xác nhận đúng sự kiện gửi từ PBX
// $secret_authen = "f9f3e4fc7f7f5ced27c9d8f3e6d4c4edss";
// $appurl = "https://apps.worldfone.vn/externalcrm/";





if ($secret_authen == $secret) {
    switch ($callstatus) {
	case "Start":
	    start_process($request);
	    break;
	case "Dialing":
	    dialing_process($request);
	    break;
	case "DialAnswer":
	    dialanswer_process($request);
	    break;
	case "HangUp":
	    hangup_process($request);
	    break;
	case "CDR":
	    cdr_process($request);
	    break;
	case "Trim":
	    cdr_process($request);
	  //  trim_process($request);
	    break;
	case "SyncCurCalls":
	    sync_process($request, $appurl, $secret);
	    break;
    }
    echo "200";
} else {
    echo "401";
}

function start_process($request) {

	
    $direction = '';
    $callstatus = '';
    $workstatus = "New";
    $calluuid = '';
    $userextension = '';
    if ($direction == "outbound") {
	 $userextension = '';
	 $customernumber = '';
	    } else {
	 $userextension = '';
	 $customernumber = '';
    }
    $date  = "";
    $starttime = date("Y-m-d H:i:s");
    $strid = strtotime('now').$calluuid ;
    $id = md5($strid);
    $query = "INSERT INTO `tttt_history_calls` (`id`,`direction`,`callstatus`, `workstatus`,`calluuid`,`userextension`,`customernumber`,`starttime`) 
 	VALUES ('$id','$direction', '$callstatus','$workstatus','$calluuid','$userextension','$customernumber','$starttime')";
 	mysqli_query($GLOBALS['conn'],$query);
    relationship_query($id,$customernumber);
}

function relationship_query($id,$phone) {
	$query_sync = "SELECT * FROM `tttt_history_calls` WHERE `customernumber` = '$phone' AND `customertype` IS NULL "; 
	$sync_rawdata = mysqli_query($GLOBALS['conn'],$query_sync);
	while($sync_array = mysqli_fetch_assoc($sync_rawdata)){
		$id = $sync_array['id'];
		$sqlcontacts = "SELECT * FROM `contacts` WHERE (`phone_mobile` = '$phone' OR `phone_home` = '$phone' OR `phone_work` = '$phone') AND `deleted` =0 ";

		$resultcontacts = mysqli_query($GLOBALS['conn'],$sqlcontacts);
		$rowcontacts = mysqli_fetch_array($resultcontacts,MYSQLI_ASSOC);
		if(!empty($rowcontacts)){

			$idcontact = $rowcontacts["id"];
			date_default_timezone_set('Asia/ho_chi_minh');      
			$date=date("Y/m/d h:i:sa");
			$idre = md5(microtime(true));
			$insertsql = "INSERT INTO `tttt_history_calls_contacts_c` (`id`, `date_modified`, `deleted`, `tttt_history_calls_contactstttt_history_calls_ida`, `tttt_history_calls_contactscontacts_idb`) 
			VALUES ('$idre', '$date', '0', '$id', '$idcontact')";
			mysqli_query($GLOBALS['conn'],$insertsql);

			$sql = "UPDATE `tttt_history_calls` SET `customertype` = 'sync' WHERE `id`= '$id'"; 

			$result = mysqli_query($GLOBALS['conn'],$sql);

		}else{ 
			$sqlleads = "SELECT * FROM `leads` WHERE (`phone_mobile` = '$phone' OR `phone_home` = '$phone' OR `phone_work` = '$phone') AND `deleted` =0 ";
			$resultleads = mysqli_query($GLOBALS['conn'],$sqlleads);
			$rowleads = mysqli_fetch_array($resultleads,MYSQLI_ASSOC);
			if(!empty($rowleads)){
				$idcontact = $rowleads["id"];
				date_default_timezone_set('Asia/ho_chi_minh');      
				$date=date("Y/m/d h:i:sa");
				$idre = md5(microtime(true));
				$insertsql = "INSERT INTO `tttt_history_calls_leads_c` (`id`, `date_modified`, `deleted`, `tttt_history_calls_leadstttt_history_calls_ida`, `tttt_history_calls_leadsleads_idb`) 
				VALUES ('$idre', '$date', '0', '$id', '$idcontact')";
				mysqli_query($GLOBALS['conn'],$insertsql); 
				$sql = "UPDATE `tttt_history_calls` SET `customertype` = 'sync' WHERE `id`= '$id'"; 

				$result = mysqli_query($GLOBALS['conn'],$sql);
			}
		}
	}
	
}


function dialing_process($request) {
    $direction = $request['direction'];
    $callstatus = $request['callstatus'];
    $workstatus = "Ring";
    $calluuid = $request['calluuid'];
    $userextension = "";
    if ($direction == "outbound") {
	$userextension = $request['callernumber'];
	$customernumber = $request['destinationnumber'];
    } else {
	$userextension = $request['destinationnumber'];
	$customernumber = $request['callernumber'];
    }
    $calluuid2 = $request['parentcalluuid'];
    $calltype = $request['calltype'];
    if (checkCalluuid($calluuid)) {
	$date = date_create($request['starttime']);
	$starttime = date_format($date, 'Y-m-d H:i:s');
	$query = "UPDATE `tttt_history_calls` SET  `direction`='$direction',`callstatus`='$callstatus', `workstatus`='$workstatus',`userextension`='$userextension',`customernumber`='$customernumber',`calltype` ='$calltype',`calluuid2`='$calluuid2'
			WHERE calluuid='$calluuid' ";
			mysqli_query($GLOBALS['conn'],$query);
    } else {
	$date = date_create($request['starttime']);
	$starttime = date_format($date, 'Y-m-d H:i:s');
	$strid = strtotime('now').$calluuid ;
    $id = md5($strid);
	$query = "INSERT INTO `tttt_history_calls` (`id`,`direction`,`callstatus`, `workstatus`,`calluuid`,`userextension`,`customernumber`,`starttime`,`calltype`,`calluuid2`) 
		VALUES ('$id','$direction', '$callstatus','$workstatus','$calluuid','$userextension','$customernumber','$starttime','$calltype','$calluuid2')";
		mysqli_query($GLOBALS['conn'],$query);
		 relationship_query($id,$customernumber);
    }

   
}

function dialanswer_process($request) {
    $callstatus = $request['callstatus'];
    $workstatus = "On-Call";
    $calluuid = $request['calluuid'];
    $calluuid2 = $request['childcalluuid'];
    if (checkCallOut($calluuid)) {
	$customernumber = $request['destinationnumber'];
	$query = "UPDATE `tttt_history_calls` SET  `callstatus`='$callstatus',answertime=NOW(), `workstatus`='$workstatus',`customernumber`='$customernumber',`calluuid2`='$calluuid2'
			WHERE calluuid='$calluuid' ";
    } else {
	$userextension = $request['destinationnumber'];
	$customernumber = $request['callernumber'];
	$query = "UPDATE `tttt_history_calls` SET  `callstatus`='$callstatus', answertime=NOW(),`workstatus`='$workstatus',`userextension`='$userextension',`customernumber`='$customernumber',`calluuid2`='$calluuid2'
			WHERE calluuid IN ('$calluuid','$calluuid2') ";
    }
    mysqli_query($GLOBALS['conn'],$query);
}

function hangup_process($request) {
    $callstatus = $request['callstatus'];
    $workstatus = "Complete";
    $calluuid = $request['calluuid'];
    $causetxt = $request['causetxt'];
    $query = "UPDATE `tttt_history_calls` SET  `callstatus`='$callstatus', `workstatus`='$workstatus',`causetxt`='$causetxt'
			WHERE calluuid='$calluuid' ";
    mysqli_query($GLOBALS['conn'],$query);
}

function cdr_process($request) {
    $date = date_create($request['starttime']);
    $starttime = date_format($date, 'Y-m-d H:i:s');
    $date = date_create($request['answertime']);
    $answertime = date_format($date, 'Y-m-d H:i:s');
    $date = date_create($request['endtime']);
    $endtime = date_format($date, 'Y-m-d H:i:s');
    $workstatus = "Complete";
    $callstatus = "HangUp";
    $calluuid = $request['calluuid'];
    $billduration = $request['billduration'];
    $totalduration = $request['totalduration'];
    $query = "UPDATE `tttt_history_calls` SET 	callstatus='$callstatus',`workstatus`='$workstatus', `starttime`='$starttime', `answertime`='$answertime',`endtime`='$endtime',`billduration`='$billduration',`totalduration`='$totalduration'
			WHERE calluuid='$calluuid' ";
    mysqli_query($GLOBALS['conn'],$query);
    getSyncCdr($calluuid, $appurl, $secret);
    
}

function trim_process($request) {
    $calluuid = $request['calluuid'];
    $query = "DELETE FROM tttt_history_calls WHERE calluuid='$calluuid' ";
    mysqli_query($GLOBALS['conn'],$query);
}

function sync_process($request, $appurl, $secret) {
    if ($request['calluuids'] == "") {
	$calluuids = null;
    } else {
	$calluuids = explode(",", $request['calluuids']);
    }
    $curcdrs = getCurrentCall();
    if ($curcdrs != null) {
	foreach ($curcdrs as $cdr) {
	    if ($calluuids != null) {
		if (!in_array($cdr['calluuid'], $calluuids)) {
		    getSyncCdr($cdr['calluuid'], $appurl, $secret);
		}
	    } else {
		getSyncCdr($cdr['calluuid'], $appurl, $secret);
	    }
	}
    }
    echo 200;
}

function checkCalluuid($calluuid) {
    $query = "SELECT calluuid FROM  tttt_history_calls WHERE calluuid = '$calluuid'";
    $results = mysqli_query($GLOBALS['conn'],$query);
    $k = 0;
    while ($row = mysqli_fetch_array($results, MYSQLI_ASSOC)) {
	$k = 1;
    }
    if ($k == 1) {
	return true;
    }
    return false;
}

function checkCallOut($calluuid) {
    $query = "SELECT calluuid FROM  tttt_history_calls WHERE calluuid = '$calluuid' AND direction='outbound'";
    $results = mysqli_query($GLOBALS['conn'],$query);
    $k = 0;
    while ($row = mysqli_fetch_array($results, MYSQLI_ASSOC)) {
	$k = 1;
    }
    if ($k == 1) {
	return true;
    }
    return false;
}

function getCurrentCall() {
    $query = "SELECT calluuid FROM tttt_history_calls WHERE workstatus NOT IN ('Complete') LIMIT 10";
    $results = mysqli_query($GLOBALS['conn'],$query);
    $data = null;
    while ($row = mysqli_fetch_array($results, MYSQLI_ASSOC)) {
	$data[] = $row;
    }

    return $data;
}

function getSyncCdr($calluuid, $appurl, $secret) {
    $curl = curl_init();
    curl_setopt_array($curl, array(
	CURLOPT_URL => $appurl . "getcdr2.php?calluuid=" . $calluuid . "&secrect=$secret&version=4",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_POSTFIELDS => json_encode(array("secret" => $secret)),
	CURLOPT_HTTPHEADER => array(
	    "accept: application/json",
	    "cache-control: no-cache",
	    "content-type: application/json"
	),
    ));
    $response = curl_exec($curl);
    $responseObj = json_decode($response, true);
    if ($responseObj['status'] == 200) {
	if (isset($responseObj['data'])) {
	    $dataUpdate = (array) $responseObj['data'];
	    $data['callstatus'] = "HangUp";
	    $data['workstatus'] = "Complete";

	    $data['starttime'] = date('Y-m-d H:i:s', strtotime($dataUpdate['starttime']));
	    $data['answertime'] = date('Y-m-d H:i:s', strtotime($dataUpdate['answertime']));
	    $data['endtime'] = date('Y-m-d H:i:s', strtotime($dataUpdate['endtime']));
	    $data['calluuid'] = $dataUpdate['calluuid'];
	    $data['billduration'] = (int) $dataUpdate['billduration'];
	    $data['totalduration'] = (int) $dataUpdate['totalduration'];
	    $data['disposition'] = $dataUpdate['disposition'];
	    if ($dataUpdate['direction'] == 'outbound') {
		$data['userextension'] = $dataUpdate['callernumber'];
		if (preg_match("#^0(.*)$#i", $dataUpdate['destinationnumber']) != 0 || preg_match("#^(84|19|18)(.*)$#i", $dataUpdate['destinationnumber']) != 0 || strlen($dataUpdate['destinationnumber']) > 10) {
		    $data['customernumber'] = $dataUpdate['destinationnumber'];
		} else {
		    $data['customernumber'] = "0" . $dataUpdate['destinationnumber'];
		}
	    } else {
		$data['userextension'] = $dataUpdate['destinationnumber'];
		if ((preg_match("#^0(.*)$#i", $dataUpdate['callernumber']) != 0) || (preg_match("#^(84|19|18)(.*)$#i", $dataUpdate['callernumber']) != 0) || strlen($dataUpdate['callernumber']) > 10) {
		    $data['customernumber'] = $dataUpdate['callernumber'];
		} else {
		    $data['customernumber'] = "0" . $dataUpdate['callernumber'];
		}
		if ($data['disposition'] != "ANSWERED") {
		    $data['disposition'] = "NO ANSWER";
		}
	    }
	    $query = "UPDATE tttt_history_calls SET callstatus='HangUp', workstatus='Complete', starttime='" . $data['starttime'] . "',
						answertime='" . $data['answertime'] . "', endtime='" . $data['endtime'] . "', billduration='" . $data['billduration'] . "',
						totalduration='" . $data['totalduration'] . "', disposition='".$data['disposition']."', userextension='" . $data['userextension'] . "', 
						customernumber='" . $data['customernumber'] . "' 
						WHERE calluuid='$calluuid'";
	    mysqli_query($GLOBALS['conn'],$query);
	}
    } else {
	if (isset($responseObj['status'])) {
	    if ($responseObj['status'] == 401) {
		$query = "UPDATE tttt_history_calls SET callstatus='HangUp', workstatus='Complete' 
						WHERE calluuid='$calluuid'";
		mysqli_query($GLOBALS['conn'],$query);
	    }
	}
    }
    $err = curl_error($curl);
    curl_close($curl);
}
