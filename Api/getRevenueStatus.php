<?php
        try {
			if (isset($_SERVER['HTTPS']) &&
				($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
				isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
				$_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https'){
					$protocol = 'https://';
				}
			else {
				$protocol = 'http://';
			}
		    $apiUrl = $protocol.$_SERVER['HTTP_HOST']."/index.php?entryPoint=getRevenueStatus";
			$data = array(
				"period"=>$_POST["period"],
				"user"=>$_POST["user"]
			);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_URL, $apiUrl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			/*
            $headers = [
              'Content-type: application/json',
			  'Name: X-VOIP24H-AUTH',
			  'Value:Bearer '.$token,
			  'authorization:Bearer '.$token,
            ];
			*/
			
           // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
            $response = curl_exec($ch);
            //if (!empty($response)) {
            //    $response = json_decode($response, true);
				
           // }
			echo  $response;
            curl_close($ch);
        } catch (Exception $ex) {
            $GLOBALS['log']->error($ex);
        }
        