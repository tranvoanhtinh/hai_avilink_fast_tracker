<?php
include('../config.php');
//include('../include/utils.php');
$servername = $sugar_config["dbconfig"]["db_host_name"];
$username   = $sugar_config["dbconfig"]["db_user_name"];
$password   = $sugar_config["dbconfig"]["db_password"];
$db			= $sugar_config["dbconfig"]["db_name"];
//require_once('include/entryPoint.php');
$data= json_decode(file_get_contents("php://input"));
$header=getallheaders();
function ensure_length(&$string, $length)
{
    $strlen = strlen($string);
    if ($strlen < $length) {
        $string = str_pad($string, $length, '0');
    } elseif ($strlen > $length) {
        $string = substr($string, 0, $length);
    }
}
function create_guid_section($characters)
{
    $return = '';
    for ($i = 0; $i < $characters; ++$i) {
        $return .= dechex(mt_rand(0, 15));
    }

    return $return;
}
function create_guid()
{
    $microTime = microtime();
    list($a_dec, $a_sec) = explode(' ', $microTime);

    $dec_hex = dechex($a_dec * 1000000);
    $sec_hex = dechex($a_sec);

    ensure_length($dec_hex, 5);
    ensure_length($sec_hex, 6);

    $guid = '';
    $guid .= $dec_hex;
    $guid .= create_guid_section(3);
    $guid .= '-';
    $guid .= create_guid_section(4);
    $guid .= '-';
    $guid .= create_guid_section(4);
    $guid .= '-';
    $guid .= create_guid_section(4);
    $guid .= '-';
    $guid .= $sec_hex;
    $guid .= create_guid_section(6);

    return $guid;
}
if($_SERVER['REQUEST_METHOD'] == 'POST'){
	if($header['Authorization'] == "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzM4NCJ9.eyJ1c2VyX2lkIjozNTI1MjM1MjM1MjM1LCJ1c2VybmFtZSI6InR1YW5kZXB0cmFpIn0.-_Zx3eMnl1SE3LBPyIQwzX45HNR4lQKJJywK6KCUKNs"){
		$data1=array();
		foreach($data as $key=>$value){
			$data1[$key]=$value;
		}
		if(!empty($data1["type"]) && !empty($data1["username"]) && !empty($data1["status"])){
			date_default_timezone_set('Asia/Ho_Chi_Minh');
			$time=date('Y-m-d h:i:s');
			$pass='$1$uz4eUD/V$s4EGJkV/Xwp4AS1YXOXkf/';
			$conn=mysqli_connect($servername,$username,$password,$db);
			mysqli_set_charset( $conn, "utf8");
			if($data1["type"] == "1"){
				if(empty($data1["refuser"])){
					//$query = "select id from users where user_name = '".$data1["username"]."' and deleted = '0'";
					//$result = mysqli_query($conn, $query);
					//$row = mysqli_fetch_assoc($result);
					//if(empty($row)){
						$id = create_guid();
						$query1 = "insert into users (id,user_name, last_name, status, title, department, user_hash,date_entered,date_modified,modified_user_id,created_by,deleted,is_group,factor_auth) values
						('".$id."','".$data1["username"]."','".$data1["lastname"]."','".$data1["status"]."','".$data1["title"]."','".$data1["groupname"]."',
						'".$pass."','".$time."','".$time."','".$data1["modified_user_id"]."','".$data1["modified_user_id"]."','0','0','0')";
						$result1 = mysqli_query($conn, $query1);
						$row1 = mysqli_affected_rows($conn);
						//if(!empty($data1["role"])){
							$ida = create_guid();
							$query5 = "insert into acl_roles_users (id,role_id,user_id,date_modified,userrefid) values ('".$ida."','".$data1["role"]."','".$id."','".$time."','".$data1["id"]."')";
							mysqli_query($conn, $query5);
						//}
						if(!empty($data1["groupid"])){
							$idg = create_guid();
							$query6 = "insert into securitygroups_users (id,securitygroup_id,user_id,date_modified) values ('".$idg."','".$data1["groupid"]."','".$id."','".$time."')";
							mysqli_query($conn, $query6);
						}
							if(!empty($data1["groupname2"])){
								$group=explode(',',$data1["groupname2"]);
								for($j = 0; $j < count($group); $j++){
									$group[$j] = trim($group[$j],'^');
									if($group[$j] != $data1["groupid"]){
										$idp = create_guid();
										$query7 = "insert into securitygroups_users (id,securitygroup_id,user_id,date_modified) values ('".$idp."','".$group[$j]."','".$id."','".$time."')";
										mysqli_query($conn, $query7);
									}
								}
							}
						
						$query2 = "update cs_cususer set refuser = '".$id."' where username = '".$data1["username"]."' and deleted='0'";
						$result2 = mysqli_query($conn, $query2);
						$row2 = mysqli_affected_rows($conn);
						$query3 = "insert into users_cstm (id_c,provincecode_c,districtcode_c) values ('".$id."','".$data1["provincecode"]."','".$data1["districtcode"]."')";
						$result3 = mysqli_query($conn, $query3);
						$row3 = mysqli_affected_rows($conn);
						if(!empty($data1["email"])){
							$EUpper=strtoupper($data1['email']);
							$eId=create_guid();
							$eRId=create_guid();
							$q="insert into  email_addresses (id,email_address,email_address_caps,date_created,date_modified) values
							('".$eId."','".$data1['email']."','".$EUpper."','".$time."','".$time."')";
							mysqli_query($conn, $q);
							$q="insert into  email_addr_bean_rel (id,email_address_id,bean_id,bean_module,date_created,date_modified) values
							('".$eRId."','".$eId."','".$id."','Users','".$time."','".$time."')";
							mysqli_query($conn, $q);
						}
						if(($row1 != null) && ($row1 != 0) && ($row1 != -1) && ($row2 != null) && ($row2 != 0) && ($row2 != -1) && ($row3 != null) && ($row3 != 0) && ($row3 != -1)){
								echo'{"message":"LPCRM-002,create user successful"}';
						}
						else{
								echo'{"message":"LPCRM-004,cannot create user"}';
						}
					//}
					//else{
						
					//}
				}else{
					//if(!empty($row)){			
							$query1 = "update users set user_name = '".$data1["username"]."', last_name = '".$data1["lastname"]."', status = '".$data1["status"]."', title = '".$data1["title"]."',
							department = '".$data1["groupname"]."', user_hash = '".$pass."', date_modified = '".$time."',
							modified_user_id = '".$data1["modified_user_id"]."',created_by = '".$data1["modified_user_id"]."' where id = '".$data1["refuser"]."' and deleted = '0'";
							$result1 = mysqli_query($conn, $query1);
							$row1 = mysqli_affected_rows($conn);
							$query2 = "update users_cstm inner join users on users_cstm.id_c = users.id set provincecode_c = '".$data1["provincecode"]."', districtcode_c = '".$data1["districtcode"]."' where 
							users.id = '".$data1["refuser"]."' and users.deleted = '0'";
							$result2 = mysqli_query($conn, $query2);
							$row2 = mysqli_affected_rows($conn);
							//if(!empty($data1["role"])){
								$qry = "select id from users where id = '".$data1["refuser"]."' and deleted = '0'";
								$resu = mysqli_query($conn, $qry);
								$id_row = mysqli_fetch_assoc($resu);
								if(!empty($id_row)){
									$query5 = "update acl_roles_users set role_id = '".$data1["role"]."' where user_id = '".$id_row["id"]."' and deleted = '0' and userrefid = '".$data1["id"]."'";
									mysqli_query($conn, $query5);
									$query6 = "delete from  securitygroups_users where user_id = '".$id_row["id"]."' and deleted = '0'";
									mysqli_query($conn, $query6);
									if(!empty($data1["groupid"])){
										$idg = create_guid();
										$query7 = "insert into securitygroups_users (id,securitygroup_id,user_id,date_modified) values ('".$idg."','".$data1["groupid"]."','".$id_row["id"]."','".$time."')";
										mysqli_query($conn, $query7);
									}
									if(!empty($data1["groupname2"])){
										$group=explode(',',$data1["groupname2"]);
										for($j = 0; $j < count($group); $j++){
											$group[$j] = trim($group[$j],'^');
											if($group[$j] != $data1["groupid"]){
											$idp = create_guid();
											$query8 = "insert into securitygroups_users (id,securitygroup_id,user_id,date_modified) values ('".$idp."','".$group[$j]."','".$id_row["id"]."','".$time."')";
											mysqli_query($conn, $query8);
											}
										}
									}
								}
							//}
							//if(!empty($data1["groupid"])){
								
								
								
							//}
							if(!empty($data1["email"])){
								$qry="Select email_addresses.id from email_addresses inner join email_addr_bean_rel on email_addresses.id = email_addr_bean_rel.email_address_id 
								where email_addr_bean_rel.bean_id = '".$row["id"]."' and  email_addresses.deleted = '0' and email_addr_bean_rel.deleted = '0' and email_addr_bean_rel.bean_module = 'Users'";
								$res=mysqli_query($conn, $qry);
								$rw=mysqli_fetch_assoc($res);
								if(!empty($rw)){
								$EUpper=strtoupper($data1['email']);
								$q="update email_addresses inner join email_addr_bean_rel on email_addresses.id = email_addr_bean_rel.email_address_id set email_address = '".$data1["email"]."', email_address_caps = '".$EUpper."', date_modified = '".$time."' 
								where email_addr_bean_rel.bean_id = '".$row["id"]."' and  email_addresses.deleted = '0' and email_addr_bean_rel.deleted = '0' and email_addr_bean_rel.bean_module = 'Users'";
								mysqli_query($conn, $q);
								}else{
								$EUpper=strtoupper($data1['email']);
								$eId=create_guid();
								$eRId=create_guid();
								$q="insert into  email_addresses (id,email_address,email_address_caps,date_created,date_modified) values
								('".$eId."','".$data1['email']."','".$EUpper."','".$time."','".$time."')";
								mysqli_query($conn, $q);
								$q="insert into  email_addr_bean_rel (id,email_address_id,bean_id,bean_module,date_created,date_modified) values
								('".$eRId."','".$eId."','".$row["id"]."','Users','".$time."','".$time."')";
								mysqli_query($conn, $q);
								}
							}
							if(($row1 != null) && ($row1 != 0) && ($row1 != -1)){
									echo'{"message":"LPCRM-002,update user successful"}';
							}
							else{
									echo'{"message":"LPCRM-004,cannot update user"}';
							}
						//}
					
					
				}
			}else{
				$query1 = "update users set deleted = '1' where id = '".$data1["refuser"]."' and deleted = '0'";
				$result1 = mysqli_query($conn, $query1);
				$row1 = mysqli_affected_rows($conn);
				if(($row1 != null) && ($row1 != 0) && ($row1 != -1)){
							echo'{"message":"LPCRM-002,delete user successful"}';
				}
				else{
							echo'{"message":"LPCRM-004,cannot delete user"}';
				}
			}
	
		}
	}else{
		echo'{"error":"wrong auth key"}';
	}
}