<?php

header("Content-type: application/json; charset=utf-8");

require_once  '../config.php';
require_once  '../config_override.php';
$userextension = $_POST["user"];
$servername = $sugar_config["dbconfig"]["db_host_name"];
$username   = $sugar_config["dbconfig"]["db_user_name"];
$password   = $sugar_config["dbconfig"]["db_password"];

$conn = new mysqli($servername, $username, $password);
$GLOBALS['conn'] =$conn ;
$default_popup ='Lead';
switch ($sugar_config['defaultpopup']) {
	case 'Lead':
		$default_popup ='Lead';
	break;
	case 'Account':
		$default_popup ='Account';
	break;
	case 'Target':
		$default_popup ='Target';
	break;
	
	default:
		$default_popup ='Lead';
		break;
}

mysqli_select_db($GLOBALS['conn'],$sugar_config["dbconfig"]["db_name"]);
$sql = "SELECT * FROM  tttt_history_calls WHERE `userextension`=".$userextension." AND (`workstatus` = 'On-Call' OR  `workstatus` = 'Ring' ) " ;

mysqli_set_charset($GLOBALS['conn'],"utf8");
$result = mysqli_query($conn,$sql);
if(empty($result)) exit();
$row = mysqli_fetch_array($result,MYSQLI_ASSOC);
$timenow = strtotime("now") ;

if(isset($row["customernumber"])){
	$row['defaultpopup'] = $default_popup;
	$phone = $row["customernumber"] ;

	$sqlcontacts = "SELECT * FROM `contacts` WHERE (`phone_mobile`  LIKE '%$phone%' OR `phone_home` LIKE '%$phone%' OR `phone_work` LIKE '%$phone%') AND `deleted` =0 " ;

	$resultcontacts = mysqli_query($GLOBALS['conn'],$sqlcontacts);
	$rowcontacts = mysqli_fetch_array($resultcontacts,MYSQLI_ASSOC);
	$btn_string = '
	<a type="button" id="quick_create_meeting" title="Quick Create Meeting" class="btn btn-primary action-btn1" ><i class="fa fa-users" aria-hidden="true"></i></a>
	<a type="button" id="quick_create_opportunities" title ="Quick Create Opportunities" class="btn btn-primary action-btn2"><i class="fa fa-usd" ></i></a>
	<a type="button" id="quick_create_case" title ="Quick Create Case" class="btn btn-primary action-btn2"><i class="fa fa-folder" aria-hidden="true"></i></a>
	<a type="button" style="width: auto" id="save_popup" title ="Lưu" class="btn btn-danger">Lưu thông tin</a>
	';
	$btn_string2 = '
	<a type="button" id="quick_create_meeting" title="Quick Create Meeting" class="btn btn-primary action-btn1" ><i class="fa fa-users" aria-hidden="true"></i></a>
	<a type="button" style="width: auto" id="save_popup" title ="Lưu" class="btn btn-danger">Lưu thông tin</a>
	';
	if(isset($rowcontacts["last_name"]) ){

		$row["fullname"] = $rowcontacts["last_name"]." ".$rowcontacts["first_name"];
		$idcontact = $rowcontacts["id"];
		$row["buton"]   = $btn_string;
		$row['datafrom'] = 'Contacts';
		$row['href'] = 'index.php?module=Contacts&offset=3&stamp='.$timenow.'&return_module=Contacts&action=DetailView&record='.$idcontact;
		$row['idd'] = $idcontact;
		$sql_accounts_contacts = "SELECT * FROM `accounts_contacts` WHERE (`contact_id`  ='$idcontact') AND `deleted` =0 " ;
		$result_accounts_contacts = mysqli_query($GLOBALS['conn'],$sql_accounts_contacts);
		$row_accounts_contacts= mysqli_fetch_array($result_accounts_contacts,MYSQLI_ASSOC);
		if(isset($row_accounts_contacts['account_id'])){
			$idaccount =  $row_accounts_contacts["account_id"];
			$sqllaccount = "SELECT * FROM `accounts` WHERE (`id` ='$idaccount' ) AND `deleted` =0" ;
			$result_accounts = mysqli_query($GLOBALS['conn'],$sqllaccount);
			$row_accounts= mysqli_fetch_array($result_accounts,MYSQLI_ASSOC);
			$row['account_id'] = $idaccount;
			$row['account_name'] = $row_accounts['name'];
		}

	}else{
		$sqllaccount = "SELECT * FROM `accounts` WHERE (`phone_office`  LIKE '%$phone%' OR `phone_alternate` LIKE '%$phone%') AND `deleted` =0" ;
		$resultaccount = mysqli_query($GLOBALS['conn'],$sqllaccount);
		
		$rowaccount = mysqli_fetch_array($resultaccount,MYSQLI_ASSOC);
		if(!empty($rowaccount)){
			$row["fullname"] = $rowaccount["name"];
			$idaccount = $rowaccount["id"];
			$row["buton"]   = $btn_string;
			$row['datafrom'] = 'Accounts'; 
			$row['href'] = 'index.php?module=Accounts&offset=1&stamp='.$timenow.'&return_module=Accounts&action=DetailView&record='.$idaccount;
			$row['idd'] = $idaccount;
			$row['account_id'] = $idaccount;
			$row['account_name'] = $rowaccount['name'];
		}else{
			$sqlleads = "SELECT * FROM `leads` WHERE (`phone_mobile`  LIKE '%$phone%' OR `phone_home` LIKE '%$phone%' OR `phone_work` LIKE '%$phone%') AND `deleted` =0 " ;
			$resultleads = mysqli_query($GLOBALS['conn'],$sqlleads);

			$rowleads = mysqli_fetch_array($resultleads,MYSQLI_ASSOC);
			if(!empty($rowleads)){
				$row["fullname"] = $rowleads["last_name"]." ".$rowleads["first_name"];
				$idlead = $rowleads["id"];
				$row["buton"]   = $btn_string2;
				$row['datafrom'] = 'Leads';
				$row['href'] = 'index.php?module=Leads&offset=1&stamp='.$timenow.'&return_module=Leads&action=DetailView&record='.$idlead;
				$row['idd'] = $idlead;
			}else{ 
				$sqlprospects = "SELECT * FROM `prospects` WHERE (`phone_mobile`  LIKE '%$phone%' OR `phone_home` LIKE '%$phone%' OR `phone_work` LIKE '%$phone%') AND `deleted` =0 " ;
				$resultprospects = mysqli_query($GLOBALS['conn'],$sqlprospects);

				$rowprospects = mysqli_fetch_array($resultprospects,MYSQLI_ASSOC);
				if(!empty($rowprospects)){
					$row["fullname"] = $rowprospects["last_name"]." ".$rowprospects["first_name"];
					$idprospects = $rowprospects["id"];
					$row["buton"]   = $btn_string2;
					$row['datafrom'] = 'Prospects';
					$row['href'] ='index.php?module=Prospects&offset=1&stamp='.$timenow.'&return_module=Prospects&action=DetailView&record='.$idprospects;
					$row['idd'] = $idprospects;
				}else{
					$row["fullname"] =  "Không xác định";

					$row["buton"]   = '
					<a  class="btn btn-primary action-btn3" id="call_quick_create_lead" title="Quick Create Lead"> <span class="glyphicon glyphicon-plus"></span></a>
					<a  class="btn btn-primary action-btn3" id="call_quick_create_contact" title="Quick Create Contact"><span class="glyphicon glyphicon-plus"></span></a>
					<a  class="btn btn-primary action-btn3" id="call_quick_create_target" title="Quick Create Target"><span class="glyphicon glyphicon-plus"></span></a>
					<a type="button" style="width: auto" id="save_popup" title ="Lưu" class="btn btn-danger">Lưu thông tin</a>
					';
				}
			}
		}

	}

}

echo json_encode($row);
exit();

