<?php
if (isset($_SERVER['HTTPS']) &&
		($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
		isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
		$_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https'){
					$protocol = 'https://';
		}
		else {
			$protocol = 'http://';
		}
		$data = array(
		"time"=>$_GET["time"],
		"page"=>$_GET["page"],
		"recordPerPage"=>$_GET["recordPerPage"],
		"myProduct"=>$_GET["myProduct"],
		"user"=>$_GET["user"]
		);
			$apiUrl = $protocol.$_SERVER['HTTP_HOST']."/index.php?entryPoint=getProductForUser";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_URL, $apiUrl);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            $response = curl_exec($ch);
			echo $response;
			