<?php
$entry_point_registry['phone_account'] = array(
    'file' => 'custom/modules/AOS_Quotes/entrypoint/phone_account.php',
    'auth' => true,
);
$entry_point_registry['mobileref_account'] = array(
    'file' => 'custom/modules/AOS_Invoices/entrypoint/mobileref_account.php',
    'auth' => true,
);
$entry_point_registry['Accounts_phone'] = array(
    'file' => 'custom/modules/AOS_Quotes/entrypoint/Accounts_phone.php',
    'auth' => true,
);

$entry_point_registry['mobileref'] = array(
    'file' => 'custom/modules/AOS_Invoices/entrypoint/mobileref.php',
    'auth' => true,
);