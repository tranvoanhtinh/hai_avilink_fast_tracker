<?php

function getIndustries()
{
    $industries = array();
    $industries[''] = '';

    $sql = 'SELECT id, name FROM mts_industry WHERE deleted = 0 ORDER BY name';
    $res = $GLOBALS['db']->query($sql);

    while($row = $GLOBALS['db']->fetchByAssoc($res)) {
        $industries[$row['id']] = $row['name'];
    }
    return $industries;
}