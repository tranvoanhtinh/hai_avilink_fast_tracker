<?php


if (!isset($hook_array) || !is_array($hook_array)) {
    $hook_array = array();
}
if (!isset($hook_array['before_save']) || !is_array($hook_array['before_save'])) {
    $hook_array['before_save'] = array();
}
$hook_array['before_save'][] = Array(99, 'Lead Card Management', 'custom/include/LogicHooks/LeadCardLogicHook.php','LeadCardLogicHook', 'autoSetStatus');