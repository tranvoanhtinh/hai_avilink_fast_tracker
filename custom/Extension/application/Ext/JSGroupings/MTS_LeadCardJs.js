SUGAR.util.doWhen(function () {
    return $('head').length > 0;
}, function () {
    $('head').append('<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">');
});

SUGAR.util.doWhen(function () {
    return $('.p_login_top').length > 0;
}, function () {
    $('.p_login_top').find('a').css('background-image', 'none');
});