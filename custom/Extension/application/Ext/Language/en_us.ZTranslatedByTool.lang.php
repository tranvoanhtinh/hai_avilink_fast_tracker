<?php
$app_strings['LBL_SEARCH_REAULTS_TITLE'] = 'Results';

$app_strings['ERR_SEARCH_INVALID_QUERY'] = 'An error has occurred while performing the search. Your query syntax might not be valid.';

$app_strings['ERR_SEARCH_NO_RESULTS'] = 'No results matching your search criteria. Try broadening your search.';

$app_strings['LBL_SEARCH_PERFORMED_IN'] = 'Search performed in';

$app_strings['LBL_EMAIL_CODE'] = 'Email Code:';

$app_strings['LBL_SEND'] = 'Send';

$app_strings['LBL_LOGOUT'] = 'Logout';

$app_strings['LBL_TOUR_NEXT'] = 'Next';

$app_strings['LBL_TOUR_SKIP'] = 'Skip';

$app_strings['LBL_TOUR_BACK'] = 'Back';

$app_strings['LBL_TOUR_TAKE_TOUR'] = 'Take the tour';

$app_strings['LBL_MOREDETAIL'] = 'More Detail';

$app_strings['LBL_EDIT_INLINE'] = 'Edit Inline';

$app_strings['LBL_VIEW_INLINE'] = 'View';

$app_strings['LBL_BASIC_SEARCH'] = 'Filter';

$app_strings['LBL_BLANK'] = ' ';

$app_strings['LBL_ID_FF_ADD'] = 'Add';

$app_strings['LBL_ID_FF_ADD_EMAIL'] = 'Add Email Address';

$app_strings['LBL_HIDE_SHOW'] = 'Hide/Show';

$app_strings['LBL_DELETE_INLINE'] = 'Delete';

$app_strings['LBL_ID_FF_CLEAR'] = 'Clear';

$app_strings['LBL_ID_FF_VCARD'] = 'vCard';

$app_strings['LBL_ID_FF_REMOVE'] = 'Remove';

$app_strings['LBL_ID_FF_REMOVE_EMAIL'] = 'Remove Email Address';

$app_strings['LBL_ID_FF_OPT_OUT'] = 'Opt Out';

$app_strings['LBL_ID_FF_INVALID'] = 'Make Invalid';

$app_strings['LBL_ADD'] = 'Add';

$app_strings['LBL_COMPANY_LOGO'] = 'Company logo';

$app_strings['LBL_CONNECTORS_POPUPS'] = 'Connectors Popups';

$app_strings['LBL_CLOSEINLINE'] = 'Close';

$app_strings['LBL_VIEWINLINE'] = 'View';

$app_strings['LBL_INFOINLINE'] = 'Info';

$app_strings['LBL_PRINT'] = 'Print';

$app_strings['LBL_HELP'] = 'Help';

$app_strings['LBL_ID_FF_SELECT'] = 'Select';

$app_strings['DEFAULT'] = 'BASIC';

$app_strings['LBL_SORT'] = 'Sort';

$app_strings['LBL_EMAIL_SMTP_SSL_OR_TLS'] = 'Enable SMTP over SSL or TLS?';

$app_strings['LBL_NO_ACTION'] = 'There is no action by that name: %s';

$app_strings['LBL_NO_SHORTCUT_MENU'] = 'There are no actions available.';

$app_strings['LBL_NO_DATA'] = 'No Data';

$app_strings['LBL_ROUTING_FLAGGED'] = 'flag set';

$app_strings['LBL_ROUTING_TO'] = 'to';

$app_strings['LBL_ROUTING_TO_ADDRESS'] = 'to address';

$app_strings['LBL_ROUTING_WITH_TEMPLATE'] = 'with template';

$app_strings['NTC_OVERWRITE_ADDRESS_PHONE_CONFIRM'] = 'This record currently contains values in the Office Phone and Address fields. To overwrite these values with the following Office Phone and Address of the Account that you selected, click ';

$app_strings['LBL_DROP_HERE'] = '[Drop Here]';

$app_strings['LBL_EMAIL_ACCOUNTS_GMAIL_DEFAULTS'] = 'Prefill Gmail™ Defaults';

$app_strings['LBL_EMAIL_ACCOUNTS_NAME'] = 'Name';

$app_strings['LBL_EMAIL_ACCOUNTS_OUTBOUND'] = 'Outgoing Mail Server Properties';

$app_strings['LBL_EMAIL_ACCOUNTS_SMTPPASS'] = 'SMTP Password';

$app_strings['LBL_EMAIL_ACCOUNTS_SMTPPORT'] = 'SMTP Port';

$app_strings['LBL_EMAIL_ACCOUNTS_SMTPSERVER'] = 'SMTP Server';

$app_strings['LBL_EMAIL_ACCOUNTS_SMTPUSER'] = 'SMTP Username';

$app_strings['LBL_EMAIL_ACCOUNTS_SMTPDEFAULT'] = 'Default';

$app_strings['LBL_EMAIL_WARNING_MISSING_USER_CREDS'] = 'Warning: Missing username and password for outgoing mail account.';

$app_strings['LBL_EMAIL_ACCOUNTS_SUBTITLE'] = 'Set up Mail Accounts to view incoming emails from your email accounts.';

$app_strings['LBL_EMAIL_ACCOUNTS_OUTBOUND_SUBTITLE'] = 'Provide SMTP mail server information to use for outgoing email in Mail Accounts.';

$app_strings['LBL_EMAIL_ADDRESS_BOOK_ADD'] = 'Done';

$app_strings['LBL_EMAIL_ADDRESS_BOOK_CLEAR'] = 'Clear';

$app_strings['LBL_EMAIL_ADDRESS_BOOK_ADD_TO'] = 'To:';

$app_strings['LBL_EMAIL_ADDRESS_BOOK_ADD_CC'] = 'Cc:';

$app_strings['LBL_EMAIL_ADDRESS_BOOK_ADD_BCC'] = 'Bcc:';

$app_strings['LBL_EMAIL_ADDRESS_BOOK_ADRRESS_TYPE'] = 'To/Cc/Bcc';

$app_strings['LBL_EMAIL_ADDRESS_BOOK_EMAIL_ADDR'] = 'Email Address';

$app_strings['LBL_EMAIL_ADDRESS_BOOK_FILTER'] = 'Filter';

$app_strings['LBL_EMAIL_ADDRESS_BOOK_NAME'] = 'Name';

$app_strings['LBL_EMAIL_ADDRESS_BOOK_NOT_FOUND'] = 'No Addresses Found';

$app_strings['LBL_EMAIL_ADDRESS_BOOK_SAVE_AND_ADD'] = 'Save & Add to Address Book';

$app_strings['LBL_EMAIL_ADDRESS_BOOK_SELECT_TITLE'] = 'Select Email Recipients';

$app_strings['LBL_EMAIL_ADDRESS_BOOK_TITLE'] = 'Address Book';

$app_strings['LBL_EMAIL_REMOVE_SMTP_WARNING'] = 'Warning! The outbound account you are trying to delete is associated to an existing inbound account. Are you sure you want to continue?';

$app_strings['LBL_EMAIL_ADDRESSES'] = 'Email';

$app_strings['LBL_EMAIL_ADDRESS_PRIMARY'] = 'Email Address';

$app_strings['LBL_EMAIL_ADDRESS_OPT_IN'] = 'You have confirmed that your email address has been opted in: ';

$app_strings['LBL_EMAIL_ADDRESS_OPT_IN_ERR'] = 'Unable to confirm email address';

$app_strings['LBL_EMAIL_ARCHIVE_TO_SUITE'] = 'Import to SuiteCRM';

$app_strings['LBL_EMAIL_ASSIGNMENT'] = 'Assignment';

$app_strings['LBL_EMAIL_ATTACH_FILE_TO_EMAIL'] = 'Attach';

$app_strings['LBL_EMAIL_ATTACHMENT'] = 'Attach';

$app_strings['LBL_EMAIL_ATTACHMENTS'] = 'From Local System';

$app_strings['LBL_EMAIL_ATTACHMENTS2'] = 'From SuiteCRM Documents';

$app_strings['LBL_EMAIL_ATTACHMENTS3'] = 'Template Attachments';

$app_strings['LBL_EMAIL_ATTACHMENTS_FILE'] = 'File';

$app_strings['LBL_EMAIL_ATTACHMENTS_DOCUMENT'] = 'Document';

$app_strings['LBL_EMAIL_BCC'] = 'BCC';

$app_strings['LBL_EMAIL_CANCEL'] = 'Cancel';

$app_strings['LBL_EMAIL_CC'] = 'CC';

$app_strings['LBL_EMAIL_CHARSET'] = 'Character Set';

$app_strings['LBL_EMAIL_CHECK'] = 'Check Mail';

$app_strings['LBL_EMAIL_CHECKING_NEW'] = 'Checking for New Email';

$app_strings['LBL_EMAIL_CHECKING_DESC'] = 'One moment please... <br><br>If this is the first check for the mail account, it may take some time.';

$app_strings['LBL_EMAIL_CLOSE'] = 'Close';

$app_strings['LBL_EMAIL_COFFEE_BREAK'] = 'Checking for New Email. <br><br>Large mail accounts may take a considerable amount of time.';

$app_strings['LBL_EMAIL_COMPOSE'] = 'Email';

$app_strings['LBL_EMAIL_COMPOSE_ERR_NO_RECIPIENTS'] = 'Please enter recipient(s) for this email.';

$app_strings['LBL_EMAIL_COMPOSE_NO_BODY'] = 'The body of this email is empty. Send anyway?';

$app_strings['LBL_EMAIL_COMPOSE_NO_SUBJECT'] = 'This email has no subject. Send anyway?';

$app_strings['LBL_EMAIL_COMPOSE_NO_SUBJECT_LITERAL'] = '(no subject)';

$app_strings['LBL_EMAIL_COMPOSE_INVALID_ADDRESS'] = 'Please enter valid email address for To, CC and BCC fields';

$app_strings['LBL_EMAIL_CONFIRM_CLOSE'] = 'Discard this email?';

$app_strings['LBL_EMAIL_CONFIRM_DELETE_SIGNATURE'] = 'Are you sure you want to delete this signature?';

$app_strings['LBL_EMAIL_SENT_SUCCESS'] = 'Email sent';

$app_strings['LBL_EMAIL_CREATE_NEW'] = '--Create On Save--';

$app_strings['LBL_EMAIL_MULT_GROUP_FOLDER_ACCOUNTS'] = 'Multiple';

$app_strings['LBL_EMAIL_MULT_GROUP_FOLDER_ACCOUNTS_EMPTY'] = 'Empty';

$app_strings['LBL_EMAIL_DATE_SENT_BY_SENDER'] = 'Date Sent by Sender';

$app_strings['LBL_EMAIL_DATE_TODAY'] = 'Today';

$app_strings['LBL_EMAIL_DELETE'] = 'Delete';

$app_strings['LBL_EMAIL_DELETE_CONFIRM'] = 'Delete selected messages?';

$app_strings['LBL_EMAIL_DELETE_SUCCESS'] = 'Email deleted successfully.';

$app_strings['LBL_EMAIL_DELETING_MESSAGE'] = 'Deleting Message';

$app_strings['LBL_EMAIL_DETAILS'] = 'Details';

$app_strings['LBL_EMAIL_EDIT_CONTACT_WARN'] = 'Only the Primary address will be used when working with Contacts.';

$app_strings['LBL_EMAIL_EMPTYING_TRASH'] = 'Emptying Trash';

$app_strings['LBL_EMAIL_DELETING_OUTBOUND'] = 'Deleting outbound server';

$app_strings['LBL_EMAIL_CLEARING_CACHE_FILES'] = 'Clearing cache files';

$app_strings['LBL_EMAIL_EMPTY_MSG'] = 'No emails to display.';

$app_strings['LBL_EMAIL_EMPTY_ADDR_MSG'] = 'No email addresses to display.';

$app_strings['LBL_EMAIL_ERROR_ADD_GROUP_FOLDER'] = 'Folder name must be unique and not empty. Please try again.';

$app_strings['LBL_EMAIL_ERROR_DELETE_GROUP_FOLDER'] = 'Cannot delete a folder. Either the folder or its children has emails or a mail box associated to it.';

$app_strings['LBL_EMAIL_ERROR_CANNOT_FIND_NODE'] = 'Cannot determine the intended folder from context. Try again.';

$app_strings['LBL_EMAIL_ERROR_CHECK_IE_SETTINGS'] = 'Please check your settings.';

$app_strings['LBL_EMAIL_ERROR_DESC'] = 'Errors were detected: ';

$app_strings['LBL_EMAIL_DELETE_ERROR_DESC'] = 'You do not have access to this area. Contact your site administrator to obtain access.';

$app_strings['LBL_EMAIL_ERROR_DUPE_FOLDER_NAME'] = 'SuiteCRM Folder names must be unique.';

$app_strings['LBL_EMAIL_ERROR_EMPTY'] = 'Please enter some search criteria.';

$app_strings['LBL_EMAIL_ERROR_GENERAL_TITLE'] = 'An error has occurred';

$app_strings['LBL_EMAIL_ERROR_MESSAGE_DELETED'] = 'Message Removed from Server';

$app_strings['LBL_EMAIL_ERROR_IMAP_MESSAGE_DELETED'] = 'Either the message was removed from Server or moved to a different folder';

$app_strings['LBL_EMAIL_ERROR_MAILSERVERCONNECTION'] = 'Connection to the mail server failed. Please contact your Administrator';

$app_strings['LBL_EMAIL_ERROR_MOVE'] = 'Moving email between servers and/or mail accounts is not supported at this time.';

$app_strings['LBL_EMAIL_ERROR_MOVE_TITLE'] = 'Move Error';

$app_strings['LBL_EMAIL_ERROR_NAME'] = 'A name is required.';

$app_strings['LBL_EMAIL_ERROR_FROM_ADDRESS'] = 'From Address is required. Please enter a valid email address.';

$app_strings['LBL_EMAIL_ERROR_NO_FILE'] = 'Please provide a file.';

$app_strings['LBL_EMAIL_ERROR_SERVER'] = 'A mail server address is required.';

$app_strings['LBL_EMAIL_ERROR_SAVE_ACCOUNT'] = 'The mail account may not have been saved.';

$app_strings['LBL_EMAIL_ERROR_TIMEOUT'] = 'An error has occurred while communicating with the mail server.';

$app_strings['LBL_EMAIL_ERROR_USER'] = 'A login name is required.';

$app_strings['LBL_EMAIL_ERROR_PORT'] = 'A mail server port is required.';

$app_strings['LBL_EMAIL_ERROR_PROTOCOL'] = 'A server protocol is required.';

$app_strings['LBL_EMAIL_ERROR_MONITORED_FOLDER'] = 'Monitored Folder is required.';

$app_strings['LBL_EMAIL_ERROR_TRASH_FOLDER'] = 'Trash Folder is required.';

$app_strings['LBL_EMAIL_ERROR_VIEW_RAW_SOURCE'] = 'This information is not available';

$app_strings['LBL_EMAIL_ERROR_NO_OUTBOUND'] = 'No outgoing mail server specified.';

$app_strings['LBL_EMAIL_ERROR_SENDING'] = 'Error Sending Email. Please contact your administrator for assistance.';

$app_strings['LBL_EMAIL_FOLDERS'] = '<img src=';

$app_strings['LBL_EMAIL_FOLDERS_SHORT'] = '<img src=';

$app_strings['LBL_EMAIL_FOLDERS_ADD'] = 'Add';

$app_strings['LBL_EMAIL_FOLDERS_ADD_DIALOG_TITLE'] = 'Add New Folder';

$app_strings['LBL_EMAIL_FOLDERS_RENAME_DIALOG_TITLE'] = 'Rename Folder';

$app_strings['LBL_EMAIL_FOLDERS_ADD_NEW_FOLDER'] = 'Save';

$app_strings['LBL_EMAIL_FOLDERS_ADD_THIS_TO'] = 'Add this folder to';

$app_strings['LBL_EMAIL_FOLDERS_CHANGE_HOME'] = 'This folder cannot be changed';

$app_strings['LBL_EMAIL_FOLDERS_DELETE_CONFIRM'] = 'Are you sure you would like to delete this folder?\\nThis process cannot be reversed.\\nFolder deletions will cascade to all contained folders.';

$app_strings['LBL_EMAIL_FOLDERS_NEW_FOLDER'] = 'New Folder Name';

$app_strings['LBL_EMAIL_FOLDERS_NO_VALID_NODE'] = 'Please select a folder before performing this action.';

$app_strings['LBL_EMAIL_FOLDERS_TITLE'] = 'Folder Management';

$app_strings['LBL_EMAIL_FORWARD'] = 'Forward';

$app_strings['LBL_EMAIL_DELIMITER'] = '::;::';

$app_strings['LBL_EMAIL_DOWNLOAD_STATUS'] = 'Downloaded [[count]] of [[total]] emails';

$app_strings['LBL_EMAIL_FROM'] = 'From';

$app_strings['LBL_EMAIL_GROUP'] = 'group';

$app_strings['LBL_EMAIL_UPPER_CASE_GROUP'] = 'Group';

$app_strings['LBL_EMAIL_HOME_FOLDER'] = 'Home';

$app_strings['LBL_EMAIL_IE_DELETE'] = 'Deleting Mail Account';

$app_strings['LBL_EMAIL_IE_DELETE_SIGNATURE'] = 'Deleting signature';

$app_strings['LBL_EMAIL_IE_DELETE_CONFIRM'] = 'Are you sure you would like to delete this mail account?';

$app_strings['LBL_EMAIL_IE_DELETE_SUCCESSFUL'] = 'Deletion successful.';

$app_strings['LBL_EMAIL_IE_SAVE'] = 'Saving Mail Account Information';

$app_strings['LBL_EMAIL_IMPORTING_EMAIL'] = 'Importing Email';

$app_strings['LBL_EMAIL_IMPORT_EMAIL'] = 'Import into SuiteCRM';

$app_strings['LBL_EMAIL_IMPORT_SETTINGS'] = 'Import Settings';

$app_strings['LBL_EMAIL_INVALID'] = 'Invalid';

$app_strings['LBL_EMAIL_LOADING'] = 'Loading...';

$app_strings['LBL_EMAIL_MARK'] = 'Mark';

$app_strings['LBL_EMAIL_MARK_FLAGGED'] = 'As Flagged';

$app_strings['LBL_EMAIL_MARK_READ'] = 'As Read';

$app_strings['LBL_EMAIL_MARK_UNFLAGGED'] = 'As Unflagged';

$app_strings['LBL_EMAIL_MARK_UNREAD'] = 'As Unread';

$app_strings['LBL_EMAIL_ASSIGN_TO'] = 'Assign To';

$app_strings['LBL_EMAIL_MENU_ADD_FOLDER'] = 'Create Folder';

$app_strings['LBL_EMAIL_MENU_COMPOSE'] = 'Compose to';

$app_strings['LBL_EMAIL_MENU_DELETE_FOLDER'] = 'Delete Folder';

$app_strings['LBL_EMAIL_MENU_EMPTY_TRASH'] = 'Empty Trash';

$app_strings['LBL_EMAIL_MENU_SYNCHRONIZE'] = 'Synchronize';

$app_strings['LBL_EMAIL_MENU_CLEAR_CACHE'] = 'Clear cache files';

$app_strings['LBL_EMAIL_MENU_REMOVE'] = 'Remove';

$app_strings['LBL_EMAIL_MENU_RENAME_FOLDER'] = 'Rename Folder';

$app_strings['LBL_EMAIL_MENU_RENAMING_FOLDER'] = 'Renaming Folder';

$app_strings['LBL_EMAIL_MENU_MAKE_SELECTION'] = 'Please make a selection before trying this operation.';

$app_strings['LBL_EMAIL_MENU_HELP_ADD_FOLDER'] = 'Create a Folder (remote or in SuiteCRM)';

$app_strings['LBL_EMAIL_MENU_HELP_DELETE_FOLDER'] = 'Delete a Folder (remote or in SuiteCRM)';

$app_strings['LBL_EMAIL_MENU_HELP_EMPTY_TRASH'] = 'Empties all Trash folders for your mail accounts';

$app_strings['LBL_EMAIL_MENU_HELP_MARK_READ'] = 'Mark these email(s) read';

$app_strings['LBL_EMAIL_MENU_HELP_MARK_UNFLAGGED'] = 'Mark these email(s) unflagged';

$app_strings['LBL_EMAIL_MENU_HELP_RENAME_FOLDER'] = 'Rename a Folder (remote or in SuiteCRM)';

$app_strings['LBL_EMAIL_MESSAGES'] = 'messages';

$app_strings['LBL_EMAIL_ML_NAME'] = 'List Name';

$app_strings['LBL_EMAIL_ML_ADDRESSES_1'] = 'Selected List Addresses';

$app_strings['LBL_EMAIL_ML_ADDRESSES_2'] = 'Available List Addresses';

$app_strings['LBL_EMAIL_MULTISELECT'] = '<b>Ctrl-Click</b> to select multiples<br />(Mac users use <b>CMD-Click</b>)';

$app_strings['LBL_EMAIL_NO'] = 'No';

$app_strings['LBL_EMAIL_NOT_SENT'] = 'System is unable to process your request. Please contact the system administrator.';

$app_strings['LBL_EMAIL_OK'] = 'OK';

$app_strings['LBL_EMAIL_ONE_MOMENT'] = 'One moment please...';

$app_strings['LBL_EMAIL_OPEN_ALL'] = 'Open Multiple Messages';

$app_strings['LBL_EMAIL_OPTIONS'] = 'Options';

$app_strings['LBL_EMAIL_QUICK_COMPOSE'] = 'Quick Compose';

$app_strings['LBL_EMAIL_OPT_OUT'] = 'Opted Out';

$app_strings['LBL_EMAIL_OPT_OUT_AND_INVALID'] = 'Opted Out and Invalid';

$app_strings['LBL_EMAIL_PERFORMING_TASK'] = 'Performing Task';

$app_strings['LBL_EMAIL_PRIMARY'] = 'Primary';

$app_strings['LBL_EMAIL_PRINT'] = 'Print';

$app_strings['LBL_EMAIL_QC_BUGS'] = 'Bug';

$app_strings['LBL_EMAIL_QC_CASES'] = 'Case';

$app_strings['LBL_EMAIL_QC_LEADS'] = 'Lead';

$app_strings['LBL_EMAIL_QC_CONTACTS'] = 'Contact';

$app_strings['LBL_EMAIL_QC_TASKS'] = 'Task';

$app_strings['LBL_EMAIL_QC_OPPORTUNITIES'] = 'Opportunity';

$app_strings['LBL_EMAIL_QUICK_CREATE'] = 'Quick Create';

$app_strings['LBL_EMAIL_REBUILDING_FOLDERS'] = 'Rebuilding Folders';

$app_strings['LBL_EMAIL_RELATE_TO'] = 'Relate';

$app_strings['LBL_EMAIL_VIEW_RELATIONSHIPS'] = 'View Relationships';

$app_strings['LBL_EMAIL_RECORD'] = 'Email Record';

$app_strings['LBL_EMAIL_REMOVE'] = 'Remove';

$app_strings['LBL_EMAIL_REPLY'] = 'Reply';

$app_strings['LBL_EMAIL_REPLY_ALL'] = 'Reply All';

$app_strings['LBL_EMAIL_REPLY_TO'] = 'Reply-to';

$app_strings['LBL_EMAIL_RETRIEVING_MESSAGE'] = 'Retrieving Message';

$app_strings['LBL_EMAIL_RETRIEVING_RECORD'] = 'Retrieving Email Record';

$app_strings['LBL_EMAIL_SELECT_ONE_RECORD'] = 'Please select only one email record';

$app_strings['LBL_EMAIL_RETURN_TO_VIEW'] = 'Return to Previous Module?';

$app_strings['LBL_EMAIL_REVERT'] = 'Revert';

$app_strings['LBL_EMAIL_RELATE_EMAIL'] = 'Relate Email';

$app_strings['LBL_EMAIL_RULES_TITLE'] = 'Rule Management';

$app_strings['LBL_EMAIL_SAVE'] = 'Save';

$app_strings['LBL_EMAIL_SAVE_AND_REPLY'] = 'Save & Reply';

$app_strings['LBL_EMAIL_SAVE_DRAFT'] = 'Save Draft';

$app_strings['LBL_EMAIL_DRAFT_SAVED'] = 'Draft has been saved';

$app_strings['LBL_EMAIL_SEARCH'] = '<img src=';

$app_strings['LBL_EMAIL_SEARCH_SHORT'] = '<img src=';

$app_strings['LBL_EMAIL_SEARCH_DATE_FROM'] = 'Date From';

$app_strings['LBL_EMAIL_SEARCH_DATE_UNTIL'] = 'Date Until';

$app_strings['LBL_EMAIL_SEARCH_NO_RESULTS'] = 'No results match your search criteria.';

$app_strings['LBL_EMAIL_SEARCH_RESULTS_TITLE'] = 'Search Results';

$app_strings['LBL_EMAIL_SELECT'] = 'Select';

$app_strings['LBL_EMAIL_SEND'] = 'Send';

$app_strings['LBL_EMAIL_SENDING_EMAIL'] = 'Sending Email';

$app_strings['LBL_EMAIL_SETTINGS'] = 'Settings';

$app_strings['LBL_EMAIL_SETTINGS_ACCOUNTS'] = 'Mail Accounts';

$app_strings['LBL_EMAIL_SETTINGS_ADD_ACCOUNT'] = 'Clear Form';

$app_strings['LBL_EMAIL_SETTINGS_CHECK_INTERVAL'] = 'Check for New Mail';

$app_strings['LBL_EMAIL_SETTINGS_FROM_ADDR'] = 'From Address';

$app_strings['LBL_EMAIL_SETTINGS_FROM_TO_EMAIL_ADDR'] = 'Email Address For Test Notification:';

$app_strings['LBL_EMAIL_SETTINGS_FROM_NAME'] = 'From Name';

$app_strings['LBL_EMAIL_SETTINGS_REPLY_TO_ADDR'] = 'Reply to Address';

$app_strings['LBL_EMAIL_SETTINGS_FULL_SYNC'] = 'Synchronize All Mail Accounts';

$app_strings['LBL_EMAIL_TEST_NOTIFICATION_SENT'] = 'An email was sent to the specified email address using the provided outgoing mail settings. Please check to see if the email was received to verify the settings are correct.';

$app_strings['LBL_EMAIL_TEST_SEE_FULL_SMTP_LOG'] = 'See full SMTP Log';

$app_strings['LBL_EMAIL_SETTINGS_FULL_SYNC_WARN'] = 'Perform a full synchronization?\\nLarge mail accounts may take a few minutes.';

$app_strings['LBL_EMAIL_SUBSCRIPTION_FOLDER_HELP'] = 'Click the Shift key or the Ctrl key to select multiple folders.';

$app_strings['LBL_EMAIL_SETTINGS_GENERAL'] = 'General';

$app_strings['LBL_EMAIL_SETTINGS_GROUP_FOLDERS_CREATE'] = 'Create Group Folders';

$app_strings['LBL_EMAIL_SETTINGS_GROUP_FOLDERS_EDIT'] = 'Edit Group Folder';

$app_strings['LBL_EMAIL_SETTINGS_NAME'] = 'Mail Account Name';

$app_strings['LBL_EMAIL_SETTINGS_REQUIRE_REFRESH'] = 'Select the number of emails per page in the Inbox. This setting might require a page refresh in order to take effect.';

$app_strings['LBL_EMAIL_SETTINGS_RETRIEVING_ACCOUNT'] = 'Retrieving Mail Account';

$app_strings['LBL_EMAIL_SETTINGS_SAVED'] = 'The settings have been saved.';

$app_strings['LBL_EMAIL_SETTINGS_SEND_EMAIL_AS'] = 'Send Plain Text Emails Only';

$app_strings['LBL_EMAIL_SETTINGS_SHOW_NUM_IN_LIST'] = 'Emails per Page';

$app_strings['LBL_EMAIL_SETTINGS_TITLE_LAYOUT'] = 'Visual Settings';

$app_strings['LBL_EMAIL_SETTINGS_TITLE_PREFERENCES'] = 'Preferences';

$app_strings['LBL_EMAIL_SETTINGS_USER_FOLDERS'] = 'Available User Folders';

$app_strings['LBL_EMAIL_ERROR_PREPEND'] = 'An email error occurred:';

$app_strings['LBL_EMAIL_INVALID_PERSONAL_OUTBOUND'] = 'The outbound mail server selected for the mail account you are using is invalid. Check the settings or select a different mail server for the mail account.';

$app_strings['LBL_EMAIL_INVALID_SYSTEM_OUTBOUND'] = 'An outgoing mail server is not configured to send emails. Please configure an outgoing mail server or select an outgoing mail server for the mail account that you are using in Settings >> Mail Account.';

$app_strings['LBL_DEFAULT_EMAIL_SIGNATURES'] = 'Default Signature';

$app_strings['LBL_EMAIL_SIGNATURES'] = 'Signatures';

$app_strings['LBL_SMTPTYPE_GMAIL'] = 'Gmail';

$app_strings['LBL_SMTPTYPE_YAHOO'] = 'Yahoo! Mail';

$app_strings['LBL_SMTPTYPE_EXCHANGE'] = 'Microsoft Exchange';

$app_strings['LBL_SMTPTYPE_OTHER'] = 'Other';

$app_strings['LBL_EMAIL_SPACER_MAIL_SERVER'] = '[ Remote Folders ]';

$app_strings['LBL_EMAIL_SPACER_LOCAL_FOLDER'] = '[ SuiteCRM Folders ]';

$app_strings['LBL_EMAIL_SUBJECT'] = 'Subject';

$app_strings['LBL_EMAIL_SUCCESS'] = 'Success';

$app_strings['LBL_EMAIL_SUITE_FOLDER'] = 'SuiteCRM Folder';

$app_strings['LBL_EMAIL_TEMPLATE_EDIT_PLAIN_TEXT'] = 'Email template body is empty';

$app_strings['LBL_EMAIL_TEMPLATES'] = 'Templates';

$app_strings['LBL_EMAIL_TO'] = 'To';

$app_strings['LBL_EMAIL_VIEW'] = 'View';

$app_strings['LBL_EMAIL_VIEW_HEADERS'] = 'Display Headers';

$app_strings['LBL_EMAIL_VIEW_RAW'] = 'Display Raw Email';

$app_strings['LBL_EMAIL_VIEW_UNSUPPORTED'] = 'This feature is unsupported when used with POP3.';

$app_strings['LBL_DEFAULT_LINK_TEXT'] = 'Default link text.';

$app_strings['LBL_EMAIL_YES'] = 'Yes';

$app_strings['LBL_EMAIL_TEST_OUTBOUND_SETTINGS'] = 'Send Test Email';

$app_strings['LBL_EMAIL_TEST_OUTBOUND_SETTINGS_SENT'] = 'Test Email Sent';

$app_strings['LBL_EMAIL_MESSAGE_NO'] = 'Message No';

$app_strings['LBL_EMAIL_IMPORT_SUCCESS'] = 'Import Passed';

$app_strings['LBL_EMAIL_IMPORT_FAIL'] = 'Import Failed because either the message is already imported or deleted from server';

$app_strings['LBL_LINK_NONE'] = 'None';

$app_strings['LBL_LINK_ALL'] = 'All';

$app_strings['LBL_LINK_RECORDS'] = 'Records';

$app_strings['LBL_LINK_SELECT'] = 'Select';

$app_strings['LBL_LINK_ACTIONS'] = 'ACTIONS';

$app_strings['LBL_CLOSE_ACTIVITY_HEADER'] = 'Confirm';

$app_strings['LBL_CLOSE_ACTIVITY_CONFIRM'] = 'Do you want to close this #module#?';

$app_strings['LBL_INVALID_FILE_EXTENSION'] = 'Invalid File Extension';

$app_strings['ERR_AJAX_LOAD'] = 'An error has occurred:';

$app_strings['ERR_AJAX_LOAD_FAILURE'] = 'There was an error processing your request, please try again at a later time.';

$app_strings['ERR_AJAX_LOAD_FOOTER'] = 'If this error persists, please have your administrator disable Ajax for this module';

$app_strings['ERR_DECIMAL_SEP_EQ_THOUSANDS_SEP'] = 'The decimal separator cannot use the same character as the thousands separator.\\n\\n  Please change the values.';

$app_strings['ERR_DELETE_RECORD'] = 'A record number must be specified to delete the contact.';

$app_strings['ERR_EXPORT_DISABLED'] = 'Exports Disabled.';

$app_strings['ERR_EXPORT_TYPE'] = 'Error exporting ';

$app_strings['ERR_INVALID_EMAIL_ADDRESS'] = 'not a valid email address.';

$app_strings['ERR_INVALID_FILE_REFERENCE'] = 'Invalid File Reference';

$app_strings['ERR_NO_HEADER_ID'] = 'This feature is unavailable in this theme.';

$app_strings['ERR_NOT_ADMIN'] = 'Unauthorized access to administration.';

$app_strings['ERR_MISSING_REQUIRED_FIELDS'] = 'Missing required field:';

$app_strings['ERR_INVALID_REQUIRED_FIELDS'] = 'Invalid required field:';

$app_strings['ERR_INVALID_VALUE'] = 'Invalid Value:';

$app_strings['ERR_NO_SUCH_FILE'] = 'File does not exist on system';

$app_strings['ERR_NO_SINGLE_QUOTE'] = 'Cannot use the single quotation mark for ';

$app_strings['ERR_NOTHING_SELECTED'] = 'Please make a selection before proceeding.';

$app_strings['ERR_SELF_REPORTING'] = 'User cannot report to him or herself.';

$app_strings['ERR_SQS_NO_MATCH_FIELD'] = 'No match for field: ';

$app_strings['ERR_SQS_NO_MATCH'] = 'No Match';

$app_strings['ERR_ADDRESS_KEY_NOT_SPECIFIED'] = 'Please specify \'key\' index in displayParams attribute for the Meta-Data definition';

$app_strings['ERR_EXISTING_PORTAL_USERNAME'] = 'Error: The Portal Name is already assigned to another contact.';

$app_strings['ERR_COMPATIBLE_PRECISION_VALUE'] = 'Field value is not compatible with precision value';

$app_strings['ERR_EXTERNAL_API_SAVE_FAIL'] = 'An error occurred when trying to save to the external account.';

$app_strings['ERR_NO_DB'] = 'Could not connect to the database. Please refer to suitecrm.log for details (0).';

$app_strings['ERR_DB_FAIL'] = 'Database failure. Please refer to suitecrm.log for details.';

$app_strings['ERR_DB_VERSION'] = 'SuiteCRM {0} Files May Only Be Used With A SuiteCRM {1} Database.';

$app_strings['LBL_ACCOUNT'] = 'Account';

$app_strings['LBL_ACCOUNTS'] = 'Accounts';

$app_strings['LBL_ACTIVITIES_SUBPANEL_TITLE'] = 'Activities';

$app_strings['LBL_ACCUMULATED_HISTORY_BUTTON_KEY'] = 'H';

$app_strings['LBL_ACCUMULATED_HISTORY_BUTTON_LABEL'] = 'View Summary';

$app_strings['LBL_ACCUMULATED_HISTORY_BUTTON_TITLE'] = 'View Summary';

$app_strings['LBL_ADD_BUTTON'] = 'Add';

$app_strings['LBL_ADD_DOCUMENT'] = 'Add Document';

$app_strings['LBL_ADD_TO_PROSPECT_LIST_BUTTON_KEY'] = 'L';

$app_strings['LBL_ADD_TO_PROSPECT_LIST_BUTTON_LABEL'] = 'Add To Target List';

$app_strings['LBL_ADD_TO_PROSPECT_LIST_BUTTON_LABEL_ACCOUNTS_CONTACTS'] = 'Add Contacts To Target List';

$app_strings['LBL_ADDITIONAL_DETAILS_CLOSE_TITLE'] = 'Click to Close';

$app_strings['LBL_ADDITIONAL_DETAILS'] = 'Additional Details';

$app_strings['LBL_ADMIN'] = 'Admin';

$app_strings['LBL_ALT_HOT_KEY'] = 'Alt+';

$app_strings['LBL_ARCHIVE'] = 'Archive';

$app_strings['LBL_ASSIGNED_TO_USER'] = 'Assigned to User';

$app_strings['LBL_ASSIGNED_TO'] = 'Assigned to:';

$app_strings['LBL_BACK'] = 'Back';

$app_strings['LBL_BILLING_ADDRESS'] = 'Billing Address';

$app_strings['LBL_QUICK_CREATE'] = 'Create ';

$app_strings['LBL_BROWSER_TITLE'] = 'SuiteCRM - Open Source CRM';

$app_strings['LBL_BUGS'] = 'Bugs';

$app_strings['LBL_BY'] = 'by';

$app_strings['LBL_CALLS'] = 'Calls';

$app_strings['LBL_CAMPAIGNS_SEND_QUEUED'] = 'Send Queued Campaign Emails';

$app_strings['LBL_SUBMIT_BUTTON_LABEL'] = 'Submit';

$app_strings['LBL_CASE'] = 'Case';

$app_strings['LBL_CASES'] = 'Cases';

$app_strings['LBL_CHANGE_PASSWORD'] = 'Change password';

$app_strings['LBL_CHARSET'] = 'UTF-8';

$app_strings['LBL_CHECKALL'] = 'Check All';

$app_strings['LBL_CITY'] = 'City';

$app_strings['LBL_CLEAR_BUTTON_LABEL'] = 'Clear';

$app_strings['LBL_CLEAR_BUTTON_TITLE'] = 'Clear';

$app_strings['LBL_CLEARALL'] = 'Clear All';

$app_strings['LBL_CLOSE_BUTTON_TITLE'] = 'Close';

$app_strings['LBL_CLOSE_AND_CREATE_BUTTON_LABEL'] = 'Close and Create New';

$app_strings['LBL_CLOSE_AND_CREATE_BUTTON_TITLE'] = 'Close and Create New';

$app_strings['LBL_CLOSE_AND_CREATE_BUTTON_KEY'] = 'C';

$app_strings['LBL_OPEN_ITEMS'] = 'Open Items:';

$app_strings['LBL_COMPOSE_EMAIL_BUTTON_KEY'] = 'L';

$app_strings['LBL_COMPOSE_EMAIL_BUTTON_LABEL'] = 'Compose Email';

$app_strings['LBL_COMPOSE_EMAIL_BUTTON_TITLE'] = 'Compose Email';

$app_strings['LBL_SEARCH_DROPDOWN_YES'] = 'Yes';

$app_strings['LBL_SEARCH_DROPDOWN_NO'] = 'No';

$app_strings['LBL_CONTACT_LIST'] = 'Contact List';

$app_strings['LBL_CONTACT'] = 'Contact';

$app_strings['LBL_CONTACTS'] = 'Contacts';

$app_strings['LBL_CONTRACT'] = 'Contract';

$app_strings['LBL_CONTRACTS'] = 'Contracts';

$app_strings['LBL_COUNTRY'] = 'Country:';

$app_strings['LBL_CREATE_BUTTON_LABEL'] = 'CREATE';

$app_strings['LBL_CREATED_BY_USER'] = 'Created by User';

$app_strings['LBL_CREATED_USER'] = 'Created by User';

$app_strings['LBL_CREATED'] = 'Created by';

$app_strings['LBL_CURRENT_USER_FILTER'] = 'My Items:';

$app_strings['LBL_CURRENCY'] = 'Currency:';

$app_strings['LBL_DOCUMENTS'] = 'Documents';

$app_strings['LBL_DATE_ENTERED'] = 'Date Created:';

$app_strings['LBL_DATE_MODIFIED'] = 'Date Modified:';

$app_strings['LBL_EDIT_BUTTON'] = 'Edit';

$app_strings['LBL_DUPLICATE_BUTTON'] = 'Duplicate';

$app_strings['LBL_DELETE_BUTTON'] = 'Delete';

$app_strings['LBL_DELETE'] = 'Delete';

$app_strings['LBL_DELETED'] = 'Deleted';

$app_strings['LBL_DIRECT_REPORTS'] = 'Direct Reports';

$app_strings['LBL_DONE_BUTTON_LABEL'] = 'Done';

$app_strings['LBL_DONE_BUTTON_TITLE'] = 'Done';

$app_strings['LBL_FAVORITES'] = 'Favorites';

$app_strings['LBL_VCARD'] = 'vCard';

$app_strings['LBL_EMPTY_VCARD'] = 'Please select a vCard file';

$app_strings['LBL_EMPTY_REQUIRED_VCARD'] = 'vCard does not have all the required fields for this module. Please refer to suitecrm.log for details.';

$app_strings['LBL_VCARD_ERROR_FILESIZE'] = 'The uploaded file exceeds the 30000 bytes size limit which was specified in the HTML form.';

$app_strings['LBL_VCARD_ERROR_DEFAULT'] = 'There was an error uploading the vCard file. Please refer to suitecrm.log for details.';

$app_strings['LBL_IMPORT_VCARD'] = 'Import vCard:';

$app_strings['LBL_IMPORT_VCARD_BUTTON_LABEL'] = 'Import vCard';

$app_strings['LBL_IMPORT_VCARD_BUTTON_TITLE'] = 'Import vCard';

$app_strings['LBL_VIEW_BUTTON'] = 'View';

$app_strings['LBL_EMAIL_PDF_BUTTON_LABEL'] = 'Email as PDF';

$app_strings['LBL_EMAIL_PDF_BUTTON_TITLE'] = 'Email as PDF';

$app_strings['LBL_EMAILS'] = 'Emails';

$app_strings['LBL_EMPLOYEES'] = 'Employees';

$app_strings['LBL_ENTER_DATE'] = 'Enter Date';

$app_strings['LBL_EXPORT'] = 'Export';

$app_strings['LBL_FAVORITES_FILTER'] = 'My Favorites:';

$app_strings['LBL_GO_BUTTON_LABEL'] = 'Go';

$app_strings['LBL_HIDE'] = 'Hide';

$app_strings['LBL_ID'] = 'ID';

$app_strings['LBL_IMPORT'] = 'Import';

$app_strings['LBL_IMPORT_STARTED'] = 'Import Started: ';

$app_strings['LBL_LAST_VIEWED'] = 'Recently Viewed';

$app_strings['LBL_LEADS'] = 'Leads';

$app_strings['LBL_LESS'] = 'less';

$app_strings['LBL_CAMPAIGN'] = 'Campaign:';

$app_strings['LBL_CAMPAIGNS'] = 'Campaigns';

$app_strings['LBL_CAMPAIGNLOG'] = 'CampaignLog';

$app_strings['LBL_CAMPAIGN_CONTACT'] = 'Campaigns';

$app_strings['LBL_CAMPAIGN_ID'] = 'campaign_id';

$app_strings['LBL_CAMPAIGN_NONE'] = 'None';

$app_strings['LBL_THEME'] = 'Theme:';

$app_strings['LBL_FOUND_IN_RELEASE'] = 'Found In Release';

$app_strings['LBL_FIXED_IN_RELEASE'] = 'Fixed In Release';

$app_strings['LBL_LIST_ACCOUNT_NAME'] = 'Account Name';

$app_strings['LBL_LIST_ASSIGNED_USER'] = 'User';

$app_strings['LBL_LIST_CONTACT_NAME'] = 'Contact Name';

$app_strings['LBL_LIST_CONTACT_ROLE'] = 'Contact Role';

$app_strings['LBL_LIST_DATE_ENTERED'] = 'Date Created';

$app_strings['LBL_LIST_EMAIL'] = 'Email';

$app_strings['LBL_LIST_NAME'] = 'Name';

$app_strings['LBL_LIST_OF'] = 'of';

$app_strings['LBL_LIST_PHONE'] = 'Phone';

$app_strings['LBL_LIST_RELATED_TO'] = 'Related to';

$app_strings['LBL_LIST_USER_NAME'] = 'User Name';

$app_strings['LBL_LISTVIEW_NO_SELECTED'] = 'Please select at least 1 record to proceed.';

$app_strings['LBL_LISTVIEW_TWO_REQUIRED'] = 'Please select at least 2 records to proceed.';

$app_strings['LBL_LISTVIEW_OPTION_SELECTED'] = 'Selected Records';

$app_strings['LBL_LISTVIEW_SELECTED_OBJECTS'] = 'Selected: ';

$app_strings['LBL_LOCALE_NAME_EXAMPLE_FIRST'] = 'David';

$app_strings['LBL_LOCALE_NAME_EXAMPLE_LAST'] = 'Livingstone';

$app_strings['LBL_LOCALE_NAME_EXAMPLE_SALUTATION'] = 'Dr.';

$app_strings['LBL_LOCALE_NAME_EXAMPLE_TITLE'] = 'Code Monkey Extraordinaire';

$app_strings['LBL_CANCEL'] = 'Cancel';

$app_strings['LBL_VERIFY'] = 'Verify';

$app_strings['LBL_RESEND'] = 'Resend';

$app_strings['LBL_PROFILE'] = 'Profile';

$app_strings['LBL_MAILMERGE'] = 'Mail Merge';

$app_strings['LBL_MASS_UPDATE'] = 'Mass Update';

$app_strings['LBL_NO_MASS_UPDATE_FIELDS_AVAILABLE'] = 'There are no fields available for the Mass Update operation';

$app_strings['LBL_OPT_OUT_FLAG_PRIMARY'] = 'Opt out Primary Email';

$app_strings['LBL_OPT_IN_FLAG_PRIMARY'] = 'Opt in Primary Email';

$app_strings['LBL_MEETINGS'] = 'Meetings';

$app_strings['LBL_MEETING_GO_BACK'] = 'Go back to the meeting';

$app_strings['LBL_MEMBERS'] = 'Members';

$app_strings['LBL_MEMBER_OF'] = 'Member Of';

$app_strings['LBL_MODIFIED_BY_USER'] = 'Modified by User';

$app_strings['LBL_MODIFIED_USER'] = 'Modified by User';

$app_strings['LBL_MODIFIED'] = 'Modified by';

$app_strings['LBL_MODIFIED_NAME'] = 'Modified By Name';

$app_strings['LBL_MORE'] = 'More';

$app_strings['LBL_MY_ACCOUNT'] = 'My Settings';

$app_strings['LBL_NAME'] = 'Name';

$app_strings['LBL_NEW_BUTTON_KEY'] = 'N';

$app_strings['LBL_NEW_BUTTON_LABEL'] = 'Create';

$app_strings['LBL_NEW_BUTTON_TITLE'] = 'Create';

$app_strings['LBL_NEXT_BUTTON_LABEL'] = 'Next';

$app_strings['LBL_NONE'] = '--None--';

$app_strings['LBL_NOTES'] = 'Notes';

$app_strings['LBL_OPPORTUNITIES'] = 'Opportunities';

$app_strings['LBL_OPPORTUNITY_NAME'] = 'Opportunity Name';

$app_strings['LBL_OPPORTUNITY'] = 'Opportunity';

$app_strings['LBL_OR'] = 'OR';

$app_strings['LBL_PANEL_OVERVIEW'] = 'OVERVIEW';

$app_strings['LBL_PANEL_ASSIGNMENT'] = 'OTHER';

$app_strings['LBL_PANEL_ADVANCED'] = 'MORE INFORMATION';

$app_strings['LBL_PARENT_TYPE'] = 'Parent Type';

$app_strings['LBL_PERCENTAGE_SYMBOL'] = '%';

$app_strings['LBL_POSTAL_CODE'] = 'Postal Code:';

$app_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Primary Address City:';

$app_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'Primary Address Country:';

$app_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Primary Address Postal Code:';

$app_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Primary Address State:';

$app_strings['LBL_PRIMARY_ADDRESS_STREET_2'] = 'Primary Address Street 2:';

$app_strings['LBL_PRIMARY_ADDRESS_STREET_3'] = 'Primary Address Street 3:';

$app_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Primary Address Street:';

$app_strings['LBL_PRIMARY_ADDRESS'] = 'Primary Address:';

$app_strings['LBL_PROSPECTS'] = 'Prospects';

$app_strings['LBL_PRODUCTS'] = 'Products';

$app_strings['LBL_PROJECT_TASKS'] = 'Project Tasks';

$app_strings['LBL_PROJECTS'] = 'Projects';

$app_strings['LBL_QUOTES'] = 'Quotes';

$app_strings['LBL_RELATED'] = 'Related';

$app_strings['LBL_RELATED_RECORDS'] = 'Related Records';

$app_strings['LBL_REMOVE'] = 'Remove';

$app_strings['LBL_REPORTS_TO'] = 'Reports To';

$app_strings['LBL_REQUIRED_SYMBOL'] = '*';

$app_strings['LBL_REQUIRED_TITLE'] = 'Indicates required field';

$app_strings['LBL_EMAIL_DONE_BUTTON_LABEL'] = 'Done';

$app_strings['LBL_FULL_FORM_BUTTON_KEY'] = 'L';

$app_strings['LBL_FULL_FORM_BUTTON_LABEL'] = 'Full Form';

$app_strings['LBL_FULL_FORM_BUTTON_TITLE'] = 'Full Form';

$app_strings['LBL_SAVE_NEW_BUTTON_LABEL'] = 'Save & Create New';

$app_strings['LBL_SAVE_NEW_BUTTON_TITLE'] = 'Save & Create New';

$app_strings['LBL_SAVE_OBJECT'] = 'Save {0}';

$app_strings['LBL_SEARCH_BUTTON_KEY'] = 'Q';

$app_strings['LBL_SEARCH_BUTTON_LABEL'] = 'Search';

$app_strings['LBL_SEARCH_BUTTON_TITLE'] = 'Search';

$app_strings['LBL_FILTER'] = 'Filter';

$app_strings['LBL_SEARCH'] = 'Search';

$app_strings['LBL_SEARCH_ALT'] = '';

$app_strings['LBL_SEARCH_MORE'] = 'more';

$app_strings['LBL_UPLOAD_IMAGE_FILE_INVALID'] = 'Invalid file format, only image file can be uploaded.';

$app_strings['LBL_SELECT_BUTTON_KEY'] = 'T';

$app_strings['LBL_SELECT_BUTTON_LABEL'] = 'Select';

$app_strings['LBL_SELECT_BUTTON_TITLE'] = 'Select';

$app_strings['LBL_BROWSE_DOCUMENTS_BUTTON_LABEL'] = 'Browse Documents';

$app_strings['LBL_BROWSE_DOCUMENTS_BUTTON_TITLE'] = 'Browse Documents';

$app_strings['LBL_SELECT_CONTACT_BUTTON_KEY'] = 'T';

$app_strings['LBL_SELECT_CONTACT_BUTTON_LABEL'] = 'Select Contact';

$app_strings['LBL_SELECT_CONTACT_BUTTON_TITLE'] = 'Select Contact';

$app_strings['LBL_SELECT_REPORTS_BUTTON_LABEL'] = 'SELECT FROM Reports';

$app_strings['LBL_SELECT_REPORTS_BUTTON_TITLE'] = 'Select Reports';

$app_strings['LBL_SELECT_USER_BUTTON_KEY'] = 'U';

$app_strings['LBL_SELECT_USER_BUTTON_LABEL'] = 'Select User';

$app_strings['LBL_SELECT_USER_BUTTON_TITLE'] = 'Select User';

$app_strings['LBL_ACCESSKEY_CLEAR_RELATE_KEY'] = ' ';

$app_strings['LBL_ACCESSKEY_CLEAR_RELATE_TITLE'] = 'Clear Selection';

$app_strings['LBL_ACCESSKEY_CLEAR_RELATE_LABEL'] = 'Clear Selection';

$app_strings['LBL_ACCESSKEY_CLEAR_COLLECTION_KEY'] = ' ';

$app_strings['LBL_ACCESSKEY_CLEAR_COLLECTION_TITLE'] = 'Clear Selection';

$app_strings['LBL_ACCESSKEY_CLEAR_COLLECTION_LABEL'] = 'Clear Selection';

$app_strings['LBL_ACCESSKEY_SELECT_FILE_KEY'] = 'F';

$app_strings['LBL_ACCESSKEY_SELECT_FILE_TITLE'] = 'Select File';

$app_strings['LBL_ACCESSKEY_SELECT_FILE_LABEL'] = 'Select File';

$app_strings['LBL_ACCESSKEY_CLEAR_FILE_KEY'] = ' ';

$app_strings['LBL_ACCESSKEY_CLEAR_FILE_TITLE'] = 'Clear File';

$app_strings['LBL_ACCESSKEY_CLEAR_FILE_LABEL'] = 'Clear File';

$app_strings['LBL_ACCESSKEY_SELECT_USERS_KEY'] = 'U';

$app_strings['LBL_ACCESSKEY_SELECT_USERS_TITLE'] = 'Select User';

$app_strings['LBL_ACCESSKEY_SELECT_USERS_LABEL'] = 'Select User';

$app_strings['LBL_ACCESSKEY_CLEAR_USERS_KEY'] = ' ';

$app_strings['LBL_ACCESSKEY_CLEAR_USERS_TITLE'] = 'Clear User';

$app_strings['LBL_ACCESSKEY_CLEAR_USERS_LABEL'] = 'Clear User';

$app_strings['LBL_ACCESSKEY_SELECT_ACCOUNTS_KEY'] = 'A';

$app_strings['LBL_ACCESSKEY_SELECT_ACCOUNTS_TITLE'] = 'Select Account';

$app_strings['LBL_ACCESSKEY_SELECT_ACCOUNTS_LABEL'] = 'Select Account';

$app_strings['LBL_ACCESSKEY_CLEAR_ACCOUNTS_KEY'] = ' ';

$app_strings['LBL_ACCESSKEY_CLEAR_ACCOUNTS_TITLE'] = 'Clear Account';

$app_strings['LBL_ACCESSKEY_CLEAR_ACCOUNTS_LABEL'] = 'Clear Account';

$app_strings['LBL_ACCESSKEY_SELECT_CAMPAIGNS_KEY'] = 'M';

$app_strings['LBL_ACCESSKEY_SELECT_CAMPAIGNS_TITLE'] = 'Select Campaign';

$app_strings['LBL_ACCESSKEY_SELECT_CAMPAIGNS_LABEL'] = 'Select Campaign';

$app_strings['LBL_ACCESSKEY_CLEAR_CAMPAIGNS_KEY'] = ' ';

$app_strings['LBL_ACCESSKEY_CLEAR_CAMPAIGNS_TITLE'] = 'Clear Campaign';

$app_strings['LBL_ACCESSKEY_CLEAR_CAMPAIGNS_LABEL'] = 'Clear Campaign';

$app_strings['LBL_ACCESSKEY_SELECT_CONTACTS_KEY'] = 'C';

$app_strings['LBL_ACCESSKEY_SELECT_CONTACTS_TITLE'] = 'Select Contact';

$app_strings['LBL_ACCESSKEY_SELECT_CONTACTS_LABEL'] = 'Select Contact';

$app_strings['LBL_ACCESSKEY_CLEAR_CONTACTS_KEY'] = ' ';

$app_strings['LBL_ACCESSKEY_CLEAR_CONTACTS_TITLE'] = 'Clear Contact';

$app_strings['LBL_ACCESSKEY_CLEAR_CONTACTS_LABEL'] = 'Clear Contact';

$app_strings['LBL_ACCESSKEY_SELECT_TEAMSET_KEY'] = 'Z';

$app_strings['LBL_ACCESSKEY_SELECT_TEAMSET_TITLE'] = 'Select Team';

$app_strings['LBL_ACCESSKEY_SELECT_TEAMSET_LABEL'] = 'Select Team';

$app_strings['LBL_ACCESSKEY_CLEAR_TEAMS_KEY'] = ' ';

$app_strings['LBL_ACCESSKEY_CLEAR_TEAMS_TITLE'] = 'Clear Team';

$app_strings['LBL_ACCESSKEY_CLEAR_TEAMS_LABEL'] = 'Clear Team';

$app_strings['LBL_SERVER_RESPONSE_RESOURCES'] = 'Resources used to construct this page (queries, files)';

$app_strings['LBL_SERVER_RESPONSE_TIME_SECONDS'] = 'seconds.';

$app_strings['LBL_SERVER_RESPONSE_TIME'] = 'Server response time:';

$app_strings['LBL_SERVER_MEMORY_BYTES'] = 'bytes';

$app_strings['LBL_SERVER_MEMORY_USAGE'] = 'Server Memory Usage: {0} ({1})';

$app_strings['LBL_SERVER_MEMORY_LOG_MESSAGE'] = 'Usage: - module: {0} - action: {1}';

$app_strings['LBL_SERVER_PEAK_MEMORY_USAGE'] = 'Server Peak Memory Usage: {0} ({1})';

$app_strings['LBL_SHIPPING_ADDRESS'] = 'Shipping Address';

$app_strings['LBL_SHOW'] = 'Show';

$app_strings['LBL_STATE'] = 'State:';

$app_strings['LBL_STATUS_UPDATED'] = 'Your Status for this event has been updated!';

$app_strings['LBL_STATUS'] = 'Status:';

$app_strings['LBL_STREET'] = 'Street';

$app_strings['LBL_SUBJECT'] = 'Subject';

$app_strings['LBL_INBOUNDEMAIL_ID'] = 'Inbound Email ID';

$app_strings['LBL_SCENARIO_SALES'] = 'Sales';

$app_strings['LBL_SCENARIO_MARKETING'] = 'Marketing';

$app_strings['LBL_SCENARIO_FINANCE'] = 'Finance';

$app_strings['LBL_SCENARIO_SERVICE'] = 'Service';

$app_strings['LBL_SCENARIO_PROJECT'] = 'Project Management';

$app_strings['LBL_SCENARIO_SALES_DESCRIPTION'] = 'This scenario facilitates the management of sales items';

$app_strings['LBL_SCENARIO_MAKETING_DESCRIPTION'] = 'This scenario facilitates the management of marketing items';

$app_strings['LBL_SCENARIO_FINANCE_DESCRIPTION'] = 'This scenario facilitates the management of finance related items';

$app_strings['LBL_SCENARIO_SERVICE_DESCRIPTION'] = 'This scenario facilitates the management of service related items';

$app_strings['LBL_SCENARIO_PROJECT_DESCRIPTION'] = 'This scenario facilitates the management of project related items';

$app_strings['LBL_SYNC'] = 'Sync';

$app_strings['LBL_TABGROUP_ALL'] = 'All';

$app_strings['LBL_TABGROUP_ACTIVITIES'] = 'Activities';

$app_strings['LBL_TABGROUP_COLLABORATION'] = 'Collaboration';

$app_strings['LBL_TABGROUP_MARKETING'] = 'Marketing';

$app_strings['LBL_TABGROUP_OTHER'] = 'Other';

$app_strings['LBL_TABGROUP_SALES'] = 'Sales';

$app_strings['LBL_TABGROUP_SUPPORT'] = 'Support';

$app_strings['LBL_TASKS'] = 'Tasks';

$app_strings['LBL_THOUSANDS_SYMBOL'] = 'K';

$app_strings['LBL_TRACK_EMAIL_BUTTON_LABEL'] = 'Archive Email';

$app_strings['LBL_TRACK_EMAIL_BUTTON_TITLE'] = 'Archive Email';

$app_strings['LBL_UNDELETE_BUTTON_LABEL'] = 'Undelete';

$app_strings['LBL_UNDELETE_BUTTON_TITLE'] = 'Undelete';

$app_strings['LBL_UNDELETE_BUTTON'] = 'Undelete';

$app_strings['LBL_UNDELETE'] = 'Undelete';

$app_strings['LBL_UNSYNC'] = 'Unsync';

$app_strings['LBL_UPDATE'] = 'Update';

$app_strings['LBL_USER_LIST'] = 'User List';

$app_strings['LBL_USERS'] = 'Users';

$app_strings['LBL_VERIFY_EMAIL_ADDRESS'] = 'Checking for existing email entry...';

$app_strings['LBL_VERIFY_PORTAL_NAME'] = 'Checking for existing portal name...';

$app_strings['LBL_VIEW_IMAGE'] = 'view';

$app_strings['LNK_ABOUT'] = 'About';

$app_strings['LNK_ADVANCED_FILTER'] = 'Advanced Filter';

$app_strings['LNK_BASIC_FILTER'] = 'Quick Filter';

$app_strings['LBL_ADVANCED_SEARCH'] = 'Advanced Filter';

$app_strings['LBL_QUICK_FILTER'] = 'Quick Filter';

$app_strings['LNK_SEARCH_NONFTS_VIEW_ALL'] = 'Show All';

$app_strings['LNK_CLOSE'] = 'Close';

$app_strings['LBL_MODIFY_CURRENT_FILTER'] = 'Modify current filter';

$app_strings['LNK_SAVED_VIEWS'] = 'Layout Options';

$app_strings['LNK_DELETE'] = 'Delete';

$app_strings['LNK_EDIT'] = 'Edit';

$app_strings['LNK_GET_LATEST'] = 'Get latest';

$app_strings['LNK_GET_LATEST_TOOLTIP'] = 'Replace with latest version';

$app_strings['LNK_HELP'] = 'Help';

$app_strings['LNK_CREATE'] = 'Create';

$app_strings['LNK_LIST_END'] = 'End';

$app_strings['LNK_LIST_NEXT'] = 'Next';

$app_strings['LNK_LIST_PREVIOUS'] = 'Previous';

$app_strings['LNK_LIST_RETURN'] = 'Return to List';

$app_strings['LNK_LIST_START'] = 'Start';

$app_strings['LNK_LOAD_SIGNED'] = 'Sign';

$app_strings['LNK_LOAD_SIGNED_TOOLTIP'] = 'Replace with signed document';

$app_strings['LNK_PRINT'] = 'Print';

$app_strings['LNK_BACKTOTOP'] = 'Back to top';

$app_strings['LNK_REMOVE'] = 'Remove';

$app_strings['LNK_RESUME'] = 'Resume';

$app_strings['LNK_VIEW_CHANGE_LOG'] = 'View Change Log';

$app_strings['NTC_CLICK_BACK'] = 'Please click the browser back button and fix the error.';

$app_strings['NTC_DATE_FORMAT'] = '(yyyy-mm-dd)';

$app_strings['NTC_DELETE_CONFIRMATION_MULTIPLE'] = 'Are you sure you want to delete selected record(s)?';

$app_strings['NTC_TEMPLATE_IS_USED'] = 'The template is used in at least one email marketing record. Are you sure you want to delete it?';

$app_strings['NTC_TEMPLATES_IS_USED'] = 'The following templates are used in email marketing records. Are you sure you want to delete them?











































































































































































































';

$app_strings['NTC_DELETE_CONFIRMATION'] = 'Are you sure you want to delete this record?';

$app_strings['NTC_DELETE_CONFIRMATION_NUM'] = 'Are you sure you want to delete the ';

$app_strings['NTC_UPDATE_CONFIRMATION_NUM'] = 'Are you sure you want to update the ';

$app_strings['NTC_DELETE_SELECTED_RECORDS'] = ' selected record(s)?';

$app_strings['NTC_LOGIN_MESSAGE'] = 'Please enter your user name and password.';

$app_strings['NTC_NO_ITEMS_DISPLAY'] = 'none';

$app_strings['NTC_REMOVE_CONFIRMATION'] = 'Are you sure you want to remove this relationship? Only the relationship will be removed. The record will not be deleted.';

$app_strings['NTC_REQUIRED'] = 'Indicates required field';

$app_strings['NTC_TIME_FORMAT'] = '(24:00)';

$app_strings['NTC_WELCOME'] = 'Welcome';

$app_strings['NTC_YEAR_FORMAT'] = '(yyyy)';

$app_strings['WARN_UNSAVED_CHANGES'] = 'You are about to leave this record without saving any changes you may have made to the record. Are you sure you want to navigate away from this record?';

$app_strings['ERROR_NO_RECORD'] = 'Error retrieving record. This record may be deleted or you may not be authorized to view it.';

$app_strings['WARN_BROWSER_VERSION_WARNING'] = '<b>Warning:</b> Your browser version is no longer supported or you are using an unsupported browser.<p></p>The following browser versions are recommended:<p></p><ul><li>Internet Explorer 10 (compatibility view not supported)<li>Firefox 32.0<li>Safari 5.1<li>Chrome 37</ul>';

$app_strings['WARN_BROWSER_IE_COMPATIBILITY_MODE_WARNING'] = '<b>Warning:</b> Your browser is in IE compatibility view which is not supported.';

$app_strings['ERROR_TYPE_NOT_VALID'] = 'Error. This type is not valid.';

$app_strings['ERROR_NO_BEAN'] = 'Failed to get bean.';

$app_strings['LBL_DUP_MERGE'] = 'Find Duplicates';

$app_strings['LBL_MANAGE_SUBSCRIPTIONS'] = 'Manage Subscriptions';

$app_strings['LBL_MANAGE_SUBSCRIPTIONS_FOR'] = 'Manage Subscriptions for ';

$app_strings['LBL_LOADING'] = 'Loading...';

$app_strings['LBL_SEARCHING'] = 'Searching...';

$app_strings['LBL_SAVING_LAYOUT'] = 'Saving Layout...';

$app_strings['LBL_SAVED_LAYOUT'] = 'Layout has been saved.';

$app_strings['LBL_SAVED'] = 'Saved';

$app_strings['LBL_SAVING'] = 'Saving';

$app_strings['LBL_DISPLAY_COLUMNS'] = 'Display Columns';

$app_strings['LBL_HIDE_COLUMNS'] = 'Hide Columns';

$app_strings['LBL_SEARCH_CRITERIA'] = 'Search Criteria';

$app_strings['LBL_SAVED_VIEWS'] = 'Saved Views';

$app_strings['LBL_PROCESSING_REQUEST'] = 'Processing...';

$app_strings['LBL_REQUEST_PROCESSED'] = 'Done';

$app_strings['LBL_AJAX_FAILURE'] = 'Ajax failure';

$app_strings['LBL_MERGE_DUPLICATES'] = 'Merge';

$app_strings['LBL_SAVED_FILTER_SHORTCUT'] = 'My Filters';

$app_strings['LBL_SEARCH_POPULATE_ONLY'] = 'Perform a search using the search form above';

$app_strings['LBL_DETAILVIEW'] = 'Detail View';

$app_strings['LBL_LISTVIEW'] = 'List View';

$app_strings['LBL_EDITVIEW'] = 'Edit View';

$app_strings['LBL_BILLING_STREET'] = 'Street:';

$app_strings['LBL_SHIPPING_STREET'] = 'Street:';

$app_strings['LBL_SEARCHFORM'] = 'Search Form';

$app_strings['LBL_SAVED_SEARCH_ERROR'] = 'Please provide a name for this view.';

$app_strings['LBL_DISPLAY_LOG'] = 'Display Log';

$app_strings['ERROR_JS_ALERT_SYSTEM_CLASS'] = 'System';

$app_strings['ERROR_JS_ALERT_TIMEOUT_TITLE'] = 'Session Timeout';

$app_strings['ERROR_JS_ALERT_TIMEOUT_MSG_1'] = 'Your session is about to timeout in 2 minutes. Please save your work.';

$app_strings['ERROR_JS_ALERT_TIMEOUT_MSG_2'] = 'Your session has timed out.';

$app_strings['MSG_JS_ALERT_MTG_REMINDER_AGENDA'] = '











































































































































































































Agenda: ';

$app_strings['MSG_JS_ALERT_MTG_REMINDER_MEETING'] = 'Meeting';

$app_strings['MSG_JS_ALERT_MTG_REMINDER_CALL'] = 'Call';

$app_strings['MSG_JS_ALERT_MTG_REMINDER_TIME'] = 'Time: ';

$app_strings['MSG_JS_ALERT_MTG_REMINDER_LOC'] = 'Location: ';

$app_strings['MSG_JS_ALERT_MTG_REMINDER_DESC'] = 'Description: ';

$app_strings['MSG_JS_ALERT_MTG_REMINDER_STATUS'] = 'Status: ';

$app_strings['MSG_JS_ALERT_MTG_REMINDER_RELATED_TO'] = 'Related To: ';

$app_strings['MSG_JS_ALERT_MTG_REMINDER_CALL_MSG'] = '











































































































































































































Click OK to view this call or click Cancel to dismiss this message.';

$app_strings['MSG_JS_ALERT_MTG_REMINDER_MEETING_MSG'] = '











































































































































































































Click OK to view this meeting or click Cancel to dismiss this message.';

$app_strings['MSG_JS_ALERT_MTG_REMINDER_NO_EVENT_NAME'] = 'Event';

$app_strings['MSG_JS_ALERT_MTG_REMINDER_NO_DESCRIPTION'] = 'Event isn\'t set.';

$app_strings['MSG_JS_ALERT_MTG_REMINDER_NO_LOCATION'] = 'Location isn\'t set.';

$app_strings['MSG_JS_ALERT_MTG_REMINDER_NO_START_DATE'] = 'Start date isn\'t defined.';

$app_strings['MSG_LIST_VIEW_NO_RESULTS_BASIC'] = 'No results found.';

$app_strings['MSG_LIST_VIEW_NO_RESULTS_CHANGE_CRITERIA'] = 'No results found... Perhaps change your search criteria and try again?';

$app_strings['MSG_LIST_VIEW_NO_RESULTS'] = 'No results found for <item1>';

$app_strings['MSG_LIST_VIEW_NO_RESULTS_SUBMSG'] = 'Create <item1> as a new <item2>';

$app_strings['MSG_LIST_VIEW_CHANGE_SEARCH'] = 'or change your search criteria';

$app_strings['MSG_EMPTY_LIST_VIEW_NO_RESULTS'] = 'You currently have no records saved. <item2> or <item3> one now.';

$app_strings['LBL_ADD_TO_FAVORITES'] = 'Add to My Favorites';

$app_strings['LBL_CREATE_CONTACT'] = 'Create Contact';

$app_strings['LBL_CREATE_CASE'] = 'Create Case';

$app_strings['LBL_CREATE_NOTE'] = 'Create Note';

$app_strings['LBL_CREATE_OPPORTUNITY'] = 'Create Opportunity';

$app_strings['LBL_SCHEDULE_CALL'] = 'Log Call';

$app_strings['LBL_SCHEDULE_MEETING'] = 'Schedule Meeting';

$app_strings['LBL_CREATE_TASK'] = 'Create Task';

$app_strings['LBL_GENERATE_WEB_TO_LEAD_FORM'] = 'Generate Form';

$app_strings['LBL_SAVE_WEB_TO_LEAD_FORM'] = 'Save Web Form';

$app_strings['LBL_AVAILABLE_FIELDS'] = 'Available Fields';

$app_strings['LBL_FIRST_FORM_COLUMN'] = 'First Form Column';

$app_strings['LBL_SECOND_FORM_COLUMN'] = 'Second Form Column';

$app_strings['LBL_ASSIGNED_TO_REQUIRED'] = 'Missing required field: Assigned to';

$app_strings['LBL_RELATED_CAMPAIGN_REQUIRED'] = 'Missing required field: Related campaign';

$app_strings['LBL_TYPE_OF_PERSON_FOR_FORM'] = 'Web form to create ';

$app_strings['LBL_TYPE_OF_PERSON_FOR_FORM_DESC'] = 'Submitting this form will create ';

$app_strings['LBL_ADD_ALL_LEAD_FIELDS'] = 'Add All Fields';

$app_strings['LBL_RESET_ALL_LEAD_FIELDS'] = 'Reset all Fields';

$app_strings['LBL_REMOVE_ALL_LEAD_FIELDS'] = 'Remove All Fields';

$app_strings['LBL_NEXT_BTN'] = 'Next';

$app_strings['LBL_ONLY_IMAGE_ATTACHMENT'] = 'Only the following supported image type attachments can be embedded: JPG, PNG.';

$app_strings['LBL_TRAINING'] = 'Support Forum';

$app_strings['ERR_MSSQL_DB_CONTEXT'] = 'Changed database context to';

$app_strings['ERR_MSSQL_WARNING'] = 'Warning:';

$app_strings['ERR_CANNOT_CREATE_METADATA_FILE'] = 'Error: File [[file]] is missing. Unable to create because no corresponding HTML file was found.';

$app_strings['ERR_CANNOT_FIND_MODULE'] = 'Error: Module [module] does not exist.';

$app_strings['LBL_ALT_ADDRESS'] = 'Other Address:';

$app_strings['ERR_SMARTY_UNEQUAL_RELATED_FIELD_PARAMETERS'] = 'Error: There are an unequal number of arguments for the \'key\' and \'copy\' elements in the displayParams array.';

$app_strings['LBL_DASHLET_CONFIGURE_GENERAL'] = 'General';

$app_strings['LBL_DASHLET_CONFIGURE_FILTERS'] = 'Filters';

$app_strings['LBL_DASHLET_CONFIGURE_MY_ITEMS_ONLY'] = 'Only My Items';

$app_strings['LBL_DASHLET_CONFIGURE_TITLE'] = 'Title';

$app_strings['LBL_DASHLET_CONFIGURE_DISPLAY_ROWS'] = 'Display Rows';

$app_strings['LBL_MAX_DASHLETS_REACHED'] = 'You have reached the maximum number of SuiteCRM Dashlets your adminstrator has set. Please remove a SuiteCRM Dashlet to add more.';

$app_strings['LBL_ADDING_DASHLET'] = 'Adding SuiteCRM Dashlet...';

$app_strings['LBL_ADDED_DASHLET'] = 'SuiteCRM Dashlet Added';

$app_strings['LBL_REMOVE_DASHLET_CONFIRM'] = 'Are you sure you want to remove this SuiteCRM Dashlet?';

$app_strings['LBL_REMOVING_DASHLET'] = 'Removing SuiteCRM Dashlet...';

$app_strings['LBL_REMOVED_DASHLET'] = 'SuiteCRM Dashlet Removed';

$app_strings['LBL_LOADING_PAGE'] = 'Loading page, please wait...';

$app_strings['LBL_RELOAD_PAGE'] = 'Please <a href=';

$app_strings['LBL_ADD_DASHLETS'] = 'Add Dashlets';

$app_strings['LBL_CLOSE_DASHLETS'] = 'Close';

$app_strings['LBL_OPTIONS'] = 'Options';

$app_strings['LBL_1_COLUMN'] = '1 Column';

$app_strings['LBL_2_COLUMN'] = '2 Column';

$app_strings['LBL_3_COLUMN'] = '3 Column';

$app_strings['LBL_PAGE_NAME'] = 'Page Name';

$app_strings['LBL_SEARCH_RESULTS'] = 'Search Results';

$app_strings['LBL_SEARCH_MODULES'] = 'Modules';

$app_strings['LBL_SEARCH_TOOLS'] = 'Tools';

$app_strings['LBL_SEARCH_HELP_TITLE'] = 'Search Tips';

$app_strings['LBL_NO_IMAGE'] = 'No Image';

$app_strings['LBL_MODULE'] = 'Module';

$app_strings['LBL_COPY_ADDRESS_FROM_LEFT'] = 'Copy address from left:';

$app_strings['LBL_SAVE_AND_CONTINUE'] = 'Save and Continue';

$app_strings['LBL_SEARCH_HELP_TEXT'] = '<p><br /><strong>Multiselect controls</strong></p><ul><li>Click on the values to select an attribute.</li><li>Ctrl-click to select multiple. Mac users use CMD-click.</li><li>To select all values between two attributes,  click first value and then shift-click last value.</li></ul><p><strong>Advanced Search & Layout Options</strong><br><br>Using the <b>Saved Search & Layout</b> option, you can save a set of search parameters and/or a custom List View layout in order to quickly obtain the desired search results in the future. You can save an unlimited number of custom searches and layouts. All saved searches appear by name in the Saved Searches list, with the last loaded saved search appearing at the top of the list.<br><br>To customize the List View layout, use the Hide Columns and Display Columns boxes to select which fields to display in the search results. For example, you can view or hide details such as the record name, and assigned user, and assigned team in the search results. To add a column to List View, select the field from the Hide Columns list and use the left arrow to move it to the Display Columns list. To remove a column from List View, select it from the Display Columns list and use the right arrow to move it to the Hide Columns list.<br><br>If you save layout settings, you will be able to load them at any time to view the search results in the custom layout.<br><br>To save and update a search and/or layout:<ol><li>Enter a name for the search results in the <b>Save this search as</b> field and click <b>Save</b>.The name now displays in the Saved Searches list adjacent to the <b>Clear</b> button.</li><li>To view a saved search, select it from the Saved Searches list. The search results are displayed in the List View.</li><li>To update the properties of a saved search, select the saved search from the list, enter the new search criteria and/or layout options in the Advanced Search area, and click <b>Update</b> next to <b>Modify Current Search</b>.</li><li>To delete a saved search, select it in the Saved Searches list, click <b>Delete</b> next to <b>Modify Current Search</b>, and then click <b>OK</b> to confirm the deletion.</li></ol><p><strong>Tips</strong><br><br>By using the % as a wildcard operator you can make your search more broad. For example instead of just searching for results that equal ';

$app_strings['ERR_QUERY_LIMIT'] = 'Error: Query limit of $limit reached for $module module.';

$app_strings['ERROR_NOTIFY_OVERRIDE'] = 'Error: ResourceObserver->notify() needs to be overridden.';

$app_strings['ERR_MONITOR_FILE_MISSING'] = 'Error: Unable to create monitor because metadata file is empty or file does not exist.';

$app_strings['ERR_MONITOR_NOT_CONFIGURED'] = 'Error: There is no monitor configured for requested name';

$app_strings['ERR_UNDEFINED_METRIC'] = 'Error: Unable to set value for undefined metric';

$app_strings['ERR_STORE_FILE_MISSING'] = 'Error: Unable to find Store implementation file';

$app_strings['LBL_MONITOR_ID'] = 'Monitor Id';

$app_strings['LBL_USER_ID'] = 'User Id';

$app_strings['LBL_MODULE_NAME'] = 'Module Name';

$app_strings['LBL_ITEM_ID'] = 'Item Id';

$app_strings['LBL_ITEM_SUMMARY'] = 'Item Summary';

$app_strings['LBL_ACTION'] = 'Action';

$app_strings['LBL_SESSION_ID'] = 'Session Id';

$app_strings['LBL_BREADCRUMBSTACK_CREATED'] = 'BreadCrumbStack created for user id {0}';

$app_strings['LBL_VISIBLE'] = 'Record Visible';

$app_strings['LBL_DATE_LAST_ACTION'] = 'Date of Last Action';

$app_strings['MSG_IS_NOT_BEFORE'] = 'is not before';

$app_strings['MSG_IS_MORE_THAN'] = 'is more than';

$app_strings['MSG_SHOULD_BE'] = 'should be';

$app_strings['MSG_OR_GREATER'] = 'or greater';

$app_strings['LBL_LIST'] = 'List';

$app_strings['LBL_CREATE_BUG'] = 'Create Bug';

$app_strings['LBL_OBJECT_IMAGE'] = 'object image';

$app_strings['LBL_MASSUPDATE_DATE'] = 'Select Date';

$app_strings['LBL_VALIDATE_RANGE'] = 'is not within the valid range';

$app_strings['LBL_CHOOSE_START_AND_END_DATES'] = 'Please choose both a starting and ending date range';

$app_strings['LBL_CHOOSE_START_AND_END_ENTRIES'] = 'Please choose both starting and ending range entries';

$app_strings['LBL_DROPDOWN_LIST_ALL'] = 'All';

$app_strings['ERR_CONNECTOR_FILL_BEANS_SIZE_MISMATCH'] = 'Error: The Array count of the bean parameter does not match the Array count of the results.';

$app_strings['ERR_MISSING_MAPPING_ENTRY_FORM_MODULE'] = 'Error: Missing mapping entry for module.';

$app_strings['ERROR_UNABLE_TO_RETRIEVE_DATA'] = 'Error: Unable to retrieve data for {0} Connector. The service may currently be inaccessible or the configuration settings may be invalid. Connector error message: ({1}).';

$app_strings['LBL_FASTCGI_LOGGING'] = 'For optimal experience using IIS/FastCGI sapi, set fastcgi.logging to 0 in your php.ini file.';

$app_strings['LBL_COLLECTION_NAME'] = 'Name';

$app_strings['LBL_COLLECTION_PRIMARY'] = 'Primary';

$app_strings['ERROR_MISSING_COLLECTION_SELECTION'] = 'Empty required field';

$app_strings['LBL_ASSIGNED_TO_NAME'] = 'Assigned to';

$app_strings['LBL_DESCRIPTION'] = 'Description';

$app_strings['LBL_YESTERDAY'] = 'yesterday';

$app_strings['LBL_TODAY'] = 'today';

$app_strings['LBL_TOMORROW'] = 'tomorrow';

$app_strings['LBL_NEXT_WEEK'] = 'next week';

$app_strings['LBL_NEXT_MONDAY'] = 'next monday';

$app_strings['LBL_NEXT_FRIDAY'] = 'next friday';

$app_strings['LBL_TWO_WEEKS'] = 'two weeks';

$app_strings['LBL_NEXT_MONTH'] = 'next month';

$app_strings['LBL_FIRST_DAY_OF_NEXT_MONTH'] = 'first day of next month';

$app_strings['LBL_THREE_MONTHS'] = 'three months';

$app_strings['LBL_SIXMONTHS'] = 'six months';

$app_strings['LBL_NEXT_YEAR'] = 'next year';

$app_strings['LBL_HOURS'] = 'Hours';

$app_strings['LBL_MINUTES'] = 'Minutes';

$app_strings['LBL_MERIDIEM'] = 'Meridiem';

$app_strings['LBL_DATE'] = 'Date';

$app_strings['LBL_DASHLET_CONFIGURE_AUTOREFRESH'] = 'Auto-Refresh';

$app_strings['LBL_DURATION_DAY'] = 'day';

$app_strings['LBL_DURATION_HOUR'] = 'hour';

$app_strings['LBL_DURATION_MINUTE'] = 'minute';

$app_strings['LBL_DURATION_DAYS'] = 'days';

$app_strings['LBL_DURATION_HOURS'] = 'Duration Hours';

$app_strings['LBL_DURATION_MINUTES'] = 'Duration Minutes';

$app_strings['LBL_CHOOSE_MONTH'] = 'Choose Month';

$app_strings['LBL_ENTER_YEAR'] = 'Enter Year';

$app_strings['LBL_ENTER_VALID_YEAR'] = 'Please enter a valid year';

$app_strings['ERR_FILE_WRITE'] = 'Error: Could not write file {0}. Please check system and web server permissions.';

$app_strings['ERR_FILE_NOT_FOUND'] = 'Error: Could not load file {0}. Please check system and web server permissions.';

$app_strings['LBL_AND'] = 'And';

$app_strings['LBL_SEARCH_EXTERNAL_API'] = 'File on External Source';

$app_strings['LBL_EXTERNAL_SECURITY_LEVEL'] = 'Security';

$app_strings['LBL_IMPORT_SAMPLE_FILE_TEXT'] = '











































































































































































































';

$app_strings['LBL_NOTIFICATIONS_NONE'] = 'No Current Notifications';

$app_strings['LBL_ALT_SORT_DESC'] = 'Sorted Descending';

$app_strings['LBL_ALT_SORT_ASC'] = 'Sorted Ascending';

$app_strings['LBL_ALT_SORT'] = 'Sort';

$app_strings['LBL_ALT_SHOW_OPTIONS'] = 'Show Options';

$app_strings['LBL_ALT_HIDE_OPTIONS'] = 'Hide Options';

$app_strings['LBL_ALT_MOVE_COLUMN_LEFT'] = 'Move selected entry to the list on the left';

$app_strings['LBL_ALT_MOVE_COLUMN_RIGHT'] = 'Move selected entry to the list on the right';

$app_strings['LBL_ALT_MOVE_COLUMN_UP'] = 'Move selected entry up in the displayed list order';

$app_strings['LBL_ALT_MOVE_COLUMN_DOWN'] = 'Move selected entry down in the displayed list order';

$app_strings['LBL_ALT_INFO'] = 'Information';

$app_strings['MSG_DUPLICATE'] = 'The {0} record you are about to create might be a duplicate of an {0} record that already exists. {1} records containing similar names are listed below.<br>Click Create {1} to continue creating this new {0}, or select an existing {0} listed below.';

$app_strings['MSG_SHOW_DUPLICATES'] = 'The {0} record you are about to create might be a duplicate of a {0} record that already exists. {1} records containing similar names are listed below. Click Save to continue creating this new {0}, or click Cancel to return to the module without creating the {0}.';

$app_strings['LBL_EMAIL_TITLE'] = 'email address';

$app_strings['LBL_EMAIL_OPT_TITLE'] = 'opted out email address';

$app_strings['LBL_EMAIL_INV_TITLE'] = 'invalid email address';

$app_strings['LBL_EMAIL_PRIM_TITLE'] = 'Make Primary Email Address';

$app_strings['LBL_SELECT_ALL_TITLE'] = 'Select all';

$app_strings['LBL_SELECT_THIS_ROW_TITLE'] = 'Select this row';

$app_strings['UPLOAD_ERROR_TEXT'] = 'ERROR: There was an error during upload. Error code: {0} - {1}';

$app_strings['UPLOAD_ERROR_TEXT_SIZEINFO'] = 'ERROR: There was an error during upload. Error code: {0} - {1}. The upload_maxsize is {2} ';

$app_strings['UPLOAD_ERROR_HOME_TEXT'] = 'ERROR: There was an error during your upload, please contact an administrator for help.';

$app_strings['UPLOAD_MAXIMUM_EXCEEDED'] = 'Size of Upload ({0} bytes) Exceeded Allowed Maximum: {1} bytes';

$app_strings['UPLOAD_REQUEST_ERROR'] = 'An error has occurred. Please refresh your page and try again.';

$app_strings['LBL_EDIT_BUTTON_KEY'] = 'i';

$app_strings['LBL_EDIT_BUTTON_LABEL'] = 'Edit';

$app_strings['LBL_EDIT_BUTTON_TITLE'] = 'Edit';

$app_strings['LBL_DUPLICATE_BUTTON_KEY'] = 'u';

$app_strings['LBL_DUPLICATE_BUTTON_LABEL'] = 'Duplicate';

$app_strings['LBL_DUPLICATE_BUTTON_TITLE'] = 'Duplicate';

$app_strings['LBL_DELETE_BUTTON_KEY'] = 'd';

$app_strings['LBL_DELETE_BUTTON_LABEL'] = 'Delete';

$app_strings['LBL_DELETE_BUTTON_TITLE'] = 'Delete';

$app_strings['LBL_BULK_ACTION_BUTTON_LABEL'] = 'BULK ACTION';

$app_strings['LBL_BULK_ACTION_BUTTON_LABEL_MOBILE'] = 'ACTION';

$app_strings['LBL_SAVE_BUTTON_KEY'] = 'a';

$app_strings['LBL_SAVE_BUTTON_LABEL'] = 'Save';

$app_strings['LBL_SAVE_BUTTON_TITLE'] = 'Save';

$app_strings['LBL_CANCEL_BUTTON_KEY'] = 'l';

$app_strings['LBL_CANCEL_BUTTON_LABEL'] = 'Cancel';

$app_strings['LBL_CANCEL_BUTTON_TITLE'] = 'Cancel';

$app_strings['LBL_FIRST_INPUT_EDIT_VIEW_KEY'] = '7';

$app_strings['LBL_ADV_SEARCH_LNK_KEY'] = '8';

$app_strings['LBL_FIRST_INPUT_SEARCH_KEY'] = '9';

$app_strings['ERR_CONNECTOR_NOT_ARRAY'] = 'connector array in {0} been defined incorrectly or is empty and could not be used.';

$app_strings['ERR_SUHOSIN'] = 'Upload stream is blocked by Suhosin, please add ';

$app_strings['ERR_BAD_RESPONSE_FROM_SERVER'] = 'Bad response from the server';

$app_strings['LBL_ACCOUNT_PRODUCT_QUOTE_LINK'] = 'Quote';

$app_strings['LBL_ACCOUNT_PRODUCT_SALE_PRICE'] = 'Sale Price';

$app_strings['LBL_EMAIL_CHECK_INTERVAL_DOM'] = 'Array';

$app_strings['ERR_A_REMINDER_IS_EMPTY_OR_INCORRECT'] = 'A reminder is empty or incorrect.';

$app_strings['ERR_REMINDER_IS_NOT_SET_POPUP_OR_EMAIL'] = 'Reminder is not set for either a popup or email.';

$app_strings['ERR_NO_INVITEES_FOR_REMINDER'] = 'No invitees for reminder.';

$app_strings['LBL_DELETE_REMINDER_CONFIRM'] = 'Reminder doesn\'t include any invitees, do you want to remove the reminder?';

$app_strings['LBL_DELETE_REMINDER'] = 'Delete Reminder';

$app_strings['LBL_OK'] = 'Ok';

$app_strings['LBL_COLUMNS_FILTER_HEADER_TITLE'] = 'Choose columns';

$app_strings['LBL_COLUMN_CHOOSER'] = 'Column Chooser';

$app_strings['LBL_SAVE_CHANGES_BUTTON_TITLE'] = 'Save changes';

$app_strings['LBL_DISPLAYED'] = 'Displayed';

$app_strings['LBL_HIDDEN'] = 'Hidden';

$app_strings['ERR_EMPTY_COLUMNS_LIST'] = 'At least, one element required';

$app_strings['LBL_FILTER_HEADER_TITLE'] = 'Filter';

$app_strings['LBL_CATEGORY'] = 'Category';

$app_strings['LBL_LIST_CATEGORY'] = 'Category';

$app_strings['ERR_FACTOR_TPL_INVALID'] = 'Factor Authentication message is invalid, please contact to your administrator.';

$app_strings['LBL_SUBTHEMES'] = 'Style';

$app_strings['LBL_SUBTHEME_OPTIONS_DAWN'] = 'Dawn';

$app_strings['LBL_SUBTHEME_OPTIONS_DAY'] = 'Day';

$app_strings['LBL_SUBTHEME_OPTIONS_DUSK'] = 'Dusk';

$app_strings['LBL_SUBTHEME_OPTIONS_NIGHT'] = 'Night';

$app_strings['LBL_CONFIRM_DISREGARD_DRAFT_TITLE'] = 'Disregard draft';

$app_strings['LBL_CONFIRM_DISREGARD_DRAFT_BODY'] = 'This operation will delete this email, do you want to continue?';

$app_strings['LBL_CONFIRM_DISREGARD_EMAIL_TITLE'] = 'Exit compose dialog';

$app_strings['LBL_CONFIRM_DISREGARD_EMAIL_BODY'] = 'By leaving the compose dialog all entered information will be lost, do you wish to continue?';

$app_strings['LBL_CONFIRM_APPLY_EMAIL_TEMPLATE_TITLE'] = 'Apply an Email Template';

$app_strings['LBL_CONFIRM_APPLY_EMAIL_TEMPLATE_BODY'] = 'This operation will override the email Body and Subject fields, do you want to continue?';

$app_strings['LBL_CONFIRM_OPT_IN_TITLE'] = 'Confirmed Opt In';

$app_strings['LBL_OPT_IN_TITLE'] = 'Opt In';

$app_strings['LBL_CONFIRM_OPT_IN_DATE'] = 'Confirmed Opt In Date';

$app_strings['LBL_CONFIRM_OPT_IN_SENT_DATE'] = 'Confirmed Opt In Sent Date';

$app_strings['LBL_CONFIRM_OPT_IN_FAIL_DATE'] = 'Confirmed Opt In Fail Date';

$app_strings['LBL_CONFIRM_OPT_IN_TOKEN'] = 'Confirm Opt In Token';

$app_strings['ERR_OPT_IN_TPL_NOT_SET'] = 'Opt In Email Template is not configured. Please set up in email settings.';

$app_strings['ERR_OPT_IN_RELATION_INCORRECT'] = 'Opt In requires the email to be related to Account/Contact/Lead/Target';

$app_strings['LBL_SECURITYGROUP_NONINHERITABLE'] = 'Non-Inheritable Group';

$app_strings['LBL_PRIMARY_GROUP'] = 'Primary Group';

$app_strings['LBL_SUITE_TOP'] = 'Back to top';

$app_strings['LBL_SUITE_SUPERCHARGED'] = 'Supercharged by SuiteCRM';

$app_strings['LBL_SUITE_POWERED_BY'] = 'Powered By SugarCRM';

$app_strings['LBL_SUITE_DESC1'] = 'SuiteCRM has been written and assembled by <a href=';

$app_strings['LBL_SUITE_DESC2'] = 'This program is free software; you can redistribute it and/or modify it under the terms of the GNU Affero General Public License version 3 as published by the Free Software Foundation, including the additional permission set forth in the source code header.';

$app_strings['LBL_SUITE_DESC3'] = 'SuiteCRM is a trademark of SalesAgility Ltd. All other company and product names may be trademarks of the respective companies with which they are associated.';

$app_strings['LBL_GENERATE_PASSWORD_BUTTON_TITLE'] = 'Reset Password';

$app_strings['LBL_SEND_CONFIRM_OPT_IN_EMAIL'] = 'Send Confirm Opt In Email';

$app_strings['LBL_CONFIRM_OPT_IN_ONLY_FOR_PERSON'] = 'Confirm Opt In Email sending only for Accounts/Contacts/Leads/Prospects';

$app_strings['LBL_CONFIRM_OPT_IN_IS_DISABLED'] = 'Confirm Opt In Email sending is disabled, enable Confirm Opt In option in Email Settings or contact your Administrator.';

$app_strings['LBL_CONTACT_HAS_NO_PRIMARY_EMAIL'] = 'Confirm Opt In Email sending is not possible because the Contact has not Primary Email Address';

$app_strings['LBL_CONFIRM_EMAIL_SENDING_FAILED'] = 'Confirm Opt In Email sending failed';

$app_strings['LBL_CONFIRM_EMAIL_SENT'] = 'Confirm Opt In Email sent successfully';

$app_strings['LBL_STATUS_EVENT'] = 'Invite Status';

$app_strings['LBL_ACCEPT_STATUS'] = 'Accept Status';

$app_strings['LBL_LISTVIEW_OPTION_CURRENT'] = 'Select This Page';

$app_strings['LBL_LISTVIEW_OPTION_ENTIRE'] = 'Select All';

$app_strings['LBL_LISTVIEW_NONE'] = 'Deselect All';

$app_strings['LBL_AOP_EMAIL_REPLY_DELIMITER'] = '========== Please reply above this line ==========';

$app_strings['LBL_CRON_ON_THE_MONTHDAY'] = 'on the';

$app_strings['LBL_CRON_ON_THE_WEEKDAY'] = 'on';

$app_strings['LBL_CRON_AT'] = 'at';

$app_strings['LBL_CRON_RAW'] = 'Advanced';

$app_strings['LBL_CRON_MIN'] = 'Min';

$app_strings['LBL_CRON_HOUR'] = 'Hour';

$app_strings['LBL_CRON_DAY'] = 'Day';

$app_strings['LBL_CRON_MONTH'] = 'Month';

$app_strings['LBL_CRON_DOW'] = 'DOW';

$app_strings['LBL_CRON_DAILY'] = 'Daily';

$app_strings['LBL_CRON_WEEKLY'] = 'Weekly';

$app_strings['LBL_CRON_MONTHLY'] = 'Monthly';

$app_strings['LBL_PRINT_AS_PDF'] = 'Print as PDF';

$app_strings['LBL_SELECT_TEMPLATE'] = 'Please Select a Template';

$app_strings['LBL_NO_TEMPLATE'] = 'ERROR\\nNo templates found.\\nPlease go to the PDF templates module and create one';

$app_strings['LBL_GANTT_BUTTON_LABEL'] = 'View Gantt';

$app_strings['LBL_DETAIL_BUTTON_LABEL'] = 'View Detail';

$app_strings['LBL_CREATE_PROJECT'] = 'Create Project';

$app_strings['LBL_MAP'] = 'Map';

$app_strings['LBL_JJWG_MAPS_LNG'] = 'Longitude';

$app_strings['LBL_JJWG_MAPS_LAT'] = 'Latitude';

$app_strings['LBL_JJWG_MAPS_GEOCODE_STATUS'] = 'Geocode Status';

$app_strings['LBL_JJWG_MAPS_ADDRESS'] = 'Address';

$app_strings['LBL_RESCHEDULE_LABEL'] = 'Reschedule';

$app_strings['LBL_RESCHEDULE_TITLE'] = 'Please enter the reschedule information';

$app_strings['LBL_RESCHEDULE_DATE'] = 'Date:';

$app_strings['LBL_RESCHEDULE_REASON'] = 'Reason:';

$app_strings['LBL_RESCHEDULE_ERROR1'] = 'Please select a valid date';

$app_strings['LBL_RESCHEDULE_ERROR2'] = 'Please select a reason';

$app_strings['LBL_RESCHEDULE_PANEL'] = 'Reschedule';

$app_strings['LBL_RESCHEDULE_HISTORY'] = 'Call attempt history';

$app_strings['LBL_RESCHEDULE_COUNT'] = 'Call Attempts';

$app_strings['LBL_LOGIN_AS'] = 'Login as ';

$app_strings['LBL_LOGOUT_AS'] = 'Logout as ';

$app_strings['LBL_SECURITYGROUP'] = 'Security Group';

$app_strings['FACEBOOK_USER_C'] = 'Facebook';

$app_strings['TWITTER_USER_C'] = 'Twitter';

$app_strings['LBL_PANEL_SOCIAL_FEED'] = 'Social Feed Details';

$app_strings['LBL_SUBPANEL_FILTER_LABEL'] = 'Filter';

$app_strings['LBL_COLLECTION_TYPE'] = 'Type';

$app_strings['LBL_ADD_TAB'] = 'Add Tab';

$app_strings['LBL_EDIT_TAB'] = 'Edit Tabs';

$app_strings['LBL_SUITE_DASHBOARD'] = 'SUITECRM DASHBOARD';

$app_strings['LBL_ENTER_DASHBOARD_NAME'] = 'Enter Dashboard Name:';

$app_strings['LBL_NUMBER_OF_COLUMNS'] = 'Number of Columns:';

$app_strings['LBL_DELETE_DASHBOARD1'] = 'Are you sure you want to delete the';

$app_strings['LBL_DELETE_DASHBOARD2'] = 'dashboard?';

$app_strings['LBL_ADD_DASHBOARD_PAGE'] = 'Add a Dashboard Page';

$app_strings['LBL_DELETE_DASHBOARD_PAGE'] = 'Remove Current Dashboard Page';

$app_strings['LBL_RENAME_DASHBOARD_PAGE'] = 'Rename Dashboard Page';

$app_strings['LBL_SUITE_DASHBOARD_ACTIONS'] = 'ACTIONS';

$app_strings['LBL_CONFIRM_CANCEL_INLINE_EDITING'] = 'You have clicked away from the field you were editing without saving it. Click ok if you\'re happy to lose your change, or cancel if you would like to continue editing';

$app_strings['LBL_LOADING_ERROR_INLINE_EDITING'] = 'There was an error loading the field. Your session may have timed out. Please log in again to fix this';

$app_strings['LBL_OPT_IN_PENDING_EMAIL_NOT_SENT'] = 'Pending Confirm opt in, Confirm opt in not sent';

$app_strings['LBL_OPT_IN_PENDING_EMAIL_FAILED'] = 'Confirm opt in email sending failed';

$app_strings['LBL_OPT_IN_PENDING_EMAIL_SENT'] = 'Pending Confirm opt in, Confirm opt in sent';

$app_strings['LBL_OPT_IN'] = 'Opted in';

$app_strings['LBL_OPT_IN_CONFIRMED'] = 'Confirmed Opted in';

$app_strings['LBL_OPT_IN_OPT_OUT'] = 'Opted Out';

$app_strings['LBL_OPT_IN_INVALID'] = 'Invalid';

$app_strings['RESPONSE_SEND_CONFIRM_OPT_IN_EMAIL'] = 'The confirm opt in email has been added to the email queue for %s email address(es). ';

$app_strings['RESPONSE_SEND_CONFIRM_OPT_IN_EMAIL_NOT_OPT_IN'] = 'Unable to send email to %s email address(es), because they are not opted in. ';

$app_strings['RESPONSE_SEND_CONFIRM_OPT_IN_EMAIL_MISSING_EMAIL_ADDRESS_ID'] = '%s email address do not have a valid id. ';

$app_strings['ERR_TWO_FACTOR_FAILED'] = 'Two Factor Authentication failed';

$app_strings['ERR_TWO_FACTOR_CODE_SENT'] = 'Two Factor Authentication code sent.';

$app_strings['ERR_TWO_FACTOR_CODE_FAILED'] = 'Two Factor Authentication code failed to send.';

$app_strings['LBL_THANKS_FOR_SUBMITTING'] = 'Thank you for submitting your interest.';

$app_strings['ERR_IP_CHANGE'] = 'Your session was terminated due to a significant change in your IP address';

$app_strings['ERR_RETURN'] = 'Return to Home';

$app_strings['LBL_DEFAULT_API_ERROR_TITLE'] = 'JSON API Error';

$app_strings['LBL_DEFAULT_API_ERROR_DETAIL'] = 'JSON API Error occurred.';

$app_strings['LBL_API_EXCEPTION_DETAIL'] = 'Api Version: 8';

$app_strings['LBL_BAD_REQUEST_EXCEPTION_DETAIL'] = 'Please ensure you fill in the fields required';

$app_strings['LBL_EMPTY_BODY_EXCEPTION_DETAIL'] = 'Json API expects body of the request to be JSON';

$app_strings['LBL_INVALID_JSON_API_REQUEST_EXCEPTION_DETAIL'] = 'Unable to validate the Json Api Payload Request';

$app_strings['LBL_INVALID_JSON_API_RESPONSE_EXCEPTION_DETAIL'] = 'Unable to validate the Json Api Payload Response';

$app_strings['LBL_MODULE_NOT_FOUND_EXCEPTION_DETAIL'] = 'Json API cannot find resource';

$app_strings['LBL_NOT_ACCEPTABLE_EXCEPTION_DETAIL'] = 'Json API expects the ';

$app_strings['LBL_UNSUPPORTED_MEDIA_TYPE_EXCEPTION_DETAIL'] = 'Json API expects the ';

$app_strings['MSG_BROWSER_NOTIFICATIONS_ENABLED'] = 'Desktop notifications are now enabled for this web browser.';

$app_strings['MSG_BROWSER_NOTIFICATIONS_DISABLED'] = 'Desktop notifications are disabled for this web browser. Use your browser preferences to enable them again.';

$app_strings['MSG_BROWSER_NOTIFICATIONS_UNSUPPORTED'] = 'This browser does not support desktop notifications.';

$app_strings['LBL_GOOGLE_SYNC_ERR'] = 'SuiteCRM Google Sync - ERROR';

$app_strings['LBL_THERE_WAS_AN_ERR'] = 'There was an error: ';

$app_strings['LBL_CLICK_HERE'] = 'Click here';

$app_strings['LBL_TO_CONTINUE'] = ' to continue.';

$app_strings['IMAP_HANDLER_ERROR'] = 'ERROR: {error}; key was: ';

$app_strings['IMAP_HANDLER_SUCCESS'] = 'OK: test settings changed to ';

$app_strings['IMAP_HANDLER_ERROR_INVALID_REQUEST'] = 'Invalid request, use ';

$app_strings['IMAP_HANDLER_ERROR_UNKNOWN_BY_KEY'] = 'Unknown error occurred, key ';

$app_strings['IMAP_HANDLER_ERROR_NO_TEST_SET'] = 'Test settings does not exists.';

$app_strings['IMAP_HANDLER_ERROR_NO_KEY'] = 'Key not found.';

$app_strings['IMAP_HANDLER_ERROR_KEY_SAVE'] = 'Key saving error.';

$app_strings['IMAP_HANDLER_ERROR_UNKNOWN'] = 'Unknown error';

$app_strings['LBL_SEARCH_TITLE'] = 'Search';

$app_strings['LBL_SEARCH_TEXT_FIELD_TITLE_ATTR'] = 'Input Search Criteria';

$app_strings['LBL_SEARCH_SUBMIT_FIELD_TITLE_ATTR'] = 'Search';

$app_strings['LBL_SEARCH_SUBMIT_FIELD_VALUE'] = 'Search';

$app_strings['LBL_SEARCH_QUERY'] = 'Search query: ';

$app_strings['LBL_SEARCH_RESULTS_PER_PAGE'] = 'Results per page: ';

$app_strings['LBL_SEARCH_ENGINE'] = 'Engine: ';

$app_strings['LBL_SEARCH_TOTAL'] = 'Total result(s): ';

$app_strings['LBL_SEARCH_PREV'] = 'Previous';

$app_strings['LBL_SEARCH_NEXT'] = 'Next';

$app_strings['LBL_SEARCH_PAGE'] = 'Page ';

$app_strings['LBL_SEARCH_OF'] = ' of ';

$app_strings['LBL_TABGROUP_REPORTS'] = 'Reports Dynamic';

$app_strings['LBL_CREATE_APPS_BUTTON_LABEL'] = 'Create Applications';

$app_strings['LBL_NEW_COLUMN_TITLE'] = 'New';

$app_strings['LBL_IN_PROCESS_COLUMN_TITLE'] = 'In Process';

$app_strings['LBL_CONVERTED_COLUMN_TITLE'] = 'Converted';

$app_strings['LBL_INVOICED_COLUMN_TITLE'] = 'Transaction';

$app_strings['LBL_ETS_AMOUNT_TITLE'] = 'Revenue probability';

$app_strings['LBL_COMMISSION_AMOUNT_TITLE'] = 'Total Revenue & Commission';

$app_strings['LBL_DETAIL_LABEL'] = 'Detail';

$app_strings['LBL_FILTER_LABEL'] = 'Filter Activities';

$app_strings['LBL_PLEASE_SELECT_A_DATE'] = 'Please fill a date';

$app_strings['LBL_BTN_FILTER'] = 'Filter';

$app_strings['LBL_DATE_FROM'] = 'From';

$app_strings['LBL_DATE_TO'] = 'To';

$app_strings['LBL_PERIOD'] = 'Period';

$app_strings['LBL_ADD_CALL_MODAL_TITLE'] = 'New call';

$app_strings['LBL_ADD_MEETING_MODAL_TITLE'] = 'New meeting';

$app_strings['LBL_CONFIRM_CLOSE_ACTIVITY_QUESTION'] = 'Are you sure want to close this activity?';

$app_strings['LBL_CLOSE'] = 'Close';

$app_strings['LBL_QUOTATION'] = 'Quote';

$app_strings['LBL_CONFIRM_DEAD_LEAD'] = 'Are you sure want to stop care on this customer?';

$app_strings['LBL_CONFIRM_CONVERTED_LEAD'] = 'Are you sure want to convert this customer?';

$app_strings['LBL_INPROCESS_CUSTOMER_PERCENT'] = 'In Process customer';

$app_strings['LBL_NEW_CUSTOMER_PERCENT'] = 'Total New customer';

$app_strings['LBL_CONVERTED_CUSTOMER_PERCENT'] = 'The number client has converted';

$app_strings['LBL_REVENUE_REACHED'] = 'Actually Revenue (Million VND)';

$app_strings['LBL_REVENUE_IN_KPI'] = 'Revenue in KPI (Million VND)';

$app_strings['LBL_INPROCESS_CUSTOMER_REPORT_TITLE'] = 'Percent In Process of New in Period';

$app_strings['LBL_CONVERTED_CUSTOMER_REPORT_TITLE'] = 'Percent Converted of In Process in Period';

$app_strings['LBL_REVENUE_REPORT_TITLE'] = 'Percent Actually revenue';

$app_strings['LBL_KPI_REPORT_TITLE'] = 'KPI';

$app_strings['LBL_NUM_CONVERTED_CUSTOMER_REPORT_TITLE'] = 'Number of Transaction in Period';

$app_strings['LBL_KP_COMMISSION_TITLE'] = 'Total Revenue & Commission';

$app_strings['LBL_NEW_CUSTOMER'] = 'New Leads';

$app_strings['LBL_INPROCESS_CUSTOMER'] = 'In Process Leads';

$app_strings['LBL_MONTHS'] = 'Months';

$app_strings['LBL_NUMBER_OF_CONTRACT'] = 'Number of Transaction';

$app_strings['LBL_KPI_CONFIG_INCORRECT'] = 'Warning: Not found any kpi config is valid or has not configured kpi, Please contact administrator!';

$app_strings['LBL_ID_FF_OPT_IN'] = 'Chọn tham gia';

$app_strings['LBL_ERROR_UNDEFINED_BEHAVIOR'] = 'Lỗi bất ngờ đã xảy ra.';

$app_strings['LBL_ERROR_UNHANDLED_VALUE'] = 'Một giá trị đã không được xử lý một cách chính xác đó ngăn chặn một quá trình tiếp tục.';

$app_strings['LBL_ERROR_UNUSABLE_VALUE'] = 'Một giá trị không sử dụng được tìm thấy đó ngăn chặn một quá trình tiếp tục.';

$app_strings['LBL_ERROR_INVALID_TYPE'] = 'Các loại giá trị là khác nhau hơn so với những gì đã được dự kiến.';

$app_strings['LBL_NOTIFICATIONS'] = 'Chú ý';

$app_strings['LBL_EMAIL_REPORTS_TITLE'] = 'Báo cáo';

$app_strings['LBL_EMAIL_OPT_IN'] = 'Đã tham gia';

$app_strings['LBL_EMAIL_OPT_IN_AND_INVALID'] = 'Đã chọn tham gia và không hợp lệ';

$app_strings['ERR_FILE_EMPTY'] = 'Tập tin rỗng';

$app_strings['MSG_IS_LESS_THAN'] = 'là ít hơn';

$app_strings['LBL_NUMBER_NEW_CUSTOMER'] = 'Number New Customer';

$app_strings['LBL_NUMBER_NEW_CUSTOMER_CENTER_BUYER'] = 'New Customers';

$app_strings['LBL_NUMBER_EXIT_CUSTOMER_CENTER_BUYER'] = 'Exit Customers';

$app_strings['LBL_NUMBER_CONVERTED_CUSTOMER'] = 'Converted Leads';

$app_strings['LBL_NUMBER_INPROCESS_CUSTOMER'] = 'Number In-process Customer';

$app_strings['LBL_NUMBER_CALL_CENTER'] = 'Call Center';

$app_strings['LBL_REVENUE'] = 'Revenue';

$app_strings['LBL_EFFECT_DATE'] = 'Effect date';

$app_strings['LBL_EXPIRY_DATE'] = 'Expiry Date';

$app_strings['LBL_NUMBER_EMAIL_QUOTE'] = 'Email Quote';

$app_strings['LBL_NUMBER_EMAIL'] = 'Email';

$app_strings['LBL_NUMBER_MEETING'] = 'Meeting';

$app_strings['LBL_NUMBER_CALL'] = 'Call';

$app_strings['LBL_GROUPTAB6_1615864794'] = 'Sys Admin';

$app_strings['KPI_CUSTOMER'] = 'KPI OF CUSTOMER';

$app_strings['KPI_ACTIVITIES'] = 'KPI OF ACTIVITIES';
?>