<?php
$app_strings['LBL_SEARCH_REAULTS_TITLE'] = 'Kết quả';

$app_strings['ERR_SEARCH_INVALID_QUERY'] = 'Có lỗi xảy ra khi thực hiện tìm kiếm. Cú pháp truy vấn của bạn không hợp lệ.';

$app_strings['ERR_SEARCH_NO_RESULTS'] = 'Không có kết quả tìm kiếm theo tiêu chí của bạn. Thử mở rộng tìm kiếm.';

$app_strings['LBL_SEARCH_PERFORMED_IN'] = 'Tìm kiếm thực hiện trong';

$app_strings['LBL_EMAIL_CODE'] = 'Mã email:';

$app_strings['LBL_SEND'] = 'Gửi';

$app_strings['LBL_LOGOUT'] = 'Đăng xuất';

$app_strings['LBL_TOUR_NEXT'] = 'Tới';

$app_strings['LBL_TOUR_SKIP'] = 'Bỏ qua';

$app_strings['LBL_TOUR_BACK'] = 'Lùi';

$app_strings['LBL_TOUR_TAKE_TOUR'] = 'Du ngoạn một vòng';

$app_strings['LBL_MOREDETAIL'] = 'Chi tiết hơn';

$app_strings['LBL_EDIT_INLINE'] = 'Sửa trực tiếp';

$app_strings['LBL_VIEW_INLINE'] = 'Xem trực tiếp';

$app_strings['LBL_BASIC_SEARCH'] = 'Bộ lọc';

$app_strings['LBL_BLANK'] = ' ';

$app_strings['LBL_ID_FF_ADD'] = 'Thêm';

$app_strings['LBL_ID_FF_ADD_EMAIL'] = 'Thêm Địa chỉ Email';

$app_strings['LBL_HIDE_SHOW'] = 'Ẩn/hiện';

$app_strings['LBL_DELETE_INLINE'] = 'Xóa';

$app_strings['LBL_ID_FF_CLEAR'] = 'Xóa sạch';

$app_strings['LBL_ID_FF_VCARD'] = 'vCard';

$app_strings['LBL_ID_FF_REMOVE'] = 'Xóa bỏ';

$app_strings['LBL_ID_FF_REMOVE_EMAIL'] = 'Xóa địa chỉ Email';

$app_strings['LBL_ID_FF_OPT_OUT'] = 'Chọn không tham gia';

$app_strings['LBL_ID_FF_INVALID'] = 'Thực hiện không hợp lệ';

$app_strings['LBL_ADD'] = 'Thêm';

$app_strings['LBL_COMPANY_LOGO'] = 'Logo công ty';

$app_strings['LBL_CONNECTORS_POPUPS'] = 'Kết nối quảng cáo bật nhanh';

$app_strings['LBL_CLOSEINLINE'] = 'Đóng';

$app_strings['LBL_VIEWINLINE'] = 'Xem';

$app_strings['LBL_INFOINLINE'] = 'Thông tin';

$app_strings['LBL_PRINT'] = 'In';

$app_strings['LBL_HELP'] = 'Giúp đỡ';

$app_strings['LBL_ID_FF_SELECT'] = 'Chọn';

$app_strings['DEFAULT'] = 'Cơ bản';

$app_strings['LBL_SORT'] = 'Sắp xếp';

$app_strings['LBL_EMAIL_SMTP_SSL_OR_TLS'] = 'Kích hoạt SMTP qua SSL hoặc TLS?';

$app_strings['LBL_NO_ACTION'] = 'Không có hành động bởi cái tên: %s';

$app_strings['LBL_NO_SHORTCUT_MENU'] = 'Không có hành động sẵn sàng.';

$app_strings['LBL_NO_DATA'] = 'Không có dữ liệu';

$app_strings['LBL_ROUTING_FLAGGED'] = 'cài đặt cờ';

$app_strings['LBL_ROUTING_TO'] = 'a';

$app_strings['LBL_ROUTING_TO_ADDRESS'] = 'đến địa chỉ';

$app_strings['LBL_ROUTING_WITH_TEMPLATE'] = 'với mẫu';

$app_strings['NTC_OVERWRITE_ADDRESS_PHONE_CONFIRM'] = 'Kỷ lục này hiện đang có giá trị trong các lĩnh vực văn phòng điện thoại và địa chỉ. Để ghi đè lên các giá trị này với các văn phòng điện thoại và địa chỉ của tài khoản mà bạn đã chọn, nhấn ';

$app_strings['LBL_DROP_HERE'] = '[Thả ở đây]';

$app_strings['LBL_EMAIL_ACCOUNTS_GMAIL_DEFAULTS'] = 'Điền trước Gmail™ mặc định';

$app_strings['LBL_EMAIL_ACCOUNTS_NAME'] = 'Tên';

$app_strings['LBL_EMAIL_ACCOUNTS_OUTBOUND'] = 'Thuộc tính máy chủ gửi thư';

$app_strings['LBL_EMAIL_ACCOUNTS_SMTPPASS'] = 'Mật khẩu SMTP';

$app_strings['LBL_EMAIL_ACCOUNTS_SMTPPORT'] = 'Cổng SMTP';

$app_strings['LBL_EMAIL_ACCOUNTS_SMTPSERVER'] = 'Máy chủ SMTP';

$app_strings['LBL_EMAIL_ACCOUNTS_SMTPUSER'] = 'Tên người dùng SMTP';

$app_strings['LBL_EMAIL_ACCOUNTS_SMTPDEFAULT'] = 'Mặc định';

$app_strings['LBL_EMAIL_WARNING_MISSING_USER_CREDS'] = 'Cảnh báo: Thiếu tên người dùng và mật khẩu cho tài khoản e-mail gửi đi.';

$app_strings['LBL_EMAIL_ACCOUNTS_SUBTITLE'] = 'Thiết lập tài khoản Mail để xem các email từ các tài khoản email của bạn.';

$app_strings['LBL_EMAIL_ACCOUNTS_OUTBOUND_SUBTITLE'] = 'Cung cấp thông tin máy chủ SMTP mail để sử dụng cho email gửi đi trong tài khoản Mail.';

$app_strings['LBL_EMAIL_ADDRESS_BOOK_ADD'] = 'Xong';

$app_strings['LBL_EMAIL_ADDRESS_BOOK_CLEAR'] = 'Xóa';

$app_strings['LBL_EMAIL_ADDRESS_BOOK_ADD_TO'] = 'Đến:';

$app_strings['LBL_EMAIL_ADDRESS_BOOK_ADD_CC'] = 'Cc:';

$app_strings['LBL_EMAIL_ADDRESS_BOOK_ADD_BCC'] = 'Bcc:';

$app_strings['LBL_EMAIL_ADDRESS_BOOK_ADRRESS_TYPE'] = 'To/Cc/Bcc';

$app_strings['LBL_EMAIL_ADDRESS_BOOK_EMAIL_ADDR'] = 'Địa chỉ Email';

$app_strings['LBL_EMAIL_ADDRESS_BOOK_FILTER'] = 'Bộ lọc';

$app_strings['LBL_EMAIL_ADDRESS_BOOK_NAME'] = 'Tên';

$app_strings['LBL_EMAIL_ADDRESS_BOOK_NOT_FOUND'] = 'Không tìm thấy địa chỉ';

$app_strings['LBL_EMAIL_ADDRESS_BOOK_SAVE_AND_ADD'] = 'Lưu & Thêm vào sổ địa chỉ';

$app_strings['LBL_EMAIL_ADDRESS_BOOK_SELECT_TITLE'] = 'Chọn người nhận Email';

$app_strings['LBL_EMAIL_ADDRESS_BOOK_TITLE'] = 'Sổ địa chỉ';

$app_strings['LBL_EMAIL_REMOVE_SMTP_WARNING'] = 'Cảnh báo! Ngoài tài khoản bạn đang cố gắng để xóa là liên quan đến tài khoản hiện tại trong nước. Bạn có chắc bạn muốn tiếp tục không?';

$app_strings['LBL_EMAIL_ADDRESSES'] = 'Hộp thư đến';

$app_strings['LBL_EMAIL_ADDRESS_PRIMARY'] = 'Địa chỉ Email';

$app_strings['LBL_EMAIL_ADDRESS_OPT_IN'] = 'Bạn đã xác nhận địa chỉ email của bạn đã được chọn để sử dụng: ';

$app_strings['LBL_EMAIL_ADDRESS_OPT_IN_ERR'] = 'Không thể xác nhận địa chỉ email';

$app_strings['LBL_EMAIL_ARCHIVE_TO_SUITE'] = 'Nhập vào SuiteCRM';

$app_strings['LBL_EMAIL_ASSIGNMENT'] = 'Chỉ định';

$app_strings['LBL_EMAIL_ATTACH_FILE_TO_EMAIL'] = 'Đính kèm';

$app_strings['LBL_EMAIL_ATTACHMENT'] = 'Đính kèm';

$app_strings['LBL_EMAIL_ATTACHMENTS'] = 'Từ hệ thống cơ sở';

$app_strings['LBL_EMAIL_ATTACHMENTS2'] = 'Từ các tài liệu SuiteCRM';

$app_strings['LBL_EMAIL_ATTACHMENTS3'] = 'Mẫu đính kèm';

$app_strings['LBL_EMAIL_ATTACHMENTS_FILE'] = 'Tập tin';

$app_strings['LBL_EMAIL_ATTACHMENTS_DOCUMENT'] = 'Tài liệu';

$app_strings['LBL_EMAIL_BCC'] = 'BCC';

$app_strings['LBL_EMAIL_CANCEL'] = 'Hủy';

$app_strings['LBL_EMAIL_CC'] = 'CC';

$app_strings['LBL_EMAIL_CHARSET'] = 'Ký tự';

$app_strings['LBL_EMAIL_CHECK'] = 'Kiểm tra Mail';

$app_strings['LBL_EMAIL_CHECKING_NEW'] = 'Kiểm tra Email mới';

$app_strings['LBL_EMAIL_CHECKING_DESC'] = 'Xin vui lòng chờ một chút... <br><br>Nếu điều này là kiểm tra đầu tiên cho các tài khoản mail, có thể mất một thời gian.';

$app_strings['LBL_EMAIL_CLOSE'] = 'Đóng';

$app_strings['LBL_EMAIL_COFFEE_BREAK'] = 'Kiểm tra Email mới. Tài khoản mail <br><br>lớn có thể mất một lượng đáng kể thời gian.';

$app_strings['LBL_EMAIL_COMPOSE'] = 'Email';

$app_strings['LBL_EMAIL_COMPOSE_ERR_NO_RECIPIENTS'] = 'Vui lòng nhập (nhiều) người nhận cho email này.';

$app_strings['LBL_EMAIL_COMPOSE_NO_BODY'] = 'Phần nội dung của email này là trống rỗng. Cứ gửi chứ?';

$app_strings['LBL_EMAIL_COMPOSE_NO_SUBJECT'] = 'Email này có không có chủ đề. Cứ gửi chứ?';

$app_strings['LBL_EMAIL_COMPOSE_NO_SUBJECT_LITERAL'] = '(không có chủ đề)';

$app_strings['LBL_EMAIL_COMPOSE_INVALID_ADDRESS'] = 'Vui lòng nhập địa chỉ email hợp lệ cho To, CC và BCC';

$app_strings['LBL_EMAIL_CONFIRM_CLOSE'] = 'Loại bỏ  email này?';

$app_strings['LBL_EMAIL_CONFIRM_DELETE_SIGNATURE'] = 'Bạn có chắc muốn xóa chữ ký này?';

$app_strings['LBL_EMAIL_SENT_SUCCESS'] = 'Email được gửi';

$app_strings['LBL_EMAIL_CREATE_NEW'] = '--Tạo lưu trữ--';

$app_strings['LBL_EMAIL_MULT_GROUP_FOLDER_ACCOUNTS'] = 'Nhiều cùng lúc';

$app_strings['LBL_EMAIL_MULT_GROUP_FOLDER_ACCOUNTS_EMPTY'] = 'Trống';

$app_strings['LBL_EMAIL_DATE_SENT_BY_SENDER'] = 'Ngày được gửi bởi người gửi';

$app_strings['LBL_EMAIL_DATE_TODAY'] = 'Xem Lịch';

$app_strings['LBL_EMAIL_DELETE'] = 'Xóa';

$app_strings['LBL_EMAIL_DELETE_CONFIRM'] = 'Xoá thư đã chọn không?';

$app_strings['LBL_EMAIL_DELETE_SUCCESS'] = 'Email đã xóa thành công.';

$app_strings['LBL_EMAIL_DELETING_MESSAGE'] = 'Xoá tin nhắn';

$app_strings['LBL_EMAIL_DETAILS'] = 'Chi tiết';

$app_strings['LBL_EMAIL_EDIT_CONTACT_WARN'] = 'Chỉ có địa chỉ chính sẽ được sử dụng khi làm việc với danh bạ.';

$app_strings['LBL_EMAIL_EMPTYING_TRASH'] = 'Làm rỗng thùng rác';

$app_strings['LBL_EMAIL_DELETING_OUTBOUND'] = 'Đang xóa máy chủ của thư gửi đi';

$app_strings['LBL_EMAIL_CLEARING_CACHE_FILES'] = 'Đang làm sạch bộ nhớ tập tin';

$app_strings['LBL_EMAIL_EMPTY_MSG'] = 'Không có email để hiển thị.';

$app_strings['LBL_EMAIL_EMPTY_ADDR_MSG'] = 'Không có địa chỉ email để hiển thị.';

$app_strings['LBL_EMAIL_ERROR_ADD_GROUP_FOLDER'] = 'Tên thư mục phải là duy nhất và không để trống. Vui lòng thử lại.';

$app_strings['LBL_EMAIL_ERROR_DELETE_GROUP_FOLDER'] = 'Không thể xóa một thư mục. Hoặc thư mục hoặc thư mục con có email hoặc một hộp thư liên quan đến nó.';

$app_strings['LBL_EMAIL_ERROR_CANNOT_FIND_NODE'] = 'Không thể xác định thư mục dự kiến từ bối cảnh. Thử lại.';

$app_strings['LBL_EMAIL_ERROR_CHECK_IE_SETTINGS'] = 'Hãy kiểm tra cài đặt của bạn.';

$app_strings['LBL_EMAIL_ERROR_DESC'] = 'Lỗi được phát hiện: ';

$app_strings['LBL_EMAIL_DELETE_ERROR_DESC'] = 'Bạn không có quyền truy cập vào khu vực này. Liên hệ với quản trị trang web của bạn để có được truy cập.';

$app_strings['LBL_EMAIL_ERROR_DUPE_FOLDER_NAME'] = 'Tên thư mục SuiteCRM phải là duy nhất.';

$app_strings['LBL_EMAIL_ERROR_EMPTY'] = 'Vui lòng nhập một số tiêu chí tìm kiếm.';

$app_strings['LBL_EMAIL_ERROR_GENERAL_TITLE'] = 'Một lỗi đã xảy ra';

$app_strings['LBL_EMAIL_ERROR_MESSAGE_DELETED'] = 'Thông báo đã gỡ bỏ khỏi máy chủ';

$app_strings['LBL_EMAIL_ERROR_IMAP_MESSAGE_DELETED'] = 'Tin nhắn đã xóa từ máy chủ hoặc di chuyển đến một thư mục khác nhau';

$app_strings['LBL_EMAIL_ERROR_MAILSERVERCONNECTION'] = 'Kết nối tới máy chủ thư đã thất bại. Xin vui lòng liên hệ với người quản trị';

$app_strings['LBL_EMAIL_ERROR_MOVE'] = 'Di chuyển email giữa máy chủ và/hoặc tài khoản thư không được hỗ trợ tại thời điểm này.';

$app_strings['LBL_EMAIL_ERROR_MOVE_TITLE'] = 'Lỗi di chuyển';

$app_strings['LBL_EMAIL_ERROR_NAME'] = 'Tên là bắt buộc.';

$app_strings['LBL_EMAIL_ERROR_FROM_ADDRESS'] = 'Từ địa chỉ được yêu cầu. Vui lòng nhập một địa chỉ email hợp lệ.';

$app_strings['LBL_EMAIL_ERROR_NO_FILE'] = 'Vui lòng cung cấp một tập tin.';

$app_strings['LBL_EMAIL_ERROR_SERVER'] = 'Một địa chỉ máy chủ là cần thiết.';

$app_strings['LBL_EMAIL_ERROR_SAVE_ACCOUNT'] = 'Tài khoản thư có thể không đã được lưu.';

$app_strings['LBL_EMAIL_ERROR_TIMEOUT'] = 'Xuất hiện lỗi trong khi giao tiếp với máy chủ gửi thư.';

$app_strings['LBL_EMAIL_ERROR_USER'] = 'Tên truy cập là bắt buộc.';

$app_strings['LBL_EMAIL_ERROR_PORT'] = 'Một cổng máy chủ gửi thư được yêu cầu.';

$app_strings['LBL_EMAIL_ERROR_PROTOCOL'] = 'Một giao thức máy chủ là cần thiết.';

$app_strings['LBL_EMAIL_ERROR_MONITORED_FOLDER'] = 'Giám sát thư mục được yêu cầu.';

$app_strings['LBL_EMAIL_ERROR_TRASH_FOLDER'] = 'Thư mục thùng rác là cần thiết.';

$app_strings['LBL_EMAIL_ERROR_VIEW_RAW_SOURCE'] = 'Thông tin này không có sẵn';

$app_strings['LBL_EMAIL_ERROR_NO_OUTBOUND'] = 'Không có máy chủ gửi thư đi được chỉ định.';

$app_strings['LBL_EMAIL_ERROR_SENDING'] = 'Lỗi gửi Email. Xin vui lòng liên hệ với quản trị của bạn để được trợ giúp.';

$app_strings['LBL_EMAIL_FOLDERS'] = '<img src=';

$app_strings['LBL_EMAIL_FOLDERS_SHORT'] = '<img src=';

$app_strings['LBL_EMAIL_FOLDERS_ADD'] = 'Thêm';

$app_strings['LBL_EMAIL_FOLDERS_ADD_DIALOG_TITLE'] = 'Thêm thư mục mới';

$app_strings['LBL_EMAIL_FOLDERS_RENAME_DIALOG_TITLE'] = 'Đổi tên thư mục';

$app_strings['LBL_EMAIL_FOLDERS_ADD_NEW_FOLDER'] = 'Lưu';

$app_strings['LBL_EMAIL_FOLDERS_ADD_THIS_TO'] = 'Thêm thư mục này vào';

$app_strings['LBL_EMAIL_FOLDERS_CHANGE_HOME'] = 'Thư mục này không thể thay đổi';

$app_strings['LBL_EMAIL_FOLDERS_DELETE_CONFIRM'] = 'Bạn có chắc bạn muốn xóa bỏ thư mục này? \\n Quá trình này không thể đảo ngược. \\n Thư mục xóa sẽ loại bỏ tất cả các thư mục chứa bên trong nó.';

$app_strings['LBL_EMAIL_FOLDERS_NEW_FOLDER'] = 'Tên thư mục mới';

$app_strings['LBL_EMAIL_FOLDERS_NO_VALID_NODE'] = 'Vui lòng chọn một thư mục trước khi thực hiện hành động này.';

$app_strings['LBL_EMAIL_FOLDERS_TITLE'] = 'Quản lý thư mục';

$app_strings['LBL_EMAIL_FORWARD'] = 'Chuyển tiếp';

$app_strings['LBL_EMAIL_DELIMITER'] = '::;::';

$app_strings['LBL_EMAIL_DOWNLOAD_STATUS'] = 'Đã tải về [[count]] của [[total]] thư điện tử';

$app_strings['LBL_EMAIL_FROM'] = 'Từ';

$app_strings['LBL_EMAIL_GROUP'] = 'Nhóm';

$app_strings['LBL_EMAIL_UPPER_CASE_GROUP'] = 'Nhóm';

$app_strings['LBL_EMAIL_HOME_FOLDER'] = 'bắt đầu';

$app_strings['LBL_EMAIL_IE_DELETE'] = 'Đang xóa hộp thư';

$app_strings['LBL_EMAIL_IE_DELETE_SIGNATURE'] = 'Xóa chữ ký';

$app_strings['LBL_EMAIL_IE_DELETE_CONFIRM'] = 'Bạn có chắc chắn muốn xoá tài khoản mail này?';

$app_strings['LBL_EMAIL_IE_DELETE_SUCCESSFUL'] = 'Xóa thành công.';

$app_strings['LBL_EMAIL_IE_SAVE'] = 'Đang lưu trữ thông tin hộp thư';

$app_strings['LBL_EMAIL_IMPORTING_EMAIL'] = 'Nhập Email';

$app_strings['LBL_EMAIL_IMPORT_EMAIL'] = 'Nhập vào SuiteCRM';

$app_strings['LBL_EMAIL_IMPORT_SETTINGS'] = 'Nhập thiết lập';

$app_strings['LBL_EMAIL_INVALID'] = 'Không hợp lệ';

$app_strings['LBL_EMAIL_LOADING'] = 'Đang tải...';

$app_strings['LBL_EMAIL_MARK'] = 'Điểm';

$app_strings['LBL_EMAIL_MARK_FLAGGED'] = 'Khi cắm cờ';

$app_strings['LBL_EMAIL_MARK_READ'] = 'Như là đọc';

$app_strings['LBL_EMAIL_MARK_UNFLAGGED'] = 'Như chưa cắm cờ';

$app_strings['LBL_EMAIL_MARK_UNREAD'] = 'Như là chưa đọc';

$app_strings['LBL_EMAIL_ASSIGN_TO'] = 'Chỉ định cho';

$app_strings['LBL_EMAIL_MENU_ADD_FOLDER'] = 'Tạo thư mục';

$app_strings['LBL_EMAIL_MENU_COMPOSE'] = 'Soạn thư';

$app_strings['LBL_EMAIL_MENU_DELETE_FOLDER'] = 'Xoá thư mục';

$app_strings['LBL_EMAIL_MENU_EMPTY_TRASH'] = 'Dọn sạch sọt rác';

$app_strings['LBL_EMAIL_MENU_SYNCHRONIZE'] = 'Đồng bộ hóa';

$app_strings['LBL_EMAIL_MENU_CLEAR_CACHE'] = 'Xoá tập tin tạm';

$app_strings['LBL_EMAIL_MENU_REMOVE'] = 'Xóa bỏ';

$app_strings['LBL_EMAIL_MENU_RENAME_FOLDER'] = 'Đổi tên thư mục';

$app_strings['LBL_EMAIL_MENU_RENAMING_FOLDER'] = 'Đổi tên thư mục';

$app_strings['LBL_EMAIL_MENU_MAKE_SELECTION'] = 'Xin vui lòng thực hiện một sự lựa chọn trước khi cố thao tác này.';

$app_strings['LBL_EMAIL_MENU_HELP_ADD_FOLDER'] = 'Tạo một thư mục (từ xa hoặc SuiteCRM)';

$app_strings['LBL_EMAIL_MENU_HELP_DELETE_FOLDER'] = 'Xóa một thư mục (từ xa hoặc trong SuiteCRM)';

$app_strings['LBL_EMAIL_MENU_HELP_EMPTY_TRASH'] = 'Đổ thùng rác tất cả các thư mục cho tài khoản e-mail của bạn';

$app_strings['LBL_EMAIL_MENU_HELP_MARK_READ'] = 'Đánh dấu các email(s) đã đọc';

$app_strings['LBL_EMAIL_MENU_HELP_MARK_UNFLAGGED'] = 'Đánh dấu các email(s) chưa cắm cờ';

$app_strings['LBL_EMAIL_MENU_HELP_RENAME_FOLDER'] = 'Đổi tên một thư mục (từ xa hoặc trong SuiteCRM)';

$app_strings['LBL_EMAIL_MESSAGES'] = 'thông điệp';

$app_strings['LBL_EMAIL_ML_NAME'] = 'Tên danh sách';

$app_strings['LBL_EMAIL_ML_ADDRESSES_1'] = 'Chọn danh sách địa chỉ';

$app_strings['LBL_EMAIL_ML_ADDRESSES_2'] = 'Danh sách địa chỉ có sẵn';

$app_strings['LBL_EMAIL_MULTISELECT'] = '<b>Ctrl-Click</b> chọn nhiều hơn<br />(Mac users use <b>CMD-Click</b>)';

$app_strings['LBL_EMAIL_NO'] = 'Không';

$app_strings['LBL_EMAIL_NOT_SENT'] = 'Hệ thống không thể xử lý yêu cầu của bạn. Xin vui lòng liên hệ với người quản trị hệ thống.';

$app_strings['LBL_EMAIL_OK'] = 'Ok';

$app_strings['LBL_EMAIL_ONE_MOMENT'] = 'Xin vui lòng chờ một chút...';

$app_strings['LBL_EMAIL_OPEN_ALL'] = 'Mở nhiều thư';

$app_strings['LBL_EMAIL_OPTIONS'] = 'Tùy chọn';

$app_strings['LBL_EMAIL_QUICK_COMPOSE'] = 'Soạn nhanh';

$app_strings['LBL_EMAIL_OPT_OUT'] = 'Từ chối nhận';

$app_strings['LBL_EMAIL_OPT_OUT_AND_INVALID'] = 'Chọn ra và không hợp lệ';

$app_strings['LBL_EMAIL_PERFORMING_TASK'] = 'Thực hiện nhiệm vụ';

$app_strings['LBL_EMAIL_PRIMARY'] = 'Chính';

$app_strings['LBL_EMAIL_PRINT'] = 'In';

$app_strings['LBL_EMAIL_QC_BUGS'] = 'Phát sinh';

$app_strings['LBL_EMAIL_QC_CASES'] = 'Kịch bản';

$app_strings['LBL_EMAIL_QC_LEADS'] = 'Đối tác tiềm năng';

$app_strings['LBL_EMAIL_QC_CONTACTS'] = 'Liên hệ';

$app_strings['LBL_EMAIL_QC_TASKS'] = 'Công việc';

$app_strings['LBL_EMAIL_QC_OPPORTUNITIES'] = 'Cơ hội';

$app_strings['LBL_EMAIL_QUICK_CREATE'] = 'Tạo nhanh';

$app_strings['LBL_EMAIL_REBUILDING_FOLDERS'] = 'Xây dựng lại thư mục';

$app_strings['LBL_EMAIL_RELATE_TO'] = 'Liên quan';

$app_strings['LBL_EMAIL_VIEW_RELATIONSHIPS'] = 'Xem mối quan hệ';

$app_strings['LBL_EMAIL_RECORD'] = 'Bản ghi email';

$app_strings['LBL_EMAIL_REMOVE'] = 'Xóa bỏ';

$app_strings['LBL_EMAIL_REPLY'] = 'Trả lời';

$app_strings['LBL_EMAIL_REPLY_ALL'] = 'Trả lời tất cả';

$app_strings['LBL_EMAIL_REPLY_TO'] = 'Trả lời cho';

$app_strings['LBL_EMAIL_RETRIEVING_MESSAGE'] = 'Đang truy xuất thư';

$app_strings['LBL_EMAIL_RETRIEVING_RECORD'] = 'Đang truy xuất bảng ghi thư';

$app_strings['LBL_EMAIL_SELECT_ONE_RECORD'] = 'Xin vui lòng chọn chỉ có một email';

$app_strings['LBL_EMAIL_RETURN_TO_VIEW'] = 'Quay trở lại mô-đun trước?';

$app_strings['LBL_EMAIL_REVERT'] = 'Đảo ngược';

$app_strings['LBL_EMAIL_RELATE_EMAIL'] = 'Email có liên quan';

$app_strings['LBL_EMAIL_RULES_TITLE'] = 'Quản lý quy tắc';

$app_strings['LBL_EMAIL_SAVE'] = 'Lưu';

$app_strings['LBL_EMAIL_SAVE_AND_REPLY'] = 'Lưu & trả lời';

$app_strings['LBL_EMAIL_SAVE_DRAFT'] = 'Lưu dự thảo';

$app_strings['LBL_EMAIL_DRAFT_SAVED'] = 'Bản thảo đã được lưu';

$app_strings['LBL_EMAIL_SEARCH'] = '<img src=';

$app_strings['LBL_EMAIL_SEARCH_SHORT'] = '<img src=';

$app_strings['LBL_EMAIL_SEARCH_DATE_FROM'] = 'Từ';

$app_strings['LBL_EMAIL_SEARCH_DATE_UNTIL'] = 'Cho đến ngày';

$app_strings['LBL_EMAIL_SEARCH_NO_RESULTS'] = 'Không có kết quả phù hợp với tiêu chí tìm kiếm của bạn.';

$app_strings['LBL_EMAIL_SEARCH_RESULTS_TITLE'] = 'Kết quả tìm kiếm';

$app_strings['LBL_EMAIL_SELECT'] = 'Chọn';

$app_strings['LBL_EMAIL_SEND'] = 'Gửi';

$app_strings['LBL_EMAIL_SENDING_EMAIL'] = 'Đang gửi Email';

$app_strings['LBL_EMAIL_SETTINGS'] = 'Tùy chỉnh';

$app_strings['LBL_EMAIL_SETTINGS_ACCOUNTS'] = 'Tài khoản thư';

$app_strings['LBL_EMAIL_SETTINGS_ADD_ACCOUNT'] = 'Xoá bản ghi';

$app_strings['LBL_EMAIL_SETTINGS_CHECK_INTERVAL'] = 'Kiểm tra thư mới';

$app_strings['LBL_EMAIL_SETTINGS_FROM_ADDR'] = 'Từ địa chỉ';

$app_strings['LBL_EMAIL_SETTINGS_FROM_TO_EMAIL_ADDR'] = 'Địa chỉ email để kiểm tra thông báo:';

$app_strings['LBL_EMAIL_SETTINGS_FROM_NAME'] = 'Từ tên';

$app_strings['LBL_EMAIL_SETTINGS_REPLY_TO_ADDR'] = 'Trả lời đến địa chỉ';

$app_strings['LBL_EMAIL_SETTINGS_FULL_SYNC'] = 'Đồng bộ hóa tất cả các tài khoản Mail';

$app_strings['LBL_EMAIL_TEST_NOTIFICATION_SENT'] = 'Một email được gửi đến địa chỉ email xác định bằng cách sử dụng cài đặt thư gửi đi được cung cấp. Vui lòng kiểm tra để xem nếu các email nhận được để kiểm chứng các cài đặt chính xác.';

$app_strings['LBL_EMAIL_TEST_SEE_FULL_SMTP_LOG'] = 'Xem đầy đủ SMTP đăng nhập';

$app_strings['LBL_EMAIL_SETTINGS_FULL_SYNC_WARN'] = 'Thực hiện đồng bộ hoá đầy đủ? \\nLarge tài khoản mail có thể mất vài phút.';

$app_strings['LBL_EMAIL_SUBSCRIPTION_FOLDER_HELP'] = 'Bấm phím Shift hoặc phím Ctrl để chọn nhiều thư mục.';

$app_strings['LBL_EMAIL_SETTINGS_GENERAL'] = 'Tổng quan';

$app_strings['LBL_EMAIL_SETTINGS_GROUP_FOLDERS_CREATE'] = 'Tạo nhóm thư mục';

$app_strings['LBL_EMAIL_SETTINGS_GROUP_FOLDERS_EDIT'] = 'Chỉnh sửa nhóm thư mục';

$app_strings['LBL_EMAIL_SETTINGS_NAME'] = 'Tên tài khoản thư';

$app_strings['LBL_EMAIL_SETTINGS_REQUIRE_REFRESH'] = 'Chọn số lượng các email mỗi trang trong hộp thư đến. Thiết đặt này có thể cần làm mới trang để có hiệu lực.';

$app_strings['LBL_EMAIL_SETTINGS_RETRIEVING_ACCOUNT'] = 'Đang truy xuất tài khoản thư';

$app_strings['LBL_EMAIL_SETTINGS_SAVED'] = 'Các cài đặt đã được lưu.';

$app_strings['LBL_EMAIL_SETTINGS_SEND_EMAIL_AS'] = 'Chỉ gửi email văn bản thuần tuý';

$app_strings['LBL_EMAIL_SETTINGS_SHOW_NUM_IN_LIST'] = 'Emails mỗi trang';

$app_strings['LBL_EMAIL_SETTINGS_TITLE_LAYOUT'] = 'Cài đặt hình ảnh';

$app_strings['LBL_EMAIL_SETTINGS_TITLE_PREFERENCES'] = 'Quyền ưu tiên';

$app_strings['LBL_EMAIL_SETTINGS_USER_FOLDERS'] = 'Người dùng có các thư mục';

$app_strings['LBL_EMAIL_ERROR_PREPEND'] = 'Đã có lỗi email:';

$app_strings['LBL_EMAIL_INVALID_PERSONAL_OUTBOUND'] = 'Các máy chủ thư đi chọn cho tài khoản e-mail mà bạn đang sử dụng không hợp lệ. Kiểm tra các thiết lập hoặc lựa chọn một máy chủ thư khác cho tài khoản thư.';

$app_strings['LBL_EMAIL_INVALID_SYSTEM_OUTBOUND'] = 'Một máy chủ thư đi không được cấu hình để gửi email. Xin vui lòng đặt cấu hình một máy chủ thư đi hoặc chọn một máy chủ thư đi cho tài khoản email mà bạn đang sử dụng trong cài đặt >> tài khoản thư.';

$app_strings['LBL_DEFAULT_EMAIL_SIGNATURES'] = 'Chữ ký mặc định';

$app_strings['LBL_EMAIL_SIGNATURES'] = 'Chữ ký';

$app_strings['LBL_SMTPTYPE_GMAIL'] = 'Gmail';

$app_strings['LBL_SMTPTYPE_YAHOO'] = 'Yahoo! Mail';

$app_strings['LBL_SMTPTYPE_EXCHANGE'] = 'Microsoft Exchange';

$app_strings['LBL_SMTPTYPE_OTHER'] = 'Khác';

$app_strings['LBL_EMAIL_SPACER_MAIL_SERVER'] = '[Thư mục từ xa]';

$app_strings['LBL_EMAIL_SPACER_LOCAL_FOLDER'] = '[Thư mục SuiteCRM]';

$app_strings['LBL_EMAIL_SUBJECT'] = 'Chủ đề';

$app_strings['LBL_EMAIL_SUCCESS'] = 'Thành công';

$app_strings['LBL_EMAIL_SUITE_FOLDER'] = 'Thư mục SuiteCRM';

$app_strings['LBL_EMAIL_TEMPLATE_EDIT_PLAIN_TEXT'] = 'Nội dung mẫu thư điện tử trống rỗng';

$app_strings['LBL_EMAIL_TEMPLATES'] = 'Mẫu thiết kế';

$app_strings['LBL_EMAIL_TO'] = 'A';

$app_strings['LBL_EMAIL_VIEW'] = 'Xem';

$app_strings['LBL_EMAIL_VIEW_HEADERS'] = 'Hiển thị tiêu đề';

$app_strings['LBL_EMAIL_VIEW_RAW'] = 'Màn hình hiển thị nguyên Email';

$app_strings['LBL_EMAIL_VIEW_UNSUPPORTED'] = 'Tính năng này không được hỗ trợ khi sử dụng POP3.';

$app_strings['LBL_DEFAULT_LINK_TEXT'] = 'Các văn bản liên kết mặc định.';

$app_strings['LBL_EMAIL_YES'] = 'Có';

$app_strings['LBL_EMAIL_TEST_OUTBOUND_SETTINGS'] = 'Gửi Email kiểm tra';

$app_strings['LBL_EMAIL_TEST_OUTBOUND_SETTINGS_SENT'] = 'Kiểm tra Email đã gửi';

$app_strings['LBL_EMAIL_MESSAGE_NO'] = 'Tin nhắn số.';

$app_strings['LBL_EMAIL_IMPORT_SUCCESS'] = 'Nhập khẩu đã thông qua';

$app_strings['LBL_EMAIL_IMPORT_FAIL'] = 'Nhập thất bại vì tin nhắn đã được chuyển nhập hoặc đã bị xóa từ máy chủ';

$app_strings['LBL_LINK_NONE'] = 'Không có';

$app_strings['LBL_LINK_ALL'] = 'Tất cả';

$app_strings['LBL_LINK_RECORDS'] = 'Hồ sơ';

$app_strings['LBL_LINK_SELECT'] = 'Chọn';

$app_strings['LBL_LINK_ACTIONS'] = 'Hành động';

$app_strings['LBL_CLOSE_ACTIVITY_HEADER'] = 'Xác nhận';

$app_strings['LBL_CLOSE_ACTIVITY_CONFIRM'] = 'Bạn có muốn đóng này #module #?';

$app_strings['LBL_INVALID_FILE_EXTENSION'] = 'Phần mở rộng tệp không hợp lệ';

$app_strings['ERR_AJAX_LOAD'] = 'Một lỗi đã xảy ra:';

$app_strings['ERR_AJAX_LOAD_FAILURE'] = 'Đã có lỗi khi xử lý yêu cầu của bạn, hãy thử một lần sau.';

$app_strings['ERR_AJAX_LOAD_FOOTER'] = 'Nếu lỗi này vẫn còn, xin vui lòng có người quản trị vô hiệu hóa Ajax cho mô-đun này';

$app_strings['ERR_DECIMAL_SEP_EQ_THOUSANDS_SEP'] = 'Dấu phân cách thập phân không thể sử dụng các ký tự tương tự như hàng ngàn separator.\\n\\n xin vui lòng thay đổi các giá trị.';

$app_strings['ERR_DELETE_RECORD'] = 'Một số hồ sơ phải được xác định để xóa liên lạc.';

$app_strings['ERR_EXPORT_DISABLED'] = 'Vô hiệu hóa xuất.';

$app_strings['ERR_EXPORT_TYPE'] = 'Lỗi xuất ';

$app_strings['ERR_INVALID_EMAIL_ADDRESS'] = 'không phải là một địa chỉ email hợp lệ.';

$app_strings['ERR_INVALID_FILE_REFERENCE'] = 'Tham chiếu tệp không hợp lệ';

$app_strings['ERR_NO_HEADER_ID'] = 'Tính năng này không được cung cấp trong phiên bản này.';

$app_strings['ERR_NOT_ADMIN'] = 'Không được phép truy cập vào quản trị.';

$app_strings['ERR_MISSING_REQUIRED_FIELDS'] = 'Thiếu trường bắt buộc:';

$app_strings['ERR_INVALID_REQUIRED_FIELDS'] = 'Trường bắt buộc không hợp lệ:';

$app_strings['ERR_INVALID_VALUE'] = 'Giá trị không hợp lệ:';

$app_strings['ERR_NO_SUCH_FILE'] = 'Tệp không tồn tại trên hệ thống';

$app_strings['ERR_NO_SINGLE_QUOTE'] = 'Không thể sử dụng dấu ngoặc đơn cho ';

$app_strings['ERR_NOTHING_SELECTED'] = 'Xin vui lòng thực hiện một sự lựa chọn trước khi tiếp tục.';

$app_strings['ERR_SELF_REPORTING'] = 'Người dùng không thể thông báo cho anh ta hoặc mình.';

$app_strings['ERR_SQS_NO_MATCH_FIELD'] = 'Không phù hợp cho lĩnh vực: ';

$app_strings['ERR_SQS_NO_MATCH'] = 'Không trùng khớp';

$app_strings['ERR_ADDRESS_KEY_NOT_SPECIFIED'] = 'Vui lòng chỉ định chỉ mục \'key\' trong thuộc tính Params hiển thị cho định nghĩa Meta-Data';

$app_strings['ERR_EXISTING_PORTAL_USERNAME'] = 'Lỗi: Tên cổng thông tin đã được đặt cho số liên lạc khác.';

$app_strings['ERR_COMPATIBLE_PRECISION_VALUE'] = 'Giá trị lĩnh vực không tương thích với giá trị chính xác';

$app_strings['ERR_EXTERNAL_API_SAVE_FAIL'] = 'Có lỗi khi cố gắng để lưu trữ vào tài khoản bên ngoài.';

$app_strings['ERR_NO_DB'] = 'Không thể kết nối cơ sở dữ liệu. Xin vui lòng tham khảo suitecrm. xem log để biết chi tiết.';

$app_strings['ERR_DB_FAIL'] = 'Cơ sở dữ liệu lỗi. Xin vui lòng tham khảo suitecrm. xem log để biết chi tiết.';

$app_strings['ERR_DB_VERSION'] = 'SuiteCRM {0} tập tin chỉ có thể được sử dụng với một cơ sở dữ liệu {1} SuiteCRM.';

$app_strings['LBL_ACCOUNT'] = 'Tài khoản';

$app_strings['LBL_ACCOUNTS'] = 'Khách hàng';

$app_strings['LBL_ACTIVITIES_SUBPANEL_TITLE'] = 'tài khoản';

$app_strings['LBL_ACCUMULATED_HISTORY_BUTTON_KEY'] = 'H';

$app_strings['LBL_ACCUMULATED_HISTORY_BUTTON_LABEL'] = 'Xem Tổng kết';

$app_strings['LBL_ACCUMULATED_HISTORY_BUTTON_TITLE'] = 'Xem Tổng kết';

$app_strings['LBL_ADD_BUTTON'] = 'Thêm';

$app_strings['LBL_ADD_DOCUMENT'] = 'Thêm Tài liệu';

$app_strings['LBL_ADD_TO_PROSPECT_LIST_BUTTON_KEY'] = 'L';

$app_strings['LBL_ADD_TO_PROSPECT_LIST_BUTTON_LABEL'] = 'Thêm vào mục tiêu';

$app_strings['LBL_ADD_TO_PROSPECT_LIST_BUTTON_LABEL_ACCOUNTS_CONTACTS'] = 'Thêm địa chỉ liên lạc vào danh sách mục tiêu';

$app_strings['LBL_ADDITIONAL_DETAILS_CLOSE_TITLE'] = 'Bấm vào để đóng';

$app_strings['LBL_ADDITIONAL_DETAILS'] = 'Chi tiết thêm';

$app_strings['LBL_ADMIN'] = 'Quản trị viên';

$app_strings['LBL_ALT_HOT_KEY'] = 'Alt+';

$app_strings['LBL_ARCHIVE'] = 'Lưu trữ';

$app_strings['LBL_ASSIGNED_TO_USER'] = 'Giao cho người dùng';

$app_strings['LBL_ASSIGNED_TO'] = 'Giao cho:';

$app_strings['LBL_BACK'] = 'Quay lại';

$app_strings['LBL_BILLING_ADDRESS'] = 'Địa chỉ thanh toán';

$app_strings['LBL_QUICK_CREATE'] = 'Tạo ';

$app_strings['LBL_BROWSER_TITLE'] = 'SuiteCRM - mã nguồn mở CRM';

$app_strings['LBL_BUGS'] = 'Lỗi';

$app_strings['LBL_BY'] = 'bởi';

$app_strings['LBL_CALLS'] = 'Cuộc gọi';

$app_strings['LBL_CAMPAIGNS_SEND_QUEUED'] = 'Gửi đã xếp hàng chiến dịch email';

$app_strings['LBL_SUBMIT_BUTTON_LABEL'] = 'Gửi';

$app_strings['LBL_CASE'] = 'Kịch bản';

$app_strings['LBL_CASES'] = 'Vụ việc';

$app_strings['LBL_CHANGE_PASSWORD'] = 'Đổi mật khẩu';

$app_strings['LBL_CHARSET'] = 'UTF-8';

$app_strings['LBL_CHECKALL'] = 'Chọn tất cả';

$app_strings['LBL_CITY'] = 'Thành phố';

$app_strings['LBL_CLEAR_BUTTON_LABEL'] = 'Xóa';

$app_strings['LBL_CLEAR_BUTTON_TITLE'] = 'Xóa';

$app_strings['LBL_CLEARALL'] = 'Xóa hết';

$app_strings['LBL_CLOSE_BUTTON_TITLE'] = 'Đóng';

$app_strings['LBL_CLOSE_AND_CREATE_BUTTON_LABEL'] = 'Đóng và tạo mới';

$app_strings['LBL_CLOSE_AND_CREATE_BUTTON_TITLE'] = 'Đóng và tạo mới';

$app_strings['LBL_CLOSE_AND_CREATE_BUTTON_KEY'] = 'C';

$app_strings['LBL_OPEN_ITEMS'] = 'Mở mục:';

$app_strings['LBL_COMPOSE_EMAIL_BUTTON_KEY'] = 'L';

$app_strings['LBL_COMPOSE_EMAIL_BUTTON_LABEL'] = 'Soạn thư';

$app_strings['LBL_COMPOSE_EMAIL_BUTTON_TITLE'] = 'Soạn thư';

$app_strings['LBL_SEARCH_DROPDOWN_YES'] = 'Có';

$app_strings['LBL_SEARCH_DROPDOWN_NO'] = 'Không';

$app_strings['LBL_CONTACT_LIST'] = 'Danh sách liên hệ';

$app_strings['LBL_CONTACT'] = 'Liên hệ';

$app_strings['LBL_CONTACTS'] = 'Liên hệ';

$app_strings['LBL_CONTRACT'] = 'Hợp đồng';

$app_strings['LBL_CONTRACTS'] = 'Hợp đồng';

$app_strings['LBL_COUNTRY'] = 'Quốc gia:';

$app_strings['LBL_CREATE_BUTTON_LABEL'] = 'Tạo';

$app_strings['LBL_CREATED_BY_USER'] = 'Tạo bởi người dùng';

$app_strings['LBL_CREATED_USER'] = 'Tạo bởi người dùng';

$app_strings['LBL_CREATED'] = 'Được tạo bởi';

$app_strings['LBL_CURRENT_USER_FILTER'] = 'Dữ liệu của tôi:';

$app_strings['LBL_CURRENCY'] = 'Tiền tệ:';

$app_strings['LBL_DOCUMENTS'] = 'Tài liệu';

$app_strings['LBL_DATE_ENTERED'] = 'Ngày tạo:';

$app_strings['LBL_DATE_MODIFIED'] = 'Ngày chỉnh sửa:';

$app_strings['LBL_EDIT_BUTTON'] = 'Sửa';

$app_strings['LBL_DUPLICATE_BUTTON'] = 'Trùng lặp';

$app_strings['LBL_DELETE_BUTTON'] = 'Xóa';

$app_strings['LBL_DELETE'] = 'Xóa';

$app_strings['LBL_DELETED'] = 'Đã xóa';

$app_strings['LBL_DIRECT_REPORTS'] = 'Báo cáo trực tiếp';

$app_strings['LBL_DONE_BUTTON_LABEL'] = 'Xong';

$app_strings['LBL_DONE_BUTTON_TITLE'] = 'Xong';

$app_strings['LBL_FAVORITES'] = 'yêu thích';

$app_strings['LBL_VCARD'] = 'vCard';

$app_strings['LBL_EMPTY_VCARD'] = 'Xin chọn tập tin Vcard';

$app_strings['LBL_EMPTY_REQUIRED_VCARD'] = 'vCard không có tất cả các trường bắt buộc cho các mô-đun này. Xin vui lòng tham khảo suitecrm.log để biết chi tiết.';

$app_strings['LBL_VCARD_ERROR_FILESIZE'] = 'Các tập tin đã tải lên vượt quá giới hạn kích thước 30000 byte được chỉ ra trong các hình thức HTML.';

$app_strings['LBL_VCARD_ERROR_DEFAULT'] = 'Đã có lỗi trong khi tải lên tập tin vCard. Xin vui lòng tham khảo suitecrm.log để biết chi tiết.';

$app_strings['LBL_IMPORT_VCARD'] = 'Nhập vCard:';

$app_strings['LBL_IMPORT_VCARD_BUTTON_LABEL'] = 'Nhập vCard';

$app_strings['LBL_IMPORT_VCARD_BUTTON_TITLE'] = 'Nhập vCard';

$app_strings['LBL_VIEW_BUTTON'] = 'Xem';

$app_strings['LBL_EMAIL_PDF_BUTTON_LABEL'] = 'Email dưới dạng PDF';

$app_strings['LBL_EMAIL_PDF_BUTTON_TITLE'] = 'Email dưới dạng PDF';

$app_strings['LBL_EMAILS'] = 'Emails';

$app_strings['LBL_EMPLOYEES'] = 'Nhân viên';

$app_strings['LBL_ENTER_DATE'] = 'Nhập vào ngày';

$app_strings['LBL_EXPORT'] = 'Xuất ra';

$app_strings['LBL_FAVORITES_FILTER'] = 'Yêu thích của tôi:';

$app_strings['LBL_GO_BUTTON_LABEL'] = 'Đi';

$app_strings['LBL_HIDE'] = 'Ẩn';

$app_strings['LBL_ID'] = 'ID';

$app_strings['LBL_IMPORT'] = 'Nhập';

$app_strings['LBL_IMPORT_STARTED'] = 'Bắt đầu nhập: ';

$app_strings['LBL_LAST_VIEWED'] = 'Vừa xem qua';

$app_strings['LBL_LEADS'] = 'Đầu mối';

$app_strings['LBL_LESS'] = 'ít hơn';

$app_strings['LBL_CAMPAIGN'] = 'Chiến dịch:';

$app_strings['LBL_CAMPAIGNS'] = 'Chiến dịch';

$app_strings['LBL_CAMPAIGNLOG'] = 'Log Chiến dịch';

$app_strings['LBL_CAMPAIGN_CONTACT'] = 'Chiến dịch';

$app_strings['LBL_CAMPAIGN_ID'] = 'campaign_id';

$app_strings['LBL_CAMPAIGN_NONE'] = 'Không có';

$app_strings['LBL_THEME'] = 'Giao diện:';

$app_strings['LBL_FOUND_IN_RELEASE'] = 'Tìm thấy trong bản phát hành';

$app_strings['LBL_FIXED_IN_RELEASE'] = 'Cố định trong bản phát hành';

$app_strings['LBL_LIST_ACCOUNT_NAME'] = 'Tên tài khoản';

$app_strings['LBL_LIST_ASSIGNED_USER'] = 'Người dùng';

$app_strings['LBL_LIST_CONTACT_NAME'] = 'Tên liên lạc';

$app_strings['LBL_LIST_CONTACT_ROLE'] = 'Liên hệ với vai trò';

$app_strings['LBL_LIST_DATE_ENTERED'] = 'Ngày tạo';

$app_strings['LBL_LIST_EMAIL'] = 'Email';

$app_strings['LBL_LIST_NAME'] = 'Tên';

$app_strings['LBL_LIST_OF'] = 'của';

$app_strings['LBL_LIST_PHONE'] = 'Điện thoại';

$app_strings['LBL_LIST_RELATED_TO'] = 'Liên quan đến';

$app_strings['LBL_LIST_USER_NAME'] = 'Tên người dùng';

$app_strings['LBL_LISTVIEW_NO_SELECTED'] = 'Vui lòng chọn ít nhất 1 hồ sơ để tiến hành.';

$app_strings['LBL_LISTVIEW_TWO_REQUIRED'] = 'Vui lòng chọn ít nhất 2 hồ sơ để tiến hành.';

$app_strings['LBL_LISTVIEW_OPTION_SELECTED'] = 'Đã chọn';

$app_strings['LBL_LISTVIEW_SELECTED_OBJECTS'] = 'Đã chọn: ';

$app_strings['LBL_LOCALE_NAME_EXAMPLE_FIRST'] = 'David';

$app_strings['LBL_LOCALE_NAME_EXAMPLE_LAST'] = 'Livingstone';

$app_strings['LBL_LOCALE_NAME_EXAMPLE_SALUTATION'] = 'Tiến sĩ.';

$app_strings['LBL_LOCALE_NAME_EXAMPLE_TITLE'] = 'Code Monkey Extraordinaire';

$app_strings['LBL_CANCEL'] = 'Hủy';

$app_strings['LBL_VERIFY'] = 'Xác thực';

$app_strings['LBL_RESEND'] = 'Gửi lại';

$app_strings['LBL_PROFILE'] = 'Hồ sơ';

$app_strings['LBL_MAILMERGE'] = 'Mail Merge';

$app_strings['LBL_MASS_UPDATE'] = 'Mass Update';

$app_strings['LBL_NO_MASS_UPDATE_FIELDS_AVAILABLE'] = 'Không có lĩnh vực sẵn sàng cho hoạt động cập nhật khối lượng';

$app_strings['LBL_OPT_OUT_FLAG_PRIMARY'] = 'Opt out Email chính';

$app_strings['LBL_OPT_IN_FLAG_PRIMARY'] = 'Chọn Email tham gia chính';

$app_strings['LBL_MEETINGS'] = 'Hội họp';

$app_strings['LBL_MEETING_GO_BACK'] = 'Quay trở lại cuộc họp';

$app_strings['LBL_MEMBERS'] = 'Thành viên';

$app_strings['LBL_MEMBER_OF'] = 'Thành viên của';

$app_strings['LBL_MODIFIED_BY_USER'] = 'Sửa bởi người dùng';

$app_strings['LBL_MODIFIED_USER'] = 'Sửa bởi người dùng';

$app_strings['LBL_MODIFIED'] = 'Được sửa bởi';

$app_strings['LBL_MODIFIED_NAME'] = 'Chỉnh sửa bởi Tên';

$app_strings['LBL_MORE'] = 'Nhiều hơn';

$app_strings['LBL_MY_ACCOUNT'] = 'Thiết lập của tôi';

$app_strings['LBL_NAME'] = 'Tên';

$app_strings['LBL_NEW_BUTTON_KEY'] = 'N';

$app_strings['LBL_NEW_BUTTON_LABEL'] = 'Tạo';

$app_strings['LBL_NEW_BUTTON_TITLE'] = 'Tạo';

$app_strings['LBL_NEXT_BUTTON_LABEL'] = 'Tiếp theo';

$app_strings['LBL_NONE'] = '--Không--';

$app_strings['LBL_NOTES'] = 'Ghi chú';

$app_strings['LBL_OPPORTUNITIES'] = 'Cơ hội';

$app_strings['LBL_OPPORTUNITY_NAME'] = 'Tên Cơ hội';

$app_strings['LBL_OPPORTUNITY'] = 'Cơ hội';

$app_strings['LBL_OR'] = 'Hoặc';

$app_strings['LBL_PANEL_OVERVIEW'] = 'Xem toàn bộ';

$app_strings['LBL_PANEL_ASSIGNMENT'] = 'Khác';

$app_strings['LBL_PANEL_ADVANCED'] = 'Thông tin thêm';

$app_strings['LBL_PARENT_TYPE'] = 'Loại Cấp trên';

$app_strings['LBL_PERCENTAGE_SYMBOL'] = '%';

$app_strings['LBL_POSTAL_CODE'] = 'Mã bưu chính:';

$app_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Tên thành phố chính:';

$app_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'Tên quốc gia chính:';

$app_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'Mã bưu chính:';

$app_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Tên tiểu bang chính:';

$app_strings['LBL_PRIMARY_ADDRESS_STREET_2'] = 'Địa chỉ 2:';

$app_strings['LBL_PRIMARY_ADDRESS_STREET_3'] = 'Địa chỉ 3:';

$app_strings['LBL_PRIMARY_ADDRESS_STREET'] = 'Địa chỉ:';

$app_strings['LBL_PRIMARY_ADDRESS'] = 'Địa chỉ chính:';

$app_strings['LBL_PROSPECTS'] = 'Triển vọng';

$app_strings['LBL_PRODUCTS'] = 'Sản phẩm';

$app_strings['LBL_PROJECT_TASKS'] = 'Dự án những phần việc';

$app_strings['LBL_PROJECTS'] = 'Dự án';

$app_strings['LBL_QUOTES'] = 'Báo giá';

$app_strings['LBL_RELATED'] = 'Liên quan';

$app_strings['LBL_RELATED_RECORDS'] = 'Liên quan đến hồ sơ';

$app_strings['LBL_REMOVE'] = 'Xóa bỏ';

$app_strings['LBL_REPORTS_TO'] = 'Báo cáo cho';

$app_strings['LBL_REQUIRED_SYMBOL'] = '*';

$app_strings['LBL_REQUIRED_TITLE'] = 'Chỉ ra trường bắt buộc';

$app_strings['LBL_EMAIL_DONE_BUTTON_LABEL'] = 'Xong';

$app_strings['LBL_FULL_FORM_BUTTON_KEY'] = 'L';

$app_strings['LBL_FULL_FORM_BUTTON_LABEL'] = 'Dạng đầy đủ';

$app_strings['LBL_FULL_FORM_BUTTON_TITLE'] = 'Dạng đầy đủ';

$app_strings['LBL_SAVE_NEW_BUTTON_LABEL'] = 'Lưu & tạo mới';

$app_strings['LBL_SAVE_NEW_BUTTON_TITLE'] = 'Lưu & tạo mới';

$app_strings['LBL_SAVE_OBJECT'] = 'Lưu {0}';

$app_strings['LBL_SEARCH_BUTTON_KEY'] = 'Q';

$app_strings['LBL_SEARCH_BUTTON_LABEL'] = 'Tìm kiếm';

$app_strings['LBL_SEARCH_BUTTON_TITLE'] = 'Tìm kiếm';

$app_strings['LBL_FILTER'] = 'Bộ lọc';

$app_strings['LBL_SEARCH'] = 'Tìm kiếm';

$app_strings['LBL_SEARCH_ALT'] = '';

$app_strings['LBL_SEARCH_MORE'] = 'thêm';

$app_strings['LBL_UPLOAD_IMAGE_FILE_INVALID'] = 'Định dạng tệp không hợp lệ, chỉ có tập tin hình ảnh có thể được tải lên.';

$app_strings['LBL_SELECT_BUTTON_KEY'] = 'Mục tiêu';

$app_strings['LBL_SELECT_BUTTON_LABEL'] = 'Chọn';

$app_strings['LBL_SELECT_BUTTON_TITLE'] = 'Chọn';

$app_strings['LBL_BROWSE_DOCUMENTS_BUTTON_LABEL'] = 'Duyệt tài liệu';

$app_strings['LBL_BROWSE_DOCUMENTS_BUTTON_TITLE'] = 'Duyệt tài liệu';

$app_strings['LBL_SELECT_CONTACT_BUTTON_KEY'] = 'Mục tiêu';

$app_strings['LBL_SELECT_CONTACT_BUTTON_LABEL'] = 'Chọn liên lạc';

$app_strings['LBL_SELECT_CONTACT_BUTTON_TITLE'] = 'Chọn liên lạc';

$app_strings['LBL_SELECT_REPORTS_BUTTON_LABEL'] = 'CHỌN MẪU báo cáo';

$app_strings['LBL_SELECT_REPORTS_BUTTON_TITLE'] = 'Chọn báo cáo';

$app_strings['LBL_SELECT_USER_BUTTON_KEY'] = 'U';

$app_strings['LBL_SELECT_USER_BUTTON_LABEL'] = 'Chọn người dùng';

$app_strings['LBL_SELECT_USER_BUTTON_TITLE'] = 'Chọn người dùng';

$app_strings['LBL_ACCESSKEY_CLEAR_RELATE_KEY'] = ' ';

$app_strings['LBL_ACCESSKEY_CLEAR_RELATE_TITLE'] = 'Xóa lựa chọn';

$app_strings['LBL_ACCESSKEY_CLEAR_RELATE_LABEL'] = 'Xóa lựa chọn';

$app_strings['LBL_ACCESSKEY_CLEAR_COLLECTION_KEY'] = ' ';

$app_strings['LBL_ACCESSKEY_CLEAR_COLLECTION_TITLE'] = 'Xóa lựa chọn';

$app_strings['LBL_ACCESSKEY_CLEAR_COLLECTION_LABEL'] = 'Xóa lựa chọn';

$app_strings['LBL_ACCESSKEY_SELECT_FILE_KEY'] = 'F';

$app_strings['LBL_ACCESSKEY_SELECT_FILE_TITLE'] = 'Chọn File';

$app_strings['LBL_ACCESSKEY_SELECT_FILE_LABEL'] = 'Chọn File';

$app_strings['LBL_ACCESSKEY_CLEAR_FILE_KEY'] = ' ';

$app_strings['LBL_ACCESSKEY_CLEAR_FILE_TITLE'] = 'Xóa tập tin';

$app_strings['LBL_ACCESSKEY_CLEAR_FILE_LABEL'] = 'Xóa tập tin';

$app_strings['LBL_ACCESSKEY_SELECT_USERS_KEY'] = 'U';

$app_strings['LBL_ACCESSKEY_SELECT_USERS_TITLE'] = 'Chọn người dùng';

$app_strings['LBL_ACCESSKEY_SELECT_USERS_LABEL'] = 'Chọn người dùng';

$app_strings['LBL_ACCESSKEY_CLEAR_USERS_KEY'] = ' ';

$app_strings['LBL_ACCESSKEY_CLEAR_USERS_TITLE'] = 'Xóa người dùng';

$app_strings['LBL_ACCESSKEY_CLEAR_USERS_LABEL'] = 'Xóa người dùng';

$app_strings['LBL_ACCESSKEY_SELECT_ACCOUNTS_KEY'] = 'A';

$app_strings['LBL_ACCESSKEY_SELECT_ACCOUNTS_TITLE'] = 'Chọn Khách hàng';

$app_strings['LBL_ACCESSKEY_SELECT_ACCOUNTS_LABEL'] = 'Chọn Khách hàng';

$app_strings['LBL_ACCESSKEY_CLEAR_ACCOUNTS_KEY'] = ' ';

$app_strings['LBL_ACCESSKEY_CLEAR_ACCOUNTS_TITLE'] = 'Xóa tài khoản';

$app_strings['LBL_ACCESSKEY_CLEAR_ACCOUNTS_LABEL'] = 'Xóa tài khoản';

$app_strings['LBL_ACCESSKEY_SELECT_CAMPAIGNS_KEY'] = 'M';

$app_strings['LBL_ACCESSKEY_SELECT_CAMPAIGNS_TITLE'] = 'Chọn chiến dịch';

$app_strings['LBL_ACCESSKEY_SELECT_CAMPAIGNS_LABEL'] = 'Chọn chiến dịch';

$app_strings['LBL_ACCESSKEY_CLEAR_CAMPAIGNS_KEY'] = ' ';

$app_strings['LBL_ACCESSKEY_CLEAR_CAMPAIGNS_TITLE'] = 'Xóa chiến dịch';

$app_strings['LBL_ACCESSKEY_CLEAR_CAMPAIGNS_LABEL'] = 'Xóa chiến dịch';

$app_strings['LBL_ACCESSKEY_SELECT_CONTACTS_KEY'] = 'C';

$app_strings['LBL_ACCESSKEY_SELECT_CONTACTS_TITLE'] = 'Chọn liên lạc';

$app_strings['LBL_ACCESSKEY_SELECT_CONTACTS_LABEL'] = 'Chọn liên lạc';

$app_strings['LBL_ACCESSKEY_CLEAR_CONTACTS_KEY'] = ' ';

$app_strings['LBL_ACCESSKEY_CLEAR_CONTACTS_TITLE'] = 'Xóa liên hệ';

$app_strings['LBL_ACCESSKEY_CLEAR_CONTACTS_LABEL'] = 'Xóa liên hệ';

$app_strings['LBL_ACCESSKEY_SELECT_TEAMSET_KEY'] = 'Z';

$app_strings['LBL_ACCESSKEY_SELECT_TEAMSET_TITLE'] = 'Chọn nhóm';

$app_strings['LBL_ACCESSKEY_SELECT_TEAMSET_LABEL'] = 'Chọn nhóm';

$app_strings['LBL_ACCESSKEY_CLEAR_TEAMS_KEY'] = ' ';

$app_strings['LBL_ACCESSKEY_CLEAR_TEAMS_TITLE'] = 'Xóa nhóm';

$app_strings['LBL_ACCESSKEY_CLEAR_TEAMS_LABEL'] = 'Xóa nhóm';

$app_strings['LBL_SERVER_RESPONSE_RESOURCES'] = 'Các nguồn tài nguyên được sử dụng để xây dựng trang này (truy vấn, tập tin)';

$app_strings['LBL_SERVER_RESPONSE_TIME_SECONDS'] = 'giây.';

$app_strings['LBL_SERVER_RESPONSE_TIME'] = 'Thời gian máy chủ phản hồi:';

$app_strings['LBL_SERVER_MEMORY_BYTES'] = 'bytes';

$app_strings['LBL_SERVER_MEMORY_USAGE'] = 'Sử dụng bộ nhớ máy chủ: {0} ({1})';

$app_strings['LBL_SERVER_MEMORY_LOG_MESSAGE'] = 'Sử dụng:-mô-đun: {0} - hành động: {1}';

$app_strings['LBL_SERVER_PEAK_MEMORY_USAGE'] = 'Sử dụng bộ nhớ máy tối đa: {0} ({1})';

$app_strings['LBL_SHIPPING_ADDRESS'] = 'Địa chỉ giao hàng';

$app_strings['LBL_SHOW'] = 'Hiển thị';

$app_strings['LBL_STATE'] = 'Tiểu bang:';

$app_strings['LBL_STATUS_UPDATED'] = 'Trạng thái của bạn cho sự kiện này đã được Cập Nhật!';

$app_strings['LBL_STATUS'] = 'Tình trạng:';

$app_strings['LBL_STREET'] = 'Địa chỉ';

$app_strings['LBL_SUBJECT'] = 'Chủ đề';

$app_strings['LBL_INBOUNDEMAIL_ID'] = 'Gửi đến Email ID';

$app_strings['LBL_SCENARIO_SALES'] = 'Bán hàng';

$app_strings['LBL_SCENARIO_MARKETING'] = 'Tiếp thị';

$app_strings['LBL_SCENARIO_FINANCE'] = 'Tài chính';

$app_strings['LBL_SCENARIO_SERVICE'] = 'Dịch vụ';

$app_strings['LBL_SCENARIO_PROJECT'] = 'Quản lý dự án';

$app_strings['LBL_SCENARIO_SALES_DESCRIPTION'] = 'Kịch bản này tạo thuận lợi cho việc quản lý các mặt hàng kinh doanh';

$app_strings['LBL_SCENARIO_MAKETING_DESCRIPTION'] = 'Kịch bản này tạo điều kiện cho việc quản lý các mặt hàng tiếp thị';

$app_strings['LBL_SCENARIO_FINANCE_DESCRIPTION'] = 'Kịch bản này tạo thuận lợi cho việc quản lý các mặt hàng liên quan đến tài chính';

$app_strings['LBL_SCENARIO_SERVICE_DESCRIPTION'] = 'Kịch bản này tạo điều kiện cho việc quản lý các mặt hàng liên quan đến dịch vụ';

$app_strings['LBL_SCENARIO_PROJECT_DESCRIPTION'] = 'Kịch bản này tạo thuận lợi cho việc quản lý các hạng mục liên quan đến dự án';

$app_strings['LBL_SYNC'] = 'Đồng phòng ban';

$app_strings['LBL_TABGROUP_ALL'] = 'Tất cả';

$app_strings['LBL_TABGROUP_ACTIVITIES'] = 'Hoạt động';

$app_strings['LBL_TABGROUP_COLLABORATION'] = 'Cộng tác';

$app_strings['LBL_TABGROUP_MARKETING'] = 'Marketing';

$app_strings['LBL_TABGROUP_OTHER'] = 'Khác';

$app_strings['LBL_TABGROUP_SALES'] = 'Bán hàng';

$app_strings['LBL_TABGROUP_SUPPORT'] = 'Hỗ trợ';

$app_strings['LBL_TASKS'] = 'Công việc';

$app_strings['LBL_THOUSANDS_SYMBOL'] = 'K';

$app_strings['LBL_TRACK_EMAIL_BUTTON_LABEL'] = 'Lưu trữ Email';

$app_strings['LBL_TRACK_EMAIL_BUTTON_TITLE'] = 'Lưu trữ Email';

$app_strings['LBL_UNDELETE_BUTTON_LABEL'] = 'Bỏ xoá';

$app_strings['LBL_UNDELETE_BUTTON_TITLE'] = 'Bỏ xoá';

$app_strings['LBL_UNDELETE_BUTTON'] = 'Bỏ xoá';

$app_strings['LBL_UNDELETE'] = 'Bỏ xoá';

$app_strings['LBL_UNSYNC'] = 'Không đồng bộ';

$app_strings['LBL_UPDATE'] = 'Cập nhật';

$app_strings['LBL_USER_LIST'] = 'Danh sách người dùng';

$app_strings['LBL_USERS'] = 'Người dùng';

$app_strings['LBL_VERIFY_EMAIL_ADDRESS'] = 'Kiểm tra các mục hiện có email...';

$app_strings['LBL_VERIFY_PORTAL_NAME'] = 'Kiểm tra tên cổng hiện tại...';

$app_strings['LBL_VIEW_IMAGE'] = 'xem';

$app_strings['LNK_ABOUT'] = 'Về';

$app_strings['LNK_ADVANCED_FILTER'] = 'Bộ lọc nâng cao';

$app_strings['LNK_BASIC_FILTER'] = 'Lọc nhanh';

$app_strings['LBL_ADVANCED_SEARCH'] = 'Bộ lọc nâng cao';

$app_strings['LBL_QUICK_FILTER'] = 'Lọc nhanh';

$app_strings['LNK_SEARCH_NONFTS_VIEW_ALL'] = 'Hiển thị tất cả';

$app_strings['LNK_CLOSE'] = 'Đóng';

$app_strings['LBL_MODIFY_CURRENT_FILTER'] = 'Sửa đổi bộ lọc hiện tại';

$app_strings['LNK_SAVED_VIEWS'] = 'Tùy chọn bố cục';

$app_strings['LNK_DELETE'] = 'Xóa';

$app_strings['LNK_EDIT'] = 'Sửa';

$app_strings['LNK_GET_LATEST'] = 'Lấy gần đây nhất';

$app_strings['LNK_GET_LATEST_TOOLTIP'] = 'Thay thế với phiên bản gần nhất';

$app_strings['LNK_HELP'] = 'Giúp đỡ';

$app_strings['LNK_CREATE'] = 'Tạo';

$app_strings['LNK_LIST_END'] = 'Kết thúc';

$app_strings['LNK_LIST_NEXT'] = 'Tiếp theo';

$app_strings['LNK_LIST_PREVIOUS'] = 'Trước';

$app_strings['LNK_LIST_RETURN'] = 'Trở lại danh sách';

$app_strings['LNK_LIST_START'] = 'bắt đầu';

$app_strings['LNK_LOAD_SIGNED'] = 'Nhập';

$app_strings['LNK_LOAD_SIGNED_TOOLTIP'] = 'Thay thế với tài liệu nhập';

$app_strings['LNK_PRINT'] = 'In';

$app_strings['LNK_BACKTOTOP'] = 'Lên trên';

$app_strings['LNK_REMOVE'] = 'Xóa bỏ';

$app_strings['LNK_RESUME'] = 'Bắt đầu lại';

$app_strings['LNK_VIEW_CHANGE_LOG'] = 'Xem nhật ký';

$app_strings['NTC_CLICK_BACK'] = 'Xin vui lòng bấm vào nút quay lại của trình duyệt và sửa chữa các lỗi.';

$app_strings['NTC_DATE_FORMAT'] = '(yyyy-mm-dd)';

$app_strings['NTC_DELETE_CONFIRMATION_MULTIPLE'] = 'Bạn có chắc bạn muốn xóa bỏ lựa chọn bản ghi(s)?';

$app_strings['NTC_TEMPLATE_IS_USED'] = 'Các mẫu được sử dụng trong ít nhất một hồ sơ tiếp thị email. Bạn có chắc bạn muốn xóa bỏ nó?';

$app_strings['NTC_TEMPLATES_IS_USED'] = 'Các bản mẫu được sử dụng trong các bản ghi Email Marketing. Bạn có chắc bạn muốn xóa bỏ chúng không?























































































































































































';

$app_strings['NTC_DELETE_CONFIRMATION'] = 'Bạn chắc muốn xoá hoàn toàn bản ghi?';

$app_strings['NTC_DELETE_CONFIRMATION_NUM'] = 'Bạn có chắc muốn xoá nó ';

$app_strings['NTC_UPDATE_CONFIRMATION_NUM'] = 'Bạn có chắc muốn nâng cấp nó ';

$app_strings['NTC_DELETE_SELECTED_RECORDS'] = ' chọn bản ghi(s)?';

$app_strings['NTC_LOGIN_MESSAGE'] = 'Vui lòng điền tên đăng nhập và mật khẩu.';

$app_strings['NTC_NO_ITEMS_DISPLAY'] = 'không có';

$app_strings['NTC_REMOVE_CONFIRMATION'] = 'Bạn có chắc bạn muốn loại bỏ mối quan hệ này? Chỉ có các mối quan hệ sẽ được gỡ bỏ. Hồ sơ sẽ không bị xóa.';

$app_strings['NTC_REQUIRED'] = 'Chỉ ra trường bắt buộc';

$app_strings['NTC_TIME_FORMAT'] = '(24:00)';

$app_strings['NTC_WELCOME'] = 'Chào mừng';

$app_strings['NTC_YEAR_FORMAT'] = '(yyyy)';

$app_strings['WARN_UNSAVED_CHANGES'] = 'Bạn đang về để lại bản ghi này mà không lưu bất kỳ thay đổi nào bạn có thể đã ghi lại. Bạn có chắc bạn muốn rời khỏi hồ sơ này?';

$app_strings['ERROR_NO_RECORD'] = 'Lỗi lấy hồ sơ. Hồ sơ này có thể bị xoá hoặc bạn có thể không được phép xem nó.';

$app_strings['WARN_BROWSER_VERSION_WARNING'] = '<b>Cảnh báo:</b> Phiên bản trình duyệt của bạn không còn được hỗ trợ hoặc bạn đang sử dụng một trình duyệt không được hỗ trợ. <p></p> Phiên bản trình duyệt sau đây được khuyến cáo: <p></p> <ul><li>Internet Explorer 10 (tương thích xem không được hỗ trợ) <li>Firefox 32.0 <li>Safari 5.1 <li>Chrome 37</ul>';

$app_strings['WARN_BROWSER_IE_COMPATIBILITY_MODE_WARNING'] = '<b>Cảnh báo:</b> Trình duyệt của bạn là trong chế độ xem tương thích trình duyệt IE mà không được hỗ trợ.';

$app_strings['ERROR_TYPE_NOT_VALID'] = 'Lỗi. Loại hình này là không hợp lệ.';

$app_strings['ERROR_NO_BEAN'] = 'Thất bại trong việc nhận được đậu.';

$app_strings['LBL_DUP_MERGE'] = 'Tìm bản sao';

$app_strings['LBL_MANAGE_SUBSCRIPTIONS'] = 'Quản lý đăng ký';

$app_strings['LBL_MANAGE_SUBSCRIPTIONS_FOR'] = 'Quản lý đăng ký cho ';

$app_strings['LBL_LOADING'] = 'Đang tải ...';

$app_strings['LBL_SEARCHING'] = 'Đang tìm...';

$app_strings['LBL_SAVING_LAYOUT'] = 'Đang lưu ...';

$app_strings['LBL_SAVED_LAYOUT'] = 'Khung đã được lưu.';

$app_strings['LBL_SAVED'] = 'Đã lưu';

$app_strings['LBL_SAVING'] = 'Đang lưu';

$app_strings['LBL_DISPLAY_COLUMNS'] = 'Hiển thị cột';

$app_strings['LBL_HIDE_COLUMNS'] = 'Ẩn cột';

$app_strings['LBL_SEARCH_CRITERIA'] = 'Điều kiện tìm kiếm';

$app_strings['LBL_SAVED_VIEWS'] = 'Lưu View';

$app_strings['LBL_PROCESSING_REQUEST'] = 'Đang xử lý..';

$app_strings['LBL_REQUEST_PROCESSED'] = 'Xong';

$app_strings['LBL_AJAX_FAILURE'] = 'Thất bại Ajax';

$app_strings['LBL_MERGE_DUPLICATES'] = 'Hợp nhất';

$app_strings['LBL_SAVED_FILTER_SHORTCUT'] = 'Bộ lọc của tôi';

$app_strings['LBL_SEARCH_POPULATE_ONLY'] = 'Thực hiện một tìm kiếm bằng cách sử dụng mẫu tìm kiếm ở trên';

$app_strings['LBL_DETAILVIEW'] = 'Xem chi tiết';

$app_strings['LBL_LISTVIEW'] = 'Chế độ xem danh sách';

$app_strings['LBL_EDITVIEW'] = 'Chỉnh sửa xem';

$app_strings['LBL_BILLING_STREET'] = 'Đường:';

$app_strings['LBL_SHIPPING_STREET'] = 'Đường:';

$app_strings['LBL_SEARCHFORM'] = 'Biểu mẫu tìm kiếm';

$app_strings['LBL_SAVED_SEARCH_ERROR'] = 'Vui lòng cung cấp tên cho dạng xem này.';

$app_strings['LBL_DISPLAY_LOG'] = 'Hiển thị nhật ký';

$app_strings['ERROR_JS_ALERT_SYSTEM_CLASS'] = 'Hệ thống';

$app_strings['ERROR_JS_ALERT_TIMEOUT_TITLE'] = 'Thời gian chờ phiên';

$app_strings['ERROR_JS_ALERT_TIMEOUT_MSG_1'] = 'Phiên làm việc của bạn là về để thời gian chờ trong 2 phút. Xin vui lòng lưu công việc của bạn.';

$app_strings['ERROR_JS_ALERT_TIMEOUT_MSG_2'] = 'Phiên làm việc đã hết.';

$app_strings['MSG_JS_ALERT_MTG_REMINDER_AGENDA'] = '























































































































































































Agenda: ';

$app_strings['MSG_JS_ALERT_MTG_REMINDER_MEETING'] = 'Hội họp';

$app_strings['MSG_JS_ALERT_MTG_REMINDER_CALL'] = 'Cuộc gọi';

$app_strings['MSG_JS_ALERT_MTG_REMINDER_TIME'] = 'Thời gian: ';

$app_strings['MSG_JS_ALERT_MTG_REMINDER_LOC'] = 'Địa điểm: ';

$app_strings['MSG_JS_ALERT_MTG_REMINDER_DESC'] = 'Mô tả: ';

$app_strings['MSG_JS_ALERT_MTG_REMINDER_STATUS'] = 'Trạng thái: ';

$app_strings['MSG_JS_ALERT_MTG_REMINDER_RELATED_TO'] = 'Liên quan đến: ';

$app_strings['MSG_JS_ALERT_MTG_REMINDER_CALL_MSG'] = '























































































































































































Click OK để xem cuộc gọi này hoặc bấm vào Huỷ để bỏ qua thông báo này.';

$app_strings['MSG_JS_ALERT_MTG_REMINDER_MEETING_MSG'] = '























































































































































































Click OK để xem cuộc họp này hoặc bấm vào Huỷ để bỏ qua thông báo này.';

$app_strings['MSG_JS_ALERT_MTG_REMINDER_NO_EVENT_NAME'] = 'Sự kiện';

$app_strings['MSG_JS_ALERT_MTG_REMINDER_NO_DESCRIPTION'] = 'Sự kiện không phải là thiết lập.';

$app_strings['MSG_JS_ALERT_MTG_REMINDER_NO_LOCATION'] = 'Vị trí không được thiết lập.';

$app_strings['MSG_JS_ALERT_MTG_REMINDER_NO_START_DATE'] = 'Ngày bắt đầu không được xác định.';

$app_strings['MSG_LIST_VIEW_NO_RESULTS_BASIC'] = 'Không tìm thấy kết quả nào.';

$app_strings['MSG_LIST_VIEW_NO_RESULTS_CHANGE_CRITERIA'] = 'Không tìm thấy kết quả... Có lẽ thay đổi tiêu chí tìm kiếm của bạn và cố gắng một lần nữa?';

$app_strings['MSG_LIST_VIEW_NO_RESULTS'] = 'Không có kết quả tìm thấy cho <item1>';

$app_strings['MSG_LIST_VIEW_NO_RESULTS_SUBMSG'] = 'Tạo <item1>như là một <item2>mới';

$app_strings['MSG_LIST_VIEW_CHANGE_SEARCH'] = 'hoặc thay đổi tiêu chí tìm kiếm của bạn';

$app_strings['MSG_EMPTY_LIST_VIEW_NO_RESULTS'] = 'Bạn hiện không có hồ sơ lưu trữ. <item2>hoặc <item3>một bây giờ.';

$app_strings['LBL_ADD_TO_FAVORITES'] = 'Thêm vào Yêu thích';

$app_strings['LBL_CREATE_CONTACT'] = 'Tạo thông tin Liên hệ';

$app_strings['LBL_CREATE_CASE'] = 'Tạo Vụ việc';

$app_strings['LBL_CREATE_NOTE'] = 'Tạo ghi chú';

$app_strings['LBL_CREATE_OPPORTUNITY'] = 'Tạo Opportunity';

$app_strings['LBL_SCHEDULE_CALL'] = 'Log Cuộc gọi';

$app_strings['LBL_SCHEDULE_MEETING'] = 'Lịch họp';

$app_strings['LBL_CREATE_TASK'] = 'Tạo Tác vụ';

$app_strings['LBL_GENERATE_WEB_TO_LEAD_FORM'] = 'Tạo ra các hình mẫu';

$app_strings['LBL_SAVE_WEB_TO_LEAD_FORM'] = 'Lưu mẫu cho wed';

$app_strings['LBL_AVAILABLE_FIELDS'] = 'Có sẵn các lĩnh vực';

$app_strings['LBL_FIRST_FORM_COLUMN'] = 'Cột mẫu đầu tiên';

$app_strings['LBL_SECOND_FORM_COLUMN'] = 'Cột mẫu thứ hai';

$app_strings['LBL_ASSIGNED_TO_REQUIRED'] = 'Thiếu trường bắt buộc: được chỉ định để';

$app_strings['LBL_RELATED_CAMPAIGN_REQUIRED'] = 'Thiếu trường bắt buộc: liên quan đến chiến dịch';

$app_strings['LBL_TYPE_OF_PERSON_FOR_FORM'] = 'Mẫu web để tạo ra ';

$app_strings['LBL_TYPE_OF_PERSON_FOR_FORM_DESC'] = 'Đệ trình mẫu đơn này sẽ tạo ra ';

$app_strings['LBL_ADD_ALL_LEAD_FIELDS'] = 'Thêm tất cả các lĩnh vực';

$app_strings['LBL_RESET_ALL_LEAD_FIELDS'] = 'Đặt lại tất cả các lĩnh vực';

$app_strings['LBL_REMOVE_ALL_LEAD_FIELDS'] = 'Loại bỏ tất cả các trường';

$app_strings['LBL_NEXT_BTN'] = 'Tới';

$app_strings['LBL_ONLY_IMAGE_ATTACHMENT'] = 'Chỉ có thể nhúng các tệp đính kèm hình ảnh được hỗ trợ sau đây: JPG, PNG.';

$app_strings['LBL_TRAINING'] = 'Diễn đàn hỗ trợ';

$app_strings['ERR_MSSQL_DB_CONTEXT'] = 'Thay đổi bối cảnh cơ sở dữ liệu thành';

$app_strings['ERR_MSSQL_WARNING'] = 'Cảnh báo:';

$app_strings['ERR_CANNOT_CREATE_METADATA_FILE'] = 'Lỗi: Tệp [[file]] là mất tích. Không thể tạo vì không tìm thấy file HTML tương ứng.';

$app_strings['ERR_CANNOT_FIND_MODULE'] = 'Lỗi: Mô-đun [module] không tồn tại.';

$app_strings['LBL_ALT_ADDRESS'] = 'Địa chỉ khác:';

$app_strings['ERR_SMARTY_UNEQUAL_RELATED_FIELD_PARAMETERS'] = 'Lỗi: Có một số đối số không đồng đều cho các phần tử \'key\' và \'copy\' trong mảng Params hiển thị.';

$app_strings['LBL_DASHLET_CONFIGURE_GENERAL'] = 'Tổng quan';

$app_strings['LBL_DASHLET_CONFIGURE_FILTERS'] = 'Lọc';

$app_strings['LBL_DASHLET_CONFIGURE_MY_ITEMS_ONLY'] = 'Chỉ dữ liệu của tôi';

$app_strings['LBL_DASHLET_CONFIGURE_TITLE'] = 'Tiêu đề';

$app_strings['LBL_DASHLET_CONFIGURE_DISPLAY_ROWS'] = 'Các dòng hiển thị';

$app_strings['LBL_MAX_DASHLETS_REACHED'] = 'Bạn đã đạt đến số tối đa SuiteCRM Dashlets quản trị của bạn đã thiết lập. Hãy loại bỏ SuiteCRM Dashlet để thêm nhiều hơn nữa.';

$app_strings['LBL_ADDING_DASHLET'] = 'Thêm SuiteCRM Dashlet...';

$app_strings['LBL_ADDED_DASHLET'] = 'SuiteCRM Dashlet thêm';

$app_strings['LBL_REMOVE_DASHLET_CONFIRM'] = 'Bạn có chắc bạn muốn loại bỏ này SuiteCRM Dashlet?';

$app_strings['LBL_REMOVING_DASHLET'] = 'Loại bỏ các SuiteCRM Dashlet...';

$app_strings['LBL_REMOVED_DASHLET'] = 'SuiteCRM Dashlet removed';

$app_strings['LBL_LOADING_PAGE'] = 'Đang tải trang, xin chờ...';

$app_strings['LBL_RELOAD_PAGE'] = 'Xin vui lòng <a href=';

$app_strings['LBL_ADD_DASHLETS'] = 'Thêm Dashlets';

$app_strings['LBL_CLOSE_DASHLETS'] = 'Đóng';

$app_strings['LBL_OPTIONS'] = 'Tùy chọn';

$app_strings['LBL_1_COLUMN'] = '1 cột';

$app_strings['LBL_2_COLUMN'] = '2 cột';

$app_strings['LBL_3_COLUMN'] = '3 cột';

$app_strings['LBL_PAGE_NAME'] = 'tên trang';

$app_strings['LBL_SEARCH_RESULTS'] = 'Kết quả tìm kiếm';

$app_strings['LBL_SEARCH_MODULES'] = 'Mô-đun';

$app_strings['LBL_SEARCH_TOOLS'] = 'Công cụ';

$app_strings['LBL_SEARCH_HELP_TITLE'] = 'mẹo tìm kiếm';

$app_strings['LBL_NO_IMAGE'] = 'Không có hình ảnh';

$app_strings['LBL_MODULE'] = 'Chức năng';

$app_strings['LBL_COPY_ADDRESS_FROM_LEFT'] = 'Sao chép các địa chỉ từ trái:';

$app_strings['LBL_SAVE_AND_CONTINUE'] = 'Lưu và tiếp tục';

$app_strings['LBL_SEARCH_HELP_TEXT'] = '<p>< br / ><strong>Multiselect điều khiển</strong></p><ul><li>Bấm vào các giá trị để chọn một thuộc tính.</li><li>Nhấp Ctrl để chọn nhiều. Người dùng Mac sử dụng bấm vào CMD.</li><li>Để chọn tất cả các giá trị giữa hai thuộc tính,  bấm vào giá trị đầu tiên và sau đó thay đổi nhấp chuột cuối cùng giá trị.</li></ul><p><strong>Tùy chọn nâng cao tìm kiếm bố trí &</strong><br><br>bằng cách sử dụng tùy chọn<b>Lưu tìm & Layout</b>,bạn có thể lưu một tập hợp các thông số tìm kiếm và/hoặc một giao diện tùy chỉnh dạng xem danh sách để nhanh chóng có được kết quả mong muốn tìm kiếm trong tương lai. Bạn có thể tiết kiệm một số lượng không giới hạn tìm kiếm tuỳ chỉnh và bố trí. Tất cả các lưu tìm kiếm xuất hiện theo tên trong danh sách tìm kiếm đã lưu, với cuối cùng tải tìm kiếm đã lưu xuất hiện ở đầu danh sách.<br><br>Để tùy chỉnh bố trí xem danh sách, sử dụng các hộp ẩn cột và cột hiển thị để chọn các lĩnh vực đó để hiển thị trong kết quả tìm kiếm. Ví dụ, bạn có thể xem hoặc ẩn thông tin chi tiết như ghi tên, và được chỉ định người dùng và nhóm được chỉ định trong tìm kiếm kết quả. Để thêm một cột vào xem danh sách, chọn trường từ cột ẩn danh và sử dụng mũi tên trái để di chuyển nó vào danh sách hiển thị cột. Để loại bỏ một cột từ danh sách, chọn nó trong danh sách hiển thị cột và sử dụng mũi tên bên phải để di chuyển nó đến cột ẩn danh.<br><br>Nếu bạn lưu thiết đặt bố trí, bạn sẽ có thể tải chúng bất cứ lúc nào để xem kết quả tìm kiếm tùy chỉnh bố trí. <br><br>Để lưu và cập nhật một tìm kiếm và/hoặc bố trí:<ol><li>nhập tên cho tìm kiếm kết quả trong <b>lưu tìm kiếm này như</b> lĩnh vực và nhấp vào <b>lưu</b>. Tên gọi bây giờ sẽ hiển thị trong danh sách tìm kiếm lưu cạnh nút <b>xóa</b>.</li><li>Để xem các tìm kiếm đã lưu, hãy chọn nó từ danh sách tìm kiếm lưu. Kết quả tìm kiếm được hiển thị trong giao diện danh sách.</li><li>Để cập nhật các thuộc tính của một tìm kiếm đã lưu, hãy chọn tìm kiếm đã lưu từ danh sách, nhập các tiêu chí tìm kiếm mới và/hoặc bố trí tùy chọn trong khu vực tìm kiếm nâng cao, và nhấp vào <b>Cập Nhật</b> bên cạnh <b>Thay đổi tìm kiếm hiện tại</b>.</li> <li>Để xoá tìm kiếm đã lưu, hãy chọn nó trong danh sách tìm kiếm đã lưu, nhấp vào <b>xóa</b> bên cạnh <b>Thay đổi tìm kiếm hiện thời</b> và sau đó nhấp vào <b>OK</b> để xác nhận Xoá.</li></ol> <p><strong>Lời khuyên</strong> <br><br>bằng cách sử dụng các % như là một nhà điều hành ký bạn có thể làm cho tìm kiếm của bạn rộng hơn. Ví dụ như thay vì chỉ tìm kiếm cho kết quả tương đương ';

$app_strings['ERR_QUERY_LIMIT'] = 'Lỗi: Đạt đến giới hạn truy vấn của $limit cho $module module.';

$app_strings['ERROR_NOTIFY_OVERRIDE'] = 'Lỗi: Resource Observer->notify() cần được ghi đè.';

$app_strings['ERR_MONITOR_FILE_MISSING'] = 'Lỗi: Không thể tạo màn hình vì tệp siêu dữ liệu là trống rỗng hoặc tập tin không tồn tại.';

$app_strings['ERR_MONITOR_NOT_CONFIGURED'] = 'Lỗi: Không có cấu hình màn hình cho tên yêu cầu';

$app_strings['ERR_UNDEFINED_METRIC'] = 'Lỗi: Không thể đặt giá trị cho số liệu không xác định';

$app_strings['ERR_STORE_FILE_MISSING'] = 'Lỗi: Không tìm thấy cửa hàng thực hiện tệp';

$app_strings['LBL_MONITOR_ID'] = 'Giám sát Id';

$app_strings['LBL_USER_ID'] = 'Id người dùng';

$app_strings['LBL_MODULE_NAME'] = 'Tên mô-đun';

$app_strings['LBL_ITEM_ID'] = 'Mã Id';

$app_strings['LBL_ITEM_SUMMARY'] = 'Mục tóm tắt';

$app_strings['LBL_ACTION'] = 'Hành động';

$app_strings['LBL_SESSION_ID'] = 'Id Phiên làm việc';

$app_strings['LBL_BREADCRUMBSTACK_CREATED'] = 'BreadCrumbStack tạo ra cho người sử dụng id {0}';

$app_strings['LBL_VISIBLE'] = 'Hồ sơ có thể nhìn thấy';

$app_strings['LBL_DATE_LAST_ACTION'] = 'Ngày của hành động cuối';

$app_strings['MSG_IS_NOT_BEFORE'] = 'không phải là trước khi';

$app_strings['MSG_IS_MORE_THAN'] = 'là nhiều hơn';

$app_strings['MSG_SHOULD_BE'] = 'nên';

$app_strings['MSG_OR_GREATER'] = 'hoặc cao hơn';

$app_strings['LBL_LIST'] = 'Danh sách';

$app_strings['LBL_CREATE_BUG'] = 'Tạo Bug';

$app_strings['LBL_OBJECT_IMAGE'] = 'đối tượng hình ảnh';

$app_strings['LBL_MASSUPDATE_DATE'] = 'Chọn ngày';

$app_strings['LBL_VALIDATE_RANGE'] = 'không phải là trong phạm vi hợp lệ';

$app_strings['LBL_CHOOSE_START_AND_END_DATES'] = 'Hãy chọn cả phạm vi ngày bắt đầu và kết thúc';

$app_strings['LBL_CHOOSE_START_AND_END_ENTRIES'] = 'Hãy chọn cả mục nhập phạm vi bắt đầu và kết thúc';

$app_strings['LBL_DROPDOWN_LIST_ALL'] = 'Tất cả';

$app_strings['ERR_CONNECTOR_FILL_BEANS_SIZE_MISMATCH'] = 'Lỗi: Số đếm mảng của tham số bean không khớp với số đếm mảng của các kết quả.';

$app_strings['ERR_MISSING_MAPPING_ENTRY_FORM_MODULE'] = 'Lỗi: Thiếu mục nhập lập bản đồ cho mô-đun.';

$app_strings['ERROR_UNABLE_TO_RETRIEVE_DATA'] = 'Lỗi: Không thể truy xuất dữ liệu cho {0} kết nối. Các dịch vụ hiện có thể không thể truy cập hoặc các cài đặt cấu hình có thể không hợp lệ. Thông báo lỗi kết nối: ({1}).';

$app_strings['LBL_FASTCGI_LOGGING'] = 'Tối ưu trải nghiệm sử dụng IIS/FastCGI sapi, thiết lập fastcgi.logging 0 trong tập tin php.ini của bạn.';

$app_strings['LBL_COLLECTION_NAME'] = 'Tên';

$app_strings['LBL_COLLECTION_PRIMARY'] = 'Chính';

$app_strings['ERROR_MISSING_COLLECTION_SELECTION'] = 'Trống trường bắt buộc';

$app_strings['LBL_ASSIGNED_TO_NAME'] = 'Chỉ định cho';

$app_strings['LBL_DESCRIPTION'] = 'Mô tả';

$app_strings['LBL_YESTERDAY'] = 'hôm qua';

$app_strings['LBL_TODAY'] = 'hôm nay';

$app_strings['LBL_TOMORROW'] = 'ngày mai';

$app_strings['LBL_NEXT_WEEK'] = 'thứ 7 tới';

$app_strings['LBL_NEXT_MONDAY'] = 'thứ hai tiếp theo';

$app_strings['LBL_NEXT_FRIDAY'] = 'thứ sáu tiếp theo';

$app_strings['LBL_TWO_WEEKS'] = 'hai tuần';

$app_strings['LBL_NEXT_MONTH'] = 'tháng tới';

$app_strings['LBL_FIRST_DAY_OF_NEXT_MONTH'] = 'ngày đầu tiên của tháng tiếp theo';

$app_strings['LBL_THREE_MONTHS'] = 'ba tháng';

$app_strings['LBL_SIXMONTHS'] = 'sáu tháng';

$app_strings['LBL_NEXT_YEAR'] = 'năm sau';

$app_strings['LBL_HOURS'] = 'Giờ';

$app_strings['LBL_MINUTES'] = 'Phút';

$app_strings['LBL_MERIDIEM'] = 'Meridiem';

$app_strings['LBL_DATE'] = 'Ngày giờ';

$app_strings['LBL_DASHLET_CONFIGURE_AUTOREFRESH'] = 'Tự động làm mới';

$app_strings['LBL_DURATION_DAY'] = 'ngày';

$app_strings['LBL_DURATION_HOUR'] = 'giờ';

$app_strings['LBL_DURATION_MINUTE'] = 'phút';

$app_strings['LBL_DURATION_DAYS'] = 'Ngày';

$app_strings['LBL_DURATION_HOURS'] = 'Trong suốt hàng giờ';

$app_strings['LBL_DURATION_MINUTES'] = 'Trong suốt hàng phút';

$app_strings['LBL_CHOOSE_MONTH'] = 'Chọn tháng';

$app_strings['LBL_ENTER_YEAR'] = 'Nhập năm';

$app_strings['LBL_ENTER_VALID_YEAR'] = 'Vui lòng nhập một năm hợp lệ';

$app_strings['ERR_FILE_WRITE'] = 'Lỗi: Không thể ghi tệp {0}. Vui lòng kiểm tra hệ thống và web server cho phép.';

$app_strings['ERR_FILE_NOT_FOUND'] = 'Lỗi: Không thể nạp tệp {0}. Vui lòng kiểm tra hệ thống và web server cho phép.';

$app_strings['LBL_AND'] = 'A';

$app_strings['LBL_SEARCH_EXTERNAL_API'] = 'Các tập tin trên nguồn bên ngoài';

$app_strings['LBL_EXTERNAL_SECURITY_LEVEL'] = 'Bảo mật';

$app_strings['LBL_IMPORT_SAMPLE_FILE_TEXT'] = ' ';

$app_strings['LBL_NOTIFICATIONS_NONE'] = 'Không có thông báo hiện tại';

$app_strings['LBL_ALT_SORT_DESC'] = 'Xếp giảm dần';

$app_strings['LBL_ALT_SORT_ASC'] = 'Xếp tăng dần';

$app_strings['LBL_ALT_SORT'] = 'Sắp xếp';

$app_strings['LBL_ALT_SHOW_OPTIONS'] = 'Hiển thị tùy chọn';

$app_strings['LBL_ALT_HIDE_OPTIONS'] = 'Ẩn tùy chọn';

$app_strings['LBL_ALT_MOVE_COLUMN_LEFT'] = 'Di chuyển mục đã chọn vào danh sách bên trái';

$app_strings['LBL_ALT_MOVE_COLUMN_RIGHT'] = 'Di chuyển mục đã chọn vào danh sách ở bên phải';

$app_strings['LBL_ALT_MOVE_COLUMN_UP'] = 'Di chuyển mục đã chọn lên để hiển thị danh sách';

$app_strings['LBL_ALT_MOVE_COLUMN_DOWN'] = 'Di chuyển mục đã chọn xuống để hiển thị danh sách';

$app_strings['LBL_ALT_INFO'] = 'Thông tin';

$app_strings['MSG_DUPLICATE'] = 'Bản ghi {0} bạn đang tạo ra có thể là một bản sao của một bản ghi {0} đã tồn tại. {1} bản ghi có tên tương tự được liệt kê dưới đây. {1} <br>nhấp vào tạo để tiếp tục tạo này {0} mới, hoặc chọn một {0} sẵn có được liệt kê dưới đây.';

$app_strings['MSG_SHOW_DUPLICATES'] = 'Bản ghi {0} bạn đang tạo ra có thể là một bản sao của một bản ghi {0} đã tồn tại. {1} bản ghi có tên tương tự được liệt kê dưới đây. Bấm vào lưu để tiếp tục tạo này {0} mới, hay bấm Hủy bỏ để trở về các mô-đun mà không tạo ra {0}.';

$app_strings['LBL_EMAIL_TITLE'] = 'địa chỉ email';

$app_strings['LBL_EMAIL_OPT_TITLE'] = 'Địa chỉ email từ chối';

$app_strings['LBL_EMAIL_INV_TITLE'] = 'Địa chỉ email không hợp lệ';

$app_strings['LBL_EMAIL_PRIM_TITLE'] = 'Làm cho địa chỉ Email chính';

$app_strings['LBL_SELECT_ALL_TITLE'] = 'Chọn tất cả';

$app_strings['LBL_SELECT_THIS_ROW_TITLE'] = 'Chọn hàng này';

$app_strings['UPLOAD_ERROR_TEXT'] = 'LỖI: Đã có lỗi trong khi tải lên. Mã lỗi: {0} - {1}';

$app_strings['UPLOAD_ERROR_TEXT_SIZEINFO'] = 'LỖI: Đã có lỗi trong khi tải lên. Mã lỗi: {0} - {1}. Upload_maxsize là {2} ';

$app_strings['UPLOAD_ERROR_HOME_TEXT'] = 'LỖI: Có một lỗi trong khi tải lên của bạn, xin vui lòng liên hệ với người quản trị để được giúp đỡ.';

$app_strings['UPLOAD_MAXIMUM_EXCEEDED'] = 'Kích thước của tải lên ({0} byte) vượt quá cho phép tối đa: {1} byte';

$app_strings['UPLOAD_REQUEST_ERROR'] = 'Lỗi đã xảy ra. Xin vui lòng làm tươi lại trang và thử lại.';

$app_strings['LBL_EDIT_BUTTON_KEY'] = 'i';

$app_strings['LBL_EDIT_BUTTON_LABEL'] = 'Sửa';

$app_strings['LBL_EDIT_BUTTON_TITLE'] = 'Sửa';

$app_strings['LBL_DUPLICATE_BUTTON_KEY'] = 'u';

$app_strings['LBL_DUPLICATE_BUTTON_LABEL'] = 'Trùng lặp';

$app_strings['LBL_DUPLICATE_BUTTON_TITLE'] = 'Trùng lặp';

$app_strings['LBL_DELETE_BUTTON_KEY'] = 'd';

$app_strings['LBL_DELETE_BUTTON_LABEL'] = 'Xóa';

$app_strings['LBL_DELETE_BUTTON_TITLE'] = 'Xóa';

$app_strings['LBL_BULK_ACTION_BUTTON_LABEL'] = 'Hàng loạt hành động';

$app_strings['LBL_BULK_ACTION_BUTTON_LABEL_MOBILE'] = 'Hành động';

$app_strings['LBL_SAVE_BUTTON_KEY'] = 'a';

$app_strings['LBL_SAVE_BUTTON_LABEL'] = 'Lưu';

$app_strings['LBL_SAVE_BUTTON_TITLE'] = 'Lưu';

$app_strings['LBL_CANCEL_BUTTON_KEY'] = 'l';

$app_strings['LBL_CANCEL_BUTTON_LABEL'] = 'Huỷ bỏ';

$app_strings['LBL_CANCEL_BUTTON_TITLE'] = 'Hủy';

$app_strings['LBL_FIRST_INPUT_EDIT_VIEW_KEY'] = '7';

$app_strings['LBL_ADV_SEARCH_LNK_KEY'] = '8';

$app_strings['LBL_FIRST_INPUT_SEARCH_KEY'] = '9';

$app_strings['ERR_CONNECTOR_NOT_ARRAY'] = 'đầu nối mảng trong {0} định nghĩa không chính xác hoặc có sản phẩm nào và không được sử dụng.';

$app_strings['ERR_SUHOSIN'] = 'Tải lên dòng bị chặn bởi Suhosin, xin vui lòng thêm';

$app_strings['ERR_BAD_RESPONSE_FROM_SERVER'] = 'Các phản ứng xấu từ các máy chủ';

$app_strings['LBL_ACCOUNT_PRODUCT_QUOTE_LINK'] = 'Báo giá';

$app_strings['LBL_ACCOUNT_PRODUCT_SALE_PRICE'] = 'Giá bán';

$app_strings['LBL_EMAIL_CHECK_INTERVAL_DOM'] = 'Array';

$app_strings['ERR_A_REMINDER_IS_EMPTY_OR_INCORRECT'] = 'Xin nhắc lại là rỗng hoặc không chính xác.';

$app_strings['ERR_REMINDER_IS_NOT_SET_POPUP_OR_EMAIL'] = 'Xin nhắc lại không được đặt cho một popup hoặc email.';

$app_strings['ERR_NO_INVITEES_FOR_REMINDER'] = 'Không có người mời để nhắc nhở.';

$app_strings['LBL_DELETE_REMINDER_CONFIRM'] = 'Lời nhắc không bao gồm bất kỳ người được mời nào, bạn có muốn xóa lời nhắc không?';

$app_strings['LBL_DELETE_REMINDER'] = 'Xóa bỏ nhắc nhở';

$app_strings['LBL_OK'] = 'Ok';

$app_strings['LBL_COLUMNS_FILTER_HEADER_TITLE'] = 'Chọn cột';

$app_strings['LBL_COLUMN_CHOOSER'] = 'Chọn cột';

$app_strings['LBL_SAVE_CHANGES_BUTTON_TITLE'] = 'Lưu thay đổi';

$app_strings['LBL_DISPLAYED'] = 'Hiển thị';

$app_strings['LBL_HIDDEN'] = 'Ẩn';

$app_strings['ERR_EMPTY_COLUMNS_LIST'] = 'Ít nhất, một trong những yếu tố cần thiết';

$app_strings['LBL_FILTER_HEADER_TITLE'] = 'Bộ lọc';

$app_strings['LBL_CATEGORY'] = 'Chủng loại';

$app_strings['LBL_LIST_CATEGORY'] = 'Chủng loại';

$app_strings['ERR_FACTOR_TPL_INVALID'] = 'Thông điệp xác thực yếu tố là không hợp lệ, vui lòng liên hệ với người quản trị.';

$app_strings['LBL_SUBTHEMES'] = 'Kiểu';

$app_strings['LBL_SUBTHEME_OPTIONS_DAWN'] = 'Bình minh';

$app_strings['LBL_SUBTHEME_OPTIONS_DAY'] = 'Ngày';

$app_strings['LBL_SUBTHEME_OPTIONS_DUSK'] = 'Chạng vạng';

$app_strings['LBL_SUBTHEME_OPTIONS_NIGHT'] = 'Đêm';

$app_strings['LBL_CONFIRM_DISREGARD_DRAFT_TITLE'] = 'Bỏ qua bản dự thảo';

$app_strings['LBL_CONFIRM_DISREGARD_DRAFT_BODY'] = 'Thao tác này sẽ xóa email này, bạn có muốn tiếp tục?';

$app_strings['LBL_CONFIRM_DISREGARD_EMAIL_TITLE'] = 'Thoát';

$app_strings['LBL_CONFIRM_DISREGARD_EMAIL_BODY'] = 'Nếu rời khỏi hộp soạn thảo bạn sẽ mất các thông tin đã nhập, bạn có muốn tiếp tục?';

$app_strings['LBL_CONFIRM_APPLY_EMAIL_TEMPLATE_TITLE'] = 'Áp dụng một mẫu thư điện tử';

$app_strings['LBL_CONFIRM_APPLY_EMAIL_TEMPLATE_BODY'] = 'Thao tác này sẽ ghi đè lên các thư điện tử cơ thể và các lĩnh vực chủ đề, bạn có muốn tiếp tục?';

$app_strings['LBL_CONFIRM_OPT_IN_TITLE'] = 'Xác nhận chọn tham gia';

$app_strings['LBL_OPT_IN_TITLE'] = 'Chọn tham gia';

$app_strings['LBL_CONFIRM_OPT_IN_DATE'] = 'Xác nhận chọn ngày tham gia';

$app_strings['LBL_CONFIRM_OPT_IN_SENT_DATE'] = 'Xác nhận chọn ngày gửi';

$app_strings['LBL_CONFIRM_OPT_IN_FAIL_DATE'] = 'Ngày xác nhận sử dụng sai';

$app_strings['LBL_CONFIRM_OPT_IN_TOKEN'] = 'Mã xác nhận sử dụng';

$app_strings['ERR_OPT_IN_TPL_NOT_SET'] = 'Mẫu Email chọn tham gia thì không được cấu hình. Xin vui lòng thiết lập trong cài đặt email.';

$app_strings['ERR_OPT_IN_RELATION_INCORRECT'] = 'Email tham gia yêu cầu phải có liên quan đến Tài khoản/Liên hệ/Đầu mối/Mục tiêu';

$app_strings['LBL_SECURITYGROUP_NONINHERITABLE'] = 'Nhóm không có thể thừa kế';

$app_strings['LBL_PRIMARY_GROUP'] = 'Nhóm chính';

$app_strings['LBL_SUITE_TOP'] = 'Lên trên';

$app_strings['LBL_SUITE_SUPERCHARGED'] = 'Được nén bởi SuiteCRM';

$app_strings['LBL_SUITE_POWERED_BY'] = 'Được tài trợ bởi SugarCRM';

$app_strings['LBL_SUITE_DESC1'] = 'SuiteCRM đã được viết và đóng gói bởi <a href=';

$app_strings['LBL_SUITE_DESC2'] = 'Chương trình này là phần mềm miễn phí; bạn có thể phân phối lại nó và/hoặc sửa đổi nó theo các điều khoản của Giấy phép cộng đồng chung Affose của GNU phiên bản 3 như được xuất bản bởi Tổ chức Phần mềm miễn phí, bao gồm sự cho phép bổ sung được đề cập trong tiêu đề mã nguồn.';

$app_strings['LBL_SUITE_DESC3'] = 'SuiteCRM là thương hiệu của SalesAgility Ltd. Tất cả các tên công ty và sản phẩm khác có thể là thương hiệu của các công ty tương ứng mà chúng được liên kết.';

$app_strings['LBL_GENERATE_PASSWORD_BUTTON_TITLE'] = 'Đặt lại mật khẩu';

$app_strings['LBL_SEND_CONFIRM_OPT_IN_EMAIL'] = 'Gửi xác nhận trong Email tham gia';

$app_strings['LBL_CONFIRM_OPT_IN_ONLY_FOR_PERSON'] = 'Email xác nhận sử dụng chỉ dành cho Khách Hàng/ Liên Hệ/ Khách Hàng Tiềm Năng/ Khách Hàng Triển Vọng';

$app_strings['LBL_CONFIRM_OPT_IN_IS_DISABLED'] = 'Email xác nhận sử dụng đã bị vô hiệu, kích hoạt thông số này trong Thiết lập Email hoặc liên hệ với Quản trị hệ thống.';

$app_strings['LBL_CONTACT_HAS_NO_PRIMARY_EMAIL'] = 'Email xác nhận sử dụng đang gửi là không thể vì trong Liên Hệ không có địa chỉ email chính';

$app_strings['LBL_CONFIRM_EMAIL_SENDING_FAILED'] = 'Email xác nhận sử dụng đang gửi không thành công';

$app_strings['LBL_CONFIRM_EMAIL_SENT'] = 'Email xác nhận sử dụng đã gửi thành công';

$app_strings['LBL_STATUS_EVENT'] = 'Tình trạng lời mời';

$app_strings['LBL_ACCEPT_STATUS'] = 'trạng thái đồng ý';

$app_strings['LBL_LISTVIEW_OPTION_CURRENT'] = 'chọn trang này';

$app_strings['LBL_LISTVIEW_OPTION_ENTIRE'] = 'Chọn tất cả';

$app_strings['LBL_LISTVIEW_NONE'] = 'Bỏ chọn tất cả';

$app_strings['LBL_AOP_EMAIL_REPLY_DELIMITER'] = '========== Vui lòng trả lời trên dòng này ==========';

$app_strings['LBL_CRON_ON_THE_MONTHDAY'] = 'trên các';

$app_strings['LBL_CRON_ON_THE_WEEKDAY'] = 'trên';

$app_strings['LBL_CRON_AT'] = 'tại';

$app_strings['LBL_CRON_RAW'] = 'Nâng cao';

$app_strings['LBL_CRON_MIN'] = 'Phút';

$app_strings['LBL_CRON_HOUR'] = 'Giờ';

$app_strings['LBL_CRON_DAY'] = 'Ngày';

$app_strings['LBL_CRON_MONTH'] = 'Tháng';

$app_strings['LBL_CRON_DOW'] = 'DOW';

$app_strings['LBL_CRON_DAILY'] = 'Hàng ngày';

$app_strings['LBL_CRON_WEEKLY'] = 'Hàng tuần';

$app_strings['LBL_CRON_MONTHLY'] = 'Hàng tháng';

$app_strings['LBL_PRINT_AS_PDF'] = 'In dưới dạng PDF';

$app_strings['LBL_SELECT_TEMPLATE'] = 'Vui lòng lựa chọn một Mẫu';

$app_strings['LBL_NO_TEMPLATE'] = 'Tìm thấy ERROR\\nNo mẫu. \\nPlease đi đến các mô-đun mẫu PDF và tạo một tài khoản';

$app_strings['LBL_GANTT_BUTTON_LABEL'] = 'Xem biểu đồ Gantt';

$app_strings['LBL_DETAIL_BUTTON_LABEL'] = 'Xem chi tiết';

$app_strings['LBL_CREATE_PROJECT'] = 'Tạo dự án';

$app_strings['LBL_MAP'] = 'Bản đồ';

$app_strings['LBL_JJWG_MAPS_LNG'] = 'Kinh độ';

$app_strings['LBL_JJWG_MAPS_LAT'] = 'Vĩ độ';

$app_strings['LBL_JJWG_MAPS_GEOCODE_STATUS'] = 'Trạng thái Mã địa lý';

$app_strings['LBL_JJWG_MAPS_ADDRESS'] = 'Địa chỉ';

$app_strings['LBL_RESCHEDULE_LABEL'] = 'Đổi lịch hẹn';

$app_strings['LBL_RESCHEDULE_TITLE'] = 'Vui lòng nhập thông tin lịch lại';

$app_strings['LBL_RESCHEDULE_DATE'] = 'Ngày:';

$app_strings['LBL_RESCHEDULE_REASON'] = 'Lý do:';

$app_strings['LBL_RESCHEDULE_ERROR1'] = 'Vui lòng chọn ngày đúng';

$app_strings['LBL_RESCHEDULE_ERROR2'] = 'Vui lòng lựa chọn một lý do';

$app_strings['LBL_RESCHEDULE_PANEL'] = 'Đổi lịch hẹn';

$app_strings['LBL_RESCHEDULE_HISTORY'] = 'Lịch sử cuộc gọi thử';

$app_strings['LBL_RESCHEDULE_COUNT'] = 'Cuộc gọi thử';

$app_strings['LBL_LOGIN_AS'] = 'Đăng nhập như ';

$app_strings['LBL_LOGOUT_AS'] = 'Đăng xuất như ';

$app_strings['LBL_SECURITYGROUP'] = 'Nhóm bảo mật';

$app_strings['FACEBOOK_USER_C'] = 'Facebook';

$app_strings['TWITTER_USER_C'] = 'Twitter';

$app_strings['LBL_PANEL_SOCIAL_FEED'] = 'Thông tin chi tiết nguồn cấp dữ liệu xã hội';

$app_strings['LBL_SUBPANEL_FILTER_LABEL'] = 'Bộ lọc';

$app_strings['LBL_COLLECTION_TYPE'] = 'Loại';

$app_strings['LBL_ADD_TAB'] = 'Thêm thẻ';

$app_strings['LBL_EDIT_TAB'] = 'Chỉnh sửa thẻ';

$app_strings['LBL_SUITE_DASHBOARD'] = 'Bảng điều khiển SuiteCRM';

$app_strings['LBL_ENTER_DASHBOARD_NAME'] = 'Nhập tên bảng:';

$app_strings['LBL_NUMBER_OF_COLUMNS'] = 'Số lượng các cột:';

$app_strings['LBL_DELETE_DASHBOARD1'] = 'Bạn có chắc chắn muốn xoá';

$app_strings['LBL_DELETE_DASHBOARD2'] = 'bảng điều khiển?';

$app_strings['LBL_ADD_DASHBOARD_PAGE'] = 'Thêm một trang bảng điều khiển';

$app_strings['LBL_DELETE_DASHBOARD_PAGE'] = 'Loại bỏ bảng điều khiển hiện thời';

$app_strings['LBL_RENAME_DASHBOARD_PAGE'] = 'Đổi tên trang bảng điều khiển';

$app_strings['LBL_SUITE_DASHBOARD_ACTIONS'] = 'Hành động';

$app_strings['LBL_CONFIRM_CANCEL_INLINE_EDITING'] = 'Bạn đã nhấp vào từ các lĩnh vực mà bạn đã chỉnh sửa mà không lưu nó. Bấm ok nếu bạn đang hạnh phúc để mất của bạn thay đổi, hoặc hủy bỏ nếu bạn muốn tiếp tục chỉnh sửa';

$app_strings['LBL_LOADING_ERROR_INLINE_EDITING'] = 'Đã có lỗi trong khi tải các lĩnh vực. Phiên của bạn có thể đã hết thời gian. Xin vui lòng đăng nhập lại để sửa lỗi này';

$app_strings['LBL_OPT_IN_PENDING_EMAIL_NOT_SENT'] = 'Đang chờ xác nhận chọn tham gia, xác nhận chọn tham gia không được gửi';

$app_strings['LBL_OPT_IN_PENDING_EMAIL_FAILED'] = 'Email xác nhận sử dụng đang gửi bị lỗi';

$app_strings['LBL_OPT_IN_PENDING_EMAIL_SENT'] = 'Đang chờ xác nhận chọn tham gia, xác nhận chọn tham gia đã được gửi';

$app_strings['LBL_OPT_IN'] = 'Chọn tham gia';

$app_strings['LBL_OPT_IN_CONFIRMED'] = 'Đã xác nhận chọn tham gia';

$app_strings['LBL_OPT_IN_OPT_OUT'] = 'Từ chối nhận';

$app_strings['LBL_OPT_IN_INVALID'] = 'Không hợp lệ';

$app_strings['RESPONSE_SEND_CONFIRM_OPT_IN_EMAIL'] = 'Xác nhận việc Email chọn tham gia đã được thêm vào hàng email đợi cho %s (các) đỉa chỉ Email. ';

$app_strings['RESPONSE_SEND_CONFIRM_OPT_IN_EMAIL_NOT_OPT_IN'] = 'Không thể gửi email đến %s các địa chỉ email, bởi vì họ không được chọn tham gia. ';

$app_strings['RESPONSE_SEND_CONFIRM_OPT_IN_EMAIL_MISSING_EMAIL_ADDRESS_ID'] = 'địa chỉ email %s không có một id hợp lệ. ';

$app_strings['ERR_TWO_FACTOR_FAILED'] = 'Xác thực hai yếu tố bị lỗi';

$app_strings['ERR_TWO_FACTOR_CODE_SENT'] = 'Đã gửi mã xác thực hai yếu tố.';

$app_strings['ERR_TWO_FACTOR_CODE_FAILED'] = 'Không thể gửi mã xác thực hai yếu tố.';

$app_strings['LBL_THANKS_FOR_SUBMITTING'] = 'Cảm ơn bạn đã quan tâm.';

$app_strings['ERR_IP_CHANGE'] = 'Phiên của kết nối bị chấm dứt do sự thay đổi địa chỉ IP của bạn';

$app_strings['ERR_RETURN'] = 'Trở về trang chủ';

$app_strings['LBL_DEFAULT_API_ERROR_TITLE'] = 'Lỗi JSON API';

$app_strings['LBL_DEFAULT_API_ERROR_DETAIL'] = 'Xuất hiện lỗi JSON API.';

$app_strings['LBL_API_EXCEPTION_DETAIL'] = 'Phiên bản API: 8';

$app_strings['LBL_BAD_REQUEST_EXCEPTION_DETAIL'] = 'Vui lòng đảm bảo bạn đã nhập dữ liệu vào những trường được yêu cầu bắt buộc';

$app_strings['LBL_EMPTY_BODY_EXCEPTION_DETAIL'] = 'Phần nội dung của một yêu cầu của một JSON API phải có định dạng là JSON';

$app_strings['LBL_INVALID_JSON_API_REQUEST_EXCEPTION_DETAIL'] = 'Không thể xác nhận các Json Api Payload yêu cầu';

$app_strings['LBL_INVALID_JSON_API_RESPONSE_EXCEPTION_DETAIL'] = 'Không thể xác nhận các Json Api Payload phản hồi';

$app_strings['LBL_MODULE_NOT_FOUND_EXCEPTION_DETAIL'] = 'JSON API không thể tìm thấy tài nguyên';

$app_strings['LBL_NOT_ACCEPTABLE_EXCEPTION_DETAIL'] = 'JSON API mong muốn ';

$app_strings['LBL_UNSUPPORTED_MEDIA_TYPE_EXCEPTION_DETAIL'] = 'JSON API mong muốn ';

$app_strings['MSG_BROWSER_NOTIFICATIONS_ENABLED'] = 'Thông báo hiện tại được kích hoạt cho trình duyệt web này.';

$app_strings['MSG_BROWSER_NOTIFICATIONS_DISABLED'] = 'Thông báo đã được tắt cho trình duyệt web này. Sử dụng chức năng tuỳ chọn trình duyệt của bạn để bật lên khi cần thiết.';

$app_strings['MSG_BROWSER_NOTIFICATIONS_UNSUPPORTED'] = 'Trình duyệt này không hỗ trợ chế độ thông báo máy tính để bàn.';

$app_strings['LBL_GOOGLE_SYNC_ERR'] = 'Đồng bộ SuiteCRM Google - LỖI';

$app_strings['LBL_THERE_WAS_AN_ERR'] = 'Đã có lỗi: ';

$app_strings['LBL_CLICK_HERE'] = 'Click vào đây';

$app_strings['LBL_TO_CONTINUE'] = ' để tiếp tục.';

$app_strings['IMAP_HANDLER_ERROR'] = 'LỖI: {error}; từ khóa là: ';

$app_strings['IMAP_HANDLER_SUCCESS'] = 'OK: kiểm tra cài đặt đã thay đổi ';

$app_strings['IMAP_HANDLER_ERROR_INVALID_REQUEST'] = 'Yêu cầu không hợp lệ, dùng giá trị ';

$app_strings['IMAP_HANDLER_ERROR_UNKNOWN_BY_KEY'] = 'Lỗi không rõ đã xảy ra, phím ';

$app_strings['IMAP_HANDLER_ERROR_NO_TEST_SET'] = 'Kiểm tra cài đặt không tồn tại.';

$app_strings['IMAP_HANDLER_ERROR_NO_KEY'] = 'Không tìm thấy khóa bảo mật.';

$app_strings['IMAP_HANDLER_ERROR_KEY_SAVE'] = 'Lưu khóa bảo mật bị lỗi.';

$app_strings['IMAP_HANDLER_ERROR_UNKNOWN'] = 'Không rõ nguyên nhân';

$app_strings['LBL_SEARCH_TITLE'] = 'Tìm kiếm';

$app_strings['LBL_SEARCH_TEXT_FIELD_TITLE_ATTR'] = 'Điều kiện tìm kiếm';

$app_strings['LBL_SEARCH_SUBMIT_FIELD_TITLE_ATTR'] = 'Tìm kiếm';

$app_strings['LBL_SEARCH_SUBMIT_FIELD_VALUE'] = 'Tìm kiếm';

$app_strings['LBL_SEARCH_QUERY'] = 'Truy vấn tìm kiếm: ';

$app_strings['LBL_SEARCH_RESULTS_PER_PAGE'] = 'Kết quả mỗi trang: ';

$app_strings['LBL_SEARCH_ENGINE'] = 'Bộ máy tìm kiếm: ';

$app_strings['LBL_SEARCH_TOTAL'] = 'Tổng kết quả tìm kiếm: ';

$app_strings['LBL_SEARCH_PREV'] = 'Trước';

$app_strings['LBL_SEARCH_NEXT'] = 'Tới';

$app_strings['LBL_SEARCH_PAGE'] = 'Trang tìm kiếm ';

$app_strings['LBL_SEARCH_OF'] = ' của ';

$app_strings['LBL_TABGROUP_REPORTS'] = 'Báo cáo';

$app_strings['LBL_CREATE_APPS_BUTTON_LABEL'] = 'Create Applications';

$app_strings['LBL_NEW_COLUMN_TITLE'] = 'Mới';

$app_strings['LBL_IN_PROCESS_COLUMN_TITLE'] = 'Đang liên hệ';

$app_strings['LBL_CONVERTED_COLUMN_TITLE'] = 'Ký hợp đồng';

$app_strings['LBL_INVOICED_COLUMN_TITLE'] = 'Phát sinh giao dịch';

$app_strings['LBL_ETS_AMOUNT_TITLE'] = 'Doanh thu ước tính';

$app_strings['LBL_COMMISSION_AMOUNT_TITLE'] = 'Hoa hồng tạm tính';

$app_strings['LBL_DETAIL_LABEL'] = 'Thông tin chi tiết';

$app_strings['LBL_FILTER_LABEL'] = 'Lọc hoạt động';

$app_strings['LBL_PLEASE_SELECT_A_DATE'] = 'Vui lòng chọn ngày';

$app_strings['LBL_BTN_FILTER'] = 'Lọc';

$app_strings['LBL_DATE_FROM'] = 'From';

$app_strings['LBL_DATE_TO'] = 'To';

$app_strings['LBL_PERIOD'] = 'Period';

$app_strings['LBL_ADD_CALL_MODAL_TITLE'] = 'Tạo cuộc gọi';

$app_strings['LBL_ADD_MEETING_MODAL_TITLE'] = 'Tạo cuộc họp';

$app_strings['LBL_CONFIRM_CLOSE_ACTIVITY_QUESTION'] = 'Bạn có chắc chắn muốn đóng hành động này?';

$app_strings['LBL_CLOSE'] = 'Đóng';

$app_strings['LBL_QUOTATION'] = 'Báo giá';

$app_strings['LBL_CONFIRM_DEAD_LEAD'] = 'Bạn có chắc chắn muốn ngừng chăm sóc khách hàng này không?';

$app_strings['LBL_CONFIRM_CONVERTED_LEAD'] = 'Bạn có chắc chắn muốn chuyển trạng thái khách hàng này không?';

$app_strings['LBL_INPROCESS_CUSTOMER_PERCENT'] = 'KHTN đang liên hệ và chuyển đổi';

$app_strings['LBL_NEW_CUSTOMER_PERCENT'] = 'Tổng KHTN Mới Và Đang liên hệ';

$app_strings['LBL_CONVERTED_CUSTOMER_PERCENT'] = 'KHTN đã chuyển đổi';

$app_strings['LBL_REVENUE_REACHED'] = 'Doanh thu đã đạt';

$app_strings['LBL_REVENUE_IN_KPI'] = 'Doanh thu chỉ tiêu';

$app_strings['LBL_INPROCESS_CUSTOMER_REPORT_TITLE'] = 'Tỉ lệ KHTN đang liên hệ';

$app_strings['LBL_CONVERTED_CUSTOMER_REPORT_TITLE'] = 'Tỉ lệ KHTN đã chuyển đổi';

$app_strings['LBL_REVENUE_REPORT_TITLE'] = 'Tỉ lệ doanh thu đạt được';

$app_strings['LBL_KPI_REPORT_TITLE'] = 'KPI về khách hàng';

$app_strings['LBL_NUM_CONVERTED_CUSTOMER_REPORT_TITLE'] = 'Tỷ lệ KH mới trong 12 tháng';

$app_strings['LBL_KP_COMMISSION_TITLE'] = 'Doanh số & Hoa hồng';

$app_strings['LBL_NEW_CUSTOMER'] = 'KHTN mới:';

$app_strings['LBL_INPROCESS_CUSTOMER'] = 'KHTN liên hệ: ';

$app_strings['LBL_MONTHS'] = 'Tháng';

$app_strings['LBL_NUMBER_OF_CONTRACT'] = 'Đã mua hàng';

$app_strings['LBL_KPI_CONFIG_INCORRECT'] = 'Cảnh báo : Không tìm thấy thông tin cấu hình KPI hợp lệ hoặc KPI chưa được cấu hình, vui lòng liên hệ quản trị viên để biết thêm chi tiết!!';

$app_strings['LBL_ID_FF_OPT_IN'] = 'Chọn tham gia';

$app_strings['LBL_ERROR_UNDEFINED_BEHAVIOR'] = 'Lỗi bất ngờ đã xảy ra.';

$app_strings['LBL_ERROR_UNHANDLED_VALUE'] = 'Một giá trị đã không được xử lý một cách chính xác đó ngăn chặn một quá trình tiếp tục.';

$app_strings['LBL_ERROR_UNUSABLE_VALUE'] = 'Một giá trị không sử dụng được tìm thấy đó ngăn chặn một quá trình tiếp tục.';

$app_strings['LBL_ERROR_INVALID_TYPE'] = 'Các loại giá trị là khác nhau hơn so với những gì đã được dự kiến.';

$app_strings['LBL_NOTIFICATIONS'] = 'Chú ý';

$app_strings['LBL_EMAIL_REPORTS_TITLE'] = 'Báo cáo';

$app_strings['LBL_EMAIL_OPT_IN'] = 'Đã tham gia';

$app_strings['LBL_EMAIL_OPT_IN_AND_INVALID'] = 'Đã chọn tham gia và không hợp lệ';

$app_strings['ERR_FILE_EMPTY'] = 'Tập tin rỗng';

$app_strings['MSG_IS_LESS_THAN'] = 'là ít hơn';

$app_strings['LBL_NUMBER_NEW_CUSTOMER'] = 'Số KHTN mới';

$app_strings['LBL_NUMBER_NEW_CUSTOMER_CENTER_BUYER'] = 'KH mới mua';

$app_strings['LBL_NUMBER_EXIT_CUSTOMER_CENTER_BUYER'] = 'KH củ quay lại';

$app_strings['LBL_NUMBER_CONVERTED_CUSTOMER'] = 'KHTN chuyển đổi';

$app_strings['LBL_NUMBER_INPROCESS_CUSTOMER'] = 'KHTN đang chăm sóc';

$app_strings['LBL_NUMBER_CALL_CENTER'] = 'Cuộc gọi tổng đài';

$app_strings['LBL_REVENUE'] = 'Doanh số';

$app_strings['LBL_EFFECT_DATE'] = 'Ngày bắt đầu';

$app_strings['LBL_EXPIRY_DATE'] = 'Ngày kết thúc';

$app_strings['LBL_NUMBER_EMAIL_QUOTE'] = 'Email báo giá';

$app_strings['LBL_NUMBER_EMAIL'] = 'Email tương tác';

$app_strings['LBL_NUMBER_MEETING'] = 'Cuộc gặp';

$app_strings['LBL_NUMBER_CALL'] = 'Cuộc gọi';

$app_strings['LBL_GROUPTAB6_1615864794'] = 'Sys Admin';

$app_strings['KPI_CUSTOMER'] = 'KPI KHÁCH HÀNG';

$app_strings['KPI_ACTIVITIES'] = 'KPI HOẠT ĐỘNG';
?>