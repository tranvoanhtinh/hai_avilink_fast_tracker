<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2018 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

$app_list_strings['moduleList']['cs_cusUser'] = 'cusUser';
$app_list_strings['user_status_dom']['Active'] = 'Active';
$app_list_strings['user_status_dom']['Inactive'] = 'Inactive';
$app_list_strings['account_classify_type_dom'][''] = '';
$app_list_strings['province_list'][''] = '';
$app_list_strings['province_list'][10] = ' TP Hà Nội';
$app_list_strings['province_list'][16] = ' Tỉnh Hưng Yên';
$app_list_strings['province_list'][17] = ' Tỉnh Hải Dương';
$app_list_strings['province_list'][18] = ' TP Hải Phòng ';
$app_list_strings['province_list'][20] = ' Tỉnh Quảng Ninh ';
$app_list_strings['province_list'][22] = ' Tỉnh Bắc Ninh ';
$app_list_strings['province_list'][23] = ' Tỉnh Bắc Giang ';
$app_list_strings['province_list'][24] = ' Tỉnh Lạng Sơn ';
$app_list_strings['province_list'][25] = ' Tỉnh Thái Nguyên ';
$app_list_strings['province_list'][26] = ' Tỉnh Bắc Kạn ';
$app_list_strings['province_list'][27] = ' Tỉnh Cao Bằng ';
$app_list_strings['province_list'][28] = ' Tỉnh Vĩnh Phúc ';
$app_list_strings['province_list'][29] = ' Tỉnh Phú Thọ ';
$app_list_strings['province_list'][30] = ' Tỉnh Tuyên Quang ';
$app_list_strings['province_list'][31] = ' Tỉnh Hà Giang ';
$app_list_strings['province_list'][32] = ' Tỉnh Yên Bái ';
$app_list_strings['province_list'][33] = ' Tỉnh Lào Cai ';
$app_list_strings['province_list'][35] = ' Tỉnh Hoà Bình ';
$app_list_strings['province_list'][36] = ' Tỉnh Sơn La ';
$app_list_strings['province_list'][38] = ' Tỉnh Điện Biên ';
$app_list_strings['province_list'][39] = ' Tỉnh Lai Châu ';
$app_list_strings['province_list'][40] = ' Tỉnh Hà Nam ';
$app_list_strings['province_list'][41] = ' Tỉnh Thái Bình ';
$app_list_strings['province_list'][42] = ' Tỉnh Nam Định ';
$app_list_strings['province_list'][43] = ' Tỉnh Ninh Bình ';
$app_list_strings['province_list'][44] = ' Tỉnh Thanh Hoá ';
$app_list_strings['province_list'][46] = ' Tỉnh Nghệ An ';
$app_list_strings['province_list'][48] = ' Tỉnh Hà Tĩnh ';
$app_list_strings['province_list'][51] = ' Tỉnh Quảng Bình ';
$app_list_strings['province_list'][52] = ' Tỉnh Quảng Trị ';
$app_list_strings['province_list'][53] = ' Tỉnh Thừa Thiên Huế ';
$app_list_strings['province_list'][55] = ' TP Đà Nẵng ';
$app_list_strings['province_list'][56] = ' Tỉnh Quảng Nam ';
$app_list_strings['province_list'][57] = ' Tỉnh Quảng Ngãi ';
$app_list_strings['province_list'][58] = ' Tỉnh Kon Tum ';
$app_list_strings['province_list'][59] = ' Tỉnh Bình Định ';
$app_list_strings['province_list'][60] = ' Tỉnh Gia Lai ';
$app_list_strings['province_list'][62] = ' Tỉnh Phú Yên ';
$app_list_strings['province_list'][63] = ' Tỉnh Đắk Lăk ';
$app_list_strings['province_list'][64] = ' Tỉnh Đắk Nông ';
$app_list_strings['province_list'][65] = ' Tỉnh Khánh Hoà ';
$app_list_strings['province_list'][66] = ' Tỉnh Ninh Thuận ';
$app_list_strings['province_list'][67] = ' Tỉnh Lâm Đồng ';
$app_list_strings['province_list'][70] = ' TP Hồ Chí Minh ';
$app_list_strings['province_list'][79] = ' Tỉnh Bà Rịa Vũng Tàu ';
$app_list_strings['province_list'][80] = ' Tỉnh Bình Thuận ';
$app_list_strings['province_list'][81] = ' Tỉnh Đồng Nai ';
$app_list_strings['province_list'][82] = ' Tỉnh Bình Dương ';
$app_list_strings['province_list'][83] = ' Tỉnh Bình Phước ';
$app_list_strings['province_list'][84] = ' Tỉnh Tây Ninh ';
$app_list_strings['province_list'][85] = ' Tỉnh Long An ';
$app_list_strings['province_list'][86] = ' Tỉnh Tiền Giang ';
$app_list_strings['province_list'][87] = ' Tỉnh Đồng Tháp ';
$app_list_strings['province_list'][88] = ' Tỉnh An Giang ';
$app_list_strings['province_list'][89] = ' Tỉnh Vĩnh Long ';
$app_list_strings['province_list'][90] = ' TP Cần Thơ ';
$app_list_strings['province_list'][91] = ' Tỉnh Hậu Giang ';
$app_list_strings['province_list'][92] = ' Tỉnh Kiên Giang ';
$app_list_strings['province_list'][93] = ' Tỉnh Bến Tre ';
$app_list_strings['province_list'][94] = ' Tỉnh Trà Vinh ';
$app_list_strings['province_list'][95] = ' Tỉnh Sóc Trăng ';
$app_list_strings['province_list'][96] = ' Tỉnh Bạc Liêu ';
$app_list_strings['province_list'][97] = ' Tỉnh Cà Mau ';
