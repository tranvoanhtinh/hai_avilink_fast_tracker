<?php

$app_list_strings['moduleList']['MTS_Industry'] = 'Ngành nghề';
$app_list_strings['moduleList']['MTS_KPIConfig'] = 'Thiết lập KPI';
$app_list_strings['lead_card_order_by_options'] = array(
    'amount' => 'Doanh số',
    'date_entered' => 'Ngày tạo',
	'status' => 'Tình trạng'
);

$app_list_strings['lead_card_email_type_options'] = array(
    '' => '',
    'Quotation' => 'Báo giá'
);

$app_list_strings['lead_card_filter_period'] = array(
    'this_week' => 'Tuần này',
    'last_week' => 'Tuần trước',
    'this_month' => 'Tháng này',
    'last_month' => 'Tháng trước',
    'this_year' => 'Năm này',
    'last_year' => 'Năm trước',
    'is_between' => 'Từ ngày đến ngày'
);

$app_strings['LBL_NEW_COLUMN_TITLE'] = 'Mới';
$app_strings['LBL_IN_PROCESS_COLUMN_TITLE'] = 'Đang liên hệ';
$app_strings['LBL_CONVERTED_COLUMN_TITLE'] = 'Ký hợp đồng';
$app_strings['LBL_INVOICED_COLUMN_TITLE'] = 'Phát sinh giao dịch';
$app_strings['LBL_ETS_AMOUNT_TITLE'] = 'Doanh thu ước tính';
$app_strings['LBL_COMMISSION_AMOUNT_TITLE'] = 'Hoa hồng tạm tính';
$app_strings['LBL_SEARCH_TITLE'] = 'Tìm kiếm';
$app_strings['LBL_DETAIL_LABEL'] = 'Thông tin chi tiết';
$app_strings['LBL_FILTER_LABEL'] = 'Lọc hoạt động';
$app_strings['LBL_PLEASE_SELECT_A_DATE'] = 'Vui lòng chọn ngày';
$app_strings['LBL_BTN_FILTER'] = 'Lọc';
$app_strings['LBL_DATE_FROM'] = 'From';
$app_strings['LBL_DATE_TO'] = 'To';
$app_strings['LBL_PERIOD'] = 'Period';
$app_strings['LBL_ADD_CALL_MODAL_TITLE'] = 'Tạo cuộc gọi';
$app_strings['LBL_ADD_MEETING_MODAL_TITLE']= 'Tạo cuộc họp';

$app_strings['LBL_CONFIRM_CLOSE_ACTIVITY_QUESTION'] = 'Bạn có chắc chắn muốn đóng hành động này?';
$app_strings['LBL_CLOSE_ACTIVITY_HEADER'] = 'Xác nhận';
$app_strings['LBL_CLOSE'] = 'Đóng';

$app_strings['LBL_QUOTATION'] = 'Báo giá';

$app_strings['LBL_CONFIRM_DEAD_LEAD'] = 'Bạn có chắc chắn muốn ngừng chăm sóc khách hàng này không?';
$app_strings['LBL_CONFIRM_CONVERTED_LEAD'] = 'Bạn có chắc chắn muốn chuyển trạng thái khách hàng này không?';

$app_strings['LBL_CANCEL_BUTTON_LABEL'] = 'Huỷ bỏ';

$app_strings['LBL_INPROCESS_CUSTOMER_PERCENT'] = 'Khách hàng đã liên hệ';
$app_strings['LBL_NEW_CUSTOMER_PERCENT'] = 'Tổng khách hàng mới';

$app_strings['LBL_CONVERTED_CUSTOMER_PERCENT'] = 'Khách hàng đã ký hợp đồng';

$app_strings['LBL_REVENUE_REACHED'] = 'Doanh thu đã đạt (triệu đồng)';
$app_strings['LBL_REVENUE_IN_KPI'] = 'Doanh thu chỉ tiêu (triệu đồng)';

$app_strings['LBL_INPROCESS_CUSTOMER_REPORT_TITLE'] = 'Chỉ tiêu liên hệ khách hàng';
$app_strings['LBL_CONVERTED_CUSTOMER_REPORT_TITLE'] = 'Tỉ lệ khách hàng ký hợp đồng';
$app_strings['LBL_REVENUE_REPORT_TITLE'] = 'Tỉ lệ doanh thu đạt được';
$app_strings['LBL_KPI_REPORT_TITLE'] = 'KPI';
$app_strings['LBL_NUM_CONVERTED_CUSTOMER_REPORT_TITLE'] = 'Khách hàng ký hợp đồng';
$app_strings['LBL_KP_COMMISSION_TITLE'] = 'Hoa hồng';

$app_strings['LBL_NEW_CUSTOMER'] = 'KH Mới';
$app_strings['LBL_INPROCESS_CUSTOMER'] = 'Tương tác KH';

$app_strings['LBL_MONTHS'] = 'Tháng';
$app_strings['LBL_NUMBER_OF_CONTRACT'] = 'Khách hàng ký HĐ';

$app_strings['LBL_KPI_CONFIG_INCORRECT'] = 'Cảnh báo : Không tìm thấy thông tin cấu hình KPI hợp lệ hoặc KPI chưa được cấu hình, vui lòng liên hệ quản trị viên để biết thêm chi tiết!!';


if(!isset($GLOBALS['db']) || empty($GLOBALS['db'])) {
    if(!class_exists('DBManagerFactory')) {
        require_once 'include/database/DBManagerFactory.php';
    }
    $db = DBManagerFactory::getInstance();
    $db->resetQueryCount();
    $GLOBALS['db'] = $db;
}

$app_list_strings['industry_dom'] = getIndustries();


$app_list_strings['meeting_status_dom'] = array(
    'Planned' => 'Lập kế hoạch',
    'Held' => 'Đã hoàn thành'
);

$app_list_strings['call_status_dom'] = array(
    'Planned' => 'Lập kế hoạch',
    'Held' => 'Đã hoàn thành'
);

$app_list_strings['account_type_dom'] = array (
    'Competitor' => 'Đối thủ',
    'Customer' => 'Khách hàng',
    'Investor' => 'Nhà đầu tư',
    'Partner' => 'Đối tác',
    'Other' => 'Khác',
    '' => '',
);$app_list_strings['lead_status_dom'] = array (
    'New' => 'Mới',
    'In Process' => 'Đang chăm sóc',
    'Converted' => 'Đã chuyển đổi',
    'Transaction' => 'Phát sinh giao dịch',
    'Dead' => 'Huỷ chăm sóc',
);

$app_list_strings['account_type_dom']['Agent'] = 'Đại lý';
$app_strings['LBL_ACTIVITIES_SUBPANEL_TITLE'] = 'Hoạt động cần làm';

//////////////////////////////////////////////////////
$app_list_strings['moduleList'] = array (
  'Home' => 'Trang chủ',
  'ResourceCalendar' => 'Calendar',
  'Contacts' => 'Liên hệ',
  'Accounts' => 'Khách hàng',
  'Alerts' => 'Cảnh báo',
  'Opportunities' => 'Cơ hội bán hàng',
  'Cases' => 'Khiếu nại khách hàng',
  'Notes' => 'Ghi chú',
  'Calls' => 'Cuộc gọi',
  'TemplateSectionLine' => 'Bản mẫu phần dòng',
  'Calls_Reschedule' => 'Lịch gọi',
  'Emails' => 'Emails',
  'EAPM' => 'EAPM',
  'Meetings' => 'Cuộc gặp',
  'Tasks' => 'Công việc',
  'Calendar' => 'Lịch làm việc',
  'Leads' => 'KH Tiềm năng',
  'Currencies' => 'Tiền tệ',
  'Activities' => 'Hoạt động',
  'Bugs' => 'Lỗi',
  'Feeds' => 'RSS',
  'iFrames' => 'Trang của tôi',
  'TimePeriods' => 'Khoảng thời gian',
  'ContractTypes' => 'Kiểu hợp đồng',
  'Schedulers' => 'Lịch trình',
  'Project' => 'Dự án',
  'ProjectTask' => 'Dự án những phần việc',
  'Campaigns' => 'Chiến dịch',
  'CampaignLog' => 'Ghi lại chiến dịch',
  'Documents' => 'Tài liệu',
  'DocumentRevisions' => 'Phiên bản tài liệu',
  'Connectors' => 'Kết nối',
  'Roles' => 'Vai trò',
  'Notifications' => 'Chú ý',
  'Sync' => 'Đồng bộ',
  'Users' => 'Người dùng',
  'Employees' => 'Nhân viên',
  'Administration' => 'Quản trị viên',
  'ACLRoles' => 'Vai trò',
  'InboundEmail' => 'Hộp thư đến',
  'Releases' => 'Phát hành',
  'Prospects' => 'KH Mục tiêu',
  'Queues' => 'Hàng đợi',
  'EmailMarketing' => 'Email quảng bá',
  'EmailTemplates' => 'Email - mẫu',
  'ProspectLists' => 'Danh sách kh mục tiêu',
  'SavedSearch' => 'Lưu kết quả tìm kiếm',
  'UpgradeWizard' => 'Trình nâng cấp',
  'Trackers' => 'Theo dõi',
  'TrackerSessions' => 'Theo dõi phiên làm ',
  'TrackerQueries' => 'Theo dõi truy vấn',
  'FAQ' => 'Hỏi đáp',
  'Newsletters' => 'Tin mới',
  'SugarFeed' => 'Nguồn cấp dữ liệu SuiteCRM',
  'SugarFavorites' => 'Đánh dấu SuiteCRM vào mục yêu thích',
  'OAuthKeys' => 'Bảo Mật ',
  'OAuthTokens' => 'Bảo mật',
  'OAuth2Clients' => 'OAuth Clients',
  'OAuth2Tokens' => 'Bảo mật',
  'Library' => 'Thư viện',
  'EmailAddresses' => 'Địa chỉ Email ',
  'KBDocuments' => 'Kiến thức cơ bản',
  'AOK_KnowledgeBase' => 'Kho kiến thức',
  'AOK_Knowledge_Base_Categories' => 'KB - thể loại',
  'FP_events' => 'Sự kiện',
  'FP_Event_Locations' => 'Vị trí',
  'AOD_IndexEvent' => 'Chỉ số sự kiện',
  'AOD_Index' => 'Chỉ số',
  'AOP_Case_Events' => 'Trường hợp các sự kiện',
  'AOP_Case_Updates' => 'Cập nhật trường hợp',
  'AOR_Reports' => 'Báo cáo',
  'AOR_Conditions' => 'Báo cáo điều kiện',
  'AOR_Charts' => 'Biểu đồ báo cáo',
  'AOR_Fields' => 'Báo cáo lĩnh vực',
  'AOR_Scheduled_Reports' => 'Theo lịch trình báo cáo',
  'AOS_Contracts' => 'Hợp đồng',
  'AOS_Invoices' => 'Hóa đơn',
  'AOS_PDF_Templates' => 'Các mẫu PDF',
  'AOS_Product_Categories' => 'Danh mục sản phẩm',
  'AOS_Products' => 'Sản phẩm / Dịch vụ',
  'AOS_Products_Quotes' => 'Các hàng mục',
  'AOS_Line_Item_Groups' => 'Line Item Groups',
  'AOS_Quotes' => 'Báo giá',
  'AOW_WorkFlow' => 'Quy trình công việc',
  'AOW_Conditions' => 'Quy trình làm việc điều kiện',
  'AOW_Processed' => 'Quá trình kiểm toán',
  'AOW_Actions' => 'Quy trình làm việc hành động',
  'AM_ProjectTemplates' => 'Dự án - mẫu',
  'AM_TaskTemplates' => 'Dự án công việc mẫu',
  'jjwg_Maps' => 'Các bản đồ',
  'jjwg_Markers' => 'Bản đồ - đánh dấu',
  'jjwg_Areas' => 'Bản đồ - khu vực',
  'jjwg_Address_Cache' => 'Bản đồ - địa chỉ bộ nhớ Cache',
  'jjwp_Partners' => 'Đối tác JJWP',
  'SecurityGroups' => 'Quản lý nhóm bảo mật',
  'OutboundEmailAccounts' => 'Tài khoản email gửi đi',
  'TemplateEditor' => 'Bản mẫu phần biên tập',
  'Spots' => 'Điểm',
  'AOBH_BusinessHours' => 'Giờ làm việc',
  'SurveyResponses' => 'Phản hồi khảo sát',
  'Surveys' => 'Khảo sát',
  'SurveyQuestionResponses' => 'Trả lời câu hỏi khảo sát',
  'SurveyQuestions' => 'Câu hỏi khảo sát',
  'SurveyQuestionOptions' => 'Tùy chọn câu hỏi khảo sát',
  'JAccount' => 'JAccount',
  'SharedSecurityRulesFields' => 'Các trường được chia sẻ quy tắc bảo mật',
  'SharedSecurityRules' => 'Quy tắc bảo mật chung',
  'SharedSecurityRulesActions' => 'Các hành động quy tắc bảo mật chung',
  'MTS_Industry' => 'Ngành nghề',
  'MTS_KPIConfig' => 'Thiết lập KPI',
  'rls_Reports' => 'Báo cáo động',
  'jckl_DashboardDeployments' => 'Dashboard Deployments',
  'jckl_DashboardTemplates' => 'Dashboard Templates',
  'rls_Dashboards' => 'Dashboard Reports',
  'RLS_Scheduling_Reports' => 'Scheduling Reports',
  'C_SLLTool' => 'Sugar Language Translation Tool',
);
?>