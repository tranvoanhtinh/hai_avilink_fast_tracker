<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2018 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

$app_list_strings['moduleList']['lp_Language_Mobile_App'] = 'Language_Mobile_App';
$app_list_strings['language_dom']['af'] = 'Afrikaans';
$app_list_strings['language_dom']['ar-EG'] = 'Arabic, Egypt';
$app_list_strings['language_dom']['ar-SA'] = 'Arabic, Saudi Arabia';
$app_list_strings['language_dom']['az'] = 'Azerbaijani';
$app_list_strings['language_dom']['bg'] = 'Bulgarian';
$app_list_strings['language_dom']['bn'] = 'Bengali';
$app_list_strings['language_dom']['bs'] = 'Bosnian';
$app_list_strings['language_dom']['ca'] = 'Catalan';
$app_list_strings['language_dom']['ceb'] = 'Cebuano';
$app_list_strings['language_dom']['cs'] = 'Czech';
$app_list_strings['language_dom']['da'] = 'Danish';
$app_list_strings['language_dom']['de'] = 'German';
$app_list_strings['language_dom']['de-CH'] = 'German, Switzerland';
$app_list_strings['language_dom']['el'] = 'Greek';
$app_list_strings['language_dom']['en-GB'] = 'English, United Kingdom';
$app_list_strings['language_dom']['en-US'] = 'English, United States';
$app_list_strings['language_dom']['es-ES'] = 'Spanish';
$app_list_strings['language_dom']['es-MX'] = 'Spanish, Mexico';
$app_list_strings['language_dom']['es-PY'] = 'Spanish, Paraguay';
$app_list_strings['language_dom']['es-VE'] = 'Spanish, Venezuela';
$app_list_strings['language_dom']['et'] = 'Estonian';
$app_list_strings['language_dom']['eu'] = 'Basque';
$app_list_strings['language_dom']['fa'] = 'Persian';
$app_list_strings['language_dom']['fi'] = 'Filipino';
$app_list_strings['language_dom']['fil'] = 'Finnish';
$app_list_strings['language_dom']['fr'] = 'French';
$app_list_strings['language_dom']['fr-CA'] = 'French, Canada';
$app_list_strings['language_dom']['gu-IN'] = 'Gujarati';
$app_list_strings['language_dom']['he'] = 'Hebrew';
$app_list_strings['language_dom']['hi'] = 'Hindi';
$app_list_strings['language_dom']['hr'] = 'Croatian';
$app_list_strings['language_dom']['hu'] = 'Hungarian';
$app_list_strings['language_dom']['hy-AM'] = 'Armenian';
$app_list_strings['language_dom']['id'] = 'Indonesian';
$app_list_strings['language_dom']['it'] = 'Italian';
$app_list_strings['language_dom']['ja'] = 'Japanese';
$app_list_strings['language_dom']['ka'] = 'Georgian';
$app_list_strings['language_dom']['ko'] = 'Korean';
$app_list_strings['language_dom']['lt'] = 'Lithuanian';
$app_list_strings['language_dom']['lv'] = 'Latvian';
$app_list_strings['language_dom']['mk'] = 'Macedonian';
$app_list_strings['language_dom']['nb'] = 'Norwegian Bokmal';
$app_list_strings['language_dom']['nl'] = 'Dutch';
$app_list_strings['language_dom']['pcm'] = 'Nigerian Pidgin';
$app_list_strings['language_dom']['pl'] = 'Polish';
$app_list_strings['language_dom']['pt-BR'] = 'Portuguese, Brazilian';
$app_list_strings['language_dom']['pt-PT'] = 'Portuguese';
$app_list_strings['language_dom']['ro'] = 'Romanian';
$app_list_strings['language_dom']['ru'] = 'Russian';
$app_list_strings['language_dom']['si-LK'] = 'Sinhala';
$app_list_strings['language_dom']['sk'] = 'Slovak';
$app_list_strings['language_dom']['sl'] = 'Slovenian';
$app_list_strings['language_dom']['sq'] = 'Albanian';
$app_list_strings['language_dom']['sr-CS'] = 'Serbian (Latin)';
$app_list_strings['language_dom']['sv-SE'] = 'Swedish';
$app_list_strings['language_dom']['th'] = 'Thái Lan';
$app_list_strings['language_dom']['tl'] = 'Tagalog';
$app_list_strings['language_dom']['tr'] = 'Turkish';
$app_list_strings['language_dom']['uk'] = 'Ukrainian';
$app_list_strings['language_dom']['ur-IN'] = 'Urdu (India)';
$app_list_strings['language_dom']['ur-PK'] = 'Urdu (Pakistan)';
$app_list_strings['language_dom']['vi'] = 'Tiếng Việt';
$app_list_strings['language_dom']['yo'] = 'Yoruba';
$app_list_strings['language_dom']['zh-CN'] = 'Chinese Simplified';
$app_list_strings['language_dom']['zh-TW'] = 'Chinese Traditional';
$app_list_strings['language_dom']['other'] = 'Khác';
$app_list_strings['type_data_config'][1] = '1.Ngôn ngữ';
$app_list_strings['type_data_config'][2] = '2.Màu nền';
$app_list_strings['type_data_config'][3] = '3.Màu chữ';
