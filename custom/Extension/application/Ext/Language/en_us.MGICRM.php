<?php

$app_list_strings['moduleList']['MTS_Industry'] = 'Industries';
$app_list_strings['moduleList']['MTS_KPIConfig'] = 'KPI Configs';

$app_list_strings['lead_card_order_by_options'] = array(
    'amount' => 'Revenue',
    'date_entered' => 'Date Created',
	'status' => 'Status'
);

$app_list_strings['lead_card_email_type_options'] = array(
    '' => '',
    'Quotation' => 'Quote'
);

$app_list_strings['lead_card_filter_period'] = array(
    'this_week' => 'This Week',
    'last_week' => 'Last Week',
    'this_month' => 'This Month',
    'last_month' => 'Last Month',
    'this_year' => 'This Year',
    'last_year' => 'Last Year',
    'is_between' => 'Is Between'
);

$app_strings['LBL_NEW_COLUMN_TITLE'] = 'New';
$app_strings['LBL_IN_PROCESS_COLUMN_TITLE'] = 'In Process';
$app_strings['LBL_CONVERTED_COLUMN_TITLE'] = 'Converted';
$app_strings['LBL_INVOICED_COLUMN_TITLE'] = 'Transaction';
$app_strings['LBL_ETS_AMOUNT_TITLE'] = 'Revenue probability';
$app_strings['LBL_COMMISSION_AMOUNT_TITLE'] = 'Commission';
$app_strings['LBL_SEARCH_TITLE'] = 'Search';
$app_strings['LBL_DETAIL_LABEL'] = 'Detail';
$app_strings['LBL_FILTER_LABEL'] = 'Filter Activities';
$app_strings['LBL_PLEASE_SELECT_A_DATE'] = 'Please fill a date';
$app_strings['LBL_BTN_FILTER'] = 'Filter';
$app_strings['LBL_DATE_FROM'] = 'From';
$app_strings['LBL_DATE_TO'] = 'To';
$app_strings['LBL_PERIOD'] = 'Period';
$app_strings['LBL_ADD_CALL_MODAL_TITLE'] = 'New call';
$app_strings['LBL_ADD_MEETING_MODAL_TITLE']= 'New meeting';

$app_strings['LBL_CONFIRM_CLOSE_ACTIVITY_QUESTION'] = 'Are you sure want to close this activity?';
$app_strings['LBL_CLOSE_ACTIVITY_HEADER'] = 'Confirm';
$app_strings['LBL_CLOSE'] = 'Close';

$app_strings['LBL_QUOTATION'] = 'Quote';

$app_strings['LBL_CONFIRM_DEAD_LEAD'] = 'Are you sure want to stop care on this customer?';
$app_strings['LBL_CONFIRM_CONVERTED_LEAD'] = 'Are you sure want to convert this customer?';

$app_strings['LBL_CANCEL_BUTTON_LABEL'] = 'Cancel';

$app_strings['LBL_INPROCESS_CUSTOMER_PERCENT'] = 'In Process customer';
$app_strings['LBL_NEW_CUSTOMER_PERCENT'] = 'Total New customer';

$app_strings['LBL_CONVERTED_CUSTOMER_PERCENT'] = 'The number client has contract';

$app_strings['LBL_REVENUE_REACHED'] = 'Actually Revenue (Million VND)';
$app_strings['LBL_REVENUE_IN_KPI'] = 'Revenue in KPI (Million VND)';

$app_strings['LBL_INPROCESS_CUSTOMER_REPORT_TITLE'] = 'Percent In Process of New in Period';
$app_strings['LBL_CONVERTED_CUSTOMER_REPORT_TITLE'] = 'Percent Contract of In Process in Period';
$app_strings['LBL_REVENUE_REPORT_TITLE'] = 'Percent Actually revenue';
$app_strings['LBL_KPI_REPORT_TITLE'] = 'KPI';
$app_strings['LBL_NUM_CONVERTED_CUSTOMER_REPORT_TITLE'] = 'Number of Contracts in Period';
$app_strings['LBL_KP_COMMISSION_TITLE'] = 'Commission';

$app_strings['LBL_NEW_CUSTOMER'] = 'New customer';
$app_strings['LBL_INPROCESS_CUSTOMER'] = 'In Process customer';

$app_strings['LBL_MONTHS'] = 'Months';
$app_strings['LBL_NUMBER_OF_CONTRACT'] = 'Number of Contracts';
$app_strings['LBL_KPI_CONFIG_INCORRECT'] = 'Warning: Not found any kpi config is valid or has not configured kpi, Please contact administrator!';

if(!isset($GLOBALS['db']) || empty($GLOBALS['db'])) {
    if(!class_exists('DBManagerFactory')) {
        require_once 'include/database/DBManagerFactory.php';
    }
    $db = DBManagerFactory::getInstance();
    $db->resetQueryCount();
    $GLOBALS['db'] = $db;
}

$app_list_strings['industry_dom'] = getIndustries();

$app_list_strings['lead_status_dom'] = array(
    '' => '',
    'New' => 'New',
    'In Process' => 'In Process',
    'Converted' => 'Converted',
    'Transaction' => 'Transaction',
    'Dead' => 'Dead',
);

$app_list_strings['meeting_status_dom'] = array(
    'Planned' => 'Planned',
    'Held' => 'Held'
);

$app_list_strings['call_status_dom'] = array(
    'Planned' => 'Planned',
    'Held' => 'Held'
);

$app_list_strings['account_type_dom'] = array (
    '' => '',
    'Customer' => 'Customer',
    'Competitor' => 'Competitor',
    'Investor' => 'Investor',
    'Partner' => 'Partner',
    'Other' => 'Other',
);
$app_list_strings['sales_stage_dom'] = array (
    'Prospecting' => 'Prospecting',
    'Proposal/Price Quote' => 'Proposal/Price Quote',
    'Negotiation/Review' => 'Negotiation/Review',
    'Closed Won' => 'Closed Won',
    'Closed Lost' => 'Closed Lost',
);

$app_list_strings['account_type_dom']['Agent'] = 'Agent';
?>