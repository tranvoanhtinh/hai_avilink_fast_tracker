<?php
// created: 2020-12-09 05:40:10
$dictionary["tttt_History_calls"]["fields"]["accounts_tttt_history_calls_1"] = array (
  'name' => 'accounts_tttt_history_calls_1',
  'type' => 'link',
  'relationship' => 'accounts_tttt_history_calls_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_TTTT_HISTORY_CALLS_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_tttt_history_calls_1accounts_ida',
);
$dictionary["tttt_History_calls"]["fields"]["accounts_tttt_history_calls_1_name"] = array (
  'name' => 'accounts_tttt_history_calls_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_TTTT_HISTORY_CALLS_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_tttt_history_calls_1accounts_ida',
  'link' => 'accounts_tttt_history_calls_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["tttt_History_calls"]["fields"]["accounts_tttt_history_calls_1accounts_ida"] = array (
  'name' => 'accounts_tttt_history_calls_1accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_tttt_history_calls_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_TTTT_HISTORY_CALLS_1_FROM_TTTT_HISTORY_CALLS_TITLE',
);
