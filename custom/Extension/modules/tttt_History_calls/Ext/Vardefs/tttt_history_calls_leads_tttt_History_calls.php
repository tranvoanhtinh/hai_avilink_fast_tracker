<?php


$dictionary["tttt_History_calls"]["fields"]["customernumberfrom"]=array(
 
    'required' => false,
    'name' => 'customernumberfrom',
    'vname' => 'LBL_CUSTOMERNUMBERFROM',
    'type' => 'varchar',
    'massupdate' => 0,
    'no_default' => false,
    'comments' => '',
    'help' => '',
    'importable' => 'true',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => false,
    'inline_edit' => true,
    'reportable' => true,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'len' => '50',
    'size' => '20',
  );
 $dictionary["tttt_History_calls"]["fields"]['customernumberto']= array (
    'required' => false,
    'name' => 'customernumberto',
    'vname' => 'LBL_CUSTOMERNUMBERTO',
    'type' => 'varchar',
    'massupdate' => 0,
    'no_default' => false,
    'comments' => '',
    'help' => '',
    'importable' => 'true',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => false,
    'inline_edit' => true,
    'reportable' => true,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'len' => '50',
    'size' => '20',
  );