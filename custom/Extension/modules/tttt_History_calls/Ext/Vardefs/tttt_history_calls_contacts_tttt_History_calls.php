<?php
// created: 2018-12-28 09:39:18
$dictionary["tttt_History_calls"]["fields"]["tttt_history_calls_contacts"] = array (
  'name' => 'tttt_history_calls_contacts',
  'type' => 'link',
  'relationship' => 'tttt_history_calls_contacts',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_TTTT_HISTORY_CALLS_CONTACTS_FROM_CONTACTS_TITLE',
);
