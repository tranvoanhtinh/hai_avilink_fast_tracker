<?php
 // created: 2020-12-21 11:31:23
$dictionary["cs_cusUser"]["fields"]["Allowance4"] = array(
   'required' => false,
    'name' => 'Allowance4',
    'vname' => 'LBL_ALLOWANCE4',
    'type' => 'decimal',
    'massupdate' => true,
	'len' => '18',
    'size' => '20',
    'enable_range_search' => false,
    'precision' => '0',
);

 ?>