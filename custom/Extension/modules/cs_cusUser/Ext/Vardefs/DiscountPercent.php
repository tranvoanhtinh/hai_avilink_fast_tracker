<?php
 // created: 2020-12-21 11:31:23
$dictionary["cs_cusUser"]["fields"]["DiscountPercent"] = array(
   'required' => false,
    'name' => 'DiscountPercent',
    'vname' => 'LBL_DISCOUNT_PERCENT',
    'type' => 'int',
    'massupdate' => true,
	'len' => '18',
    'size' => '20',
    'enable_range_search' => false,
 'default_value' => '0',
    'precision' => '4',
);

 ?>