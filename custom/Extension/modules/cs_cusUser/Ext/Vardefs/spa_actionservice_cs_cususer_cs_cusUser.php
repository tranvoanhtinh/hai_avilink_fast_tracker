<?php
// created: 2023-05-23 08:57:15
$dictionary["cs_cusUser"]["fields"]["spa_actionservice_cs_cususer"] = array (
  'name' => 'spa_actionservice_cs_cususer',
  'type' => 'link',
  'relationship' => 'spa_actionservice_cs_cususer',
  'source' => 'non-db',
  'module' => 'spa_ActionService',
  'bean_name' => 'spa_ActionService',
  'side' => 'right',
  'vname' => 'LBL_SPA_ACTIONSERVICE_CS_CUSUSER_FROM_SPA_ACTIONSERVICE_TITLE',
);
