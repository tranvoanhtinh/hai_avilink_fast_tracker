<?php
// created: 2023-05-05 09:44:56
$dictionary["cs_cusUser"]["fields"]["pr_payrollparam_cs_cususer"] = array (
  'name' => 'pr_payrollparam_cs_cususer',
  'type' => 'link',
  'relationship' => 'pr_payrollparam_cs_cususer',
  'source' => 'non-db',
  'module' => 'pr_PayrollParam',
  'bean_name' => 'pr_PayrollParam',
  'vname' => 'LBL_PR_PAYROLLPARAM_CS_CUSUSER_FROM_PR_PAYROLLPARAM_TITLE',
  'id_name' => 'pr_payrollparam_cs_cususerpr_payrollparam_ida',
);
$dictionary["cs_cusUser"]["fields"]["pr_payrollparam_cs_cususer_name"] = array (
  'name' => 'pr_payrollparam_cs_cususer_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_PR_PAYROLLPARAM_CS_CUSUSER_FROM_PR_PAYROLLPARAM_TITLE',
  'save' => true,
  'id_name' => 'pr_payrollparam_cs_cususerpr_payrollparam_ida',
  'link' => 'pr_payrollparam_cs_cususer',
  'table' => 'pr_payrollparam',
  'module' => 'pr_PayrollParam',
  'rname' => 'name',
);
$dictionary["cs_cusUser"]["fields"]["pr_payrollparam_cs_cususerpr_payrollparam_ida"] = array (
  'name' => 'pr_payrollparam_cs_cususerpr_payrollparam_ida',
  'type' => 'link',
  'relationship' => 'pr_payrollparam_cs_cususer',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_PR_PAYROLLPARAM_CS_CUSUSER_FROM_CS_CUSUSER_TITLE',
);
