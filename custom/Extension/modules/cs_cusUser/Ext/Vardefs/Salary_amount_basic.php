<?php
 // created: 2020-12-21 11:31:23
$dictionary["cs_cusUser"]["fields"]["Salary_Amount_basic"] = array(
   'required' => false,
    'name' => 'Salary_Amount_basic',
    'vname' => 'LBL_SALARY_AMOUNT_BASIC',
    'type' => 'decimal',
    'massupdate' => true,
	'len' => '18',
    'size' => '20',
    'enable_range_search' => false,
    'precision' => '0',
);

 ?>