<?php
// created: 2023-05-23 06:52:50
$dictionary["cs_cusUser"]["fields"]["cs_cususer_bill_billing_1"] = array (
  'name' => 'cs_cususer_bill_billing_1',
  'type' => 'link',
  'relationship' => 'cs_cususer_bill_billing_1',
  'source' => 'non-db',
  'module' => 'bill_Billing',
  'bean_name' => 'bill_Billing',
  'side' => 'right',
  'vname' => 'LBL_CS_CUSUSER_BILL_BILLING_1_FROM_BILL_BILLING_TITLE',
);
