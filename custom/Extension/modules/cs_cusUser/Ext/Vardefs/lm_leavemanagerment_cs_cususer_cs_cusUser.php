<?php
// created: 2023-06-06 10:14:10
$dictionary["cs_cusUser"]["fields"]["lm_leavemanagerment_cs_cususer"] = array (
  'name' => 'lm_leavemanagerment_cs_cususer',
  'type' => 'link',
  'relationship' => 'lm_leavemanagerment_cs_cususer',
  'source' => 'non-db',
  'module' => 'lm_LeaveManagerment',
  'bean_name' => false,
  'side' => 'right',
  'vname' => 'LBL_LM_LEAVEMANAGERMENT_CS_CUSUSER_FROM_LM_LEAVEMANAGERMENT_TITLE',
);
