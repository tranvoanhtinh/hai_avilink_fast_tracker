<?php
// created: 2023-05-05 09:44:55
$dictionary["cs_cusUser"]["fields"]["pr_payrollfile_cs_cususer"] = array (
  'name' => 'pr_payrollfile_cs_cususer',
  'type' => 'link',
  'relationship' => 'pr_payrollfile_cs_cususer',
  'source' => 'non-db',
  'module' => 'pr_PayrollFile',
  'bean_name' => false,
  'vname' => 'LBL_PR_PAYROLLFILE_CS_CUSUSER_FROM_PR_PAYROLLFILE_TITLE',
  'id_name' => 'pr_payrollfile_cs_cususerpr_payrollfile_ida',
);
$dictionary["cs_cusUser"]["fields"]["pr_payrollfile_cs_cususer_name"] = array (
  'name' => 'pr_payrollfile_cs_cususer_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_PR_PAYROLLFILE_CS_CUSUSER_FROM_PR_PAYROLLFILE_TITLE',
  'save' => true,
  'id_name' => 'pr_payrollfile_cs_cususerpr_payrollfile_ida',
  'link' => 'pr_payrollfile_cs_cususer',
  'table' => 'pr_payrollfile',
  'module' => 'pr_PayrollFile',
  'rname' => 'name',
);
$dictionary["cs_cusUser"]["fields"]["pr_payrollfile_cs_cususerpr_payrollfile_ida"] = array (
  'name' => 'pr_payrollfile_cs_cususerpr_payrollfile_ida',
  'type' => 'link',
  'relationship' => 'pr_payrollfile_cs_cususer',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'left',
  'vname' => 'LBL_PR_PAYROLLFILE_CS_CUSUSER_FROM_PR_PAYROLLFILE_TITLE',
);
