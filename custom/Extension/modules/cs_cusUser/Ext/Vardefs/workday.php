<?php
 // created: 2020-12-21 11:31:23
$dictionary["cs_cusUser"]["fields"]["Workday"] = array(
   'required' => false,
    'name' => 'Workday',
    'vname' => 'WORKDAY',
    'type' => 'decimal',
    'massupdate' => true,
	'len' => '18',
    'size' => '20',
    'enable_range_search' => false,
    'precision' => '2',
    
);

 ?>