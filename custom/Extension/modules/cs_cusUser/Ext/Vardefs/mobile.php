<?php
 // created: 2020-12-21 11:31:23
$dictionary["cs_cusUser"]["fields"]["mobile"] = array(
   'required' => true,
    'name' => 'mobile',
    'vname' => 'LBL_MOBILE',
    'type' => 'varchar',
    'massupdate' => 0,
    'no_default' => false,
    'comments' => '',
    'help' => '',
    'importable' => 'true',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => false,
    'inline_edit' => true,
    'reportable' => true,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'len' => '100',
    'size' => '20',
);

 ?>