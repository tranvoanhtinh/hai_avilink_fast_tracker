<?php
 // created: 2023-06-06 10:14:10
$layout_defs["cs_cusUser"]["subpanel_setup"]['lm_leavemanagerment_cs_cususer'] = array (
  'order' => 100,
  'module' => 'lm_LeaveManagerment',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LM_LEAVEMANAGERMENT_CS_CUSUSER_FROM_LM_LEAVEMANAGERMENT_TITLE',
  'get_subpanel_data' => 'lm_leavemanagerment_cs_cususer',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
