<?php
 // created: 2019-06-25 12:06:08
$dictionary['EmailTemplate']['fields']['sms_module_c']['inline_edit']=1;
$dictionary["EmailTemplate"]["fields"]["template_id_c"] = array(
    "name" => "template_id_c",
    "type" => "varchar",
    "len" => 20,
    "vname" => "LBL_TEMPLATE_ID",
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);
 ?>