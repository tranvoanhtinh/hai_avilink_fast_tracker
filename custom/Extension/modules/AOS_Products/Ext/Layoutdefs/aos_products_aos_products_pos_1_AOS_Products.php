<?php
 // created: 2024-01-17 10:25:15
$layout_defs["AOS_Products"]["subpanel_setup"]['aos_products_aos_products_pos_1'] = array (
  'order' => 100,
  'module' => 'AOS_Products_Pos',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_AOS_PRODUCTS_AOS_PRODUCTS_POS_1_FROM_AOS_PRODUCTS_POS_TITLE',
  'get_subpanel_data' => 'aos_products_aos_products_pos_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
