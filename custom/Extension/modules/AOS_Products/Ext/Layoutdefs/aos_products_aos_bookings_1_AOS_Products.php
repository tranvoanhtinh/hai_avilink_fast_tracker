<?php
 // created: 2024-05-23 06:25:38
$layout_defs["AOS_Products"]["subpanel_setup"]['aos_products_aos_bookings_1'] = array (
  'order' => 100,
  'module' => 'AOS_Bookings',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_AOS_PRODUCTS_AOS_BOOKINGS_1_FROM_AOS_BOOKINGS_TITLE',
  'get_subpanel_data' => 'aos_products_aos_bookings_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
