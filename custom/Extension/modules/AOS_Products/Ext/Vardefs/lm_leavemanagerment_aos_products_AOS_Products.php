<?php
// created: 2023-06-06 10:14:10
$dictionary["AOS_Products"]["fields"]["lm_leavemanagerment_aos_products"] = array (
  'name' => 'lm_leavemanagerment_aos_products',
  'type' => 'link',
  'relationship' => 'lm_leavemanagerment_aos_products',
  'source' => 'non-db',
  'module' => 'lm_LeaveManagerment',
  'bean_name' => false,
  'side' => 'right',
  'vname' => 'LBL_LM_LEAVEMANAGERMENT_AOS_PRODUCTS_FROM_LM_LEAVEMANAGERMENT_TITLE',
);
