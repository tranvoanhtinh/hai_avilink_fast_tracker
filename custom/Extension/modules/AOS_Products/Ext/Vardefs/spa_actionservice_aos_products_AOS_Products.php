<?php
// created: 2023-05-23 08:57:15
$dictionary["AOS_Products"]["fields"]["spa_actionservice_aos_products"] = array (
  'name' => 'spa_actionservice_aos_products',
  'type' => 'link',
  'relationship' => 'spa_actionservice_aos_products',
  'source' => 'non-db',
  'module' => 'spa_ActionService',
  'bean_name' => 'spa_ActionService',
  'side' => 'right',
  'vname' => 'LBL_SPA_ACTIONSERVICE_AOS_PRODUCTS_FROM_SPA_ACTIONSERVICE_TITLE',
);
