<?php
 // created: 2023-09-21 06:44:22
$dictionary['AOS_Products']['fields']['unitcode']['name']='unitcode';
$dictionary['AOS_Products']['fields']['unitcode']['vname']='LBL_UNITCODE';
$dictionary['AOS_Products']['fields']['unitcode']['len']='100';
$dictionary['AOS_Products']['fields']['unitcode']['type']='enum';
$dictionary['AOS_Products']['fields']['unitcode']['audited']=true;
$dictionary['AOS_Products']['fields']['unitcode']['inline_edit']=true;
$dictionary['AOS_Products']['fields']['unitcode']['massupdate']='1';
$dictionary['AOS_Products']['fields']['unitcode']['options']='unit_list';
$dictionary['AOS_Products']['fields']['unitcode']['merge_filter']='disabled';

 ?>