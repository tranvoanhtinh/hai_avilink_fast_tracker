<?php
$dictionary["AOS_Products"]["fields"]['description_goods_en']=array (
      'name' => 'description_goods_en',
      'vname' => 'LBL_DESCRIPTION_GOODS_EN',
      'type' => 'text',
      'rows' => '4',
      'cols' => 80,
      'required' => false,
      'inline_edit' => false,
      'merge_filter' => 'disabled',
    );
$dictionary["AOS_Products"]["fields"]['description_goods_vn']=array (
      'name' => 'description_goods_vn',
      'vname' => 'LBL_DESCRIPTION_GOODS_VN',
      'type' => 'text',
      'rows' => '4',
      'cols' => 80,
      'required' => false,
      'inline_edit' => false,
      'merge_filter' => 'disabled',
    );


$dictionary["AOS_Products"]["fields"]["update_price"] = array(
    'name' => 'update_price',
    'vname' => 'LBL_UPDATE_PRICE',
    'len' => '100',
    'type' => 'enum',
    'audited' => true,
    'inline_edit' => true,
    'massupdate' => '1',
    'options' => 'update_price_list',
    'merge_filter' => 'disabled',
    'default'=>'2',

);



$dictionary["AOS_Products"]["fields"]["exchange_rate"] = array(
    'name' => 'exchange_rate',
    'vname' => 'LBL_EXCHANGE_RATE',
    'type' => 'currency',
    'massupdate' => 0,
    'comments' => '',
    'help' => '',
    'importable' => 'true',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => 1,
    'reportable' => true,
    'len' => '26,6',
    'default'=>24000.0,
);


$dictionary["AOS_Products"]["fields"]["type_currency"] = array(
    'name' => 'type_currency',
    'vname' => 'LBL_TYPE_CURRENCY',
    'len' => '100',
    'type' => 'enum',
    'audited' => true,
    'inline_edit' => true,
    'massupdate' => '1',
    'options' => 'type_currency_list',
    'merge_filter' => 'disabled',

);




$dictionary["AOS_Products"]["fields"]["vnd"] = array(
    'name' => 'vnd',
    'vname' => 'LBL_VND',
    'type' => 'currency',
    'massupdate' => 0,
    'comments' => '',
    'help' => '',
    'importable' => 'true',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => 1,
    'reportable' => true,
    'len' => '26,6',
);

$dictionary["AOS_Products"]["fields"]["usd"] = array(
    'name' => 'usd',
    'vname' => 'LBL_USD',
    'type' => 'currency',
    'massupdate' => 0,
    'comments' => '',
    'help' => '',
    'importable' => 'true',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => 1,
    'reportable' => true,
    'len' => '26,6',
);


$dictionary["AOS_Products"]["fields"]["type_service"] = array(
    'name' => 'type_service',
    'vname' => 'LBL_TYPE_SERVICE',
    'len' => '100',
    'type' => 'enum',
    'audited' => true,
    'inline_edit' => true,
    'massupdate' => '1',
    'options' => 'type_service_list',
    'merge_filter' => 'disabled',
    'required'=> true,

);

