<?php
// created: 2024-04-15 12:21:09
$dictionary["AOS_Products"]["fields"]["aos_products_aos_packing_1"] = array (
  'name' => 'aos_products_aos_packing_1',
  'type' => 'link',
  'relationship' => 'aos_products_aos_packing_1',
  'source' => 'non-db',
  'module' => 'AOS_Packing',
  'bean_name' => 'AOS_Packing',
  'vname' => 'LBL_AOS_PRODUCTS_AOS_PACKING_1_FROM_AOS_PACKING_TITLE',
);
