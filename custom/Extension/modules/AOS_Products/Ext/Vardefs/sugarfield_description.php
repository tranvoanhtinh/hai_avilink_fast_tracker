<?php
 // created: 2024-01-10 04:16:31
$dictionary['AOS_Products']['fields']['description']['inline_edit']=true;
$dictionary['AOS_Products']['fields']['description']['comments']='Full text of the note';
$dictionary['AOS_Products']['fields']['description']['merge_filter']='disabled';
$dictionary['AOS_Products']['fields']['description']['rows']='10';
$dictionary['AOS_Products']['fields']['description']['cols']='50';

 ?>