<?php
 // created: 2024-03-30 14:38:04
$dictionary['AOS_Products']['fields']['name']['required']=true;
$dictionary['AOS_Products']['fields']['name']['inline_edit']='';
$dictionary['AOS_Products']['fields']['name']['duplicate_merge']='disabled';
$dictionary['AOS_Products']['fields']['name']['duplicate_merge_dom_value']='0';
$dictionary['AOS_Products']['fields']['name']['merge_filter']='disabled';
$dictionary['AOS_Products']['fields']['name']['unified_search']=false;

 ?>