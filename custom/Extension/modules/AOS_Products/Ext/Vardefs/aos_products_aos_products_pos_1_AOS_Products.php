<?php
// created: 2024-01-17 10:25:15
$dictionary["AOS_Products"]["fields"]["aos_products_aos_products_pos_1"] = array (
  'name' => 'aos_products_aos_products_pos_1',
  'type' => 'link',
  'relationship' => 'aos_products_aos_products_pos_1',
  'source' => 'non-db',
  'module' => 'AOS_Products_Pos',
  'bean_name' => 'AOS_Products_Pos',
  'side' => 'right',
  'vname' => 'LBL_AOS_PRODUCTS_AOS_PRODUCTS_POS_1_FROM_AOS_PRODUCTS_POS_TITLE',
);
