<?php
// created: 2023-05-23 05:07:13
$dictionary["lm_LeaveManagerment"]["fields"]["lm_leavemanagerment_cs_cususer"] = array (
  'name' => 'lm_leavemanagerment_cs_cususer',
  'type' => 'link',
  'relationship' => 'lm_leavemanagerment_cs_cususer',
  'source' => 'non-db',
  'module' => 'cs_cusUser',
  'bean_name' => 'cs_cusUser',
  'vname' => 'LBL_LM_LEAVEMANAGERMENT_CS_CUSUSER_FROM_CS_CUSUSER_TITLE',
  'id_name' => 'lm_leavemanagerment_cs_cususercs_cususer_ida',
);
$dictionary["lm_LeaveManagerment"]["fields"]["lm_leavemanagerment_cs_cususer_name"] = array (
  'name' => 'lm_leavemanagerment_cs_cususer_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LM_LEAVEMANAGERMENT_CS_CUSUSER_FROM_CS_CUSUSER_TITLE',
  'save' => true,
  'id_name' => 'lm_leavemanagerment_cs_cususercs_cususer_ida',
  'link' => 'lm_leavemanagerment_cs_cususer',
  'table' => 'cs_cususer',
  'module' => 'cs_cusUser',
  'rname' => 'name',
);
$dictionary["lm_LeaveManagerment"]["fields"]["lm_leavemanagerment_cs_cususercs_cususer_ida"] = array (
  'name' => 'lm_leavemanagerment_cs_cususercs_cususer_ida',
  'type' => 'link',
  'relationship' => 'lm_leavemanagerment_cs_cususer',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_LM_LEAVEMANAGERMENT_CS_CUSUSER_FROM_LM_LEAVEMANAGERMENT_TITLE',
);
