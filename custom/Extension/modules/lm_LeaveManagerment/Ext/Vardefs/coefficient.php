<?php
 // created: 2020-12-21 11:31:23
$dictionary["lm_LeaveManagerment"]["fields"]["coefficient"] = array(
   'required' => false,
    'name' => 'coefficient',
    'vname' => 'LBL_COEFFICIENT',
    'type' => 'decimal',
    'massupdate' => true,
   'len' => '18',
    'size' => '20',
    'enable_range_search' => false,
    'precision' => '2',
   'default_value' => '1',
);

 ?>