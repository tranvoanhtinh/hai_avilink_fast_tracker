<?php
// created: 2023-05-23 05:07:13
$dictionary["lm_LeaveManagerment"]["fields"]["lm_leavemanagerment_aos_products"] = array (
  'name' => 'lm_leavemanagerment_aos_products',
  'type' => 'link',
  'relationship' => 'lm_leavemanagerment_aos_products',
  'source' => 'non-db',
  'module' => 'AOS_Products',
  'bean_name' => 'AOS_Products',
  'vname' => 'LBL_LM_LEAVEMANAGERMENT_AOS_PRODUCTS_FROM_AOS_PRODUCTS_TITLE',
  'id_name' => 'lm_leavemanagerment_aos_productsaos_products_ida',
);
$dictionary["lm_LeaveManagerment"]["fields"]["lm_leavemanagerment_aos_products_name"] = array (
  'name' => 'lm_leavemanagerment_aos_products_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LM_LEAVEMANAGERMENT_AOS_PRODUCTS_FROM_AOS_PRODUCTS_TITLE',
  'save' => true,
  'id_name' => 'lm_leavemanagerment_aos_productsaos_products_ida',
  'link' => 'lm_leavemanagerment_aos_products',
  'table' => 'aos_products',
  'module' => 'AOS_Products',
  'rname' => 'name',
);
$dictionary["lm_LeaveManagerment"]["fields"]["lm_leavemanagerment_aos_productsaos_products_ida"] = array (
  'name' => 'lm_leavemanagerment_aos_productsaos_products_ida',
  'type' => 'link',
  'relationship' => 'lm_leavemanagerment_aos_products',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_LM_LEAVEMANAGERMENT_AOS_PRODUCTS_FROM_LM_LEAVEMANAGERMENT_TITLE',
);
