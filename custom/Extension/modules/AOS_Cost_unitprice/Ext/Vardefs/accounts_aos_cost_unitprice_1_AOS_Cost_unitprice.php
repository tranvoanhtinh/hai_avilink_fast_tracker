<?php
// created: 2024-05-27 03:35:52
$dictionary["AOS_Cost_unitprice"]["fields"]["accounts_aos_cost_unitprice_1"] = array (
  'name' => 'accounts_aos_cost_unitprice_1',
  'type' => 'link',
  'relationship' => 'accounts_aos_cost_unitprice_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_AOS_COST_UNITPRICE_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_aos_cost_unitprice_1accounts_ida',
);
$dictionary["AOS_Cost_unitprice"]["fields"]["accounts_aos_cost_unitprice_1_name"] = array (
  'name' => 'accounts_aos_cost_unitprice_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_AOS_COST_UNITPRICE_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_aos_cost_unitprice_1accounts_ida',
  'link' => 'accounts_aos_cost_unitprice_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["AOS_Cost_unitprice"]["fields"]["accounts_aos_cost_unitprice_1accounts_ida"] = array (
  'name' => 'accounts_aos_cost_unitprice_1accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_aos_cost_unitprice_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_AOS_COST_UNITPRICE_1_FROM_AOS_COST_UNITPRICE_TITLE',
);
