<?php
$dictionary["AOS_Cost_unitprice"]["fields"]["service_code"] = array(
    'name' => 'service_code',
    'vname' => 'LBL_SERVICE_CODE',
    'type' => 'varchar',
    "len" => 36,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);

$dictionary["AOS_Cost_unitprice"]["fields"]["price"] = array(
    'name' => 'price',
    'vname' => 'LBL_PRICE',
    'type' => 'currency',
    'massupdate' => '1',
    'comments' => '',
    'help' => '',
    'importable' => 'true',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => 1,
    'reportable' => true,
    'len' => '26,6',
);

$dictionary["AOS_Cost_unitprice"]["fields"]["service_date"] = array(
    'name' => 'service_date',
    'vname' => 'LBL_SERVICE_DATE',
    'type' => 'date',
    'audited' => true,
    'required' => false,
    'enable_range_search' => true,
    'options' => 'date_range_search_dom',
    "massupdate" => false,
);

$dictionary["AOS_Cost_unitprice"]["fields"]["status"] = array(
    'name' => 'status',
    'vname' => 'LBL_STATUS',
    'len' => '100',
    'type' => 'enum',
    'audited' => true,
    'inline_edit' => true,
    'massupdate' => '1',
    'options' => 'uniprice_status_list',
    'merge_filter' => 'disabled',

);


$dictionary['AOS_Cost_unitprice']['fields']['prioritize']= array (
    'name' => 'prioritize',
    'vname' => 'LBL_PRIORITIZE',
    'type' => 'bool',
    "default" => 0,
    'inline_edit' => false,
);


$dictionary["AOS_Cost_unitprice"]["fields"]["product_id"] = array(
    'name' => 'product_id',
    'vname' => 'LBL_PRODUCT_ID',
    'type' => 'varchar',
    "len" => 50,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);
$dictionary["AOS_Cost_unitprice"]["fields"]["account_id"] = array(
    'name' => 'account_id',
    'vname' => 'LBL_ACCOUNT_ID',
    'type' => 'varchar',
    "len" => 50,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);





$dictionary["AOS_Cost_unitprice"]["fields"]["exchange_rate"] = array(
    'name' => 'exchange_rate',
    'vname' => 'LBL_EXCHANGE_RATE',
    'type' => 'currency',
    'massupdate' => 0,
    'comments' => '',
    'help' => '',
    'importable' => 'true',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => 1,
    'reportable' => true,
    'len' => '26,6',
);


$dictionary["AOS_Cost_unitprice"]["fields"]["type_currency"] = array(
    'name' => 'type_currency',
    'vname' => 'LBL_TYPE_CURRENCY',
    'len' => '100',
    'type' => 'enum',
    'audited' => true,
    'inline_edit' => true,
    'massupdate' => '1',
    'options' => 'type_currency_list',
    'merge_filter' => 'disabled',

);

$dictionary["AOS_Cost_unitprice"]["fields"]["vnd"] = array(
    'name' => 'vnd',
    'vname' => 'LBL_VND',
    'type' => 'currency',
    'massupdate' => 0,
    'comments' => '',
    'help' => '',
    'importable' => 'true',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => 1,
    'reportable' => true,
    'len' => '26,6',
);

$dictionary["AOS_Cost_unitprice"]["fields"]["usd"] = array(
    'name' => 'usd',
    'vname' => 'LBL_USD',
    'type' => 'currency',
    'massupdate' => 0,
    'comments' => '',
    'help' => '',
    'importable' => 'true',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => 1,
    'reportable' => true,
    'len' => '26,6',
);