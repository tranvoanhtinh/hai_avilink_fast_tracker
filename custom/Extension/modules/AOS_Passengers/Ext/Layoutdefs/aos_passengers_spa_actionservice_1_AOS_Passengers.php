<?php
 // created: 2024-08-08 04:06:36
$layout_defs["AOS_Passengers"]["subpanel_setup"]['aos_passengers_spa_actionservice_1'] = array (
  'order' => 100,
  'module' => 'spa_ActionService',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_AOS_PASSENGERS_SPA_ACTIONSERVICE_1_FROM_SPA_ACTIONSERVICE_TITLE',
  'get_subpanel_data' => 'aos_passengers_spa_actionservice_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
