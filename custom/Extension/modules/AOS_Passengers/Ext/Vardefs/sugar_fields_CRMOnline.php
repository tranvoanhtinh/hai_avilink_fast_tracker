<?php 

$dictionary["AOS_Passengers"]["fields"]["sex"] = array(
    "name" => "sex",
    "vname" => "LBL_SEX",
    "type" => "enum",
    "options" => "sex_list",
    "default" => "",
);

$dictionary["AOS_Passengers"]["fields"]["passport"] = array(
    'name' => 'passport',
    'vname' => 'LBL_PASSPORT',
    'type' => 'varchar',
    "len" => 255,
    "importable" => true,
    "audited" => true,
    "massupdate" => false,
    'required'=> true
);


$dictionary["AOS_Passengers"]["fields"]["nationality"] = array(
    'name' => 'nationality',
    'vname' => 'LBL_NATIONALITY',
    'len' => '100',
    'type' => 'enum',
    'audited' => true,
    'inline_edit' => true,
    'massupdate' => '1',
    'options' => 'nationality_list',
    'merge_filter' => 'disabled',
    'studio' => true,
    'required'=> true

);
$dictionary['AOS_Passengers']['fields']['phone_mobile'] =  array (
    'name' => 'phone_mobile',
    'vname' => 'LBL_PHONE_MOBILE',
    'type' => 'varchar',
    "len" => 50,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);

$dictionary['AOS_Passengers']['fields']['phone_other'] =  array (
    'name' => 'phone_other',
    'vname' => 'LBL_PHONE_OTHER',
    'type' => 'varchar',
    "len" => 50,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);

$dictionary['AOS_Passengers']['fields']['phone_other'] =  array (
    'name' => 'phone_other',
    'vname' => 'LBL_PHONE_OTHER',
    'type' => 'varchar',
    "len" => 50,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);

 $dictionary["AOS_Passengers"]["fields"]["photo2"] = array(
    'name' => 'photo2',
    'vname' => 'LBL_PHOTO2',
    'type' => 'image',
    'massupdate' => false,
    'comments' => '',
    'help' => '',
    'importable' => false,
    'reportable' => true,
    'len' => 255,
    'dbType' => 'varchar',
    'width' => '160',
    'height' => '160',
    'studio' => array('listview' => true),
);
$dictionary["AOS_Passengers"]["fields"]["nationality"] = array(
    'name' => 'nationality',
    'vname' => 'LBL_NATIONALITY',
    'type' => 'varchar',
    "len" => 50,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,

);