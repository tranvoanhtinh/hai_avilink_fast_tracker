<?php
 // created: 2024-05-29 10:39:42
$dictionary['AOS_Bookings']['fields']['booking_date']['display_default']='now';
$dictionary['AOS_Bookings']['fields']['booking_date']['inline_edit']=true;
$dictionary['AOS_Bookings']['fields']['booking_date']['merge_filter']='disabled';
$dictionary['AOS_Bookings']['fields']['booking_date']['enable_range_search']=false;

 ?>