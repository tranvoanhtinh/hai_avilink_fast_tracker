<?php
$dictionary["AOS_Bookings"]["fields"]["booking_date"] = array(
    'name' => 'booking_date',
    'vname' => 'LBL_BOOKING_DATE',
    'type' => 'date',
    'audited' => true,
    'required' => false,
    "massupdate" => false,
    'display_default' => 'now',
);

$dictionary["AOS_Bookings"]["fields"]["date_of_service"] = array(
    'name' => 'date_of_service',
    'vname' => 'LBL_DATE_OF_SERVICE',
    'type' => 'datetimecombo',
    'massupdate' => '0',
    'default' => '',
    'no_default' => false,
    'comments' => '',
    'help' => '',
    'importable' => 'true',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => false,
    'reportable' => true,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'size' => '20',
    'enable_range_search' => false,
    'dbType' => 'datetime',
    'display_default' => 'now&12:00am',
);
$dictionary["AOS_Bookings"]["fields"]["date_of_service2"] = array(
    'name' => 'date_of_service2',
    'vname' => 'LBL_DATE_OF_SERVICE2',
    'type' => 'varchar',
    "len" => 36,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
    'required' => true,
);

$dictionary["AOS_Bookings"]["fields"]["invoice_no"] = array(
    'name' => 'invoice_no',
    'vname' => 'LBL_INVOICE_NO',
    'type' => 'varchar',
    "len" => 250,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);

$dictionary["AOS_Bookings"]["fields"]["type_booking"] = array(
    'name' => 'type_booking',
    'vname' => 'LBL_TYPE_BOOKING',
    'len' => '100',
    'type' => 'enum',
    'audited' => true,
    'inline_edit' => true,
    'massupdate' => '1',
    'options' => 'type_bookings_list',
);

$dictionary["AOS_Bookings"]["fields"]["type_booking"] = array(
    'name' => 'type_booking',
    'vname' => 'LBL_TYPE_BOOKING',
    'len' => '100',
    'type' => 'enum',
    'audited' => true,
    'inline_edit' => true,
    'massupdate' => '1',
    'options' => 'type_bookings_list',
);

$dictionary["AOS_Bookings"]["fields"]["invoice_issued"] = array(
    'name' => 'invoice_issued',
    'vname' => 'LBL_INVOICE_ISSUED',
    'len' => '100',
    'type' => 'enum',
    'audited' => true,
    'inline_edit' => true,
    'massupdate' => '1',
    'options' => 'invoice_issued_list',
);

$dictionary["AOS_Bookings"]["fields"]["station"] = array(
    'name' => 'station',
    'vname' => 'LBL_STATION',
    'len' => '100',
    'type' => 'enum',
    'audited' => true,
    'inline_edit' => true,
    'massupdate' => '1',
    'options' => 'station_list',

);

$dictionary["AOS_Bookings"]["fields"]["airport"] = array(
    'name' => 'airport',
    'vname' => 'LBL_AIRPORT',
    'len' => '100',
    'type' => 'enum',
    'audited' => true,
    'inline_edit' => true,
    'massupdate' => '1',
    'options' => 'airport_list',



);

$dictionary["AOS_Bookings"]["fields"]["type_service"] = array(
    'name' => 'type_service',
    'vname' => 'LBL_TYPE_SEVICE',
    'len' => '100',
    'type' => 'enum',
    'audited' => true,
    'inline_edit' => true,
    'massupdate' => '1',
    'options' => 'type_service_list2',



);

$dictionary["AOS_Bookings"]["fields"]['other_service']=array (
      'name' => 'other_service',
      'vname' => 'LBL_OTHER_SEVICE',
      'type' => 'text',
      'rows' => '4',
      'cols' => 80,
      'required' => false,
      'inline_edit' => false,

);

$dictionary["AOS_Bookings"]["fields"]["booking_status"] = array(
    'name' => 'booking_status',
    'vname' => 'LBL_BOOKING_STATUS',
    'len' => '100',
    'type' => 'enum',
    'audited' => true,
    'inline_edit' => true,
    'massupdate' => '1',
    'options' => 'booking_status_list',


);
$dictionary["AOS_Bookings"]["fields"]["contact"] = array(
    'name' => 'contact',
    'vname' => 'LBL_CONTACT',
    'type' => 'varchar',
    "len" => 255,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);
$dictionary["AOS_Bookings"]["fields"]["exchange_rate"] = array(
    'name' => 'exchange_rate',
    'vname' => 'LBL_EXCHANGE_RATE',
    'type' => 'currency',
    'massupdate' => 0,
    'comments' => '',
    'help' => '',
    'importable' => 'true',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => 1,
    'reportable' => true,
    'len' => '26,6',
    'default'=>24000.0,
    'required'=> true,
);

 $dictionary["AOS_Bookings"]["fields"]["photo"] = array(
    'name' => 'photo',
    'vname' => 'LBL_PHOTO',
    'type' => 'image',
    'massupdate' => false,
    'comments' => '',
    'help' => '',
    'importable' => false,
    'reportable' => true,
    'len' => 255,
    'dbType' => 'varchar',
    'width' => '160',
    'height' => '160',
    'studio' => array('listview' => true),
);