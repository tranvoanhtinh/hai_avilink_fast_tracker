<?php
 // created: 2024-05-21 12:11:48
$dictionary['AOS_Bookings']['fields']['name']['required']=false;
$dictionary['AOS_Bookings']['fields']['name']['inline_edit']=true;
$dictionary['AOS_Bookings']['fields']['name']['duplicate_merge']='disabled';
$dictionary['AOS_Bookings']['fields']['name']['duplicate_merge_dom_value']='0';
$dictionary['AOS_Bookings']['fields']['name']['merge_filter']='disabled';
$dictionary['AOS_Bookings']['fields']['name']['unified_search']=false;

 ?>