<?php
// created: 2024-05-28 11:28:30
$dictionary["AOS_Bookings"]["fields"]["aos_bookings_spa_actionservice_1"] = array (
  'name' => 'aos_bookings_spa_actionservice_1',
  'type' => 'link',
  'relationship' => 'aos_bookings_spa_actionservice_1',
  'source' => 'non-db',
  'module' => 'spa_ActionService',
  'bean_name' => 'spa_ActionService',
  'side' => 'right',
  'vname' => 'LBL_AOS_BOOKINGS_SPA_ACTIONSERVICE_1_FROM_SPA_ACTIONSERVICE_TITLE',
);
