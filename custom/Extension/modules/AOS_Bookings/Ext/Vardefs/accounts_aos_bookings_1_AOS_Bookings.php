<?php
// created: 2024-05-22 03:26:41
$dictionary["AOS_Bookings"]["fields"]["accounts_aos_bookings_1"] = array (
  'name' => 'accounts_aos_bookings_1',
  'type' => 'link',
  'relationship' => 'accounts_aos_bookings_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_AOS_BOOKINGS_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_aos_bookings_1accounts_ida',
);
$dictionary["AOS_Bookings"]["fields"]["accounts_aos_bookings_1_name"] = array (
  'name' => 'accounts_aos_bookings_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_AOS_BOOKINGS_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_aos_bookings_1accounts_ida',
  'link' => 'accounts_aos_bookings_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
  'required' => true,
);
$dictionary["AOS_Bookings"]["fields"]["accounts_aos_bookings_1accounts_ida"] = array (
  'name' => 'accounts_aos_bookings_1accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_aos_bookings_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_AOS_BOOKINGS_1_FROM_AOS_BOOKINGS_TITLE',
);
