<?php
 // created: 2023-05-26 10:34:43


$dictionary["pr_PayrollFile"]["fields"]["discountpercent"] = array(
   'required' => false,
    'name' => 'discountpercent',
    'vname' => 'LBL_DISCOUNT_PERCENT',
    'type' => 'decimal',
    'massupdate' => true,
	'len' => '18',
    'size' => '20',
    'enable_range_search' => false,
    'precision' => '0',
);


 ?>