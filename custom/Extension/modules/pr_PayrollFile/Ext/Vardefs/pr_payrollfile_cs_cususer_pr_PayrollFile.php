<?php
// created: 2023-05-05 09:44:55
$dictionary["pr_PayrollFile"]["fields"]["pr_payrollfile_cs_cususer"] = array (
  'name' => 'pr_payrollfile_cs_cususer',
  'type' => 'link',
  'relationship' => 'pr_payrollfile_cs_cususer',
  'source' => 'non-db',
  'module' => 'cs_cusUser',
  'bean_name' => 'cs_cusUser',
  'vname' => 'LBL_PR_PAYROLLFILE_CS_CUSUSER_FROM_CS_CUSUSER_TITLE',
  'id_name' => 'pr_payrollfile_cs_cususercs_cususer_idb',
);
$dictionary["pr_PayrollFile"]["fields"]["pr_payrollfile_cs_cususer_name"] = array (
  'name' => 'pr_payrollfile_cs_cususer_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_PR_PAYROLLFILE_CS_CUSUSER_FROM_CS_CUSUSER_TITLE',
  'save' => true,
  'id_name' => 'pr_payrollfile_cs_cususercs_cususer_idb',
  'link' => 'pr_payrollfile_cs_cususer',
  'table' => 'cs_cususer',
  'module' => 'cs_cusUser',
  'rname' => 'name',
);
$dictionary["pr_PayrollFile"]["fields"]["pr_payrollfile_cs_cususercs_cususer_idb"] = array (
  'name' => 'pr_payrollfile_cs_cususercs_cususer_idb',
  'type' => 'link',
  'relationship' => 'pr_payrollfile_cs_cususer',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'left',
  'vname' => 'LBL_PR_PAYROLLFILE_CS_CUSUSER_FROM_CS_CUSUSER_TITLE',
);
