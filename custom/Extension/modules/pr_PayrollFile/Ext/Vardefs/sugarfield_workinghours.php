<?php
 // created: 2023-05-26 10:34:43


$dictionary["pr_PayrollFile"]["fields"]["workinghours"] = array(
   'required' => false,
    'name' => 'workinghours',
    'vname' => 'LBL_WORKING_HOURS',
    'type' => 'decimal',
    'massupdate' => true,
	'len' => '18',
    'size' => '20',
	'default_value' => '1',
    'enable_range_search' => false,
    'precision' => '2',
);


 ?>