<?php
 // created: 2024-01-23 08:02:33
$dictionary['Meeting']['fields']['status']['inline_edit']=true;
$dictionary['Meeting']['fields']['status']['comments']='Meeting status (ex: Planned, Held, Not held)';
$dictionary['Meeting']['fields']['status']['merge_filter']='disabled';

 ?>