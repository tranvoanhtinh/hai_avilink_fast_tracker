<?php
$dictionary["Meeting"]["fields"]["location_a"] = array (
  'name' => 'location_a',
  'type' => 'varchar',
  'len' => 255,
  'vname' => 'LBL_LOCATION_A',
);
$dictionary["Meeting"]["fields"]["location_b"] = array (
  'name' => 'location_b',
  'type' => 'varchar',
  'len' => 255,
  'vname' => 'LBL_LOCATION_B',
);
$dictionary["Meeting"]["fields"]["image_checkin"] = array (
  'name' => 'image_checkin',
  'type' => 'varchar',
  'len' => 255,
  'vname' => 'LBL_IMAGE_CHECKIN',
);