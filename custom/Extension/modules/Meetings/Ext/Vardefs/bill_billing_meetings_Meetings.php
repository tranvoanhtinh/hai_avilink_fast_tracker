<?php
// created: 2021-08-18 06:32:45
$dictionary["Meeting"]["fields"]["bill_billing_meetings"] = array (
  'name' => 'bill_billing_meetings',
  'type' => 'link',
  'relationship' => 'bill_billing_meetings',
  'source' => 'non-db',
  'module' => 'bill_Billing',
  'bean_name' => 'bill_Billing',
  'vname' => 'LBL_BILL_BILLING_MEETINGS_FROM_BILL_BILLING_TITLE',
  'id_name' => 'bill_billing_meetingsbill_billing_ida',
);
$dictionary["Meeting"]["fields"]["bill_billing_meetings_name"] = array (
  'name' => 'bill_billing_meetings_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_BILL_BILLING_MEETINGS_FROM_BILL_BILLING_TITLE',
  'save' => true,
  'id_name' => 'bill_billing_meetingsbill_billing_ida',
  'link' => 'bill_billing_meetings',
  'table' => 'bill_billing',
  'module' => 'bill_Billing',
  'rname' => 'name',
);
$dictionary["Meeting"]["fields"]["bill_billing_meetingsbill_billing_ida"] = array (
  'name' => 'bill_billing_meetingsbill_billing_ida',
  'type' => 'link',
  'relationship' => 'bill_billing_meetings',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_BILL_BILLING_MEETINGS_FROM_MEETINGS_TITLE',
);

$dictionary["Meeting"]["fields"]["daycheckin"] = array(
                'name' => 'daycheckin',
                'vname' => 'LBL_DAYCHECKIN',
                'type' => 'datetimecombo',
                'dbType' => 'datetime',
                'massupdate' => false,
                'enable_range_search' => true,
                'options' => 'date_range_search_dom',
            );