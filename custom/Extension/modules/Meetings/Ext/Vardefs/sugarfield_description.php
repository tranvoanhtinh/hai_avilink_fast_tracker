<?php
 // created: 2020-04-16 12:09:19
$dictionary['Meeting']['fields']['description']['inline_edit']=true;
$dictionary['Meeting']['fields']['description']['comments']='Full text of the note';
$dictionary['Meeting']['fields']['description']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['description']['rows']='2';
$dictionary['Meeting']['fields']['description']['cols']='100';
$dictionary['Meeting']['fields']['description']['required']=true;

 ?>