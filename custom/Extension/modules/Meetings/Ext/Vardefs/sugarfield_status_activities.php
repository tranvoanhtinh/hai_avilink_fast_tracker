<?php
 // created: 2022-01-11 16:22:15
$dictionary['Meeting']['fields']['status_activites']['name']='status_activites';
$dictionary['Meeting']['fields']['status_activites']['vname']='LBL_STATUS_ACTIVITIES';
$dictionary['Meeting']['fields']['status_activites']['len']='100';
$dictionary['Meeting']['fields']['status_activites']['type']='enum';
$dictionary['Meeting']['fields']['status_activites']['audited']=true;
$dictionary['Meeting']['fields']['status_activites']['inline_edit']=true;
$dictionary['Meeting']['fields']['status_activites']['massupdate']='1';
$dictionary['Meeting']['fields']['status_activites']['options']='status_activities_list';
$dictionary['Meeting']['fields']['status_activites']['merge_filter']='disabled';

 ?>