<?php
 // created: 2024-04-11 10:56:31
$dictionary['AOS_Warehouse_Transfer']['fields']['name']['required']=false;
$dictionary['AOS_Warehouse_Transfer']['fields']['name']['inline_edit']=true;
$dictionary['AOS_Warehouse_Transfer']['fields']['name']['duplicate_merge']='disabled';
$dictionary['AOS_Warehouse_Transfer']['fields']['name']['duplicate_merge_dom_value']='0';
$dictionary['AOS_Warehouse_Transfer']['fields']['name']['merge_filter']='disabled';
$dictionary['AOS_Warehouse_Transfer']['fields']['name']['unified_search']=false;

 ?>