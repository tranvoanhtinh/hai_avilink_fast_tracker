<?php
 // created: 2024-03-27 04:11:44
$dictionary['AOS_Warehouse_Transfer']['fields']['billing_address_street']['len']='250';
$dictionary['AOS_Warehouse_Transfer']['fields']['billing_address_street']['inline_edit']=true;
$dictionary['AOS_Warehouse_Transfer']['fields']['billing_address_street']['comments']='The street address used for billing address';
$dictionary['AOS_Warehouse_Transfer']['fields']['billing_address_street']['merge_filter']='disabled';

 ?>