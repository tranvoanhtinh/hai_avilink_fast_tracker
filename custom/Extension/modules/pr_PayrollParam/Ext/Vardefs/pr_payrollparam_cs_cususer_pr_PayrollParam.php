<?php
// created: 2023-05-12 03:38:53
$dictionary["pr_PayrollParam"]["fields"]["pr_payrollparam_cs_cususer"] = array (
  'name' => 'pr_payrollparam_cs_cususer',
  'type' => 'link',
  'relationship' => 'pr_payrollparam_cs_cususer',
  'source' => 'non-db',
  'module' => 'cs_cusUser',
  'bean_name' => 'cs_cusUser',
  'side' => 'right',
  'vname' => 'LBL_PR_PAYROLLPARAM_CS_CUSUSER_FROM_CS_CUSUSER_TITLE',
);
$dictionary["pr_PayrollParam"]["fields"]["dayleave"] = array(
   'required' => false,
    'name' => 'dayleave',
    'vname' => 'LBL_DAY_LEAVE',
    'type' => 'decimal',
    'massupdate' => true,
	'len' => '18',
    'size' => '20',
	'default_value' => '1',
    'enable_range_search' => false,
    'precision' => '2',
);

$dictionary["pr_PayrollParam"]["fields"]["dayovertime"] = array(
   'required' => false,
    'name' => 'dayovertime',
    'vname' => 'LBL_DAY_OVERTIME',
    'type' => 'decimal',
    'massupdate' => true,
	'len' => '18',
    'size' => '20',
	'default_value' => '1',
    'enable_range_search' => false,
    'precision' => '2',
);
