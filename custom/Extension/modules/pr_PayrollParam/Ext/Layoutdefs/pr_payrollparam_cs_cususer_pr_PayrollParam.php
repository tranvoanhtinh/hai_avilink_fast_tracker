<?php
 // created: 2023-05-12 03:38:53
$layout_defs["pr_PayrollParam"]["subpanel_setup"]['pr_payrollparam_cs_cususer'] = array (
  'order' => 100,
  'module' => 'cs_cusUser',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_PR_PAYROLLPARAM_CS_CUSUSER_FROM_CS_CUSUSER_TITLE',
  'get_subpanel_data' => 'pr_payrollparam_cs_cususer',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
