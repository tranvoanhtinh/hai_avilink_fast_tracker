<?php 

 
$dictionary["AOS_Stock_Inventory"]["fields"]["product_id"] = array(
    'name' => 'product_id',
    'vname' => 'LBL_PRODUCT_ID',
    'type' => 'varchar',
    "len" => 36,
    "importable" => true,
    "audited" => true,
    "massupdate" => false,
);
  $dictionary["AOS_Stock_Inventory"]["fields"]["maincode"] = array(
    'name' => 'maincode',
    'vname' => 'LBL_MAINCODE',
    'type' => 'varchar',
    "len" => 50,
    "importable" => true,
    "audited" => true,
    "massupdate" => false,
);

   
    $dictionary["AOS_Stock_Inventory"]["fields"]["price"] =
            array(
       'required' => false,
                'name' => 'price',
                'vname' => 'LBL_PRICE',
                'type' => 'currency',
                'massupdate' => 0,
                'comments' => '',
                'help' => '',
                'importable' => 'true',
                'duplicate_merge' => 'disabled',
                'duplicate_merge_dom_value' => '0',
                'audited' => false,
                'reportable' => true,
                'len' => '26,6',
                'enable_range_search' => true,
                'options' => 'numeric_range_search_dom',
            );


    $dictionary["AOS_Stock_Inventory"]["fields"]["day"] = array(
    'name' => 'day',
    'vname' => 'LBL_DAY',
    'type' => 'varchar',
    "len" => 10,
    "importable" => true,
    "audited" => true,
    "massupdate" => false,
);

    $dictionary["AOS_Stock_Inventory"]["fields"]["month"] = array(
    'name' => 'month',
    'vname' => 'LBL_MONTH',
    'type' => 'varchar',
    "len" => 10,
    "importable" => true,
    "audited" => true,
    "massupdate" => false,
);

    $dictionary["AOS_Stock_Inventory"]["fields"]["year"] = array(
    'name' => 'year',
    'vname' => 'LBL_YEAR',
    'type' => 'varchar',
    "len" => 10,
    "importable" => true,
    "audited" => true,
    "massupdate" => false,
);



$dictionary["AOS_Stock_Inventory"]["fields"]["warehouse"] = array(
    'name' => 'warehouse',
    'vname' => 'LBL_WAREHOUSE',
    'len' => '100',
    'type' => 'enum',
    'audited' => true,
    'inline_edit' => true,
    'massupdate' => '1',
    'options' => 'take_from_list',
    'merge_filter' => 'disabled',
);




$dictionary["AOS_Stock_Inventory"]["fields"]["import"] = array(
    'name' => 'import',
    'vname' => 'LBL_IMPORT',
    'type' => 'int',
    "len" => 40,
    "importable" => true,
    "audited" => true,
    "massupdate" => false,
);



$dictionary["AOS_Stock_Inventory"]["fields"]["export"] = array(
    'name' => 'export',
    'vname' => 'LBL_EXPORT',
    'type' => 'int',
    "len" => 40,
    "importable" => true,
    "audited" => true,
    "massupdate" => false,
);

$dictionary["AOS_Stock_Inventory"]["fields"]["beginning_balance"] = array(
    'name' => 'beginning_balance',
    'vname' => 'LBL_BEGINNING_BALANCE',
    'type' => 'int',
    "len" => 40,
    "importable" => true,
    "audited" => true,
    "massupdate" => false,
    "default"=>'0',
);










$dictionary["AOS_Stock_Inventory"]["fields"]["ending_balance"] = array(
    'name' => 'ending_balance',
    'vname' => 'LBL_ENDING_BALANCE',
    'type' => 'int',
    "len" => 40,
    "importable" => true,
    "audited" => true,
    "massupdate" => false,
    "default"=>'0',
);

    $dictionary["AOS_Stock_Inventory"]["fields"]["total_price"] =
            array(
       'required' => false,
                'name' => 'total_price',
                'vname' => 'LBL_TOTAL_PRICE',
                'type' => 'currency',
                'massupdate' => 0,
                'comments' => '',
                'help' => '',
                'importable' => 'true',
                'duplicate_merge' => 'disabled',
                'duplicate_merge_dom_value' => '0',
                'audited' => false,
                'reportable' => true,
                'len' => '26,6',
                'enable_range_search' => true,
                'options' => 'numeric_range_search_dom',
            );

$dictionary["AOS_Stock_Inventory"]["fields"]["unitcode"] = array(
    'name' => 'unitcode',
    'vname' => 'LBL_UNITCODE',
    'len' => '100',
    'type' => 'enum',
    'audited' => true,
    'inline_edit' => true,
    'massupdate' => '1',
    'options' => 'unit_list',
    'merge_filter' => 'disabled',
);
