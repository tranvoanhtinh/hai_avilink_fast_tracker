<?php
 // created: 2024-05-13 10:02:31
$layout_defs["AOS_Size"]["subpanel_setup"]['aos_products_aos_size_1'] = array (
  'order' => 100,
  'module' => 'AOS_Products',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_AOS_PRODUCTS_AOS_SIZE_1_FROM_AOS_PRODUCTS_TITLE',
  'get_subpanel_data' => 'aos_products_aos_size_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
