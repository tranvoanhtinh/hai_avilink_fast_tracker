<?php
 $dictionary["AOS_Size"]["fields"]["handle"] = array(
    'name' => 'handle',
    'vname' => 'LBL_HANDLE',
    'type' => 'decimal',
    "len" => 40,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
    'precision' => '4',
);
  $dictionary["AOS_Size"]["fields"]["bottom_length"] = array(
    'name' => 'bottom_length',
    'vname' => 'LBL_BOTTOM_LENGTH',
    'type' => 'decimal',
    "len" => 40,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
    'precision' => '4',
);
   
 $dictionary["AOS_Size"]["fields"]["bottom_width"] = array(
    'name' => 'bottom_width',
    'vname' => 'LBL_BOTTOM_WIDTH',
    'type' => 'decimal',
    "len" => 40,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
    'precision' => '4',
);

?>