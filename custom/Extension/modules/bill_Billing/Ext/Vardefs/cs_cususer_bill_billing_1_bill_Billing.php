<?php
// created: 2023-05-23 06:52:50
$dictionary["bill_Billing"]["fields"]["cs_cususer_bill_billing_1"] = array (
  'name' => 'cs_cususer_bill_billing_1',
  'type' => 'link',
  'relationship' => 'cs_cususer_bill_billing_1',
  'source' => 'non-db',
  'module' => 'cs_cusUser',
  'bean_name' => 'cs_cusUser',
  'vname' => 'LBL_CS_CUSUSER_BILL_BILLING_1_FROM_CS_CUSUSER_TITLE',
  'id_name' => 'cs_cususer_bill_billing_1cs_cususer_ida',
);
$dictionary["bill_Billing"]["fields"]["cs_cususer_bill_billing_1_name"] = array (
  'name' => 'cs_cususer_bill_billing_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CS_CUSUSER_BILL_BILLING_1_FROM_CS_CUSUSER_TITLE',
  'save' => true,
  'id_name' => 'cs_cususer_bill_billing_1cs_cususer_ida',
  'link' => 'cs_cususer_bill_billing_1',
  'table' => 'cs_cususer',
  'module' => 'cs_cusUser',
  'rname' => 'name',
);
$dictionary["bill_Billing"]["fields"]["cs_cususer_bill_billing_1cs_cususer_ida"] = array (
  'name' => 'cs_cususer_bill_billing_1cs_cususer_ida',
  'type' => 'link',
  'relationship' => 'cs_cususer_bill_billing_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_CS_CUSUSER_BILL_BILLING_1_FROM_BILL_BILLING_TITLE',
);
