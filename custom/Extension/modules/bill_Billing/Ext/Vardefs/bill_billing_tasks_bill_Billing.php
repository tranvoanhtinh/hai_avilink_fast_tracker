<?php
// created: 2021-08-18 06:32:45
$dictionary["bill_Billing"]["fields"]["bill_billing_tasks"] = array (
  'name' => 'bill_billing_tasks',
  'type' => 'link',
  'relationship' => 'bill_billing_tasks',
  'source' => 'non-db',
  'module' => 'Tasks',
  'bean_name' => 'Task',
  'side' => 'right',
  'vname' => 'LBL_BILL_BILLING_TASKS_FROM_TASKS_TITLE',
);
