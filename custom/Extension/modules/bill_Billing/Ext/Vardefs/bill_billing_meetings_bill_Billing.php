<?php
// created: 2021-08-18 06:32:45
$dictionary["bill_Billing"]["fields"]["bill_billing_meetings"] = array (
  'name' => 'bill_billing_meetings',
  'type' => 'link',
  'relationship' => 'bill_billing_meetings',
  'source' => 'non-db',
  'module' => 'Meetings',
  'bean_name' => 'Meeting',
  'side' => 'right',
  'vname' => 'LBL_BILL_BILLING_MEETINGS_FROM_MEETINGS_TITLE',
);
