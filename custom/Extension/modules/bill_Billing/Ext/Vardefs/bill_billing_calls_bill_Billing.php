<?php
// created: 2021-08-18 06:32:45
$dictionary["bill_Billing"]["fields"]["bill_billing_calls"] = array (
  'name' => 'bill_billing_calls',
  'type' => 'link',
  'relationship' => 'bill_billing_calls',
  'source' => 'non-db',
  'module' => 'Calls',
  'bean_name' => 'Call',
  'side' => 'right',
  'vname' => 'LBL_BILL_BILLING_CALLS_FROM_CALLS_TITLE',
);
