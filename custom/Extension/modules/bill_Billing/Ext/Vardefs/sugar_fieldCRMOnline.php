<?php


$dictionary["bill_Billing"]["fields"]["vnd"] = array(
    'name' => 'vnd',
    'vname' => 'LBL_VND',
    'type' => 'currency',
    'massupdate' => 0,
    'comments' => '',
    'help' => '',
    'importable' => 'true',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => 1,
    'reportable' => true,
    'len' => '26,6',
);

$dictionary["bill_Billing"]["fields"]["usd"] = array(
    'name' => 'usd',
    'vname' => 'LBL_USD',
    'type' => 'currency',
    'massupdate' => 0,
    'comments' => '',
    'help' => '',
    'importable' => 'true',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => 1,
    'reportable' => true,
    'len' => '26,6',
);

$dictionary["bill_Billing"]["fields"]["type_currency"] = array(
    'name' => 'type_currency',
    'vname' => 'LBL_TYPE_CURRENCY',
    'len' => '100',
    'type' => 'enum',
    'audited' => true,
    'inline_edit' => true,
    'massupdate' => '1',
    'options' => 'type_currency_list',
    'merge_filter' => 'disabled',
    'studio' => true,
);


$dictionary["bill_Billing"]["fields"]["check_type"] = array(
    'name' => 'check_type',
    'vname' => 'LBL_CHECK_TYPE',
    'len' => '100',
    'type' => 'enum',
    'audited' => true,
    'inline_edit' => true,
    'massupdate' => '1',
    'options' => 'check_type_list',
    'merge_filter' => 'disabled',
    'studio' => true,
);