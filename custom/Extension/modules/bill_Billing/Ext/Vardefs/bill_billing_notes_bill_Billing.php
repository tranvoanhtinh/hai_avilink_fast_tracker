<?php
// created: 2021-08-18 06:32:45
$dictionary["bill_Billing"]["fields"]["bill_billing_notes"] = array (
  'name' => 'bill_billing_notes',
  'type' => 'link',
  'relationship' => 'bill_billing_notes',
  'source' => 'non-db',
  'module' => 'Notes',
  'bean_name' => 'Note',
  'side' => 'right',
  'vname' => 'LBL_BILL_BILLING_NOTES_FROM_NOTES_TITLE',
);
