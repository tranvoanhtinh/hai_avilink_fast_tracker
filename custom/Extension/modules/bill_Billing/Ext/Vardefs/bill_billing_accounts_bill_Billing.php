<?php
// created: 2021-08-18 06:32:45
$dictionary["bill_Billing"]["fields"]["bill_billing_accounts"] = array (
  'name' => 'bill_billing_accounts',
  'type' => 'link',
  'relationship' => 'bill_billing_accounts',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_BILL_BILLING_ACCOUNTS_FROM_ACCOUNTS_TITLE',
  'id_name' => 'bill_billing_accountsaccounts_ida',
);
$dictionary["bill_Billing"]["fields"]["bill_billing_accounts_name"] = array (
  'name' => 'bill_billing_accounts_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_BILL_BILLING_ACCOUNTS_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'bill_billing_accountsaccounts_ida',
  'link' => 'bill_billing_accounts',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["bill_Billing"]["fields"]["bill_billing_accountsaccounts_ida"] = array (
  'name' => 'bill_billing_accountsaccounts_ida',
  'type' => 'link',
  'relationship' => 'bill_billing_accounts',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_BILL_BILLING_ACCOUNTS_FROM_BILL_BILLING_TITLE',
);
