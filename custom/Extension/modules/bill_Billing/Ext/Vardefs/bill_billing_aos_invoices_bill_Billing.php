<?php
// created: 2021-08-18 06:32:45
$dictionary["bill_Billing"]["fields"]["bill_billing_aos_invoices"] = array (
  'name' => 'bill_billing_aos_invoices',
  'type' => 'link',
  'relationship' => 'bill_billing_aos_invoices',
  'source' => 'non-db',
  'module' => 'AOS_Invoices',
  'bean_name' => 'AOS_Invoices',
  'vname' => 'LBL_BILL_BILLING_AOS_INVOICES_FROM_AOS_INVOICES_TITLE',
  'id_name' => 'bill_billing_aos_invoicesaos_invoices_ida',
);
$dictionary["bill_Billing"]["fields"]["bill_billing_aos_invoices_name"] = array (
  'name' => 'bill_billing_aos_invoices_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_BILL_BILLING_AOS_INVOICES_FROM_AOS_INVOICES_TITLE',
  'save' => true,
  'id_name' => 'bill_billing_aos_invoicesaos_invoices_ida',
  'link' => 'bill_billing_aos_invoices',
  'table' => 'aos_invoices',
  'module' => 'AOS_Invoices',
  'rname' => 'name',
);
$dictionary["bill_Billing"]["fields"]["bill_billing_aos_invoicesaos_invoices_ida"] = array (
  'name' => 'bill_billing_aos_invoicesaos_invoices_ida',
  'type' => 'link',
  'relationship' => 'bill_billing_aos_invoices',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_BILL_BILLING_AOS_INVOICES_FROM_BILL_BILLING_TITLE',
);
