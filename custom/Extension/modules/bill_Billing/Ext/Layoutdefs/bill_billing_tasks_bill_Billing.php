<?php
 // created: 2021-08-18 06:32:45
$layout_defs["bill_Billing"]["subpanel_setup"]['bill_billing_tasks'] = array (
  'order' => 100,
  'module' => 'Tasks',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_BILL_BILLING_TASKS_FROM_TASKS_TITLE',
  'get_subpanel_data' => 'bill_billing_tasks',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
