<?php
 // created: 2024-03-27 04:03:31
$dictionary['AOS_Inventoryoutput']['fields']['billing_address_street']['len']='250';
$dictionary['AOS_Inventoryoutput']['fields']['billing_address_street']['inline_edit']=true;
$dictionary['AOS_Inventoryoutput']['fields']['billing_address_street']['comments']='The street address used for billing address';
$dictionary['AOS_Inventoryoutput']['fields']['billing_address_street']['merge_filter']='disabled';

 ?>