<?php
 // created: 2024-04-11 10:55:44
$dictionary['AOS_Inventoryoutput']['fields']['name']['required']=false;
$dictionary['AOS_Inventoryoutput']['fields']['name']['inline_edit']=true;
$dictionary['AOS_Inventoryoutput']['fields']['name']['duplicate_merge']='disabled';
$dictionary['AOS_Inventoryoutput']['fields']['name']['duplicate_merge_dom_value']='0';
$dictionary['AOS_Inventoryoutput']['fields']['name']['merge_filter']='disabled';
$dictionary['AOS_Inventoryoutput']['fields']['name']['unified_search']=false;

 ?>