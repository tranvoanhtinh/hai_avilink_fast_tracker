<?php
 // created: 2024-03-27 04:03:46
$dictionary['AOS_Inventoryoutput']['fields']['shipping_address_street']['len']='250';
$dictionary['AOS_Inventoryoutput']['fields']['shipping_address_street']['inline_edit']=true;
$dictionary['AOS_Inventoryoutput']['fields']['shipping_address_street']['comments']='The street address used for for shipping purposes';
$dictionary['AOS_Inventoryoutput']['fields']['shipping_address_street']['merge_filter']='disabled';

 ?>