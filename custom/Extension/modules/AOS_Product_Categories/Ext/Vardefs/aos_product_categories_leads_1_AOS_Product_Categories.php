<?php
// created: 2022-09-05 11:29:26
$dictionary["AOS_Product_Categories"]["fields"]["aos_product_categories_leads_1"] = array (
  'name' => 'aos_product_categories_leads_1',
  'type' => 'link',
  'relationship' => 'aos_product_categories_leads_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'side' => 'right',
  'vname' => 'LBL_AOS_PRODUCT_CATEGORIES_LEADS_1_FROM_LEADS_TITLE',
);
