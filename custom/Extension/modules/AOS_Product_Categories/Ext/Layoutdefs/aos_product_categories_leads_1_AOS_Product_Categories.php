<?php
 // created: 2022-09-05 11:29:26
$layout_defs["AOS_Product_Categories"]["subpanel_setup"]['aos_product_categories_leads_1'] = array (
  'order' => 100,
  'module' => 'Leads',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_AOS_PRODUCT_CATEGORIES_LEADS_1_FROM_LEADS_TITLE',
  'get_subpanel_data' => 'aos_product_categories_leads_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
