<?php
$dictionary["BI_Report"]["fields"]["record_rp"] = array(
      'name' => 'record_rp',
      'vname' => 'LBL_RECORD_RP',
      'len' => '100',
      'type' => 'enum',
      'audited' => true,
      'inline_edit' => true,
      'massupdate' => '1',
      'options' => 'record_rp_list',
      'merge_filter' => 'disabled',
);