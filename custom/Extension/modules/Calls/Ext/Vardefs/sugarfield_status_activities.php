<?php
 // created: 2022-01-11 16:22:15
$dictionary['Call']['fields']['status_activites']['name']='status_activites';
$dictionary['Call']['fields']['status_activites']['vname']='LBL_STATUS_ACTIVITIES';
$dictionary['Call']['fields']['status_activites']['len']='100';
$dictionary['Call']['fields']['status_activites']['type']='enum';
$dictionary['Call']['fields']['status_activites']['audited']=true;
$dictionary['Call']['fields']['status_activites']['inline_edit']=true;
$dictionary['Call']['fields']['status_activites']['massupdate']='1';
$dictionary['Call']['fields']['status_activites']['options']='status_activities_list';
$dictionary['Call']['fields']['status_activites']['merge_filter']='disabled';

 ?>