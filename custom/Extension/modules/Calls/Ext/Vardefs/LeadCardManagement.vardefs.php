<?php

$dictionary["Call"]["fields"]["is_completed"] = array(
    "name" => "is_completed",
    "vname" => "LBL_IS_COMPLETED",
    "type" => "bool",
    "default" => 0,
    'inline_edit' => false,
);

$dictionary["Call"]["fields"]["is_important"] = array(
    "name" => "is_important",
    "vname" => "LBL_IS_IMPORTANT",
    "type" => "bool",
    "default" => 0,
    'inline_edit' => false,
);