<?php
// created: 2021-08-18 06:32:45
$dictionary["Call"]["fields"]["bill_billing_calls"] = array (
  'name' => 'bill_billing_calls',
  'type' => 'link',
  'relationship' => 'bill_billing_calls',
  'source' => 'non-db',
  'module' => 'bill_Billing',
  'bean_name' => 'bill_Billing',
  'vname' => 'LBL_BILL_BILLING_CALLS_FROM_BILL_BILLING_TITLE',
  'id_name' => 'bill_billing_callsbill_billing_ida',
);
$dictionary["Call"]["fields"]["bill_billing_calls_name"] = array (
  'name' => 'bill_billing_calls_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_BILL_BILLING_CALLS_FROM_BILL_BILLING_TITLE',
  'save' => true,
  'id_name' => 'bill_billing_callsbill_billing_ida',
  'link' => 'bill_billing_calls',
  'table' => 'bill_billing',
  'module' => 'bill_Billing',
  'rname' => 'name',
);
$dictionary["Call"]["fields"]["bill_billing_callsbill_billing_ida"] = array (
  'name' => 'bill_billing_callsbill_billing_ida',
  'type' => 'link',
  'relationship' => 'bill_billing_calls',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_BILL_BILLING_CALLS_FROM_CALLS_TITLE',
);
