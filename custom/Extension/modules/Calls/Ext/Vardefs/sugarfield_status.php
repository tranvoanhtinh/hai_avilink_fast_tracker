<?php
 // created: 2024-01-23 07:52:38
$dictionary['Call']['fields']['status']['inline_edit']=true;
$dictionary['Call']['fields']['status']['comments']='The status of the call (Held, Not Held, etc.)';
$dictionary['Call']['fields']['status']['merge_filter']='disabled';

 ?>