<?php
 // created: 2019-09-07 03:50:41
$dictionary['Call']['fields']['description']['inline_edit']=true;
$dictionary['Call']['fields']['description']['comments']='Full text of the note';
$dictionary['Call']['fields']['description']['merge_filter']='disabled';
$dictionary['Call']['fields']['description']['rows']='2';
$dictionary['Call']['fields']['description']['required']=true;

 ?>