<?php

//$job_strings[]='getRevenueFromErp';
array_push($job_strings, 'getInfo_VOIP24');
defined('GET_REVENUE_API_URL_VOIP24') or define('GET_REVENUE_API_URL_VOIP24', 'http://graph.voip24h.vn/call/find');
defined('GET_REVENUE_API_URL_RECORD_VOIP24') or define('GET_REVENUE_API_URL_RECORD_VOIP24', 'http://graph.voip24h.vn/call/media');
date_default_timezone_set('Asia/ho_chi_minh'); 
$year = getdate();
if($year["mday"] <= 9){
	if($year["mday"] < 9){
	$dayFrom = "0".($year["mday"]-1);
	$dayTo = "0".($year["mday"]+1);
	}else{
	$dayFrom = "0".($year["mday"]-1);
	$dayTo = $year["mday"]+1;
	}
}else{
	if($year["mday"] == 10){
		$dayFrom = "0".($year["mday"]-1);
		$dayTo = $year["mday"]+1;
	}else{
		$dayFrom = $year["mday"]-1;
		$dayTo = $year["mday"]+1;
	}
}

if($year["mon"] < 10){
	$mon = "0".$year["mon"];
}else{
	$mon = $year["mon"];
}
function getContent_VOIP24()
{
	return file_get_contents("dateSysHistory_voip24.txt");
}
function getTokenContent_VOIP24()
{
	return file_get_contents("dateSysToken_voip24.txt");
}
function getInfo_VOIP24()
    {
		try {
				global $db;
				$result = callAPI_VOIP24();
				//$ft = fopen('logHistory.txt', 'w');
				//fwrite($ft, json_encode($result));
				//fclose($ft);
				if(count($result["data"]["response"]["data"]) > 0){
					for($i = 0;$i < count($result["data"]["response"]["data"]);$i++){
						if(!empty($result["data"]["response"]["data"])){
							$second=gmdate('H:i:s',$result["data"]["response"]["data"][$i]["duration"]);
							$timeStart = $result["data"]["response"]["data"][$i]["calldate"];
							$timeEnd = $result["data"]["response"]["data"][$i]["calldate"];
							$history= BeanFactory::newBean("tttt_History_calls");
							$history->direction=$result["data"]["response"]["data"][$i]["type"];
							if($result["data"]["response"]["data"][$i]["type"] == "inbound"){
								$que = "select tttt_worldfonepbx.assigned_user_id from tttt_worldfonepbx inner join tttt_worldfonepbx_cstm on tttt_worldfonepbx.id = tttt_worldfonepbx_cstm.id_c where tttt_worldfonepbx.description = '".$result["data"]["response"]["data"][$i]["extension"]."' 
								and tttt_worldfonepbx.deleted = 0 and tttt_worldfonepbx_cstm.status_c = '1'";
								$resu = $db->query($que);
								$r =  $db->fetchByAssoc($resu);
								$a = "";
								$history->userextension = $result["data"]["response"]["data"][$i]["extension"];
								$history->callstatus = $result["data"]["response"]["data"][$i]["status"];
								$history->customernumberfrom = $result["data"]["response"]["data"][$i]["caller"];
								$history->customernumberto=$result["data"]["response"]["data"][$i]["did"];
							}else{
								$que = "select tttt_worldfonepbx.assigned_user_id from tttt_worldfonepbx inner join tttt_worldfonepbx_cstm on tttt_worldfonepbx.id = tttt_worldfonepbx_cstm.id_c where tttt_worldfonepbx.description = '".$result["data"]["response"]["data"][$i]["caller"]."' 
								and tttt_worldfonepbx.deleted = 0 and tttt_worldfonepbx_cstm.status_c = '1'";
								$resu = $db->query($que);
								$r =  $db->fetchByAssoc($resu);
								$history->userextension = $result["data"]["response"]["data"][$i]["caller"];
								$history->callstatus = $result["data"]["response"]["data"][$i]["status"];
								$history->customernumberfrom=$result["data"]["response"]["data"][$i]["did"];
								$history->customernumberto=$result["data"]["response"]["data"][$i]["callee"];
							}
							$history->starttime = date('Y-m-d H:i:s', strtotime($timeStart.' - 7 hours'));
							$history->endtime = date('Y-m-d H:i:s', strtotime($timeEnd.' - 7 hours'));
							if(!empty($r)){
								$history->assigned_user_id=$r["assigned_user_id"];
							}
							$history->totalduration=$second;
							
							$res = callAPI_RECORD_VOIP24($result["data"]["response"]["data"][$i]["callid"]);
							if($res["data"]["response"]["data"]["message"] == "success"){
								$history->file_ghiam = $res["data"]["response"]["data"]["data"]["ogg"];
							}
							$history->save();
							
							if($result["data"]["response"]["data"][$i]["type"] == "inbound"){
								$id = create_guid();
								$date = date('Y-m-d h:i:s');
								$query="select id from accounts  where phone_alternate='".$result["data"]["response"]["data"][$i]["caller"]."' And accounts.deleted='0'";
								$res=$db->query($query);
								$row=$db->fetchByAssoc($res);
								if(!empty($row)){
									$q="insert into  accounts_tttt_history_calls_1_c  (id,date_modified,accounts_tttt_history_calls_1accounts_ida,accounts_tttt_history_calls_1tttt_history_calls_idb) values ('".$id."','".$date."','".$row["id"]."','".$history->id."')";
									$db->query($q);
								}else{
									$query="select id from leads where (phone_mobile='".$result["data"]["response"]["data"][$i]["caller"]."' OR phone_work='".$result["data"]["response"]["data"][$i]["caller"]."' OR phone_other='".$result["data"]["response"]["data"][$i]["caller"]."') and leads.deleted='0' and status IN('New','In Process','Dead')";
									$res=$db->query($query);
									$row=$db->fetchByAssoc($res);
									if(!empty($row) ){
										$q="insert into  leads_tttt_history_calls_1_c  (id,date_modified,leads_tttt_history_calls_1leads_ida,leads_tttt_history_calls_1tttt_history_calls_idb) values ('".$id."','".$date."','".$row["id"]."','".$history->id."')";
										$db->query($q);
									}else{
										$query="select id from contacts where (phone_mobile='".$result["data"]["response"]["data"][$i]["caller"]."' OR phone_work='".$result["data"]["response"]["data"][$i]["caller"]."' OR phone_other='".$result["data"]["response"]["data"][$i]["caller"]."') and contacts.deleted='0'";
										$res=$db->query($query);
										$row=$db->fetchByAssoc($res);
										if(!empty($row) ){
										$q="insert into  contacts_tttt_history_calls_1_c  (id,date_modified,contacts_tttt_history_calls_1contacts_ida,contacts_tttt_history_calls_1tttt_history_calls_idb) values ('".$id."','".$date."','".$row["id"]."','".$history->id."')";
										$db->query($q);
										}
									}
								}
							}else{
								$id = create_guid();
								$date = date('Y-m-d h:i:s');
								$query="select id from accounts  where phone_alternate='".$result["data"]["response"]["data"][$i]["callee"]."' And accounts.deleted='0'";
								$res=$db->query($query);
								$row=$db->fetchByAssoc($res);
								if(!empty($row)){
									$q="insert into  accounts_tttt_history_calls_1_c  (id,date_modified,accounts_tttt_history_calls_1accounts_ida,accounts_tttt_history_calls_1tttt_history_calls_idb) values ('".$id."','".$date."','".$row["id"]."','".$history->id."')";
									$db->query($q);
								}else{
									$query="select id from leads where (phone_mobile='".$result["data"]["response"]["data"][$i]["callee"]."' OR phone_work='".$result["data"]["response"]["data"][$i]["callee"]."' OR phone_other='".$result["data"]["response"]["data"][$i]["callee"]."') and leads.deleted='0' and status IN('New','In Process','Dead')";
									$res=$db->query($query);
									$row=$db->fetchByAssoc($res);
									if(!empty($row) ){
										$q="insert into  leads_tttt_history_calls_1_c  (id,date_modified,leads_tttt_history_calls_1leads_ida,leads_tttt_history_calls_1tttt_history_calls_idb) values ('".$id."','".$date."','".$row["id"]."','".$history->id."')";
										$db->query($q);
									}else{
										$query="select id from contacts where (phone_mobile='".$result["data"]["response"]["data"][$i]["callee"]."' OR phone_work='".$result["data"]["response"]["data"][$i]["callee"]."' OR phone_other='".$result["data"]["response"]["data"][$i]["callee"]."') and contacts.deleted='0'";
										$res=$db->query($query);
										$row=$db->fetchByAssoc($res);
										if(!empty($row) ){
										$q="insert into  contacts_tttt_history_calls_1_c  (id,date_modified,contacts_tttt_history_calls_1contacts_ida,contacts_tttt_history_calls_1tttt_history_calls_idb) values ('".$id."','".$date."','".$row["id"]."','".$history->id."')";
										$db->query($q);
										}
									}
								}
								
								
							}
							if($i == 0){
								$fp = fopen('dateSysHistory_voip24.txt', 'w');
								fwrite($fp, date("Y-m-d H:i:s", strtotime(date($result["data"]["response"]["data"][$i]["calldate"])."+ 1 second")));
								fclose($fp);
							}
						}
					}
				}else{
					$today = date("Y-m-d H:i:s");
					$startEnd = getContent_VOIP24();
					$startEndSave =  date("Y-m-d H:i:s", strtotime(date($startEnd)."+ 2 hours"));
					if($today >= $startEndSave){
						$fp = fopen('dateSysHistory_voip24.txt', 'w');
						fwrite($fp, date("Y-m-d H:i:s", strtotime(date($startEnd)."+ 2 hours")));
						fclose($fp);
					}
				}
			} catch (Exception $ex) {
				$GLOBALS['log']->error($ex->getMessage());
			}
			return true;
	}
    function callAPI_VOIP24()
    {
		$startDate = getContent_VOIP24();
		$token = getTokenContent_VOIP24();
		$today = date("Y-m-d H:i:s");
		$startEndTest=date("Y-m-d H:i:s", strtotime(date($startDate)."+ 2 hours"));
		$response = array();
		if($today >= $startEndTest){
			$startEnd = date("Y-m-d H:i:s", strtotime(date($startDate)."+ 2 hours"));
		$data = '
		{
			 "date_start":"'.$startDate.'",
			 "date_end" : "'.$startEnd.'"
		}
		';
			try {
				$apiUrl = GET_REVENUE_API_URL_VOIP24;
				$curl = curl_init();
				curl_setopt_array($curl, array(
					CURLOPT_URL => $apiUrl,
					CURLOPT_CUSTOMREQUEST => 'POST',
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_POSTFIELDS =>$data,
					CURLOPT_HTTPHEADER => array(
							'Authorization: Bearer '.$token,
							'Content-Type: application/json',
						)));
						$response = curl_exec($curl);
						curl_close($curl);
						if (!empty($response)){
							$response = json_decode($response, true);
					}
			
			}catch (Exception $ex){
				$GLOBALS['log']->error($ex);
			
			}
		}else{
			$response["data"]["response"]["data"] = array();
		}
        return $response;
    }
	
	function callAPI_RECORD_VOIP24($record)
    {
		$token = getTokenContent_VOIP24();
		$data = '
		{
			"callid": "'.$record.'"
		}

		';
        $response = array();
        try {
		    $apiUrl = GET_REVENUE_API_URL_RECORD_VOIP24;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_URL, $apiUrl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $headers = [
              'Content-type: application/json',
			  'Name: X-VOIP24H-AUTH',
			  'Value:Bearer '.$token,
			  'authorization:Bearer '.$token,
            ];
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            $response = curl_exec($ch);
            if (!empty($response)) {
                $response = json_decode($response, true);
				
            }
            curl_close($ch);
        } catch (Exception $ex) {
            $GLOBALS['log']->error($ex);
        }
        return $response;
    }
	
      
	
