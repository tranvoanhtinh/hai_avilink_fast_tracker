<?php
array_push($job_strings, 'getToken_OMICALL');
defined('GET_TOKEN_API_URL_OMICALL') or define('GET_TOKEN_API_URL_OMICALL', 'https://public-v1-stg.omicall.com/api/auth?apiKey=DF7EE3AFE814E16A34606A3D675B8D7BC4824351D1CBE0BBB01116F6BEE2A51A');
function getToken_OMICALL()
    {
		try {
			global $db;
			$result = callAPITOKEN_OMICALL();
			if(!empty($result["payload"]["access_token"])){
				$fp = fopen('dateSysToken_omicall.txt', 'w');
				fwrite($fp,$result["payload"]["access_token"]);
				fclose($fp);
			}
		} catch (Exception $ex) {
			$GLOBALS['log']->error($ex->getMessage());
		}
			return true;
	}
    function callAPITOKEN_OMICALL()
    {
		
        $response = array();
        try {
		    $apiUrl=GET_TOKEN_API_URL_OMICALL;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_URL, $apiUrl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			curl_setopt($ch,CURLOPT_FOLLOWLOCATION,true);
			//curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            $response = curl_exec($ch);
            if (!empty($response)) {
                $response = json_decode($response, true);
            }
            curl_close($ch);
        } catch (Exception $ex) {
            $GLOBALS['log']->error($ex);
        }
        return $response;
    }
	
      
	
