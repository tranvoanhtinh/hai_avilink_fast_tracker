<?php

//$job_strings[]='getRevenueFromErp';
array_push($job_strings, 'getInfo_ODS');
defined('GET_REVENUE_API_URL_ODS') or define('GET_REVENUE_API_URL_ODS', 'https://api.cloudfone.vn/api/CloudFone/GetCallHistory');
date_default_timezone_set('Asia/ho_chi_minh'); 
$year = getdate();
if($year["mday"] <= 9){
	if($year["mday"] < 9){
	$dayFrom = "0".($year["mday"]-1);
	$dayTo = "0".($year["mday"]+1);
	}else{
	$dayFrom = "0".($year["mday"]-1);
	$dayTo = $year["mday"]+1;
	}
}else{
	if($year["mday"] == 10){
		$dayFrom = "0".($year["mday"]-1);
		$dayTo = $year["mday"]+1;
	}else{
		$dayFrom = $year["mday"]-1;
		$dayTo = $year["mday"]+1;
	}
}

if($year["mon"] < 10){
	$mon = "0".$year["mon"];
}else{
	$mon = $year["mon"];
}
function getContent_ODS()
{
	return file_get_contents("dateSysHistory_ods.txt");
}
function getInfo_ODS()
    {
		try {
				global $db;
				$result = callAPI_ODS();
				if($result["total"] > 0){
					for($i = 0;$i < $result["total"];$i++){
						if(!empty($result["data"])){
							$second=gmdate('H:i:s',$result["data"][$i]["thoiGianThucGoi"]);
							$timeStart = str_replace('T', ' ', $result["data"][$i]["ngayGoi"]);
							$timeEnd = str_replace('T', ' ', $result["data"][$i]["ngayGoi"]);
							$history= BeanFactory::newBean("tttt_History_calls");
							$history->direction=$result["data"][$i]["typecall"];
							if($result["data"][$i]["typecall"] == "Inbound"){
								$que = "select tttt_worldfonepbx.assigned_user_id from tttt_worldfonepbx inner join tttt_worldfonepbx_cstm on tttt_worldfonepbx.id = tttt_worldfonepbx_cstm.id_c where tttt_worldfonepbx.description = '".$result["data"][$i]["soNhan"]."' 
								and tttt_worldfonepbx.deleted = 0 and tttt_worldfonepbx_cstm.status_c = '1'";
								$resu = $db->query($que);
								$r =  $db->fetchByAssoc($resu);
								$a = "";
								if($result["data"][$i]["trangThai"] == "NO ANSWERED"){
									for($j = 6 ; $j< strlen($result["data"][$i]["soGoiDen"]); $j++){
										if($result["data"][$i]["soGoiDen"][$j] != " "){
											$a.= $result["data"][$i]["soGoiDen"][$j];
										}else{
											break;
										}
									}
								}else{
									$a = $result["data"][$i]["soGoiDen"];
								}
								$history->userextension = $result["data"][$i]["soNhan"];
								$history->callstatus = $result["data"][$i]["trangThai"];
								$history->customernumberfrom=$a;
								$history->customernumberto=$result["data"][$i]["dauSo"];
							}else{
								$a = "";
								for($j = 6 ; $j< strlen($result["data"][$i]["soGoiDen"]); $j++){
									if($result["data"][$i]["soGoiDen"][$j] != " "){
										$a.= $result["data"][$i]["soGoiDen"][$j];
									}else{
										break;
									}
								}
				
								$que = "select tttt_worldfonepbx.assigned_user_id from tttt_worldfonepbx inner join tttt_worldfonepbx_cstm on tttt_worldfonepbx.id = tttt_worldfonepbx_cstm.id_c where tttt_worldfonepbx.description = '".$a."' 
								and tttt_worldfonepbx.deleted = 0 and tttt_worldfonepbx_cstm.status_c = '1'";
								$resu = $db->query($que);
								if(!empty($r)){
									$history->assigned_user_id=$r["assigned_user_id"];
								}
								$history->userextension = $a;
								$history->callstatus = $result["data"][$i]["trangThai"];
								$history->customernumberfrom=$result["data"][$i]["dauSo"];
								$history->customernumberto=$result["data"][$i]["soNhan"];
							}
							$history->starttime = date('Y-m-d H:i:s', strtotime($timeStart.' - 7 hours'));
							$history->endtime = date('Y-m-d H:i:s', strtotime($timeEnd.' - 7 hours'));
							$history->assigned_user_id=$r["assigned_user_id"];
							$history->totalduration=$second;
							if(!empty($result["data"][$i]["linkFile"])){
							$history->file_ghiam = $result["data"][$i]["linkFile"];
							}
							
							$history->save();
							if($result["data"][$i]["typecall"] == "Inbound"){
								$id = create_guid();
								$date = date('Y-m-d h:i:s');
								$query="select id from accounts  where phone_alternate='".$a."' And accounts.deleted='0'";
								$res=$db->query($query);
								$row=$db->fetchByAssoc($res);
								if(!empty($row)){
									$q="insert into  accounts_tttt_history_calls_1_c  (id,date_modified,accounts_tttt_history_calls_1accounts_ida,accounts_tttt_history_calls_1tttt_history_calls_idb) values ('".$id."','".$date."','".$row["id"]."','".$history->id."')";
									$db->query($q);
								}else{
									$query="select id from leads where (phone_mobile='".$a."' OR phone_work='".$a."' OR phone_other='".$a."') and leads.deleted='0' and status IN('New','In Process','Dead')";
									$res=$db->query($query);
									$row=$db->fetchByAssoc($res);
									if(!empty($row)){
										$q="insert into  leads_tttt_history_calls_1_c  (id,date_modified,leads_tttt_history_calls_1leads_ida,leads_tttt_history_calls_1tttt_history_calls_idb) values ('".$id."','".$date."','".$row["id"]."','".$history->id."')";
										$db->query($q);
									}else{
										$query="select id from contacts where (phone_mobile='".$a."' OR phone_work='".$a."' OR phone_other='".$a."') and contacts.deleted='0'";
										$res=$db->query($query);
										$row=$db->fetchByAssoc($res);
										if(!empty($row) ){
										$q="insert into  contacts_tttt_history_calls_1_c  (id,date_modified,contacts_tttt_history_calls_1contacts_ida,contacts_tttt_history_calls_1tttt_history_calls_idb) values ('".$id."','".$date."','".$row["id"]."','".$history->id."')";
										$db->query($q);
										}
									}
								}
							}else{
								$id = create_guid();
								$date = date('Y-m-d h:i:s');
								$query="select id from accounts  where phone_alternate='".$result["data"][$i]["soNhan"]."' And accounts.deleted='0'";
								$res=$db->query($query);
								$row=$db->fetchByAssoc($res);
								if(!empty($row)){
									$q="insert into  accounts_tttt_history_calls_1_c  (id,date_modified,accounts_tttt_history_calls_1accounts_ida,accounts_tttt_history_calls_1tttt_history_calls_idb) values ('".$id."','".$date."','".$row["id"]."','".$history->id."')";
									$db->query($q);
								}else{
									$query="select id from leads where (phone_mobile='".$result["data"][$i]["soNhan"]."' OR phone_work='".$result["data"][$i]["soNhan"]."' OR phone_other='".$result["data"][$i]["soNhan"]."') and leads.deleted='0' and status IN('New','In Process','Dead')";
									$res=$db->query($query);
									$row=$db->fetchByAssoc($res);
									if(!empty($row) ){
										$q="insert into  leads_tttt_history_calls_1_c  (id,date_modified,leads_tttt_history_calls_1leads_ida,leads_tttt_history_calls_1tttt_history_calls_idb) values ('".$id."','".$date."','".$row["id"]."','".$history->id."')";
										$db->query($q);
									}else{
										$query="select id from contacts where (phone_mobile='".$result["data"][$i]["soNhan"]."' OR phone_work='".$result["data"][$i]["soNhan"]."' OR phone_other='".$result["data"][$i]["soNhan"]."') and contacts.deleted='0'";
										$res=$db->query($query);
										$row=$db->fetchByAssoc($res);
										if(!empty($row) ){
										$q="insert into  contacts_tttt_history_calls_1_c  (id,date_modified,contacts_tttt_history_calls_1contacts_ida,contacts_tttt_history_calls_1tttt_history_calls_idb) values ('".$id."','".$date."','".$row["id"]."','".$history->id."')";
										$db->query($q);
										}
									}
								}
								
								
							}
							if($i == $result["total"] - 1){
								$fp = fopen('dateSysHistory_ods.txt', 'w');
								fwrite($fp, date("Y-m-d H:i:s", strtotime(date(str_replace("T"," ",($result["data"][$i]["ngayGoi"])))."+ 1 second")));
								;
								
								fclose($fp);
							}
						}
					}
				}else{
					$today = date("Y-m-d H:i:s");
					$startDate=getContent_ODS();
					if($today >= $startDate){
						$fp = fopen('dateSysHistory_ods.txt', 'w');
						fwrite($fp, date("Y-m-d H:i:s", strtotime(date($startDate)."+ 2 minutes")));
						fclose($fp);
					}
				}
			} catch (Exception $ex) {
				$GLOBALS['log']->error($ex->getMessage());
			}
			return true;
	}
    function callAPI_ODS()
    {
		$today = date("Y-m-d H:i:s");
		$startDate=getContent_ODS();
		$startEnd=date("Y-m-d H:i:s", strtotime(date($startDate)."+ 2 minutes"));
		$response = array();
		if($today >= $startEnd){
			$data = '
			{
				"ServiceCode" : "CF-PBX0001694",
				"AuthUser" : "ODS006776",
				"AuthKey" : "96c25368-29c8-402b-b88a-d0a95a27ad96",
				 "TypeGet": "0",
				 "DateStart":"'.$startDate.'",
				 "DateEnd" : "'.$startEnd.'",
				 "Key": "",
				 "PageIndex" : 1,
				 "PageSize" : 200
			}

			';
			
			try {
				$apiUrl=GET_REVENUE_API_URL_ODS;
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_HEADER, false);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_URL, $apiUrl);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$headers = [
				  'Content-type: application/json'
				];
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
				$response = curl_exec($ch);
				if (!empty($response)) {
					$response = json_decode($response, true);
					
				}
				curl_close($ch);
			} catch (Exception $ex) {
				$GLOBALS['log']->error($ex);
			}
		}else{
			$response["total"] = 0;
		}
        return $response;
    }

      
	
