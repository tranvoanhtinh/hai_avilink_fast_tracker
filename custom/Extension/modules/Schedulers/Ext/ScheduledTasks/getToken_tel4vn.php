<?php
array_push($job_strings, 'getToken_TEL4VN');
defined('GET_TOKEN_API_URL_TEL4VN') or define('GET_TOKEN_API_URL_TEL4VN', 'https://pitel04-api.tel4vn.com/v3/auth/token');
function getToken_TEL4VN()
    {
		try {
			global $db;
			$result = callAPITOKEN_TELE4VN();
			if(!empty($result["token"])){
				$fp = fopen('dateSysToken_tel4vn.txt', 'w');
				fwrite($fp,$result["token"]);
				fclose($fp);
			}
		} catch (Exception $ex) {
			$GLOBALS['log']->error($ex->getMessage());
		}
			return true;
	}
    function callAPITOKEN_TELE4VN()
    {
		$data = '
		{
			"api_key":"e4d7d2b4-8914-47a5-8c38-55462eecb6f9"
		}
		';
       
		$headers = [
              'Content-type: application/json',
            ];
        $response = array();
        try {
		    $apiUrl=GET_TOKEN_API_URL_TEL4VN;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_URL, $apiUrl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch,CURLOPT_FOLLOWLOCATION,true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            $response = curl_exec($ch);
            if (!empty($response)) {
                $response = json_decode($response, true);
            }
            curl_close($ch);
        } catch (Exception $ex) {
            $GLOBALS['log']->error($ex);
        }
        return $response;
    }
	
      
	
