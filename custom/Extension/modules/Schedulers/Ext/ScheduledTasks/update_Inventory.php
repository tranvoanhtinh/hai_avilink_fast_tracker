<?php
array_push($job_strings, 'update_Inventory');



function update_Inventory() {
    global $db;
    global $sugar_config;

    try {
        $time = file_get_contents("dateSysHistory_Inventory.txt");
        
        $sql = 'SELECT MAX(date_modified) AS last_modified_time FROM aos_stock_inventory_detail';
        $result = $db->query($sql);

        $row = $result->fetch_assoc();
        $time_new = $row['last_modified_time'];
        
        if ($time == $time_new) {
            return false;
        } else {
            file_put_contents("dateSysHistory_Inventory.txt", $time_new);
        }


        insert_Inventory_D();
        // Khởi tạo các biến ngày tháng
        $monthOffset = date('m') - 2;
        $currentYear =  date('Y');
        if ($monthOffset == 0) {
            $monthOffset = 12;
            // Giảm năm đi một đơn vị nếu tháng là tháng 0 (tháng 12 của năm trước đó)
            $currentYear = $currentYear - 1;
        }

        $startDate = "01/$monthOffset/$currentYear";
        $endDate = date('d/m/Y'); // Ngày hiện tại

        if (file_exists(getcwd() . '/config.php')) {
            require(getcwd() . '/config.php');
        }
        if(isset($sugar_config['inventory_date_start'])) {
            $startDate = $sugar_config['inventory_date_start'];
        }
        if(isset($sugar_config['inventory_date_end'])) {
            $endDate = $sugar_config['inventory_date_end'];
        }
        $fromDate = DateTime::createFromFormat('d/m/Y', $startDate);
        $toDate = DateTime::createFromFormat('d/m/Y', $endDate);

        $dateRanges = [];

        // Lặp qua các tháng từ ngày bắt đầu đến ngày kết thúc
        $currentDate = clone $fromDate;
        while ($currentDate <= $toDate) {
            $startOfMonth = $currentDate->format('01/m/Y');
            $endOfMonth = $currentDate->format('t/m/Y'); // 't' là số ngày trong tháng

            // Lưu trữ cặp ngày bắt đầu và kết thúc vào mảng
            $dateRanges[] = ['from' => $startOfMonth, 'to' => $endOfMonth];

            // Chuyển sang tháng tiếp theo
            $currentDate->modify('+1 month');
        }

        // Lặp qua các phạm vi ngày
        for ($index = 0; $index < count($dateRanges); $index++) {
            $fromDate = $dateRanges[$index]['from'];
            $toDate = $dateRanges[$index]['to'];

            $fromDate = DateTime::createFromFormat('d/m/Y', $fromDate)->format('Y-m-d');
            $toDate = DateTime::createFromFormat('d/m/Y', $toDate)->format('Y-m-d');

            $datePartsX = explode('-', $fromDate);

            // Lấy phần tử đầu tiên là tháng và phần tử thứ hai là năm
            $monthX = $datePartsX[1];
            $yearX = $datePartsX[0];

            // Lấy dữ liệu sản phẩm
            $productQuery = "SELECT * FROM `aos_products` WHERE deleted = 0 AND type = 'Good'";

            $productResult = $db->query($productQuery);
            $product[$index] = array(); // Mảng chứa thông tin sản phẩm
            $i = 0; // Biến đếm số lượng sản phẩm

            // Duyệt qua từng sản phẩm
            while ($row = $db->fetchByAssoc($productResult)) {
                // Gán dữ liệu từ hàng hiện tại vào mảng $product với key là id của sản phẩm
                $product[$index][$i]['id'] = $row['id'];
                $product[$index][$i]['name'] = $row['name'];
                $product[$index][$i]['ItemNo'] = $row['part_number'];

                // Lấy dữ liệu từ bảng chi tiết kho
                $inventoryQuery = "SELECT month, 
                                year, 
                                take_from, 
                                in_out,
                                SUM(quantity) AS quantity,
                                AVG(price) AS average_price
                         FROM aos_stock_inventory_detail 
                         WHERE product_id = '" . $row['id'] . "' 
                             AND deleted = 0 
                             AND voucherdate  >= '" . $fromDate . "' 
                             AND voucherdate  <= '" . $toDate . "'
                         GROUP BY month, year, take_from, in_out";

                $inventoryResult = $db->query($inventoryQuery);
                $j = 0; // Biến đếm số lượng bản ghi chi tiết

                while ($inventoryRow = $db->fetchByAssoc($inventoryResult)) {
                    // Lặp qua mỗi phần tử trong mảng các phạm vi ngày
                    for ($m = 0; $m < count($dateRanges); $m++) {
                        $dateRanges[$m]['from'];
                        $dateFromY = $dateRanges[$m]['from'];
                        $datePartsY = explode('/', $dateFromY);
                        $monthY = $datePartsY[1];
                        $yearY = $datePartsY[2];

                        if ($monthY == $monthX) {
                            $product[$index][$i]['detail'][$j][$m]['take_from'] = $inventoryRow['take_from'];
                            $product[$index][$i]['detail'][$j][$m]['month'] = $monthY;
                            $product[$index][$i]['detail'][$j][$m]['year'] = $yearY;
                            if ($inventoryRow['in_out'] == 'In') {
                                $product[$index][$i]['detail'][$j][$m]['in'] = $inventoryRow['quantity'];
                                $product[$index][$i]['detail'][$j][$m]['out'] = 0;
                            } else {
                                $product[$index][$i]['detail'][$j][$m]['out'] = $inventoryRow['quantity'];
                                $product[$index][$i]['detail'][$j][$m]['in'] = 0;
                            }
                            $product[$index][$i]['detail'][$j][$m]['price'] = $inventoryRow['average_price'];
                        } else {
                            $product[$index][$i]['detail'][$j][$m]['take_from'] = $inventoryRow['take_from'];
                            $product[$index][$i]['detail'][$j][$m]['month'] = $monthY;
                            $product[$index][$i]['detail'][$j][$m]['year'] = $yearY;
                            $product[$index][$i]['detail'][$j][$m]['in'] = 0;
                            $product[$index][$i]['detail'][$j][$m]['out'] = 0;
                            $product[$index][$i]['detail'][$j][$m]['price'] = 0;
                        }
                    }
                    $j++; // Tăng biến đếm số lượng bản ghi chi tiết
                }
                $i++; // Tăng biến đếm số lượng sản phẩm
            }
        }

        // Xóa dữ liệu cũ trong bảng aos_stock_inventory
        for ($n = 0; $n < count($dateRanges); $n++) {
            $dateFromZ = $dateRanges[$n]['from'];
            $datePartsZ = explode('/', $dateFromZ);
            $monthZ = $datePartsZ[1];
            $yearZ = $datePartsZ[2];
            $inventoryDeleteQuery = "DELETE FROM `aos_stock_inventory` WHERE month = '" . $monthZ . "' AND year = '" . $yearZ . "'";
            $inventoryDeleteResult = $db->query($inventoryDeleteQuery);
        }
        // Lưu dữ liệu mới vào bảng aos_stock_inventory
        for ($y = 0; $y < count($dateRanges); $y++) {
            foreach ($product[$y] as $key => $value) {
                $productId = $value['id'];
                $productName = $value['name'];
                $itemNo = $value['ItemNo'];

                // Lặp qua mảng 'detail'
                foreach ($value['detail'] as $detailArray) {
                    // Lặp qua các mảng lồng bên trong 'detail'
                    foreach ($detailArray as $detail) {
                        $takeFrom = $detail['take_from'];
                        $month = $detail['month'];
                        $year = $detail['year'];
                        $in = $detail['in'];
                        $out = $detail['out'];
                        $price = $detail['price'];



                                // Ghi lại câu truy vấn SQL2
                                $inventoryInsertQuery = "INSERT INTO aos_stock_inventory (id, name, maincode, month, year, product_id, import, export, price, warehouse)
                                    VALUES ('" . getGUID() . "', '$productName', '$itemNo', '$month', '$year', '$productId', '$in', '$out', '$price', '$takeFrom')";
                                // Thực thi truy vấn SQL2
                                $inventoryInsertResult = $db->query($inventoryInsertQuery);


                }
            }
        }
    }


$start = date("Y-m-d", strtotime(str_replace('/', '-', $dateRanges[0]['from'])));
$end = date("Y-m-d", strtotime(str_replace('/', '-', end($dateRanges)['to'])));

$startMonth = date("m", strtotime($start)); // Lấy tháng từ ngày bắt đầu
$startYear = date("Y", strtotime($start)); // Lấy năm từ ngày bắt đầu
$endMonth = date("m", strtotime($end)); // Lấy tháng từ ngày kết thúc
$endYear = date("Y", strtotime($end)); // Lấy năm từ ngày kết thúc

$inventoryGroupByQuery = "SELECT 
    product_id,
    name,
    month,
    maincode,
    AVG(price) AS avg_price,
    year,
    warehouse,
    SUM(import) AS total_import,
    SUM(export) AS total_export,
    beginning_balance,
    (SUM(import) + beginning_balance - SUM(export)) AS ending_balance 
FROM 
    aos_stock_inventory 
WHERE 
    deleted = 0 ";

// Thêm điều kiện về tháng và năm từ ngày bắt đầu đến ngày kết thúc
$inventoryGroupByQuery .= "AND (
    (year = $startYear AND month >= $startMonth AND year = $endYear AND month <= $endMonth)
    OR
    (year = $startYear AND month >= $startMonth AND year < $endYear)
    OR
    (year = $endYear AND month <= $endMonth AND year > $startYear)
    OR
    (year > $startYear AND year < $endYear)
)";

// Tiếp tục với phần GROUP BY
$inventoryGroupByQuery .= "GROUP BY 
    month, year, warehouse, product_id";


        $inventoryGroupByResult = $db->query($inventoryGroupByQuery);
        if ($inventoryGroupByResult) {

                // Xóa dữ liệu cũ trong bảng aos_stock_inventory
                for ($n = 0; $n < count($dateRanges); $n++) {

                    $dateFromZ = $dateRanges[$n]['from'];
                    $datePartsZ = explode('/', $dateFromZ);
                    $monthZ = $datePartsZ[1];



                    $yearZ = $datePartsZ[2];
                    $inventoryDeleteQuery = "DELETE FROM `aos_stock_inventory` WHERE month = '" . $monthZ . "' AND year = '" . $yearZ . "'";
                    $inventoryDeleteResult = $db->query($inventoryDeleteQuery);

                }


            while ($row = $db->fetchByAssoc($inventoryGroupByResult)) {
                    $price = 0;
                    $total_price = 0;
                    $sl_nhap = $row['total_import'];
   
                    if($sl_nhap !=0){
                    $get_AvgPrice = "SELECT AVG(price) as avg_price
                            FROM aos_stock_inventory_detail 
                            WHERE product_id = '".$row['product_id']."' 
                            AND deleted = 0 
                            AND `parent_id` IS NOT NULL 
                            AND voucherdate  >= '" . $fromDate . "' 
                            AND voucherdate  <= '" . $toDate . "'
                            AND take_from = '".$row['warehouse']."'";

                            $result_AvgPrice = $db->query($get_AvgPrice);
                            $rowAvgPrice = $result_AvgPrice->fetch_assoc();
                            if($rowAvgPrice){
                                $price = $rowAvgPrice['avg_price'];
                            }

                }
                $total_price = $sl_nhap * $price;
  
                $info_product = "SELECT unitcode, assigned_user_id FROM aos_products WHERE id = '".$row['product_id']."'AND type = 'Good'";
                $result_product = $db->query($info_product);
                while ($row_product = $result_product->fetch_assoc()) {
                     $unitcode = $row_product['unitcode'];
                     $assigned_user_id = $row_product['assigned_user_id'];


                $inventoryInsertQuery = "INSERT INTO aos_stock_inventory (assigned_user_id,unitcode,id, name, maincode, month, year, product_id, import, export, price, warehouse, beginning_balance, ending_balance, total_price) VALUES ";
                $inventoryInsertQuery .= "('".$assigned_user_id."','".$unitcode."','" . getGUID() . "', '".$row['name']."', '".$row['maincode']."', '".$row['month']."', '".$row['year']."', '".$row['product_id']."', '".$row['total_import']."', '".$row['total_export']."', '".$price."', '".$row['warehouse']."', '".$row['beginning_balance']."', '".$row['ending_balance']."', '".$total_price."')";
                // Thực thi truy vấn SQL2

                $inventoryInsertResult = $db->query($inventoryInsertQuery);

                }
            }
        }


// Thực hiện truy vấn cho các bản ghi có dữ liệu
$inventoryDataQuery = "SELECT month, year, `export`, `import`, price, beginning_balance, warehouse, ending_balance, product_id
          FROM `aos_stock_inventory`
          WHERE deleted = 0
          ORDER BY year ASC, month ASC";

// Thực thi truy vấn
$inventoryDataResult = $db->query($inventoryDataQuery);

// Kiểm tra kết quả truy vấn
if ($inventoryDataResult) {

    // Duyệt qua từng dòng kết quả
    while ($row = $inventoryDataResult->fetch_assoc()) {
        // Lấy thông tin từ bản ghi hiện tại
        $productId  = $row['product_id'];
        $month = $row['month'];
        $year = $row['year'];
        $export = $row['export'];
        $import = $row['import'];
        $warehouse = $row['warehouse'];

        // Tính toán tháng và năm trước đó

        $prevMonth = sprintf("%02d", $month - 1);
        $prevYear = $year;
        if ($prevMonth == '00') {
            $prevMonth = '12';
            $prevYear = $year - 1;
        }


        // Truy vấn để lấy dữ liệu của tháng trước đó
        $prevQuery = "SELECT `import`, `export`, beginning_balance
                       FROM `aos_stock_inventory`
                       WHERE product_id = '$productId' AND month = '$prevMonth' AND year = '$prevYear' AND warehouse = '$warehouse' AND deleted = 0";
        $prevResult = $db->query($prevQuery);

        // Kiểm tra kết quả truy vấn
        if ($prevResult && $prevResult->num_rows > 0) {

            $prevRow = $prevResult->fetch_assoc();
            $beginningBalance = $prevRow['beginning_balance'] + $prevRow['import'] - $prevRow['export'];

        } else {
            
            $beginningBalance = 0;
        }


        
        // Tính toán tồn cuối kỳ
        $endingBalance = $beginningBalance + $import - $export;



        // Cập nhật bản ghi hiện tại với beginning_balance và ending_balance tính toán được
        $updateQuery = "UPDATE `aos_stock_inventory`
                         SET `beginning_balance` = '$beginningBalance', `ending_balance` = '$endingBalance'
                         WHERE product_id = '$productId' AND month = '$month' AND year = '$year' AND warehouse = '$warehouse' AND deleted = 0";
        $db->query($updateQuery);

    }
}


      return true;
    } catch (Exception $e) {
       return false;
    }
}



function insert_Inventory_D()
{
    global $db;

    $currentDate = date("Y-m-d");
    $year = date("Y", strtotime($currentDate));
    $month = date("m", strtotime($currentDate));
    $day = date("d", strtotime($currentDate));

    $sql = "SELECT product_id, take_from, name, maincode
            FROM aos_stock_inventory_detail
            WHERE deleted = 0
            GROUP BY product_id, take_from";
    $result = $db->query($sql);

    // Duyệt qua từng hàng kết quả
    while ($row = $db->fetchByAssoc($result)) {
        $existingCheckSqlIn = "SELECT COUNT(*) AS count
                               FROM aos_stock_inventory_detail
                               WHERE month = '" . $month . "'
                               AND year = '" . $year . "'
                               AND take_from = '" . $row['take_from'] . "'
                               AND product_id = '" . $row['product_id'] . "'
                               AND in_out = 'In'
                               AND deleted = 0";
        $existingCheckResultIn = $db->query($existingCheckSqlIn);
        $existingRowIn = $db->fetchByAssoc($existingCheckResultIn);
        $existingCountIn = $existingRowIn['count'];


        // Kiểm tra xem mẫu tin đã tồn tại với in_out = 'Out' hay chưa
        $existingCheckSqlOut = "SELECT COUNT(*) AS count
                                FROM aos_stock_inventory_detail
                                WHERE month = '" . $month . "'
                                AND year = '" . $year . "'
                                AND take_from = '" . $row['take_from'] . "'
                                AND product_id = '" . $row['product_id'] . "'
                                AND in_out = 'Out'
                                AND deleted = 0";
        $existingCheckResultOut = $db->query($existingCheckSqlOut);
        $existingRowOut = $db->fetchByAssoc($existingCheckResultOut);
        $existingCountOut = $existingRowOut['count'];

        if ($existingCountIn == 0) {
            // INSERT với in_out = 'In'
            $insert_in = "INSERT INTO aos_stock_inventory_detail (id, name, deleted, voucherdate, maincode, quantity, price, day, month, product_id, year, take_from, in_out, total_price) 
                          VALUES ('" . getGUID() . "', '" . $row['name'] . "','0', '" . $currentDate . "', '" . $row['maincode'] . "', '0', '0', '" . $day . "', '" . $month . "', '" . $row['product_id'] . "', '" . $year . "', '" . $row['take_from'] . "', 'In', '0')";
            $db->query($insert_in);    
        } 
        if ($existingCountOut == 0) {
            // INSERT với in_out = 'Out'
            $insert_out = "INSERT INTO aos_stock_inventory_detail (id, name, deleted, voucherdate, maincode, quantity, price, day, month, product_id, year, take_from, in_out, total_price) 
                           VALUES ('" . getGUID() . "', '" . $row['name'] . "','0', '" . $currentDate . "', '" . $row['maincode'] . "', '0', '0', '" . $day . "', '" . $month . "', '" . $row['product_id'] . "', '" . $year . "', '" . $row['take_from'] . "', 'Out', '0')";
            $db->query($insert_out);
        } 
    }
}


// Hàm tạo GUID
function getGUID()
{
    if (function_exists('com_create_guid')) {
        $uuid = com_create_guid();
    } else {
        mt_srand((double)microtime()*10000); // optional for PHP 4.2.0 and up.
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45); // "-"
        $uuid = substr($charid, 0, 8) . $hyphen
            . substr($charid, 8, 4) . $hyphen
            . substr($charid, 12, 4) . $hyphen
            . substr($charid, 16, 4) . $hyphen
            . substr($charid, 20, 12);
    }

    // Chuyển đổi $uuid thành chữ thường và trả về
    return strtolower($uuid);
}
