<?php

//$job_strings[]='getRevenueFromErp';
array_push($job_strings, 'getRevenueFromErp');
defined('GET_REVENUE_API_URL') or define('GET_REVENUE_API_URL', 'https://ws.ntlogistics.vn/NTLIntegration/api/v1/ReportCustomerRevenueDetail');
defined('GET_REVENUE_API_TOKEN') or define('GET_REVENUE_API_TOKEN', 'crmNtAn4mjqd3yChw7L');



function getRevenueFromErp()
{
	try {
	
    $startDate=getContent();
	$postData = array("ClientKey"=>"crm","StartDate"=>$startDate);
	$result = callApiFromErp($postData);
	if (!empty($result["Data"])) {
		foreach($result["Data"] as $record){
			if($record["CRMUser"] != null && $record["ShipperName"] != null && $record["ShipperCode"] != null){
					$ngay=str_replace('/', '-', $record["Ngay"]);
					$date=date("Y-m-d H:i:s",strtotime($ngay));
					$date1=date("Y-m-d",strtotime($ngay));
					$opporBean = BeanFactory::newBean('Opportunities');
					$opporBean->assigned_user_id=$record["CRMUser"];
					$opporBean->payment_form=$record["HTTT"];
					$opporBean->sender=$record["NVTra"];
					$opporBean->receive_post_office=$record["BCNhan"];
					$opporBean->public_price=$record["PublicAmt"];
					$opporBean->parcel_quantity=$record["Sokien"];
					$opporBean->customer_id=$record["ShipperCode"];
					$opporBean->shipment_price=$record["Doanhthu"];
					$opporBean->receiver=$record["NVNhan"];
					$opporBean->main_price=$record["Main_Fee"];
					$opporBean->name=$record["Bill"];
					$opporBean->date_closed=$date1;
					$opporBean->bill=$record["Bill"];
					$opporBean->amount=$record["PublicAmt"];
					$opporBean->shipper_name=$record["ShipperName"];
					$opporBean->service=$record["Dichvu"];
					$opporBean->pay_branch=$record["Chinhanhtra"];
					$opporBean->bill_date=$date;
					$opporBean->send_post_office=$record["BCTra"];
					$opporBean->weight=$record["Trongluong"];
					$opporBean->move_branch=$record["Chinhanhdi"];
					$opporBean->to_province=$record["Matinhtra"];
					$opporBean->from_province=$record["Matinhdi"];
					$opporBean->employee_code=$record["MaNVKD"];
					$opporBean->employee_name=$record["NVKD"];
					$opporBean->save();
					$startDate= date("d/m/Y H:i:s",strtotime($record["RevDate"]));
					$accountBean= new Account(); 
					$accountBean->retrieve_by_string_fields(array('account_code' => $record["ShipperCode"]));
					if (!empty($accountBean)) {
				  $accountBean->load_relationship('opportunities');
				   $accountBean->opportunities->add($opporBean);
					}	
			}	
		}
				$fp = fopen('dateSys.txt', 'w');
				fwrite($fp, $startDate);
				fclose($fp);
			
	}
	} catch (Exception $ex) {
        $GLOBALS['log']->error($ex->getMessage());
    }
	return true;
      
}
function callApiFromErp($postData)
    {
        $response = array();
        try {
           
	    $apiUrl = GET_REVENUE_API_URL;
            $info = json_encode($postData, JSON_UNESCAPED_UNICODE);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_URL, $apiUrl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $info);
			
			$headers = [
                'Content-Type: application/json',
                'Authorization: ' .  md5(GET_REVENUE_API_TOKEN.$info),
				'Accept: application/json'
            ];
			
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            $response = curl_exec($ch);
            if (!empty($response)) {
                $response = json_decode($response, true);
            }
            curl_close($ch);
        } catch (Exception $ex) {
            $GLOBALS['log']->error($ex);
        }
		
        return $response;
		
		
    }

function getContent()
{
	return file_get_contents("dateSys.txt");
}

	
