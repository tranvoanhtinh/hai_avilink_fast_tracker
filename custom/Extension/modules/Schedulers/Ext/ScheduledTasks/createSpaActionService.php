<?php
array_push($job_strings, 'CreateSpaActionService');

function CreateSpaActionService()
{

    global $db;
    if (file_exists(getcwd() . '/config.php')) {
        require(getcwd() . '/config.php');
    }
    if (isset($sugar_config['prefix_name_auto_actionservice'])) {
        $name_prefix = $sugar_config['prefix_name_auto_actionservice'];
    }
    $sql_status = "SELECT * FROM aos_invoices WHERE check_status = 0 AND deleted = 0 ";
    $result_status = $db->query($sql_status);
    while ($row_status = $db->fetchByAssoc($result_status)) {

        // $status_invoice = $row_status['status'];
        // $discount_amount_spa = $row_status['discount_amount_spa']; //phần trăm chiết khấu
        $id_invoices = $row_status['id'];

        $phone = $row_status['mobileref'];
        $timenew = $row_status['date_modified'];

        //------------------
        // $name_prefix = 'LT';
        //-------------------

        $sql_qlty = "SELECT *
            FROM aos_products_quotes
            where deleted = 0 and parent_id =  '" . $id_invoices . "'   ";
        $result_qty = $db->query($sql_qlty);
        $index = 1; // Chỉ mục cho sản phẩm
        while ($row_qty = $db->fetchByAssoc($result_qty)) {

            $id_quotes = $row_qty['id'];
            $assigned = $row_qty['assigned_user_id'];
            // $product_unitprice_emp = $row_qty['unitprice_emp'];
            $product_parent = $row_qty['parent_id'];
            $product_id = $row_qty['product_id'];

            $product_qty = $row_qty['product_qty'];
            $sql_name_product = "SELECT * FROM aos_products WHERE id = '" . $product_id . "'    ";
            $result_product = $db->query($sql_name_product);
            while ($row_name = $db->fetchByAssoc($result_product)) {
                $price = $row_name['price'];
                $name_product  =  $row_name['name'];
                $sql_rose = "SELECT * FROM aos_products_quotes_cstm WHERE id_c = '" . $id_quotes . "'  ";
                $result_rose = $db->query($sql_rose);
                while ($row_rose = $db->fetchByAssoc($result_rose)) {
                    $rose = $row_rose['commisonservice_c'];
                    $phantram_rose = $row_rose['parkservice_c'];
                    for ($i = 1; $i <= $product_qty; $i++) {
                        $id_spa = id_auto();
                        $name = $name_prefix . '_' . $name_product . '_' . $i;
                        //$name = $name_prefix . '_' . $name_product . '_' . $index; // Tạo tên mới với chỉ mục lieutrinh_
                        $insert = "INSERT INTO spa_actionservice(`id`, `name`,`date_entered`, `action_date`,`status`,`assigned_user_id`,`mobileref`,`amountservice`,`unit_price_input`,`discount_amount_spa`) 	
                        VALUES ('" . $id_spa . "','" . $name . "' ,'" . $timenew . "','" . $timenew . "','Not Started','" . $assigned . "','" . $phone . "','" . $price . "','" . $price . "','0')";
                        $result1 = $db->query($insert);
                        //--insert bảng quan hệ sản phẩm và liệu trình
                        if ($result1) {
                            $insert2 = "INSERT INTO `aos_products_spa_actionservice_1_c`(`id`, `date_modified`, `deleted`, `aos_products_spa_actionservice_1aos_products_ida`, `aos_products_spa_actionservice_1spa_actionservice_idb`) 
                        VALUES ('" . id_auto() . "','" . $timenew . "','0','" . $product_id . "','" . $id_spa . "')";
                            $result2 = $db->query($insert2);
                        }

                        //--insert bảng quan hệ liệu trình và đơn hàng
                        if ($result2) {
                            $insert3 = " INSERT INTO `spa_actionservice_aos_invoices_c`(`id`, `date_modified`, `deleted`, `spa_actionservice_aos_invoicesaos_invoices_ida`, `spa_actionservice_aos_invoicesspa_actionservice_idb`)
                        VALUES ('" . id_auto() . "', '" . $timenew . "', 'a0', '" . $product_parent . "','" . $id_spa . "' )";
                            $result3 = $db->query($insert3);
                        }
                        //--insert bảng quan hệ khách hàng và liệu trình
                        if ($result3) {
                            $sql_invoice = "SELECT	billing_account_id FROM `aos_invoices` WHERE id = '" . $id_invoices . "' ";
                            $result_invoice = $db->query($sql_invoice);
                            $row_invoice = $db->fetchByAssoc($result_invoice);
                            $insert4 = "INSERT INTO `accounts_spa_actionservice_1_c`(`id`, `date_modified`, `deleted`, `accounts_spa_actionservice_1accounts_ida`, `accounts_spa_actionservice_1spa_actionservice_idb`) 
                        VALUES ('" . id_auto() . "','" . $timenew . "','0','" . $row_invoice['billing_account_id'] . "','" . $id_spa . "')";
                            $result4 = $db->query($insert4);
                        }
                        //cập nhật aos_invoice bằng 0 sẽ thành 1 khi đã 
                        if ($result4) {
                            $update_status = "UPDATE aos_invoices SET check_status = 1 WHERE check_status = 0";
                            $result_status = $db->query($update_status);
                        }

                        $index++; // Tăng chỉ mục lên cho sản phẩm tiếp theo
                    }
                }
            }
        }
    }
    return true;
}


//cái náy sẽ tạo ra id từ động
function id_auto()
{
    if (function_exists('com_create_guid')) {
        $uuid = com_create_guid();
    } else {
        mt_srand((float)microtime() * 10000); // optional for PHP 4.2.0 and up.
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45); // "-"
        $uuid = substr($charid, 0, 8) . $hyphen
            . substr($charid, 8, 4) . $hyphen
            . substr($charid, 12, 4) . $hyphen
            . substr($charid, 16, 4) . $hyphen
            . substr($charid, 20, 12);
    }

    // Chuyển đổi $uuid thành chữ thường và trả về
    return strtolower($uuid);
}
