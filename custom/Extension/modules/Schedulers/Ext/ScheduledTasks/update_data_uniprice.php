<?php
array_push($job_strings, 'update_data_uniprice');

function update_data_uniprice() {
    global $db;
    $date = new DateTime();
    $date_modified = $date->format('Y-m-d H:i:s');
    $qAgent0 = "SELECT * FROM `accounts` WHERE deleted = '0'";
    $resultA0 = $db->query($qAgent0);
    while ($rowA0 = $db->fetchByAssoc($resultA0)) {
        $account_id = $rowA0['id'];
        $qAgent1 = "SELECT * FROM `accounts_aos_data_unitprice_1_c` WHERE accounts_aos_data_unitprice_1accounts_ida = '".$account_id."' AND deleted ='0'";
        $resultA1 = $db->query($qAgent1);
        $checkA1 = $resultA1->num_rows;
        $rowA99 = $resultA1->fetch_assoc();
        if ($checkA1 < 1) {
            // có id account hợp lệ 
            $qSevice0 = "SELECT * FROM `aos_products` WHERE deleted = '0' AND update_price = '2'";
            $resultS0 = $db->query($qSevice0);

            while ($rowS0 = $db->fetchByAssoc($resultS0)) {
                $unitPriceBean = BeanFactory::newBean('AOS_Data_unitprice');
                $unitPriceBean->name = $rowS0['name'];
                $unitPriceBean->service_code = $rowS0['part_number'];
                $unitPriceBean->price = $rowS0['price'];
                $unitPriceBean->status = 2;
                $unitPriceBean->service_date = date('Y-m-d H:i:s'); 
                $unitPriceBean->product_id = $rowS0['id']; // Dòng này sửa từ $rowS0->id sang $rowS0['id']
                $unitPriceBean->exchange_rate = $rowS0['exchange_rate'];
                $unitPriceBean->type_currency = $rowS0['type_currency'];
                $unitPriceBean->assigned_user_id = $rowS0['assigned_user_id'];
                $unitPriceBean->account_id = $account_id;
                $id_unitPrice = $unitPriceBean->save();

                $unitPriceBean->load_relationship('accounts_aos_data_unitprice_1');
                $unitPriceBean->accounts_aos_data_unitprice_1->add($account_id);
            }
        }

        $qAgent2 = "SELECT * FROM `accounts_aos_cost_unitprice_1_c` WHERE accounts_aos_cost_unitprice_1accounts_ida = '".$account_id."' AND deleted ='0'";
        $resultA2 = $db->query($qAgent2);
        $checkA2 = $resultA2->num_rows;
        $rowA98 = $resultA1->fetch_assoc();
        if ($checkA2 < 1) {
              // có id account hợp lệ 
            $qSevice0 = "SELECT * FROM `aos_products` WHERE deleted = '0' AND update_price = '2'";
            $resultS0 = $db->query($qSevice0);
            while ($rowS0 = $db->fetchByAssoc($resultS0)) {
                $costPriceBean = BeanFactory::newBean('AOS_Cost_unitprice');
                $costPriceBean->name = $rowS0['name'];
                $costPriceBean->service_code = $rowS0['part_number'];
                $costPriceBean->price = $rowS0['price'];
                $costPriceBean->status = 2;
                $costPriceBean->service_date = date('Y-m-d H:i:s'); 
                $costPriceBean->product_id = $rowS0['id']; // Dòng này sửa từ $rowS0->id sang $rowS0['id']
                $costPriceBean->account_id = $account_id;
                $costPriceBean->exchange_rate = $rowS0['exchange_rate'];
                $costPriceBean->type_currency = $rowS0['type_currency'];
                $costPriceBean->assigned_user_id = $rowS0['assigned_user_id'];
                $id_costPrice = $costPriceBean->save();
                $costPriceBean->load_relationship('accounts_aos_cost_unitprice_1');
                $costPriceBean->accounts_aos_cost_unitprice_1->add($account_id);
            }
        }
    }



    $qSevice1 = "SELECT * FROM `aos_products` WHERE deleted = '0' AND update_price = '2'";
    $resultS1 = $db->query($qSevice1);
    while ($rowS1 = $db->fetchByAssoc($resultS1)) {


        $qAgent2 = "SELECT * FROM `accounts` WHERE deleted = '0'";
        $resultA2 = $db->query($qAgent2);
        while ($rowA2 = $db->fetchByAssoc($resultA2)) {

            $account_id2 = $rowA2['id'];
            $qDataprice = "SELECT * FROM `aos_data_unitprice` WHERE product_id ='".$rowS1['id']."' AND account_id='".$rowA2['id']."' AND deleted = '0'";

            $resultD0 = $db->query($qDataprice);
            $checkD0 = $resultD0->num_rows; // Sửa thành phương thức fetchByAssoc() để lấy dữ liệu từ kết quả truy vấn

            if ($checkD0<1) { // Sửa điều kiện kiểm tra để đảm bảo đối tượng không tồn tại
                $unitPriceBean1 = BeanFactory::newBean('AOS_Data_unitprice');
                $unitPriceBean1->name = $rowS1['name'];
                $unitPriceBean1->service_code = $rowS1['part_number'];
                $unitPriceBean1->price = $rowS1['price'];
                $unitPriceBean1->status = 2;
                $unitPriceBean1->service_date = date('Y-m-d H:i:s'); 
                $unitPriceBean1->product_id = $rowS1['id'];
                $unitPriceBean1->exchange_rate = $rowS1['exchange_rate'];
                $unitPriceBean1->type_currency = $rowS1['type_currency'];
                $unitPriceBean1->assigned_user_id = $rowS1['assigned_user_id'];
                $unitPriceBean1->account_id = $account_id2;
                $id_unitPrice1 = $unitPriceBean1->save();
                $unitPriceBean1->load_relationship('accounts_aos_data_unitprice_1');
                $unitPriceBean1->accounts_aos_data_unitprice_1->add($account_id2);
            } 

            $qCostprice = "SELECT * FROM `aos_cost_unitprice` WHERE product_id ='".$rowS1['id']."' AND account_id='".$rowA2['id']."' AND deleted = '0'";
            $resultC0 = $db->query($qCostprice);
            $checkC0 = $resultC0->num_rows;


            if ($checkC0<1) { // Sửa điều kiện kiểm tra để đảm bảo đối tượng không tồn tại
                $costPriceBean1 = BeanFactory::newBean('AOS_Cost_unitprice');
                $costPriceBean1->name = $rowS1['name'];
                $costPriceBean1->service_code = $rowS1['part_number'];
                $costPriceBean1->price = $rowS1['price'];
                $costPriceBean1->status = 2;
                $costPriceBean1->service_date = date('Y-m-d H:i:s'); 
                $costPriceBean1->product_id = $rowS1['id'];
                $costPriceBean1->account_id = $account_id2; // Sửa thành $account_id2 để đảm bảo phù hợp với biến đã được định nghĩa
                $costPriceBean1->exchange_rate = $rowS1['exchange_rate'];
                $costPriceBean1->type_currency = $rowS1['type_currency'];
                $costPriceBean1->assigned_user_id = $rowS1['assigned_user_id'];
                $id_costPrice1 = $costPriceBean1->save();
                $costPriceBean1->load_relationship('accounts_aos_cost_unitprice_1');
                $costPriceBean1->accounts_aos_cost_unitprice_1->add($account_id2);
            } 
        }
    }
    //========== chạy / hủy : hoạt động ========
    $date = new DateTime();
    $date_modified = $date->format('Y-m-d H:i:s');

    $qSevice2 = "SELECT * FROM `aos_products` WHERE deleted = '0'";
    $resultS2 = $db->query($qSevice2);
    while ($rowS2 = $db->fetchByAssoc($resultS2)) {

        $status_update = $rowS2['update_price'];

        $qAgent3 = "SELECT * FROM `accounts` WHERE deleted = '0'";
        $resultA3 = $db->query($qAgent3);
        while ($rowA3 = $db->fetchByAssoc($resultA3)) {
            $qDataprice1 = "SELECT * FROM `aos_data_unitprice` WHERE product_id ='".$rowS2['id']."' AND account_id='".$rowA3['id']."' AND deleted = '0'";

            $resultD1 = $db->query($qDataprice1);
            $checkD1 = $resultD1->num_rows; 

            $rowD1 = $resultD1->fetch_assoc();
            if ($checkD1 > 0) { // Sửa điều kiện kiểm tra để đảm bảo đối tượng không tồn tại
                $unitPriceBean2 = BeanFactory::getBean('AOS_Data_unitprice', $rowD1['id']);

                if($unitPriceBean2->status != $status_update){
                    $unitPriceBean2->status = $status_update;
                    $unitPriceBean2->date_modified = $date_modified;
                    $unitPriceBean2->save();
                }
            }

            $qCostprice1 = "SELECT * FROM `aos_cost_unitprice` WHERE product_id ='".$rowS2['id']."' AND account_id='".$rowA3['id']."' AND deleted = '0'";
            $resultC1 = $db->query($qCostprice1);
            $checkC1 = $resultC1->num_rows;
            $rowC1 = $resultC1->fetch_assoc();

            if ($checkC1 > 0) { // Sửa điều kiện kiểm tra để đảm bảo đối tượng không tồn tại
                $costPriceBean2 = BeanFactory::getBean('AOS_Cost_unitprice', $rowC1['id']);
                if($costPriceBean2->status != $status_update){
                    $costPriceBean2->status = $status_update;
                    $costPriceBean2->date_modified = $date_modified;
                    $costPriceBean2->save();
                }
            } 
        }
    }
    return true;
}
   