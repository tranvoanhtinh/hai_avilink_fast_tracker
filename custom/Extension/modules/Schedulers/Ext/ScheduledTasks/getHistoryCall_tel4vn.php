<?php
array_push($job_strings, 'getInfo_TEL4VN');
defined('GET_REVENUE_API_URL_TEL4VN') or define('GET_REVENUE_API_URL_TEL4VN', 'https://pitel04-api.tel4vn.com/v3/cdr?');
date_default_timezone_set('Asia/ho_chi_minh'); 
$year = getdate();
if($year["mday"] <= 9){
	if($year["mday"] < 9){
	$dayFrom = "0".($year["mday"]-1);
	$dayTo = "0".($year["mday"]+1);
	}else{
	$dayFrom = "0".($year["mday"]-1);
	$dayTo = $year["mday"]+1;
	}
}else{
	if($year["mday"] == 10){
		$dayFrom = "0".($year["mday"]-1);
		$dayTo = $year["mday"]+1;
	}else{
		$dayFrom = $year["mday"]-1;
		$dayTo = $year["mday"]+1;
	}
}

if($year["mon"] < 10){
	$mon = "0".$year["mon"];
}else{
	$mon = $year["mon"];
}
function getContent_TEL4VN()
{
	return file_get_contents("dateSysHistory_tel4vn.txt");
}
function getTokenContent_TEL4VN()
{
	return file_get_contents("dateSysToken_tel4vn.txt");
}
function getInfo_TEL4VN()
    {
		try {
				global $db;
				$result = callAPI_TEL4VN();
				if(count($result["data"]) > 0){
					for($i = 0;$i < count($result["data"]);$i++){
						if(!empty($result["data"])){
							$second=gmdate('H:i:s',$result["data"][$i]["duration"]);
							$timeStart = str_replace('T', ' ',$result["data"][$i]["time_started"]);
							$timeStart = str_replace('Z', '',$timeStart);
							$timeEnd = str_replace('T', ' ',$result["data"][$i]["time_ended"]);
							$timeEnd = str_replace('Z', '',$timeEnd);
							//$timeStart = str_replace('T', ' ', $result["data"][$i]["ngayGoi"]);
							//$timeEnd = str_replace('T', ' ', $result["data"][$i]["ngayGoi"]);
							$history= BeanFactory::newBean("tttt_History_calls");
							$history->direction=$result["data"][$i]["direction"];
							if($result["data"][$i]["direction"] == "inbound"){
								$que = "select tttt_worldfonepbx.assigned_user_id from tttt_worldfonepbx inner join tttt_worldfonepbx_cstm on tttt_worldfonepbx.id = tttt_worldfonepbx_cstm.id_c where tttt_worldfonepbx.description = '".$result["data"][$i]["extension"]."' 
								and tttt_worldfonepbx.deleted = 0 and tttt_worldfonepbx_cstm.status_c = '1'";
								$resu = $db->query($que);
								$r =  $db->fetchByAssoc($resu);
								$history->userextension = $result["data"][$i]["extension"];
								$history->callstatus = $result["data"][$i]["status"];
								$history->customernumberfrom = $result["data"][$i]["from_number"];
								$history->customernumberto=$result["data"][$i]["to_number"];
							}else{
								$que = "select tttt_worldfonepbx.assigned_user_id from tttt_worldfonepbx inner join tttt_worldfonepbx_cstm on tttt_worldfonepbx.id = tttt_worldfonepbx_cstm.id_c where tttt_worldfonepbx.description = '".$result["data"][$i]["extension"]."' 
								and tttt_worldfonepbx.deleted = 0 and tttt_worldfonepbx_cstm.status_c = '1'";
								$resu = $db->query($que);
								$r =  $db->fetchByAssoc($resu);
								$history->userextension = $result["data"][$i]["extension"];
								$history->callstatus = $result["data"][$i]["status"];
								$history->customernumberfrom=$result["data"][$i]["from_number"];
								$history->customernumberto=$result["data"][$i]["to_number"];
							}
							$history->starttime = date('Y-m-d H:i:s', strtotime($timeStart.' - 7 hours'));
							$history->endtime = date('Y-m-d H:i:s', strtotime($timeEnd.' - 7 hours'));
							if(!empty($r)){
								$history->assigned_user_id=$r["assigned_user_id"];
							}
							$history->totalduration=$second;
							$history->file_ghiam = $result["data"][$i]["recording_url"];
							$history->save();
							
							if($result["data"][$i]["direction"] == "inbound"){
								$id = create_guid();
								$date = date('Y-m-d h:i:s');
								$query="select id from accounts  where phone_alternate='".$result["data"][$i]["from_number"]."' And accounts.deleted='0'";
								$res=$db->query($query);
								$row=$db->fetchByAssoc($res);
								if(!empty($row)){
									$q="insert into  accounts_tttt_history_calls_1_c  (id,date_modified,accounts_tttt_history_calls_1accounts_ida,accounts_tttt_history_calls_1tttt_history_calls_idb) values ('".$id."','".$date."','".$row["id"]."','".$history->id."')";
									$db->query($q);
								}else{
									$query="select id from leads where (phone_mobile='".$result["data"][$i]["from_number"]."' OR phone_work='".$result["data"][$i]["from_number"]."' OR phone_other='".$result["data"][$i]["from_number"]."') and leads.deleted='0' and status IN('New','In Process','Dead')";
									$res=$db->query($query);
									$row=$db->fetchByAssoc($res);
									if(!empty($row) ){
										$q="insert into  leads_tttt_history_calls_1_c  (id,date_modified,leads_tttt_history_calls_1leads_ida,leads_tttt_history_calls_1tttt_history_calls_idb) values ('".$id."','".$date."','".$row["id"]."','".$history->id."')";
										$db->query($q);
									}else{
										$query="select id from contacts where (phone_mobile='".$result["data"][$i]["from_number"]."' OR phone_work='".$result["data"][$i]["from_number"]."' OR phone_other='".$result["data"][$i]["from_number"]."') and contacts.deleted='0'";
										$res=$db->query($query);
										$row=$db->fetchByAssoc($res);
										if(!empty($row) ){
										$q="insert into  contacts_tttt_history_calls_1_c  (id,date_modified,contacts_tttt_history_calls_1contacts_ida,contacts_tttt_history_calls_1tttt_history_calls_idb) values ('".$id."','".$date."','".$row["id"]."','".$history->id."')";
										$db->query($q);
										}
									}
								}
							}else{
								$id = create_guid();
								$date = date('Y-m-d h:i:s');
								$query="select id from accounts  where phone_alternate='".$result["data"][$i]["to_number"]."' And accounts.deleted='0'";
								$res=$db->query($query);
								$row=$db->fetchByAssoc($res);
								if(!empty($row)){
									$q="insert into  accounts_tttt_history_calls_1_c  (id,date_modified,accounts_tttt_history_calls_1accounts_ida,accounts_tttt_history_calls_1tttt_history_calls_idb) values ('".$id."','".$date."','".$row["id"]."','".$history->id."')";
									$db->query($q);
								}else{
									$query="select id from leads where (phone_mobile='".$result["data"][$i]["to_number"]."' OR phone_work='".$result["data"][$i]["to_number"]."' OR phone_other='".$result["data"][$i]["to_number"]."') and leads.deleted='0' and status IN('New','In Process','Dead')";
									$res=$db->query($query);
									$row=$db->fetchByAssoc($res);
									if(!empty($row) ){
										$q="insert into  leads_tttt_history_calls_1_c  (id,date_modified,leads_tttt_history_calls_1leads_ida,leads_tttt_history_calls_1tttt_history_calls_idb) values ('".$id."','".$date."','".$row["id"]."','".$history->id."')";
										$db->query($q);
									}else{
										$query="select id from contacts where (phone_mobile='".$result["data"][$i]["to_number"]."' OR phone_work='".$result["data"][$i]["to_number"]."' OR phone_other='".$result["data"][$i]["to_number"]."') and contacts.deleted='0'";
										$res=$db->query($query);
										$row=$db->fetchByAssoc($res);
										if(!empty($row) ){
										$q="insert into  contacts_tttt_history_calls_1_c  (id,date_modified,contacts_tttt_history_calls_1contacts_ida,contacts_tttt_history_calls_1tttt_history_calls_idb) values ('".$id."','".$date."','".$row["id"]."','".$history->id."')";
										$db->query($q);
										}
									}
								}
								
								
							}
							if($i == 0){
								$fp = fopen('dateSysHistory_tel4vn.txt', 'w');
								fwrite($fp, date("Y-m-d H:i:s", strtotime(date($timeStart)."+ 1 second")));
								fclose($fp);
							}
						}
					}
				}else{
					$today = date("Y-m-d H:i:s");
					$startEnd = getContent_TEL4VN();
					$startEndSave =  date("Y-m-d H:i:s", strtotime(date($startEnd)."+ 2 hours"));
					if($today > $startEndSave){
						$fp = fopen('dateSysHistory_tel4vn.txt', 'w');
						fwrite($fp, date("Y-m-d H:i:s", strtotime(date($startEnd)."+ 2 hours")));
						fclose($fp);
					}
				}
			} catch (Exception $ex) {
				$GLOBALS['log']->error($ex->getMessage());
			}
			return true;
	}
    function callAPI_TEL4VN()
    {
		$startDate = getContent_TEL4VN();
		$token = getTokenContent_TEL4VN();
		$today = date("Y-m-d H:i:s");
		$startEndTest=date("Y-m-d H:i:s", strtotime(date($startDate)."+ 2 hours"));
		$response = array();
		if($today > $startEndTest){
			$startEnd = date("Y-m-d H:i:s", strtotime(date($startDate)."+ 2 hours"));
		$data = '
		{
			 "start_date":"'.$startDate.'",
			 "end_date" : "'.$startEnd.'"
		}
		';
			try {
				$startDate = str_replace(' ', '%20', $startDate);
				$startEnd = str_replace(' ', '%20', $startEnd);
				$apiUrl = GET_REVENUE_API_URL_TEL4VN;
				$apiUrl = $apiUrl."start_date=".$startDate."&end_date=".$startEnd;
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_HEADER, false);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_URL, $apiUrl);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$headers = [
				  'Authorization: Bearer '.$token
				];
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
				//curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
				$response = curl_exec($ch);
				if (!empty($response)) {
					$response = json_decode($response, true);
					
				}
			
			}catch (Exception $ex){
				$GLOBALS['log']->error($ex);
			
			}
		}else{
			$response["data"] = array();
		}
        return $response;
    }
	
	
	
      
	
