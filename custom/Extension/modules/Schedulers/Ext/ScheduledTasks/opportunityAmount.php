<?php

array_push($job_strings, 'opportunityAmount');

function opportunityAmount()
{
    global $db;
    
    try {
        $q = "SELECT id_c, lead_account_status_c FROM accounts_cstm";
        $result = $db->query($q);

        if ($result) {
            while ($row = $db->fetchByAssoc($result)) {
                $id_acc = $row['id_c'];
                $account_status = $row['lead_account_status_c'];
                $amount = 0;
                
                if ($account_status == 'Transaction') {
                    $q2 = "SELECT opportunity_id  FROM accounts_opportunities WHERE account_id ='".$id_acc."'";
                    $result2 = $db->query($q2);
                    
                    while ($row2 = $db->fetchByAssoc($result2)) {
                        $id_op = $row2['opportunity_id'];
                        $q3 = "SELECT amount FROM opportunities WHERE id = '".$id_op."' AND sales_stage = 'Closed Won' AND deleted = 0";

                        $result3 = $db->query($q3);
                        $row3 = $result3->fetch_assoc();
                        
                        if ($row3 !== null) {
                            $amount += $row3['amount']; // Tính tổng tất cả các amount
                        }
                    }
                } else {
                    $q2 = "SELECT opportunity_id  FROM accounts_opportunities WHERE account_id ='".$id_acc."'";
                    $result2 = $db->query($q2);
                    
                    while ($row2 = $db->fetchByAssoc($result2)) {
                        $id_op = $row2['opportunity_id'];
                        $q3 = "SELECT amount FROM opportunities WHERE id = '".$id_op."' AND sales_stage IN ('Negotiation/Review', 'Proposal/Price Quote', 'Prospecting') AND deleted = 0";

                        $result3 = $db->query($q3);
                        $row3 = $result3->fetch_assoc();
                        
                        if ($row3 !== null) {
                            $amount += $row3['amount']; // Tính tổng tất cả các amount
                        }
                    }
                }

                $q4 = "UPDATE accounts SET opportunityamount = '".$amount."' WHERE id = '".$id_acc."'";
                $result4 = $db->query($q4);
                
                if (!$result4) {
                    throw new Exception("Failed to update opportunity amount for account with ID: $id_acc");
                }
            }
        } else {
            throw new Exception("Failed to retrieve account data");
        }
        
        return true; // Trả về true nếu mọi thứ diễn ra thành công
    } catch (Exception $ex) {
        // Ghi log lỗi
        $GLOBALS['log']->error($ex->getMessage());
        return false; // Trả về false nếu có lỗi xảy ra
    }
}
