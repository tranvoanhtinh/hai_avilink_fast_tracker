<?php

//$job_strings[]='getRevenueFromErp';
array_push($job_strings, 'getInfo_OMICALL');
defined('GET_REVENUE_API_URL_OMICALL') or define('GET_REVENUE_API_URL_OMICALL', 'https://public-v1-stg.omicall.com/api/call_transaction/list?');
date_default_timezone_set('Asia/ho_chi_minh'); 
$year = getdate();
if($year["mday"] <= 9){
	if($year["mday"] < 9){
	$dayFrom = "0".($year["mday"]-1);
	$dayTo = "0".($year["mday"]+1);
	}else{
	$dayFrom = "0".($year["mday"]-1);
	$dayTo = $year["mday"]+1;
	}
}else{
	if($year["mday"] == 10){
		$dayFrom = "0".($year["mday"]-1);
		$dayTo = $year["mday"]+1;
	}else{
		$dayFrom = $year["mday"]-1;
		$dayTo = $year["mday"]+1;
	}
}

if($year["mon"] < 10){
	$mon = "0".$year["mon"];
}else{
	$mon = $year["mon"];
}
function getContent_OMICALL()
{
	return file_get_contents("dateSysHistory_omicall.txt");
}
function getTokenContent_OMICALL()
{
	return file_get_contents("dateSysToken_omicall.txt");
}
function getInfo_OMICALL()
    {
		try {
				global $db;
				$result = callAPI_OMICALL();
				if($result["payload"]["total_items"] > 0){
					for($i = 0;$i < $result["payload"]["total_items"];$i++){
						if(!empty($result["payload"]["items"])){
							$second=gmdate('H:i:s',$result["payload"]["items"][$i]["duration"]);
							$timeStart = date("Y-m-d H:i:s",$result["payload"]["items"][$i]["time_start_to_answer"]);
							$timeEnd = date("Y-m-d H:i:s",$result["payload"]["items"][$i]["time_start_to_answer"] + $result["payload"]["items"][$i]["duration"] + $result["payload"]["items"][$i]["answer_sec"]);
							$history= BeanFactory::newBean("tttt_History_calls");
							$history->direction=$result["payload"]["items"][$i]["direction"];
							if($result["payload"]["items"][$i]["direction"] == "inbound"){
								$que = "select tttt_worldfonepbx.assigned_user_id from tttt_worldfonepbx inner join tttt_worldfonepbx_cstm on tttt_worldfonepbx.id = tttt_worldfonepbx_cstm.id_c where tttt_worldfonepbx.description = '".$result["payload"]["items"][$i]["sip_user"]."' 
								and tttt_worldfonepbx.deleted = 0 and tttt_worldfonepbx_cstm.status_c = '1'";
								$resu = $db->query($que);
								$r =  $db->fetchByAssoc($resu);
								$a = "";
								$history->userextension = $result["payload"]["items"][$i]["sip_user"];
								$history->callstatus = $result["payload"]["items"][$i]["disposition"];
								$history->customernumberfrom = $result["payload"]["items"][$i]["source_number"];
								$history->customernumberto=$result["payload"]["items"][$i]["destination_number"];
							}else{
								$que = "select tttt_worldfonepbx.assigned_user_id from tttt_worldfonepbx inner join tttt_worldfonepbx_cstm on tttt_worldfonepbx.id = tttt_worldfonepbx_cstm.id_c where tttt_worldfonepbx.description = '".$result["payload"]["items"][$i]["sip_user"]."' 
								and tttt_worldfonepbx.deleted = 0 and tttt_worldfonepbx_cstm.status_c = '1'";
								$resu = $db->query($que);
								$r =  $db->fetchByAssoc($resu);
								$history->userextension = $result["payload"]["items"][$i]["sip_user"];
								$history->callstatus = $result["payload"]["items"][$i]["disposition"];
								$history->customernumberfrom=$result["payload"]["items"][$i]["source_number"];
								$history->customernumberto=$result["payload"]["items"][$i]["destination_number"];
							}
							$history->starttime = date('Y-m-d H:i:s', strtotime($timeStart.' - 7 hours'));
							$history->endtime = date('Y-m-d H:i:s', strtotime($timeEnd.' - 7 hours'));
							if(!empty($r)){
								$history->assigned_user_id=$r["assigned_user_id"];
							}
							$history->totalduration=$second;
							$history->file_ghiam = $result["payload"]["items"][$i]["recording_file"];
							$history->save();
							
							if($result["payload"]["items"][$i]["direction"] == "inbound"){
								$id = create_guid();
								$date = date('Y-m-d h:i:s');
								$query="select id from accounts  where phone_alternate='".$result["payload"]["items"][$i]["source_number"]."' And accounts.deleted='0'";
								$res=$db->query($query);
								$row=$db->fetchByAssoc($res);
								if(!empty($row)){
									$q="insert into  accounts_tttt_history_calls_1_c  (id,date_modified,accounts_tttt_history_calls_1accounts_ida,accounts_tttt_history_calls_1tttt_history_calls_idb) values ('".$id."','".$date."','".$row["id"]."','".$history->id."')";
									$db->query($q);
								}else{
									$query="select id from leads where (phone_mobile='".$result["payload"]["items"][$i]["source_number"]."' OR phone_work='".$result["payload"]["items"][$i]["source_number"]."' OR phone_other='".$result["payload"]["items"][$i]["source_number"]."') and leads.deleted='0' and status IN('New','In Process','Dead')";
									$res=$db->query($query);
									$row=$db->fetchByAssoc($res);
									if(!empty($row) ){
										$q="insert into  leads_tttt_history_calls_1_c  (id,date_modified,leads_tttt_history_calls_1leads_ida,leads_tttt_history_calls_1tttt_history_calls_idb) values ('".$id."','".$date."','".$row["id"]."','".$history->id."')";
										$db->query($q);
									}else{
										$query="select id from contacts where (phone_mobile='".$result["payload"]["items"][$i]["source_number"]."' OR phone_work='".$result["payload"]["items"][$i]["source_number"]."' OR phone_other='".$result["payload"]["items"][$i]["source_number"]."') and contacts.deleted='0'";
										$res=$db->query($query);
										$row=$db->fetchByAssoc($res);
										if(!empty($row) ){
										$q="insert into  contacts_tttt_history_calls_1_c  (id,date_modified,contacts_tttt_history_calls_1contacts_ida,contacts_tttt_history_calls_1tttt_history_calls_idb) values ('".$id."','".$date."','".$row["id"]."','".$history->id."')";
										$db->query($q);
										}
									}
								}
							}else{
								$id = create_guid();
								$date = date('Y-m-d h:i:s');
								$query="select id from accounts  where phone_alternate='".$result["payload"]["items"][$i]["destination_number"]."' And accounts.deleted='0'";
								$res=$db->query($query);
								$row=$db->fetchByAssoc($res);
								if(!empty($row)){
									$q="insert into  accounts_tttt_history_calls_1_c  (id,date_modified,accounts_tttt_history_calls_1accounts_ida,accounts_tttt_history_calls_1tttt_history_calls_idb) values ('".$id."','".$date."','".$row["id"]."','".$history->id."')";
									$db->query($q);
								}else{
									$query="select id from leads where (phone_mobile='".$result["payload"]["items"][$i]["destination_number"]."' OR phone_work='".$result["payload"]["items"][$i]["destination_number"]."' OR phone_other='".$result["payload"]["items"][$i]["destination_number"]."') and leads.deleted='0' and status IN('New','In Process','Dead')";
									$res=$db->query($query);
									$row=$db->fetchByAssoc($res);
									if(!empty($row) ){
										$q="insert into  leads_tttt_history_calls_1_c  (id,date_modified,leads_tttt_history_calls_1leads_ida,leads_tttt_history_calls_1tttt_history_calls_idb) values ('".$id."','".$date."','".$row["id"]."','".$history->id."')";
										$db->query($q);
									}else{
										$query="select id from contacts where (phone_mobile='".$result["payload"]["items"][$i]["destination_number"]."' OR phone_work='".$result["payload"]["items"][$i]["destination_number"]."' OR phone_other='".$result["payload"]["items"][$i]["destination_number"]."') and contacts.deleted='0'";
										$res=$db->query($query);
										$row=$db->fetchByAssoc($res);
										if(!empty($row) ){
										$q="insert into  contacts_tttt_history_calls_1_c  (id,date_modified,contacts_tttt_history_calls_1contacts_ida,contacts_tttt_history_calls_1tttt_history_calls_idb) values ('".$id."','".$date."','".$row["id"]."','".$history->id."')";
										$db->query($q);
										}
									}
								}
								
								
							}
							if($i == 0){
								$fp = fopen('dateSysHistory_omicall.txt', 'w');
								fwrite($fp, date("Y-m-d H:i:s", (int)($result["payload"]["items"][$i]["created_date"]/1000)."+ 1 second"));
								fclose($fp);
							}
						}
					}
				}else{
					$today = date("Y-m-d H:i:s");
					$startEnd = getContent_OMICALL();
					$startEndSave =  date("Y-m-d H:i:s", strtotime(date($startEnd)."+ 2 minutes"));
					if($today > $startEndSave){
						$fp = fopen('dateSysHistory_omicall.txt', 'w');
						fwrite($fp, date("Y-m-d H:i:s", strtotime(date($startEnd)."+ 2 minutes")));
						fclose($fp);
					}
				}
			} catch (Exception $ex) {
				$GLOBALS['log']->error($ex->getMessage());
			}
			return true;
	}
    function callAPI_OMICALL()
    {
		$startDate = getContent_OMICALL();
		$token = getTokenContent_OMICALL();
		$today = date("Y-m-d H:i:s");
		$startEndTest=date("Y-m-d H:i:s", strtotime(date($startDate)."+ 2 minutes"));
		$response = array();
		if($today > $startEndTest){
			$startEnd = strtotime(date($startDate)."+ 2 minutes") * 1000;
			$startDate = strtotime(date($startDate)) * 1000;
			try {
				$apiUrl = GET_REVENUE_API_URL_OMICALL;
				$apiUrl = $apiUrl."from_date=".$startDate."&to_date=".$startEnd;
				$curl = curl_init();
				curl_setopt_array($curl, array(
					CURLOPT_URL => $apiUrl,
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => '',
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 0,
				  CURLOPT_FOLLOWLOCATION => true,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => 'GET',
				  CURLOPT_HTTPHEADER => array(
					'Content-type: application/json',
					'Authorization: Bearer '.$token
				  ),
				));

				$response = curl_exec($curl);
				curl_close($curl);
				if (!empty($response)){
					$response = json_decode($response, true);
				}
			
			}catch (Exception $ex){
				$GLOBALS['log']->error($ex);
			
			}
		}else{
			$response["payload"]["items"] = array();
		}
        return $response;
    }
	
      
	
