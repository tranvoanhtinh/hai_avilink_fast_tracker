<?php
 // created: 2024-06-04 08:47:47
$layout_defs["Contacts"]["subpanel_setup"]['contacts_aos_bookings_1'] = array (
  'order' => 100,
  'module' => 'AOS_Bookings',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CONTACTS_AOS_BOOKINGS_1_FROM_AOS_BOOKINGS_TITLE',
  'get_subpanel_data' => 'contacts_aos_bookings_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
