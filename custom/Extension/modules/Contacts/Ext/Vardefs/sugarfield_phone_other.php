<?php
 // created: 2019-08-15 12:21:19
$dictionary['Contact']['fields']['phone_other']['len']='10';
$dictionary['Contact']['fields']['phone_other']['audited']=true;
$dictionary['Contact']['fields']['phone_other']['inline_edit']='';
$dictionary['Contact']['fields']['phone_other']['comments']='Other phone number for the contact';
$dictionary['Contact']['fields']['phone_other']['merge_filter']='disabled';

 ?>