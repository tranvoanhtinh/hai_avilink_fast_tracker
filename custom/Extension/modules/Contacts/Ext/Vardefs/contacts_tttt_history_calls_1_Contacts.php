<?php
// created: 2020-12-09 05:55:34
$dictionary["Contact"]["fields"]["contacts_tttt_history_calls_1"] = array (
  'name' => 'contacts_tttt_history_calls_1',
  'type' => 'link',
  'relationship' => 'contacts_tttt_history_calls_1',
  'source' => 'non-db',
  'module' => 'tttt_History_calls',
  'bean_name' => 'tttt_History_calls',
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_TTTT_HISTORY_CALLS_1_FROM_TTTT_HISTORY_CALLS_TITLE',
);
