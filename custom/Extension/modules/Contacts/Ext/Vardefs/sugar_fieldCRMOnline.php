<?php 

$dictionary["Contact"]["fields"]["sex"] = array(
    "name" => "sex",
    "vname" => "LBL_SEX",
    "type" => "enum",
    "options" => "sex_list",
    "default" => "",
);

$dictionary["Contact"]["fields"]["passport"] = array(
    'name' => 'passport',
    'vname' => 'LBL_PASSPORT',
    'type' => 'varchar',
    "len" => 255,
    "importable" => true,
    "audited" => true,
    "massupdate" => false,
);


$dictionary["Contact"]["fields"]["nationality"] = array(
    'name' => 'nationality',
    'vname' => 'LBL_NATIONALITY',
    'len' => '100',
    'type' => 'enum',
    'audited' => true,
    'inline_edit' => true,
    'massupdate' => '1',
    'options' => 'nationality_list',
    'merge_filter' => 'disabled',
    'studio' => true,

);

