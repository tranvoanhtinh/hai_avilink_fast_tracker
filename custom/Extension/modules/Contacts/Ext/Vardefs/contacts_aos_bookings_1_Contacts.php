<?php
// created: 2024-06-04 08:47:51
$dictionary["Contact"]["fields"]["contacts_aos_bookings_1"] = array (
  'name' => 'contacts_aos_bookings_1',
  'type' => 'link',
  'relationship' => 'contacts_aos_bookings_1',
  'source' => 'non-db',
  'module' => 'AOS_Bookings',
  'bean_name' => 'AOS_Bookings',
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_AOS_BOOKINGS_1_FROM_AOS_BOOKINGS_TITLE',
);
