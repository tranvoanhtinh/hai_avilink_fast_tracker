<?php
 // created: 2020-05-20 12:35:28
$dictionary['Contact']['fields']['birthdate']['inline_edit']=true;
$dictionary['Contact']['fields']['birthdate']['options']='date_range_search_dom';
$dictionary['Contact']['fields']['birthdate']['comments']='The birthdate of the contact';
$dictionary['Contact']['fields']['birthdate']['merge_filter']='disabled';
$dictionary['Contact']['fields']['birthdate']['enable_range_search']='1';

 ?>