<?php
// created: 2024-05-25 04:32:43
$dictionary["Contact"]["fields"]["contacts_spa_actionservice_1"] = array (
  'name' => 'contacts_spa_actionservice_1',
  'type' => 'link',
  'relationship' => 'contacts_spa_actionservice_1',
  'source' => 'non-db',
  'module' => 'spa_ActionService',
  'bean_name' => 'spa_ActionService',
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_SPA_ACTIONSERVICE_1_FROM_SPA_ACTIONSERVICE_TITLE',
);
