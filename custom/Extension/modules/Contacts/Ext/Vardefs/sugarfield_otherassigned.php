<?php
$dictionary["Contact"]["fields"]["otherassigned"] = array(
    'name' => 'otherassigned',
    'vname' => 'LBL_OTHER_ASSIGNED',
    'type' => 'varchar',
    "len" => 255,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);