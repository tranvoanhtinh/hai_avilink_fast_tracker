<?php
// created: 2018-12-28 09:39:18
$dictionary["Contact"]["fields"]["tttt_history_calls_contacts"] = array (
  'name' => 'tttt_history_calls_contacts',
  'type' => 'link',
  'relationship' => 'tttt_history_calls_contacts',
  'source' => 'non-db',
  'module' => 'tttt_History_calls',
  'bean_name' => 'tttt_History_calls',
  'vname' => 'LBL_TTTT_HISTORY_CALLS_CONTACTS_FROM_TTTT_HISTORY_CALLS_TITLE',
);
