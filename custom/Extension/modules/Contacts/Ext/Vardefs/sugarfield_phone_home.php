<?php
 // created: 2019-08-15 12:20:53
$dictionary['Contact']['fields']['phone_home']['len']='10';
$dictionary['Contact']['fields']['phone_home']['audited']=true;
$dictionary['Contact']['fields']['phone_home']['inline_edit']='';
$dictionary['Contact']['fields']['phone_home']['comments']='Home phone number of the contact';
$dictionary['Contact']['fields']['phone_home']['merge_filter']='disabled';

 ?>