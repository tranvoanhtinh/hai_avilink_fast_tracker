<?php
 // created: 2019-08-15 12:21:06
$dictionary['Contact']['fields']['phone_mobile']['len']='10';
$dictionary['Contact']['fields']['phone_mobile']['audited']=true;
$dictionary['Contact']['fields']['phone_mobile']['inline_edit']='';
$dictionary['Contact']['fields']['phone_mobile']['comments']='Mobile phone number of the contact';
$dictionary['Contact']['fields']['phone_mobile']['merge_filter']='disabled';

 ?>