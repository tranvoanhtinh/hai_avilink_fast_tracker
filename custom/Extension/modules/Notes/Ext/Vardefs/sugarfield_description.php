<?php
 // created: 2020-05-05 07:06:29
$dictionary['Note']['fields']['description']['inline_edit']=true;
$dictionary['Note']['fields']['description']['comments']='Full text of the note';
$dictionary['Note']['fields']['description']['merge_filter']='disabled';
$dictionary['Note']['fields']['description']['rows']='5';
$dictionary['Note']['fields']['description']['audited']=true;

 ?>