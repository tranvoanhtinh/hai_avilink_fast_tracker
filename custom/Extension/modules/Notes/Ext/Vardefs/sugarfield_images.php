<?php
 $dictionary["Note"]["fields"]["image"] = array(

            'name' => 'image',
            'vname' => 'LBL_IMAGE',
            'type' => 'image',
            'massupdate' => false,
            'comments' => '',
            'help' => '',
            'importable' => false,
            'reportable' => true,
            'len' => 255,
            'dbType' => 'varchar',
            'width' => '160',
            'height' => '160',
            'studio' => array('listview' => true),
  
);
