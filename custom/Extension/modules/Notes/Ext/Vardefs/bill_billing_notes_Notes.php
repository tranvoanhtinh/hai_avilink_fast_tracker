<?php
// created: 2021-08-18 06:32:45
$dictionary["Note"]["fields"]["bill_billing_notes"] = array (
  'name' => 'bill_billing_notes',
  'type' => 'link',
  'relationship' => 'bill_billing_notes',
  'source' => 'non-db',
  'module' => 'bill_Billing',
  'bean_name' => 'bill_Billing',
  'vname' => 'LBL_BILL_BILLING_NOTES_FROM_BILL_BILLING_TITLE',
  'id_name' => 'bill_billing_notesbill_billing_ida',
);
$dictionary["Note"]["fields"]["bill_billing_notes_name"] = array (
  'name' => 'bill_billing_notes_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_BILL_BILLING_NOTES_FROM_BILL_BILLING_TITLE',
  'save' => true,
  'id_name' => 'bill_billing_notesbill_billing_ida',
  'link' => 'bill_billing_notes',
  'table' => 'bill_billing',
  'module' => 'bill_Billing',
  'rname' => 'name',
);
$dictionary["Note"]["fields"]["bill_billing_notesbill_billing_ida"] = array (
  'name' => 'bill_billing_notesbill_billing_ida',
  'type' => 'link',
  'relationship' => 'bill_billing_notes',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_BILL_BILLING_NOTES_FROM_NOTES_TITLE',
);
