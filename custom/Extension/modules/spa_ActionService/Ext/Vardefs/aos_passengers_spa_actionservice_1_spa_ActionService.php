<?php
// created: 2024-08-08 04:06:36
$dictionary["spa_ActionService"]["fields"]["aos_passengers_spa_actionservice_1"] = array (
  'name' => 'aos_passengers_spa_actionservice_1',
  'type' => 'link',
  'relationship' => 'aos_passengers_spa_actionservice_1',
  'source' => 'non-db',
  'module' => 'AOS_Passengers',
  'bean_name' => 'AOS_Passengers',
  'vname' => 'LBL_AOS_PASSENGERS_SPA_ACTIONSERVICE_1_FROM_AOS_PASSENGERS_TITLE',
  'id_name' => 'aos_passengers_spa_actionservice_1aos_passengers_ida',
);
$dictionary["spa_ActionService"]["fields"]["aos_passengers_spa_actionservice_1_name"] = array (
  'name' => 'aos_passengers_spa_actionservice_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_AOS_PASSENGERS_SPA_ACTIONSERVICE_1_FROM_AOS_PASSENGERS_TITLE',
  'save' => true,
  'id_name' => 'aos_passengers_spa_actionservice_1aos_passengers_ida',
  'link' => 'aos_passengers_spa_actionservice_1',
  'table' => 'aos_passengers',
  'module' => 'AOS_Passengers',
  'rname' => 'name',
);
$dictionary["spa_ActionService"]["fields"]["aos_passengers_spa_actionservice_1aos_passengers_ida"] = array (
  'name' => 'aos_passengers_spa_actionservice_1aos_passengers_ida',
  'type' => 'link',
  'relationship' => 'aos_passengers_spa_actionservice_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_AOS_PASSENGERS_SPA_ACTIONSERVICE_1_FROM_SPA_ACTIONSERVICE_TITLE',
);
