<?php
// created: 2024-05-28 11:28:30
$dictionary["spa_ActionService"]["fields"]["aos_bookings_spa_actionservice_1"] = array (
  'name' => 'aos_bookings_spa_actionservice_1',
  'type' => 'link',
  'relationship' => 'aos_bookings_spa_actionservice_1',
  'source' => 'non-db',
  'module' => 'AOS_Bookings',
  'bean_name' => 'AOS_Bookings',
  'vname' => 'LBL_AOS_BOOKINGS_SPA_ACTIONSERVICE_1_FROM_AOS_BOOKINGS_TITLE',
  'id_name' => 'aos_bookings_spa_actionservice_1aos_bookings_ida',
);
$dictionary["spa_ActionService"]["fields"]["aos_bookings_spa_actionservice_1_name"] = array (
  'name' => 'aos_bookings_spa_actionservice_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_AOS_BOOKINGS_SPA_ACTIONSERVICE_1_FROM_AOS_BOOKINGS_TITLE',
  'save' => true,
  'id_name' => 'aos_bookings_spa_actionservice_1aos_bookings_ida',
  'link' => 'aos_bookings_spa_actionservice_1',
  'table' => 'aos_bookings',
  'module' => 'AOS_Bookings',
  'rname' => 'name',
);
$dictionary["spa_ActionService"]["fields"]["aos_bookings_spa_actionservice_1aos_bookings_ida"] = array (
  'name' => 'aos_bookings_spa_actionservice_1aos_bookings_ida',
  'type' => 'link',
  'relationship' => 'aos_bookings_spa_actionservice_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_AOS_BOOKINGS_SPA_ACTIONSERVICE_1_FROM_SPA_ACTIONSERVICE_TITLE',
);
