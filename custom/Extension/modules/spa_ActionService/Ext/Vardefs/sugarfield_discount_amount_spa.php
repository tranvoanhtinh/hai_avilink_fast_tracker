<?php

$dictionary["spa_ActionService"]["fields"]["discount_amount_spa"] = array(
    'name' => 'discount_amount_spa',
    'vname' => 'LBL_DISCOUNT_AMOUNT_SPA',
    'len' => '100',
    'type' => 'enum',
    'audited' => true,
    'inline_edit' => true,
    'massupdate' => '1',
    'options' => 'discount_percent_list',
    'merge_filter' => 'disabled',
);