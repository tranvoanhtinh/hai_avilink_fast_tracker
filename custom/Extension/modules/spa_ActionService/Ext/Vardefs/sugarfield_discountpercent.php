<?php
 // created: 2021-09-15 09:24:17


$dictionary["spa_ActionService"]["fields"]["discountpercent"] = array(
   'required' => false,
    'name' => 'discountpercent',
    'vname' => 'LBL_DISCOUNT_PERCENT',
    'type' => 'decimal',
    'massupdate' => true,
	'len' => '18',
    'size' => '20',
    'enable_range_search' => false,
    'precision' => '0',
);




 ?>