<?php
 // created: 2021-09-15 09:24:17
$dictionary['spa_ActionService']['fields']['amountservice']['name']='amountservice';
$dictionary['spa_ActionService']['fields']['amountservice']['vname']='LBL_AMOUNT_SERVICE';
$dictionary['spa_ActionService']['fields']['amountservice']['type']='currency';
$dictionary['spa_ActionService']['fields']['amountservice']['dbType']='double';
$dictionary['spa_ActionService']['fields']['amountservice']['duplicate_merge']='enabled';
$dictionary['spa_ActionService']['fields']['amountservice']['required']=false;
$dictionary['spa_ActionService']['fields']['amountservice']['options']='numeric_range_search_dom';
$dictionary['spa_ActionService']['fields']['amountservice']['enable_range_search']=true;
$dictionary['spa_ActionService']['fields']['amountservice']['inline_edit']='';
$dictionary['spa_ActionService']['fields']['amountservice']['duplicate_merge_dom_value']='1';
$dictionary['spa_ActionService']['fields']['amountservice']['merge_filter']='disabled';
$dictionary['spa_ActionService']['fields']['amountservice']['audited']=true;
$dictionary['spa_ActionService']['fields']['amountservice']['default']='0';





 ?>