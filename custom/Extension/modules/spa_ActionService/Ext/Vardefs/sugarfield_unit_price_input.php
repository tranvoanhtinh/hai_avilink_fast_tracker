<?php

$dictionary["spa_ActionService"]["fields"]["unit_price_input"] =
    array(
        'required' => false,
        'name' => 'unit_price_input',
        'vname' => 'LBL_UNIT_PRICE_INPUT',
        'type' => 'currency',
        'massupdate' => 0,
        'comments' => '',
        'help' => '',
        'importable' => 'true',
        'duplicate_merge' => 'disabled',
        'duplicate_merge_dom_value' => '0',
        'audited' => false,
        'reportable' => true,
        'len' => '26,6',
        'enable_range_search' => true,
        'options' => 'numeric_range_search_dom',
    );
