<?php
 // created: 2024-04-27 12:28:19
$dictionary['spa_ActionService']['fields']['name']['required']=false;
$dictionary['spa_ActionService']['fields']['name']['inline_edit']=true;
$dictionary['spa_ActionService']['fields']['name']['duplicate_merge']='disabled';
$dictionary['spa_ActionService']['fields']['name']['duplicate_merge_dom_value']='0';
$dictionary['spa_ActionService']['fields']['name']['merge_filter']='disabled';
$dictionary['spa_ActionService']['fields']['name']['unified_search']=false;

 ?>