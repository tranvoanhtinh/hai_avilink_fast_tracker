<?php
// created: 2023-05-27 06:35:10
$dictionary["spa_ActionService"]["fields"]["spa_actionservice_aos_invoices"] = array (
  'name' => 'spa_actionservice_aos_invoices',
  'type' => 'link',
  'relationship' => 'spa_actionservice_aos_invoices',
  'source' => 'non-db',
  'module' => 'AOS_Invoices',
  'bean_name' => 'AOS_Invoices',
  'vname' => 'LBL_SPA_ACTIONSERVICE_AOS_INVOICES_FROM_AOS_INVOICES_TITLE',
  'id_name' => 'spa_actionservice_aos_invoicesaos_invoices_ida',
);
$dictionary["spa_ActionService"]["fields"]["spa_actionservice_aos_invoices_name"] = array (
  'name' => 'spa_actionservice_aos_invoices_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SPA_ACTIONSERVICE_AOS_INVOICES_FROM_AOS_INVOICES_TITLE',
  'save' => true,
  'id_name' => 'spa_actionservice_aos_invoicesaos_invoices_ida',
  'link' => 'spa_actionservice_aos_invoices',
  'table' => 'aos_invoices',
  'module' => 'AOS_Invoices',
  'rname' => 'name',
);
$dictionary["spa_ActionService"]["fields"]["spa_actionservice_aos_invoicesaos_invoices_ida"] = array (
  'name' => 'spa_actionservice_aos_invoicesaos_invoices_ida',
  'type' => 'link',
  'relationship' => 'spa_actionservice_aos_invoices',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_SPA_ACTIONSERVICE_AOS_INVOICES_FROM_SPA_ACTIONSERVICE_TITLE',
);
