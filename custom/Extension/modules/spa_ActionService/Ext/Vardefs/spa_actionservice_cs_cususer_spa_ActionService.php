<?php
// created: 2023-05-27 06:35:10
$dictionary["spa_ActionService"]["fields"]["spa_actionservice_cs_cususer"] = array (
  'name' => 'spa_actionservice_cs_cususer',
  'type' => 'link',
  'relationship' => 'spa_actionservice_cs_cususer',
  'source' => 'non-db',
  'module' => 'cs_cusUser',
  'bean_name' => 'cs_cusUser',
  'vname' => 'LBL_SPA_ACTIONSERVICE_CS_CUSUSER_FROM_CS_CUSUSER_TITLE',
  'id_name' => 'spa_actionservice_cs_cususercs_cususer_ida',
);
$dictionary["spa_ActionService"]["fields"]["spa_actionservice_cs_cususer_name"] = array (
  'name' => 'spa_actionservice_cs_cususer_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SPA_ACTIONSERVICE_CS_CUSUSER_FROM_CS_CUSUSER_TITLE',
  'save' => true,
  'id_name' => 'spa_actionservice_cs_cususercs_cususer_ida',
  'link' => 'spa_actionservice_cs_cususer',
  'table' => 'cs_cususer',
  'module' => 'cs_cusUser',
  'rname' => 'name',
);
$dictionary["spa_ActionService"]["fields"]["spa_actionservice_cs_cususercs_cususer_ida"] = array (
  'name' => 'spa_actionservice_cs_cususercs_cususer_ida',
  'type' => 'link',
  'relationship' => 'spa_actionservice_cs_cususer',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_SPA_ACTIONSERVICE_CS_CUSUSER_FROM_SPA_ACTIONSERVICE_TITLE',
);
