<?php
// created: 2024-05-25 04:32:43
$dictionary["spa_ActionService"]["fields"]["contacts_spa_actionservice_1"] = array (
  'name' => 'contacts_spa_actionservice_1',
  'type' => 'link',
  'relationship' => 'contacts_spa_actionservice_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_CONTACTS_SPA_ACTIONSERVICE_1_FROM_CONTACTS_TITLE',
  'id_name' => 'contacts_spa_actionservice_1contacts_ida',
);
$dictionary["spa_ActionService"]["fields"]["contacts_spa_actionservice_1_name"] = array (
  'name' => 'contacts_spa_actionservice_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_SPA_ACTIONSERVICE_1_FROM_CONTACTS_TITLE',
  'save' => true,
  'id_name' => 'contacts_spa_actionservice_1contacts_ida',
  'link' => 'contacts_spa_actionservice_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["spa_ActionService"]["fields"]["contacts_spa_actionservice_1contacts_ida"] = array (
  'name' => 'contacts_spa_actionservice_1contacts_ida',
  'type' => 'link',
  'relationship' => 'contacts_spa_actionservice_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_SPA_ACTIONSERVICE_1_FROM_SPA_ACTIONSERVICE_TITLE',
);
