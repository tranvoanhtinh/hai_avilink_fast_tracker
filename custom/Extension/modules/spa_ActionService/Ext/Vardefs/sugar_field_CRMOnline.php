<?php
$dictionary["spa_ActionService"]["fields"]["booking_date"] = array(
    'name' => 'booking_date',
    'vname' => 'LBL_BOOKING_DATE',
    'type' => 'date',
    'audited' => true,
    'required' => false,
    "massupdate" => false,
);

$dictionary["spa_ActionService"]["fields"]["date_std_std"] = array(
    'name' => 'date_std_std',
    'vname' => 'LBL_DATE_STD_STA',
    'type' => 'datetimecombo',
    'massupdate' => '0',
    'default' => '',
    'no_default' => false,
    'comments' => '',
    'help' => '',
    'importable' => 'true',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => false,
    'reportable' => true,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'size' => '20',
    'enable_range_search' => false,
    'dbType' => 'datetime',
    'display_default' => 'now&12:00am',
);


$dictionary["spa_ActionService"]["fields"]["airport_from"] = array(
    'name' => 'airport_from',
    'vname' => 'LBL_AIRPORT_FROM',
    'len' => '100',
    'type' => 'enum',
    'audited' => true,
    'inline_edit' => true,
    'massupdate' => '1',
    'options' => 'airport_list',
    'merge_filter' => 'disabled',
    'studio' => true,

);
$dictionary["spa_ActionService"]["fields"]["airport_to"] = array(
    'name' => 'airport_to',
    'vname' => 'LBL_AIRPORT_TO',
    'len' => '100',
    'type' => 'enum',
    'audited' => true,
    'inline_edit' => true,
    'massupdate' => '1',
    'options' => 'airport_list',
    'merge_filter' => 'disabled',
    'studio' => true,

);

$dictionary["spa_ActionService"]["fields"]["flight_name"] = array(
    'name' => 'flight_name',
    'vname' => 'LBL_FLIGHT_NAME',
    'type' => 'varchar',
    "len" => 255,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);

$dictionary["spa_ActionService"]["fields"]["type_service"] = array(
    'name' => 'type_service',
    'vname' => 'LBL_TYPE_SERVICE',
    'len' => '100',
    'type' => 'enum',
    'audited' => true,
    'inline_edit' => true,
    'massupdate' => '1',
    'options' => 'type_service_list',
    'merge_filter' => 'disabled',
    'studio' => true,
    "required" => true,

);



$dictionary["spa_ActionService"]["fields"]["vnd"] = array(
    'name' => 'vnd',
    'vname' => 'LBL_VND',
    'type' => 'currency',
    'massupdate' => 0,
    'comments' => '',
    'help' => '',
    'importable' => 'true',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => 1,
    'reportable' => true,
    'len' => '26,6',
);

$dictionary["spa_ActionService"]["fields"]["usd"] = array(
    'name' => 'usd',
    'vname' => 'LBL_USD',
    'type' => 'currency',
    'massupdate' => 0,
    'comments' => '',
    'help' => '',
    'importable' => 'true',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => 1,
    'reportable' => true,
    'len' => '26,6',
);

$dictionary["spa_ActionService"]["fields"]["vnd_total"] = array(
    'name' => 'vnd_total',
    'vname' => 'LBL_VND_TOTAL',
    'type' => 'currency',
    'massupdate' => 0,
    'comments' => '',
    'help' => '',
    'importable' => 'true',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => 1,
    'reportable' => true,
    'len' => '26,6',
);

$dictionary["spa_ActionService"]["fields"]["usd_total"] = array(
    'name' => 'usd_total',
    'vname' => 'LBL_USD_TOTAL',
    'type' => 'currency',
    'massupdate' => 0,
    'comments' => '',
    'help' => '',
    'importable' => 'true',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => 1,
    'reportable' => true,
    'len' => '26,6',
);

$dictionary["spa_ActionService"]["fields"]["exchange_rate"] = array(
    'name' => 'exchange_rate',
    'vname' => 'LBL_EXCHANGE_RATE',
    'type' => 'currency',
    'massupdate' => 0,
    'comments' => '',
    'help' => '',
    'importable' => 'true',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => 1,
    'reportable' => true,
    'len' => '26,6',
);


$dictionary["spa_ActionService"]["fields"]["type_currency"] = array(
    'name' => 'type_currency',
    'vname' => 'LBL_TYPE_CURRENCY',
    'len' => '100',
    'type' => 'enum',
    'audited' => true,
    'inline_edit' => true,
    'massupdate' => '1',
    'options' => 'type_currency_list',
    'merge_filter' => 'disabled',
    'studio' => true,
    "required" => true,
);

$dictionary["spa_ActionService"]["fields"]["vnd"] = array(
    'name' => 'vnd',
    'vname' => 'LBL_VND',
    'type' => 'currency',
    'massupdate' => 0,
    'comments' => '',
    'help' => '',
    'importable' => 'true',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => 1,
    'reportable' => true,
    'len' => '26,6',
);

$dictionary["spa_ActionService"]["fields"]["usd"] = array(
    'name' => 'usd',
    'vname' => 'LBL_USD',
    'type' => 'currency',
    'massupdate' => 0,
    'comments' => '',
    'help' => '',
    'importable' => 'true',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => 1,
    'reportable' => true,
    'len' => '26,6',
);

$dictionary["spa_ActionService"]["fields"]["product_id"] = array(
    'name' => 'product_id',
    'vname' => 'LBL_PRODUCT_ID',
    'type' => 'varchar',
    "len" => 50,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);