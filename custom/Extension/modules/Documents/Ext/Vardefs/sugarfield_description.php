<?php
 // created: 2022-05-23 04:31:33
$dictionary['Document']['fields']['description']['inline_edit']=true;
$dictionary['Document']['fields']['description']['comments']='Full text of the note';
$dictionary['Document']['fields']['description']['merge_filter']='disabled';
$dictionary['Document']['fields']['description']['rows']='1';
$dictionary['Document']['fields']['description']['cols']='80';

 ?>