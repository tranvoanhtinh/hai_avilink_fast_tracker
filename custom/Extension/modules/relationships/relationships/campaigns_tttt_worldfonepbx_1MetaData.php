<?php
// created: 2022-12-02 10:33:32
$dictionary["campaigns_tttt_worldfonepbx_1"] = array (
  'true_relationship_type' => 'one-to-one',
  'from_studio' => true,
  'relationships' => 
  array (
    'campaigns_tttt_worldfonepbx_1' => 
    array (
      'lhs_module' => 'Campaigns',
      'lhs_table' => 'campaigns',
      'lhs_key' => 'id',
      'rhs_module' => 'tttt_WorldfonePBX',
      'rhs_table' => 'tttt_worldfonepbx',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'campaigns_tttt_worldfonepbx_1_c',
      'join_key_lhs' => 'campaigns_tttt_worldfonepbx_1campaigns_ida',
      'join_key_rhs' => 'campaigns_tttt_worldfonepbx_1tttt_worldfonepbx_idb',
    ),
  ),
  'table' => 'campaigns_tttt_worldfonepbx_1_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'campaigns_tttt_worldfonepbx_1campaigns_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'campaigns_tttt_worldfonepbx_1tttt_worldfonepbx_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'campaigns_tttt_worldfonepbx_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'campaigns_tttt_worldfonepbx_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'campaigns_tttt_worldfonepbx_1campaigns_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'campaigns_tttt_worldfonepbx_1_idb2',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'campaigns_tttt_worldfonepbx_1tttt_worldfonepbx_idb',
      ),
    ),
  ),
);