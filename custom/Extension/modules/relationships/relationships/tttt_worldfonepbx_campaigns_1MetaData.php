<?php
// created: 2022-12-02 10:31:24
$dictionary["tttt_worldfonepbx_campaigns_1"] = array (
  'true_relationship_type' => 'one-to-one',
  'from_studio' => true,
  'relationships' => 
  array (
    'tttt_worldfonepbx_campaigns_1' => 
    array (
      'lhs_module' => 'tttt_WorldfonePBX',
      'lhs_table' => 'tttt_worldfonepbx',
      'lhs_key' => 'id',
      'rhs_module' => 'Campaigns',
      'rhs_table' => 'campaigns',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'tttt_worldfonepbx_campaigns_1_c',
      'join_key_lhs' => 'tttt_worldfonepbx_campaigns_1tttt_worldfonepbx_ida',
      'join_key_rhs' => 'tttt_worldfonepbx_campaigns_1campaigns_idb',
    ),
  ),
  'table' => 'tttt_worldfonepbx_campaigns_1_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'tttt_worldfonepbx_campaigns_1tttt_worldfonepbx_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'tttt_worldfonepbx_campaigns_1campaigns_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'tttt_worldfonepbx_campaigns_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'tttt_worldfonepbx_campaigns_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'tttt_worldfonepbx_campaigns_1tttt_worldfonepbx_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'tttt_worldfonepbx_campaigns_1_idb2',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'tttt_worldfonepbx_campaigns_1campaigns_idb',
      ),
    ),
  ),
);