<?php
// created: 2024-01-17 10:25:15
$dictionary["AOS_Products_Pos"]["fields"]["aos_products_aos_products_pos_1"] = array (
  'name' => 'aos_products_aos_products_pos_1',
  'type' => 'link',
  'relationship' => 'aos_products_aos_products_pos_1',
  'source' => 'non-db',
  'module' => 'AOS_Products',
  'bean_name' => 'AOS_Products',
  'vname' => 'LBL_AOS_PRODUCTS_AOS_PRODUCTS_POS_1_FROM_AOS_PRODUCTS_TITLE',
  'id_name' => 'aos_products_aos_products_pos_1aos_products_ida',
);
$dictionary["AOS_Products_Pos"]["fields"]["aos_products_aos_products_pos_1_name"] = array (
  'name' => 'aos_products_aos_products_pos_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_AOS_PRODUCTS_AOS_PRODUCTS_POS_1_FROM_AOS_PRODUCTS_TITLE',
  'save' => true,
  'id_name' => 'aos_products_aos_products_pos_1aos_products_ida',
  'link' => 'aos_products_aos_products_pos_1',
  'table' => 'aos_products',
  'module' => 'AOS_Products',
  'rname' => 'name',
);
$dictionary["AOS_Products_Pos"]["fields"]["aos_products_aos_products_pos_1aos_products_ida"] = array (
  'name' => 'aos_products_aos_products_pos_1aos_products_ida',
  'type' => 'link',
  'relationship' => 'aos_products_aos_products_pos_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_AOS_PRODUCTS_AOS_PRODUCTS_POS_1_FROM_AOS_PRODUCTS_POS_TITLE',
);
