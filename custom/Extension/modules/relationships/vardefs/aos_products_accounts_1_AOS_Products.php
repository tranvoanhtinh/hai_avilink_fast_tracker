<?php
// created: 2021-05-07 10:03:50
$dictionary["AOS_Products"]["fields"]["aos_products_accounts_1"] = array (
  'name' => 'aos_products_accounts_1',
  'type' => 'link',
  'relationship' => 'aos_products_accounts_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_AOS_PRODUCTS_ACCOUNTS_1_FROM_ACCOUNTS_TITLE',
);
