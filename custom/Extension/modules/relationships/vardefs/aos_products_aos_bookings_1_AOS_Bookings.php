<?php
// created: 2024-05-23 06:25:43
$dictionary["AOS_Bookings"]["fields"]["aos_products_aos_bookings_1"] = array (
  'name' => 'aos_products_aos_bookings_1',
  'type' => 'link',
  'relationship' => 'aos_products_aos_bookings_1',
  'source' => 'non-db',
  'module' => 'AOS_Products',
  'bean_name' => 'AOS_Products',
  'vname' => 'LBL_AOS_PRODUCTS_AOS_BOOKINGS_1_FROM_AOS_PRODUCTS_TITLE',
  'id_name' => 'aos_products_aos_bookings_1aos_products_ida',
);
$dictionary["AOS_Bookings"]["fields"]["aos_products_aos_bookings_1_name"] = array (
  'name' => 'aos_products_aos_bookings_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_AOS_PRODUCTS_AOS_BOOKINGS_1_FROM_AOS_PRODUCTS_TITLE',
  'save' => true,
  'id_name' => 'aos_products_aos_bookings_1aos_products_ida',
  'link' => 'aos_products_aos_bookings_1',
  'table' => 'aos_products',
  'module' => 'AOS_Products',
  'rname' => 'name',
);
$dictionary["AOS_Bookings"]["fields"]["aos_products_aos_bookings_1aos_products_ida"] = array (
  'name' => 'aos_products_aos_bookings_1aos_products_ida',
  'type' => 'link',
  'relationship' => 'aos_products_aos_bookings_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_AOS_PRODUCTS_AOS_BOOKINGS_1_FROM_AOS_BOOKINGS_TITLE',
);
