<?php
// created: 2021-09-21 10:26:29
$dictionary["AOS_Products"]["fields"]["aos_products_bill_billing_1"] = array (
  'name' => 'aos_products_bill_billing_1',
  'type' => 'link',
  'relationship' => 'aos_products_bill_billing_1',
  'source' => 'non-db',
  'module' => 'bill_Billing',
  'bean_name' => 'bill_Billing',
  'side' => 'right',
  'vname' => 'LBL_AOS_PRODUCTS_BILL_BILLING_1_FROM_BILL_BILLING_TITLE',
);
