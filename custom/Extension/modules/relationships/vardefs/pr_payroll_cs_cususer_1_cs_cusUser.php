<?php
// created: 2023-05-16 06:50:02
$dictionary["cs_cusUser"]["fields"]["pr_payroll_cs_cususer_1"] = array (
  'name' => 'pr_payroll_cs_cususer_1',
  'type' => 'link',
  'relationship' => 'pr_payroll_cs_cususer_1',
  'source' => 'non-db',
  'module' => 'pr_payroll',
  'bean_name' => 'pr_payroll',
  'vname' => 'LBL_PR_PAYROLL_CS_CUSUSER_1_FROM_PR_PAYROLL_TITLE',
  'id_name' => 'pr_payroll_cs_cususer_1pr_payroll_ida',
);
$dictionary["cs_cusUser"]["fields"]["pr_payroll_cs_cususer_1_name"] = array (
  'name' => 'pr_payroll_cs_cususer_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_PR_PAYROLL_CS_CUSUSER_1_FROM_PR_PAYROLL_TITLE',
  'save' => true,
  'id_name' => 'pr_payroll_cs_cususer_1pr_payroll_ida',
  'link' => 'pr_payroll_cs_cususer_1',
  'table' => 'pr_payroll',
  'module' => 'pr_payroll',
  'rname' => 'name',
);
$dictionary["cs_cusUser"]["fields"]["pr_payroll_cs_cususer_1pr_payroll_ida"] = array (
  'name' => 'pr_payroll_cs_cususer_1pr_payroll_ida',
  'type' => 'link',
  'relationship' => 'pr_payroll_cs_cususer_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_PR_PAYROLL_CS_CUSUSER_1_FROM_CS_CUSUSER_TITLE',
);
