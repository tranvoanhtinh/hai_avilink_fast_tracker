<?php
// created: 2024-06-07 04:35:49
$dictionary["bill_Billing"]["fields"]["aos_bookings_bill_billing_1"] = array (
  'name' => 'aos_bookings_bill_billing_1',
  'type' => 'link',
  'relationship' => 'aos_bookings_bill_billing_1',
  'source' => 'non-db',
  'module' => 'AOS_Bookings',
  'bean_name' => 'AOS_Bookings',
  'vname' => 'LBL_AOS_BOOKINGS_BILL_BILLING_1_FROM_AOS_BOOKINGS_TITLE',
  'id_name' => 'aos_bookings_bill_billing_1aos_bookings_ida',
);
$dictionary["bill_Billing"]["fields"]["aos_bookings_bill_billing_1_name"] = array (
  'name' => 'aos_bookings_bill_billing_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_AOS_BOOKINGS_BILL_BILLING_1_FROM_AOS_BOOKINGS_TITLE',
  'save' => true,
  'id_name' => 'aos_bookings_bill_billing_1aos_bookings_ida',
  'link' => 'aos_bookings_bill_billing_1',
  'table' => 'aos_bookings',
  'module' => 'AOS_Bookings',
  'rname' => 'name',
);
$dictionary["bill_Billing"]["fields"]["aos_bookings_bill_billing_1aos_bookings_ida"] = array (
  'name' => 'aos_bookings_bill_billing_1aos_bookings_ida',
  'type' => 'link',
  'relationship' => 'aos_bookings_bill_billing_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_AOS_BOOKINGS_BILL_BILLING_1_FROM_BILL_BILLING_TITLE',
);
