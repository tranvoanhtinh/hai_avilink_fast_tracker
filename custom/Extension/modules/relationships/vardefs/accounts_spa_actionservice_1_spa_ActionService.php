<?php
// created: 2024-05-03 09:30:21
$dictionary["spa_ActionService"]["fields"]["accounts_spa_actionservice_1"] = array (
  'name' => 'accounts_spa_actionservice_1',
  'type' => 'link',
  'relationship' => 'accounts_spa_actionservice_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_SPA_ACTIONSERVICE_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_spa_actionservice_1accounts_ida',
);
$dictionary["spa_ActionService"]["fields"]["accounts_spa_actionservice_1_name"] = array (
  'name' => 'accounts_spa_actionservice_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_SPA_ACTIONSERVICE_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_spa_actionservice_1accounts_ida',
  'link' => 'accounts_spa_actionservice_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["spa_ActionService"]["fields"]["accounts_spa_actionservice_1accounts_ida"] = array (
  'name' => 'accounts_spa_actionservice_1accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_spa_actionservice_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_SPA_ACTIONSERVICE_1_FROM_SPA_ACTIONSERVICE_TITLE',
);
