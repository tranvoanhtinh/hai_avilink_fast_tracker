<?php
// created: 2024-08-08 04:06:36
$dictionary["AOS_Passengers"]["fields"]["aos_passengers_spa_actionservice_1"] = array (
  'name' => 'aos_passengers_spa_actionservice_1',
  'type' => 'link',
  'relationship' => 'aos_passengers_spa_actionservice_1',
  'source' => 'non-db',
  'module' => 'spa_ActionService',
  'bean_name' => 'spa_ActionService',
  'side' => 'right',
  'vname' => 'LBL_AOS_PASSENGERS_SPA_ACTIONSERVICE_1_FROM_SPA_ACTIONSERVICE_TITLE',
);
