<?php
// created: 2020-12-09 05:55:34
$dictionary["tttt_History_calls"]["fields"]["contacts_tttt_history_calls_1"] = array (
  'name' => 'contacts_tttt_history_calls_1',
  'type' => 'link',
  'relationship' => 'contacts_tttt_history_calls_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_CONTACTS_TTTT_HISTORY_CALLS_1_FROM_CONTACTS_TITLE',
  'id_name' => 'contacts_tttt_history_calls_1contacts_ida',
);
$dictionary["tttt_History_calls"]["fields"]["contacts_tttt_history_calls_1_name"] = array (
  'name' => 'contacts_tttt_history_calls_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_TTTT_HISTORY_CALLS_1_FROM_CONTACTS_TITLE',
  'save' => true,
  'id_name' => 'contacts_tttt_history_calls_1contacts_ida',
  'link' => 'contacts_tttt_history_calls_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["tttt_History_calls"]["fields"]["contacts_tttt_history_calls_1contacts_ida"] = array (
  'name' => 'contacts_tttt_history_calls_1contacts_ida',
  'type' => 'link',
  'relationship' => 'contacts_tttt_history_calls_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_TTTT_HISTORY_CALLS_1_FROM_TTTT_HISTORY_CALLS_TITLE',
);
