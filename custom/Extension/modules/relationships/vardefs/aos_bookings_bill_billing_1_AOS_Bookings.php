<?php
// created: 2024-06-07 04:35:49
$dictionary["AOS_Bookings"]["fields"]["aos_bookings_bill_billing_1"] = array (
  'name' => 'aos_bookings_bill_billing_1',
  'type' => 'link',
  'relationship' => 'aos_bookings_bill_billing_1',
  'source' => 'non-db',
  'module' => 'bill_Billing',
  'bean_name' => 'bill_Billing',
  'side' => 'right',
  'vname' => 'LBL_AOS_BOOKINGS_BILL_BILLING_1_FROM_BILL_BILLING_TITLE',
);
