<?php
// created: 2024-01-06 02:41:51
$dictionary["AOS_Products"]["fields"]["aos_products_notes_1"] = array (
  'name' => 'aos_products_notes_1',
  'type' => 'link',
  'relationship' => 'aos_products_notes_1',
  'source' => 'non-db',
  'module' => 'Notes',
  'bean_name' => 'Note',
  'side' => 'right',
  'vname' => 'LBL_AOS_PRODUCTS_NOTES_1_FROM_NOTES_TITLE',
);
