<?php
// created: 2023-05-16 06:50:02
$dictionary["pr_payroll"]["fields"]["pr_payroll_cs_cususer_1"] = array (
  'name' => 'pr_payroll_cs_cususer_1',
  'type' => 'link',
  'relationship' => 'pr_payroll_cs_cususer_1',
  'source' => 'non-db',
  'module' => 'cs_cusUser',
  'bean_name' => 'cs_cusUser',
  'side' => 'right',
  'vname' => 'LBL_PR_PAYROLL_CS_CUSUSER_1_FROM_CS_CUSUSER_TITLE',
);
