<?php
// created: 2020-12-09 05:58:18
$dictionary["Lead"]["fields"]["leads_tttt_history_calls_1"] = array (
  'name' => 'leads_tttt_history_calls_1',
  'type' => 'link',
  'relationship' => 'leads_tttt_history_calls_1',
  'source' => 'non-db',
  'module' => 'tttt_History_calls',
  'bean_name' => 'tttt_History_calls',
  'side' => 'right',
  'vname' => 'LBL_LEADS_TTTT_HISTORY_CALLS_1_FROM_TTTT_HISTORY_CALLS_TITLE',
);
