<?php
// created: 2021-05-07 10:03:27
$dictionary["AOS_Products"]["fields"]["aos_products_leads_1"] = array (
  'name' => 'aos_products_leads_1',
  'type' => 'link',
  'relationship' => 'aos_products_leads_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'side' => 'right',
  'vname' => 'LBL_AOS_PRODUCTS_LEADS_1_FROM_LEADS_TITLE',
);
