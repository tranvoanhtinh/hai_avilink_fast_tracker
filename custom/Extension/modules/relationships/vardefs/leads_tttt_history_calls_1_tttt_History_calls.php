<?php
// created: 2020-12-09 05:58:18
$dictionary["tttt_History_calls"]["fields"]["leads_tttt_history_calls_1"] = array (
  'name' => 'leads_tttt_history_calls_1',
  'type' => 'link',
  'relationship' => 'leads_tttt_history_calls_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_LEADS_TTTT_HISTORY_CALLS_1_FROM_LEADS_TITLE',
  'id_name' => 'leads_tttt_history_calls_1leads_ida',
);
$dictionary["tttt_History_calls"]["fields"]["leads_tttt_history_calls_1_name"] = array (
  'name' => 'leads_tttt_history_calls_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LEADS_TTTT_HISTORY_CALLS_1_FROM_LEADS_TITLE',
  'save' => true,
  'id_name' => 'leads_tttt_history_calls_1leads_ida',
  'link' => 'leads_tttt_history_calls_1',
  'table' => 'leads',
  'module' => 'Leads',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["tttt_History_calls"]["fields"]["leads_tttt_history_calls_1leads_ida"] = array (
  'name' => 'leads_tttt_history_calls_1leads_ida',
  'type' => 'link',
  'relationship' => 'leads_tttt_history_calls_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_LEADS_TTTT_HISTORY_CALLS_1_FROM_TTTT_HISTORY_CALLS_TITLE',
);
