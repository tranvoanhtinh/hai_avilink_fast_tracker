<?php
// created: 2023-05-30 10:15:14
$dictionary["AOS_Products"]["fields"]["aos_products_spa_actionservice_1"] = array (
  'name' => 'aos_products_spa_actionservice_1',
  'type' => 'link',
  'relationship' => 'aos_products_spa_actionservice_1',
  'source' => 'non-db',
  'module' => 'spa_ActionService',
  'bean_name' => 'spa_ActionService',
  'side' => 'right',
  'vname' => 'LBL_AOS_PRODUCTS_SPA_ACTIONSERVICE_1_FROM_SPA_ACTIONSERVICE_TITLE',
);
