<?php
// created: 2024-06-05 05:34:18
$dictionary["AOS_Passengers"]["fields"]["accounts_aos_passengers_1"] = array (
  'name' => 'accounts_aos_passengers_1',
  'type' => 'link',
  'relationship' => 'accounts_aos_passengers_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_AOS_PASSENGERS_1_FROM_ACCOUNTS_TITLE',
);
