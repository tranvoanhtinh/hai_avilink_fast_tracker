<?php
// created: 2024-05-23 06:25:43
$dictionary["AOS_Products"]["fields"]["aos_products_aos_bookings_1"] = array (
  'name' => 'aos_products_aos_bookings_1',
  'type' => 'link',
  'relationship' => 'aos_products_aos_bookings_1',
  'source' => 'non-db',
  'module' => 'AOS_Bookings',
  'bean_name' => 'AOS_Bookings',
  'side' => 'right',
  'vname' => 'LBL_AOS_PRODUCTS_AOS_BOOKINGS_1_FROM_AOS_BOOKINGS_TITLE',
);
