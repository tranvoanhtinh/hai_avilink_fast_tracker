<?php
// created: 2024-05-27 03:38:07
$dictionary["Account"]["fields"]["accounts_aos_data_unitprice_1"] = array (
  'name' => 'accounts_aos_data_unitprice_1',
  'type' => 'link',
  'relationship' => 'accounts_aos_data_unitprice_1',
  'source' => 'non-db',
  'module' => 'AOS_Data_unitprice',
  'bean_name' => 'AOS_Data_unitprice',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_AOS_DATA_UNITPRICE_1_FROM_AOS_DATA_UNITPRICE_TITLE',
);
