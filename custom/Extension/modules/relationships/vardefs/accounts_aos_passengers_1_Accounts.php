<?php
// created: 2024-06-05 05:34:18
$dictionary["Account"]["fields"]["accounts_aos_passengers_1"] = array (
  'name' => 'accounts_aos_passengers_1',
  'type' => 'link',
  'relationship' => 'accounts_aos_passengers_1',
  'source' => 'non-db',
  'module' => 'AOS_Passengers',
  'bean_name' => 'AOS_Passengers',
  'vname' => 'LBL_ACCOUNTS_AOS_PASSENGERS_1_FROM_AOS_PASSENGERS_TITLE',
);
