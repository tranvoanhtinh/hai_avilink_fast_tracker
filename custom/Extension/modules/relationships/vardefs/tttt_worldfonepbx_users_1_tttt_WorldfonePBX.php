<?php
// created: 2022-10-11 09:31:13
$dictionary["tttt_WorldfonePBX"]["fields"]["tttt_worldfonepbx_users_1"] = array (
  'name' => 'tttt_worldfonepbx_users_1',
  'type' => 'link',
  'relationship' => 'tttt_worldfonepbx_users_1',
  'source' => 'non-db',
  'module' => 'Users',
  'bean_name' => 'User',
  'vname' => 'LBL_TTTT_WORLDFONEPBX_USERS_1_FROM_USERS_TITLE',
);
