<?php
// created: 2024-05-27 03:35:52
$dictionary["Account"]["fields"]["accounts_aos_cost_unitprice_1"] = array (
  'name' => 'accounts_aos_cost_unitprice_1',
  'type' => 'link',
  'relationship' => 'accounts_aos_cost_unitprice_1',
  'source' => 'non-db',
  'module' => 'AOS_Cost_unitprice',
  'bean_name' => 'AOS_Cost_unitprice',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_AOS_COST_UNITPRICE_1_FROM_AOS_COST_UNITPRICE_TITLE',
);
