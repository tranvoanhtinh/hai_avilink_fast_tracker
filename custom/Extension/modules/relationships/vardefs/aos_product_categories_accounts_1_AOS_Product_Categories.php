<?php
// created: 2023-11-13 10:28:56
$dictionary["AOS_Product_Categories"]["fields"]["aos_product_categories_accounts_1"] = array (
  'name' => 'aos_product_categories_accounts_1',
  'type' => 'link',
  'relationship' => 'aos_product_categories_accounts_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_AOS_PRODUCT_CATEGORIES_ACCOUNTS_1_FROM_ACCOUNTS_TITLE',
);
