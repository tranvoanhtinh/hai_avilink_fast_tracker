<?php
// created: 2021-12-14 04:25:00
$dictionary["Account"]["fields"]["fp_event_locations_accounts_1"] = array (
  'name' => 'fp_event_locations_accounts_1',
  'type' => 'link',
  'relationship' => 'fp_event_locations_accounts_1',
  'source' => 'non-db',
  'module' => 'FP_Event_Locations',
  'bean_name' => 'FP_Event_Locations',
  'vname' => 'LBL_FP_EVENT_LOCATIONS_ACCOUNTS_1_FROM_FP_EVENT_LOCATIONS_TITLE',
  'id_name' => 'fp_event_locations_accounts_1fp_event_locations_ida',
);
$dictionary["Account"]["fields"]["fp_event_locations_accounts_1_name"] = array (
  'name' => 'fp_event_locations_accounts_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_FP_EVENT_LOCATIONS_ACCOUNTS_1_FROM_FP_EVENT_LOCATIONS_TITLE',
  'save' => true,
  'id_name' => 'fp_event_locations_accounts_1fp_event_locations_ida',
  'link' => 'fp_event_locations_accounts_1',
  'table' => 'fp_event_locations',
  'module' => 'FP_Event_Locations',
  'rname' => 'name',
);
$dictionary["Account"]["fields"]["fp_event_locations_accounts_1fp_event_locations_ida"] = array (
  'name' => 'fp_event_locations_accounts_1fp_event_locations_ida',
  'type' => 'link',
  'relationship' => 'fp_event_locations_accounts_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_FP_EVENT_LOCATIONS_ACCOUNTS_1_FROM_ACCOUNTS_TITLE',
);
