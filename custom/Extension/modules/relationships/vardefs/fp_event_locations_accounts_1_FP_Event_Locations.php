<?php
// created: 2021-12-14 04:25:00
$dictionary["FP_Event_Locations"]["fields"]["fp_event_locations_accounts_1"] = array (
  'name' => 'fp_event_locations_accounts_1',
  'type' => 'link',
  'relationship' => 'fp_event_locations_accounts_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_FP_EVENT_LOCATIONS_ACCOUNTS_1_FROM_ACCOUNTS_TITLE',
);
