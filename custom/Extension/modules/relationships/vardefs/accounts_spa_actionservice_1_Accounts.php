<?php
// created: 2024-05-03 09:30:21
$dictionary["Account"]["fields"]["accounts_spa_actionservice_1"] = array (
  'name' => 'accounts_spa_actionservice_1',
  'type' => 'link',
  'relationship' => 'accounts_spa_actionservice_1',
  'source' => 'non-db',
  'module' => 'spa_ActionService',
  'bean_name' => 'spa_ActionService',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_SPA_ACTIONSERVICE_1_FROM_SPA_ACTIONSERVICE_TITLE',
);
