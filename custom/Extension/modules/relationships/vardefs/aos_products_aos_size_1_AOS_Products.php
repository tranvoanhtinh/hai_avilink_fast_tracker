<?php
// created: 2024-05-13 10:02:31
$dictionary["AOS_Products"]["fields"]["aos_products_aos_size_1"] = array (
  'name' => 'aos_products_aos_size_1',
  'type' => 'link',
  'relationship' => 'aos_products_aos_size_1',
  'source' => 'non-db',
  'module' => 'AOS_Size',
  'bean_name' => 'AOS_Size',
  'vname' => 'LBL_AOS_PRODUCTS_AOS_SIZE_1_FROM_AOS_SIZE_TITLE',
);
