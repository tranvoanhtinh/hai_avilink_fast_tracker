<?php
 // created: 2024-04-15 12:21:09
$layout_defs["AOS_Products"]["subpanel_setup"]['aos_products_aos_packing_1'] = array (
  'order' => 100,
  'module' => 'AOS_Packing',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_AOS_PRODUCTS_AOS_PACKING_1_FROM_AOS_PACKING_TITLE',
  'get_subpanel_data' => 'aos_products_aos_packing_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
