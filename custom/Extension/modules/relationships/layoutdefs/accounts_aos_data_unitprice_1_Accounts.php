<?php
 // created: 2024-05-27 03:38:06
$layout_defs["Accounts"]["subpanel_setup"]['accounts_aos_data_unitprice_1'] = array (
  'order' => 100,
  'module' => 'AOS_Data_unitprice',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_AOS_DATA_UNITPRICE_1_FROM_AOS_DATA_UNITPRICE_TITLE',
  'get_subpanel_data' => 'accounts_aos_data_unitprice_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
