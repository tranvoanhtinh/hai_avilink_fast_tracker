<?php
 // created: 2024-05-28 11:28:30
$layout_defs["AOS_Bookings"]["subpanel_setup"]['aos_bookings_spa_actionservice_1'] = array (
  'order' => 100,
  'module' => 'spa_ActionService',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_AOS_BOOKINGS_SPA_ACTIONSERVICE_1_FROM_SPA_ACTIONSERVICE_TITLE',
  'get_subpanel_data' => 'aos_bookings_spa_actionservice_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
