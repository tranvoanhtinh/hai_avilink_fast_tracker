<?php
 // created: 2024-06-07 04:35:48
$layout_defs["AOS_Bookings"]["subpanel_setup"]['aos_bookings_bill_billing_1'] = array (
  'order' => 100,
  'module' => 'bill_Billing',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_AOS_BOOKINGS_BILL_BILLING_1_FROM_BILL_BILLING_TITLE',
  'get_subpanel_data' => 'aos_bookings_bill_billing_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
