<?php
 // created: 2023-05-23 06:52:50
$layout_defs["cs_cusUser"]["subpanel_setup"]['cs_cususer_bill_billing_1'] = array (
  'order' => 100,
  'module' => 'bill_Billing',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CS_CUSUSER_BILL_BILLING_1_FROM_BILL_BILLING_TITLE',
  'get_subpanel_data' => 'cs_cususer_bill_billing_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
