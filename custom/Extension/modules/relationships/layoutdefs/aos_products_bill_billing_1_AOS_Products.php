<?php
 // created: 2021-09-21 10:26:28
$layout_defs["AOS_Products"]["subpanel_setup"]['aos_products_bill_billing_1'] = array (
  'order' => 100,
  'module' => 'bill_Billing',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_AOS_PRODUCTS_BILL_BILLING_1_FROM_BILL_BILLING_TITLE',
  'get_subpanel_data' => 'aos_products_bill_billing_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
