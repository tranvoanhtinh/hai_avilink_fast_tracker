<?php
 // created: 2021-09-15 09:24:17
$dictionary['Account']['fields']['opportunityamount']['name']='opportunityamount';
$dictionary['Account']['fields']['opportunityamount']['vname']='LBL_OPPORTUNITY_AMOUNT';
$dictionary['Account']['fields']['opportunityamount']['type']='currency';
$dictionary['Account']['fields']['opportunityamount']['dbType']='double';
$dictionary['Account']['fields']['opportunityamount']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['opportunityamount']['required']=false;
$dictionary['Account']['fields']['opportunityamount']['options']='numeric_range_search_dom';
$dictionary['Account']['fields']['opportunityamount']['enable_range_search']=true;
$dictionary['Account']['fields']['opportunityamount']['inline_edit']='';
$dictionary['Account']['fields']['opportunityamount']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['opportunityamount']['merge_filter']='disabled';
$dictionary['Account']['fields']['opportunityamount']['audited']=true;

 ?>