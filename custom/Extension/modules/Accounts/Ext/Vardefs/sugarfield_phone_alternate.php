<?php
 // created: 2021-10-10 08:02:57
$dictionary['Account']['fields']['phone_alternate']['inline_edit']='';
$dictionary['Account']['fields']['phone_alternate']['comments']='An alternate phone number';
$dictionary['Account']['fields']['phone_alternate']['merge_filter']='disabled';
$dictionary['Account']['fields']['phone_alternate']['len']='50';
$dictionary['Account']['fields']['phone_alternate']['audited']=true;

 ?>