<?php
// created: 2021-08-18 06:32:45
$dictionary["Account"]["fields"]["bill_billing_accounts"] = array (
  'name' => 'bill_billing_accounts',
  'type' => 'link',
  'relationship' => 'bill_billing_accounts',
  'source' => 'non-db',
  'module' => 'bill_Billing',
  'bean_name' => 'bill_Billing',
  'side' => 'right',
  'vname' => 'LBL_BILL_BILLING_ACCOUNTS_FROM_BILL_BILLING_TITLE',
);
