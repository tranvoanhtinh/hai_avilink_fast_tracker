<?php
 // created: 2022-09-30 05:20:54
$dictionary['Account']['fields']['classify_account_level']['name']='classify_account_level';
$dictionary['Account']['fields']['classify_account_level']['vname']='LBL_CLASSIFY_ACCOUNT_LEVEL';
$dictionary['Account']['fields']['classify_account_level']['len']='100';
$dictionary['Account']['fields']['classify_account_level']['type']='enum';
$dictionary['Account']['fields']['classify_account_level']['audited']=true;
$dictionary['Account']['fields']['classify_account_level']['inline_edit']=true;
$dictionary['Account']['fields']['classify_account_level']['massupdate']='1';
$dictionary['Account']['fields']['classify_account_level']['options']='classify_account_level_list';
$dictionary['Account']['fields']['classify_account_level']['merge_filter']='disabled';

 ?>