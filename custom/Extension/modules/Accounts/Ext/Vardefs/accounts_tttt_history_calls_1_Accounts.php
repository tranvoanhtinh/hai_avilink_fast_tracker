<?php
// created: 2020-12-09 05:40:10
$dictionary["Account"]["fields"]["accounts_tttt_history_calls_1"] = array (
  'name' => 'accounts_tttt_history_calls_1',
  'type' => 'link',
  'relationship' => 'accounts_tttt_history_calls_1',
  'source' => 'non-db',
  'module' => 'tttt_History_calls',
  'bean_name' => 'tttt_History_calls',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_TTTT_HISTORY_CALLS_1_FROM_TTTT_HISTORY_CALLS_TITLE',
);
