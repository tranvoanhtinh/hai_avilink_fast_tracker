<?php
 // created: 2022-01-20 07:05:28
$dictionary['Account']['fields']['company_type']['name']='company_type';
$dictionary['Account']['fields']['company_type']['vname']='LBL_COMPANY_TYPE';
$dictionary['Account']['fields']['company_type']['len']='100';
$dictionary['Account']['fields']['company_type']['type']='enum';
$dictionary['Account']['fields']['company_type']['audited']=true;
$dictionary['Account']['fields']['company_type']['inline_edit']=true;
$dictionary['Account']['fields']['company_type']['massupdate']=0;
$dictionary['Account']['fields']['company_type']['options']='company_type_list';
$dictionary['Account']['fields']['company_type']['merge_filter']='disabled';

 ?>