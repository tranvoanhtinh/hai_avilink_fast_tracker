<?php
 $dictionary["Account"]["fields"]["photo"] = array(

            'name' => 'photo',
            'vname' => 'LBL_PHOTO',
            'type' => 'image',
            'massupdate' => false,
            'comments' => '',
            'help' => '',
            'importable' => false,
            'reportable' => true,
            'len' => 255,
            'dbType' => 'varchar',
            'width' => '160',
            'height' => '160',
            'studio' => array('listview' => true),
  
);
