<?php
 // created: 2022-01-11 16:22:15
$dictionary['Account']['fields']['object_customer']['name']='object_customer';
$dictionary['Account']['fields']['object_customer']['vname']='LBL_OBJECT_CUSTOMER';
$dictionary['Account']['fields']['object_customer']['len']='100';
$dictionary['Account']['fields']['object_customer']['type']='enum';
$dictionary['Account']['fields']['object_customer']['audited']=true;
$dictionary['Account']['fields']['object_customer']['inline_edit']=true;
$dictionary['Account']['fields']['object_customer']['massupdate']='1';
$dictionary['Account']['fields']['object_customer']['options']='object_customer_list';
$dictionary['Account']['fields']['object_customer']['merge_filter']='disabled';

 ?>