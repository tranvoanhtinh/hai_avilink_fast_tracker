<?php
// created: 2024-05-22 03:26:41
$dictionary["Account"]["fields"]["accounts_aos_bookings_1"] = array (
  'name' => 'accounts_aos_bookings_1',
  'type' => 'link',
  'relationship' => 'accounts_aos_bookings_1',
  'source' => 'non-db',
  'module' => 'AOS_Bookings',
  'bean_name' => 'AOS_Bookings',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_AOS_BOOKINGS_1_FROM_AOS_BOOKINGS_TITLE',
);
