<?php
 $dictionary["Account"]["fields"]["scanner"] = array(
    'name' => 'scanner',
    'vname' => 'LBL_SCANNER',
    'type' => 'varchar',
    "len" => 255,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);
 ?>