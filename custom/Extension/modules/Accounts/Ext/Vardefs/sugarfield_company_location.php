<?php
 // created: 2022-01-20 07:05:16
$dictionary['Account']['fields']['company_location']['name']='company_location';
$dictionary['Account']['fields']['company_location']['vname']='LBL_COMPANY_LOCATION';
$dictionary['Account']['fields']['company_location']['len']='100';
$dictionary['Account']['fields']['company_location']['type']='enum';
$dictionary['Account']['fields']['company_location']['audited']=true;
$dictionary['Account']['fields']['company_location']['inline_edit']=true;
$dictionary['Account']['fields']['company_location']['massupdate']=0;
$dictionary['Account']['fields']['company_location']['options']='company_location_list';
$dictionary['Account']['fields']['company_location']['merge_filter']='disabled';

 ?>