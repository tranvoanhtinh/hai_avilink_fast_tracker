<?php
 // created: 2022-01-20 07:05:08
$dictionary['Account']['fields']['company_legal']['name']='company_legal';
$dictionary['Account']['fields']['company_legal']['vname']='LBL_COMPANY_LEGAL';
$dictionary['Account']['fields']['company_legal']['len']='100';
$dictionary['Account']['fields']['company_legal']['type']='enum';
$dictionary['Account']['fields']['company_legal']['audited']=true;
$dictionary['Account']['fields']['company_legal']['inline_edit']=true;
$dictionary['Account']['fields']['company_legal']['massupdate']=0;
$dictionary['Account']['fields']['company_legal']['options']='company_legal_list';
$dictionary['Account']['fields']['company_legal']['merge_filter']='disabled';

 ?>