<?php
 // created: 2021-09-15 09:24:17
$dictionary['Account']['fields']['paymentsamount']['name']='paymentsamount';
$dictionary['Account']['fields']['paymentsamount']['vname']='LBL_Payments_Amount';
$dictionary['Account']['fields']['paymentsamount']['type']='currency';
$dictionary['Account']['fields']['paymentsamount']['dbType']='double';
$dictionary['Account']['fields']['paymentsamount']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['paymentsamount']['required']=false;
$dictionary['Account']['fields']['paymentsamount']['options']='numeric_range_search_dom';
$dictionary['Account']['fields']['paymentsamount']['enable_range_search']=true;
$dictionary['Account']['fields']['paymentsamount']['inline_edit']='';
$dictionary['Account']['fields']['paymentsamount']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['paymentsamount']['merge_filter']='disabled';
$dictionary['Account']['fields']['paymentsamount']['audited']=true;

 ?>