<?php
 // created: 2022-01-11 16:22:15
$dictionary['Account']['fields']['countries']['name']='countries';
$dictionary['Account']['fields']['countries']['vname']='LBL_COUNTRIES';
$dictionary['Account']['fields']['countries']['len']='100';
$dictionary['Account']['fields']['countries']['type']='enum';
$dictionary['Account']['fields']['countries']['audited']=true;
$dictionary['Account']['fields']['countries']['inline_edit']=true;
$dictionary['Account']['fields']['countries']['massupdate']='1';
$dictionary['Account']['fields']['countries']['options']='countries_dom';
$dictionary['Account']['fields']['countries']['merge_filter']='disabled';

 ?>