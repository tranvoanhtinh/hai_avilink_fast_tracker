<?php
 // created: 2021-04-28 09:06:25
$dictionary['Account']['fields']['description']['required']=true;
$dictionary['Account']['fields']['description']['inline_edit']=true;
$dictionary['Account']['fields']['description']['comments']='Full text of the note';
$dictionary['Account']['fields']['description']['merge_filter']='disabled';
$dictionary['Account']['fields']['description']['rows']='4';

 ?>