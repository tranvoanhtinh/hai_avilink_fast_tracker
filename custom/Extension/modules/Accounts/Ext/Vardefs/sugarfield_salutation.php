<?php
 // created: 2021-10-18 07:50:43
$dictionary['Account']['fields']['salutation']['name']='salutation';
$dictionary['Account']['fields']['salutation']['vname']='LBL_SALUTATION';
$dictionary['Account']['fields']['salutation']['len']='100';
$dictionary['Account']['fields']['salutation']['type']='enum';
$dictionary['Account']['fields']['salutation']['inline_edit']=true;
$dictionary['Account']['fields']['salutation']['merge_filter']='disabled';
$dictionary['Account']['fields']['salutation']['massupdate']='1';
$dictionary['Account']['fields']['salutation']['options']='salutation_dom';

 ?>