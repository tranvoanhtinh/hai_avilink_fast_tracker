<?php
 // created: 2023-12-07 03:38:15
$dictionary['Account']['fields']['social_account']['name']='social_account';
$dictionary['Account']['fields']['social_account']['vname']='LBL_SOCIAL_ACCOUNT';
$dictionary['Account']['fields']['social_account']['len']='100';
$dictionary['Account']['fields']['social_account']['type']='enum';
$dictionary['Account']['fields']['social_account']['audited']=true;
$dictionary['Account']['fields']['social_account']['inline_edit']=true;
$dictionary['Account']['fields']['social_account']['massupdate']='1';
$dictionary['Account']['fields']['social_account']['options']='social_account_list';
$dictionary['Account']['fields']['social_account']['merge_filter']='disabled';

 ?>