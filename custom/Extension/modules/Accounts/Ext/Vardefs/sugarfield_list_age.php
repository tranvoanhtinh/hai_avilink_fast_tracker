<?php
$dictionary['Account']['fields']['list_age']['name']='list_age';
$dictionary['Account']['fields']['list_age']['vname']='LBL_LIST_AGE';
$dictionary['Account']['fields']['list_age']['len']='100';
$dictionary['Account']['fields']['list_age']['type']='enum';
$dictionary['Account']['fields']['list_age']['audited']=true;
$dictionary['Account']['fields']['list_age']['inline_edit']=true;
$dictionary['Account']['fields']['list_age']['massupdate']='1';
$dictionary['Account']['fields']['list_age']['options'] = 'list_age_list';
$dictionary['Account']['fields']['list_age']['merge_filter']='disabled';