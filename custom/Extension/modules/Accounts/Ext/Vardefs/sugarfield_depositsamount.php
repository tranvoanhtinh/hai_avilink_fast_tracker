<?php
 // created: 2021-09-09 06:01:28
$dictionary['Account']['fields']['depositsamount']['name']='depositsamount';
$dictionary['Account']['fields']['depositsamount']['vname']='LBL_Deposits_Amount';
$dictionary['Account']['fields']['depositsamount']['type']='currency';
$dictionary['Account']['fields']['depositsamount']['dbType']='double';
$dictionary['Account']['fields']['depositsamount']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['depositsamount']['required']=false;
$dictionary['Account']['fields']['depositsamount']['options']='numeric_range_search_dom';
$dictionary['Account']['fields']['depositsamount']['enable_range_search']=true;
$dictionary['Account']['fields']['depositsamount']['inline_edit']='';
$dictionary['Account']['fields']['depositsamount']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['depositsamount']['merge_filter']='disabled';
$dictionary['Account']['fields']['depositsamount']['audited']=true;

 ?>