<?php
 // created: 2019-10-29 07:11:22
$dictionary["Account"]["fields"]["aos_product_id"] = array(
    'name' => 'aos_product_id',
    'vname' => 'LBL_AOS_PRODUCT_ID',
    'type' => 'id',
	'id_name' => 'aos_product_id',
    'isnull' => 'true',
     'module' => 'AOS_Products',
	 'table' => 'aos_products',
    'reportable' => false,
     'massupdate' => false,
    'duplicate_merge' => 'disabled',
);
 ?>