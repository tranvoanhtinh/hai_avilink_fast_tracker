<?php
 // created: 2023-10-04 10:00:32
$dictionary['Account']['fields']['sex']['name']='sex';
$dictionary['Account']['fields']['sex']['vname']='LBL_SEX';
$dictionary['Account']['fields']['sex']['len']='100';
$dictionary['Account']['fields']['sex']['type']='enum';
$dictionary['Account']['fields']['sex']['audited']=true;
$dictionary['Account']['fields']['sex']['inline_edit']=true;
$dictionary['Account']['fields']['sex']['massupdate']='1';
$dictionary['Account']['fields']['sex']['options']='sex_list';
$dictionary['Account']['fields']['sex']['merge_filter']='disabled';

 ?>