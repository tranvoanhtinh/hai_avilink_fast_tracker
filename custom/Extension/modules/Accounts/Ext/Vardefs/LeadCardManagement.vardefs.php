<?php

$dictionary["Account"]["fields"]["opportunity_amount"] = array(
    "name" => "opportunity_amount",
    "type" => "decimal",
    "len" => 20,
    "precision" => 2,
    "source" => "non-db",
    "vname" => "LBL_OPPORTUNITY_AMOUNT"
);

$dictionary["Account"]["fields"]["status"] = array(
    "name" => "status",
    "type" => "enum",
    "len" => 20,
    "precision" => 2,
    "source" => "non-db",
    "vname" => "LBL_STATUS",
    "options" => "lead_status_dom"
);

$dictionary["Account"]["fields"]["lead_source"] = array(
    "name" => "lead_source",
    "type" => "enum",
    "len" => 20,
    "precision" => 2,
    "source" => "non-db",
    "vname" => "LBL_LIST_LEAD_SOURCE",
    "options" => "lead_source_dom",
);

$dictionary["Account"]["fields"]["tax_code"] = array(
    "name" => "tax_code",
    "type" => "varchar",
    "len" => 20,
    "vname" => "LBL_TAX_CODE",
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);

$dictionary["Account"]["fields"]["account_code"] = array(
    "name" => "account_code",
    "type" => "varchar",
    "len" => 20,
    "vname" => "LBL_ACCOUNT_CODE",
    "importable" => true,
    "audited" => true,
    "massupdate" => false,
    'required' => false,
);

$dictionary["Account"]["fields"]["contact_name"] = array(
    "name" => "contact_name",
    "type" => "varchar",
    "len" => 255,
    "source" => "non-db",
    "vname" => "LBL_CONTACT_NAME",
);

$dictionary["Account"]["fields"]["otherassigned"] = array(
    'name' => 'otherassigned',
    'vname' => 'LBL_OTHER_ASSIGNED',
    'type' => 'varchar',
    "len" => 255,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);
$dictionary["Account"]["fields"]["phone_alternate"]['importable'] = true;
$dictionary["Account"]["fields"]["status"]['massupdate'] = false;
$dictionary["Account"]["fields"]["account_type"]['massupdate'] = false;
$dictionary["Account"]["fields"]["lead_source"]['massupdate'] = false;
$dictionary["Account"]["fields"]["industry"]['massupdate'] = false;
$dictionary["Account"]["fields"]["lead_account_status_c"]['massupdate'] = false;
$dictionary["Account"]["fields"]["type_c"]['massupdate'] = false;

$dictionary["Account"]["indices"][] = array('name' => 'idx_acc_id_del', 'type' => 'index', 'fields' => array('account_code', 'deleted'));
$dictionary["Account"]["indices"][] = array('name' => 'idx_tax_code_del', 'type' => 'index', 'fields' => array('tax_code', 'deleted'));
$dictionary["Account"]["indices"][] = array('name' => 'idx_phone_alternate_del', 'type' => 'index', 'fields' => array('phone_alternate', 'deleted'));


