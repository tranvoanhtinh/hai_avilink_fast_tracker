<?php
 // created: 2024-06-05 05:34:18
$layout_defs["Accounts"]["subpanel_setup"]['accounts_aos_passengers_1'] = array (
  'order' => 100,
  'module' => 'AOS_Passengers',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_AOS_PASSENGERS_1_FROM_AOS_PASSENGERS_TITLE',
  'get_subpanel_data' => 'accounts_aos_passengers_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
