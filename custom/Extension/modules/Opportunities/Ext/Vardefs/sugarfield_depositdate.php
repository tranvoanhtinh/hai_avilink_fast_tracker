<?php
 // created: 2021-10-03 08:06:28
$dictionary['Opportunity']['fields']['depositdate']['name']='depositdate';
$dictionary['Opportunity']['fields']['depositdate']['vname']='LBL_DEPOSIT_DATE';
$dictionary['Opportunity']['fields']['depositdate']['type']='date';
$dictionary['Opportunity']['fields']['depositdate']['audited']=true;
$dictionary['Opportunity']['fields']['depositdate']['options']='date_range_search_dom';
$dictionary['Opportunity']['fields']['depositdate']['required']=true;
$dictionary['Opportunity']['fields']['depositdate']['inline_edit']=true;
$dictionary['Opportunity']['fields']['depositdate']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['depositdate']['enable_range_search']='1';
$dictionary['Opportunity']['fields']['depositdate']['display_default']='now';

 ?>