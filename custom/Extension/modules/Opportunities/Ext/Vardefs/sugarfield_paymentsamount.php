<?php
 // created: 2021-09-15 09:24:17
$dictionary['Opportunity']['fields']['paymentsamount']['name']='paymentsamount';
$dictionary['Opportunity']['fields']['paymentsamount']['vname']='LBL_Payments_Amount';
$dictionary['Opportunity']['fields']['paymentsamount']['type']='currency';
$dictionary['Opportunity']['fields']['paymentsamount']['dbType']='double';
$dictionary['Opportunity']['fields']['paymentsamount']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['paymentsamount']['required']=false;
$dictionary['Opportunity']['fields']['paymentsamount']['options']='numeric_range_search_dom';
$dictionary['Opportunity']['fields']['paymentsamount']['enable_range_search']=true;
$dictionary['Opportunity']['fields']['paymentsamount']['inline_edit']='';
$dictionary['Opportunity']['fields']['paymentsamount']['duplicate_merge_dom_value']='1';
$dictionary['Opportunity']['fields']['paymentsamount']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['paymentsamount']['audited']=true;

 ?>