<?php
 // created: 2021-09-09 06:01:28
$dictionary['Opportunity']['fields']['depositsamount']['name']='depositsamount';
$dictionary['Opportunity']['fields']['depositsamount']['vname']='LBL_Deposits_Amount';
$dictionary['Opportunity']['fields']['depositsamount']['type']='currency';
$dictionary['Opportunity']['fields']['depositsamount']['dbType']='double';
$dictionary['Opportunity']['fields']['depositsamount']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['depositsamount']['required']=false;
$dictionary['Opportunity']['fields']['depositsamount']['options']='numeric_range_search_dom';
$dictionary['Opportunity']['fields']['depositsamount']['enable_range_search']=true;
$dictionary['Opportunity']['fields']['depositsamount']['inline_edit']='';
$dictionary['Opportunity']['fields']['depositsamount']['duplicate_merge_dom_value']='1';
$dictionary['Opportunity']['fields']['depositsamount']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['depositsamount']['audited']=true;

 ?>