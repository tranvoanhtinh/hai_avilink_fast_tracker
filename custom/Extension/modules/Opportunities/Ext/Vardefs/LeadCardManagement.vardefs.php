<?php
/*
$dictionary["Opportunity"]["fields"]["customer_id"] = array(
    'name' => 'customer_id',
    'vname' => 'LBL_CUSTOMER_ID',
    'type' => 'name',
    'link' => true, // bug 39288
    'dbType' => 'varchar',
    'len' => 255,
    'unified_search' => true,
    'full_text_search' => array('boost' => 3),
    'required' => false,
    'importable' => 'required'
);

$dictionary["Opportunity"]["fields"]["from_province"] = array(
    'name' => 'from_province',
    'vname' => 'LBL_FROM_PROVINCE',
    'type' => 'enum',
    //'function' => 'getCities',
    'options' => 'city_options',
    'required' => false,
);

$dictionary["Opportunity"]["fields"]["to_province"] = array(
    'name' => 'to_province',
    'vname' => 'LBL_TO_PROVINCE',
    'type' => 'enum',
    //'function' => 'getCities',
    'options' => 'city_options',
    'required' => false,
);

$dictionary["Opportunity"]["fields"]["public_price"] = array(
            'name' => 'public_price',
            'vname' => 'LBL_PUBLIC_PRICE',
            'type' => 'currency',
            'dbType' => 'double',
            'duplicate_merge' => '1',
            'required' => true,
            'options' => 'numeric_range_search_dom',
            'enable_range_search' => true
);

$dictionary["Opportunity"]["fields"]["shipment_price"] = array(
    'name' => 'shipment_price',
    'vname' => 'LBL_SHIPMENT_PRICE',
    'type' => 'currency',
    'dbType' => 'double',
    'duplicate_merge' => '1',
    'required' => true,
    'options' => 'numeric_range_search_dom',
    'enable_range_search' => true
);

$dictionary["Opportunity"]["fields"]["main_price"] = array(
    'name' => 'main_price',
    'vname' => 'LBL_MAIN_PRICE',
    'type' => 'currency',
    'dbType' => 'double',
    'duplicate_merge' => '1',
    'required' => true,
    'options' => 'numeric_range_search_dom',
    'enable_range_search' => true
);
*/
$dictionary["Opportunity"]["fields"]["sales_stage"]["importable"] = true;
$dictionary["Opportunity"]["fields"]["sales_stage"]["default"] = 'Closed Won';
$dictionary["Opportunity"]["fields"]["account_name"]["importable"] = true;
