<?php
 // created: 2020-04-28 04:58:29
$dictionary['Opportunity']['fields']['description']['inline_edit']=true;
$dictionary['Opportunity']['fields']['description']['comments']='Full text of the note';
$dictionary['Opportunity']['fields']['description']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['description']['rows']='4';
$dictionary['Opportunity']['fields']['description']['cols']='80';

 ?>