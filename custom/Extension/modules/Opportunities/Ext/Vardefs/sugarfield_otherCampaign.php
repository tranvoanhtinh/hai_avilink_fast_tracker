<?php
 // created: 2021-03-05 09:37:42
$dictionary["Opportunity"]["fields"]["othercampaign"] = array(
    'name' => 'othercampaign',
    'vname' => 'LBL_OTHER_CAMPAIGN',
    'type' => 'varchar',
    "len" => 255,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);

 ?>