<?php

$dictionary["AOS_Pos_Line_Items_Detail"]["fields"]["total_ctns"] = array(
                'required' => false,
                'name' => 'total_ctns',
                'vname' => 'LBL_TOTAL_CTNS',
                'type' => 'currency',
                'massupdate' => 0,
                'comments' => '',
                'help' => '',
                'importable' => 'true',
                'duplicate_merge' => 'disabled',
                'duplicate_merge_dom_value' => '0',
                'audited' => 1,
                'reportable' => true,
                'len' => '26,6',
);


   
$dictionary["AOS_Pos_Line_Items_Detail"]["fields"]["total_cbm"] =
            array(
                'required' => false,
                'name' => 'total_cbm',
                'vname' => 'LBL_TOTAL_CBM',
                'type' => 'currency',
                'massupdate' => 0,
                'comments' => '',
                'help' => '',
                'importable' => 'true',
                'duplicate_merge' => 'disabled',
                'duplicate_merge_dom_value' => '0',
                'audited' => 1,
                'reportable' => true,
                'len' => '26,6',
);