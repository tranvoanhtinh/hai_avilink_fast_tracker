<?php
 // created: 2024-02-26 09:02:10
$layout_defs["AOS_Stock_Inventory_Detail"]["subpanel_setup"]['aos_inventoryoutput_aos_stock_inventory_detail_1'] = array (
  'order' => 100,
  'module' => 'AOS_Inventoryoutput',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_AOS_INVENTORYOUTPUT_AOS_STOCK_INVENTORY_DETAIL_1_FROM_AOS_INVENTORYOUTPUT_TITLE',
  'get_subpanel_data' => 'aos_inventoryoutput_aos_stock_inventory_detail_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
