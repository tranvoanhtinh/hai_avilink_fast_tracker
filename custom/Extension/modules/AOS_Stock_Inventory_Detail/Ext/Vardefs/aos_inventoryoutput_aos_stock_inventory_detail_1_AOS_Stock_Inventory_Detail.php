<?php
// created: 2024-02-26 09:02:10
$dictionary["AOS_Stock_Inventory_Detail"]["fields"]["aos_inventoryoutput_aos_stock_inventory_detail_1"] = array (
  'name' => 'aos_inventoryoutput_aos_stock_inventory_detail_1',
  'type' => 'link',
  'relationship' => 'aos_inventoryoutput_aos_stock_inventory_detail_1',
  'source' => 'non-db',
  'module' => 'AOS_Inventoryoutput',
  'bean_name' => 'AOS_Inventoryoutput',
  'vname' => 'LBL_AOS_INVENTORYOUTPUT_AOS_STOCK_INVENTORY_DETAIL_1_FROM_AOS_INVENTORYOUTPUT_TITLE',
);
