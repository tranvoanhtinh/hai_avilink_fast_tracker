<?php

$dictionary["AOS_Stock_Inventory_Detail"]["fields"]["voucherdate"] = array(
    'name' => 'voucherdate',
    'vname' => 'LBL_VOUCHER_DATE',
    'type' => 'date',
    'audited' => true,
    'required' => false,
    'enable_range_search' => true,
    'options' => 'date_range_search_dom',
    "massupdate" => false,
);
$dictionary["AOS_Stock_Inventory_Detail"]["fields"]["voucherno"] = array(
    'name' => 'voucherno',
    'vname' => 'LBL_VOUCHERNO',
    'type' => 'varchar',
    "len" => 50,
    "importable" => true,
    "audited" => true,
    "massupdate" => false,
   
);

$dictionary["AOS_Stock_Inventory_Detail"]["fields"]["type_voucher"] = array(
      'name' => 'type_voucher',
      'vname' => 'LBL_TYPE_VOUCHER',
      'len' => '100',
      'type' => 'enum',
      'audited' => true,
      'inline_edit' => true,
      'massupdate' => '1',
      'options' => 'type_voucher_list',
      'merge_filter' => 'disabled',

);    
$dictionary["AOS_Stock_Inventory_Detail"]["fields"]["maincode"] = array(
    'name' => 'maincode',
    'vname' => 'LBL_MAINCODE',
    'type' => 'varchar',
    "len" => 50,
    "importable" => true,
    "audited" => true,
    "massupdate" => false,
);
    $dictionary["AOS_Stock_Inventory_Detail"]["fields"]["quantity"] = array(
    'name' => 'quantity',
    'vname' => 'LBL_QUANTITY',
    'type' => 'varchar',
    "len" => 50,
    "importable" => true,
    "audited" => true,
    "massupdate" => false,
);


$dictionary["AOS_Stock_Inventory_Detail"]["fields"]["price"] =
    array(
        'required' => false,
        'name' => 'price',
        'vname' => 'LBL_PRICE',
        'type' => 'currency',
        'massupdate' => 0,
        'comments' => '',
        'help' => '',
        'importable' => 'true',
        'duplicate_merge' => 'disabled',
        'duplicate_merge_dom_value' => '0',
        'audited' => false,
        'reportable' => true,
        'len' => '26,6',
        'enable_range_search' => true,
        'options' => 'numeric_range_search_dom',
    );

$dictionary["AOS_Stock_Inventory_Detail"]["fields"]["day"] = array(
    'name' => 'day',
    'vname' => 'LBL_DAY',
    'type' => 'varchar',
    "len" => 10,
    "importable" => true,
    "audited" => true,
    "massupdate" => false,
);

$dictionary["AOS_Stock_Inventory_Detail"]["fields"]["month"] = array(
    'name' => 'month',
    'vname' => 'LBL_MONTH',
    'type' => 'varchar',
    "len" => 10,
    "importable" => true,
    "audited" => true,
    "massupdate" => false,
);
$dictionary["AOS_Stock_Inventory_Detail"]["fields"]["parent_id"] = array(
    'name' => 'parent_id',
    'vname' => 'LBL_PARENT_ID',
    'type' => 'varchar',
    "len" => 36,
    "importable" => true,
    "audited" => true,
    "massupdate" => false,
);
$dictionary["AOS_Stock_Inventory_Detail"]["fields"]["product_id"] = array(
    'name' => 'product_id',
    'vname' => 'LBL_PRODUCT_ID',
    'type' => 'varchar',
    "len" => 36,
    "importable" => true,
    "audited" => true,
    "massupdate" => false,
);

$dictionary["AOS_Stock_Inventory_Detail"]["fields"]["year"] = array(
    'name' => 'year',
    'vname' => 'LBL_YEAR',
    'type' => 'varchar',
    "len" => 10,
    "importable" => true,
    "audited" => true,
    "massupdate" => false,
);


$dictionary["AOS_Stock_Inventory_Detail"]["fields"]["photo"] = array(
    'name' => 'photo',
    'vname' => 'LBL_PHOTO',
    'type' => 'image',
    'massupdate' => false,
    'comments' => '',
    'help' => '',
    'importable' => false,
    'reportable' => true,
    'len' => 100,
    'dbType' => 'varchar',
    'width' => '160',
    'height' => '160',
    'studio' => array('listview' => true),

);


$dictionary["AOS_Stock_Inventory_Detail"]["fields"]["take_from"] = array(

      'name' => 'take_from',
      'vname' => 'LBL_TAKE_FROM',
      'len' => '100',
      'type' => 'enum',
      'audited' => true,
      'inline_edit' => true,
      'massupdate' => '1',
      'options' => 'take_from_list',
      'merge_filter' => 'disabled',

);    


$dictionary["AOS_Stock_Inventory_Detail"]["fields"]["in_out"] = array(
    'name' => 'in_out',
    'vname' => 'LBL_IN_OUT',
    'type' => 'varchar',
    "len" => 20,
    "importable" => true,
    "audited" => true,
    "massupdate" => false,
);

$dictionary["AOS_Stock_Inventory_Detail"]["fields"]["total_price"] =
        array(
   'required' => false,
            'name' => 'total_price',
            'vname' => 'LBL_TOTAL_PRICE',
            'type' => 'currency',
            'massupdate' => 0,
            'comments' => '',
            'help' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => false,
            'reportable' => true,
            'len' => '26,6',
            'enable_range_search' => true,
            'options' => 'numeric_range_search_dom',
        );


