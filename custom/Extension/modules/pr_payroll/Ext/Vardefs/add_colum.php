<?php
 $dictionary["pr_payroll"]["fields"]["real_workday"] = array(
    'name' => 'real_workday',
    'vname' => 'LBL_REAL_WORKDAY',
    'type' => 'decimal',
    'massupdate' => true,
	'len' => '18',
    'size' => '20',
    'enable_range_search' => false,
    'precision' => '2',
);
$dictionary["pr_payroll"]["fields"]["work_salary"] = array(
    'name' => 'work_salary',
    'vname' => 'LBL_WORK_SALARY',
    'type' => 'decimal',
    'massupdate' => true,
	'len' => '18',
    'size' => '20',
    'enable_range_search' => false,
    'precision' => '2',
);
$dictionary["pr_payroll"]["fields"]["overtime"] = array(
    'name' => 'overtime',
    'vname' => 'LBL_OVERTIME',
    'type' => 'decimal',
    'massupdate' => true,
	'len' => '18',
    'size' => '20',
    'enable_range_search' => false,
    'precision' => '2',
);
$dictionary["pr_payroll"]["fields"]["overtime_salary"] = array(
    'name' => 'overtime_salary',
    'vname' => 'LBL_OVERTIME_SALARY',
    'type' => 'decimal',
    'massupdate' => true,
	'len' => '18',
    'size' => '20',
    'enable_range_search' => false,
    'precision' => '2',
);
$dictionary["pr_payroll"]["fields"]["workday"] = array(
    'name' => 'workday',
    'vname' => 'LBL_WORKDAY',
    'type' => 'decimal',
    'massupdate' => true,
	'len' => '18',
    'size' => '20',
    'enable_range_search' => false,
    'precision' => '2',
);
$dictionary["pr_payroll"]["fields"]["dayleave"] = array(
    'name' => 'dayleave',
    'vname' => 'LBL_DAYLEAVE',
    'type' => 'decimal',
    'massupdate' => true,
	'len' => '18',
    'size' => '20',
    'enable_range_search' => false,
    'precision' => '2',
);
 ?>