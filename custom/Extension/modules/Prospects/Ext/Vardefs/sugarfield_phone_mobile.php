<?php
 // created: 2019-08-15 12:20:11
$dictionary['Prospect']['fields']['phone_mobile']['len']='10';
$dictionary['Prospect']['fields']['phone_mobile']['audited']=true;
$dictionary['Prospect']['fields']['phone_mobile']['inline_edit']='';
$dictionary['Prospect']['fields']['phone_mobile']['comments']='Mobile phone number of the contact';
$dictionary['Prospect']['fields']['phone_mobile']['merge_filter']='disabled';
$dictionary['Prospect']['fields']['phone_mobile']['unified_search']=false;

 ?>