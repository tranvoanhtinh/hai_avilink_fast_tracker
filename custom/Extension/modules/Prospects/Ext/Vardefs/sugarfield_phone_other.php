<?php
 // created: 2019-08-15 12:20:28
$dictionary['Prospect']['fields']['phone_other']['len']='10';
$dictionary['Prospect']['fields']['phone_other']['audited']=true;
$dictionary['Prospect']['fields']['phone_other']['inline_edit']='';
$dictionary['Prospect']['fields']['phone_other']['comments']='Other phone number for the contact';
$dictionary['Prospect']['fields']['phone_other']['merge_filter']='disabled';
$dictionary['Prospect']['fields']['phone_other']['unified_search']=false;

 ?>