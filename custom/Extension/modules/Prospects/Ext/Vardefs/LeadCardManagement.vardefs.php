<?php

$dictionary["Prospect"]["fields"]["phone_mobile"]['importable'] = true;
$dictionary["Prospect"]["indices"][] = array('name' => 'idx_phone_mobile_del', 'type' => 'index', 'fields' => array('phone_mobile', 'deleted'));