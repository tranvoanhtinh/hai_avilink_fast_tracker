<?php
 // created: 2019-08-15 12:20:00
$dictionary['Prospect']['fields']['phone_home']['len']='10';
$dictionary['Prospect']['fields']['phone_home']['audited']=true;
$dictionary['Prospect']['fields']['phone_home']['inline_edit']='';
$dictionary['Prospect']['fields']['phone_home']['comments']='Home phone number of the contact';
$dictionary['Prospect']['fields']['phone_home']['merge_filter']='disabled';
$dictionary['Prospect']['fields']['phone_home']['unified_search']=false;

 ?>