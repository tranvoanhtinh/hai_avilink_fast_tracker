<?php

$dictionary["User"]["fields"]["user_id"] = array(
    "name" => "user_id",
    "type" => "varchar",
    "vname" => "LBL_EMPLOYEE_ID",
    "audit" => true,
    "required" => false,
);
$dictionary["User"]["fields"]["DiscountPercent"] = array(
'required' => false,
    'name' => 'DiscountPercent',
    'vname' => 'LBL_DISCOUNT_PERCENT',
    'type' => 'int',
    'massupdate' => true,
    'default' => '0',
    'no_default' => false,
    'comments' => '',
    'help' => '',
    'importable' => 'true',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => true,
    'inline_edit' => true,
    'reportable' => true,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'len' => '255',
    'size' => '20',
    'enable_range_search' => false,
    'disable_num_format' => '',
    'min' => 0,
    'max' => false,
    'validation' => 
    array (
      'type' => 'range',
      'min' => 0,
      'max' => false,
    ),
);
$dictionary["User"]["indices"][] = array('name' => 'idx_user_id_del', 'type' => 'index', 'fields' => array('user_id', 'deleted'));

