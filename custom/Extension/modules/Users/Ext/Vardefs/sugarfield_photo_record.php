<?php

$dictionary["User"]["fields"]["photo_record"] = array(
    "name" => "photo_record",
    "type" => "varchar",
    "vname" => "LBL_PHOTO_RECORD",
    "audit" => true,
    "required" => false,
);