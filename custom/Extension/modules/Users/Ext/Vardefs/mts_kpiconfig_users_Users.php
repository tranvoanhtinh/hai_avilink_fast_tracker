<?php
// created: 2018-09-02 09:33:00
$dictionary["User"]["fields"]["mts_kpiconfig_users"] = array (
  'name' => 'mts_kpiconfig_users',
  'type' => 'link',
  'relationship' => 'mts_kpiconfig_users',
  'source' => 'non-db',
  'module' => 'MTS_KPIConfig',
  'bean_name' => false,
  'vname' => 'LBL_MTS_KPICONFIG_USERS_FROM_MTS_KPICONFIG_TITLE',
);
