<?php
 // created: 2022-10-11 09:31:13
$layout_defs["Users"]["subpanel_setup"]['tttt_worldfonepbx_users_1'] = array (
  'order' => 100,
  'module' => 'tttt_WorldfonePBX',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_TTTT_WORLDFONEPBX_USERS_1_FROM_TTTT_WORLDFONEPBX_TITLE',
  'get_subpanel_data' => 'tttt_worldfonepbx_users_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
