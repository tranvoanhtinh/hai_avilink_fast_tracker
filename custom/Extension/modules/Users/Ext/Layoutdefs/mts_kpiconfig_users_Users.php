<?php
 // created: 2018-09-02 09:33:00
$layout_defs["Users"]["subpanel_setup"]['mts_kpiconfig_users'] = array (
  'order' => 100,
  'module' => 'MTS_KPIConfig',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_MTS_KPICONFIG_USERS_FROM_MTS_KPICONFIG_TITLE',
  'get_subpanel_data' => 'mts_kpiconfig_users',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
