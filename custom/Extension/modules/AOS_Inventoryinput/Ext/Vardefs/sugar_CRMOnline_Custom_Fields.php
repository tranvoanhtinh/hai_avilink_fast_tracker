<?php


$dictionary["AOS_Inventoryinput"]["fields"]["delivery_date"] = array(
    'name' => 'delivery_date',
    'vname' => 'LBL_DELIVERY_DATE',
    'type' => 'date',
    'audited' => true,
    'required' => false,
    'enable_range_search' => true,
    'options' => 'date_range_search_dom',
    "massupdate" => false,
);
$dictionary["AOS_Inventoryinput"]["fields"]["estimated_delivery_date"] = array(
    'name' => 'estimated_delivery_date',
    'vname' => 'LBL_ESTIMATED_DELIVERY_DATE',
    'type' => 'date',
    'audited' => true,
    'required' => false,
    'enable_range_search' => true,
    'options' => 'date_range_search_dom',
    "massupdate" => false,
);
$dictionary["AOS_Inventoryinput"]["fields"]["start_date"] = array(
    'name' => 'start_date',
    'vname' => 'LBL_START_DATE',
    'type' => 'datetimecombo',
    'dbType' => 'datetime',
    'audited' => true,
    'required' => false,
    'enable_range_search' => true,
    'options' => 'date_range_search_dom',
    "massupdate" => false,
);
$dictionary["AOS_Inventoryinput"]["fields"]["ads"] = array(
    'name' => 'ads',
    'vname' => 'LBL_ADS',
    'type' => 'varchar',
    "len" => 255,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);
$dictionary["AOS_Inventoryinput"]["fields"]["odn"] = array(
    'name' => 'odn',
    'vname' => 'LBL_ODN',
    'type' => 'varchar',
    "len" => 255,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);
$dictionary["AOS_Inventoryinput"]["fields"]["customer_ref"] = array(
    'name' => 'customer_ref',
    'vname' => 'LBL_CUSTOMER_REF',
    'type' => 'varchar',
    "len" => 255,
    "importable" => true,
    "audited" => true,
    "massupdate" => false,
);
$dictionary["AOS_Inventoryinput"]["fields"]["invoiceno"] = array(
    'name' => 'invoiceno',
    'vname' => 'LBL_INVOICE_NO',
    'type' => 'varchar',
    "len" => 255,
    "importable" => true,
    "audited" => true,
    "massupdate" => false,
);
$dictionary["AOS_Inventoryinput"]["fields"]["key_invoiceref"] = array(
    'name' => 'key_invoiceref',
    'vname' => 'LBL_KEY_INVOICE_REF',
    'type' => 'varchar',
    "len" => 255,
    "importable" => true,
    "audited" => true,
    "massupdate" => false,
);
$dictionary["AOS_Inventoryinput"]["fields"]["mobileref"] = array(
    'name' => 'mobileref',
    'vname' => 'LBL_MOBILE_REF',
    'type' => 'varchar',
    "len" => 255,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);
$dictionary["AOS_Inventoryinput"]["fields"]["payment_date"] = array(
    'name' => 'payment_date',
    'vname' => 'LBL_PAYMENT_DATE',
    'type' => 'date',
    'audited' => true,
    'required' => false,
    'enable_range_search' => true,
    'options' => 'date_range_search_dom',
    "massupdate" => false,
);
$dictionary["AOS_Inventoryinput"]["fields"]["pi_effective_date"] = array(
    'name' => 'pi_effective_date',
    'vname' => 'LBL_PI_EFFECTIVE_DATE',
    'type' => 'date',
    'audited' => true,
    'required' => false,
    'enable_range_search' => true,
    'options' => 'date_range_search_dom',
    "massupdate" => false,
);


$dictionary["AOS_Inventoryinput"]["fields"]["is_invoice"] = array(
    'name' => 'is_invoice',
    'vname' => 'LBL_ISINVOICE',
    'type' => 'bool',
    'duplicate_merge' => 'enabled',
    'required' => false,
    'inline_edit' => '',
    'merge_filter' => 'disabled',
    'audited' => true,
    'default' => '0',
);

$dictionary["AOS_Inventoryinput"]["fields"]["is_commercial"] = array(
    'name' => 'is_commercial',
    'vname' => 'LBL_IS_COMMERCIAL',
    'type' => 'bool',
    'duplicate_merge' => 'enabled',
    'required' => false,
    'inline_edit' => '',
    'merge_filter' => 'disabled',
    'audited' => false,
    'default' => '0',
    'duplicate_merge_dom_value' => '1',
);

$dictionary["AOS_Inventoryinput"]["fields"]["is_non_commercial"] = array(
    'name' => 'is_non_commercial',
    'vname' => 'LBL_IS_NON_COMMERCIAL',
    'type' => 'bool',
    'duplicate_merge' => 'enabled',
    'required' => false,
    'inline_edit' => '',
    'merge_filter' => 'disabled',
    'audited' => false,
    'default' => '0',
    'duplicate_merge_dom_value' => '1',
);

$dictionary["AOS_Inventoryinput"]["fields"]["paymentmethod"] = array(
    'name' => 'paymentmethod',
    'vname' => 'LBL_PAYMENTMETHOD',
    'len' => '100',
    'type' => 'enum',
    'audited' => true,
    'inline_edit' => true,
    'massupdate' => '1',
    'options' => 'paymentmethod_list',
    'merge_filter' => 'disabled',
    'studio' => true,
    'default' => '9',
);

$dictionary["AOS_Inventoryinput"]["fields"]["shipping_method"] = array(
    'name' => 'shipping_method',
    'vname' => 'LBL_SHIPPING_METHOD',
    'len' => '100',
    'type' => 'enum',
    'audited' => true,
    'inline_edit' => true,
    'massupdate' => '1',
    'options' => 'shipping_method_list',
    'merge_filter' => 'disabled',
);

$dictionary["AOS_Inventoryinput"]["fields"]["statustransport"] = array(
    'name' => 'statustransport',
    'vname' => 'LBL_STATUSTRANSPORT',
    'len' => '100',
    'type' => 'enum',
    'audited' => true,
    'inline_edit' => true,
    'massupdate' => '1',
    'options' => 'statustransport_list',
    'merge_filter' => 'disabled',
);

$dictionary["AOS_Inventoryinput"]["fields"]["summary_product"] = array(
    'name' => 'summary_product',
    'vname' => 'LBL_SUMMARY_PRODUCTS',
    'type' => 'text',
    'full_text_search' => 1,
    'inline_edit' => true,
    'merge_filter' => 'disabled',
);

$dictionary["AOS_Inventoryinput"]["fields"]["commisonservice"] = array(
    'name' => 'commisonservice',
    'vname' => 'LBL_COMMISONSERVICE',
    'type' => 'text',
    'full_text_search' => 1,
    'inline_edit' => true,
    'merge_filter' => 'disabled',
);
$dictionary["AOS_Inventoryinput"]["fields"]["take_from"] = array(
    'name' => 'take_from',
    'vname' => 'LBL_TAKE_FROM',
    'len' => '100',
    'type' => 'enum',
    'audited' => true,
    'inline_edit' => true,
    'massupdate' => '1',
    'required'=> true,
    'options' => 'take_from_list',
    'merge_filter' => 'disabled',
);

$dictionary["AOS_Inventoryinput"]["fields"]["cashier"] = array(
    'name' => 'cashier',
    'vname' => 'LBL_CASHIER',
    'len' => '100',
    'type' => 'enum',
    'audited' => true,
    'inline_edit' => true,
    'massupdate' => '1',
    'options' => 'cashier_list',
    'merge_filter' => 'disabled',
);
$dictionary["AOS_Inventoryinput"]["fields"]["shipingfeeavg"] = array(
    'name' => 'shipingfeeavg',
    'vname' => 'LBL_SHIPPING_FEE_AVG',
    'type' => 'currency',
    'dbType' => 'double',
    'duplicate_merge' => 'enabled',
    'required' => false,
    'options' => 'numeric_range_search_dom',
    'enable_range_search' => true,
    'inline_edit' => '',
    'duplicate_merge_dom_value' => '1',
    'merge_filter' => 'disabled',
    'audited' => true,
);
$dictionary["AOS_Inventoryinput"]["fields"]["trade_term"] = array(
    'name' => 'trade_term',
    'vname' => 'LBL_TRADE_TERM',
    'len' => '100',
    'type' => 'enum',
    'audited' => true,
    'inline_edit' => true,
    'massupdate' => '1',
    'options' => 'trade_term_list',
    'merge_filter' => 'disabled',
);
$dictionary["AOS_Inventoryinput"]["fields"]["type_invoice"] = array(
    'name' => 'type_invoice',
    'vname' => 'LBL_TYPE_INVOICE',
    'len' => '100',
    'type' => 'enum',
    'audited' => true,
    'inline_edit' => true,
    'massupdate' => '1',
    'options' => 'Type_Invoice_list',
    'merge_filter' => 'disabled',
    'studio' => true,
);


$dictionary["AOS_Inventoryinput"]["fields"]["total_cbm"] = array(
    'name' => 'total_cbm',
    'vname' => 'LBL_TOTAL_CBM',
    'type' => 'currency',
    'massupdate' => 0,
    'comments' => '',
    'help' => '',
    'importable' => 'true',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => 1,
    'reportable' => true,
    'len' => '26,6',
);
$dictionary["AOS_Inventoryinput"]["fields"]["total_ctns"] = array(
    'name' => 'total_ctns',
    'vname' => 'LBL_TOTAL_CTNS',
    'type' => 'currency',
    'massupdate' => 0,
    'comments' => '',
    'help' => '',
    'importable' => 'true',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => 1,
    'reportable' => true,
    'len' => '26,6',
);

$dictionary["AOS_Inventoryinput"]["fields"]["lp_total_amt"] = array(
    'required' => false,
    'name' => 'lp_total_amt',
    'vname' => 'LBL_LP_TOTAL_AMT',
    'type' => 'currency',
    'massupdate' => 0,
    'comments' => '',
    'help' => '',
    'importable' => 'true',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => 1,
    'reportable' => true,
    'len' => '26,6',
);


$dictionary["AOS_Inventoryinput"]["fields"]["lp_subtotal_amount"] =
    array(
        'required' => false,
        'name' => 'lp_subtotal_amount',
        'vname' => 'LBL_LP_SUBTOTAL_AMOUNT',
        'type' => 'currency',
        'massupdate' => 0,
        'comments' => '',
        'help' => '',
        'importable' => 'true',
        'duplicate_merge' => 'disabled',
        'duplicate_merge_dom_value' => '0',
        'audited' => 1,
        'reportable' => true,
        'len' => '26,6',
    );

$dictionary["AOS_Inventoryinput"]["fields"]["lp_total_amount"] =
    array(
        'required' => false,
        'name' => 'lp_total_amount',
        'vname' => 'LBL_LP_TOTAL_AMOUNT',
        'type' => 'currency',
        'massupdate' => 0,
        'comments' => '',
        'help' => '',
        'importable' => 'true',
        'duplicate_merge' => 'disabled',
        'duplicate_merge_dom_value' => '0',
        'audited' => false,
        'reportable' => true,
        'len' => '26,6',
        'enable_range_search' => true,
        'options' => 'numeric_range_search_dom',
    );
$dictionary["AOS_Inventoryinput"]["fields"]["lp_discount_amount"] =
    array(
        'required' => false,
        'name' => 'lp_discount_amount',
        'vname' => 'LBL_LP_DISCOUNT_AMOUNT',
        'type' => 'currency',
        'massupdate' => 0,
        'comments' => '',
        'help' => '',
        'importable' => 'true',
        'duplicate_merge' => 'disabled',
        'duplicate_merge_dom_value' => '0',
        'audited' => false,
        'reportable' => true,
        'len' => '26,6',
        'enable_range_search' => true,
        'options' => 'numeric_range_search_dom',
    );
$dictionary["AOS_Inventoryinput"]["fields"]["lp_shipping_amount"] =
    array(
        'required' => false,
        'name' => 'lp_shipping_amount',
        'vname' => 'LBL_LP_SHIPPING_AMOUNT',
        'type' => 'currency',
        'massupdate' => 0,
        'comments' => '',
        'help' => '',
        'importable' => 'true',
        'duplicate_merge' => 'disabled',
        'duplicate_merge_dom_value' => '0',
        'audited' => false,
        'reportable' => true,
        'len' => '26,6',
        'enable_range_search' => true,
        'options' => 'numeric_range_search_dom',
    );
$dictionary["AOS_Inventoryinput"]["fields"]["lp_shipping_tax_amount"] =
    array(
        'required' => false,
        'name' => 'lp_shipping_tax_amount',
        'vname' => 'LBL_LP_SHIPPING_TAX_AMOUNT',
        'type' => 'currency',
        'massupdate' => 0,
        'comments' => '',
        'help' => '',
        'importable' => 'true',
        'duplicate_merge' => 'disabled',
        'duplicate_merge_dom_value' => '0',
        'audited' => false,
        'reportable' => true,
        'len' => '26,6',
        'enable_range_search' => true,
        'options' => 'numeric_range_search_dom',
    );
$dictionary["AOS_Inventoryinput"]["fields"]["lp_tax_amount"] =
    array(
        'required' => false,
        'name' => 'lp_tax_amount',
        'vname' => 'LBL_LP_TAX_AMOUNT',
        'type' => 'currency',
        'massupdate' => 0,
        'comments' => '',
        'help' => '',
        'importable' => 'true',
        'duplicate_merge' => 'disabled',
        'duplicate_merge_dom_value' => '0',
        'audited' => false,
        'reportable' => true,
        'len' => '26,6',
        'enable_range_search' => true,
        'options' => 'numeric_range_search_dom',
    );

$dictionary["AOS_Inventoryinput"]["fields"]["status_inventory"] = array(
    'name' => 'status_inventory',
    'vname' => 'LBL_STATUS_INVENTORY',
    'len' => '100',
    'type' => 'enum',
    'audited' => true,
    'inline_edit' => true,
    'massupdate' => '1',
    'options' => 'inventory_status_list',
    'merge_filter' => 'disabled',
  

);


$dictionary["AOS_Inventoryinput"]["fields"]["voucherdate"] = array(
    'name' => 'voucherdate',
    'vname' => 'LBL_VOUCHER_DATE',
    'type' => 'date',
    'audited' => true,
    'required' => false,
    'enable_range_search' => true,
    'options' => 'date_range_search_dom',
    "massupdate" => false,
    'display_default' => 'now',
);
$dictionary["AOS_Inventoryinput"]["fields"]["transfer_id"] = array(
    'name' => 'transfer_id',
    'vname' => 'LBL_TRANSFER_ID',
    'type' => 'varchar',
    "len" => 36,
    "importable" => true,
    "audited" => true,
    "massupdate" => false,
);
$dictionary["AOS_Inventoryinput"]["fields"]["transfer"] = array(
    'name' => 'transfer',
    'vname' => 'LBL_TRANSFER',
    'type' => 'varchar',
    "len" => 10,
    "importable" => true,
    "audited" => true,
    "massupdate" => false,
);


$dictionary["AOS_Inventoryinput"]["fields"]["transfer_from"] = array(
    'name' => 'transfer_from',
    'vname' => 'LBL_TRANSFER_FROM',
    'len' => '100',
    'type' => 'enum',
    'audited' => true,
    'inline_edit' => true,
    'massupdate' => '1',
    'required'=> true,
    'options' => 'take_from_list',
    'merge_filter' => 'disabled',
);


$dictionary["AOS_Inventoryinput"]["fields"]["aos_invoice_id"] = array(

    'name' => 'aos_invoice_id',
    'vname' => 'LBL_AOS_INVOICE_ID',
    'type' => 'id',
    'massupdate' => '0',
    'default' => NULL,
    'no_default' => false,
    'comments' => '',
    'help' => '',
    'importable' => 'true',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => false,
    'reportable' => false,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'len' => '36',
    'size' => '20',
);

$dictionary["AOS_Inventoryinput"]["fields"]["invoice"] = array(
    'source'=>'non-db',
    'name' => 'invoice',
    'vname' => 'LBL_INVOICE',
    'type' => 'relate',
    'massupdate' => '0',
    'default' => NULL,
    'no_default' => false,
    'comments' => '',
    'help' => '',
    'importable' => 'true',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => false,
    'reportable' => true,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'len' => '255',
    'size' => '20',
    'id_name' => 'aos_invoice_id',
    'ext2' => 'AOS_Invoices',
    'module' => 'AOS_Invoices',
    'rname' => 'name',
    'quicksearch' => 'enabled',
    'studio' => 'visible',
);  


$dictionary["AOS_Inventoryinput"]["fields"]["aos_inventoryoutput_id"] = array(

      'name' => 'aos_inventoryoutput_id',
      'vname' => 'LBL_INVENTORY_OUT_ID',
      'type' => 'id',
      'massupdate' => '0',
      'default' => NULL,
      'no_default' => false,
      'comments' => '',
      'help' => '',
      'importable' => 'true',
      'duplicate_merge' => 'disabled',
      'duplicate_merge_dom_value' => '0',
      'audited' => false,
      'reportable' => false,
      'unified_search' => false,
      'merge_filter' => 'disabled',
      'len' => '36',
      'size' => '20',
    );

$dictionary["AOS_Inventoryinput"]["fields"]["inventory_out"] = array(
    'source'=>'non-db',
    'name' => 'inventory_out',
    'vname' => 'LBL_INVENTORY_OUT',
    'type' => 'relate',
    'massupdate' => '0',
    'default' => NULL,
    'no_default' => false,
    'comments' => '',
    'help' => '',
    'importable' => 'true',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => false,
    'reportable' => true,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'len' => '255',
    'size' => '20',
    'id_name' => 'aos_inventoryoutput_id',
    'ext2' => 'AOS_Inventoryoutput',
    'module' => 'AOS_Inventoryoutput',
    'rname' => 'name',
    'quicksearch' => 'enabled',
    'studio' => 'visible',
);  

$dictionary["AOS_Inventoryinput"]["fields"]["total_discount_amount"] = array(
       'required' => false,
                'name' => 'total_discount_amount',
                'vname' => 'LBL_TOTAL_DISCOUNT_AMOUNT',
                'type' => 'currency',
                'massupdate' => 0,
                'comments' => '',
                'help' => '',
                'importable' => 'true',
                'duplicate_merge' => 'disabled',
                'duplicate_merge_dom_value' => '0',
                'audited' => false,
                'reportable' => true,
                'len' => '26,6',
                'enable_range_search' => true,
                'options' => 'numeric_range_search_dom',
            );


$dictionary["AOS_Inventoryinput"]["fields"]["remaining_money"] = array(
       'required' => false,
                'name' => 'remaining_money',
                'vname' => 'LBL_REMAINING_MONEY',
                'type' => 'currency',
                'massupdate' => 0,
                'comments' => '',
                'help' => '',
                'importable' => 'true',
                'duplicate_merge' => 'disabled',
                'duplicate_merge_dom_value' => '0',
                'audited' => false,
                'reportable' => true,
                'len' => '26,6',
                'enable_range_search' => true,
                'options' => 'numeric_range_search_dom',
            );
$dictionary["AOS_Inventoryinput"]["fields"]["advance_payment"] = array(
       'required' => false,
                'name' => 'advance_payment',
                'vname' => 'LBL_ADVANCE_PAYMENT',
                'type' => 'currency',
                'massupdate' => 0,
                'comments' => '',
                'help' => '',
                'importable' => 'true',
                'duplicate_merge' => 'disabled',
                'duplicate_merge_dom_value' => '0',
                'audited' => false,
                'reportable' => true,
                'len' => '26,6',
                'enable_range_search' => true,
                'options' => 'numeric_range_search_dom',
            );
$dictionary["AOS_Inventoryinput"]["fields"]["discount_percent"] = array(
       'required' => false,
                'name' => 'discount_percent',
                'vname' => 'LBL_DISCOUNT_PERCENT',
                'name' => 'discount_percent',
                'len' => '100',
                'type' => 'enum',
                'audited' => true,
                'inline_edit' => true,
                'massupdate' => '1',
                'options' => 'discount_percent_list',
                'merge_filter' => 'disabled',
            );



