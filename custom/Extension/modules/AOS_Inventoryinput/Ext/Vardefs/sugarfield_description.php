<?php
 // created: 2024-03-20 02:35:22
$dictionary['AOS_Inventoryinput']['fields']['description']['inline_edit']=true;
$dictionary['AOS_Inventoryinput']['fields']['description']['comments']='Full text of the note';
$dictionary['AOS_Inventoryinput']['fields']['description']['merge_filter']='disabled';
$dictionary['AOS_Inventoryinput']['fields']['description']['rows']='4';
$dictionary['AOS_Inventoryinput']['fields']['description']['cols']='60';

 ?>