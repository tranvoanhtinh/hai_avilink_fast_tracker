<?php
 // created: 2024-04-11 10:49:48
$dictionary['AOS_Inventoryinput']['fields']['name']['required']=false;
$dictionary['AOS_Inventoryinput']['fields']['name']['inline_edit']=true;
$dictionary['AOS_Inventoryinput']['fields']['name']['duplicate_merge']='disabled';
$dictionary['AOS_Inventoryinput']['fields']['name']['duplicate_merge_dom_value']='0';
$dictionary['AOS_Inventoryinput']['fields']['name']['merge_filter']='disabled';
$dictionary['AOS_Inventoryinput']['fields']['name']['unified_search']=false;

 ?>