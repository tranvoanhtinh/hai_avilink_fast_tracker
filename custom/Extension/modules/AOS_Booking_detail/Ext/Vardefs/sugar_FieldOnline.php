<?php

$dictionary["AOS_Booking_detail"]["fields"]["booking_date"] = array(
    'name' => 'booking_date',
    'vname' => 'LBL_BOOKING_DATE',
    'type' => 'date',
    'audited' => true,
    'required' => false,
    "massupdate" => false,
);
$dictionary["AOS_Booking_detail"]["fields"]["invoice_no"] = array(
    'name' => 'invoice_no',
    'vname' => 'LBL_INVOICE_NO',
    'type' => 'varchar',
    "len" => 250,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);

$dictionary["AOS_Booking_detail"]["fields"]["date_std_sta"] = array(
    'name' => 'date_std_sta',
    'vname' => 'LBL_DATE_STD_STA',
    'type' => 'datetimecombo',
    'massupdate' => '0',
    'default' => '',
    'no_default' => false,
    'comments' => '',
    'help' => '',
    'importable' => 'true',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => false,
    'reportable' => true,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'size' => '20',
    'enable_range_search' => false,
    'dbType' => 'datetime',
    'display_default' => 'now&12:00am',
);

$dictionary["AOS_Booking_detail"]["fields"]["airport_from"] = array(
    'name' => 'airport_from',
    'vname' => 'LBL_AIRPORT_FROM',
    'type' => 'varchar',
    "len" => 20,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,

);
$dictionary["AOS_Booking_detail"]["fields"]["airport_to"] = array(
    'name' => 'airport_to',
    'vname' => 'LBL_AIRPORT_TO',
    'type' => 'varchar',
    "len" => 20,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,

);

$dictionary["AOS_Booking_detail"]["fields"]["flight_name"] = array(
    'name' => 'flight_name',
    'vname' => 'LBL_FLIGHT_NAME',
    'type' => 'varchar',
    "len" => 255,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);
$dictionary["AOS_Booking_detail"]["fields"]["pnr"] = array(
    'name' => 'pnr',
    'vname' => 'LBL_PNR',
    'type' => 'varchar',
    "len" => 50,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);

$dictionary["AOS_Booking_detail"]["fields"]["nationality"] = array(
    'name' => 'nationality',
    'vname' => 'LBL_NATIONALITY',
    'type' => 'varchar',
    "len" => 50,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,

);

$dictionary["AOS_Booking_detail"]["fields"]["service"] = array(
    'name' => 'service',
    'vname' => 'LBL_SERVICE',
    'type' => 'text',
    'full_text_search' => 1,
    'inline_edit' => true,
    'merge_filter' => 'disabled',
);

$dictionary["AOS_Booking_detail"]["fields"]["booking_id"] = array(
    'name' => 'booking_id',
    'vname' => 'LBL_BOOKING_ID',
    'type' => 'varchar',
    "len" => 50,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);

$dictionary["AOS_Booking_detail"]["fields"]["booking_no"] = array(
    'name' => 'booking_no',
    'vname' => 'LBL_BOOKING_NO',
    'type' => 'int',
    "len" => 50,
);
$dictionary["AOS_Booking_detail"]["fields"]["passenger_no"] = array(
    'name' => 'passenger_no',
    'vname' => 'LBL_PASSENGER_NO',
    'type' => 'int',
    "len" => 50,
);
$dictionary["AOS_Booking_detail"]["fields"]["passenger_id"] = array(
    'name' => 'passenger_id',
    'vname' => 'LBL_PASSENGER_ID',
    'type' => 'varchar',
    "len" => 50,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,

);
$dictionary["AOS_Booking_detail"]["fields"]["passport"] = array(
    'name' => 'passport',
    'vname' => 'LBL_PASSPORT',
    'type' => 'varchar',
    "len" => 255,
    "importable" => true,
    "audited" => true,
    "massupdate" => false,
);