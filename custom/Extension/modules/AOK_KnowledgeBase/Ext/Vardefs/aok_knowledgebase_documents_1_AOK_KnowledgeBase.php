<?php
// created: 2020-07-06 06:44:45
$dictionary["AOK_KnowledgeBase"]["fields"]["aok_knowledgebase_documents_1"] = array (
  'name' => 'aok_knowledgebase_documents_1',
  'type' => 'link',
  'relationship' => 'aok_knowledgebase_documents_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'vname' => 'LBL_AOK_KNOWLEDGEBASE_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
  'id_name' => 'aok_knowledgebase_documents_1documents_idb',
);
$dictionary["AOK_KnowledgeBase"]["fields"]["aok_knowledgebase_documents_1_name"] = array (
  'name' => 'aok_knowledgebase_documents_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_AOK_KNOWLEDGEBASE_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
  'save' => true,
  'id_name' => 'aok_knowledgebase_documents_1documents_idb',
  'link' => 'aok_knowledgebase_documents_1',
  'table' => 'documents',
  'module' => 'Documents',
  'rname' => 'document_name',
);
$dictionary["AOK_KnowledgeBase"]["fields"]["aok_knowledgebase_documents_1documents_idb"] = array (
  'name' => 'aok_knowledgebase_documents_1documents_idb',
  'type' => 'link',
  'relationship' => 'aok_knowledgebase_documents_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'left',
  'vname' => 'LBL_AOK_KNOWLEDGEBASE_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',

);
$dictionary["AOK_KnowledgeBase"]["fields"]["filename"] =
            array(
                'name' => 'filename',
    'vname' => 'LBL_FILENAME',
    'type' => 'file',
    'dbType' => 'varchar',
    'len' => '255',
    'reportable'=>true,
    'comment' => 'File name associated with the note (attachment)',
    'importable' => false,
            );
 $dictionary["AOK_KnowledgeBase"]["fields"]["file_mime_type"] = array(
    'name' => 'file_mime_type',
    'vname' => 'LBL_FILE_MIME_TYPE',
    'type' => 'varchar',
    'len' => '100',
    'comment' => 'Attachment MIME type',
    'importable' => false,
  );
  $dictionary["AOK_KnowledgeBase"]["fields"]["file_url"] = array(
    'name'=>'file_url',
    'vname' => 'LBL_FILE_URL',
    'type'=>'function',
    'function_class'=>'UploadFile',
    'function_name'=>'get_upload_url',
    'function_params'=> array('$this'),
    'source'=>'function',
    'reportable'=>false,
    'comment' => 'Path to file (can be URL)',
    'importable' => false,
    );