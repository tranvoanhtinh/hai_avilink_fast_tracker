<?php
 // created: 2023-05-16 06:14:03
$layout_defs["AOS_Invoices"]["subpanel_setup"]['aos_invoices_cs_cususer_1'] = array (
  'order' => 100,
  'module' => 'cs_cusUser',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_AOS_INVOICES_CS_CUSUSER_1_FROM_CS_CUSUSER_TITLE',
  'get_subpanel_data' => 'aos_invoices_cs_cususer_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
