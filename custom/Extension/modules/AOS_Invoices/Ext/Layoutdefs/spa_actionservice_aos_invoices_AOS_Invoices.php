<?php
 // created: 2023-05-23 08:57:15
$layout_defs["AOS_Invoices"]["subpanel_setup"]['spa_actionservice_aos_invoices'] = array (
  'order' => 100,
  'module' => 'spa_ActionService',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SPA_ACTIONSERVICE_AOS_INVOICES_FROM_SPA_ACTIONSERVICE_TITLE',
  'get_subpanel_data' => 'spa_actionservice_aos_invoices',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
