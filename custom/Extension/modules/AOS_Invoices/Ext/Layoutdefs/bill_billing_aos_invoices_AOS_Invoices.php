<?php
 // created: 2021-08-18 06:32:45
$layout_defs["AOS_Invoices"]["subpanel_setup"]['bill_billing_aos_invoices'] = array (
  'order' => 100,
  'module' => 'bill_Billing',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_BILL_BILLING_AOS_INVOICES_FROM_BILL_BILLING_TITLE',
  'get_subpanel_data' => 'bill_billing_aos_invoices',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
