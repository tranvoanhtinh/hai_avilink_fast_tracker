<?php
 $dictionary["AOS_Invoices"]["fields"]["start_date"] = array(
    'name' => 'start_date',
    'vname' => 'LBL_START_DATE',
     'type' => 'datetimecombo',
	'dbType' => 'datetime',
    'audited' => true,
    'required' => false,
    'enable_range_search' => true,
    'options' => 'date_range_search_dom',
	 "massupdate" => false,
);
 ?>