<?php
//Tiền chiết khấu trên tổng đơn hàng
$dictionary["AOS_Invoices"]["fields"]["total_discount_amount"] =
            array(
       'required' => false,
                'name' => 'total_discount_amount',
                'vname' => 'LBL_TOTAL_DISCOUNT_AMOUNT',
                'type' => 'currency',
                'massupdate' => 0,
                'comments' => '',
                'help' => '',
                'importable' => 'true',
                'duplicate_merge' => 'disabled',
                'duplicate_merge_dom_value' => '0',
                'audited' => false,
                'reportable' => true,
                'len' => '26,6',
                'enable_range_search' => true,
                'options' => 'numeric_range_search_dom',
            );