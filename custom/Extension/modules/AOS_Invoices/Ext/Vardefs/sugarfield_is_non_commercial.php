<?php
 // created: 2022-07-16 09:26:01
$dictionary['AOS_Invoices']['fields']['is_non_commercial']['name']='is_non_commercial';
$dictionary['AOS_Invoices']['fields']['is_non_commercial']['vname']='LBL_IS_NON_COMMERCIAL';
$dictionary['AOS_Invoices']['fields']['is_non_commercial']['type']='bool';
$dictionary['AOS_Invoices']['fields']['is_non_commercial']['duplicate_merge']='enabled';
$dictionary['AOS_Invoices']['fields']['is_non_commercial']['required']=false;
$dictionary['AOS_Invoices']['fields']['is_non_commercial']['inline_edit']='';
$dictionary['AOS_Invoices']['fields']['is_non_commercial']['merge_filter']='disabled';
$dictionary['AOS_Invoices']['fields']['is_non_commercial']['audited']=false;
$dictionary['AOS_Invoices']['fields']['is_non_commercial']['default']='0';
$dictionary['AOS_Invoices']['fields']['is_non_commercial']['duplicate_merge_dom_value']='1';

 ?>