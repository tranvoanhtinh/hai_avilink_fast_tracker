<?php
 $dictionary["AOS_Invoices"]["fields"]["mobileref"] = array(
    'name' => 'mobileref',
    'vname' => 'LBL_MOBILE_REF',
    'type' => 'varchar',
    "len" => 255,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);
 ?>