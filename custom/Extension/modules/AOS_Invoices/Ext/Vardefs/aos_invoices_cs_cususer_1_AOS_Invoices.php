<?php
// created: 2023-05-16 06:14:03
$dictionary["AOS_Invoices"]["fields"]["aos_invoices_cs_cususer_1"] = array (
  'name' => 'aos_invoices_cs_cususer_1',
  'type' => 'link',
  'relationship' => 'aos_invoices_cs_cususer_1',
  'source' => 'non-db',
  'module' => 'cs_cusUser',
  'bean_name' => 'cs_cusUser',
  'side' => 'right',
  'vname' => 'LBL_AOS_INVOICES_CS_CUSUSER_1_FROM_CS_CUSUSER_TITLE',
);
