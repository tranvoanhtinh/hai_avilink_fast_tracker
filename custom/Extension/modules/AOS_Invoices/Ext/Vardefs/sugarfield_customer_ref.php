<?php
 $dictionary["AOS_Invoices"]["fields"]["customer_ref"] = array(
    'name' => 'customer_ref',
    'vname' => 'LBL_CUSTOMER_REF',
    'type' => 'varchar',
    "len" => 255,
    "importable" => true,
    "audited" => true,
    "massupdate" => false,
);
 ?>