<?php
 // created: 2024-03-06 11:02:41
$dictionary['AOS_Invoices']['fields']['name']['required']=false;
$dictionary['AOS_Invoices']['fields']['name']['inline_edit']=true;
$dictionary['AOS_Invoices']['fields']['name']['duplicate_merge']='disabled';
$dictionary['AOS_Invoices']['fields']['name']['duplicate_merge_dom_value']='0';
$dictionary['AOS_Invoices']['fields']['name']['merge_filter']='disabled';
$dictionary['AOS_Invoices']['fields']['name']['unified_search']=false;

 ?>