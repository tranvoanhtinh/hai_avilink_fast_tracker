<?php
 // created: 2021-09-09 06:01:28
$dictionary['AOS_Invoices']['fields']['receive_amount']['name']='receive_amount';
$dictionary['AOS_Invoices']['fields']['receive_amount']['vname']='LBL_RECEIVE_AMOUNT';
$dictionary['AOS_Invoices']['fields']['receive_amount']['type']='currency';
$dictionary['AOS_Invoices']['fields']['receive_amount']['dbType']='double';
$dictionary['AOS_Invoices']['fields']['receive_amount']['duplicate_merge']='enabled';
$dictionary['AOS_Invoices']['fields']['receive_amount']['required']=false;
$dictionary['AOS_Invoices']['fields']['receive_amount']['options']='numeric_range_search_dom';
$dictionary['AOS_Invoices']['fields']['receive_amount']['enable_range_search']=true;
$dictionary['AOS_Invoices']['fields']['receive_amount']['inline_edit']='';
$dictionary['AOS_Invoices']['fields']['receive_amount']['duplicate_merge_dom_value']='1';
$dictionary['AOS_Invoices']['fields']['receive_amount']['merge_filter']='disabled';
$dictionary['AOS_Invoices']['fields']['receive_amount']['audited']=true;

 ?>