<?php
 $dictionary["AOS_Invoices"]["fields"]["ads"] = array(
    'name' => 'ads',
    'vname' => 'LBL_ADS',
    'type' => 'varchar',
    "len" => 255,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);
 ?>