<?php
 // created: 2021-09-09 06:01:28
$dictionary['AOS_Invoices']['fields']['return_amount']['name']='return_amount';
$dictionary['AOS_Invoices']['fields']['return_amount']['vname']='LBL_RETURN_AMOUNT';
$dictionary['AOS_Invoices']['fields']['return_amount']['type']='currency';
$dictionary['AOS_Invoices']['fields']['return_amount']['dbType']='double';
$dictionary['AOS_Invoices']['fields']['return_amount']['duplicate_merge']='enabled';
$dictionary['AOS_Invoices']['fields']['return_amount']['required']=false;
$dictionary['AOS_Invoices']['fields']['return_amount']['options']='numeric_range_search_dom';
$dictionary['AOS_Invoices']['fields']['return_amount']['enable_range_search']=true;
$dictionary['AOS_Invoices']['fields']['return_amount']['inline_edit']='';
$dictionary['AOS_Invoices']['fields']['return_amount']['duplicate_merge_dom_value']='1';
$dictionary['AOS_Invoices']['fields']['return_amount']['merge_filter']='disabled';
$dictionary['AOS_Invoices']['fields']['return_amount']['audited']=true;

 ?>