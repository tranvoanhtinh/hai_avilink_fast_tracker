<?php
 // created: 2024-04-17 04:34:09
$dictionary['AOS_Invoices']['fields']['discount_amount']['audited']=false;
$dictionary['AOS_Invoices']['fields']['discount_amount']['inline_edit']=true;
$dictionary['AOS_Invoices']['fields']['discount_amount']['merge_filter']='disabled';
$dictionary['AOS_Invoices']['fields']['discount_amount']['enable_range_search']=false;

 ?>