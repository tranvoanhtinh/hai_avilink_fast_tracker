<?php
//tiền còn lại

$dictionary["AOS_Invoices"]["fields"]["remaining_money"] =
            array(
       'required' => false,
                'name' => 'remaining_money',
                'vname' => 'LBL_REMAINING_MONEY',
                'type' => 'currency',
                'massupdate' => 0,
                'comments' => '',
                'help' => '',
                'importable' => 'true',
                'duplicate_merge' => 'disabled',
                'duplicate_merge_dom_value' => '0',
                'audited' => false,
                'reportable' => true,
                'len' => '26,6',
                'enable_range_search' => true,
                'options' => 'numeric_range_search_dom',
            );
