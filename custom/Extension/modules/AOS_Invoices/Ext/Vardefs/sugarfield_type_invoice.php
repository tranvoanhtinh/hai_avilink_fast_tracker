<?php
 // created: 2023-06-09 09:56:58
$dictionary['AOS_Invoices']['fields']['type_invoice']['name']='type_invoice';
$dictionary['AOS_Invoices']['fields']['type_invoice']['vname']='LBL_TYPE_INVOICE';
$dictionary['AOS_Invoices']['fields']['type_invoice']['len']='100';
$dictionary['AOS_Invoices']['fields']['type_invoice']['type']='enum';
$dictionary['AOS_Invoices']['fields']['type_invoice']['audited']=true;
$dictionary['AOS_Invoices']['fields']['type_invoice']['inline_edit']=true;
$dictionary['AOS_Invoices']['fields']['type_invoice']['massupdate']='1';
$dictionary['AOS_Invoices']['fields']['type_invoice']['options']='Type_Invoice_list';
$dictionary['AOS_Invoices']['fields']['type_invoice']['merge_filter']='disabled';
$dictionary['AOS_Invoices']['fields']['type_invoice']['studio']=true;

 ?>