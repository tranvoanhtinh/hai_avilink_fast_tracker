<?php
 $dictionary["AOS_Invoices"]["fields"]["pi_effective_date"] = array(
    'name' => 'pi_effective_date',
    'vname' => 'LBL_PI_EFFECTIVE_DATE',
    'type' => 'date',
    'audited' => true,
    'required' => false,
    'enable_range_search' => true,
    'options' => 'date_range_search_dom',
	 "massupdate" => false,
);
 ?>