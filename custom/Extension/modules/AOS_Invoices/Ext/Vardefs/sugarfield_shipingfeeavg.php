<?php
 // created: 2021-09-09 06:01:28
$dictionary['AOS_Invoices']['fields']['shipingfeeavg']['name']='shipingfeeavg';
$dictionary['AOS_Invoices']['fields']['shipingfeeavg']['vname']='LBL_SHIPPING_FEE_AVG';
$dictionary['AOS_Invoices']['fields']['shipingfeeavg']['type']='currency';
$dictionary['AOS_Invoices']['fields']['shipingfeeavg']['dbType']='double';
$dictionary['AOS_Invoices']['fields']['shipingfeeavg']['duplicate_merge']='enabled';
$dictionary['AOS_Invoices']['fields']['shipingfeeavg']['required']=false;
$dictionary['AOS_Invoices']['fields']['shipingfeeavg']['options']='numeric_range_search_dom';
$dictionary['AOS_Invoices']['fields']['shipingfeeavg']['enable_range_search']=true;
$dictionary['AOS_Invoices']['fields']['shipingfeeavg']['inline_edit']='';
$dictionary['AOS_Invoices']['fields']['shipingfeeavg']['duplicate_merge_dom_value']='1';
$dictionary['AOS_Invoices']['fields']['shipingfeeavg']['merge_filter']='disabled';
$dictionary['AOS_Invoices']['fields']['shipingfeeavg']['audited']=true;

 ?>