<?php
 $dictionary["AOS_Invoices"]["fields"]["payment_date"] = array(
    'name' => 'payment_date',
    'vname' => 'LBL_PAYMENT_DATE',
    'type' => 'date',
    'audited' => true,
    'required' => false,
    'enable_range_search' => true,
    'options' => 'date_range_search_dom',
	 "massupdate" => false,
);
 ?>