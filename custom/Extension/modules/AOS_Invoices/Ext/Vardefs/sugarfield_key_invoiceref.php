<?php
 // created: 2021-10-08 10:32:40


$dictionary["AOS_Invoices"]["fields"]["key_invoiceref"] = array(
    'name' => 'key_invoiceref',
    'vname' => 'LBL_KEY_INVOICE_REF',
    'type' => 'varchar',
    "len" => 255,
    "importable" => true,
    "audited" => true,
    "massupdate" => false,
);

 ?>