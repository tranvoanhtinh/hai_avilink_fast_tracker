<?php
 // created: 2021-12-09 04:35:43
$dictionary['AOS_Invoices']['fields']['status_invoice_ref']['name']='status_invoice_ref';
$dictionary['AOS_Invoices']['fields']['status_invoice_ref']['vname']='LBL_STATUS_INVOICE_REF';
$dictionary['AOS_Invoices']['fields']['status_invoice_ref']['len']='100';
$dictionary['AOS_Invoices']['fields']['status_invoice_ref']['type']='enum';
$dictionary['AOS_Invoices']['fields']['status_invoice_ref']['audited']=true;
$dictionary['AOS_Invoices']['fields']['status_invoice_ref']['inline_edit']=true;
$dictionary['AOS_Invoices']['fields']['status_invoice_ref']['massupdate']='1';
$dictionary['AOS_Invoices']['fields']['status_invoice_ref']['options']='invoice_status_ref_list';
$dictionary['AOS_Invoices']['fields']['status_invoice_ref']['merge_filter']='disabled';
$dictionary['AOS_Invoices']['fields']['status_invoice_ref']['importable']='false';

 ?>