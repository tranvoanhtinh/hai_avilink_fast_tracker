<?php
 $dictionary["AOS_Invoices"]["fields"]["estimated_delivery_date"] = array(
    'name' => 'estimated_delivery_date',
    'vname' => 'LBL_ESTIMATED_DELIVERY_DATE',
    'type' => 'date',
    'audited' => true,
    'required' => false,
    'enable_range_search' => true,
    'options' => 'date_range_search_dom',
	 "massupdate" => false,
);
 ?>