<?php
 // created: 2024-04-25 11:43:48
$dictionary['AOS_Invoices']['fields']['advance_payment']['required']=false;
$dictionary['AOS_Invoices']['fields']['advance_payment']['name']='advance_payment';
$dictionary['AOS_Invoices']['fields']['advance_payment']['vname']='LBL_ADVANCE_PAYMENT';
$dictionary['AOS_Invoices']['fields']['advance_payment']['type']='currency';
$dictionary['AOS_Invoices']['fields']['advance_payment']['massupdate']=0;
$dictionary['AOS_Invoices']['fields']['advance_payment']['comments']='';
$dictionary['AOS_Invoices']['fields']['advance_payment']['help']='';
$dictionary['AOS_Invoices']['fields']['advance_payment']['importable']='true';
$dictionary['AOS_Invoices']['fields']['advance_payment']['duplicate_merge']='disabled';
$dictionary['AOS_Invoices']['fields']['advance_payment']['duplicate_merge_dom_value']='0';
$dictionary['AOS_Invoices']['fields']['advance_payment']['audited']=false;
$dictionary['AOS_Invoices']['fields']['advance_payment']['reportable']=true;
$dictionary['AOS_Invoices']['fields']['advance_payment']['len']='26,6';
$dictionary['AOS_Invoices']['fields']['advance_payment']['enable_range_search']=true;
$dictionary['AOS_Invoices']['fields']['advance_payment']['options']='numeric_range_search_dom';
$dictionary['AOS_Invoices']['fields']['advance_payment']['inline_edit']=true;
$dictionary['AOS_Invoices']['fields']['advance_payment']['merge_filter']='disabled';

 ?>