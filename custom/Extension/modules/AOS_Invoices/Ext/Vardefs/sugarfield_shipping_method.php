<?php
 // created: 2022-08-17 12:29:53
$dictionary['AOS_Invoices']['fields']['shipping_method']['name']='shipping_method';
$dictionary['AOS_Invoices']['fields']['shipping_method']['vname']='LBL_SHIPPING_METHOD';
$dictionary['AOS_Invoices']['fields']['shipping_method']['len']='100';
$dictionary['AOS_Invoices']['fields']['shipping_method']['type']='enum';
$dictionary['AOS_Invoices']['fields']['shipping_method']['audited']=true;
$dictionary['AOS_Invoices']['fields']['shipping_method']['inline_edit']=true;
$dictionary['AOS_Invoices']['fields']['shipping_method']['massupdate']='1';
$dictionary['AOS_Invoices']['fields']['shipping_method']['options']='shipping_method_list';
$dictionary['AOS_Invoices']['fields']['shipping_method']['merge_filter']='disabled';

 ?>