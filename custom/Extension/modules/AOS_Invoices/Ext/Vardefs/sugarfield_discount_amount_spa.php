<?php

$dictionary["AOS_Invoices"]["fields"]["discount_amount_spa"] = array(
    'name' => 'discount_amount_spa',
    'vname' => 'LBL_DISCOUNT_AMOUNT_SPA',
    'len' => '100',
    'type' => 'enum',
    'audited' => true,
    'inline_edit' => true,
    'massupdate' => '1',
    'options' => 'discount_amount_spa_list',
    'merge_filter' => 'disabled',
);