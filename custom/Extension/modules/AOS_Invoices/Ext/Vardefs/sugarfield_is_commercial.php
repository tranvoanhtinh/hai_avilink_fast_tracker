<?php
 // created: 2022-08-02 11:05:15
$dictionary['AOS_Invoices']['fields']['is_commercial']['name']='is_commercial';
$dictionary['AOS_Invoices']['fields']['is_commercial']['vname']='LBL_IS_COMMERCIAL';
$dictionary['AOS_Invoices']['fields']['is_commercial']['type']='bool';
$dictionary['AOS_Invoices']['fields']['is_commercial']['duplicate_merge']='enabled';
$dictionary['AOS_Invoices']['fields']['is_commercial']['required']=false;
$dictionary['AOS_Invoices']['fields']['is_commercial']['inline_edit']='';
$dictionary['AOS_Invoices']['fields']['is_commercial']['merge_filter']='disabled';
$dictionary['AOS_Invoices']['fields']['is_commercial']['audited']=false;
$dictionary['AOS_Invoices']['fields']['is_commercial']['default']='0';
$dictionary['AOS_Invoices']['fields']['is_commercial']['duplicate_merge_dom_value']='1';

 ?>