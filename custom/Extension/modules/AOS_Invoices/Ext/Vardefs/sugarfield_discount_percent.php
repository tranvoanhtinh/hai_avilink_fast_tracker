<?php
 // created: 2022-11-11 05:28:12
$dictionary['AOS_Invoices']['fields']['discount_percent']['name']='discount_percent';
$dictionary['AOS_Invoices']['fields']['discount_percent']['vname']='LBL_DISCOUNT_PERCENT';
$dictionary['AOS_Invoices']['fields']['discount_percent']['len']='100';
$dictionary['AOS_Invoices']['fields']['discount_percent']['type']='enum';
$dictionary['AOS_Invoices']['fields']['discount_percent']['audited']=true;
$dictionary['AOS_Invoices']['fields']['discount_percent']['inline_edit']=true;
$dictionary['AOS_Invoices']['fields']['discount_percent']['massupdate']='1';
$dictionary['AOS_Invoices']['fields']['discount_percent']['options']='discount_percent_list';
$dictionary['AOS_Invoices']['fields']['discount_percent']['merge_filter']='disabled';

 ?>