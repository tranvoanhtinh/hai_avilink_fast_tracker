<?php
 // created: 2021-10-08 10:32:40


$dictionary["AOS_Invoices"]["fields"]["invoiceno"] = array(
    'name' => 'invoiceno',
    'vname' => 'LBL_INVOICE_NO',
    'type' => 'varchar',
    "len" => 255,
    "importable" => true,
    "audited" => true,
    "massupdate" => false,
);

 ?>