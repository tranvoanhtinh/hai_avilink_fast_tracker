<?php
 // created: 2022-08-17 12:29:53
$dictionary['AOS_Invoices']['fields']['cashier']['name']='cashier';
$dictionary['AOS_Invoices']['fields']['cashier']['vname']='LBL_CASHIER';
$dictionary['AOS_Invoices']['fields']['cashier']['len']='100';
$dictionary['AOS_Invoices']['fields']['cashier']['type']='enum';
$dictionary['AOS_Invoices']['fields']['cashier']['audited']=true;
$dictionary['AOS_Invoices']['fields']['cashier']['inline_edit']=true;
$dictionary['AOS_Invoices']['fields']['cashier']['massupdate']='1';
$dictionary['AOS_Invoices']['fields']['cashier']['options']='cashier_list';
$dictionary['AOS_Invoices']['fields']['cashier']['merge_filter']='disabled';

 ?>