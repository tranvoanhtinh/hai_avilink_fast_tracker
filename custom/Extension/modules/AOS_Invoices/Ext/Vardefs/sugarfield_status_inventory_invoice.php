<?php

$dictionary["AOS_Invoices"]["fields"]["status_inventory"] = array(
    'name' => 'status_inventory',
    'vname' => 'LBL_STATUS_INVENTORY',
    'len' => '100',
    'type' => 'enum',
    'audited' => true,
    'inline_edit' => true,
    'massupdate' => '1',
    'options' => 'inventory_status_list',
    'merge_filter' => 'disabled',
    'studio' => true,

);
