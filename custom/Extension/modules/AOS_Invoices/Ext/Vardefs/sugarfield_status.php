<?php
 // created: 2024-01-30 04:55:28
$dictionary['AOS_Invoices']['fields']['status']['inline_edit']=true;
$dictionary['AOS_Invoices']['fields']['status']['merge_filter']='disabled';
$dictionary['AOS_Invoices']['fields']['status']['massupdate']='1';
$dictionary['AOS_Invoices']['fields']['status']['options']='invoice_status_dom';
$dictionary['AOS_Invoices']['fields']['status']['default']='Unpaid';

 ?>