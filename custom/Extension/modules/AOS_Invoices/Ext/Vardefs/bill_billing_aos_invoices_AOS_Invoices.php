<?php
// created: 2021-08-18 06:32:45
$dictionary["AOS_Invoices"]["fields"]["bill_billing_aos_invoices"] = array (
  'name' => 'bill_billing_aos_invoices',
  'type' => 'link',
  'relationship' => 'bill_billing_aos_invoices',
  'source' => 'non-db',
  'module' => 'bill_Billing',
  'bean_name' => 'bill_Billing',
  'side' => 'right',
  'vname' => 'LBL_BILL_BILLING_AOS_INVOICES_FROM_BILL_BILLING_TITLE',
);
