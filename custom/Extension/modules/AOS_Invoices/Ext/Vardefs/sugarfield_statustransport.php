<?php
 // created: 2021-10-08 10:32:40
$dictionary['AOS_Invoices']['fields']['statustransport']['name']='statustransport';
$dictionary['AOS_Invoices']['fields']['statustransport']['vname']='LBL_STATUSTRANSPORT';
$dictionary['AOS_Invoices']['fields']['statustransport']['len']='100';
$dictionary['AOS_Invoices']['fields']['statustransport']['type']='enum';
$dictionary['AOS_Invoices']['fields']['statustransport']['audited']=true;
$dictionary['AOS_Invoices']['fields']['statustransport']['inline_edit']=true;
$dictionary['AOS_Invoices']['fields']['statustransport']['massupdate']='1';
$dictionary['AOS_Invoices']['fields']['statustransport']['options']='statustransport_list';
$dictionary['AOS_Invoices']['fields']['statustransport']['merge_filter']='disabled';

 ?>