<?php
// created: 2023-05-23 08:57:15
$dictionary["AOS_Invoices"]["fields"]["spa_actionservice_aos_invoices"] = array (
  'name' => 'spa_actionservice_aos_invoices',
  'type' => 'link',
  'relationship' => 'spa_actionservice_aos_invoices',
  'source' => 'non-db',
  'module' => 'spa_ActionService',
  'bean_name' => 'spa_ActionService',
  'side' => 'right',
  'vname' => 'LBL_SPA_ACTIONSERVICE_AOS_INVOICES_FROM_SPA_ACTIONSERVICE_TITLE',
);
