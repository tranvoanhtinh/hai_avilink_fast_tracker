<?php
 // created: 2021-09-15 09:24:17
$dictionary['AOS_Invoices']['fields']['is_invoice']['name']='is_invoice';
$dictionary['AOS_Invoices']['fields']['is_invoice']['vname']='LBL_ISINVOICE';
$dictionary['AOS_Invoices']['fields']['is_invoice']['type']='bool';
$dictionary['AOS_Invoices']['fields']['is_invoice']['duplicate_merge']='enabled';
$dictionary['AOS_Invoices']['fields']['is_invoice']['required']=false;
$dictionary['AOS_Invoices']['fields']['is_invoice']['inline_edit']='';
$dictionary['AOS_Invoices']['fields']['is_invoice']['merge_filter']='disabled';
$dictionary['AOS_Invoices']['fields']['is_invoice']['audited']=true;
$dictionary['AOS_Invoices']['fields']['is_invoice']['default']='0';

 ?>