<?php
 // created: 2024-03-06 07:55:08
$dictionary['AOS_Invoices']['fields']['take_from']['name']='take_from';
$dictionary['AOS_Invoices']['fields']['take_from']['vname']='LBL_TAKE_FROM';
$dictionary['AOS_Invoices']['fields']['take_from']['len']='100';
$dictionary['AOS_Invoices']['fields']['take_from']['type']='enum';
$dictionary['AOS_Invoices']['fields']['take_from']['audited']=true;
$dictionary['AOS_Invoices']['fields']['take_from']['inline_edit']=true;
$dictionary['AOS_Invoices']['fields']['take_from']['massupdate']='1';
$dictionary['AOS_Invoices']['fields']['take_from']['options']='take_from_list';
$dictionary['AOS_Invoices']['fields']['take_from']['merge_filter']='disabled';
$dictionary['AOS_Invoices']['fields']['take_from']['required']=true;
$dictionary['AOS_Invoices']['fields']['take_from']['default']='1';

 ?>