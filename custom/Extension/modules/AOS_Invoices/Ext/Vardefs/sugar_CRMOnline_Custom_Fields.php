<?php

$dictionary["AOS_Invoices"]["fields"]["check_status"] = array(
    'name' => 'check_status',
    'vname' => 'LBL_CHECK_STATUS',
    'type' => 'varchar',
    "len" => 255,
    "importable" => true,
    "audited" => true,
    "massupdate" => false,
);
$dictionary["AOS_Invoices"]["fields"]["aos_inventoryoutput_id"] = array(

      'name' => 'aos_inventoryoutput_id',
      'vname' => 'LBL_INVENTORY_OUT_ID',
      'type' => 'id',
      'massupdate' => '0',
      'default' => NULL,
      'no_default' => false,
      'comments' => '',
      'help' => '',
      'importable' => 'true',
      'duplicate_merge' => 'disabled',
      'duplicate_merge_dom_value' => '0',
      'audited' => false,
      'reportable' => false,
      'unified_search' => false,
      'merge_filter' => 'disabled',
      'len' => '36',
      'size' => '20',
    );

$dictionary["AOS_Invoices"]["fields"]["inventory_out"] = array(
    'source'=>'non-db',
    'name' => 'inventory_out',
    'vname' => 'LBL_INVENTORY_OUT',
    'type' => 'relate',
    'massupdate' => '0',
    'default' => NULL,
    'no_default' => false,
    'comments' => '',
    'help' => '',
    'importable' => 'true',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => false,
    'reportable' => true,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'len' => '255',
    'size' => '20',
    'id_name' => 'aos_inventoryoutput_id',
    'ext2' => 'AOS_Inventoryoutput',
    'module' => 'AOS_Inventoryoutput',
    'rname' => 'name',
    'quicksearch' => 'enabled',
    'studio' => 'visible',
);  



$dictionary["AOS_Invoices"]["fields"]["aos_inventoryinput_id"] = array(


    'name' => 'aos_inventoryinput_id',
    'vname' => 'LBL_INVENTORY_IN_ID',
    'type' => 'id',
    'massupdate' => '0',
    'default' => NULL,
    'no_default' => false,
    'comments' => '',
    'help' => '',
    'importable' => 'true',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => false,
    'reportable' => false,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'len' => '36',
    'size' => '20',
);

$dictionary["AOS_Invoices"]["fields"]["inventory_in"] = array(
    'source'=>'non-db',
    'name' => 'inventory_in',
    'vname' => 'LBL_INVENTORY_IN',
    'type' => 'relate',
    'massupdate' => '0',
    'default' => NULL,
    'no_default' => false,
    'comments' => '',
    'help' => '',
    'importable' => 'true',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => false,
    'reportable' => true,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'len' => '255',
    'size' => '20',
    'id_name' => 'aos_inventoryinput_id',
    'ext2' => 'AOS_Inventoryinput',
    'module' => 'AOS_Inventoryinput',
    'rname' => 'name',
    'quicksearch' => 'enabled',
    'studio' => 'visible',
);  



// $dictionary["AOS_Invoices"]["fields"]["aos_invoice_id"] = array(


//     'name' => 'aos_invoice_id',
//     'vname' => 'LBL_AOS_INVOICE_ID',
//     'type' => 'id',
//     'massupdate' => '0',
//     'default' => NULL,
//     'no_default' => false,
//     'comments' => '',
//     'help' => '',
//     'importable' => 'true',
//     'duplicate_merge' => 'disabled',
//     'duplicate_merge_dom_value' => '0',
//     'audited' => false,
//     'reportable' => false,
//     'unified_search' => false,
//     'merge_filter' => 'disabled',
//     'len' => '36',
//     'size' => '20',
// );

// $dictionary["AOS_Invoices"]["fields"]["invoice"] = array(
//     'source'=>'non-db',
//     'name' => 'invoice',
//     'vname' => 'LBL_INVOICE',
//     'type' => 'relate',
//     'massupdate' => '0',
//     'default' => NULL,
//     'no_default' => false,
//     'comments' => '',
//     'help' => '',
//     'importable' => 'true',
//     'duplicate_merge' => 'disabled',
//     'duplicate_merge_dom_value' => '0',
//     'audited' => false,
//     'reportable' => true,
//     'unified_search' => false,
//     'merge_filter' => 'disabled',
//     'len' => '255',
//     'size' => '20',
//     'id_name' => 'aos_invoice_id',
//     'ext2' => 'AOS_Invoices',
//     'module' => 'AOS_Invoices',
//     'rname' => 'name',
//     'quicksearch' => 'enabled',
//     'studio' => 'visible',
// );  







$dictionary["AOS_Invoices"]["fields"]["advance_payment"] = array(
       'required' => false,
                'name' => 'advance_payment',
                'vname' => 'LBL_ADVANCE_PAYMENT',
                'type' => 'currency',
                'massupdate' => 0,
                'comments' => '',
                'help' => '',
                'importable' => 'true',
                'duplicate_merge' => 'disabled',
                'duplicate_merge_dom_value' => '0',
                'audited' => false,
                'reportable' => true,
                'len' => '26,6',
                'enable_range_search' => true,
                'options' => 'numeric_range_search_dom',
            );
$dictionary["AOS_Invoices"]["fields"]["discount_percent"] = array(
       'required' => false,
                'name' => 'discount_percent',
                'vname' => 'LBL_DISCOUNT_PERCENT',
                'name' => 'discount_percent',
                'len' => '100',
                'type' => 'enum',
                'audited' => true,
                'inline_edit' => true,
                'massupdate' => '1',
                'options' => 'discount_percent_list',
                'merge_filter' => 'disabled',
            );



