<?php
 // created: 2023-06-12 12:05:29
$dictionary['AOS_Invoices']['fields']['paymentmethod']['name']='paymentmethod';
$dictionary['AOS_Invoices']['fields']['paymentmethod']['vname']='LBL_PAYMENTMETHOD';
$dictionary['AOS_Invoices']['fields']['paymentmethod']['len']='100';
$dictionary['AOS_Invoices']['fields']['paymentmethod']['type']='enum';
$dictionary['AOS_Invoices']['fields']['paymentmethod']['audited']=true;
$dictionary['AOS_Invoices']['fields']['paymentmethod']['inline_edit']=true;
$dictionary['AOS_Invoices']['fields']['paymentmethod']['massupdate']='1';
$dictionary['AOS_Invoices']['fields']['paymentmethod']['options']='paymentmethod_list';
$dictionary['AOS_Invoices']['fields']['paymentmethod']['merge_filter']='disabled';
$dictionary['AOS_Invoices']['fields']['paymentmethod']['studio']=true;
$dictionary['AOS_Invoices']['fields']['paymentmethod']['default']='9';

 ?>