<?php

$dictionary["AOS_pos"]["fields"]["aos_pos_cs_cususer_1"] = array (
    'name' => 'aos_pos_cs_cususer_1',
    'type' => 'link',
    'relationship' => 'aos_pos_cs_cususer_1',
    'source' => 'non-db',
    'module' => 'cs_cusUser',
    'bean_name' => 'cs_cusUser',
    'side' => 'right',
    'vname' => 'LBL_AOS_POS_CS_CUSUSER_1_FROM_CS_CUSUSER_TITLE',
);
$dictionary["AOS_pos"]["fields"]["bill_billing_aos_pos"] = array (
    'name' => 'bill_billing_aos_pos',
    'type' => 'link',
    'relationship' => 'bill_billing_aos_pos',
    'source' => 'non-db',
    'module' => 'bill_Billing',
    'bean_name' => 'bill_Billing',
    'side' => 'right',
    'vname' => 'LBL_BILL_BILLING_AOS_POS_FROM_BILL_BILLING_TITLE',
);
$dictionary["AOS_pos"]["fields"]["spa_actionservice_aos_pos"] = array (
    'name' => 'spa_actionservice_aos_pos',
    'type' => 'link',
    'relationship' => 'spa_actionservice_aos_pos',
    'source' => 'non-db',
    'module' => 'spa_ActionService',
    'bean_name' => 'spa_ActionService',
    'side' => 'right',
    'vname' => 'LBL_SPA_ACTIONSERVICE_AOS_POS_FROM_SPA_ACTIONSERVICE_TITLE',
);
