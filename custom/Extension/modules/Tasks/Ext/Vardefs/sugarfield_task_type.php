<?php
$dictionary["Task"]["fields"]["task_type"] = array(
    'name' => 'task_type',
    'vname' => 'LBL_TASK_TYPE',
    'len' => '100',
    'type' => 'enum',
    'audited' => true,
    'inline_edit' => true,
    'massupdate' => '1',
    'options' => 'task_type_list',
    'merge_filter' => 'disabled',
);