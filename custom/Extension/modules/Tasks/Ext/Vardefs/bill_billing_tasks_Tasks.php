<?php
// created: 2021-08-18 06:32:45
$dictionary["Task"]["fields"]["bill_billing_tasks"] = array (
  'name' => 'bill_billing_tasks',
  'type' => 'link',
  'relationship' => 'bill_billing_tasks',
  'source' => 'non-db',
  'module' => 'bill_Billing',
  'bean_name' => 'bill_Billing',
  'vname' => 'LBL_BILL_BILLING_TASKS_FROM_BILL_BILLING_TITLE',
  'id_name' => 'bill_billing_tasksbill_billing_ida',
);
$dictionary["Task"]["fields"]["bill_billing_tasks_name"] = array (
  'name' => 'bill_billing_tasks_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_BILL_BILLING_TASKS_FROM_BILL_BILLING_TITLE',
  'save' => true,
  'id_name' => 'bill_billing_tasksbill_billing_ida',
  'link' => 'bill_billing_tasks',
  'table' => 'bill_billing',
  'module' => 'bill_Billing',
  'rname' => 'name',
);
$dictionary["Task"]["fields"]["bill_billing_tasksbill_billing_ida"] = array (
  'name' => 'bill_billing_tasksbill_billing_ida',
  'type' => 'link',
  'relationship' => 'bill_billing_tasks',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_BILL_BILLING_TASKS_FROM_TASKS_TITLE',
);
