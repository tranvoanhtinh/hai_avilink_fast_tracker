<?php
 // created: 2020-12-09 05:40:10
$layout_defs["Accounts"]["subpanel_setup"]['accounts_tttt_history_calls_1'] = array (
  'order' => 100,
  'module' => 'tttt_History_calls',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_TTTT_HISTORY_CALLS_1_FROM_TTTT_HISTORY_CALLS_TITLE',
  'get_subpanel_data' => 'accounts_tttt_history_calls_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
