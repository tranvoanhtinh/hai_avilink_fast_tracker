<?php

$dictionary['AOS_Quotes']['fields']['phoneaccount'] =  array (
    'name' => 'phoneaccount',
    'vname' => 'LBL_PHONEACCOUNT',
    'type' => 'varchar',
    "len" => 255,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);