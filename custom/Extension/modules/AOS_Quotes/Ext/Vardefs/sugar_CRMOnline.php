<?php
$dictionary["AOS_Quotes"]["fields"]["total_discount_amount"] = array(
       'required' => false,
                'name' => 'total_discount_amount',
                'vname' => 'LBL_TOTAL_DISCOUNT_AMOUNT',
                'type' => 'currency',
                'massupdate' => 0,
                'comments' => '',
                'help' => '',
                'importable' => 'true',
                'duplicate_merge' => 'disabled',
                'duplicate_merge_dom_value' => '0',
                'audited' => false,
                'reportable' => true,
                'len' => '26,6',
                'enable_range_search' => true,
                'options' => 'numeric_range_search_dom',
            );


$dictionary["AOS_Quotes"]["fields"]["remaining_money"] = array(
       'required' => false,
                'name' => 'remaining_money',
                'vname' => 'LBL_REMAINING_MONEY',
                'type' => 'currency',
                'massupdate' => 0,
                'comments' => '',
                'help' => '',
                'importable' => 'true',
                'duplicate_merge' => 'disabled',
                'duplicate_merge_dom_value' => '0',
                'audited' => false,
                'reportable' => true,
                'len' => '26,6',
                'enable_range_search' => true,
                'options' => 'numeric_range_search_dom',
            );
$dictionary["AOS_Quotes"]["fields"]["advance_payment"] = array(
       'required' => false,
                'name' => 'advance_payment',
                'vname' => 'LBL_ADVANCE_PAYMENT',
                'type' => 'currency',
                'massupdate' => 0,
                'comments' => '',
                'help' => '',
                'importable' => 'true',
                'duplicate_merge' => 'disabled',
                'duplicate_merge_dom_value' => '0',
                'audited' => false,
                'reportable' => true,
                'len' => '26,6',
                'enable_range_search' => true,
                'options' => 'numeric_range_search_dom',
            );
$dictionary["AOS_Quotes"]["fields"]["discount_percent"] = array(
       'required' => false,
                'name' => 'discount_percent',
                'vname' => 'LBL_DISCOUNT_PERCENT',
                'name' => 'discount_percent',
                'len' => '100',
                'type' => 'enum',
                'audited' => true,
                'inline_edit' => true,
                'massupdate' => '1',
                'options' => 'discount_percent_list',
                'merge_filter' => 'disabled',
            );



