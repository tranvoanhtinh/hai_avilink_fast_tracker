<?php
 // created: 2021-09-15 09:24:17
$dictionary['tttt_WorldfonePBX']['fields']['is_sendsms']['name']='is_sendsms';
$dictionary['tttt_WorldfonePBX']['fields']['is_sendsms']['vname']='LBL_ISSENDSMS';
$dictionary['tttt_WorldfonePBX']['fields']['is_sendsms']['type']='bool';
$dictionary['tttt_WorldfonePBX']['fields']['is_sendsms']['duplicate_merge']='enabled';
$dictionary['tttt_WorldfonePBX']['fields']['is_sendsms']['required']=false;
$dictionary['tttt_WorldfonePBX']['fields']['is_sendsms']['inline_edit']='';
$dictionary['tttt_WorldfonePBX']['fields']['is_sendsms']['merge_filter']='disabled';
$dictionary['tttt_WorldfonePBX']['fields']['is_sendsms']['audited']=true;
$dictionary['tttt_WorldfonePBX']['fields']['is_sendsms']['default']='1';

 ?>