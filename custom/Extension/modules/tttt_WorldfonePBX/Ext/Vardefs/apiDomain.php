

<?php
 $dictionary["tttt_WorldfonePBX"]["fields"]["api_domain"] = array(
    'name' => 'api_domain',
    'vname' => 'LBL_API_DOMAIN',
    'type' => 'varchar',
    "len" => 500,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);
 ?>

