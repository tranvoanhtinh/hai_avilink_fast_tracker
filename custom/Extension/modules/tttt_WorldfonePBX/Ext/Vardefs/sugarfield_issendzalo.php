<?php
 // created: 2021-09-15 09:24:17
$dictionary['tttt_WorldfonePBX']['fields']['is_sendzalo']['name']='is_sendzalo';
$dictionary['tttt_WorldfonePBX']['fields']['is_sendzalo']['vname']='LBL_ISSENDZALO';
$dictionary['tttt_WorldfonePBX']['fields']['is_sendzalo']['type']='bool';
$dictionary['tttt_WorldfonePBX']['fields']['is_sendzalo']['duplicate_merge']='enabled';
$dictionary['tttt_WorldfonePBX']['fields']['is_sendzalo']['required']=false;
$dictionary['tttt_WorldfonePBX']['fields']['is_sendzalo']['inline_edit']='';
$dictionary['tttt_WorldfonePBX']['fields']['is_sendzalo']['merge_filter']='disabled';
$dictionary['tttt_WorldfonePBX']['fields']['is_sendzalo']['audited']=true;
$dictionary['tttt_WorldfonePBX']['fields']['is_sendzalo']['default']='1';

 ?>