<?php
$dictionary["tttt_WorldfonePBX"]["fields"]["swiftcode"] = array (
    'type' => 'varchar',
    'len' => 255,
    'label' => 'LBL_SWIFTCODE',
    'sortable' => false,
    'width' => '10%',
    'default' => '',
    'name' => 'swiftcode',
  );
 ?>