<?php
 $dictionary["tttt_WorldfonePBX"]["fields"]["wss_url"] = array(
    'name' => 'wss_url',
    'vname' => 'LBL_WSS_URL',
    'type' => 'varchar',
    "len" => 500,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);
 ?>

