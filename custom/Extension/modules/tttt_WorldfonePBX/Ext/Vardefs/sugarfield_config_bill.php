

<?php
 $dictionary["tttt_WorldfonePBX"]["fields"]["company_name"] = array(
    'name' => 'company_name',
    'vname' => 'LBL_COMPANY_NAME',
    'type' => 'varchar',
    "len" => 500,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);
$dictionary["tttt_WorldfonePBX"]["fields"]["company_address"] = array(
    'name' => 'company_address',
    'vname' => 'LBL_COMPANY_ADDRESS',
    'type' => 'varchar',
    "len" => 500,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);
$dictionary["tttt_WorldfonePBX"]["fields"]["company_website"] = array(
    'name' => 'company_website',
    'vname' => 'LBL_COMPANY_WEBSITE',
    'type' => 'varchar',
    "len" => 250,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);

$dictionary["tttt_WorldfonePBX"]["fields"]["company_sologun"] = array(
    'name' => 'company_sologun',
    'vname' => 'LBL_COMPANY_SOLOGUN',
    'type' => 'varchar',
    "len" => 500,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);



		
 ?>

