<?php
 $dictionary["Lead"]["fields"]["form_id_face"] = array(
    'name' => 'form_id_face',
    'vname' => 'LBL_FORM_ID_FACE',
    'type' => 'varchar',
    "len" => 255,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);
 ?>