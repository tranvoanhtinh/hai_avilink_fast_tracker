<?php
 // created: 2020-04-20 11:07:43
$dictionary['Lead']['fields']['description']['required']=true;
$dictionary['Lead']['fields']['description']['inline_edit']=true;
$dictionary['Lead']['fields']['description']['comments']='Full text of the note';
$dictionary['Lead']['fields']['description']['merge_filter']='disabled';
$dictionary['Lead']['fields']['description']['rows']='4';

 ?>