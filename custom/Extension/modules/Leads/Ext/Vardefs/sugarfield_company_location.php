<?php
 // created: 2022-01-11 16:22:15
$dictionary['Lead']['fields']['company_location']['name']='company_location';
$dictionary['Lead']['fields']['company_location']['vname']='LBL_COMPANY_LOCATION';
$dictionary['Lead']['fields']['company_location']['len']='100';
$dictionary['Lead']['fields']['company_location']['type']='enum';
$dictionary['Lead']['fields']['company_location']['audited']=true;
$dictionary['Lead']['fields']['company_location']['inline_edit']=true;
$dictionary['Lead']['fields']['company_location']['massupdate']='1';
$dictionary['Lead']['fields']['company_location']['options']='company_location_list';
$dictionary['Lead']['fields']['company_location']['merge_filter']='disabled';

 ?>