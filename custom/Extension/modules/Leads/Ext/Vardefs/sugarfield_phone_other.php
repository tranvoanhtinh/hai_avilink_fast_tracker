<?php
 // created: 2020-03-04 04:56:18
$dictionary['Lead']['fields']['phone_other']['len']='11';
$dictionary['Lead']['fields']['phone_other']['audited']=true;
$dictionary['Lead']['fields']['phone_other']['inline_edit']='';
$dictionary['Lead']['fields']['phone_other']['comments']='Other phone number for the contact';
$dictionary['Lead']['fields']['phone_other']['merge_filter']='disabled';

 ?>