<?php
 // created: 2022-08-05 06:38:54
$dictionary['Lead']['fields']['social_account']['name']='social_account';
$dictionary['Lead']['fields']['social_account']['vname']='LBL_SOCIAL_ACCOUNT';
$dictionary['Lead']['fields']['social_account']['len']='100';
$dictionary['Lead']['fields']['social_account']['type']='enum';
$dictionary['Lead']['fields']['social_account']['audited']=true;
$dictionary['Lead']['fields']['social_account']['inline_edit']=true;
$dictionary['Lead']['fields']['social_account']['massupdate']='1';
$dictionary['Lead']['fields']['social_account']['options']='social_account_list';
$dictionary['Lead']['fields']['social_account']['merge_filter']='disabled';

 ?>