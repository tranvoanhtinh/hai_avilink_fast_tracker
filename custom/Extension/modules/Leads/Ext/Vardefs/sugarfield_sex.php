<?php
 // created: 2023-10-04 10:00:32
$dictionary['Lead']['fields']['sex']['name']='sex';
$dictionary['Lead']['fields']['sex']['vname']='LBL_SEX';
$dictionary['Lead']['fields']['sex']['len']='100';
$dictionary['Lead']['fields']['sex']['type']='enum';
$dictionary['Lead']['fields']['sex']['audited']=true;
$dictionary['Lead']['fields']['sex']['inline_edit']=true;
$dictionary['Lead']['fields']['sex']['massupdate']='1';
$dictionary['Lead']['fields']['sex']['options']='sex_list';
$dictionary['Lead']['fields']['sex']['merge_filter']='disabled';

 ?>