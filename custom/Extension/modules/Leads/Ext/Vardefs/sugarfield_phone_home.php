<?php
 // created: 2020-03-04 04:54:34
$dictionary['Lead']['fields']['phone_home']['len']='11';
$dictionary['Lead']['fields']['phone_home']['audited']=true;
$dictionary['Lead']['fields']['phone_home']['inline_edit']='';
$dictionary['Lead']['fields']['phone_home']['comments']='Home phone number of the contact';
$dictionary['Lead']['fields']['phone_home']['merge_filter']='disabled';

 ?>