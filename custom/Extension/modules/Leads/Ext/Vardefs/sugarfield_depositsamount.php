<?php
 // created: 2021-09-09 06:01:28
$dictionary['Lead']['fields']['depositsamount']['name']='depositsamount';
$dictionary['Lead']['fields']['depositsamount']['vname']='LBL_Deposits_Amount';
$dictionary['Lead']['fields']['depositsamount']['type']='currency';
$dictionary['Lead']['fields']['depositsamount']['dbType']='double';
$dictionary['Lead']['fields']['depositsamount']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['depositsamount']['required']=false;
$dictionary['Lead']['fields']['depositsamount']['options']='numeric_range_search_dom';
$dictionary['Lead']['fields']['depositsamount']['enable_range_search']=true;
$dictionary['Lead']['fields']['depositsamount']['inline_edit']='';
$dictionary['Lead']['fields']['depositsamount']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['depositsamount']['merge_filter']='disabled';
$dictionary['Lead']['fields']['depositsamount']['audited']=true;

 ?>