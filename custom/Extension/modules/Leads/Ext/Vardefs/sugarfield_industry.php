<?php
 // created: 2022-04-29 10:56:43
$dictionary['Lead']['fields']['industry']['name']='industry';
$dictionary['Lead']['fields']['industry']['vname']='LBL_INDUSTRY';
$dictionary['Lead']['fields']['industry']['len']='100';
$dictionary['Lead']['fields']['industry']['type']='enum';
$dictionary['Lead']['fields']['industry']['audited']=false;
$dictionary['Lead']['fields']['industry']['inline_edit']=true;
$dictionary['Lead']['fields']['industry']['massupdate']='1';
$dictionary['Lead']['fields']['industry']['options']='industry_dom';
$dictionary['Lead']['fields']['industry']['merge_filter']='disabled';

 ?>