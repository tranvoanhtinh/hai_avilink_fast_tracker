<?php

$dictionary["Lead"]["fields"]["opportunity_amount"]["type"] = "currency";
$dictionary["Lead"]["fields"]["opportunity_amount"]["dbType"] = "double";
$dictionary["Lead"]["fields"]["opportunity_amount"]["len"] = "26,6";
$dictionary["Lead"]["fields"]["probability"] = array(
                'name' => 'probability',
                'vname' => 'LBL_PROBABILITY',
                'type' => 'int',
                'dbType' => 'double',
                'audited' => true,
                'comment' => 'The probability of closure',
                'validation' => array('type' => 'range', 'min' => 0, 'max' => 100),
                'merge_filter' => 'enabled',
);

$dictionary["Lead"]["fields"]["date_new"] = array(
    'name' => 'date_new',
    'vname' => 'LBL_DATE_NEW',
    'type' => 'date',
    'audited' => true,
    'required' => false,
    'enable_range_search' => true,
    'options' => 'date_range_search_dom',
	 "massupdate" => false,
);

$dictionary["Lead"]["fields"]["date_processed"] = array(
    'name' => 'date_processed',
    'vname' => 'LBL_DATE_PROCESSED',
    'type' => 'date',
    'audited' => true,
    'required' => false,
    'enable_range_search' => true,
    'options' => 'date_range_search_dom',
	"massupdate" => false,
);

$dictionary["Lead"]["fields"]["date_converted"] = array(
    'name' => 'date_converted',
    'vname' => 'LBL_DATE_CONVERTED',
    'type' => 'date',
    'audited' => true,
    'required' => false,
    'enable_range_search' => true,
    'options' => 'date_range_search_dom',
	 "massupdate" => false,
);

$dictionary["Lead"]["fields"]["date_transaction"] = array(
    'name' => 'date_transaction',
    'vname' => 'LBL_DATE_TRANSACTION',
    'type' => 'date',
    'audited' => true,
    'required' => false,
    'enable_range_search' => true,
    'options' => 'date_range_search_dom',
	 "massupdate" => false,
);
$dictionary["Lead"]["fields"]["otherassigned"] = array(
    'name' => 'otherassigned',
    'vname' => 'LBL_OTHER_ASSIGNED',
    'type' => 'varchar',
    "len" => 255,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);
$dictionary["Lead"]["fields"]["converted"]['massupdate'] = false;
$dictionary["Lead"]["fields"]["status"]['massupdate'] = false;

$dictionary["Lead"]["fields"]["phone_mobile"]['importable'] = true;
$dictionary["Lead"]["indices"][] = array('name' => 'idx_phone_mobile_del', 'type' => 'index', 'fields' => array('phone_mobile', 'deleted'));