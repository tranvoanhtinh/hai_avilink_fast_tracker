<?php
 // created: 2021-10-10 08:02:26
$dictionary['Lead']['fields']['phone_mobile']['len']='50';
$dictionary['Lead']['fields']['phone_mobile']['audited']=true;
$dictionary['Lead']['fields']['phone_mobile']['inline_edit']='';
$dictionary['Lead']['fields']['phone_mobile']['comments']='Mobile phone number of the contact';
$dictionary['Lead']['fields']['phone_mobile']['merge_filter']='disabled';
$dictionary['Lead']['fields']['phone_mobile']['required']=true;

 ?>