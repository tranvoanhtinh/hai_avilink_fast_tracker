<?php
 $dictionary["Lead"]["fields"]["consultant"] = array(
    'name' => 'consultant',
    'vname' => 'LBL_CONSULTANT',
    'type' => 'varchar',
    "len" => 255,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);
 ?>