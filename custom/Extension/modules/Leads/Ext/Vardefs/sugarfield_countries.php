<?php
 // created: 2022-11-10 10:53:37
$dictionary['Lead']['fields']['countries']['name']='countries';
$dictionary['Lead']['fields']['countries']['vname']='LBL_COUNTRIES';
$dictionary['Lead']['fields']['countries']['len']='100';
$dictionary['Lead']['fields']['countries']['type']='enum';
$dictionary['Lead']['fields']['countries']['audited']=false;
$dictionary['Lead']['fields']['countries']['inline_edit']=true;
$dictionary['Lead']['fields']['countries']['options']='countries_dom';
$dictionary['Lead']['fields']['countries']['merge_filter']='disabled';

 ?>