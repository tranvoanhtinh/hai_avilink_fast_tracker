<?php
 // created: 2019-07-17 16:33:49
$dictionary['Lead']['fields']['status_description']['audited']=true;
$dictionary['Lead']['fields']['status_description']['inline_edit']=true;
$dictionary['Lead']['fields']['status_description']['comments']='Description of the status of the lead';
$dictionary['Lead']['fields']['status_description']['merge_filter']='disabled';
$dictionary['Lead']['fields']['status_description']['rows']='4';
$dictionary['Lead']['fields']['status_description']['cols']='20';

 ?>