<?php
 $dictionary["Lead"]["fields"]["campaign_id_face_ads"] = array(
    'name' => 'campaign_id_face_ads',
    'vname' => 'LBL_CAMPAIGN_ID_FACE_ADS',
    'type' => 'varchar',
    "len" => 255,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);
 ?>