<?php
 // created: 2022-01-11 16:22:15
$dictionary['Lead']['fields']['status_activites']['name']='status_activites';
$dictionary['Lead']['fields']['status_activites']['vname']='LBL_STATUS_ACTIVITIES';
$dictionary['Lead']['fields']['status_activites']['len']='100';
$dictionary['Lead']['fields']['status_activites']['type']='enum';
$dictionary['Lead']['fields']['status_activites']['audited']=true;
$dictionary['Lead']['fields']['status_activites']['inline_edit']=true;
$dictionary['Lead']['fields']['status_activites']['massupdate']='1';
$dictionary['Lead']['fields']['status_activites']['options']='status_activities_list';
$dictionary['Lead']['fields']['status_activites']['merge_filter']='disabled';

 ?>