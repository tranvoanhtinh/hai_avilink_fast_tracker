<?php
$dictionary['Lead']['fields']['is_single']['name']='is_single';
$dictionary['Lead']['fields']['is_single']['vname']='LBL_IS_SINGLE';
$dictionary['Lead']['fields']['is_single']['type']='bool';
$dictionary['Lead']['fields']['is_single']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['is_single']['required']=false;
$dictionary['Lead']['fields']['is_single']['inline_edit']='';
$dictionary['Lead']['fields']['is_single']['merge_filter']='disabled';
$dictionary['Lead']['fields']['is_single']['audited']=true;
$dictionary['Lead']['fields']['is_single']['default']='0';

 ?>