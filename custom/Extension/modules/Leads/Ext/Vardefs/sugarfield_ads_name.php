<?php
 $dictionary["Lead"]["fields"]["ads_name"] = array(
    'name' => 'ads_name',
    'vname' => 'LBL_ADS_NAME',
    'type' => 'varchar',
    "len" => 255,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);
 ?>