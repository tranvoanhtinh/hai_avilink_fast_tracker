<?php
 $dictionary["Lead"]["fields"]["relatives"] = array(
    'name' => 'relatives',
    'vname' => 'LBL_RELATIVES',
    'type' => 'varchar',
    "len" => 255,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);
 ?>