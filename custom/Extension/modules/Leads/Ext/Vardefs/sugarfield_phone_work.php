<?php
 // created: 2020-03-04 04:56:03
$dictionary['Lead']['fields']['phone_work']['len']='11';
$dictionary['Lead']['fields']['phone_work']['inline_edit']='';
$dictionary['Lead']['fields']['phone_work']['comments']='Work phone number of the contact';
$dictionary['Lead']['fields']['phone_work']['merge_filter']='disabled';

 ?>