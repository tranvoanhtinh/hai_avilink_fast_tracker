<?php
 $dictionary["Lead"]["fields"]["customer_id"] = array(
 
                'name' => 'currency_id',
                'type' => 'id',
                'group' => 'currency_id',
                'vname' => 'LBL_CURRENCY',
                'function' => array('name' => 'getCurrencyDropDown', 'returns' => 'html', 'onListView' => true),
                'reportable' => false,
                'comment' => 'Currency used for display purposes',
     );

$dictionary["Lead"]["fields"]["currency_symbol"] = array(
 
                'name' => 'currency_symbol',
                'rname' => 'symbol',
                'id_name' => 'currency_id',
                'vname' => 'LBL_CURRENCY_SYMBOL',
                'type' => 'relate',
                'isnull' => 'true',
                'table' => 'currencies',
                'module' => 'Currencies',
                'source' => 'non-db',
                'function' => array('name' => 'getCurrencySymbolDropDown', 'returns' => 'html'),
                'studio' => 'false',
                'duplicate_merge' => 'disabled',
       
);


$dictionary["Lead"]["fields"]["currency_name"] = array(
 
                'name' => 'currency_name',
                'rname' => 'name',
                'id_name' => 'currency_id',
                'vname' => 'LBL_CURRENCY_NAME',
                'type' => 'relate',
                'isnull' => 'true',
                'table' => 'currencies',
                'module' => 'Currencies',
                'source' => 'non-db',
                'function' => array('name' => 'getCurrencyNameDropDown', 'returns' => 'html'),
                'studio' => 'false',
                'duplicate_merge' => 'disabled',
          
);

 ?>