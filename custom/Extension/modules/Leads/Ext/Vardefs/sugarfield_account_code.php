<?php
 $dictionary["Lead"]["fields"]["account_code"] = array(
    'name' => 'account_code',
    'vname' => 'LBL_ACCOUNT_CODE',
    'type' => 'varchar',
    "len" => 255,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);
 ?>