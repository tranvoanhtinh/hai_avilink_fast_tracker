<?php
 // created: 2021-09-15 09:24:17
$dictionary['Lead']['fields']['paymentsamount']['name']='paymentsamount';
$dictionary['Lead']['fields']['paymentsamount']['vname']='LBL_Payments_Amount';
$dictionary['Lead']['fields']['paymentsamount']['type']='currency';
$dictionary['Lead']['fields']['paymentsamount']['dbType']='double';
$dictionary['Lead']['fields']['paymentsamount']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['paymentsamount']['required']=false;
$dictionary['Lead']['fields']['paymentsamount']['options']='numeric_range_search_dom';
$dictionary['Lead']['fields']['paymentsamount']['enable_range_search']=true;
$dictionary['Lead']['fields']['paymentsamount']['inline_edit']='';
$dictionary['Lead']['fields']['paymentsamount']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['paymentsamount']['merge_filter']='disabled';
$dictionary['Lead']['fields']['paymentsamount']['audited']=true;

 ?>