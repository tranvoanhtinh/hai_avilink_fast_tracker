<?php
 $dictionary["Lead"]["fields"]["campaign_name_face_ads"] = array(
    'name' => 'campaign_name_face_ads',
    'vname' => 'LBL_CAMPAIGN_NAME_FACE_ADS',
    'type' => 'varchar',
    "len" => 255,
    "importable" => true,
    "audited" => true,
    "massupdate" => true,
);
 ?>