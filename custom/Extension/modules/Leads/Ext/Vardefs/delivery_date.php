<?php
 $dictionary["Lead"]["fields"]["delivery_date"] = array(
    'name' => 'delivery_date',
    'vname' => 'LBL_DELIVERY_DATE',
    'type' => 'datetimecombo',
	'dbType' => 'datetime',
    'audited' => true,
    'required' => false,
    'enable_range_search' => true,
    'options' => 'date_range_search_dom',
	 "massupdate" => false,
);
 ?>