<?php
$dictionary['Lead']['fields']['list_age']['name']='list_age';
$dictionary['Lead']['fields']['list_age']['vname']='LBL_LIST_AGE';
$dictionary['Lead']['fields']['list_age']['len']='100';
$dictionary['Lead']['fields']['list_age']['type']='enum';
$dictionary['Lead']['fields']['list_age']['audited']=true;
$dictionary['Lead']['fields']['list_age']['inline_edit']=true;
$dictionary['Lead']['fields']['list_age']['massupdate']='1';
$dictionary['Lead']['fields']['list_age']['options'] = 'list_age_list';
$dictionary['Lead']['fields']['list_age']['merge_filter']='disabled';