<?php
 // created: 2020-04-17 10:28:59
$dictionary['Lead']['fields']['account_name']['required']=false;
$dictionary['Lead']['fields']['account_name']['inline_edit']=true;
$dictionary['Lead']['fields']['account_name']['comments']='Account name for lead';
$dictionary['Lead']['fields']['account_name']['merge_filter']='disabled';
$dictionary['Lead']['fields']['account_name']['full_text_search']=NULL;
$dictionary['Lead']['fields']['account_name']['audited']=true;
$dictionary['Lead']['fields']['account_name']['help']='Account name for lead';

 ?>