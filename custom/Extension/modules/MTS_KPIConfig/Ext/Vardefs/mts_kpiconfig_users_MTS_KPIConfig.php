<?php
// created: 2018-09-02 09:33:00
$dictionary["MTS_KPIConfig"]["fields"]["mts_kpiconfig_users"] = array (
  'name' => 'mts_kpiconfig_users',
  'type' => 'link',
  'relationship' => 'mts_kpiconfig_users',
  'source' => 'non-db',
  'module' => 'Users',
  'bean_name' => 'User',
  'vname' => 'LBL_MTS_KPICONFIG_USERS_FROM_USERS_TITLE',
);
