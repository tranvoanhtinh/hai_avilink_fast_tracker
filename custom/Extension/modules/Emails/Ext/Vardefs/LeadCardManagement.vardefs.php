<?php

$dictionary["Email"]["fields"]["email_type"] = array(
    "name" => "email_type",
    "vname" => "LBL_EMAIL_TYPE",
    "type" => "enum",
    "options" => "lead_card_email_type_options",
    "default" => "",
);

$dictionary["Email"]["fields"]["date_sent"]["display_default"] = 'now';