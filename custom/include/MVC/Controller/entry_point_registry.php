<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
$entry_point_registry['changeSta'] = array('file' =>
'custom/include/EntryPoints/changeSta.php', 'auth' =>true);

$entry_point_registry['changeStatusCases'] = array('file' =>
'custom/include/EntryPoints/changeStatusCases.php', 'auth' =>true);

$entry_point_registry['getInfo'] = array('file' =>
'custom/include/EntryPoints/getInfo.php', 'auth' =>false);
$entry_point_registry['getInfo1'] = array('file' =>
'custom/include/EntryPoints/getInfo1.php', 'auth' =>false);
$entry_point_registry['getToken'] = array(
    'file' => 'custom/include/EntryPoints/getToken.php',
    'auth' => false
  );
$entry_point_registry['saveUser'] = array(
    'file' => 'custom/include/EntryPoints/saveUser.php',
    'auth' => false
  );
$entry_point_registry['getRole'] = array(
    'file' => 'custom/include/EntryPoints/getRole.php',
    'auth' => false
); 
$entry_point_registry['checkUsername'] = array(
    'file' => 'custom/include/EntryPoints/checkUsername.php',
    'auth' => false
);   
 $entry_point_registry['getAccount'] = array(
    'file' => 'custom/include/EntryPoints/getAccount.php',
    'auth' => true
	 );

$entry_point_registry['hook'] = array(
    'file' => 'custom/include/EntryPoints/hook.php',
    'auth' => false
  );
$entry_point_registry['getGroup'] = array(
    'file' => 'custom/include/EntryPoints/getGroup.php',
    'auth' => true
);	 
$entry_point_registry['getUser'] = array(
    'file' => 'custom/include/EntryPoints/getUser.php',
    'auth' => true
);  
$entry_point_registry['dataFilter'] = array(
    'file' => 'custom/include/EntryPoints/dataFilter.php',
    'auth' => true
);  
$entry_point_registry['loadProvince'] = array(
    'file' => 'custom/include/EntryPoints/loadProvince.php',
    'auth' => true
);  
$entry_point_registry['showProvince'] = array(
    'file' => 'custom/include/EntryPoints/showProvince.php',
    'auth' => true
); 
$entry_point_registry['fixReceiveNotifications'] = array(
    'file' => 'custom/include/EntryPoints/fixReceiveNotifications.php',
    'auth' => true
); 
$entry_point_registry['getTitle'] = array(
    'file' => 'custom/include/EntryPoints/getTitle.php',
    'auth' => true
); 
$entry_point_registry['getIdsGroup'] = array(
    'file' => 'custom/include/EntryPoints/getIdsGroup.php',
    'auth' => true
);

$entry_point_registry['getGroupName'] = array(
    'file' => 'custom/include/EntryPoints/getGroupName.php',
    'auth' => true
);
$entry_point_registry['getIdGroup'] = array(
    'file' => 'custom/include/EntryPoints/getIdGroup.php',
    'auth' => true
);
$entry_point_registry['saveGroup'] = array(
    'file' => 'custom/include/EntryPoints/saveGroup.php',
    'auth' => true
);
$entry_point_registry['CheckAccount'] = array(
    'file' => 'custom/include/EntryPoints/CheckAccount.php',
    'auth' => true
);	
$entry_point_registry['setAllowView'] = array(
    'file' => 'custom/include/EntryPoints/setAllowView.php',
    'auth' => true
); 
$entry_point_registry['resetPass'] = array(
    'file' => 'custom/include/EntryPoints/resetPass.php',
    'auth' => true
); 
$entry_point_registry['exportExcel'] = array(
    'file' => 'custom/include/EntryPoints/exportExcel.php',
    'auth' => true
);
$entry_point_registry['showUserAssi'] = array(
    'file' => 'custom/include/EntryPoints/showUserAssi.php',
    'auth' => true
);
$entry_point_registry['saveAssign'] = array(
    'file' => 'custom/include/EntryPoints/saveAssign.php',
    'auth' => true
);
$entry_point_registry['getOtherAssign'] = array(
    'file' => 'custom/include/EntryPoints/getOtherAssign.php',
    'auth' => true
);
$entry_point_registry['sendZaloMessage'] = array(
    'file' => 'custom/include/EntryPoints/sendZaloMessage.php',
    'auth' => false
);
$entry_point_registry['saveItem'] = array(
    'file' => 'custom/include/EntryPoints/saveItem.php',
    'auth' => true
);
$entry_point_registry['getItem'] = array(
    'file' => 'custom/include/EntryPoints/getItem.php',
    'auth' => true
);
$entry_point_registry['getDate'] = array(
    'file' => 'custom/include/EntryPoints/getDate.php',
    'auth' => true
);
$entry_point_registry['updateGroup'] = array(
    'file' => 'custom/include/EntryPoints/updateGroup.php',
    'auth' => true
);
$entry_point_registry['getDate'] = array(
    'file' => 'custom/include/EntryPoints/getDate.php',
    'auth' => true
);
$entry_point_registry['getExtInfo'] = array(
    'file' => 'custom/include/EntryPoints/getExtInfo.php',
    'auth' => true
);
$entry_point_registry['checkUser'] = array(
    'file' => 'custom/include/EntryPoints/checkUser.php',
    'auth' => true
);
$entry_point_registry['saveCampaign'] = array(
    'file' => 'custom/include/EntryPoints/saveCampaign.php',
    'auth' => true
);
$entry_point_registry['getOtherCampaign'] = array(
    'file' => 'custom/include/EntryPoints/getOtherCampaign.php',
    'auth' => true
);
$entry_point_registry['showUserCampaign'] = array(
    'file' => 'custom/include/EntryPoints/showUserCampaign.php',
    'auth' => true
);
$entry_point_registry['retrieve_dash_page'] = array(
    'file' => 'include/MySugar/retrieve_dash_page.php',
    'auth' => true
);
$entry_point_registry['deleteUser'] = array(
    'file' => 'custom/include/EntryPoints/deleteUser.php',
    'auth' => true
);

$entry_point_registry['moduleFilter'] = array(
    'file' => 'custom/include/EntryPoints/moduleFilter.php',
    'auth' => true
);
$entry_point_registry['loadProvince2'] = array(
    'file' => 'custom/include/EntryPoints/loadProvince2.php',
    'auth' => true
);
$entry_point_registry['getLanguage'] = array(
    'file' => 'custom/include/EntryPoints/getLanguage.php',
    'auth' => true
);
$entry_point_registry['loadPrice'] = array(
    'file' => 'custom/include/EntryPoints/loadPrice.php',
    'auth' => true
);
$entry_point_registry['getDeposite'] = array(
    'file' => 'custom/include/EntryPoints/getDeposite.php',
    'auth' => true
);
$entry_point_registry['getOppLanguage'] = array(
    'file' => 'custom/include/EntryPoints/getOppLanguage.php',
    'auth' => true
);
$entry_point_registry['getTime'] = array(
    'file' => 'custom/include/EntryPoints/getTime.php',
    'auth' => true
);
$entry_point_registry['updateProduct'] = array(
    'file' => 'custom/include/EntryPoints/updateProduct.php',
    'auth' => true
);
$entry_point_registry['customerRei'] = array(
    'file' => 'custom/include/EntryPoints/customerRei.php',
    'auth' => true
);
$entry_point_registry['getLeadLanguage'] = array(
    'file' => 'custom/include/EntryPoints/getLeadLanguage.php',
    'auth' => true
);
$entry_point_registry['saveNote'] = array(
    'file' => 'custom/include/EntryPoints/saveNote.php',
    'auth' => true
);
$entry_point_registry['getTemplateId'] = array(
    'file' => 'custom/include/EntryPoints/getTemplateId.php',
    'auth' => true
);
$entry_point_registry['checkSession'] = array(
    'file' => 'custom/include/EntryPoints/checkSession.php',
    'auth' => true
);
$entry_point_registry['getExt'] = array(
    'file' => 'custom/include/EntryPoints/getExt.php',
    'auth' => true
);
$entry_point_registry['exportInvoice'] = array(
    'file' => 'custom/include/EntryPoints/exportInvoice.php',
    'auth' => true
);
$entry_point_registry['changeInvoices'] = array(
    'file' => 'custom/include/EntryPoints/changeInvoices.php',
    'auth' => true
);
$entry_point_registry['replaceInvoices'] = array(
    'file' => 'custom/include/EntryPoints/replaceInvoices.php',
    'auth' => true
);
$entry_point_registry['getInvLanguage'] = array(
    'file' => 'custom/include/EntryPoints/getInvLanguage.php',
    'auth' => true
);
$entry_point_registry['actFilter'] = array(
    'file' => 'custom/include/EntryPoints/actFilter.php',
    'auth' => true
);
$entry_point_registry['getAllLanguage'] = array(
    'file' => 'custom/include/EntryPoints/getAllLanguage.php',
    'auth' => true
);
$entry_point_registry['insertRole'] = array(
    'file' => 'custom/include/EntryPoints/insertRole.php',
    'auth' => true
);
$entry_point_registry['kpiFilter'] = array(
    'file' => 'custom/include/EntryPoints/kpiFilter.php',
    'auth' => true
);
$entry_point_registry['getRevenueStatus'] = array(
    'file' => 'custom/include/EntryPoints/getRevenueStatus.php',
    'auth' => false
);
$entry_point_registry['exportAosInvoiceExcel'] = array(
    'file' => 'custom/include/EntryPoints/exportAosInvoiceExcel.php',
    'auth' => true
);
$entry_point_registry['getAds'] = array(
    'file' => 'custom/include/EntryPoints/getAds.php',
    'auth' => true
);
$entry_point_registry['exportPdf'] = array(
    'file' => 'custom/include/EntryPoints/exportPdf.php',
    'auth' => true
);
$entry_point_registry['getPrintLang'] = array(
    'file' => 'custom/include/EntryPoints/getPrintLang.php',
    'auth' => true
);
$entry_point_registry['saveReturnPay'] = array(
    'file' => 'custom/include/EntryPoints/saveReturnPay.php',
    'auth' => true
);

$entry_point_registry['getLanguageForDropdown'] = array(
    'file' => 'custom/include/EntryPoints/getLanguageForDropdown.php',
    'auth' => false
);
$entry_point_registry['getZalo'] = array(
    'file' => 'custom/include/EntryPoints/getZalo.php',
    'auth' => false
);
$entry_point_registry['deleteProduct'] = array(
    'file' => 'custom/include/EntryPoints/deleteProduct.php',
    'auth' => false
);
$entry_point_registry['getLanguageInv'] = array(
    'file' => 'custom/include/EntryPoints/getLanguageInv.php',
    'auth' => false
);
$entry_point_registry['testGanLead'] = array(
    'file' => 'custom/include/EntryPoints/testGanLead.php',
    'auth' => false
);
$entry_point_registry['getLead'] = array(
    'file' => 'custom/include/EntryPoints/getLead.php',
    'auth' => false
);
$entry_point_registry['getAccounts'] = array(
    'file' => 'custom/include/EntryPoints/getAccounts.php',
    'auth' => false
);
$entry_point_registry['getAccounts'] = array(
    'file' => 'custom/include/EntryPoints/getAccounts.php',
    'auth' => false
);
$entry_point_registry['getTask'] = array(
    'file' => 'custom/include/EntryPoints/getTask.php',
    'auth' => false
);
$entry_point_registry['getCall'] = array(
    'file' => 'custom/include/EntryPoints/getCall.php',
    'auth' => false
);
$entry_point_registry['getContact'] = array(
    'file' => 'custom/include/EntryPoints/getContact.php',
    'auth' => false
);
$entry_point_registry['getMeeting'] = array(
    'file' => 'custom/include/EntryPoints/getMeeting.php',
    'auth' => false
);
$entry_point_registry['getOpp'] = array(
    'file' => 'custom/include/EntryPoints/getOpp.php',
    'auth' => false
);
$entry_point_registry['getDropdownForSalu'] = array(
    'file' => 'custom/include/EntryPoints/getDropdownForSalu.php',
    'auth' => false
);
$entry_point_registry['viewAccount'] = array(
    'file' => 'custom/include/EntryPoints/viewAccount.php',
    'auth' => false
);
$entry_point_registry['viewLead'] = array(
    'file' => 'custom/include/EntryPoints/viewLead.php',
    'auth' => false
);
$entry_point_registry['getUserCustom'] = array(
    'file' => 'custom/include/EntryPoints/getUserCustom.php',
    'auth' => false
);
$entry_point_registry['getUserPayroll'] = array(
    'file' => 'custom/include/EntryPoints/getUserPayroll.php',
    'auth' => false
);
$entry_point_registry['saveUserPayroll'] = array(
    'file' => 'custom/include/EntryPoints/saveUserPayroll.php',
    'auth' => false
);
$entry_point_registry['getPayrollParamLanguage'] = array(
    'file' => 'custom/include/EntryPoints/getPayrollParamLanguage.php',
    'auth' => false
);
$entry_point_registry['getPayrollLanguage'] = array(
    'file' => 'custom/include/EntryPoints/getPayrollLanguage.php',
    'auth' => false
);
$entry_point_registry['calculateSalary'] = array(
    'file' => 'custom/include/EntryPoints/calculateSalary.php',
    'auth' => false
);
$entry_point_registry['savePayroll'] = array(
    'file' => 'custom/include/EntryPoints/savePayroll.php',
    'auth' => false
);
$entry_point_registry['getAdmin'] = array(
    'file' => 'custom/include/EntryPoints/getAdmin.php',
    'auth' => false
);
$entry_point_registry['loadDiscountPercent'] = array(
    'file' => 'custom/include/EntryPoints/loadDiscountPercent.php',
    'auth' => false
);
$entry_point_registry['getExtension'] = array(
    'file' => 'custom/include/EntryPoints/getExtension.php',
    'auth' => false
);
$entry_point_registry['filterRevenueByCustomer'] = array(
    'file' => 'custom/include/EntryPoints/filterRevenueByCustomer.php',
    'auth' => false
);
$entry_point_registry['filterRevenueByAccount'] = array(
    'file' => 'custom/include/EntryPoints/filterRevenueByAccount.php',
    'auth' => false
);
$entry_point_registry['filterRevenueByInvoice'] = array(
    'file' => 'custom/include/EntryPoints/filterRevenueByInvoice.php',
    'auth' => false
);
$entry_point_registry['getRevenueLanguage'] = array(
    'file' => 'custom/include/EntryPoints/getRevenueLanguage.php',
    'auth' => false
);
$entry_point_registry['getRevenueInvLanguage'] = array(
    'file' => 'custom/include/EntryPoints/getRevenueInvLanguage.php',
    'auth' => false
);
$entry_point_registry['getRevenueCustomerLanguage'] = array(
    'file' => 'custom/include/EntryPoints/getRevenueCustomerLanguage.php',
    'auth' => false
);
$entry_point_registry['testAccount'] = array(
    'file' => 'custom/include/EntryPoints/testAccount.php',
    'auth' => false
);
$entry_point_registry['searchProduct'] = array(
    'file' => 'custom/include/EntryPoints/searchProduct.php',
    'auth' => false
);
$entry_point_registry['searchProduct'] = array(
    'file' => 'custom/include/EntryPoints/searchProduct.php',
    'auth' => false
);
$entry_point_registry['getRevenueStatus'] = array(
    'file' => 'custom/include/EntryPoints/getRevenueStatus.php',
    'auth' => false
);
$entry_point_registry['getProductForUser'] = array(
    'file' => 'custom/include/EntryPoints/getProductForUser.php',
    'auth' => false
);
$entry_point_registry['getCategory'] = array(
    'file' => 'custom/include/EntryPoints/getCategory.php',
    'auth' => false
);

$entry_point_registry['getCategoryParent'] = array(
    'file' => 'custom/include/EntryPoints/getCategoryParent.php',
    'auth' => false
);

$entry_point_registry['filterRevenueNotTransaction'] = array(
    'file' => 'custom/include/EntryPoints/filterRevenueNotTransaction.php',
    'auth' => false
);
$entry_point_registry['getRevenueNotTransaction'] = array(
    'file' => 'custom/include/EntryPoints/getRevenueNotTransaction.php',
    'auth' => false
);
$entry_point_registry['activitiesLanguage'] = array(
    'file' => 'custom/include/EntryPoints/activitiesLanguage.php',
    'auth' => false
);
$entry_point_registry['CustomReport'] = array(
    'file' => 'custom/include/EntryPoints/CustomReport.php',
    'auth' => false
);
$entry_point_registry['Aos_product_listview_sizes'] = array(
    'file' => 'custom/include/EntryPoints/Aos_product_listview_sizes.php',
    'auth' => false
);

$entry_point_registry['Aos_pos_form_sizes'] = array(
    'file' => 'custom/include/EntryPoints/Aos_pos_form_sizes.php',
    'auth' => false
);
$entry_point_registry['Aos_pos_packing_size'] = array(
    'file' => 'custom/include/EntryPoints/Aos_pos_packing_size.php',
    'auth' => false
);

$entry_point_registry['user_active'] = array(
    'file' => 'custom/include/EntryPoints/user_active.php',
    'auth' => false
);
$entry_point_registry['get_image_product'] = array(
    'file' => 'custom/include/EntryPoints/get_image_product.php',
    'auth' => false
);

$entry_point_registry['display_phone'] = array(
    'file' => 'custom/include/EntryPoints/display_phone.php',
    'auth' => false
);

$entry_point_registry['getCategoryParent_v2'] = array(
    'file' => 'custom/include/EntryPoints/getCategoryParent_v2.php',
    'auth' => false
);

$entry_point_registry['inventory'] = array(
    'file' => 'custom/include/EntryPoints/inventory.php',
    'auth' => false
);

$entry_point_registry['link_inventory_detail'] = array(
    'file' => 'custom/include/EntryPoints/link_inventory_detail.php',
    'auth' => false
);

$entry_point_registry['pino_name_product'] = array(
    'file' => 'custom/include/EntryPoints/pino_name_product.php',
    'auth' => false
);

$entry_point_registry['display_phone_contact'] = array(
    'file' => 'custom/include/EntryPoints/display_phone_contact.php',
    'auth' => false
);

$entry_point_registry['display_pos_itmes'] = array(
    'file' => 'custom/include/EntryPoints/display_pos_itmes.php',
    'auth' => false
);



$entry_point_registry['get_voucher_code'] = array(
    'file' => 'custom/include/EntryPoints/get_voucher_code.php',
    'auth' => false
);

$entry_point_registry['check_voucher_code'] = array(
    'file' => 'custom/include/EntryPoints/check_voucher_code.php',
    'auth' => false
);

$entry_point_registry['get_info_passenger'] = array(
    'file' => 'custom/include/EntryPoints/get_info_passenger.php',
    'auth' => false
);

$entry_point_registry['get_num_booking_code'] = array(
    'file' => 'custom/include/EntryPoints/get_num_booking_code.php',
    'auth' => false
);
$entry_point_registry['get_type_datetime'] = array(
    'file' => 'custom/include/EntryPoints/get_type_datetime.php',
    'auth' => false
);
$entry_point_registry['get_info_inputsearch'] = array(
    'file' => 'custom/include/EntryPoints/get_info_inputsearch.php',
    'auth' => false
);
$entry_point_registry['get_info_accounts'] = array(
    'file' => 'custom/include/EntryPoints/get_info_accounts.php',
    'auth' => false
);
$entry_point_registry['get_info_inputsearch_spa'] = array(
    'file' => 'custom/include/EntryPoints/get_info_inputsearch_spa.php',
    'auth' => false
);

$entry_point_registry['get_info_psg_search'] = array(
    'file' => 'custom/include/EntryPoints/get_info_psg_search.php',
    'auth' => false
);