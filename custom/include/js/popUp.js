function filterKey() {
  var input, filter, ul, li, a, i;
  input = document.getElementById("myInputType");
  filter = input.value.toUpperCase();
  div = document.getElementById("type_advanced");
  a = div.getElementsByTagName("option");
  for (i = 0; i < a.length; i++) {
    txtValue = a[i].textContent || a[i].innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      a[i].style.display = "";
    } else {
      a[i].style.display = "none";
    }
  }
}
$(document).ready(function(){
	var html = '';
	html += '<div><input style="width:150px;" placeholder="Search.." id="myInputType" onkeyup="filterKey()"></div>';
	var advanSearch = document.getElementsByClassName("col-advanced-search");
	var formItem = advanSearch[2].getElementsByClassName("form-item");
	formItem[0].innerHTML =  html + formItem[0].innerHTML ;
});
