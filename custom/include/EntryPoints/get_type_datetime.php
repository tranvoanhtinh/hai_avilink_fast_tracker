  <?php

    require_once 'include/TimeDate.php';
    $time = new TimeDate(); 
    $typetime = $time->get_time_format();
    $typedate = $time->get_date_format();

    $date = array (
        'date'=> $typedate,
        'time'=> $typetime,
    ); 
    echo json_encode($date);