
<?php
// Kiểm tra xem yêu cầu là POST hay không
if ($_SERVER["REQUEST_METHOD"] === "POST") {
    global $db;

    // Kiểm tra xem có giá trị 'id' được gửi từ yêu cầu không
    if (isset($_POST['id'])) {
        // Lấy giá trị 'id' từ yêu cầu
        $id = $_POST['id'];
        $sql = "SELECT aos_packing.*
                FROM aos_packing
                JOIN aos_products_aos_packing_1_c ON aos_packing.id = aos_products_aos_packing_1_c.aos_products_aos_packing_1aos_packing_idb 
                JOIN aos_products ON aos_products_aos_packing_1_c.aos_products_aos_packing_1aos_products_ida = aos_products.id 
                WHERE aos_products.id = '".$id."' AND aos_packing.deleted = 0";

        $result = $db->query($sql);

        $packings = array(); // Tạo mảng để lưu trữ kích thước

        while ($row = $result->fetch_assoc()) {
            // Thêm mỗi kích thước vào mảng với key là 'size' + số đếm
            if($row['packing_l_cm'] != null && $row['packing_w_cm'] != null && $row['packing_h_cm'] != null){
            $packings[] = array(
                'packing_l' => customFormat($row['packing_l_cm']),
                'packing_w' => customFormat($row['packing_w_cm']),
                'packing_h' => customFormat($row['packing_h_cm']),
            );
            }
        }



        // Gửi mảng kích thước dưới dạng JSON
        header('Content-Type: application/json');
        if($packings){
        echo json_encode($packings);
        }else {
        echo json_encode($packings);
        }
    }
}
function customFormat($number) {
    // Định dạng số với một số thập phân
    return number_format((float) $number, 1);
}
?>

