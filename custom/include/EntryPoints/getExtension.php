<?php
function getDateRangeToSearchForAmount($period_list_selected = 'this_month')
{
        if(empty($period_list_selected)) $period_list_selected = 'Tháng này';
        switch ($period_list_selected) {
            case 'Tuần này':
                $datetimeTo = new DateTime("this week sunday");
               // $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("this week monday");
              //  $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Tuần trước':
                $datetimeTo = new DateTime("last week sunday");
               // $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("last week monday");
               // $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Tháng này':
                $datetimeTo = new DateTime('last day of this month');
              //  $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("first day of this month");
              //  $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Tháng trước':
                $datetimeTo = new DateTime("last day of last month");
              //  $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("first day of last month");
              //  $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Năm này':
                $datetimeTo = new DateTime('this year last day of december');
             //   $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime('this year first day of january');
             //   $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Năm trước':
                $datetimeTo = new DateTime('last year last day of december');
              //  $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime('last year first day of january');
              //  $datetimeFrom->setTime(0, 0, 0);
                break;

        }
        return array(
            'from' => $datetimeFrom,
            'to' => $datetimeTo
        );
}
function getDateTimeAsDbForAmount($period = 'this_month', $date_from = '', $date_to = '')
{
        
		$timedate = new TimeDate();
		date_default_timezone_set('Asia/Ho_Chi_Minh');
        if(empty($date_from) && empty($date_to)) {
            $range = getDateRangeToSearchForAmount($period);

            $from_by_user = $range['from']->format($timedate::DB_DATETIME_FORMAT);
            $to_by_user = $range['to']->format($timedate::DB_DATETIME_FORMAT);

        }
        else {

            $fromObj = $timedate->fromUserDate($date_from);
            $fromObj->setTime(0, 0, 0);
            $from_by_user = $fromObj->format($timedate::DB_DATETIME_FORMAT);

            $toObj = $timedate->fromUserDate($date_to);
            $toObj->setTime(23,59,59);
            $to_by_user = $toObj->format($timedate::DB_DATETIME_FORMAT);

        }

        $from_display = $timedate->to_display_date_time($from_by_user, true, false);
        $from = $timedate->to_db($from_display);

        $to_display = $timedate->to_display_date_time($to_by_user, true, false);
        $to = $timedate->to_db($to_display);

        $datetime_as_db = array(
            'from' => $from,
            'to' => $to
        );

        return $datetime_as_db;
}

if($_SERVER['REQUEST_METHOD'] == 'POST'){
	try{
	global $db;
	$arr = array();
	$whr='';
	$sea = "";
	$myExt = "";
	$start= ($_POST["page"] -1) * 200;
	if(isset($_POST['time']) && $_POST['time'] != "Tất cả"){
		$time = getDateTimeAsDbForAmount($_POST['time']);
		$from=date('Y-m-d', strtotime($time['from']));
		$to=date('Y-m-d', strtotime($time['to']));
		if(date('m', strtotime($time['to'])) == "02" && $_POST['time'] == "Tháng này"){
			$to = date('Y', strtotime($time['to']))."-02-29";
		}
		$wh = " and tttt_worldfonepbx.date_entered >= '".$from."' and tttt_worldfonepbx.date_entered <= '".$to."'";
	}else{
		$wh= '';	
	}
	if($_POST['myExt'] == "1"){
		$myExt = " and tttt_worldfonepbx.assigned_user_id = '".$_POST["user"]."'";
	}
	if(isset($_POST['search'])){
		if($_POST['search'] != ""){
			$sea .= " and (tttt_worldfonepbx.name like '%".$_POST['search']."%' OR tttt_worldfonepbx.description like '%".$_POST['search']."%')";
		}
	}
	$where = "tttt_worldfonepbx.deleted = '0' and tttt_worldfonepbx_cstm.status_c = '1' and type_sys_app = '4' ".$wh.$sea.$myExt;
	global $current_user;
	$current_user->id = $_POST["user"];
	$phone = new tttt_WorldfonePBX();
	//$lead->load_relationship('user_name');
	$query = $phone->create_new_list_query("tttt_worldfonepbx.date_entered DESC",$where,array());
	$query = $query." order by tttt_worldfonepbx.date_entered DESC";
	$queryCount = "select count(tttt_worldfonepbx.id) as num from tttt_worldfonepbx inner join ($query) as abc on tttt_worldfonepbx.id = abc.id where tttt_worldfonepbx.deleted = '0'";
	$resCount = $db->query($queryCount);
	$rowCount = $db->fetchByAssoc($resCount);
    $limit_query = $phone->db->limitQuery($query, $start,200, false, '', false);
    $res = $phone->db->query($limit_query);
	while($row = $phone->db->fetchByAssoc($res)){
		$row["length"] = $rowCount["num"];
		$arr[] = $row;
	}
	echo json_encode($arr);
	}
	catch(PDOException $e) {
		echo "Error: " . $e->getMessage();
	}
}