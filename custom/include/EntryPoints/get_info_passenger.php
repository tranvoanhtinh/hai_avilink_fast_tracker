<?php
global $db;

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    if(isset($_POST['passport'])) {
        $passport = $_POST['passport'];

        $sql = "SELECT * FROM aos_passengers WHERE passport = '" . $db->quote($passport) . "' AND deleted = '0' ";

        $result = $db->query($sql);
        if ($result && $result->num_rows > 0) {
            $row = $result->fetch_assoc();
            $info = array(
                'id' => $row['id'],
                'name' => $row['name'],
                'nationality' => $row['nationality'],
            );
            echo json_encode($info);
        } else {
            echo -1; // Không tìm thấy hành khách
        }
    } elseif ($_POST['status'] === 'get_passenger_passport') {
        $sql = "SELECT * FROM aos_passengers WHERE deleted = '0'";
        $result = $db->query($sql);
        $infos = array();
        if ($result && $result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $info = array(
                    'id'=> $row['id'],
                    'name' => $row['name'],
                    'passport' => $row['passport'],
                    'nationality' => $row['nationality'],
                );
                $infos[] = $info;
            }
            echo json_encode($infos);
        } else {
            echo -1; // Không có hành khách nào
        }
    }
}
?>
