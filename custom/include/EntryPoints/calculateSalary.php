<?php
global $db;
global $sugar_config;
$arr = array();
$user = BeanFactory::newBean('cs_cusUser');
$where= " and pr_payrollparam.month_num = '4' and pr_payrollparam.year_num = '2024'";
$query = $user->create_new_list_query("cs_cususer.date_entered DESC","cs_cususer.status = 'Active'",array());
$query = $query." order by cs_cususer.date_entered DESC";
$res = $user->db->query($query);
// tính toán theo từng user

while($row = $user->db->fetchByAssoc($res)){

    $q  = "select pr_payrollparam.* from pr_payrollparam inner join pr_payrollparam_cs_cususer_c on pr_payrollparam.id = pr_payrollparam_cs_cususer_c.pr_payrollparam_cs_cususerpr_payrollparam_ida inner join cs_cususer on pr_payrollparam_cs_cususer_c.pr_payrollparam_cs_cususercs_cususer_idb = cs_cususer.id and cs_cususer.deleted = '0' where cs_cususer.id = '".$row["id"]."' and pr_payrollparam.deleted = '0'".$where;
    $res2 = $db->query($q);

  
    $row2 = $db->fetchByAssoc($res2);
    $q8  = "select pr_payrollfile.* from pr_payrollfile inner join pr_payrollfile_cs_cususer_c on pr_payrollfile.id = pr_payrollfile_cs_cususer_c.pr_payrollfile_cs_cususerpr_payrollfile_ida and   pr_payrollfile_cs_cususer_c.deleted = '0' where pr_payrollfile_cs_cususer_c.pr_payrollfile_cs_cususercs_cususer_idb = '".$row["id"]."' and pr_payrollfile.deleted = '0'";
    $res8 = $db->query($q8);
 

    $row8 = $db->fetchByAssoc($res8);
    $q3  = "select pr_payrollfile.* from pr_payrollfile inner join pr_payrollfile_cs_cususer_c on pr_payrollfile.id = pr_payrollfile_cs_cususer_c.pr_payrollfile_cs_cususerpr_payrollfile_ida and pr_payrollfile_cs_cususer_c.deleted ='0' inner join cs_cususer on pr_payrollfile_cs_cususer_c.pr_payrollfile_cs_cususercs_cususer_idb =   cs_cususer.id and cs_cususer.deleted = '0' where pr_payrollfile.deleted = '0' and cs_cususer.id = '".$row["id"]."'";
    $res3 = $db->query($q3);
 
    $row3 = $db->fetchByAssoc($res3);

    $q6 = "select lm_leavemanagerment.status as leaveStatus ,DAY(lm_leavemanagerment.fromdate) as leaveFromDate,DAY(lm_leavemanagerment.todate) as leaveToDate,lm_leavemanagerment.coefficient,lm_leavemanagerment.reason from lm_leavemanagerment inner join lm_leavemanagerment_cs_cususer_c on  lm_leavemanagerment.id = lm_leavemanagerment_cs_cususer_c.lm_leavemanagerment_cs_cususerlm_leavemanagerment_idb and lm_leavemanagerment_cs_cususer_c.deleted = '0' where lm_leavemanagerment.deleted = '0' and  (MONTH(lm_leavemanagerment.fromdate) = '4' and YEAR(lm_leavemanagerment.fromdate) = '2024' and  MONTH(lm_leavemanagerment.todate) = '4' and  YEAR(lm_leavemanagerment.todate) = '2024') and (lm_leavemanagerment.reason= '2' OR lm_leavemanagerment.reason= '4')  and lm_leavemanagerment_cs_cususer_c.lm_leavemanagerment_cs_cususercs_cususer_ida = '".$row["id"]."'";
    $res6 = $db->query($q6);

    $q7 = "select pr_payrollfile.* from pr_payrollfile inner join pr_payrollfile_cs_cususer_c on pr_payrollfile.id = pr_payrollfile_cs_cususer_c.pr_payrollfile_cs_cususerpr_payrollfile_ida and pr_payrollfile_cs_cususer_c.deleted = '0' where pr_payrollfile_cs_cususer_c.pr_payrollfile_cs_cususercs_cususer_idb = '".$row["id"]."'";
    $res7 = $db->query($q7);
 
    $row7 = $db->fetchByAssoc($res7);
    if(empty($row7["workinghours"])){
        $row7["workinghours"] = 0;
    }
    $ot = $row2["dayovertime"]?$row2["dayovertime"]:0;
    $allLeave = 0;
    while($row6 = $db->fetchByAssoc($res6)){

        if($row6["reason"] == "2" ){
            $leave = (($row6["leaveToDate"] - $row6["leaveFromDate"])+1);
            $allLeave += ($leave * $row6["coefficient"]  * $row7["workinghours"]);
        }
        if($row6["reason"] == "4" ){
            $overtime = (($row6["leaveToDate"] - $row6["leaveFromDate"])+1);
            //ngày công tăng ca
            $ot += ($row7["workinghours"] * $overtime * $row6["coefficient"]);
        }
    }
    // lương cơ bản
    if(!empty($row3["salary_amount_basic"]) && !empty($row3["workday"]) && !empty($row7["workinghours"])){
        $salary_basic = $row3["salary_amount_basic"] / $row3["workday"] / $row7["workinghours"];
    }else{
        $salary_basic = 0;
    }
    // ngày công thực tế
    $real_workingday = $row8["workday"] * $row7["workinghours"] - ($allLeave) - $row2["dayleave"];

    // lương làm việc
    $worksalary = $real_workingday * $salary_basic;

    //lương tăng ca
    $overtimeSalary  = $salary_basic * $ot;
    //workday payroll
    $payrollWorkday = $row7["workday"] * $row7["workinghours"];
    //leave payroll
    $payrollLeave = $allLeave + $row2["dayleave"];
    //trợ cấp
    $allowance = ($row3["allowance1"]?$row3["allowance1"]:0) + ($row3["allowance2"]?$row3["allowance2"]:0) + ($row3["allowance3"]?$row3["allowance3"]:0) + ($row3["allowance4"]?$row3["allowance4"]:0) + ($row2["allowance1"]?$row2["allowance1"]:0) + ($row2["allowance2"]?$row2["allowance2"]:0) + ($row2["allowance3"]?$row2["allowance3"]:0);
    // thưởng
    $bonus = ($row2["bonus1"]?$row2["bonus1"]:0) + ($row2["bonus2"]?$row2["bonus2"]:0) + ($row2["bonus3"]?$row2["bonus3"]:0);
    // phạt
    $amercement = ($row2["amercement1"]?$row2["amercement1"]:0) + ($row2["amercement2"]?$row2["amercement2"]:0) + ($row2["amercement3"]?$row2["amercement3"]:0);
    //tạm ứng
    $queryAdvanSalary = "select bill_billing.*,sum(bill_billing.billingamount) as advance_salary from bill_billing inner join cs_cususer_bill_billing_1_c on bill_billing.id = cs_cususer_bill_billing_1_c.cs_cususer_bill_billing_1bill_billing_idb  and bill_billing.type = '3' where cs_cususer_bill_billing_1_c.cs_cususer_bill_billing_1cs_cususer_ida = '".$row["id"]."' and cs_cususer_bill_billing_1_c.deleted = '0' and bill_billing.billing_status = 'completed' and MONTH(billingdate) = '4' and YEAR(billingdate) = '2024'";

    $resAdvance = $db->query($queryAdvanSalary);

    
    $advance_salary_row = $db->fetchByAssoc($resAdvance);
    $advance_salary = $advance_salary_row["advance_salary"]?$advance_salary_row["advance_salary"]:0;
    // commission service
    $qPro = "select commisonservice_c from aos_products_quotes_cstm inner join aos_products_quotes on aos_products_quotes_cstm.id_c = aos_products_quotes.id and aos_products_quotes.deleted = '0' inner join aos_invoices on aos_products_quotes.parent_id = aos_invoices.id where commissrecipient_c = '".$row["id"]."' and aos_invoices.deleted =   '0' and MONTH(aos_invoices.invoice_date) = '4' and YEAR(aos_invoices.invoice_date) = '2024'";
    $resPro = $db->query($qPro);

   
    $totalPro = 0;
    while($rowPro = $db->fetchByAssoc($resPro)){
        if(!empty($rowPro["commisonservice_c"])){
            $totalPro += $rowPro["commisonservice_c"];
        }
    }

    $invoices = "select aos_invoices.*,count(aos_invoices_cs_cususer_1_c.aos_invoices_cs_cususer_1cs_cususer_idb) as numUser from aos_invoices inner join aos_invoices_cs_cususer_1_c on aos_invoices.id = aos_invoices_cs_cususer_1_c.aos_invoices_cs_cususer_1aos_invoices_ida  and aos_invoices_cs_cususer_1_c.deleted = '0' where aos_invoices.deleted =   '0' and MONTH(invoice_date) = '4' and YEAR(invoice_date) = '2024' group by (aos_invoices_cs_cususer_1_c.aos_invoices_cs_cususer_1aos_invoices_ida)";
    $res3 = $db->query($invoices);

    $commison_service = 0;

    while($row3 = $db->fetchByAssoc($res3)){
        $invoices5 = "select aos_invoices.*,aos_invoices_cs_cususer_1_c.aos_invoices_cs_cususer_1aos_invoices_ida  from aos_invoices inner join aos_invoices_cs_cususer_1_c on aos_invoices.id = aos_invoices_cs_cususer_1_c.aos_invoices_cs_cususer_1aos_invoices_ida  and aos_invoices_cs_cususer_1_c.deleted = '0' where aos_invoices_cs_cususer_1_c.aos_invoices_cs_cususer_1cs_cususer_idb =   '".$row["id"]."' and aos_invoices.deleted =   '0' and MONTH(invoice_date) = '4' and YEAR(invoice_date) = '2024' group by (aos_invoices_cs_cususer_1_c.aos_invoices_cs_cususer_1aos_invoices_ida)";
        $res5 = $db->query($invoices5);

                if($res5){
  
    }
        while($row5 = $db->fetchByAssoc($res5)){
            if(!empty($row3["id"]) && ($row5["aos_invoices_cs_cususer_1aos_invoices_ida"] == $row3["id"])){
                $product = "select aos_products_quotes.*,aos_products_quotes_cstm.* from aos_products_quotes inner join aos_products_quotes_cstm on  aos_products_quotes.id = aos_products_quotes_cstm.id_c where aos_products_quotes.parent_id = '".$row3["id"]."' and aos_products_quotes.deleted = '0'";
                $res4 = $db->query($product);
                        if($res4){
    
    }
                while($row4 = $db->fetchByAssoc($res4)){
                    if(!empty($row4["commisonservice_c"])){
                        if($row3["numUser"] > 0){
                            $commison_service += ($row4["commisonservice_c"]/$row3["numUser"]);
                        }else{
                            $commison_service += 0;
                        }
                    }
                }


            }
        }
    }


    $commison_service += $totalPro;
    // commission action
    $q5 = "select spa_actionservice.*,sum(spa_actionservice.commission_action) as commisonAction from spa_actionservice inner join spa_actionservice_cs_cususer_c on spa_actionservice.id = spa_actionservice_cs_cususer_c.spa_actionservice_cs_cususerspa_actionservice_idb and spa_actionservice_cs_cususer_c.deleted = '0' and spa_actionservice_cs_cususercs_cususer_ida = '".$row["id"]."' and status = 'Completed' and MONTH(action_date) = '4' and YEAR(action_date) = '2024'";
    $res5 = $res5 = $db->query($q5);

    
    $rowCommisionAction = $db->fetchByAssoc($res5);
    $commison_action = $rowCommisionAction["commisonAction"];
    // thực lãnh

    $net_salary =  $worksalary + $overtimeSalary + $allowance + $bonus + $commison_action + $commison_service - ($amercement + $advance_salary);
    $arr[] = array(
        "id"=>$row["id"],
        "username"=>$row["username"],
        "user"=>$row["username"]."-".$row["name"],
        "basic_salary"=>$salary_basic,
        "workday"=>$payrollWorkday,
        "dayleave"=>$payrollLeave,
        "real_workingday"=>$real_workingday,
        "work_salary"=>$worksalary,
        "overtime"=>$ot,
        "overtime_salary"=>$overtimeSalary,
        "allowance"=>$allowance,
        "bonus"=>$bonus,
        "amercement"=>$amercement,
        "advance_salary"=>$advance_salary,
        "commision_service"=>$commison_service,
        "commision_action"=>$commison_action,
        "net_salary"=>$net_salary,
        "note"=>"",
    );

}
 
array_walk_recursive($arr, function (&$value) {
    if ($value === INF || $value === -INF || is_nan($value)) {
        $value = null;
    }
});

// Encode the array to JSON
$json = json_encode($arr);

// Check if JSON encoding was successful
if ($json === false) {
    echo "Error encoding JSON: " . json_last_error_msg();
} else {
    // Output the JSON
    echo $json;
}

