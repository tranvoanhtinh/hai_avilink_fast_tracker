<?php
//global $current_user;
global $db;
$whr='';
$q = " select id from users where id  = '".$_SESSION['authenticated_user_id']."' and is_admin = '1' and deleted = '0'";
$re = $db->query($q);
$r= $db->fetchByAssoc($re);
if(empty($r["id"])){
	$q2 = " select cs_cususer.id from cs_cususer inner join acl_roles on cs_cususer.role = acl_roles.id 
	 where cs_cususer.refuser ='".$_SESSION['authenticated_user_id']."' and acl_roles.name <> 'Role cho nhan vien' and cs_cususer.deleted = '0'";
	$re2 = $db->query($q2);
	$r2= $db->fetchByAssoc($re2);
	if(empty($r2["id"])){
	$employee = true;
	}
}else{
	$employee = false;
	
}

//$curr_id=$current_user->id;
$query="SELECT securitygroup_id FROM securitygroups_users where user_id='".$_SESSION['authenticated_user_id']."' and deleted = '0'";
$res=$db->query($query);
while($row=$db->fetchByAssoc($res)){
	$whr.="'".$row['securitygroup_id']."'";
	$whr.=",";	
}

$whr=trim($whr,",");

function getDateTimeAsDb($period = 'this_month', $date_from = '', $date_to = '')
    {
        global $timedate;
		date_default_timezone_set('Asia/Ho_Chi_Minh');
        if(empty($date_from) && empty($date_to)) {
            $range = getDateRangeToSearch($period);

            $from_by_user = $range['from']->format($timedate::DB_DATETIME_FORMAT);
            $to_by_user = $range['to']->format($timedate::DB_DATETIME_FORMAT);

        }
        else {

            $fromObj = $timedate->fromUserDate($date_from);
            $fromObj->setTime(0, 0, 0);
            $from_by_user = $fromObj->format($timedate::DB_DATETIME_FORMAT);

            $toObj = $timedate->fromUserDate($date_to);
            $toObj->setTime(23,59,59);
            $to_by_user = $toObj->format($timedate::DB_DATETIME_FORMAT);

        }

        $from_display = $timedate->to_display_date_time($from_by_user, true, false);
        $from = $timedate->to_db($from_display);

        $to_display = $timedate->to_display_date_time($to_by_user, true, false);
        $to = $timedate->to_db($to_display);

        $datetime_as_db = array(
            'from' => $from,
            'to' => $to
        );

        return $datetime_as_db;
    }
function getDateRangeToSearch($period_list_selected = 'this_month')
    {
        if(empty($period_list_selected)) $period_list_selected = 'this_month';
		date_default_timezone_set('Asia/Ho_Chi_Minh');
        switch ($period_list_selected) {
            case 'this_week':
                $datetimeTo = new DateTime("next week monday");
              //  $datetimeTo->setTime(23, 59, 59);
			   
                $datetimeFrom = new DateTime("this week tuesday");
				 
               // $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'last_week':
                $datetimeTo = new DateTime("this week monday");
              //  $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("last week tuesday");
              //  $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'this_month':
                $datetimeTo = new DateTime('last day of this month');
               // $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("first day of this month");
               // $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'last_month':
                $datetimeTo = new DateTime("last day of last month");
                //$datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("first day of last month");
               // $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'this_year':
                $datetimeTo = new DateTime('this year last day of december');
                //$datetimeTo->setTime(23, 59, 59);
				$datetimeTo->add(new DateInterval('P1D'));
                $datetimeFrom = new DateTime('this year first day of january');
               // $datetimeFrom->setTime(0, 0, 0);
			    $datetimeFrom->add(new DateInterval('P1D'));
                break;
            case 'last_year':
                $datetimeTo = new DateTime('last year last day of december');
				$datetimeTo->add(new DateInterval('P1D'));
               // $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime('last year first day of january');
               // $datetimeFrom->setTime(0, 0, 0);
			    $datetimeFrom->add(new DateInterval('P1D'));
                break;

        }
        return array(
            'from' => $datetimeFrom,
            'to' => $datetimeTo
        );
    }
	
$prov=array(
"10"=>" TP Hà Nội",
"16"=>"Tỉnh Hưng Yên",
"17"=>"Tỉnh Hải Dương",
"18"=>"Tp Hải Phòng",
"20"=>"Tỉnh Quảng Ninh",
"22"=>"Tỉnh Bắc Ninh",
"23"=>"Tỉnh Bắc Giang",
"24"=>"Tỉnh Lạng Sơn",
"25"=>"Tỉnh Thái Nguyên",
"26"=>"Tỉnh Bắc Kạn",
"27"=>"Tỉnh Cao Bằng",
"28"=>"Tỉnh Vĩnh Phúc",
"29"=>"Tỉnh Phú Thọ",
"30"=>"Tỉnh Tuyên Quang",
"31"=>"Tỉnh Hà Giang",
"32"=>"Tỉnh Yên Bái",
"33"=>"Tỉnh Lào Cai",
"35"=>"Tỉnh Hoà Bình",
"36"=>"Tỉnh Sơn La",
"38"=>"Tỉnh Điện Biên",
"39"=>"Tỉnh Lai Châu",
"40"=>"Tỉnh Hà Nam",
"41"=>"Tỉnh Thái Bình",
"42"=>"Tỉnh Nam Định",
"43"=>"Tỉnh Ninh Bình",
"44"=>"Tỉnh Thanh Hóa",
"46"=>"Tỉnh Nghệ An",
"48"=>"Tỉnh Hà Tĩnh",
"51"=>"Tỉnh Quảng Bình",
"52"=>"Tỉnh Quảng Trị",
"53"=>"Tỉnh Thừa Thiên Huế",
"55"=>"TP Đà Nẵng",
"56"=>"Tỉnh Quảng Nam",
"57"=>"Tỉnh Quảng Ngãi",
"58"=>"Tỉnh Kon Tum",
"59"=>"Tỉnh Bình Định",
"60"=>"Tỉnh Gia Lai",
"62"=>"Tỉnh Phú Yên",
"63"=>"Tỉnh Đắk Lăk",
"64"=>"Tỉnh Đắk Nông",
"65"=>"Tỉnh Khánh Hoà",
"66"=>"Tỉnh Ninh Thuận",
"67"=>"Tỉnh Lâm Đồng",
"68"=>"Tỉnh Hưng Yên",
"69"=>"Tỉnh Hưng Yên",
"70"=>"TP Hồ Chí Minh",
"79"=>"Tỉnh Bà Rịa Vũng Tàu",
"80"=>"Tỉnh Bình Thuận",
"81"=>"Tỉnh Đồng Nai",
"82"=>"Tỉnh Bình Dương",
"83"=>"Tỉnh Bình Phước",
"84"=>"Tỉnh Tây Ninh",
"85"=>"Tỉnh Long An",
"86"=>"Tỉnh Tiền Giang",
"87"=>"Tỉnh Đồng Tháp",
"88"=>"Tỉnh An Giang",
"89"=>"Tỉnh Vĩnh Long",
"90"=>"TP Cần Thơ",
"91"=>"Tỉnh Hậu Giang",
"92"=>"Tỉnh Kiên Giang",
"93"=>"Tỉnh Bến Tre",
"94"=>"Tỉnh Trà Vinh",
"95"=>"Tỉnh Sóc Trăng",
"96"=>"Tỉnh Bạc Liêu",
"97"=>"Tỉnh Cà Mau",
"98"=>"Vnpost",
"99"=>"PPTT",
"100"=>"DVBC",
"101"=>"TCBC",
"102"=>"HCC"
);

$user=array();
$group=array();
$province=array();
$limit=" Limit 0,20";
//$_POST['province']="10";
//$_POST['groupUsers']="All";

if((!empty($_POST['period'])) && ($_POST['period'] != 'default')){
			//if(empty($_POST['date_from']) && empty($_POST['date_to'])){
				if($_POST['period'] != "is_between"){
			$time=getDateTimeAsDb($_POST['period']);
				}
				else{
					if(empty($_POST['date_from']) && empty($_POST['date_to'])){
					$leadWhere="";
					$accountWhere="";
					$oppWhere="";
					$callWhere="";
					$meetingWhere="";
					$emailWhere="";
					}else{
						if(!empty($_POST['date_from']) && !empty($_POST['date_to'])){
							if(strtotime($_POST['date_from']) != strtotime($_POST['date_to'])){
								$from = date('Y-m-d', strtotime(str_replace("/","-",$_POST['date_from'])));	
								$to = date('Y-m-d', strtotime(str_replace("/","-",$_POST['date_to'])));	
							}else{
								$from1=date('Y-m-d', strtotime(str_replace("/","-",$_POST['date_from'])));	
							}
						}
					}
				}
				//}else{
			//$time=getDateTimeAsDb($_POST['period'],$_POST['date_from'],$_POST['date_to']);
			//	}
			if($time){
			$from=date('Y-m-d', strtotime($time['from']));
			$to=date('Y-m-d', strtotime($time['to']));
			}
			/*
			$leadWhere=" And FORMAT(leads.date_entered,'YYYY-mm-dd') >='".$from."' And FORMAT(leads.date_entered,'YYYY-mm-dd') <='".$to."'";
			$accountWhere=" And FORMAT(accounts.date_entered,'YYYY-mm-dd') >='".$from."' And FORMAT(accounts.date_entered,'YYYY-mm-dd') <='".$to."'";
			$oppWhere=" And FORMAT(opportunities.date_closed,'YYYY-mm-dd') >='".$from."' And FORMAT(opportunities.date_closed,'YYYY-mm-dd') <='".$to."'";
			$callWhere=" And FORMAT(calls.date_entered,'YYYY-mm-dd') >='".$from."' And FORMAT(calls.date_entered,'YYYY-mm-dd') <='".$to."'";
			$meetingWhere=" And FORMAT(meetings.date_entered,'YYYY-mm-dd') >='".$from."' And FORMAT(meetings.date_entered,'YYYY-mm-dd') <='".$to."'";
			$emailWhere=" And FORMAT(emails.date_entered,'YYYY-mm-dd') >='".$from."' And FORMAT(emails.date_entered,'YYYY-mm-dd') <='".$to."'";
			$accountWhereTran=" And FORMAT(opportunities.date_closed,'YYYY-mm-dd') >='".$from."' And FORMAT(opportunities.date_closed,'YYYY-mm-dd') <='".$to."'";
			*/
			if($from1){
			$leadWhere=" And DATE_FORMAT(leads.date_entered,'%Y-%m-%d') ='".$from1."'";
			$accountWhere=" And DATE_FORMAT(accounts.date_entered,'%Y-%m-%d') ='".$from1."'";
			$oppWhere=" And DATE_FORMAT(opportunities.date_closed,'%Y-%m-%d') ='".$from1."'";
			$callWhere=" And DATE_FORMAT(calls.date_entered,'%Y-%m-%d') ='".$from1."'";
			$meetingWhere=" And DATE_FORMAT(meetings.date_entered,'%Y-%m-%d') ='".$from1."'";
			$emailWhere=" And DATE_FORMAT(emails.date_entered,'%Y-%m-%d') ='".$from1."'";
			$accountWhereTran=" And DATE_FORMAT(opportunities.date_closed,'%Y-%m-%d') ='".$from1."'";
			}else{
			$leadWhere=" And DATE_FORMAT(leads.date_entered,'%Y-%m-%d') >='".$from."' And DATE_FORMAT(leads.date_entered,'%Y-%m-%d') <='".$to."'";
			$accountWhere=" And DATE_FORMAT(accounts.date_entered,'%Y-%m-%d') >='".$from."' And DATE_FORMAT(accounts.date_entered,'%Y-%m-%d') <='".$to."'";
			$oppWhere=" And DATE_FORMAT(opportunities.date_closed,'%Y-%m-%d') >='".$from."' And DATE_FORMAT(opportunities.date_closed,'%Y-%m-%d') <='".$to."'";
			$callWhere=" And DATE_FORMAT(calls.date_entered,'%Y-%m-%d') >='".$from."' And DATE_FORMAT(calls.date_entered,'%Y-%m-%d') <='".$to."'";
			$meetingWhere=" And DATE_FORMAT(meetings.date_entered,'%Y-%m-%d') >='".$from."' And DATE_FORMAT(meetings.date_entered,'%Y-%m-%d') <='".$to."'";
			$emailWhere=" And DATE_FORMAT(emails.date_entered,'%Y-%m-%d') >='".$from."' And DATE_FORMAT(emails.date_entered,'%Y-%m-%d') <='".$to."'";
			$accountWhereTran=" And DATE_FORMAT(opportunities.date_closed,'%Y-%m-%d') >='".$from."' And DATE_FORMAT(opportunities.date_closed,'%Y-%m-%d') <='".$to."'";
			}
}else{
	$leadWhere="";
	$accountWhere="";
	$oppWhere="";
	$callWhere="";
	$meetingWhere="";
	$emailWhere="";
}
	if($_POST["province"] != null){
		if($_POST["province"] != "All"){
			$New=0;
			$InProcess=0;
			$Converted=0;
			$Transaction=0;
			$CloseWon=0;
			$CloseLost=0;
			$Other=0;
			$Dead=0;
			$amountNew=0;
			$amountInProcess=0;
			$amountConverted=0;
			$amountTran=0;
			$amountWon=0;
			$amountLost=0;
			$amountDead=0;
			$amountCall=0;
			$amountMeeting=0;
			$amountEmail=0;
			$amountOther=0;
				if($_POST['groupUsers'] == null || $_POST['groupUsers'] == '' || $_POST['groupUsers'][0] == "All"){
					if($_POST["users"] == null || $_POST["users"] == ''){
						if($_POST["titleUsers"] == null || $_POST["titleUsers"] == ''){
						$qr="select distinct(securitygroups.name) as seName,users.id as id,users.user_name as name,users.department as Se from users inner join users_cstm on users.id=users_cstm.id_c inner join 
						securitygroups_users on users.id=securitygroups_users.user_id inner join  securitygroups on securitygroups.id = securitygroups_users.securitygroup_id where users.deleted='0' and users.status='Active' and users_cstm.provincecode_c='".$_POST["province"]."'
						and securitygroups_users.securitygroup_id In (".$whr.") Order By users.user_name ASC";
						$res=$db->query($qr);
						while($row=$db->fetchByAssoc($res)){
							if(!empty($row)){
								if($row["seName"] == $row["Se"]){
									$query1="select leads.lead_source, leads.date_entered, aos_products.name as pName,leads.last_name,leads.phone_mobile,leads.account_name,campaigns.name,leads.description,leads.status,leads.primary_address_street,users.user_name,email_addresses.email_address  from leads left join aos_products_leads_1_c on leads.id = aos_products_leads_1_c.aos_products_leads_1leads_idb left join aos_products on aos_products_leads_1_c.aos_products_leads_1aos_products_ida = aos_products.id left join campaigns  on leads.campaign_id = campaigns.id 
									left join users on leads.assigned_user_id = users.id left join email_addr_bean_rel on leads.id = email_addr_bean_rel.bean_id left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id where (leads.status = 'New' or leads.status ='In Process') and leads.deleted='0' and leads.assigned_user_id ='".$row['id']."'".$leadWhere;
									$result1=$db->query($query1);
									//$row1 =$db->fetchByAssoc($result1);
									while($row1 =$db->fetchByAssoc($result1)){
										if($employee == true){
											if($row["id"] == $_SESSION['authenticated_user_id']){
												$user=array(
														"lastName"=>$row1['last_name'],
														"phoneMobile"=>$row1["phone_mobile"],
														"accountName"=>$row1["account_name"],
														"name"=>$row1["name"],
														"description"=>$row1["description"],
														"status"=>$row1["status"],
														"primaryAddressStreet"=>$row1["primary_address_street"],
														"userName"=>$row1["user_name"],
														"emailAddress"=>$row1["email_address"],
														"group"=>$row["Se"],
														"product"=>$row1["pName"],
														"source"=>$row1["lead_source"],
														"date"=>date('d/m/Y H:i:s', strtotime($row1["date_entered"])),
													);
												if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
													$group[$row["Se"]]=array();
													$group[$row["Se"]][]=$user;
												}else{
													$group[$row["Se"]][]=$user;
												}
												
											}
										}else{
											$user=array(
														"lastName"=>$row1['last_name'],
														"phoneMobile"=>$row1["phone_mobile"],
														"accountName"=>$row1["account_name"],
														"name"=>$row1["name"],
														"description"=>$row1["description"],
														"status"=>$row1["status"],
														"primaryAddressStreet"=>$row1["primary_address_street"],
														"userName"=>$row1["user_name"],
														"emailAddress"=>$row1["email_address"],
														"group"=>$row["Se"],
														"product"=>$row1["pName"],
														"source"=>$row1["lead_source"],
														"date"=>date('d/m/Y H:i:s', strtotime($row1["date_entered"])),
													);
												if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
													$group[$row["Se"]]=array();
													$group[$row["Se"]][]=$user;
												}else{
													$group[$row["Se"]][]=$user;
												}
												
										}
									}
										
									
								}	
							}
						}
					}else{
						for($i=0;$i<count($_POST["titleUsers"]);$i++){
							$q="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
					 inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
					 users.status='Active' and users_cstm.provincecode_c='".$_POST["province"]."' and securitygroups_users.securitygroup_id In (".$whr.") and users.title = '".$_POST["titleUsers"][$i]."'";
							$r=$db->query($q);
							while($rww=$db->fetchByAssoc($r)){
								if(!empty($rww)){
									if($rww["seName"] == $rww["Se"]){
									$query1="select leads.lead_source, leads.date_entered, aos_products.name as pName,leads.last_name,leads.phone_mobile,leads.account_name,campaigns.name,leads.description,leads.status,leads.primary_address_street,users.user_name,email_addresses.email_address  from leads left join aos_products_leads_1_c on leads.id = aos_products_leads_1_c.aos_products_leads_1leads_idb left join aos_products on aos_products_leads_1_c.aos_products_leads_1aos_products_ida = aos_products.id left join campaigns  on leads.campaign_id = campaigns.id 
									left join users on leads.assigned_user_id = users.id left join email_addr_bean_rel on leads.id = email_addr_bean_rel.bean_id left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id where (leads.status = 'New' or leads.status ='In Process') and leads.deleted='0' and leads.assigned_user_id ='".$rww['id']."'".$leadWhere;
									$result1=$db->query($query1);
									$row1 =$db->fetchByAssoc($result1);
									while($row1 = $db->fetchByAssoc($result1)){
										if($employee == true){
											if($rww["id"] == $_SESSION['authenticated_user_id']){
												$user=array(
														"lastName"=>$row1['last_name'],
														"phoneMobile"=>$row1["phone_mobile"],
														"accountName"=>$row1["account_name"],
														"name"=>$row1["name"],
														"description"=>$row1["description"],
														"status"=>$row1["status"],
														"primaryAddressStreet"=>$row1["primary_address_street"],
														"userName"=>$row1["user_name"],
														"emailAddress"=>$row1["email_address"],
														"group"=>$row["Se"],
														"product"=>$row1["pName"],
														"source"=>$row1["lead_source"],
														"date"=>date('d/m/Y H:i:s', strtotime($row1["date_entered"])),
													);
												if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
													$group[$row["Se"]]=array();
													$group[$row["Se"]][]=$user;
												}else{
													$group[$row["Se"]][]=$user;
												}
												
											}
										}else{
											$user=array(
														"lastName"=>$row1['last_name'],
														"phoneMobile"=>$row1["phone_mobile"],
														"accountName"=>$row1["account_name"],
														"name"=>$row1["name"],
														"description"=>$row1["description"],
														"status"=>$row1["status"],
														"primaryAddressStreet"=>$row1["primary_address_street"],
														"userName"=>$row1["user_name"],
														"emailAddress"=>$row1["email_address"],
														"group"=>$row["Se"],
														"product"=>$row1["pName"],
														"source"=>$row1["lead_source"],
														"date"=>date('d/m/Y H:i:s', strtotime($row1["date_entered"])),
													);
												if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
													$group[$row["Se"]]=array();
													$group[$row["Se"]][]=$user;
												}else{
													$group[$row["Se"]][]=$user;
												}
												
										}
									}
										
									
								}	
								}
							}
						}
						
						
						
					}
						
					}
					
					
					else
					{
						for($i=0;$i<count($_POST["users"]);$i++){
							$qr="select users.user_name as name,users.department as Se from users inner join securitygroups_users on users.id=securitygroups_users.user_id where users.deleted='0' and users.status='Active' and  users.id='".$_POST["users"][$i]."' and securitygroups_users.securitygroup_id In (".$whr.")";	
							$res=$db->query($qr);
							$row=$db->fetchByAssoc($res);
							if(!empty($row)){
									$query1="select leads.lead_source, leads.date_entered, aos_products.name as pName,leads.last_name,leads.phone_mobile,leads.account_name,campaigns.name,leads.description,leads.status,leads.primary_address_street,users.user_name,email_addresses.email_address  from leads left join aos_products_leads_1_c on leads.id = aos_products_leads_1_c.aos_products_leads_1leads_idb left join aos_products on aos_products_leads_1_c.aos_products_leads_1aos_products_ida = aos_products.id left join campaigns  on leads.campaign_id = campaigns.id 
									left join users on leads.assigned_user_id = users.id left join email_addr_bean_rel on leads.id = email_addr_bean_rel.bean_id left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id where (leads.status = 'New' or leads.status ='In Process') and leads.deleted='0' and leads.assigned_user_id ='".$_POST["users"][$i]."'".$leadWhere;
									$result1=$db->query($query1);
									//$row1 =$db->fetchByAssoc($result1);
									while($row1 =$db->fetchByAssoc($result1)){
										if($employee == true){
											if($row["id"] == $_SESSION['authenticated_user_id']){
												$user=array(
														"lastName"=>$row1['last_name'],
														"phoneMobile"=>$row1["phone_mobile"],
														"accountName"=>$row1["account_name"],
														"name"=>$row1["name"],
														"description"=>$row1["description"],
														"status"=>$row1["status"],
														"primaryAddressStreet"=>$row1["primary_address_street"],
														"userName"=>$row1["user_name"],
														"emailAddress"=>$row1["email_address"],
														"group"=>$row["Se"],
														"product"=>$row1["pName"],
														"source"=>$row1["lead_source"],
														"date"=>date('d/m/Y H:i:s', strtotime($row1["date_entered"])),
													);
												if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
													$group[$row["Se"]]=array();
													$group[$row["Se"]][]=$user;
												}else{
													$group[$row["Se"]][]=$user;
												}
												
											}
										}else{
											$user=array(
														"lastName"=>$row1['last_name'],
														"phoneMobile"=>$row1["phone_mobile"],
														"accountName"=>$row1["account_name"],
														"name"=>$row1["name"],
														"description"=>$row1["description"],
														"status"=>$row1["status"],
														"primaryAddressStreet"=>$row1["primary_address_street"],
														"userName"=>$row1["user_name"],
														"emailAddress"=>$row1["email_address"],
														"group"=>$row["Se"],
														"product"=>$row1["pName"],
														"source"=>$row1["lead_source"],
														"date"=>date('d/m/Y H:i:s', strtotime($row1["date_entered"]))
													);
												if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
													$group[$row["Se"]]=array();
													$group[$row["Se"]][]=$user;
												}else{
													$group[$row["Se"]][]=$user;
												}
												
										}
									}
										
									
									
							}
						}
					}	
					
				}
				
				else
				{
					
					if($_POST["users"] == null || $_POST["users"] == ''){
						if($_POST["titleUsers"] == null || $_POST["titleUsers"] == ''){
						for($j=0;$j<count($_POST["groupUsers"]);$j++){
						$grs.="'".$_POST["groupUsers"][$j]."'";
							if($j<count($_POST["groupUsers"])-1){
								$grs.=",";	
							}
							if($_POST["groupUsers"][$j] == "All"){
								$grs="All";
								break;
							}
						}
						if($grs != "All"){
						$qr="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
					 inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
					 users.status='Active' and users_cstm.provincecode_c='".$_POST["province"]."' and securitygroups_users.securitygroup_id In (".$grs.") and securitygroups_users.securitygroup_id In (".$whr.")";		
						}else{
						$qr="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
					 inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
					 users.status='Active' and users_cstm.provincecode_c='".$_POST["province"]."' and securitygroups_users.securitygroup_id In (".$whr.")";	
						}
						$res=$db->query($qr);
						while($row=$db->fetchByAssoc($res)){
							if(!empty($row)){
								if($row["seName"]==$row["Se"]){
								$query1="select leads.lead_source, leads.date_entered, aos_products.name as pName,leads.last_name,leads.phone_mobile,leads.account_name,campaigns.name,leads.description,leads.status,leads.primary_address_street,users.user_name,email_addresses.email_address  from leads left join aos_products_leads_1_c on leads.id = aos_products_leads_1_c.aos_products_leads_1leads_idb left join aos_products on aos_products_leads_1_c.aos_products_leads_1aos_products_ida = aos_products.id left join campaigns  on leads.campaign_id = campaigns.id 
									left join users on leads.assigned_user_id = users.id left join email_addr_bean_rel on leads.id = email_addr_bean_rel.bean_id left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id where (leads.status = 'New' or leads.status ='In Process') and leads.deleted='0' and leads.assigned_user_id ='".$row['id']."'".$leadWhere;
									$result1=$db->query($query1);
									//$row1 =$db->fetchByAssoc($result1);
									while($row1 =$db->fetchByAssoc($result1)){
										if($employee == true){
											if($row["id"] == $_SESSION['authenticated_user_id']){
												$user=array(
														"lastName"=>$row1['last_name'],
														"phoneMobile"=>$row1["phone_mobile"],
														"accountName"=>$row1["account_name"],
														"name"=>$row1["name"],
														"description"=>$row1["description"],
														"status"=>$row1["status"],
														"primaryAddressStreet"=>$row1["primary_address_street"],
														"userName"=>$row1["user_name"],
														"emailAddress"=>$row1["email_address"],
														"group"=>$row["Se"],
														"product"=>$row1["pName"],
														"source"=>$row1["lead_source"],
														"date"=>date('d/m/Y H:i:s', strtotime($row1["date_entered"]))
													);
												if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
													$group[$row["Se"]]=array();
													$group[$row["Se"]][]=$user;
												}else{
													$group[$row["Se"]][]=$user;
												}
												
											}
										}else{
											$user=array(
														"lastName"=>$row1['last_name'],
														"phoneMobile"=>$row1["phone_mobile"],
														"accountName"=>$row1["account_name"],
														"name"=>$row1["name"],
														"description"=>$row1["description"],
														"status"=>$row1["status"],
														"primaryAddressStreet"=>$row1["primary_address_street"],
														"userName"=>$row1["user_name"],
														"emailAddress"=>$row1["email_address"],
														"group"=>$row["Se"],
														"product"=>$row1["pName"],
														"source"=>$row1["lead_source"],
														"date"=>date('d/m/Y H:i:s', strtotime($row1["date_entered"]))
													);
												if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
													$group[$row["Se"]]=array();
													$group[$row["Se"]][]=$user;
												}else{
													$group[$row["Se"]][]=$user;
												}
												
										}
									}
								}
							}
						}
						}else{
							for($j=0;$j<count($_POST["groupUsers"]);$j++){
									$grs.="'".$_POST["groupUsers"][$j]."'";
									if($j<count($_POST["groupUsers"])-1){
										$grs.=",";	
									}
									if($_POST["groupUsers"][$j] == "All"){
										$grs="All";
										break;
									}
							}
							for($i=0;$i<count($_POST["titleUsers"]);$i++){
								if($grs != "All"){
							$q="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
					 inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
					 users.status='Active' and users_cstm.provincecode_c='".$_POST["province"]."' and securitygroups_users.securitygroup_id In (".$whr.") and securitygroups_users.securitygroup_id In (".$grs.") and users.title='".$_POST["titleUsers"][$i]."'";
								}else{
									$q="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
									inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
									users.status='Active' and users_cstm.provincecode_c='".$_POST["province"]."' and securitygroups_users.securitygroup_id In (".$whr.") and users.title='".$_POST["titleUsers"][$i]."'";
								}
							$r=$db->query($q);
							while($rww=$db->fetchByAssoc($r)){
								if(!empty($rww)){
									if($rww["seName"]==$rww["Se"]){
										$qr="select users.user_name as name,users.department as Se from users inner join securitygroups_users on users.id=securitygroups_users.user_id where users.deleted='0' and users.status='Active' and  users.id='".$rww["id"]."' and securitygroups_users.securitygroup_id In (".$whr.")";	
										$res=$db->query($qr);
										$row=$db->fetchByAssoc($res);
										if(!empty($row)){
										$query1="select leads.lead_source, leads.date_entered, aos_products.name as pName,leads.last_name,leads.phone_mobile,leads.account_name,campaigns.name,leads.description,leads.status,leads.primary_address_street,users.user_name,email_addresses.email_address  from leads left join aos_products_leads_1_c on leads.id = aos_products_leads_1_c.aos_products_leads_1leads_idb left join aos_products on aos_products_leads_1_c.aos_products_leads_1aos_products_ida = aos_products.id left join campaigns  on leads.campaign_id = campaigns.id 
									left join users on leads.assigned_user_id = users.id left join email_addr_bean_rel on leads.id = email_addr_bean_rel.bean_id left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id where (leads.status = 'New' or leads.status ='In Process') and leads.deleted='0' and leads.assigned_user_id ='".$rww["id"]."'".$leadWhere;
									$result1=$db->query($query1);
									//$row1 =$db->fetchByAssoc($result1);
									while($row1 =$db->fetchByAssoc($result1)){
										if($employee == true){
											if($rww["id"] == $_SESSION['authenticated_user_id']){
												$user=array(
														"lastName"=>$row1['last_name'],
														"phoneMobile"=>$row1["phone_mobile"],
														"accountName"=>$row1["account_name"],
														"name"=>$row1["name"],
														"description"=>$row1["description"],
														"status"=>$row1["status"],
														"primaryAddressStreet"=>$row1["primary_address_street"],
														"userName"=>$row1["user_name"],
														"emailAddress"=>$row1["email_address"],
														"group"=>$row["Se"],
														"product"=>$row1["pName"],
														"source"=>$row1["lead_source"],
														"date"=>date('d/m/Y H:i:s', strtotime($row1["date_entered"]))
													);
												if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
													$group[$row["Se"]]=array();
													$group[$row["Se"]][]=$user;
												}else{
													$group[$row["Se"]][]=$user;
												}
												
											}
										}else{
											$user=array(
														"lastName"=>$row1['last_name'],
														"phoneMobile"=>$row1["phone_mobile"],
														"accountName"=>$row1["account_name"],
														"name"=>$row1["name"],
														"description"=>$row1["description"],
														"status"=>$row1["status"],
														"primaryAddressStreet"=>$row1["primary_address_street"],
														"userName"=>$row1["user_name"],
														"emailAddress"=>$row1["email_address"],
														"group"=>$row["Se"],
														"product"=>$row1["pName"],
														"source"=>$row1["lead_source"],
														"date"=>date('d/m/Y H:i:s', strtotime($row1["date_entered"]))
													);
												if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
													$group[$row["Se"]]=array();
													$group[$row["Se"]][]=$user;
												}else{
													$group[$row["Se"]][]=$user;
												}
												
										}
									}
										}
									}
								}
							}
						}
						}
					}
					else
					{
						for($i=0;$i<count($_POST["users"]);$i++){
							$qr="select users.user_name as name,users.department as Se from users inner join securitygroups_users on users.id=securitygroups_users.user_id where users.deleted='0' and users.status='Active' and  users.id='".$_POST["users"][$i]."' and securitygroups_users.securitygroup_id In (".$whr.")";	
							$res=$db->query($qr);
							$row=$db->fetchByAssoc($res);
							if(!empty($row)){
								$query1="select leads.lead_source, leads.date_entered, aos_products.name as pName,leads.last_name,leads.phone_mobile,leads.account_name,campaigns.name,leads.description,leads.status,leads.primary_address_street,users.user_name,email_addresses.email_address  from leads left join aos_products_leads_1_c on leads.id = aos_products_leads_1_c.aos_products_leads_1leads_idb left join aos_products on aos_products_leads_1_c.aos_products_leads_1aos_products_ida = aos_products.id left join campaigns  on leads.campaign_id = campaigns.id 
									left join users on leads.assigned_user_id = users.id left join email_addr_bean_rel on leads.id = email_addr_bean_rel.bean_id left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id where (leads.status = 'New' or leads.status ='In Process') and leads.deleted='0' and leads.assigned_user_id ='".$_POST["users"][$i]."'".$leadWhere;
									$result1=$db->query($query1);
									//$row1 =$db->fetchByAssoc($result1);
									while($row1 =$db->fetchByAssoc($result1)){
										if($employee == true){
											if($row["id"] == $_SESSION['authenticated_user_id']){
												$user=array(
														"lastName"=>$row1['last_name'],
														"phoneMobile"=>$row1["phone_mobile"],
														"accountName"=>$row1["account_name"],
														"name"=>$row1["name"],
														"description"=>$row1["description"],
														"status"=>$row1["status"],
														"primaryAddressStreet"=>$row1["primary_address_street"],
														"userName"=>$row1["user_name"],
														"emailAddress"=>$row1["email_address"],
														"group"=>$row["Se"],
														"product"=>$row1["pName"],
														"source"=>$row1["lead_source"],
														"date"=>date('d/m/Y H:i:s', strtotime($row1["date_entered"]))
													);
												if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
													$group[$row["Se"]]=array();
													$group[$row["Se"]][]=$user;
												}else{
													$group[$row["Se"]][]=$user;
												}
												
											}
										}else{
											$user=array(
														"lastName"=>$row1['last_name'],
														"phoneMobile"=>$row1["phone_mobile"],
														"accountName"=>$row1["account_name"],
														"name"=>$row1["name"],
														"description"=>$row1["description"],
														"status"=>$row1["status"],
														"primaryAddressStreet"=>$row1["primary_address_street"],
														"userName"=>$row1["user_name"],
														"emailAddress"=>$row1["email_address"],
														"group"=>$row["Se"],
														"product"=>$row1["pName"],
														"source"=>$row1["lead_source"],
														"date"=>date('d/m/Y H:i:s', strtotime($row1["date_entered"]))
													);
												if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
													$group[$row["Se"]]=array();
													$group[$row["Se"]][]=$user;
												}else{
													$group[$row["Se"]][]=$user;
												}
												
										}
									}
							}
						}
					}
				//	print_r ($group);
				}
				
			
			echo json_encode (array(array(
			"province"=>$prov[$_POST["province"]],
			"data"=>$group
			)));
			
		}
		
		else
		{
			for($k=10;$k<105;$k++){
				$p='';
				$New=0;
				$InProcess=0;
				$Converted=0;
				$Dead=0;
				$Transaction=0;
				$CloseWon=0;
				$CloseLost=0;
				$Other=0;
				$amountNew=0;
				$amountInProcess=0;
				$amountConverted=0;
				$amountTran=0;
				$amountWon=0;
				$amountLost=0;
				$amountDead=0;
				$amountCall=0;
				$amountMeeting=0;
				$amountEmail=0;
				$amountOther=0;
				if($_POST['groupUsers'] == null || $_POST['groupUsers'] == '' || $_POST['groupUsers'][0] == "All"){
					if($_POST["users"] == null || $_POST["users"] == ''){
						if($_POST["titleUsers"] == null || $_POST["titleUsers"] == ''){
						$qr="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join users_cstm on users.id=users_cstm.id_c inner join securitygroups_users on users.id = securitygroups_users.user_id
						inner join  securitygroups on securitygroups.id = securitygroups_users.securitygroup_id where users.deleted='0' and users.status='Active' and users_cstm.provincecode_c='".$k."' and securitygroups_users.securitygroup_id In (".$whr.")";	
						$res=$db->query($qr);
						while($row=$db->fetchByAssoc($res)){
							if(!empty($row)){
								$p=$k;
								if($row["seName"] == $row["Se"]){
									$query1="select leads.lead_source, leads.date_entered, aos_products.name as pName,leads.last_name,leads.phone_mobile,leads.account_name,campaigns.name,leads.description,leads.status,leads.primary_address_street,users.user_name,email_addresses.email_address  from leads left join aos_products_leads_1_c on leads.id = aos_products_leads_1_c.aos_products_leads_1leads_idb left join aos_products on aos_products_leads_1_c.aos_products_leads_1aos_products_ida = aos_products.id left join campaigns  on leads.campaign_id = campaigns.id 
									left join users on leads.assigned_user_id = users.id left join email_addr_bean_rel on leads.id = email_addr_bean_rel.bean_id left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id where (leads.status = 'New' or leads.status ='In Process') and leads.deleted='0' and leads.assigned_user_id ='".$row['id']."'".$leadWhere;
									$result1=$db->query($query1);
									//$row1 =$db->fetchByAssoc($result1);
									while($row1 =$db->fetchByAssoc($result1)){
										if($employee == true){
											if($row["id"] == $_SESSION['authenticated_user_id']){
												$user=array(
														"lastName"=>$row1['last_name'],
														"phoneMobile"=>$row1["phone_mobile"],
														"accountName"=>$row1["account_name"],
														"name"=>$row1["name"],
														"description"=>$row1["description"],
														"status"=>$row1["status"],
														"primaryAddressStreet"=>$row1["primary_address_street"],
														"userName"=>$row1["user_name"],
														"emailAddress"=>$row1["email_address"],
														"group"=>$row["Se"],
														"product"=>$row1["pName"],
														"source"=>$row1["lead_source"],
														"date"=>date('d/m/Y H:i:s', strtotime($row1["date_entered"]))
													);
												if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
													$group[$row["Se"]]=array();
													$group[$row["Se"]][]=$user;
												}else{
													$group[$row["Se"]][]=$user;
												}
												
											}
										}else{
											$user=array(
														"lastName"=>$row1['last_name'],
														"phoneMobile"=>$row1["phone_mobile"],
														"accountName"=>$row1["account_name"],
														"name"=>$row1["name"],
														"description"=>$row1["description"],
														"status"=>$row1["status"],
														"primaryAddressStreet"=>$row1["primary_address_street"],
														"userName"=>$row1["user_name"],
														"emailAddress"=>$row1["email_address"],
														"group"=>$row["Se"],
														"product"=>$row1["pName"],
														"source"=>$row1["lead_source"],
														"date"=>date('d/m/Y H:i:s', strtotime($row1["date_entered"]))
													);
												if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
													$group[$row["Se"]]=array();
													$group[$row["Se"]][]=$user;
												}else{
													$group[$row["Se"]][]=$user;
												}
												
										}
									}
								}
							}
						  } 
						}else{
								for($i=0;$i<count($_POST["titleUsers"]);$i++){
								$q="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
								inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
								users.status='Active' and users_cstm.provincecode_c='".$k."' and securitygroups_users.securitygroup_id In (".$whr.") and users.title = '".$_POST["titleUsers"][$i]."'";
								$r=$db->query($q);
								while($rww=$db->fetchByAssoc($r)){
									if(!empty($rww)){
										//$p=$k;
										if($rww["seName"] == $rww["Se"]){
											$qr="select users_cstm.provincecode_c as province, users.user_name as name,users.department as Se from users inner join securitygroups_users on users.id = securitygroups_users.user_id inner join users_cstm on users.id = users_cstm.id_c where users.deleted='0' and users.status='Active' and  users.id='".$rww["id"]."' and securitygroups_users.securitygroup_id In (".$whr.")";	
											$res=$db->query($qr);
											$row=$db->fetchByAssoc($res);
											if(!empty($row)){
												if($row["province"] == $k){
													$p=$k;
													$query1="select leads.lead_source, leads.date_entered, aos_products.name as pName,leads.last_name,leads.phone_mobile,leads.account_name,campaigns.name,leads.description,leads.status,leads.primary_address_street,users.user_name,email_addresses.email_address  from leads left join aos_products_leads_1_c on leads.id = aos_products_leads_1_c.aos_products_leads_1leads_idb left join aos_products on aos_products_leads_1_c.aos_products_leads_1aos_products_ida = aos_products.id left join campaigns  on leads.campaign_id = campaigns.id 
													left join users on leads.assigned_user_id = users.id left join email_addr_bean_rel on leads.id = email_addr_bean_rel.bean_id left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id where (leads.status = 'New' or leads.status ='In Process') and leads.deleted='0' and leads.assigned_user_id ='".$rww["id"]."'".$leadWhere;
													$result1=$db->query($query1);
													//$row1 =$db->fetchByAssoc($result1);
													while($row1 =$db->fetchByAssoc($result1)){
														if($employee == true){
															if($rww["id"] == $_SESSION['authenticated_user_id']){
																$user=array(
																"lastName"=>$row1['last_name'],
																"phoneMobile"=>$row1["phone_mobile"],
																"accountName"=>$row1["account_name"],
																"name"=>$row1["name"],
																"description"=>$row1["description"],
																"status"=>$row1["status"],
																"primaryAddressStreet"=>$row1["primary_address_street"],
																"userName"=>$row1["user_name"],
																"emailAddress"=>$row1["email_address"],
																"group"=>$row["Se"],
																"product"=>$row1["pName"],
																"source"=>$row1["lead_source"],
																"date"=>date('d/m/Y H:i:s', strtotime($row1["date_entered"]))
																);
																if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
																	$group[$row["Se"]]=array();
																	$group[$row["Se"]][]=$user;
																}else{
																	$group[$row["Se"]][]=$user;
																}
															}
														}else{
															$user=array(
																"lastName"=>$row1['last_name'],
																"phoneMobile"=>$row1["phone_mobile"],
																"accountName"=>$row1["account_name"],
																"name"=>$row1["name"],
																"description"=>$row1["description"],
																"status"=>$row1["status"],
																"primaryAddressStreet"=>$row1["primary_address_street"],
																"userName"=>$row1["user_name"],
																"emailAddress"=>$row1["email_address"],
																"group"=>$row["Se"],
																"product"=>$row1["pName"],
																"source"=>$row1["lead_source"],
																"date"=>date('d/m/Y H:i:s', strtotime($row1["date_entered"]))
															);
															if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
																$group[$row["Se"]]=array();
																$group[$row["Se"]][]=$user;
															}else{
																$group[$row["Se"]][]=$user;
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
						
					}
					else
					{
						for($i=0;$i<count($_POST["users"]);$i++){
							$qr="select users_cstm.provincecode_c as province, users.user_name as name,users.department as Se from users inner join securitygroups_users on users.id = securitygroups_users.user_id inner join users_cstm on users.id = users_cstm.id_c where users.deleted='0' and users.status='Active' and  users.id='".$_POST["users"][$i]."' and securitygroups_users.securitygroup_id In (".$whr.")";	
							$res=$db->query($qr);
							$row=$db->fetchByAssoc($res);
								if(!empty($row)){
									if($row["province"] == $k){
									$p=$k;
									$query1="select leads.lead_source, leads.date_entered, aos_products.name as pName,leads.last_name,leads.phone_mobile,leads.account_name,campaigns.name,leads.description,leads.status,leads.primary_address_street,users.user_name,email_addresses.email_address  from leads left join aos_products_leads_1_c on leads.id = aos_products_leads_1_c.aos_products_leads_1leads_idb left join aos_products on aos_products_leads_1_c.aos_products_leads_1aos_products_ida = aos_products.id left join campaigns  on leads.campaign_id = campaigns.id 
									left join users on leads.assigned_user_id = users.id left join email_addr_bean_rel on leads.id = email_addr_bean_rel.bean_id left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id where (leads.status = 'New' or leads.status ='In Process') and leads.deleted='0' and leads.assigned_user_id ='".$_POST["users"][$i]."'".$leadWhere;
									$result1=$db->query($query1);
									//$row1 =$db->fetchByAssoc($result1);
										while($row1 =$db->fetchByAssoc($result1)){
											if($employee == true){
												if($row["id"] == $_SESSION['authenticated_user_id']){
													$user=array(
															"lastName"=>$row1['last_name'],
															"phoneMobile"=>$row1["phone_mobile"],
															"accountName"=>$row1["account_name"],
															"name"=>$row1["name"],
															"description"=>$row1["description"],
															"status"=>$row1["status"],
															"primaryAddressStreet"=>$row1["primary_address_street"],
															"userName"=>$row1["user_name"],
															"emailAddress"=>$row1["email_address"],
															"group"=>$row["Se"],
															"product"=>$row1["pName"],
															"source"=>$row1["lead_source"],
															"date"=>date('d/m/Y H:i:s', strtotime($row1["date_entered"]))
														);
													if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
														$group[$row["Se"]]=array();
														$group[$row["Se"]][]=$user;
													}else{
														$group[$row["Se"]][]=$user;
													}
													
												}
											}else{
												$user=array(
															"lastName"=>$row1['last_name'],
															"phoneMobile"=>$row1["phone_mobile"],
															"accountName"=>$row1["account_name"],
															"name"=>$row1["name"],
															"description"=>$row1["description"],
															"status"=>$row1["status"],
															"primaryAddressStreet"=>$row1["primary_address_street"],
															"userName"=>$row1["user_name"],
															"emailAddress"=>$row1["email_address"],
															"group"=>$row["Se"],
															"product"=>$row1["pName"],
															"source"=>$row1["lead_source"],
															"date"=>date('d/m/Y H:i:s', strtotime($row1["date_entered"]))
														);
													if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
														$group[$row["Se"]]=array();
														$group[$row["Se"]][]=$user;
													}else{
														$group[$row["Se"]][]=$user;
													}
													
											}
										}
									}
								}
							}
						}	
						
						
					}
					else
					{
						if($_POST["users"] == null || $_POST["users"] == ''){
							if($_POST["titleUsers"] == null || $_POST["titleUsers"] == ''){
							for($j=0;$j<count($_POST["groupUsers"]);$j++){
							$grs.="'".$_POST["groupUsers"][$j]."'";
								if($j<count($_POST["groupUsers"])-1){
									$grs.=",";	
								}
								if($_POST["groupUsers"][$j] == "All"){
									$grs="All";
									break;
								}
							}
							if($grs != "All"){
								$qr="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
								inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
								users.status='Active' and users_cstm.provincecode_c='".$k."' and securitygroups_users.securitygroup_id In (".$grs.") and securitygroups_users.securitygroup_id In (".$whr.")";		
							}else{
								$qr="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
							    inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
							    users.status='Active' and users_cstm.provincecode_c='".$k."' and securitygroups_users.securitygroup_id In (".$whr.")";	
							}
							$res=$db->query($qr);
							while($row=$db->fetchByAssoc($res)){
								if(!empty($row)){
									$p=$k;
									if($row["Se"] == $row["seName"]){
										$query1="select leads.lead_source, leads.date_entered, aos_products.name as pName,leads.last_name,leads.phone_mobile,leads.account_name,campaigns.name,leads.description,leads.status,leads.primary_address_street,users.user_name,email_addresses.email_address  from leads left join aos_products_leads_1_c on leads.id = aos_products_leads_1_c.aos_products_leads_1leads_idb left join aos_products on aos_products_leads_1_c.aos_products_leads_1aos_products_ida = aos_products.id left join campaigns  on leads.campaign_id = campaigns.id 
										left join users on leads.assigned_user_id = users.id left join email_addr_bean_rel on leads.id = email_addr_bean_rel.bean_id left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id where (leads.status = 'New' or leads.status ='In Process') and leads.deleted='0' and leads.assigned_user_id ='".$row["id"]."'".$leadWhere;
										$result1=$db->query($query1);
										//$row1 =$db->fetchByAssoc($result1);
										while($row1 =$db->fetchByAssoc($result1)){
											if($employee == true){
												if($row["id"] == $_SESSION['authenticated_user_id']){
													$user=array(
															"lastName"=>$row1['last_name'],
															"phoneMobile"=>$row1["phone_mobile"],
															"accountName"=>$row1["account_name"],
															"name"=>$row1["name"],
															"description"=>$row1["description"],
															"status"=>$row1["status"],
															"primaryAddressStreet"=>$row1["primary_address_street"],
															"userName"=>$row1["user_name"],
															"emailAddress"=>$row1["email_address"],
															"group"=>$row["Se"],
															"product"=>$row1["pName"],
															"source"=>$row1["lead_source"],
															"date"=>date('d/m/Y H:i:s', strtotime($row1["date_entered"]))
														);
													if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
														$group[$row["Se"]]=array();
														$group[$row["Se"]][]=$user;
													}else{
														$group[$row["Se"]][]=$user;
													}
													
												}
											}else{
												$user=array(
															"lastName"=>$row1['last_name'],
															"phoneMobile"=>$row1["phone_mobile"],
															"accountName"=>$row1["account_name"],
															"name"=>$row1["name"],
															"description"=>$row1["description"],
															"status"=>$row1["status"],
															"primaryAddressStreet"=>$row1["primary_address_street"],
															"userName"=>$row1["user_name"],
															"emailAddress"=>$row1["email_address"],
															"group"=>$row["Se"],
															"product"=>$row1["pName"],
															"source"=>$row1["lead_source"],
															"date"=>date('d/m/Y H:i:s', strtotime($row1["date_entered"]))
														);
													if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
														$group[$row["Se"]]=array();
														$group[$row["Se"]][]=$user;
													}else{
														$group[$row["Se"]][]=$user;
													}
													
											}
										}
									}
								}
							}
						}else{
							for($j=0;$j<count($_POST["groupUsers"]);$j++){
							$grs.="'".$_POST["groupUsers"][$j]."'";
								if($j<count($_POST["groupUsers"])-1){
									$grs.=",";	
								}
								if($_POST["groupUsers"][$j] == "All"){
									$grs="All";
									break;
								}
							}
						for($i=0;$i<count($_POST["titleUsers"]);$i++){
							if($grs != "All"){
							$q="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
								inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
								users.status='Active' and users_cstm.provincecode_c='".$k."' and securitygroups_users.securitygroup_id In (".$grs.") and securitygroups_users.securitygroup_id In (".$whr.") and users.title = '".$_POST["titleUsers"][$i]."'";
							}else{
								$q="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
								inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
								users.status='Active' and users_cstm.provincecode_c='".$k."' and securitygroups_users.securitygroup_id In (".$whr.") and users.title = '".$_POST["titleUsers"][$i]."'";
							}
								$r=$db->query($q);
								while($rww=$db->fetchByAssoc($r)){
									if(!empty($rww)){
										//$p=$k;
										if($rww["seName"]==$rww["Se"]){
											$qr="select users_cstm.provincecode_c as province, users.user_name as name,users.department as Se from users inner join securitygroups_users on users.id = securitygroups_users.user_id inner join users_cstm on users.id = users_cstm.id_c where users.deleted='0' and users.status='Active' 
											and  users.id='".$rww["id"]."' and securitygroups_users.securitygroup_id In (".$whr.")";	
											$res=$db->query($qr);
											$row=$db->fetchByAssoc($res);
											if(!empty($row)){
												if($row["province"] == $k){
												$p=$k;
												$query1="select leads.lead_source, leads.date_entered, aos_products.name as pName,leads.last_name,leads.phone_mobile,leads.account_name,campaigns.name,leads.description,leads.status,leads.primary_address_street,users.user_name,email_addresses.email_address  from leads left join aos_products_leads_1_c on leads.id = aos_products_leads_1_c.aos_products_leads_1leads_idb left join aos_products on aos_products_leads_1_c.aos_products_leads_1aos_products_ida = aos_products.id left join campaigns  on leads.campaign_id = campaigns.id 
												left join users on leads.assigned_user_id = users.id left join email_addr_bean_rel on leads.id = email_addr_bean_rel.bean_id left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id where (leads.status = 'New' or leads.status ='In Process') and leads.deleted='0' and leads.assigned_user_id ='".$rww["id"]."'".$leadWhere;
												$result1=$db->query($query1);
												//$row1 =$db->fetchByAssoc($result1);
													while($row1 =$db->fetchByAssoc($result1)){
														if($employee == true){
															if($rww["id"] == $_SESSION['authenticated_user_id']){
																$user=array(
																		"lastName"=>$row1['last_name'],
																		"phoneMobile"=>$row1["phone_mobile"],
																		"accountName"=>$row1["account_name"],
																		"name"=>$row1["name"],
																		"description"=>$row1["description"],
																		"status"=>$row1["status"],
																		"primaryAddressStreet"=>$row1["primary_address_street"],
																		"userName"=>$row1["user_name"],
																		"emailAddress"=>$row1["email_address"],
																		"group"=>$row["Se"],
																		"product"=>$row1["pName"],
																		"source"=>$row1["lead_source"],
																		"date"=>date('d/m/Y H:i:s', strtotime($row1["date_entered"]))
																	);
																if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
																	$group[$row["Se"]]=array();
																	$group[$row["Se"]][]=$user;
																}else{
																	$group[$row["Se"]][]=$user;
																}
																
															}
														}
														else{
														$user=array(
															"lastName"=>$row1['last_name'],
															"phoneMobile"=>$row1["phone_mobile"],
															"accountName"=>$row1["account_name"],
															"name"=>$row1["name"],
															"description"=>$row1["description"],
															"status"=>$row1["status"],
															"primaryAddressStreet"=>$row1["primary_address_street"],
															"userName"=>$row1["user_name"],
															"emailAddress"=>$row1["email_address"],
															"group"=>$row["Se"],
															"product"=>$row1["pName"],
															"source"=>$row1["lead_source"],
															"date"=>date('d/m/Y H:i:s', strtotime($row1["date_entered"]))
														);
														if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
															$group[$row["Se"]]=array();
															$group[$row["Se"]][]=$user;
														}else{
															$group[$row["Se"]][]=$user;
														}
														
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						
						else
						{
							for($i=0;$i<count($_POST["users"]);$i++){
							$qr="select users_cstm.provincecode_c as province, users.user_name as name,users.department as Se from users inner join securitygroups_users on users.id = securitygroups_users.user_id inner join users_cstm on users.id = users_cstm.id_c where users.deleted='0' and users.status='Active' 
							and  users.id='".$_POST["users"][$i]."' and securitygroups_users.securitygroup_id In (".$whr.")";	
							$res=$db->query($qr);
							$row=$db->fetchByAssoc($res);
							if(!empty($row)){
								if($row["province"] == $k){
								$p=$k;
								$query1="select leads.lead_source, leads.date_entered, aos_products.name as pName,leads.last_name,leads.phone_mobile,leads.account_name,campaigns.name,leads.description,leads.status,leads.primary_address_street,users.user_name,email_addresses.email_address  from leads left join aos_products_leads_1_c on leads.id = aos_products_leads_1_c.aos_products_leads_1leads_idb left join aos_products on aos_products_leads_1_c.aos_products_leads_1aos_products_ida = aos_products.id left join campaigns  on leads.campaign_id = campaigns.id 
								left join users on leads.assigned_user_id = users.id left join email_addr_bean_rel on leads.id = email_addr_bean_rel.bean_id left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id where (leads.status = 'New' or leads.status ='In Process') and leads.deleted='0' and leads.assigned_user_id ='".$_POST["users"][$i]."'".$leadWhere;
								$result1=$db->query($query1);
								//$row1 =$db->fetchByAssoc($result1);
									while($row1 =$db->fetchByAssoc($result1)){
										if($employee == true){
											if($row["id"] == $_SESSION['authenticated_user_id']){
												$user=array(
														"lastName"=>$row1['last_name'],
														"phoneMobile"=>$row1["phone_mobile"],
														"accountName"=>$row1["account_name"],
														"name"=>$row1["name"],
														"description"=>$row1["description"],
														"status"=>$row1["status"],
														"primaryAddressStreet"=>$row1["primary_address_street"],
														"userName"=>$row1["user_name"],
														"emailAddress"=>$row1["email_address"],
														"group"=>$row["Se"],
														"product"=>$row1["pName"],
														"source"=>$row1["lead_source"],
														"date"=>date('d/m/Y H:i:s', strtotime($row1["date_entered"]))
													);
												if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
													$group[$row["Se"]]=array();
													$group[$row["Se"]][]=$user;
												}else{
													$group[$row["Se"]][]=$user;
												}
												
											}
										}else{
											$user=array(
														"lastName"=>$row1['last_name'],
														"phoneMobile"=>$row1["phone_mobile"],
														"accountName"=>$row1["account_name"],
														"name"=>$row1["name"],
														"description"=>$row1["description"],
														"status"=>$row1["status"],
														"primaryAddressStreet"=>$row1["primary_address_street"],
														"userName"=>$row1["user_name"],
														"emailAddress"=>$row1["email_address"],
														"group"=>$row["Se"],
														"product"=>$row1["pName"],
														"source"=>$row1["lead_source"],
														"date"=>date('d/m/Y H:i:s', strtotime($row1["date_entered"]))
													);
												if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
													$group[$row["Se"]]=array();
													$group[$row["Se"]][]=$user;
												}else{
													$group[$row["Se"]][]=$user;
												}
												
										}
									}
								}
							}
						}
					}
				
				}
				if(!empty($prov[$k]) && !empty($p)){
				$province[]=array(
					"province"=>$prov[$k],
					"data"=>$group
				);
				}
				$group=array();
			}
			
			echo json_encode ($province);
			
		
		
	}	
	
}	