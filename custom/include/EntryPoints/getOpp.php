<?php
function getDateRangeToSearchForAmount($period_list_selected = 'this_month')
{
        if(empty($period_list_selected)) $period_list_selected = 'Tháng này';
        switch ($period_list_selected) {
            case 'Tuần này':
                $datetimeTo = new DateTime("this week sunday");
               // $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("this week monday");
              //  $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Tuần trước':
                $datetimeTo = new DateTime("last week sunday");
               // $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("last week monday");
               // $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Tháng này':
                $datetimeTo = new DateTime('last day of this month');
              //  $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("first day of this month");
              //  $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Tháng trước':
                $datetimeTo = new DateTime("last day of last month");
              //  $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("first day of last month");
              //  $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Năm này':
                $datetimeTo = new DateTime('this year last day of december');
             //   $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime('this year first day of january');
             //   $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Năm trước':
                $datetimeTo = new DateTime('last year last day of december');
              //  $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime('last year first day of january');
              //  $datetimeFrom->setTime(0, 0, 0);
                break;

        }
        return array(
            'from' => $datetimeFrom,
            'to' => $datetimeTo
        );
}
function getDateTimeAsDbForAmount($period = 'this_month', $date_from = '', $date_to = '')
{
        
		$timedate = new TimeDate();
		date_default_timezone_set('Asia/Ho_Chi_Minh');
        if(empty($date_from) && empty($date_to)) {
            $range = getDateRangeToSearchForAmount($period);

            $from_by_user = $range['from']->format($timedate::DB_DATETIME_FORMAT);
            $to_by_user = $range['to']->format($timedate::DB_DATETIME_FORMAT);

        }
        else {

            $fromObj = $timedate->fromUserDate($date_from);
            $fromObj->setTime(0, 0, 0);
            $from_by_user = $fromObj->format($timedate::DB_DATETIME_FORMAT);

            $toObj = $timedate->fromUserDate($date_to);
            $toObj->setTime(23,59,59);
            $to_by_user = $toObj->format($timedate::DB_DATETIME_FORMAT);

        }

        $from_display = $timedate->to_display_date_time($from_by_user, true, false);
        $from = $timedate->to_db($from_display);

        $to_display = $timedate->to_display_date_time($to_by_user, true, false);
        $to = $timedate->to_db($to_display);

        $datetime_as_db = array(
            'from' => $from,
            'to' => $to
        );

        return $datetime_as_db;
}

if($_SERVER['REQUEST_METHOD'] == 'POST'){
	if (isset($_SERVER['HTTPS']) &&
		($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
		isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
		$_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https'){
					$protocol = 'https://';
		}
		else {
			$protocol = 'http://';
		}
			$apiUrl = $protocol.$_SERVER['HTTP_HOST']."/index.php?entryPoint=getLanguageForDropdown&id=sales_stage_dom";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_URL, $apiUrl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            $response = curl_exec($ch);
			$response = ((array)((array)(json_decode($response))[0])["language"]);
            curl_close($ch);
	try{
	global $db;
	global $current_user;
	$current_user->id = $_POST["user"];
	$arr = array();
	$whr='';
	$sea = "";
	$myOpp = "";
	$start= ($_POST["page"] -1) * 200;
	if(isset($_POST['time']) && $_POST['time'] != "Tất cả"){
		$time = getDateTimeAsDbForAmount($_POST['time']);
		$from=date('Y-m-d', strtotime($time['from']));
		$to=date('Y-m-d', strtotime($time['to']));
		if(date('m', strtotime($time['to'])) == "02" && $_POST['time'] == "Tháng này"){
			$to = date('Y', strtotime($time['to']))."-02-29";
		}
		$wh = " and opportunities.date_entered >= '".$from."' and opportunities.date_entered <= '".$to."'";
	}else{
		$wh= '';	
	}
	if($_POST['myOpp'] == "1"){
		$myOpp = " and opportunities.assigned_user_id = '".$_POST["user"]."'";
	}
	if(isset($_POST['search'])){
		if($_POST['search'] != ""){
			$sea .= " and opportunities.name like '%".$_POST['search']."%'";
		}
	}
	$where = "opportunities.deleted = '0'".$wh.$sea.$myOpp;
	$opportunity = BeanFactory::newBean('Opportunities');
	$queryNew = $opportunity->create_new_list_query("opportunities.date_entered DESC",$where,array());
	
	$query = "select opportunities.*,accounts.name as accName,accounts.phone_alternate,email_addresses.email_address,users.user_name ,aos_products.name as proName from ($queryNew) as opportunities left join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id left join accounts on accounts_opportunities.account_id = accounts.id  left join email_addr_bean_rel on accounts.id = email_addr_bean_rel.bean_id left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id left join aos_products_opportunities_1_c on opportunities.id = aos_products_opportunities_1_c.aos_products_opportunities_1opportunities_idb left join aos_products on aos_products_opportunities_1_c.aos_products_opportunities_1aos_products_ida = aos_products.id inner join users on opportunities.assigned_user_id = users.id order by opportunities.date_entered DESC";
	$queryCount = "select count(opportunities.id) as num from opportunities inner join ($queryNew) as opportunity on opportunities.id = opportunity.id where opportunities.deleted = '0'";
	$resCount = $db->query($queryCount);
	$rowCount = $db->fetchByAssoc($resCount);
    $limit_query = $db->limitQuery($query, $start,200, false, '', false);
    $res = $db->query($limit_query);
	while($row = $db->fetchByAssoc($res)){
				$arr[] = array(
							"id"=>$row["id"],
							"name"=>$row["name"],
							"accountName"=>$row["accName"],
							"amount"=>$row["amount"],
							"date"=>date('Y-m-d H:i:s', strtotime($row["date_entered"].' + 7 hours')),
							"userName"=>$row["user_name"],
							"dateClose"=>$row["date_closed"],
							"status"=>$response[$row["sales_stage"]],
							"proName"=>$row["proName"],
							"depositdate"=>$row["depositdate"],
							"description"=>$row["description"],
							"type"=>$row["servicetype_c"],
							"length"=>$rowCount["num"]
				);	
	}
	echo json_encode($arr);
	}
	catch(PDOException $e) {
		echo "Error: " . $e->getMessage();
	}
}