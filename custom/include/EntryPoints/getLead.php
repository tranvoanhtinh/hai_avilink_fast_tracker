<?php
function getDateRangeToSearchForAmount($period_list_selected = 'this_month')
{
        if(empty($period_list_selected)) $period_list_selected = 'Tháng này';
        switch ($period_list_selected) {
            case 'Tuần này':
                $datetimeTo = new DateTime("this week sunday");
               // $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("this week monday");
              //  $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Tuần trước':
                $datetimeTo = new DateTime("last week sunday");
               // $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("last week monday");
               // $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Tháng này':
                $datetimeTo = new DateTime('last day of this month');
              //  $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("first day of this month");
              //  $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Tháng trước':
                $datetimeTo = new DateTime("last day of last month");
              //  $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("first day of last month");
              //  $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Năm này':
                $datetimeTo = new DateTime('this year last day of december');
             //   $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime('this year first day of january');
             //   $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Năm trước':
                $datetimeTo = new DateTime('last year last day of december');
              //  $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime('last year first day of january');
              //  $datetimeFrom->setTime(0, 0, 0);
                break;

        }
        return array(
            'from' => $datetimeFrom,
            'to' => $datetimeTo
        );
}
function getDateTimeAsDbForAmount($period = 'this_month', $date_from = '', $date_to = '')
{
        
		$timedate = new TimeDate();
		date_default_timezone_set('Asia/Ho_Chi_Minh');
        if(empty($date_from) && empty($date_to)) {
            $range = getDateRangeToSearchForAmount($period);

            $from_by_user = $range['from']->format($timedate::DB_DATETIME_FORMAT);
            $to_by_user = $range['to']->format($timedate::DB_DATETIME_FORMAT);

        }
        else {

            $fromObj = $timedate->fromUserDate($date_from);
            $fromObj->setTime(0, 0, 0);
            $from_by_user = $fromObj->format($timedate::DB_DATETIME_FORMAT);

            $toObj = $timedate->fromUserDate($date_to);
            $toObj->setTime(23,59,59);
            $to_by_user = $toObj->format($timedate::DB_DATETIME_FORMAT);

        }

        $from_display = $timedate->to_display_date_time($from_by_user, true, false);
        $from = $timedate->to_db($from_display);

        $to_display = $timedate->to_display_date_time($to_by_user, true, false);
        $to = $timedate->to_db($to_display);

        $datetime_as_db = array(
            'from' => $from,
            'to' => $to
        );

        return $datetime_as_db;
}

if($_SERVER['REQUEST_METHOD'] == 'POST'){
	if (isset($_SERVER['HTTPS']) &&
		($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
		isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
		$_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https'){
					$protocol = 'https://';
		}
		else {
			$protocol = 'http://';
		}
			$apiUrl = $protocol.$_SERVER['HTTP_HOST']."/index.php?entryPoint=getLanguageForDropdown&id=status_activities_list";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_URL, $apiUrl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            $response = curl_exec($ch);
			$response = ((array)((array)(json_decode($response))[0])["language"]);
            curl_close($ch);
	try{
	global $db;
	$arr = array();
	$whr='';
	$sea = "";
	$myLead = "";
	$start= ($_POST["page"] -1) * 200;
	if(isset($_POST['time']) && $_POST['time'] != "Tất cả"){
		$time = getDateTimeAsDbForAmount($_POST['time']);
		$from=date('Y-m-d', strtotime($time['from']));
		$to=date('Y-m-d', strtotime($time['to']));
		if(date('m', strtotime($time['to'])) == "02" && $_POST['time'] == "Tháng này"){
			$to = date('Y', strtotime($time['to']))."-02-29";
		}
		$wh = " and leads.date_entered >= '".$from."' and leads.date_entered <= '".$to."'";
	}else{
		$wh= '';	
	}
	if($_POST['myLead'] == "1"){
		$myLead = " and leads.assigned_user_id = '".$_POST["user"]."'";
	}
	if(isset($_POST['search'])){
		if($_POST['search'] != ""){
			$sea .= " and (leads.last_name like '%".$_POST['search']."%' OR leads.phone_mobile like '%".$_POST['search']."%' OR leads.account_name like '%".$_POST['search']."%' OR leads_cstm.identitycard_c like '%".$_POST['search']."%' OR leads_cstm.vatcode_c like '%".$_POST['search']."%')";
		}
	}
	$where = "leads.status IN ('New','In Process','Dead') ".$wh.$sea.$myLead;
	global $current_user;
	$current_user->id = $_POST["user"];
	$lead = new Lead();
	//$lead->load_relationship('user_name');
	$query = $lead->create_new_list_query("leads.date_entered DESC",$where,array());
	$query = $query." order by leads.date_entered DESC";
	$queryCount = "select count(leads.id) as num from leads inner join ($query) as lead on leads.id = lead.id where leads.deleted = '0'";
	$resCount = $db->query($queryCount);
	$rowCount = $db->fetchByAssoc($resCount);
    $limit_query = $lead->db->limitQuery($query, $start,200, false, '', false);
    $res = $lead->db->query($limit_query);
	while($row = $lead->db->fetchByAssoc($res)){
		$q2 = "select email_addresses.email_address from email_addresses inner join email_addr_bean_rel on email_addresses.id = email_addr_bean_rel.email_address_id and email_addresses.deleted = '0' inner join leads on leads.id =  email_addr_bean_rel.bean_id and leads.id = '".$row["id"]."' and leads.deleted = '0' where email_addr_bean_rel.primary_address = '1'";
		$r2 = $db->query($q2);
		$row2 = $db->fetchByAssoc($r2);
		$q3 = "select users.user_name from users inner join leads on users.id = leads.assigned_user_id and leads.deleted = '0' where users.id = '".$row["assigned_user_id"]."' and leads.id = '".$row["id"]."'  and users.deleted = '0'";
		$r3 = $db->query($q3);
		$row3 = $db->fetchByAssoc($r3);
		$arr[] = array(
						"id"=>$row["id"],
						"name"=>$row["last_name"],
						"phone"=>$row["phone_mobile"],
						"status"=>$row["status"],
						"user_name"=>$row3["user_name"],
						"date_entered"=>date('Y-m-d H:i:s', strtotime($row["date_entered"].' + 7 hours')),
						"status_activites"=>$response[$row["status_activites"]],
						"accountName"=>$row["account_name"],
						"email"=>($row2["email_address"] != null && $row2["email_address"] != "")?$row2["email_address"]:"",
						"description"=>$row["description"],
						"length"=>$rowCount["num"]
					);
	}
	echo json_encode($arr);
	}
	catch(PDOException $e) {
		echo "Error: " . $e->getMessage();
	}
}