<?php 
global $current_language;
global $db;
$arr = array();
global $sugar_config;
$payPa_mod_strings = return_module_language($current_language, "pr_PayrollParam");

$array = array(
	"language" => $payPa_mod_strings,
	"digit"=>$sugar_config["default_currency_significant_digits_payroll"]
);
echo json_encode($array);