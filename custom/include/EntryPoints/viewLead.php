<?php
function getDateRangeToSearchForAmount($period_list_selected = 'this_month')
{
        if(empty($period_list_selected)) $period_list_selected = 'Tháng này';
        switch ($period_list_selected) {
            case 'Tuần này':
                $datetimeTo = new DateTime("this week sunday");
               // $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("this week monday");
              //  $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Tuần trước':
                $datetimeTo = new DateTime("last week sunday");
               // $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("last week monday");
               // $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Tháng này':
                $datetimeTo = new DateTime('last day of this month');
              //  $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("first day of this month");
              //  $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Tháng trước':
                $datetimeTo = new DateTime("last day of last month");
              //  $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("first day of last month");
              //  $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Năm này':
                $datetimeTo = new DateTime('this year last day of december');
             //   $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime('this year first day of january');
             //   $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Năm trước':
                $datetimeTo = new DateTime('last year last day of december');
              //  $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime('last year first day of january');
              //  $datetimeFrom->setTime(0, 0, 0);
                break;

        }
        return array(
            'from' => $datetimeFrom,
            'to' => $datetimeTo
        );
}
function getDateTimeAsDbForAmount($period = 'this_month', $date_from = '', $date_to = '')
{
        
		$timedate = new TimeDate();
		date_default_timezone_set('Asia/Ho_Chi_Minh');
        if(empty($date_from) && empty($date_to)) {
            $range = getDateRangeToSearchForAmount($period);

            $from_by_user = $range['from']->format($timedate::DB_DATETIME_FORMAT);
            $to_by_user = $range['to']->format($timedate::DB_DATETIME_FORMAT);

        }
        else {

            $fromObj = $timedate->fromUserDate($date_from);
            $fromObj->setTime(0, 0, 0);
            $from_by_user = $fromObj->format($timedate::DB_DATETIME_FORMAT);

            $toObj = $timedate->fromUserDate($date_to);
            $toObj->setTime(23,59,59);
            $to_by_user = $toObj->format($timedate::DB_DATETIME_FORMAT);

        }

        $from_display = $timedate->to_display_date_time($from_by_user, true, false);
        $from = $timedate->to_db($from_display);

        $to_display = $timedate->to_display_date_time($to_by_user, true, false);
        $to = $timedate->to_db($to_display);

        $datetime_as_db = array(
            'from' => $from,
            'to' => $to
        );

        return $datetime_as_db;
}

if($_SERVER['REQUEST_METHOD'] == 'POST'){
	global $db;
	global $current_user;
	$current_user->id = $_POST["user"];
	date_default_timezone_set('Asia/Ho_Chi_Minh');
	$q = " select email.email_address,users.user_name,leads_cstm.shortname_c,leads_cstm.bussiness_type_c,leads.salutation,leads.last_name, leads.phone_mobile, leads.account_name, leads.primary_address_street,leads.description,leads.date_entered,leads.date_modified,leads.assigned_user_id,leads.opportunity_name,leads.opportunity_amount,aos_products.name as proName from leads inner join leads_cstm on leads.id = leads_cstm.id_c inner join users on leads.assigned_user_id = users.id left join (select email_addr_bean_rel.bean_id, email_addresses.email_address from email_addr_bean_rel inner join email_addresses on email_addresses.id = email_addr_bean_rel.email_address_id and email_addresses.deleted = '0' and email_addr_bean_rel.primary_address = '1') as email on leads.id = email.bean_id left join aos_products_leads_1_c on leads.id = aos_products_leads_1_c.aos_products_leads_1leads_idb left join aos_products on aos_products_leads_1_c.aos_products_leads_1aos_products_ida = aos_products.id where leads.deleted = '0' and leads.id = '".$_POST["id"]."'";
	$res = $db->query($q);
	$row = $db->fetchByAssoc($res);
	$row["salutation"] =="Mr." ?$row["salutation"] = "Anh.": ($row["salutation"] =="Ms."?$row["salutation"] = "Chị.": "");
	if(!empty($row)){
		$call = array();
		$calls = array();
		$meeting = array();
		$meetings = array();
		$note = array();
		$notes = array();
		//$group = array();
		$bean = BeanFactory::newBean('Calls');
		$queryNew = $bean->create_new_list_query("calls.date_modified DESC","",array());
		$query = "select distinct reminders.related_event_module_id, calls.*,reminders.popup,reminders.email,reminders.timer_popup,reminders.timer_email from ($queryNew) as calls left join reminders on calls.id = reminders.related_event_module_id left join calls_leads on calls.id = calls_leads.call_id where calls_leads.lead_id ='".$_POST["id"]."'   
		and calls.deleted='0' and calls.parent_type = 'Leads' ORDER BY calls.date_start DESC limit 0,5";
		$result = $db->query($query);
		while($row2 = $db->fetchByAssoc($result)){
			$row2["date_start"] = date('Y-m-d H:i:s', strtotime($row2["date_start"].' + 7 hours'));
			$calls[] = $row2;
		}
		$bean = BeanFactory::newBean('Meetings');
		$queryNew = $bean->create_new_list_query("meetings.date_modified DESC","",array());
		$query = "select distinct reminders.related_event_module_id,meetings.*,reminders.popup,reminders.email,reminders.timer_popup,reminders.timer_email from ($queryNew) as meetings left join reminders on meetings.id = reminders.related_event_module_id left join meetings_leads on meetings.id = meetings_leads.meeting_id where meetings_leads.lead_id ='".$_POST["id"]."'   
		and meetings.deleted='0' and meetings.parent_type = 'Leads' ORDER BY meetings.date_start DESC limit 0,5";
		$result = $db->query($query);
		while($row2 = $db->fetchByAssoc($result)){
			$row2["date_start"] = date('Y-m-d H:i:s', strtotime($row2["date_start"].' + 7 hours'));
			$meetings[] = $row2;
		}
		$bean = BeanFactory::newBean('Notes');
		$queryNew = $bean->create_new_list_query("notes.date_modified DESC","",array());
		$query ="select notes.* from ($queryNew) as notes where parent_id ='".$_POST["id"]."' and deleted ='0' and parent_type = 'Leads' order by date_modified DESC limit 0,5";
		$result = $db->query($query);
		while($row2 = $db->fetchByAssoc($result)){
			$row2["date_entered"] = date('Y-m-d H:i:s', strtotime($row2["date_entered"].' + 7 hours'));
			$notes[] = $row2;
		}
		$row["call"] = $calls;
		$row["meeting"] = $meetings;
		$row["note"] = $notes;
		echo json_encode($row);
	}
}