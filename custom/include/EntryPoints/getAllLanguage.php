<?php
global $current_language;
global $app_list_strings;
$lead_mod_strings = return_module_language($current_language, "Leads");
$account_mod_strings = return_module_language($current_language, "Accounts");
$opp_mod_strings = return_module_language($current_language, "Opportunities");
$call_mod_strings = return_module_language($current_language, "Calls");
$meeting_mod_strings = return_module_language($current_language, "Meetings");
echo json_encode(array(
				"leadLan" => $lead_mod_strings,
				"accLan"=> $account_mod_strings,
				"oppLan"=> $opp_mod_strings,
				"callLan"=> $call_mod_strings,
				"meetingLan"=> $meeting_mod_strings,
				"modNameLead"=>$app_list_strings['moduleList']["Leads"],
				"modNameAcc"=>$app_list_strings['moduleList']["Accounts"],
				"modNameOpp"=>$app_list_strings['moduleList']["Opportunities"],
				"modNameCall"=>$app_list_strings['moduleList']["Calls"],
				"modNameMeeting"=>$app_list_strings['moduleList']["Meetings"],
				"leadStatus"=>$app_list_strings["lead_status_dom"],
				"oppStatus"=>$app_list_strings["sales_stage_dom"],
				"callStatus"=>$app_list_strings["call_status_dom"],
				"meetingStatus"=>$app_list_strings["meeting_status_dom"],
				"source"=>$app_list_strings["lead_source_dom"]
				));