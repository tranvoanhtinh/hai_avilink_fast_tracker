<?php


// Kiểm tra xem yêu cầu là POST hay không
if ($_SERVER["REQUEST_METHOD"] === "POST") {
    global $db;

    // Kiểm tra xem có giá trị 'id' được gửi từ yêu cầu không
    if (isset($_POST['id'])) {
        // Lấy giá trị 'id' từ yêu cầu
        $id = $_POST['id'];

        // Cập nhật câu truy vấn SQL với biến 'packing' thay vì 'size'
        $sql = "SELECT product_image FROM aos_products WHERE id = '" . $id . "' AND deleted = 0 LIMIT 1";

        // Thực hiện truy vấn SQL và kiểm tra kết quả
        $result = $db->query($sql);

        if ($result) {
            // Fetch dòng dữ liệu đầu tiên từ kết quả
            

            // Kiểm tra xem có dữ liệu không
            if ($row = $result->fetch_assoc()) {             
                    // Lấy giá trị của trường 'product_image'
                    $image = $row['product_image'];

                    if($image =='' || $image == null){
                        echo -1;
                    }else {
                    // Gửi giá trị của 'product_image' dưới dạng JSON
                    header('Content-Type: application/json');
                    echo $image;    
                    }            
            }
        }
    }
}
