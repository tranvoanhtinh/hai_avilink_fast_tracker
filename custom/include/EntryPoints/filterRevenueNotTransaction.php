<?php
try{
global $current_user;
global $db;
$current_user->id = $_SESSION['authenticated_user_id'];

if((!empty($_POST['period'])) && ($_POST['period'] != 'default')){
	/*
	if($_POST['period'] != "is_between"){
		$time=getDateTimeAsDb($_POST['period']);
	}
	else{
		if(empty($_POST['date_from']) && empty($_POST['date_to'])){
			$invoiceWhere="";
			$accountWhere="";
			$oppWhere="";
			$billingWhere="";
		}else{
			if(!empty($_POST['date_from']) && !empty($_POST['date_to'])){
				if(strtotime($_POST['date_from']) != strtotime($_POST['date_to'])){
					$from = date('Y-m-d', strtotime(str_replace("/","-",$_POST['date_from'])));	
					$to = date('Y-m-d', strtotime(str_replace("/","-",$_POST['date_to'])));	
				}else{
					$from1=date('Y-m-d', strtotime(str_replace("/","-",$_POST['date_from'])));	
				}
			}
		}
	}
	*/
	if(!empty($_POST['date_from']) && !empty($_POST['date_to'])){
				if($_POST['date_from'] != $_POST['date_to']){
					$from = date('Y-m-d', strtotime(str_replace("/","-",$_POST['date_from'])));	
					$to = date('Y-m-d', strtotime(str_replace("/","-",$_POST['date_to'])));	
				}else{
					$from1=date('Y-m-d', strtotime(str_replace("/","-",$_POST['date_from'])));	
				}
	}
	//if($time){
	//	$from=date('Y-m-d', strtotime($time['from']));
	//	$to=date('Y-m-d', strtotime($time['to']));
	//}
	if($from1){
		$invoiceWhere=" And DATE_FORMAT(aos_invoices.invoice_date,'%Y-%m-%d') ='".$from1."'";
		$oppWhere=" DATE_FORMAT(opportunities.depositdate,'%Y-%m-%d') ='".$from1."'";
		$accountWhere=" DATE_FORMAT(accounts.date_entered,'%Y-%m-%d') = DATE_FORMAT('".$from1."','%Y-%m-%d')";
		$billingWhere=" And DATE_FORMAT(bill_billing.billingdate,'%Y-%m-%d') ='".$from1."'";
	}else{
		$invoiceWhere=" And DATE_FORMAT(aos_invoices.invoice_date,'%Y-%m-%d') >='".$from."' And DATE_FORMAT(aos_invoices.invoice_date,'%Y-%m-%d') <='".$to."'";
		$oppWhere=" DATE_FORMAT(opportunities.depositdate,'%Y-%m-%d') >='".$from."' And DATE_FORMAT(opportunities.depositdate,'%Y-%m-%d') <='".$to."'";
		$accountWhere=" DATE_FORMAT(accounts.date_entered,'%Y-%m-%d') >= DATE_FORMAT('".$from."' ,'%Y-%m-%d') And DATE_FORMAT(accounts.date_entered,'%Y-%m-%d') <= DATE_FORMAT('".$to."','%Y-%m-%d')";
		$billingWhere=" And DATE_FORMAT(bill_billing.billingdate,'%Y-%m-%d') >='".$from."' And DATE_FORMAT(bill_billing.billingdate,'%Y-%m-%d') <='".$to."'";
	}
}else{
	$invoiceWhere="";
	$accountWhere="";
	$oppWhere="";
	$billingWhere="";
}	
	$whereProvince = '';
	$whereGroup = '';
	$whereTitle = '';
	$whereId = '';
	$user='';
	$whereAll = '';
	$queryArray = array();
	if($_POST["province"] != "All"){
		$que = "select id from users inner join users_cstm on users.id = users_cstm.id_c where users_cstm.provincecode_c = '".$_POST["province"]."' and users.deleted = '0'"; 
		$res = $db->query($que);
		while($row = $db->fetchByAssoc($res)){
			if(!empty($row["id"])){
				$whereProvince .= "'".$row["id"]."',";
			}
		}
		if($whereProvince != ''){
			$whereProvince = trim($whereProvince,",");
			$whereProvince = "IN (".$whereProvince.")";
			$queryArray[] = $whereProvince;
		}
	}else{
		$que = "select id from users inner join users_cstm on users.id = users_cstm.id_c where users.deleted = '0'"; 
		$res = $db->query($que);
		while($row = $db->fetchByAssoc($res)){
			if(!empty($row["id"])){
				$whereProvince .= "'".$row["id"]."',";
			}
		}
		if($whereProvince != ''){
			$whereProvince = trim($whereProvince,",");
			$whereProvince = "IN (".$whereProvince.")";
			$queryArray[] = $whereProvince;
		}
		
	}
	if($_POST["groupUsers"]){
		if(array_search('All',$_POST["groupUsers"]) == false && count($_POST["groupUsers"])>0){
			for($i = 0;$i<count($_POST["groupUsers"]);$i++){
				$que = "select id from users inner join users_cstm on users.id = users_cstm.id_c where users.department = '".$_POST["groupUsers"][$i]."' and users.deleted = '0'"; 
				$res = $db->query($que);
				while($row = $db->fetchByAssoc($res)){
					if(!empty($row["id"])){
						$whereGroup .= "'".$row["id"]."',";
					}
				}
			}
			if($whereGroup != ''){
				$whereGroup = trim($whereGroup,",");
				$whereGroup = "IN (".$whereGroup.")";
				$queryArray[] = $whereGroup;
			}
		}else{
			for($i = 0;$i<count($_POST["groupUsers"]);$i++){
				$que = "select id from users inner join users_cstm on users.id = users_cstm.id_c where users.deleted = '0'";
				$res = $db->query($que);
				while($row = $db->fetchByAssoc($res)){
					if(!empty($row["id"])){
						$whereGroup .= "'".$row["id"]."',";
					}
				}
			}
			if($whereGroup != ''){
				$whereGroup = trim($whereGroup,",");
				$whereGroup = "IN (".$whereGroup.")";
				$queryArray[] = $whereGroup;
			}
		}
	}
	if($_POST["titleUsers"]){
		for($i = 0;$i<count($_POST["titleUsers"]);$i++){
			$que = "select id from users inner join users_cstm on users.id = users_cstm.id_c where users.title = '".$_POST["titleUsers"][$i]."' and users.deleted = '0'"; 
			$res = $db->query($que);
			while($row = $db->fetchByAssoc($res)){
				if(!empty($row["id"])){
					$whereTitle .= "'".$row["id"]."',";
				}
			}
		}
		if($whereTitle != ''){
			$whereTitle = trim($whereTitle,",");
			$whereTitle = "IN (".$whereTitle.")";
			$queryArray[] = $whereTitle;
		
		}
	}
	
	if($_POST["users"]){
		for($i = 0;$i<count($_POST["users"]);$i++){
			$que = "select id from users inner join users_cstm on users.id = users_cstm.id_c where users.id = '".$_POST["users"][$i]."' and users.deleted = '0'"; 
			$res = $db->query($que);
			while($row = $db->fetchByAssoc($res)){
				if(!empty($row["id"])){
					$whereId  .= "'".$row["id"]."',";
				}
			}
		}
		if($whereId != ''){
			$whereId  = trim($whereId,",");
			$whereId = "IN (".$whereId.")";
			$queryArray[] = $whereId;
		}
	}
	$whereAllAccount = '';
	$whereAllOpp = '';
	$whereAllInv = '';
	$whereAllBill = '';
	for($i=0;$i<count($queryArray);$i++){
		if($i < count($queryArray) -1){
			$whereAllAccount .= "accounts.assigned_user_id ".$queryArray[$i]." and ";
			$whereAllInv .= "aos_invoices.assigned_user_id ".$queryArray[$i]." and ";
		}
		else{
			$whereAllOpp .= "opportunities.assigned_user_id ".$queryArray[$i];
			$whereAllAccount .= "accounts.assigned_user_id ".$queryArray[$i];
			$whereAllInv .= "aos_invoices.assigned_user_id ".$queryArray[$i];
			$whereAllBill .= "bill_billing.assigned_user_id ".$queryArray[$i];
		}
	}
	/*
	if($accountWhere != ''){
		if($whereAllAccount != ''){
			$whereAllAccount = $whereAllAccount." and ".$accountWhere;
		}else{
			$whereAllAccount .= $accountWhere;
		}
	}
	*/
	$account = BeanFactory::newBean('Accounts');
		$queryNew = $account->create_new_list_query("accounts.date_entered DESC",$whereAllAccount,array());
		$q3 = "SELECT accounts.*,users.user_name,aos_invoices.invoice_date,SUM(aos_invoices.total_amount) as total
		FROM ($queryNew) as accounts
		inner join aos_invoices on accounts.id = aos_invoices.billing_account_id and aos_invoices.deleted = '0' and aos_invoices.invoice_date not between '".$from."' and '".$to."' and aos_invoices.invoice_date < '".$from."' inner join users on accounts.assigned_user_id = users.id WHERE accounts.deleted = 0 GROUP BY accounts.id,aos_invoices.invoice_date  ORDER BY accounts.name,aos_invoices.invoice_date DESC";
		$res3 = $account->db->query($q3);
		$AllTotal = 0;
		$id='';
		while($r3 = $account->db->fetchByAssoc($res3)){
			$AllTotal += $r3["total"];
			$q2 = "select email_addresses.email_address from email_addresses inner join email_addr_bean_rel on email_addresses.id = email_addr_bean_rel.email_address_id and email_addresses.deleted = '0' inner join accounts on accounts.id =  email_addr_bean_rel.bean_id and accounts.id = '".$r3["id"]."' and accounts.deleted = '0' where email_addr_bean_rel.primary_address = '1'";
			$r2 = $db->query($q2);
			$row2 = $db->fetchByAssoc($r2);
			if($id != $r3["id"] && $r3["id"] != null){
				$arr[] = array(
					"name"=>$r3["name"],			
					"phone"=>$r3["phone_alternate"],
					"email"=>$row2["email_address"],
					"address"=>$r3["billing_address_street"],
					"contact"=>$r3["primarycontact_c"],
					"user"=>$r3["user_name"],
					"invoice_date"=>date("d-m-Y",strtotime($r3["invoice_date"])),
					"final_sales"=>$r3["total"]
				);
				$id = $r3["id"];
			}
		}
		$arr[] = array(
			"total" => $AllTotal,
			
			);		
	echo json_encode($arr);
}catch(Exception $e) {
	echo json_encode($e->getMessage());
}
		