<?php
global $db;
$whr='';
$q = " select id from users where id  = '".$_SESSION['authenticated_user_id']."' and is_admin = '1' and deleted = '0'";
$re = $db->query($q);
$r= $db->fetchByAssoc($re);
if(empty($r["id"])){
	$q2 = " select cs_cususer.id from cs_cususer inner join acl_roles on cs_cususer.role = acl_roles.id 
	where cs_cususer.refuser ='".$_SESSION['authenticated_user_id']."' and acl_roles.name <> 'Role cho nhan vien' and acl_roles.name <> 'TCT_ROLE'  and cs_cususer.deleted = '0'";
	$re2 = $db->query($q2);
	$r2= $db->fetchByAssoc($re2);
	if(empty($r2["id"])){
		$q3 = " select cs_cususer.id from cs_cususer inner join acl_roles on cs_cususer.role = acl_roles.id 
		where cs_cususer.refuser ='".$_SESSION['authenticated_user_id']."' and acl_roles.name = 'TCT_ROLE'  and cs_cususer.deleted = '0'";
		$re3 = $db->query($q3);
		$r3= $db->fetchByAssoc($re3);
		if(!empty($r3["id"])){
			$employee = false;
			$employer = true;
			$leader = false;
		}else{
			$employee = true;
			$employer = false;
			$leader = false;
		}
	}else{
		$employee = false;
		$employer = false;
		$leader = true;
	}
}else{
	$employee = false;
	$employer = true;
	$leader = false;
}
$query="SELECT securitygroup_id FROM securitygroups_users where user_id='".$_SESSION['authenticated_user_id']."' and deleted = '0'";
$res=$db->query($query);
while($row=$db->fetchByAssoc($res)){
	$whr.="'".$row['securitygroup_id']."'";
	$whr.=",";	
}

$whr=trim($whr,",");

function getDateTimeAsDb($period = 'this_month', $date_from = '', $date_to = '')
    {
        global $timedate;
		date_default_timezone_set('Asia/Ho_Chi_Minh');
        if(empty($date_from) && empty($date_to)) {
            $range = getDateRangeToSearch($period);

            $from_by_user = $range['from']->format($timedate::DB_DATETIME_FORMAT);
            $to_by_user = $range['to']->format($timedate::DB_DATETIME_FORMAT);

        }
        else {

            $fromObj = $timedate->fromUserDate($date_from);
            $fromObj->setTime(0, 0, 0);
            $from_by_user = $fromObj->format($timedate::DB_DATETIME_FORMAT);

            $toObj = $timedate->fromUserDate($date_to);
            $toObj->setTime(23,59,59);
            $to_by_user = $toObj->format($timedate::DB_DATETIME_FORMAT);

        }

        $from_display = $timedate->to_display_date_time($from_by_user, true, false);
        $from = $timedate->to_db($from_display);

        $to_display = $timedate->to_display_date_time($to_by_user, true, false);
        $to = $timedate->to_db($to_display);

        $datetime_as_db = array(
            'from' => $from,
            'to' => $to
        );

        return $datetime_as_db;
    }
function getDateRangeToSearch($period_list_selected = 'this_month')
    {
        if(empty($period_list_selected)) $period_list_selected = 'this_month';
		date_default_timezone_set('Asia/Ho_Chi_Minh');
        switch ($period_list_selected) {
            case 'this_week':
                $datetimeTo = new DateTime("next week monday");
              //  $datetimeTo->setTime(23, 59, 59);
			   
                $datetimeFrom = new DateTime("this week tuesday");
				 
               // $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'last_week':
                $datetimeTo = new DateTime("this week monday");
              //  $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("last week tuesday");
              //  $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'this_month':
                $datetimeTo = new DateTime('last day of this month');
               // $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("first day of this month");
               // $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'last_month':
                $datetimeTo = new DateTime("last day of last month");
                //$datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("first day of last month");
               // $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'this_year':
                $datetimeTo = new DateTime('this year last day of december');
                //$datetimeTo->setTime(23, 59, 59);
				$datetimeTo->add(new DateInterval('P1D'));
                $datetimeFrom = new DateTime('this year first day of january');
               // $datetimeFrom->setTime(0, 0, 0);
			    $datetimeFrom->add(new DateInterval('P1D'));
                break;
            case 'last_year':
                $datetimeTo = new DateTime('last year last day of december');
				$datetimeTo->add(new DateInterval('P1D'));
               // $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime('last year first day of january');
               // $datetimeFrom->setTime(0, 0, 0);
			    $datetimeFrom->add(new DateInterval('P1D'));
                break;

        }
        return array(
            'from' => $datetimeFrom,
            'to' => $datetimeTo
        );
    }
	
$prov=array(
"10"=>" TP Hà Nội",
"16"=>"Tỉnh Hưng Yên",
"17"=>"Tỉnh Hải Dương",
"18"=>"Tp Hải Phòng",
"20"=>"Tỉnh Quảng Ninh",
"22"=>"Tỉnh Bắc Ninh",
"23"=>"Tỉnh Bắc Giang",
"24"=>"Tỉnh Lạng Sơn",
"25"=>"Tỉnh Thái Nguyên",
"26"=>"Tỉnh Bắc Kạn",
"27"=>"Tỉnh Cao Bằng",
"28"=>"Tỉnh Vĩnh Phúc",
"29"=>"Tỉnh Phú Thọ",
"30"=>"Tỉnh Tuyên Quang",
"31"=>"Tỉnh Hà Giang",
"32"=>"Tỉnh Yên Bái",
"33"=>"Tỉnh Lào Cai",
"35"=>"Tỉnh Hoà Bình",
"36"=>"Tỉnh Sơn La",
"38"=>"Tỉnh Điện Biên",
"39"=>"Tỉnh Lai Châu",
"40"=>"Tỉnh Hà Nam",
"41"=>"Tỉnh Thái Bình",
"42"=>"Tỉnh Nam Định",
"43"=>"Tỉnh Ninh Bình",
"44"=>"Tỉnh Thanh Hóa",
"46"=>"Tỉnh Nghệ An",
"48"=>"Tỉnh Hà Tĩnh",
"51"=>"Tỉnh Quảng Bình",
"52"=>"Tỉnh Quảng Trị",
"53"=>"Tỉnh Thừa Thiên Huế",
"55"=>"TP Đà Nẵng",
"56"=>"Tỉnh Quảng Nam",
"57"=>"Tỉnh Quảng Ngãi",
"58"=>"Tỉnh Kon Tum",
"59"=>"Tỉnh Bình Định",
"60"=>"Tỉnh Gia Lai",
"62"=>"Tỉnh Phú Yên",
"63"=>"Tỉnh Đắk Lăk",
"64"=>"Tỉnh Đắk Nông",
"65"=>"Tỉnh Khánh Hoà",
"66"=>"Tỉnh Ninh Thuận",
"67"=>"Tỉnh Lâm Đồng",
"68"=>"Tỉnh Hưng Yên",
"69"=>"Tỉnh Hưng Yên",
"70"=>"TP Hồ Chí Minh",
"79"=>"Tỉnh Bà Rịa Vũng Tàu",
"80"=>"Tỉnh Bình Thuận",
"81"=>"Tỉnh Đồng Nai",
"82"=>"Tỉnh Bình Dương",
"83"=>"Tỉnh Bình Phước",
"84"=>"Tỉnh Tây Ninh",
"85"=>"Tỉnh Long An",
"86"=>"Tỉnh Tiền Giang",
"87"=>"Tỉnh Đồng Tháp",
"88"=>"Tỉnh An Giang",
"89"=>"Tỉnh Vĩnh Long",
"90"=>"TP Cần Thơ",
"91"=>"Tỉnh Hậu Giang",
"92"=>"Tỉnh Kiên Giang",
"93"=>"Tỉnh Bến Tre",
"94"=>"Tỉnh Trà Vinh",
"95"=>"Tỉnh Sóc Trăng",
"96"=>"Tỉnh Bạc Liêu",
"97"=>"Tỉnh Cà Mau",
"98"=>"Vnpost",
"99"=>"PPTT",
"100"=>"DVBC",
"101"=>"TCBC",
"102"=>"HCC",
"103"=>"Global"
);
$user=array();
$group=array();
$province=array();
$limit=" Limit 0,20";
if((!empty($_POST['period'])) && ($_POST['period'] != 'default')){
				if($_POST['period'] != "is_between"){
					$time=getDateTimeAsDb($_POST['period']);
				}
				else{
					if(empty($_POST['date_from']) && empty($_POST['date_to'])){
					$leadWhere="";
					$accountWhere="";
					$oppWhere="";
					$callWhere="";
					$meetingWhere="";
					$emailWhere="";
					}else{
						if(!empty($_POST['date_from']) && !empty($_POST['date_to'])){
							if(strtotime($_POST['date_from']) != strtotime($_POST['date_to'])){
								$from = date('Y-m-d', strtotime(str_replace("/","-",$_POST['date_from'])));	
								$to = date('Y-m-d', strtotime(str_replace("/","-",$_POST['date_to'])));	
							}else{
								$from1=date('Y-m-d', strtotime(str_replace("/","-",$_POST['date_from'])));	
							}
						}
					}
				}
			if($time){
				$from=date('Y-m-d', strtotime($time['from']));
				$to=date('Y-m-d', strtotime($time['to']));
			}
			if($from1){
				$leadWhere=" And DATE_FORMAT(leads.date_entered,'%Y-%m-%d') ='".$from1."'";
				$accountWhere=" And DATE_FORMAT(accounts.date_entered,'%Y-%m-%d') ='".$from1."'";
				$oppWhere=" And DATE_FORMAT(opportunities.date_closed,'%Y-%m-%d') ='".$from1."'";
				$callWhere=" And DATE_FORMAT(calls.date_entered,'%Y-%m-%d') ='".$from1."'";
				$meetingWhere=" And DATE_FORMAT(meetings.date_entered,'%Y-%m-%d') ='".$from1."'";
			}else{
				$leadWhere=" And DATE_FORMAT(leads.date_entered,'%Y-%m-%d') >='".$from."' And DATE_FORMAT(leads.date_entered,'%Y-%m-%d') <='".$to."'";
				$accountWhere=" And DATE_FORMAT(accounts.date_entered,'%Y-%m-%d') >='".$from."' And DATE_FORMAT(accounts.date_entered,'%Y-%m-%d') <='".$to."'";
				$oppWhere=" And DATE_FORMAT(opportunities.date_closed,'%Y-%m-%d') >='".$from."' And DATE_FORMAT(opportunities.date_closed,'%Y-%m-%d') <='".$to."'";
				$callWhere=" And DATE_FORMAT(calls.date_entered,'%Y-%m-%d') >='".$from."' And DATE_FORMAT(calls.date_entered,'%Y-%m-%d') <='".$to."'";
				$meetingWhere=" And DATE_FORMAT(meetings.date_entered,'%Y-%m-%d') >='".$from."' And DATE_FORMAT(meetings.date_entered,'%Y-%m-%d') <='".$to."'";
			}
}else{
	$leadWhere="";
	$accountWhere="";
	$oppWhere="";
	$callWhere="";
	$meetingWhere="";
} 

if($_POST["province"] != null){
		if($_POST["province"] != "All"){
			if($_POST['groupUsers'] == null || $_POST['groupUsers'] == '' || $_POST['groupUsers'][0] == "All"){
					if($_POST["users"] == null || $_POST["users"] == ''){
						if($_POST["titleUsers"] == null || $_POST["titleUsers"] == ''){
							if($employer == false || $employee == true || $leader == true){
								if($leader == true){
									$qr="select distinct(securitygroups.name) as seName,users.id as id,users.user_name as name,users.department as Se from users inner join users_cstm on users.id=users_cstm.id_c inner join 
									securitygroups_users on users.id=securitygroups_users.user_id inner join  securitygroups on securitygroups.id = securitygroups_users.securitygroup_id where users.deleted='0' and users.status='Active' and users_cstm.provincecode_c='".$_POST["province"]."'
									and securitygroups_users.securitygroup_id In (".$whr.")";
								}
								if($employee == true){
									$qr="select distinct(securitygroups.name) as seName,users.id as id,users.user_name as name,users.department as Se from users inner join users_cstm on users.id=users_cstm.id_c inner join 
									securitygroups_users on users.id=securitygroups_users.user_id inner join  securitygroups on securitygroups.id = securitygroups_users.securitygroup_id where users.deleted='0' and users.status='Active' and users_cstm.provincecode_c='".$_POST["province"]."'
									and users.id =  '".$_SESSION['authenticated_user_id']."'";
								}
							}else{
								if($employer == true){
									$qr="select distinct(securitygroups.name) as seName,users.id as id,users.user_name as name,users.department as Se from users inner join users_cstm on users.id=users_cstm.id_c inner join 
									securitygroups_users on users.id=securitygroups_users.user_id inner join  securitygroups on securitygroups.id = securitygroups_users.securitygroup_id where users.deleted='0' and users.status='Active' and users_cstm.provincecode_c='".$_POST["province"]."'"; 
								}
							}
							$res=$db->query($qr);
							while($row=$db->fetchByAssoc($res)){
								$call = array();
								$meeting = array();
								$opp = array();
								$account = array();
								$lead = array();
								if(!empty($row)){
									if($row["seName"] == $row["Se"]){
										$queryLead = "select leads.* , leads_cstm.*,users.user_name,aos_products.name as proName,email_addresses.* from leads inner join leads_cstm on leads.id = leads_cstm.id_c left join aos_products_leads_1_c on leads.id = aos_products_leads_1_c.aos_products_leads_1leads_idb left join aos_products on aos_products_leads_1_c.aos_products_leads_1aos_products_ida = aos_products.id inner join users on leads.assigned_user_id = users.id left join email_addr_bean_rel on leads.id = email_addr_bean_rel.bean_id and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'  left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id where leads.assigned_user_id = '".$row['id']."' and leads.deleted ='0' and leads.status NOT IN ('Converted','Transaction') ".$leadWhere;
										$resultLead=$db->query($queryLead);
										while($rowLead =$db->fetchByAssoc($resultLead)){
											
											$rowLead["date_entered"] = date('d/m/Y H:i:s', strtotime($rowLead["date_entered"].'+ 7 hours'));
												$lead[] = $rowLead;
										}
										$queryAccount = "select accounts.* , accounts_cstm.*,email_addresses.email_address,aos_products.name as proName,users.user_name  from accounts inner join accounts_cstm on accounts.id = accounts_cstm.id_c left join email_addr_bean_rel on accounts.id = email_addr_bean_rel.bean_id and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'  left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id left join aos_products_accounts_1_c on accounts.id = aos_products_accounts_1_c.aos_products_accounts_1accounts_idb left join aos_products on aos_products_accounts_1_c.aos_products_accounts_1aos_products_ida = aos_products.id inner join users on accounts.assigned_user_id = users.id  where accounts.assigned_user_id = '".$row['id']."' and accounts.deleted ='0'".$accountWhere;
										$resultAccount=$db->query($queryAccount);
										while($rowAccount =$db->fetchByAssoc($resultAccount)){
											$amountQuery = "SELECT SUM(o.amount) AS amo
											FROM opportunities o INNER JOIN accounts_opportunities ao ON o.id = ao.opportunity_id AND ao.deleted = 0
											WHERE ao.account_id='".$rowAccount['id']."' AND o.deleted = 0
											AND o.sales_stage = 'Closed Won' ".$oppWhere;
											$resultAmount=$db->query($amountQuery);
											$rowAmount =$db->fetchByAssoc($resultAmount);
											$rowAccount["amount"] = $rowAmount["amo"];
											$rowAccount["date_entered"] = date('d/m/Y H:i:s', strtotime($rowAccount["date_entered"].'+ 7 hours'));
											$account[] = $rowAccount;
										}
										
										
										
										$queryMeeting = "select meetings.*,leads.last_name,leads.first_name,leads.phone_mobile,accounts.name as accName,accounts.phone_alternate,email_addresses.* , users.user_name from meetings left join leads on meetings.parent_id = leads.id left join accounts on meetings.parent_id = accounts.id inner join users on users.id = meetings.assigned_user_id left join email_addr_bean_rel on (leads.id = email_addr_bean_rel.bean_id OR accounts.id = email_addr_bean_rel.bean_id) and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'   left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id   where meetings.assigned_user_id = '".$row['id']."' and meetings.deleted ='0'".$meetingWhere;
										$resultMeeting=$db->query($queryMeeting);
										while($rowMeeting =$db->fetchByAssoc($resultMeeting)){
											$rowMeeting["date_start"] = date('d/m/Y H:i:s', strtotime($rowMeeting["date_start"].'+ 7 hours'));
											$meeting[] = $rowMeeting;
										}
										$queryCall = "select calls.*,leads.last_name,leads.first_name,leads.phone_mobile,accounts.name as accName,accounts.phone_alternate,email_addresses.*,users.user_name  from calls left join leads on calls.parent_id = leads.id left join accounts on calls.parent_id = accounts.id inner join users on users.id = calls.assigned_user_id left join email_addr_bean_rel on (leads.id = email_addr_bean_rel.bean_id OR accounts.id = email_addr_bean_rel.bean_id) and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'   left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id  where calls.assigned_user_id = '".$row['id']."' and calls.deleted ='0'".$callWhere;
										$resultCall=$db->query($queryCall);
										while($rowCall =$db->fetchByAssoc($resultCall)){
											$rowCall["date_start"] = date('d/m/Y H:i:s', strtotime($rowCall["date_start"].'+ 7 hours'));
											$call[] = $rowCall;
										}
										$queryOpp = "select opportunities.*, accounts.name as accName,accounts.phone_alternate,email_addresses.email_address,users.user_name ,aos_products.name as proName,accounts.name as accName from opportunities left join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id left join accounts on accounts_opportunities.account_id = accounts.id left join email_addr_bean_rel on accounts.id = email_addr_bean_rel.bean_id and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'  left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id  left join aos_products_opportunities_1_c on opportunities.id = aos_products_opportunities_1_c.aos_products_opportunities_1opportunities_idb left join aos_products on aos_products_opportunities_1_c.aos_products_opportunities_1aos_products_ida = aos_products.id inner join users on opportunities.assigned_user_id = users.id where opportunities.assigned_user_id = '".$row['id']."' and opportunities.deleted ='0'".$oppWhere;
										$resultOpp=$db->query($queryOpp);
										while($rowOpp =$db->fetchByAssoc($resultOpp)){
											$rowOpp["date_closed"] = date('d/m/Y H:i:s', strtotime($rowOpp["date_closed"].'+ 7 hours'));
											$opp[] = $rowOpp;
										}
										if($employee == true){
											if($row["id"] == $_SESSION['authenticated_user_id']){
												$user = array(
															"user"=>$row['name'],
															"Lead"=>$lead,
															"Account"=>$account,
															"Call"=>$call,
															"Meeting"=>$meeting,
															"Opp"=>$opp,
														);
												if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
													$group[$row["Se"]]=array();
													$group[$row["Se"]][]=$user;
												}else{
													$group[$row["Se"]][]=$user;
												}
											}
										}
										else
										{
											$user = array(
														"user"=>$row['name'],
														"Lead"=>$lead,
														"Account"=>$account,
														"Call"=>$call,
														"Meeting"=>$meeting,
														"Opp"=>$opp,
													);
											if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
												$group[$row["Se"]]=array();
												$group[$row["Se"]][]=$user;
											}else{
												$group[$row["Se"]][]=$user;
											}
												
										}
									}
								}
							}	
						}	
						else{
							for($i=0;$i<count($_POST["titleUsers"]);$i++){
								$call = array();
								$meeting = array();
								$opp = array();
								$account = array();
								$lead = array();
								if($employer == false || $employee == true || $leader == true){
									if($leader == true){
										$q="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
										inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
										users.status='Active' and users_cstm.provincecode_c='".$_POST["province"]."' and securitygroups_users.securitygroup_id In (".$whr.") and users.title = '".$_POST["titleUsers"][$i]."'";
									}
									if($employee == true){
										$q="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
										inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
										users.status='Active' and users_cstm.provincecode_c='".$_POST["province"]."' and users.id =  '".$_SESSION['authenticated_user_id']."' and users.title = '".$_POST["titleUsers"][$i]."'";
										
									}
								}else{
									if($employer == true){
										$q="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
										inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
										users.status='Active' and users_cstm.provincecode_c='".$_POST["province"]."' and users.title = '".$_POST["titleUsers"][$i]."'";
									}
								}
								$r=$db->query($q);
								while($rww=$db->fetchByAssoc($r)){
									if(!empty($rww)){
										if($rww["seName"]==$rww["Se"]){
											$queryLead = "select leads.* , leads_cstm.*,users.user_name,aos_products.name as proName,email_addresses.* from leads inner join leads_cstm on leads.id = leads_cstm.id_c left join aos_products_leads_1_c on leads.id = aos_products_leads_1_c.aos_products_leads_1leads_idb left join aos_products on aos_products_leads_1_c.aos_products_leads_1aos_products_ida = aos_products.id inner join users on leads.assigned_user_id = users.id left join email_addr_bean_rel on leads.id = email_addr_bean_rel.bean_id and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'  left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id where leads.assigned_user_id = '".$rww['id']."' and leads.deleted ='0' and leads.status NOT IN ('Converted','Transaction') ".$leadWhere;
											$resultLead=$db->query($queryLead);
											while($rowLead =$db->fetchByAssoc($resultLead)){
												$rowLead["date_entered"] = date('d/m/Y H:i:s', strtotime($rowLead["date_entered"].'+ 7 hours'));
												$lead[] = $rowLead;
											}
											$queryAccount = "select accounts.* , accounts_cstm.*,email_addresses.email_address,aos_products.name as proName,users.user_name  from accounts inner join accounts_cstm on accounts.id = accounts_cstm.id_c left join email_addr_bean_rel on accounts.id = email_addr_bean_rel.bean_id and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'  left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id left join aos_products_accounts_1_c on accounts.id = aos_products_accounts_1_c.aos_products_accounts_1accounts_idb left join aos_products on aos_products_accounts_1_c.aos_products_accounts_1aos_products_ida = aos_products.id inner join users on accounts.assigned_user_id = users.idwhere accounts.assigned_user_id = '".$rww['id']."' and accounts.deleted ='0'".$accountWhere;
											$resultAccount=$db->query($queryAccount);
											while($rowAccount =$db->fetchByAssoc($resultAccount)){
												$amountQuery = "SELECT SUM(o.amount) AS amo
												FROM opportunities o INNER JOIN accounts_opportunities ao ON o.id = ao.opportunity_id AND ao.deleted = 0
												WHERE ao.account_id='".$rowAccount['id']."' AND o.deleted = 0
												AND o.sales_stage = 'Closed Won' ".$oppWhere;
												$resultAmount=$db->query($amountQuery);
												$rowAmount =$db->fetchByAssoc($resultAmount);
												$rowAccount["amount"] = $rowAmount["amo"];
												$rowAccount["date_entered"] = date('d/m/Y H:i:s', strtotime($rowAccount["date_entered"].'+ 7 hours'));
												$account[] = $rowAccount;
											}
											$queryMeeting = "select meetings.*,leads.last_name,leads.first_name,leads.phone_mobile,accounts.name as accName,accounts.phone_alternate,email_addresses.* , users.user_name from meetings left join leads on meetings.parent_id = leads.id left join accounts on meetings.parent_id = accounts.id inner join users on users.id = meetings.assigned_user_id left join email_addr_bean_rel on (leads.id = email_addr_bean_rel.bean_id OR accounts.id = email_addr_bean_rel.bean_id) and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'   left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id   where meetings.assigned_user_id = '".$rww['id']."' and meetings.deleted ='0'".$meetingWhere;
											$resultMeeting=$db->query($queryMeeting);
											while($rowMeeting =$db->fetchByAssoc($resultMeeting)){
												$rowMeeting["date_start"] = date('d/m/Y H:i:s', strtotime($rowMeeting["date_start"].'+ 7 hours'));
												$meeting[] = $rowMeeting;
											}
											$queryCall = "select calls.*,leads.last_name,leads.first_name,leads.phone_mobile,accounts.name as accName,accounts.phone_alternate,email_addresses.*,users.user_name  from calls left join leads on calls.parent_id = leads.id left join accounts on calls.parent_id = accounts.id inner join users on users.id = calls.assigned_user_id left join email_addr_bean_rel on (leads.id = email_addr_bean_rel.bean_id OR accounts.id = email_addr_bean_rel.bean_id) and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'   left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id where calls.assigned_user_id = '".$rww['id']."' and calls.deleted ='0'".$callWhere;
											$resultCall=$db->query($queryCall);
											while($rowCall =$db->fetchByAssoc($resultCall)){
												$rowCall["date_start"] = date('d/m/Y H:i:s', strtotime($rowCall["date_start"].'+ 7 hours'));
												$call[] = $rowCall;
											}
											$queryOpp = "select opportunities.*, accounts.name as accName,accounts.phone_alternate,email_addresses.email_address,users.user_name ,aos_products.name as proName,accounts.name as accName from opportunities left join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id left join accounts on accounts_opportunities.account_id = accounts.id left join email_addr_bean_rel on accounts.id = email_addr_bean_rel.bean_id and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'  left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id  left join aos_products_opportunities_1_c on opportunities.id = aos_products_opportunities_1_c.aos_products_opportunities_1opportunities_idb left join aos_products on aos_products_opportunities_1_c.aos_products_opportunities_1aos_products_ida = aos_products.id inner join users on opportunities.assigned_user_id = users.id where opportunities.assigned_user_id = '".$rww['id']."' and opportunities.deleted ='0'".$oppWhere;
											$resultOpp=$db->query($queryOpp);
											while($rowOpp =$db->fetchByAssoc($resultOpp)){
												$rowOpp["date_closed"] = date('d/m/Y H:i:s', strtotime($rowOpp["date_closed"].'+ 7 hours'));
												$opp[] = $rowOpp;
											}
										}
										if($employee == true){
											if($rww['id'] == $_SESSION['authenticated_user_id']){	
												$user=array(
													"user"=>$row['name'],
													"Call"=>$call,
													"Meeting"=>$meeting,
													"Lead"=>$lead,
													"Account"=>$account,
													"Opp"=>$opp,
												);
												if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
													$group[$row["Se"]]=array();
													$group[$row["Se"]][]=$user;
												}
												else{
													$group[$row["Se"]][]=$user;
												}
											}
										}else{
											$user = array(
														"user"=>$row['name'],
														"Call"=>$call,
														"Meeting"=>$meeting,
														"Lead"=>$lead,
														"Account"=>$account,
														"Opp"=>$opp,
												    );
											if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
												$group[$row["Se"]]=array();
												$group[$row["Se"]][]=$user;
											}
											else{
												$group[$row["Se"]][]=$user;
											}
										}
									}
								}
							}
						}
					}else{
						for($i=0;$i<count($_POST["users"]);$i++){
							if($employer == false || $employee == true || $leader == true){
								if($leader == true){
									$qr="select users.user_name as name,users.department as Se from users inner join securitygroups_users on users.id=securitygroups_users.user_id where users.deleted='0' and users.status='Active' and  users.id='".$_POST["users"][$i]."' and securitygroups_users.securitygroup_id In (".$whr.")";	
								}
								if($employee == true){
									$qr="select users.user_name as name,users.department as Se from users inner join securitygroups_users on users.id=securitygroups_users.user_id where users.deleted='0' and users.status='Active' and  users.id='".$_POST["users"][$i]."' and users.id =  '".$_SESSION['authenticated_user_id']."'";	
								}
							}else{
								if($employer == true){
									$qr="select users.user_name as name,users.department as Se from users inner join securitygroups_users on users.id=securitygroups_users.user_id where users.deleted='0' and users.status='Active' and  users.id='".$_POST["users"][$i]."'";	
								}
							}	
							$res=$db->query($qr);
							$row=$db->fetchByAssoc($res);
							if(!empty($row)){
								$queryLead = "select leads.* , leads_cstm.*,users.user_name,aos_products.name as proName,email_addresses.* from leads inner join leads_cstm on leads.id = leads_cstm.id_c left join aos_products_leads_1_c on leads.id = aos_products_leads_1_c.aos_products_leads_1leads_idb left join aos_products on aos_products_leads_1_c.aos_products_leads_1aos_products_ida = aos_products.id inner join users on leads.assigned_user_id = users.id left join email_addr_bean_rel on leads.id = email_addr_bean_rel.bean_id and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'  left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id where leads.assigned_user_id = '".$_POST["users"][$i]."' and leads.deleted ='0' and leads.status NOT IN ('Converted','Transaction') ".$leadWhere;
								$resultLead=$db->query($queryLead);
								while($rowLead =$db->fetchByAssoc($resultLead)){
									$rowLead["date_entered"] = date('d/m/Y H:i:s', strtotime($rowLead["date_entered"].'+ 7 hours'));
									$lead[] = $rowLead;
								}
								$queryAccount = "select accounts.* , accounts_cstm.*,email_addresses.email_address,aos_products.name as proName,users.user_name  from accounts inner join accounts_cstm on accounts.id = accounts_cstm.id_c left join email_addr_bean_rel on accounts.id = email_addr_bean_rel.bean_id and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'  left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id left join aos_products_accounts_1_c on accounts.id = aos_products_accounts_1_c.aos_products_accounts_1accounts_idb left join aos_products on aos_products_accounts_1_c.aos_products_accounts_1aos_products_ida = aos_products.id inner join users on accounts.assigned_user_id = users.idwhere accounts.assigned_user_id = '".$_POST["users"][$i]."' and accounts.deleted ='0'".$accountWhere;
								$resultAccount=$db->query($queryAccount);
								while($rowAccount =$db->fetchByAssoc($resultAccount)){
									$amountQuery = "SELECT SUM(o.amount) AS amo
									FROM opportunities o INNER JOIN accounts_opportunities ao ON o.id = ao.opportunity_id AND ao.deleted = 0
									WHERE ao.account_id='".$rowAccount['id']."' AND o.deleted = 0
									AND o.sales_stage = 'Closed Won' ".$oppWhere;
									$resultAmount=$db->query($amountQuery);
									$rowAmount =$db->fetchByAssoc($resultAmount);
									$rowAccount["amount"] = $rowAmount["amo"];
									$rowAccount["date_entered"] = date('d/m/Y H:i:s', strtotime($rowAccount["date_entered"].'+ 7 hours'));
									$account[] = $rowAccount;
								}
								$queryMeeting = "select meetings.*,leads.last_name,leads.first_name,leads.phone_mobile,accounts.name as accName,accounts.phone_alternate,email_addresses.* , users.user_name from meetings left join leads on meetings.parent_id = leads.id left join accounts on meetings.parent_id = accounts.id inner join users on users.id = meetings.assigned_user_id left join email_addr_bean_rel on (leads.id = email_addr_bean_rel.bean_id OR accounts.id = email_addr_bean_rel.bean_id) and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'   left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id   where meetings.assigned_user_id = '".$_POST["users"][$i]."' and meetings.deleted ='0'".$meetingWhere;
								$resultMeeting=$db->query($queryMeeting);
								while($rowMeeting =$db->fetchByAssoc($resultMeeting)){
									$rowMeeting["date_start"] = date('d/m/Y H:i:s', strtotime($rowMeeting["date_start"].'+ 7 hours'));
									$meeting[] = $rowMeeting;
								}
								$queryCall = "select calls.*,leads.last_name,leads.first_name,leads.phone_mobile,accounts.name as accName,accounts.phone_alternate,email_addresses.*,users.user_name  from calls left join leads on calls.parent_id = leads.id left join accounts on calls.parent_id = accounts.id inner join users on users.id = calls.assigned_user_id left join email_addr_bean_rel on (leads.id = email_addr_bean_rel.bean_id OR accounts.id = email_addr_bean_rel.bean_id) and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'   left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id where calls.assigned_user_id = '".$_POST["users"][$i]."' and calls.deleted ='0'".$callWhere;
								$resultCall=$db->query($queryCall);
								while($rowCall =$db->fetchByAssoc($resultCall)){
									$rowCall["date_start"] = date('d/m/Y H:i:s', strtotime($rowCall["date_start"].'+ 7 hours'));
									$call[] = $rowCall;
								}
								$queryOpp = "select opportunities.*, accounts.name as accName,accounts.phone_alternate,email_addresses.email_address,users.user_name ,aos_products.name as proName,accounts.name as accName from opportunities left join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id left join accounts on accounts_opportunities.account_id = accounts.id left join email_addr_bean_rel on accounts.id = email_addr_bean_rel.bean_id and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'  left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id  left join aos_products_opportunities_1_c on opportunities.id = aos_products_opportunities_1_c.aos_products_opportunities_1opportunities_idb left join aos_products on aos_products_opportunities_1_c.aos_products_opportunities_1aos_products_ida = aos_products.id inner join users on opportunities.assigned_user_id = users.id where opportunities.assigned_user_id = '".$_POST["users"][$i]."' and opportunities.deleted ='0'".$oppWhere;
								$resultOpp=$db->query($queryOpp);
								while($rowOpp =$db->fetchByAssoc($resultOpp)){
									$rowOpp["date_closed"] = date('d/m/Y H:i:s', strtotime($rowOpp["date_closed"].'+ 7 hours'));
									$opp[] = $rowOpp;
								}
								if($employee == true){
									if($_POST["users"][$i] == $_SESSION['authenticated_user_id']){	
										$user = array(
														"user"=>$row['name'],
														"Call"=>$call,
														"Meeting"=>$meeting,
														"Lead"=>$lead,
														"Account"=>$account,
														"Opp"=>$opp,
													);
										if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
											$group[$row["Se"]]=array();
											$group[$row["Se"]][]=$user;
										}
										else{
											$group[$row["Se"]][]=$user;
										}
									}
								}
								else
								{
									$user = array(
												"user"=>$row['name'],
												"Call"=>$call,
												"Meeting"=>$meeting,
												"Lead"=>$lead,
												"Account"=>$account,
												"Opp"=>$opp,
											);
									if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
										$group[$row["Se"]]=array();
										$group[$row["Se"]][]=$user;
									}
									else{
										$group[$row["Se"]][]=$user;
									}
								}
							}
							
						}
					}
				}
				else
				{
					if($_POST["users"] == null || $_POST["users"] == ''){
						if($_POST["titleUsers"] == null || $_POST["titleUsers"] == ''){
							for($j=0;$j<count($_POST["groupUsers"]);$j++){
								$grs.="'".$_POST["groupUsers"][$j]."'";
								if($j<count($_POST["groupUsers"])-1){
									$grs.=",";	
								}
								if($_POST["groupUsers"][$j] == "All"){
									$grs="All";
									break;
								}
							}
							if($grs != "All"){
								if($employer == false || $employee == true || $leader == true){
									if($leader == true){
										$qr="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
										inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
										users.status='Active' and users_cstm.provincecode_c='".$_POST["province"]."' and securitygroups_users.securitygroup_id In (".$grs.") and securitygroups_users.securitygroup_id In (".$whr.")";
									}
									if($employee == true){
										$qr="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
										inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
										users.status='Active' and users_cstm.provincecode_c='".$_POST["province"]."' and securitygroups_users.securitygroup_id In (".$grs.") and users.id =  '".$_SESSION['authenticated_user_id']."'";
									}
								}else{
									if($employer == true){
										$qr="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
										inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
										users.status='Active' and users_cstm.provincecode_c='".$_POST["province"]."' and securitygroups_users.securitygroup_id In (".$grs.")";	
									}							
								}								
							}
							else{
								if($employer == false || $employee == true || $leader == true){
									$qr="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
									inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
									users.status='Active' and users_cstm.provincecode_c='".$_POST["province"]."' and securitygroups_users.securitygroup_id In (".$whr.")";	
								}else{
									if($employer == true){
										$qr="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
										inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
										users.status='Active' and users_cstm.provincecode_c='".$_POST["province"]."'";	
									}
									
								}
							}
							$res=$db->query($qr);
							while($row=$db->fetchByAssoc($res)){
								$call = array();
								$meeting = array();
								$opp = array();
								$account = array();
								$lead = array();
								if(!empty($row)){
									$queryLead = "select leads.* , leads_cstm.*,users.user_name,aos_products.name as proName,email_addresses.* from leads inner join leads_cstm on leads.id = leads_cstm.id_c left join aos_products_leads_1_c on leads.id = aos_products_leads_1_c.aos_products_leads_1leads_idb left join aos_products on aos_products_leads_1_c.aos_products_leads_1aos_products_ida = aos_products.id inner join users on leads.assigned_user_id = users.id left join email_addr_bean_rel on leads.id = email_addr_bean_rel.bean_id and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'  left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id where leads.assigned_user_id = '".$row['id']."' and leads.deleted ='0' and leads.status NOT IN ('Converted','Transaction') ".$leadWhere;
									$resultLead=$db->query($queryLead);
									while($rowLead =$db->fetchByAssoc($resultLead)){
										$rowLead["date_entered"] = date('d/m/Y H:i:s', strtotime($rowLead["date_entered"].'+ 7 hours'));
										$lead[] = $rowLead;
									}
									$queryAccount = "select accounts.* , accounts_cstm.*,email_addresses.email_address,aos_products.name as proName,users.user_name  from accounts inner join accounts_cstm on accounts.id = accounts_cstm.id_c left join email_addr_bean_rel on accounts.id = email_addr_bean_rel.bean_id and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'  left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id left join aos_products_accounts_1_c on accounts.id = aos_products_accounts_1_c.aos_products_accounts_1accounts_idb left join aos_products on aos_products_accounts_1_c.aos_products_accounts_1aos_products_ida = aos_products.id inner join users on accounts.assigned_user_id = users.idwhere accounts.assigned_user_id = '".$row['id']."' and accounts.deleted ='0'".$accountWhere;
									$resultAccount=$db->query($queryAccount);
									while($rowAccount =$db->fetchByAssoc($resultAccount)){
										$amountQuery = "SELECT SUM(o.amount) AS amo
										FROM opportunities o INNER JOIN accounts_opportunities ao ON o.id = ao.opportunity_id AND ao.deleted = 0
										WHERE ao.account_id='".$rowAccount['id']."' AND o.deleted = 0
										AND o.sales_stage = 'Closed Won' ".$oppWhere;
										$resultAmount=$db->query($amountQuery);
										$rowAmount =$db->fetchByAssoc($resultAmount);
										$rowAccount["amount"] = $rowAmount["amo"];
										$rowAccount["date_entered"] = date('d/m/Y H:i:s', strtotime($rowAccount["date_entered"].'+ 7 hours'));
										$account[] = $rowAccount;
									}
									$queryMeeting = "select meetings.*,leads.last_name,leads.first_name,leads.phone_mobile,accounts.name as accName,accounts.phone_alternate,email_addresses.* , users.user_name from meetings left join leads on meetings.parent_id = leads.id left join accounts on meetings.parent_id = accounts.id inner join users on users.id = meetings.assigned_user_id left join email_addr_bean_rel on (leads.id = email_addr_bean_rel.bean_id OR accounts.id = email_addr_bean_rel.bean_id) and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'   left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id   where meetings.assigned_user_id = '".$row['id']."' and meetings.deleted ='0'".$meetingWhere;
									$resultMeeting=$db->query($queryMeeting);
									while($rowMeeting =$db->fetchByAssoc($resultMeeting)){
										$rowMeeting["date_start"] = date('d/m/Y H:i:s', strtotime($rowMeeting["date_start"].'+ 7 hours'));
										$meeting[] = $rowMeeting;
									}
									$queryCall = "select calls.*,leads.last_name,leads.first_name,leads.phone_mobile,accounts.name as accName,accounts.phone_alternate,email_addresses.*,users.user_name  from calls left join leads on calls.parent_id = leads.id left join accounts on calls.parent_id = accounts.id inner join users on users.id = calls.assigned_user_id left join email_addr_bean_rel on (leads.id = email_addr_bean_rel.bean_id OR accounts.id = email_addr_bean_rel.bean_id) and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'   left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id where calls.assigned_user_id = '".$row['id']."' and calls.deleted ='0'".$callWhere;
									$resultCall=$db->query($queryCall);
									while($rowCall =$db->fetchByAssoc($resultCall)){
										$rowCall["date_start"] = date('d/m/Y H:i:s', strtotime($rowCall["date_start"].'+ 7 hours'));
										$call[] = $rowCall;
									}
									$queryOpp = "select opportunities.*, accounts.name as accName,accounts.phone_alternate,email_addresses.email_address,users.user_name ,aos_products.name as proName,accounts.name as accName from opportunities left join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id left join accounts on accounts_opportunities.account_id = accounts.id left join email_addr_bean_rel on accounts.id = email_addr_bean_rel.bean_id and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'  left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id  left join aos_products_opportunities_1_c on opportunities.id = aos_products_opportunities_1_c.aos_products_opportunities_1opportunities_idb left join aos_products on aos_products_opportunities_1_c.aos_products_opportunities_1aos_products_ida = aos_products.id inner join users on opportunities.assigned_user_id = users.id where opportunities.assigned_user_id = '".$row['id']."' and opportunities.deleted ='0'".$oppWhere;
									$resultOpp=$db->query($queryOpp);
									while($rowOpp =$db->fetchByAssoc($resultOpp)){
										$rowOpp["date_closed"] = date('d/m/Y H:i:s', strtotime($rowOpp["date_closed"].'+ 7 hours'));
										$opp[] = $rowOpp;
									}
									if($employee == true){
											if($row['id'] == $_SESSION['authenticated_user_id']){	
												$user=array(
														"user"=>$row['name'],
														"Call"=>$call,
														"Meeting"=>$meeting,
														"Lead"=>$lead,
														"Account"=>$account,
														"Opp"=>$opp,
													);
												if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
														$group[$row["Se"]]=array();
														$group[$row["Se"]][]=$user;
												}
												else{
														$group[$row["Se"]][]=$user;
												}
										}
									}else{
										$user=array(
												"user"=>$row['name'],
												"Call"=>$call,
												"Meeting"=>$meeting,
												"Lead"=>$lead,
												"Account"=>$account,
												"Opp"=>$opp,
											);
										if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
												$group[$row["Se"]]=array();
												$group[$row["Se"]][]=$user;
										}
										else{
												$group[$row["Se"]][]=$user;
										}
									}
								}
							}
						}
						}else{
							for($i=0;$i<count($_POST["users"]);$i++){
									if($employer == false || $employee == true || $leader == true){
									$qr="select users.user_name as name,users.department as Se from users inner join securitygroups_users on users.id=securitygroups_users.user_id where users.deleted='0' and users.status='Active' and  users.id='".$_POST["users"][$i]."' and securitygroups_users.securitygroup_id In (".$whr.")";	
								}else{
									if($employer == true){
										$qr="select users.user_name as name,users.department as Se from users inner join securitygroups_users on users.id=securitygroups_users.user_id where users.deleted='0' and users.status='Active' and  users.id='".$_POST["users"][$i]."'";
									}
								
								}
							
							$res=$db->query($qr);
							$row=$db->fetchByAssoc($res);
							if(!empty($row)){
								$queryLead = "select leads.* , leads_cstm.*,users.user_name,aos_products.name as proName,email_addresses.* from leads inner join leads_cstm on leads.id = leads_cstm.id_c left join aos_products_leads_1_c on leads.id = aos_products_leads_1_c.aos_products_leads_1leads_idb left join aos_products on aos_products_leads_1_c.aos_products_leads_1aos_products_ida = aos_products.id inner join users on leads.assigned_user_id = users.id left join email_addr_bean_rel on leads.id = email_addr_bean_rel.bean_id and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'  left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id where leads.assigned_user_id = '".$_POST["users"][$i]."' and leads.deleted ='0' and leads.status NOT IN ('Converted','Transaction') ".$leadWhere;
								$resultLead=$db->query($queryLead);
								while($rowLead =$db->fetchByAssoc($resultLead)){
										$rowLead["date_entered"] = date('d/m/Y H:i:s', strtotime($rowLead["date_entered"].'+ 7 hours'));
										$lead[] = $rowLead;
								}
								$queryAccount = "select accounts.* , accounts_cstm.*,email_addresses.email_address,aos_products.name as proName,users.user_name  from accounts inner join accounts_cstm on accounts.id = accounts_cstm.id_c left join email_addr_bean_rel on accounts.id = email_addr_bean_rel.bean_id and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'  left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id left join aos_products_accounts_1_c on accounts.id = aos_products_accounts_1_c.aos_products_accounts_1accounts_idb left join aos_products on aos_products_accounts_1_c.aos_products_accounts_1aos_products_ida = aos_products.id inner join users on accounts.assigned_user_id = users.idwhere accounts.assigned_user_id = '".$_POST["users"][$i]."' and accounts.deleted ='0'".$accountWhere;
								$resultAccount=$db->query($queryAccount);
								while($rowAccount =$db->fetchByAssoc($resultAccount)){
									$amountQuery = "SELECT SUM(o.amount) AS amo
									FROM opportunities o INNER JOIN accounts_opportunities ao ON o.id = ao.opportunity_id AND ao.deleted = 0
									WHERE ao.account_id='".$rowAccount['id']."' AND o.deleted = 0
									AND o.sales_stage = 'Closed Won' ".$oppWhere;
									$resultAmount=$db->query($amountQuery);
									$rowAmount =$db->fetchByAssoc($resultAmount);
									$rowAccount["amount"] = $rowAmount["amo"];
									$rowAccount["date_entered"] = date('d/m/Y H:i:s', strtotime($rowAccount["date_entered"].'+ 7 hours'));
									$account[] = $rowAccount;
								}
								$queryMeeting = "select meetings.*,leads.last_name,leads.first_name,leads.phone_mobile,accounts.name as accName,accounts.phone_alternate,email_addresses.* , users.user_name from meetings left join leads on meetings.parent_id = leads.id left join accounts on meetings.parent_id = accounts.id inner join users on users.id = meetings.assigned_user_id left join email_addr_bean_rel on (leads.id = email_addr_bean_rel.bean_id OR accounts.id = email_addr_bean_rel.bean_id) and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'   left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id   where meetings.assigned_user_id = '".$_POST["users"][$i]."' and meetings.deleted ='0'".$meetingWhere;
								$resultMeeting=$db->query($queryMeeting);
								while($rowMeeting =$db->fetchByAssoc($resultMeeting)){
									$rowMeeting["date_start"] = date('d/m/Y H:i:s', strtotime($rowMeeting["date_start"].'+ 7 hours'));
									$meeting[] = $rowMeeting;
								}
								$queryCall = "select calls.*,leads.last_name,leads.first_name,leads.phone_mobile,accounts.name as accName,accounts.phone_alternate,email_addresses.*,users.user_name  from calls left join leads on calls.parent_id = leads.id left join accounts on calls.parent_id = accounts.id inner join users on users.id = calls.assigned_user_id left join email_addr_bean_rel on (leads.id = email_addr_bean_rel.bean_id OR accounts.id = email_addr_bean_rel.bean_id) and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'   left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id where calls.assigned_user_id = '".$_POST["users"][$i]."' and calls.deleted ='0'".$callWhere;
								$resultCall=$db->query($queryCall);
								while($rowCall =$db->fetchByAssoc($resultCall)){
									$rowCall["date_start"] = date('d/m/Y H:i:s', strtotime($rowCall["date_start"].'+ 7 hours'));
									$call[] = $rowCall;
								}
								$queryOpp = "select opportunities.*, accounts.name as accName,accounts.phone_alternate,email_addresses.email_address,users.user_name ,aos_products.name as proName,accounts.name as accName from opportunities left join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id left join accounts on accounts_opportunities.account_id = accounts.id left join email_addr_bean_rel on accounts.id = email_addr_bean_rel.bean_id and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'  left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id  left join aos_products_opportunities_1_c on opportunities.id = aos_products_opportunities_1_c.aos_products_opportunities_1opportunities_idb left join aos_products on aos_products_opportunities_1_c.aos_products_opportunities_1aos_products_ida = aos_products.id inner join users on opportunities.assigned_user_id = users.id where opportunities.assigned_user_id = '".$_POST["users"][$i]."' and opportunities.deleted ='0'".$oppWhere;
								$resultOpp=$db->query($queryOpp);
								while($rowOpp =$db->fetchByAssoc($resultOpp)){
									$rowOpp["date_closed"] = date('d/m/Y H:i:s', strtotime($rowOpp["date_closed"].'+ 7 hours'));
									$opp[] = $rowOpp;
								}
								if($employee == true){
										if($_POST["users"][$i] == $_SESSION['authenticated_user_id']){	
											$user = array(
														"user"=>$row['name'],
														"Call"=>$call,
														"Meeting"=>$meeting,
														"Lead"=>$lead,
														"Account"=>$account,
														"Opp"=>$opp,
													);
										if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
											$group[$row["Se"]]=array();
											$group[$row["Se"]][]=$user;
										}else{
											$group[$row["Se"]][]=$user;
										}
										}
									}else{
										$user = array(
													"user"=>$row['name'],
													"Call"=>$call,
													"Meeting"=>$meeting,
													"Lead"=>$lead,
													"Account"=>$account,
													"Opp"=>$opp,
												);
										if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
											$group[$row["Se"]]=array();
											$group[$row["Se"]][]=$user;
										}else{
											$group[$row["Se"]][]=$user;
										}
									}
								}
							}
						}
					}
					/*
					else
					{
						for($i=0;$i<count($_POST["users"]);$i++){
							if($employer == false || $employee == true || $leader == true){
								$qr="select users.user_name as name,users.department as Se from users inner join securitygroups_users on users.id=securitygroups_users.user_id where users.deleted='0' and users.status='Active' and  users.id='".$_POST["users"][$i]."' and securitygroups_users.securitygroup_id In (".$whr.")";	
							}else{
								if($employer == true){
									$qr="select users.user_name as name,users.department as Se from users inner join securitygroups_users on users.id=securitygroups_users.user_id where users.deleted='0' and users.status='Active' and  users.id='".$_POST["users"][$i]."'";
								}
								
							}
							
							$res=$db->query($qr);
							$row=$db->fetchByAssoc($res);
							if(!empty($row)){
								$queryLead = "select * from leads where assigned_user_id = '".$_POST["users"][$i]."' and deleted ='0' ".$leadWhere;
								$resultLead=$db->query($queryLead);
								while($rowLead =$db->fetchByAssoc($resultLead)){
									$lead[] = $rowLead;
								}
								$queryAccount = "select * from accounts where assigned_user_id = '".$_POST["users"][$i]."' and deleted ='0'".$accountWhere;
								$resultAccount=$db->query($queryAccount);
								while($rowAccount =$db->fetchByAssoc($resultAccount)){
									$account[] = $rowAccount;
								}
								$queryMeeting = "select meetings.*,leads.last_name,leads.first_name,leads.phone_mobile,accounts.name as accName,accounts.phone_alternate,email_addresses.* , users.user_name from meetings left join leads on meetings.parent_id = leads.id left join accounts on meetings.parent_id = accounts.id inner join users on users.id = meetings.assigned_user_id left join email_addr_bean_rel on (leads.id = email_addr_bean_rel.bean_id OR accounts.id = email_addr_bean_rel.bean_id) and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'   left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id   where meetings.assigned_user_id = '".$_POST["users"][$i]."' and deleted ='0'".$meetingWhere;
								$resultMeeting=$db->query($queryMeeting);
								while($rowMeeting =$db->fetchByAssoc($resultMeeting)){
									$meeting[] = $rowMeeting;
								}
								$queryCall = "select calls.*,leads.last_name,leads.first_name,leads.phone_mobile,accounts.name as accName,accounts.phone_alternate,email_addresses.*,users.user_name  from calls left join leads on calls.parent_id = leads.id left join accounts on calls.parent_id = accounts.id inner join users on users.id = calls.assigned_user_id left join email_addr_bean_rel on (leads.id = email_addr_bean_rel.bean_id OR accounts.id = email_addr_bean_rel.bean_id) and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'   left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id where calls.assigned_user_id = '".$_POST["users"][$i]."' and deleted ='0'".$callWhere;
								$resultCall=$db->query($queryCall);
								while($rowCall =$db->fetchByAssoc($resultCall)){
									$call[] = $rowCall;
								}
								$queryOpp = "select * from opportunities where assigned_user_id = '".$_POST["users"][$i]."' and deleted ='0'".$oppWhere;
								$resultOpp=$db->query($queryOpp);
								while($rowOpp =$db->fetchByAssoc($resultOpp)){
									$opp[] = $rowOpp;
								}
								if($employee == true){
									if($_POST["users"][$i] == $_SESSION['authenticated_user_id']){	
										$user = array(
													"user"=>$row['name'],
													"Call"=>$call,
													"Meeting"=>$meeting,
													"Lead"=>$lead,
													"Account"=>$account,
													"Opp"=>$opp,
												);
										if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
											$group[$row["Se"]]=array();
											$group[$row["Se"]][]=$user;
										}else{
											$group[$row["Se"]][]=$user;
										}
									}
								}else{
									$user = array(
												"user"=>$row['name'],
												"Call"=>$call,
												"Meeting"=>$meeting,
												"Lead"=>$lead,
												"Account"=>$account,
												"Opp"=>$opp,
											);
									if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
										$group[$row["Se"]]=array();
										$group[$row["Se"]][]=$user;
									}else{
										$group[$row["Se"]][]=$user;
									}
								}
							}
						}
					}
					*/
				echo json_encode (array(array(
					"province"=>$prov[$_POST["province"]],
					"data"=>$group
				)));
		}else{
			for($k=10;$k<105;$k++){
				$p = "";
				if($_POST['groupUsers'] == null || $_POST['groupUsers'] == '' || $_POST['groupUsers'][0] == "All"){
					if($_POST["users"] == null || $_POST["users"] == ''){
						if($_POST["titleUsers"] == null || $_POST["titleUsers"] == ''){
						if($employer == false || $employee == true || $leader == true){
							if($leader == true){
								$qr="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join users_cstm on users.id=users_cstm.id_c inner join securitygroups_users on users.id = securitygroups_users.user_id
								inner join  securitygroups on securitygroups.id = securitygroups_users.securitygroup_id where users.deleted='0' and users.status='Active' and users_cstm.provincecode_c='".$k."' and securitygroups_users.securitygroup_id In (".$whr.")";	
							}
							if($employee == true){
								$qr="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join users_cstm on users.id=users_cstm.id_c inner join securitygroups_users on users.id = securitygroups_users.user_id
								inner join  securitygroups on securitygroups.id = securitygroups_users.securitygroup_id where users.deleted='0' and users.status='Active' and users_cstm.provincecode_c='".$k."' and users.id =  '".$_SESSION['authenticated_user_id']."'";	
							}
						}else{
							if($employer == true){
								$qr="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join users_cstm on users.id=users_cstm.id_c inner join securitygroups_users on users.id = securitygroups_users.user_id
								inner join  securitygroups on securitygroups.id = securitygroups_users.securitygroup_id where users.deleted='0' and users.status='Active' and users_cstm.provincecode_c='".$k."'";
							}
						}
						$res=$db->query($qr);
						while($row=$db->fetchByAssoc($res)){
							$call = array();
							$meeting = array();
							$opp = array();
							$lead = array();
							$account = array();
							if(!empty($row)){
								if($row["seName"] == $row["Se"]){
									$p=$k;
									$queryLead = "select leads.* , leads_cstm.*,users.user_name,aos_products.name as proName,email_addresses.* from leads inner join leads_cstm on leads.id = leads_cstm.id_c left join aos_products_leads_1_c on leads.id = aos_products_leads_1_c.aos_products_leads_1leads_idb left join aos_products on aos_products_leads_1_c.aos_products_leads_1aos_products_ida = aos_products.id inner join users on leads.assigned_user_id = users.id left join email_addr_bean_rel on leads.id = email_addr_bean_rel.bean_id and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'  left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id where leads.assigned_user_id = '".$row['id']."' and leads.deleted ='0' and leads.status NOT IN ('Converted','Transaction') ".$leadWhere;
									$resultLead=$db->query($queryLead);
									while($rowLead =$db->fetchByAssoc($resultLead)){
										$rowLead["date_entered"] = date('d/m/Y H:i:s', strtotime($rowLead["date_entered"].'+ 7 hours'));
										$lead[] = $rowLead;
									}
									$queryAccount = "select accounts.* , accounts_cstm.*,email_addresses.email_address,aos_products.name as proName,users.user_name  from accounts inner join accounts_cstm on accounts.id = accounts_cstm.id_c left join email_addr_bean_rel on accounts.id = email_addr_bean_rel.bean_id and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'  left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id left join aos_products_accounts_1_c on accounts.id = aos_products_accounts_1_c.aos_products_accounts_1accounts_idb left join aos_products on aos_products_accounts_1_c.aos_products_accounts_1aos_products_ida = aos_products.id inner join users on accounts.assigned_user_id = users.idwhere accounts.assigned_user_id = '".$row['id']."' and accounts.deleted ='0'".$accountWhere;
									$resultAccount=$db->query($queryAccount);
									while($rowAccount =$db->fetchByAssoc($resultAccount)){
										$amountQuery = "SELECT SUM(o.amount) AS amo
										FROM opportunities o INNER JOIN accounts_opportunities ao ON o.id = ao.opportunity_id AND ao.deleted = 0
										WHERE ao.account_id='".$rowAccount['id']."' AND o.deleted = 0
										AND o.sales_stage = 'Closed Won' ".$oppWhere;
										$resultAmount=$db->query($amountQuery);
										$rowAmount =$db->fetchByAssoc($resultAmount);
										$rowAccount["amount"] = $rowAmount["amo"];
										$rowAccount["date_entered"] = date('d/m/Y H:i:s', strtotime($rowAccount["date_entered"].'+ 7 hours'));
										$account[] = $rowAccount;
									}
									$queryMeeting = "select meetings.*,leads.last_name,leads.first_name,leads.phone_mobile,accounts.name as accName,accounts.phone_alternate,email_addresses.* , users.user_name from meetings left join leads on meetings.parent_id = leads.id left join accounts on meetings.parent_id = accounts.id inner join users on users.id = meetings.assigned_user_id left join email_addr_bean_rel on (leads.id = email_addr_bean_rel.bean_id OR accounts.id = email_addr_bean_rel.bean_id) and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'   left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id   where meetings.assigned_user_id = '".$row["id"]."' and meetings.deleted ='0'".$meetingWhere;
									$resultMeeting=$db->query($queryMeeting);
									while($rowMeeting =$db->fetchByAssoc($resultMeeting)){
										$rowMeeting["date_start"] = date('d/m/Y H:i:s', strtotime($rowMeeting["date_start"].'+ 7 hours'));
										$meeting[] = $rowMeeting;
									}
									$queryCall = "select calls.*,leads.last_name,leads.first_name,leads.phone_mobile,accounts.name as accName,accounts.phone_alternate,email_addresses.*,users.user_name  from calls left join leads on calls.parent_id = leads.id left join accounts on calls.parent_id = accounts.id inner join users on users.id = calls.assigned_user_id left join email_addr_bean_rel on (leads.id = email_addr_bean_rel.bean_id OR accounts.id = email_addr_bean_rel.bean_id) and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'   left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id where calls.assigned_user_id = '".$row["id"]."' and calls.deleted ='0'".$callWhere;
									$resultCall=$db->query($queryCall);
									while($rowCall =$db->fetchByAssoc($resultCall)){
										$rowCall["date_start"] = date('d/m/Y H:i:s', strtotime($rowCall["date_start"].'+ 7 hours'));
										$call[] = $rowCall;
									}
									$queryOpp = "select opportunities.*, accounts.name as accName,accounts.phone_alternate,email_addresses.email_address,users.user_name ,aos_products.name as proName,accounts.name as accName from opportunities left join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id left join accounts on accounts_opportunities.account_id = accounts.id left join email_addr_bean_rel on accounts.id = email_addr_bean_rel.bean_id and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'  left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id  left join aos_products_opportunities_1_c on opportunities.id = aos_products_opportunities_1_c.aos_products_opportunities_1opportunities_idb left join aos_products on aos_products_opportunities_1_c.aos_products_opportunities_1aos_products_ida = aos_products.id inner join users on opportunities.assigned_user_id = users.id where opportunities.assigned_user_id = '".$row["id"]."' and opportunities.deleted ='0'".$oppWhere;
									$resultOpp=$db->query($queryOpp);
									while($rowOpp =$db->fetchByAssoc($resultOpp)){
										$rowOpp["date_closed"] = date('d/m/Y H:i:s', strtotime($rowOpp["date_closed"].'+ 7 hours'));
										$opp[] = $rowOpp;
									}
									if($employee == true){
										if($_POST["users"][$i] == $_SESSION['authenticated_user_id']){	
											$user=array(
												"user"=>$row['name'],
												"Call"=>$call,
												"Meeting"=>$meeting,
												"Lead"=>$lead,
												"Account"=>$account,
												"Opp"=>$opp,
											);
											if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
												$group[$row["Se"]]=array();
												$group[$row["Se"]][]=$user;
											}else{
												$group[$row["Se"]][]=$user;
											}
										}
									}else{
										$user=array(
												"user"=>$row['name'],
												"Call"=>$call,
												"Meeting"=>$meeting,
												"Lead"=>$lead,
												"Account"=>$account,
												"Opp"=>$opp,
											);
											if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
												$group[$row["Se"]]=array();
												$group[$row["Se"]][]=$user;
											}else{
												$group[$row["Se"]][]=$user;
											}
										}
									}
								}
							} 
						}else{
								for($i=0;$i<count($_POST["titleUsers"]);$i++){
									if($employer == false || $employee == true || $leader == true){
										if($leader == true){
											$q="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
											inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
											users.status='Active' and users_cstm.provincecode_c='".$k."' and securitygroups_users.securitygroup_id In (".$whr.") and users.title = '".$_POST["titleUsers"][$i]."'";
										}
										if($employee == true){
											$q="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
											inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
											users.status='Active' and users_cstm.provincecode_c='".$k."' and users.id =  '".$_SESSION['authenticated_user_id']."' and users.title = '".$_POST["titleUsers"][$i]."'";
										}
									}else{
										if($employer == true){
											$q="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
											inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
											users.status='Active' and users_cstm.provincecode_c='".$k."' and users.title = '".$_POST["titleUsers"][$i]."'";
										}
									}
								
								$r=$db->query($q);
								while($rww=$db->fetchByAssoc($r)){
									if(!empty($rww)){
										if($rww["seName"] == $rww["Se"]){
											$p=$k;
											$qr="select users_cstm.provincecode_c as province, users.user_name as name,users.department as Se from users inner join securitygroups_users on users.id = securitygroups_users.user_id inner join users_cstm on users.id = users_cstm.id_c where users.deleted='0' and users.status='Active' and  users.id='".$rww["id"]."' and securitygroups_users.securitygroup_id In (".$whr.")";	
											$res=$db->query($qr);
											$row=$db->fetchByAssoc($res);
											if(!empty($row)){
												if($row["province"] == $k){
													$queryLead = "select leads.* , leads_cstm.*,users.user_name,aos_products.name as proName,email_addresses.* from leads inner join leads_cstm on leads.id = leads_cstm.id_c left join aos_products_leads_1_c on leads.id = aos_products_leads_1_c.aos_products_leads_1leads_idb left join aos_products on aos_products_leads_1_c.aos_products_leads_1aos_products_ida = aos_products.id inner join users on leads.assigned_user_id = users.id left join email_addr_bean_rel on leads.id = email_addr_bean_rel.bean_id and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'  left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id where leads.assigned_user_id = '".$rww["id"]."' and leads.deleted ='0' and leads.status NOT IN ('Converted','Transaction') ".$leadWhere;
													$resultLead=$db->query($queryLead);
													while($rowLead =$db->fetchByAssoc($resultLead)){
														$rowLead["date_entered"] = date('d/m/Y H:i:s', strtotime($rowLead["date_entered"].'+ 7 hours'));
														$lead[] = $rowLead;
													}
													$queryAccount = "select accounts.* , accounts_cstm.*,email_addresses.email_address,aos_products.name as proName,users.user_name  from accounts inner join accounts_cstm on accounts.id = accounts_cstm.id_c left join email_addr_bean_rel on accounts.id = email_addr_bean_rel.bean_id and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'  left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id left join aos_products_accounts_1_c on accounts.id = aos_products_accounts_1_c.aos_products_accounts_1accounts_idb left join aos_products on aos_products_accounts_1_c.aos_products_accounts_1aos_products_ida = aos_products.id inner join users on accounts.assigned_user_id = users.idwhere accounts.assigned_user_id = '".$rww['id']."' and accounts.deleted ='0'".$accountWhere;
													$resultAccount=$db->query($queryAccount);
													while($rowAccount =$db->fetchByAssoc($resultAccount)){
														$amountQuery = "SELECT SUM(o.amount) AS amo
														FROM opportunities o INNER JOIN accounts_opportunities ao ON o.id = ao.opportunity_id AND ao.deleted = 0
														WHERE ao.account_id='".$rowAccount['id']."' AND o.deleted = 0
														AND o.sales_stage = 'Closed Won' ".$oppWhere;
														$resultAmount=$db->query($amountQuery);
														$rowAmount =$db->fetchByAssoc($resultAmount);
														$rowAccount["amount"] = $rowAmount["amo"];
														$rowAccount["date_entered"] = date('d/m/Y H:i:s', strtotime($rowAccount["date_entered"].'+ 7 hours'));
														$account[] = $rowAccount;
													}
													$queryMeeting = "select meetings.*,leads.last_name,leads.first_name,leads.phone_mobile,accounts.name as accName,accounts.phone_alternate,email_addresses.* , users.user_name from meetings left join leads on meetings.parent_id = leads.id left join accounts on meetings.parent_id = accounts.id inner join users on users.id = meetings.assigned_user_id left join email_addr_bean_rel on (leads.id = email_addr_bean_rel.bean_id OR accounts.id = email_addr_bean_rel.bean_id) and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'   left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id   where meetings.assigned_user_id = '".$rww["id"]."' and meetings.deleted ='0'".$meetingWhere;
													$resultMeeting=$db->query($queryMeeting);
													while($rowMeeting =$db->fetchByAssoc($resultMeeting)){
														$rowMeeting["date_start"] = date('d/m/Y H:i:s', strtotime($rowMeeting["date_start"].'+ 7 hours'));
														$meeting[] = $rowMeeting;
													}
													$queryCall = "select calls.*,leads.last_name,leads.first_name,leads.phone_mobile,accounts.name as accName,accounts.phone_alternate,email_addresses.*,users.user_name  from calls left join leads on calls.parent_id = leads.id left join accounts on calls.parent_id = accounts.id inner join users on users.id = calls.assigned_user_id left join email_addr_bean_rel on (leads.id = email_addr_bean_rel.bean_id OR accounts.id = email_addr_bean_rel.bean_id) and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'   left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id where calls.assigned_user_id = '".$rww["id"]."' and calls.deleted ='0'".$callWhere;
													$resultCall=$db->query($queryCall);
													while($rowCall =$db->fetchByAssoc($resultCall)){
														$rowCall["date_start"] = date('d/m/Y H:i:s', strtotime($rowCall["date_start"].'+ 7 hours'));
														$call[] = $rowCall;
													}
													$queryOpp = "select opportunities.*, accounts.name as accName,accounts.phone_alternate,email_addresses.email_address,users.user_name ,aos_products.name as proName,accounts.name as accName from opportunities left join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id left join accounts on accounts_opportunities.account_id = accounts.id left join email_addr_bean_rel on accounts.id = email_addr_bean_rel.bean_id and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'  left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id  left join aos_products_opportunities_1_c on opportunities.id = aos_products_opportunities_1_c.aos_products_opportunities_1opportunities_idb left join aos_products on aos_products_opportunities_1_c.aos_products_opportunities_1aos_products_ida = aos_products.id inner join users on opportunities.assigned_user_id = users.id where opportunities.assigned_user_id = '".$rww["id"]."' and opportunities.deleted ='0'".$oppWhere;
													$resultOpp=$db->query($queryOpp);
													while($rowOpp =$db->fetchByAssoc($resultOpp)){
														$rowOpp["date_closed"] = date('d/m/Y H:i:s', strtotime($rowOpp["date_closed"].'+ 7 hours'));
														$opp[] = $rowOpp;
													}
													if($employee == true){
														if($rww["id"] == $_SESSION['authenticated_user_id'])
														{	
															$user = array(
																		"user"=>$row['name'],
																		"Call"=>$call,
																		"Meeting"=>$meeting,
																		"Lead"=>$lead,
																		"Account"=>$account,
																		"Opp"=>$opp,
																	);
															if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
																$group[$row["Se"]]=array();
																$group[$row["Se"]][]=$user;
															}else{
																$group[$row["Se"]][]=$user;
															}
														}
													}else
													{
														$user = array(
																	"user"=>$row['name'],
																	"Call"=>$call,
																	"Meeting"=>$meeting,
																	"Lead"=>$lead,
																	"Account"=>$account,
																	"Opp"=>$opp,
																);
														if($group[$row["Se"]]=='' || $group[$row["Se"]] == null)
														{
															$group[$row["Se"]]=array();
															$group[$row["Se"]][]=$user;
														}else{
															$group[$row["Se"]][]=$user;
														}
													}
												}
											}
										}
									}
								}
							}
						}
						
					}
					else
					{
						for($i=0;$i<count($_POST["users"]);$i++){
							if($employer == false || $employee == true || $leader == true){
								if($leader == true){
									$qr="select users_cstm.provincecode_c as province, users.user_name as name,users.department as Se from users inner join securitygroups_users on users.id = securitygroups_users.user_id inner join users_cstm on users.id = users_cstm.id_c where users.deleted='0' and users.status='Active' and  users.id='".$_POST["users"][$i]."' and securitygroups_users.securitygroup_id In (".$whr.")";	
								}
								if($employee == true){
									$qr="select users_cstm.provincecode_c as province, users.user_name as name,users.department as Se from users inner join securitygroups_users on users.id = securitygroups_users.user_id inner join users_cstm on users.id = users_cstm.id_c where users.deleted='0' and users.status='Active' and  users.id='".$_POST["users"][$i]."' and users.id =  '".$_SESSION['authenticated_user_id']."'";	
								}
								
							}else{
								if($employer == true){
									$qr="select users_cstm.provincecode_c as province, users.user_name as name,users.department as Se from users inner join securitygroups_users on users.id = securitygroups_users.user_id inner join users_cstm on users.id = users_cstm.id_c where users.deleted='0' and users.status='Active' and  users.id='".$_POST["users"][$i]."'";	
								}
								
							}
							
							$res=$db->query($qr);
							$row=$db->fetchByAssoc($res);
								if(!empty($row)){
									if($row["province"] == $k){
									$p=$k;
									$queryLead = "select leads.* , leads_cstm.*,users.user_name,aos_products.name as proName,email_addresses.* from leads inner join leads_cstm on leads.id = leads_cstm.id_c left join aos_products_leads_1_c on leads.id = aos_products_leads_1_c.aos_products_leads_1leads_idb left join aos_products on aos_products_leads_1_c.aos_products_leads_1aos_products_ida = aos_products.id inner join users on leads.assigned_user_id = users.id left join email_addr_bean_rel on leads.id = email_addr_bean_rel.bean_id and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'  left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id where leads.assigned_user_id = '".$_POST["users"][$i]."' and leads.deleted ='0' and leads.status NOT IN ('Converted','Transaction') ".$leadWhere;
									$resultLead=$db->query($queryLead);
									while($rowLead =$db->fetchByAssoc($resultLead)){
										$rowLead["date_entered"] = date('d/m/Y H:i:s', strtotime($rowLead["date_entered"].'+ 7 hours'));
										$lead[] = $rowLead;
									}
									$queryAccount = "select accounts.* , accounts_cstm.*,email_addresses.email_address,aos_products.name as proName,users.user_name  from accounts inner join accounts_cstm on accounts.id = accounts_cstm.id_c left join email_addr_bean_rel on accounts.id = email_addr_bean_rel.bean_id and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'  left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id left join aos_products_accounts_1_c on accounts.id = aos_products_accounts_1_c.aos_products_accounts_1accounts_idb left join aos_products on aos_products_accounts_1_c.aos_products_accounts_1aos_products_ida = aos_products.id inner join users on accounts.assigned_user_id = users.idwhere accounts.assigned_user_id = '".$_POST["users"][$i]."' and accounts.deleted ='0'".$accountWhere;
									$resultAccount=$db->query($queryAccount);
									while($rowAccount =$db->fetchByAssoc($resultAccount)){
										$amountQuery = "SELECT SUM(o.amount) AS amo
										FROM opportunities o INNER JOIN accounts_opportunities ao ON o.id = ao.opportunity_id AND ao.deleted = 0
										WHERE ao.account_id='".$rowAccount['id']."' AND o.deleted = 0
										AND o.sales_stage = 'Closed Won' ".$oppWhere;
										$resultAmount=$db->query($amountQuery);
										$rowAmount =$db->fetchByAssoc($resultAmount);
										$rowAccount["amount"] = $rowAmount["amo"];
										$rowAccount["date_entered"] = date('d/m/Y H:i:s', strtotime($rowAccount["date_entered"].'+ 7 hours'));
										$account[] = $rowAccount;
									}
									$queryMeeting = "select meetings.*,leads.last_name,leads.first_name,leads.phone_mobile,accounts.name as accName,accounts.phone_alternate,email_addresses.* , users.user_name from meetings left join leads on meetings.parent_id = leads.id left join accounts on meetings.parent_id = accounts.id inner join users on users.id = meetings.assigned_user_id left join email_addr_bean_rel on (leads.id = email_addr_bean_rel.bean_id OR accounts.id = email_addr_bean_rel.bean_id) and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'   left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id   where meetings.assigned_user_id = '".$_POST["users"][$i]."' and meetings.deleted ='0'".$meetingWhere;
									$resultMeeting=$db->query($queryMeeting);
									while($rowMeeting =$db->fetchByAssoc($resultMeeting)){
										$rowMeeting["date_start"] = date('d/m/Y H:i:s', strtotime($rowMeeting["date_start"].'+ 7 hours'));
										$meeting[] = $rowMeeting;
									}
									$queryCall = "select calls.*,leads.last_name,leads.first_name,leads.phone_mobile,accounts.name as accName,accounts.phone_alternate,email_addresses.*,users.user_name  from calls left join leads on calls.parent_id = leads.id left join accounts on calls.parent_id = accounts.id inner join users on users.id = calls.assigned_user_id left join email_addr_bean_rel on (leads.id = email_addr_bean_rel.bean_id OR accounts.id = email_addr_bean_rel.bean_id) and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'   left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id where calls.assigned_user_id = '".$_POST["users"][$i]."' and calls.deleted ='0'".$callWhere;
									$resultCall=$db->query($queryCall);
									while($rowCall =$db->fetchByAssoc($resultCall)){
										$rowCall["date_start"] = date('d/m/Y H:i:s', strtotime($rowCall["date_start"].'+ 7 hours'));
										$call[] = $rowCall;
									}
									$queryOpp = "select opportunities.*, accounts.name as accName,accounts.phone_alternate,email_addresses.email_address,users.user_name ,aos_products.name as proName,accounts.name as accName from opportunities left join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id left join accounts on accounts_opportunities.account_id = accounts.id left join email_addr_bean_rel on accounts.id = email_addr_bean_rel.bean_id and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'  left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id  left join aos_products_opportunities_1_c on opportunities.id = aos_products_opportunities_1_c.aos_products_opportunities_1opportunities_idb left join aos_products on aos_products_opportunities_1_c.aos_products_opportunities_1aos_products_ida = aos_products.id inner join users on opportunities.assigned_user_id = users.id where opportunities.assigned_user_id = '".$_POST["users"][$i]."' and opportunities.deleted ='0'".$oppWhere;
									$resultOpp=$db->query($queryOpp);
									while($rowOpp =$db->fetchByAssoc($resultOpp)){
										$rowOpp["date_closed"] = date('d/m/Y H:i:s', strtotime($rowOpp["date_closed"].'+ 7 hours'));
										$opp[] = $rowOpp;
									}
										if($employee == true){
											if($_POST["users"][$i] == $_SESSION['authenticated_user_id']){	
												$user = array(
															"user"=>$row['name'],
															"Call"=>$call,
															"Meeting"=>$meeting,
															"Lead"=>$lead,
															"Account"=>$account,
															"Opp"=>$opp,
														);
												if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
													$group[$row["Se"]]=array();
													$group[$row["Se"]][]=$user;
												}else{
													$group[$row["Se"]][]=$user;
												}
											}
										}else{
											$user = array(
														"user"=>$row['name'],
														"Call"=>$call,
														"Meeting"=>$meeting,
														"Lead"=>$lead,
														"Account"=>$account,
														"Opp"=>$opp,
													);
											if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
												$group[$row["Se"]]=array();
												$group[$row["Se"]][]=$user;
											}else{
												$group[$row["Se"]][]=$user;
											}
										}
									}
								}
							}
						}
					}
					else
					{
						if($_POST["users"] == null || $_POST["users"] == ''){
							if($_POST["titleUsers"] == null || $_POST["titleUsers"] == ''){
							$grs = "";
							for($j=0;$j<count($_POST["groupUsers"]);$j++){
							$grs.="'".$_POST["groupUsers"][$j]."'";
							if($j<count($_POST["groupUsers"])-1){
								$grs.=",";	
							}
							if($_POST["groupUsers"][$j] == "All"){
								$grs="All";
								break;
							}
						}
						if($grs != "All"){
							if($employer == false || $employee == true || $leader == true){
								if($leader == true){
									$qr="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
									inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
									users.status='Active' and users_cstm.provincecode_c='".$k."' and securitygroups_users.securitygroup_id In (".$grs.") and securitygroups_users.securitygroup_id In (".$whr.")";
								}
								if($employee == true){
									$qr="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
									inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
									users.status='Active' and users_cstm.provincecode_c='".$k."' and securitygroups_users.securitygroup_id In (".$grs.") and users.id =  '".$_SESSION['authenticated_user_id']."'";
								}
							}else{
								if($employer == true){
									$qr="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
									inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
									users.status='Active' and users_cstm.provincecode_c='".$k."' and securitygroups_users.securitygroup_id In (".$grs.")";	
								}							
							}								
						}else{
							if($employer == false || $employee == true || $leader == true){
								if($leader == true){
									$qr="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
									inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
									users.status='Active' and users_cstm.provincecode_c='".$k."' and securitygroups_users.securitygroup_id In (".$whr.")";	
								}
								if($employee == true){
									$qr="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
									inner join users_cstm on users_cstm.id_c=users.id inner join 			securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
									users.status='Active' and users_cstm.provincecode_c='".$k."' and users.id =  '".$_SESSION['authenticated_user_id']."'";	
								}
							}else{
								if($employer == true){
									$qr="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
									inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
									users.status='Active' and users_cstm.provincecode_c='".$k."'";	
								}
								
							}
						}
						$res=$db->query($qr);
						$row=$db->fetchByAssoc($res);
						if(!empty($row)){
							
							do{
								if($row["seName"]==$row["Se"]){
									$p = $k;
									$queryLead = "select leads.* , leads_cstm.*,users.user_name,aos_products.name as proName,email_addresses.* from leads inner join leads_cstm on leads.id = leads_cstm.id_c left join aos_products_leads_1_c on leads.id = aos_products_leads_1_c.aos_products_leads_1leads_idb left join aos_products on aos_products_leads_1_c.aos_products_leads_1aos_products_ida = aos_products.id inner join users on leads.assigned_user_id = users.id left join email_addr_bean_rel on leads.id = email_addr_bean_rel.bean_id and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'  left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id where leads.assigned_user_id = '".$row['id']."' and leads.deleted ='0' and leads.status NOT IN ('Converted','Transaction') ".$leadWhere;
									$resultLead=$db->query($queryLead);
									while($rowLead =$db->fetchByAssoc($resultLead)){
										$rowLead["date_entered"] = date('d/m/Y H:i:s', strtotime($rowLead["date_entered"].'+ 7 hours'));
										$lead[] = $rowLead;
									}
									$queryAccount = "select accounts.* , accounts_cstm.*,email_addresses.email_address,aos_products.name as proName,users.user_name  from accounts inner join accounts_cstm on accounts.id = accounts_cstm.id_c left join email_addr_bean_rel on accounts.id = email_addr_bean_rel.bean_id and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'  left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id left join aos_products_accounts_1_c on accounts.id = aos_products_accounts_1_c.aos_products_accounts_1accounts_idb left join aos_products on aos_products_accounts_1_c.aos_products_accounts_1aos_products_ida = aos_products.id inner join users on accounts.assigned_user_id = users.idwhere accounts.assigned_user_id = '".$row["id"]."' and accounts.deleted ='0'".$accountWhere;
									$resultAccount=$db->query($queryAccount);
									while($rowAccount =$db->fetchByAssoc($resultAccount)){
										$amountQuery = "SELECT SUM(o.amount) AS amo
										FROM opportunities o INNER JOIN accounts_opportunities ao ON o.id = ao.opportunity_id AND ao.deleted = 0
										WHERE ao.account_id='".$rowAccount['id']."' AND o.deleted = 0
										AND o.sales_stage = 'Closed Won' ".$oppWhere;
										$resultAmount=$db->query($amountQuery);
										$rowAmount =$db->fetchByAssoc($resultAmount);
										$rowAccount["amount"] = $rowAmount["amo"];
										$rowAccount["date_entered"] = date('d/m/Y H:i:s', strtotime($rowAccount["date_entered"].'+ 7 hours'));
										$account[] = $rowAccount;
									}
									$queryMeeting = "select meetings.*,leads.last_name,leads.first_name,leads.phone_mobile,accounts.name as accName,accounts.phone_alternate,email_addresses.* , users.user_name from meetings left join leads on meetings.parent_id = leads.id left join accounts on meetings.parent_id = accounts.id inner join users on users.id = meetings.assigned_user_id left join email_addr_bean_rel on (leads.id = email_addr_bean_rel.bean_id OR accounts.id = email_addr_bean_rel.bean_id) and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'   left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id   where meetings.assigned_user_id = '".$row["id"]."' and meetings.deleted ='0'".$meetingWhere;
									$resultMeeting=$db->query($queryMeeting);
									while($rowMeeting =$db->fetchByAssoc($resultMeeting)){
										$rowMeeting["date_start"] = date('d/m/Y H:i:s', strtotime($rowMeeting["date_start"].'+ 7 hours'));
										$meeting[] = $rowMeeting;
									}
									$queryCall = "select calls.*,leads.last_name,leads.first_name,leads.phone_mobile,accounts.name as accName,accounts.phone_alternate,email_addresses.*,users.user_name  from calls left join leads on calls.parent_id = leads.id left join accounts on calls.parent_id = accounts.id inner join users on users.id = calls.assigned_user_id left join email_addr_bean_rel on (leads.id = email_addr_bean_rel.bean_id OR accounts.id = email_addr_bean_rel.bean_id) and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'   left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id where calls.assigned_user_id = '".$row["id"]."' and calls.deleted ='0'".$callWhere;
									$resultCall=$db->query($queryCall);
									while($rowCall =$db->fetchByAssoc($resultCall)){
										$rowCall["date_start"] = date('d/m/Y H:i:s', strtotime($rowCall["date_start"].'+ 7 hours'));
										$call[] = $rowCall;
									}
									$queryOpp = "select opportunities.*, accounts.name as accName,accounts.phone_alternate,email_addresses.email_address,users.user_name ,aos_products.name as proName,accounts.name as accName from opportunities left join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id left join accounts on accounts_opportunities.account_id = accounts.id left join email_addr_bean_rel on accounts.id = email_addr_bean_rel.bean_id and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'  left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id  left join aos_products_opportunities_1_c on opportunities.id = aos_products_opportunities_1_c.aos_products_opportunities_1opportunities_idb left join aos_products on aos_products_opportunities_1_c.aos_products_opportunities_1aos_products_ida = aos_products.id inner join users on opportunities.assigned_user_id = users.id where opportunities.assigned_user_id = '".$row["id"]."' and opportunities.deleted ='0'".$oppWhere;
									$resultOpp=$db->query($queryOpp);
									while($rowOpp =$db->fetchByAssoc($resultOpp)){
										$rowOpp["date_closed"] = date('d/m/Y H:i:s', strtotime($rowOpp["date_closed"].'+ 7 hours'));
										$opp[] = $rowOpp;
									}
									if($employee == true){
											if($row['id'] == $_SESSION['authenticated_user_id']){	
												$user = array(
															"user"=>$row['name'],
															"Call"=>$call,
															"Meeting"=>$meeting,
															"Lead"=>$lead,
															"Account"=>$account,
															"Opp"=>$opp,
														);
												if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
														$group[$row["Se"]]=array();
														$group[$row["Se"]][]=$user;
												}
												else{
														$group[$row["Se"]][]=$user;
												}
										}
									}else{
										$user = array(
													"user"=>$row['name'],
													"Call"=>$call,
													"Meeting"=>$meeting,
													"Lead"=>$lead,
													"Account"=>$account,
													"Opp"=>$opp,
											    );
										if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
												$group[$row["Se"]]=array();
												$group[$row["Se"]][]=$user;
										}
										else{
												$group[$row["Se"]][]=$user;
										}
									}
								}
							}while($row=$db->fetchByAssoc($res));
						}
						}else{
							for($j=0;$j<count($_POST["groupUsers"]);$j++){
							$grs.="'".$_POST["groupUsers"][$j]."'";
								if($j<count($_POST["groupUsers"])-1){
									$grs.=",";	
								}
								if($_POST["groupUsers"][$j] == "All"){
									$grs="All";
									break;
								}
							}
						for($i=0;$i<count($_POST["titleUsers"]);$i++){
							if($grs != "All"){
								if($employer == false || $employee == true || $leader == true){
									if($leader == true){
										$q="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
										inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
										users.status='Active' and users_cstm.provincecode_c='".$k."' and securitygroups_users.securitygroup_id In (".$grs.") and securitygroups_users.securitygroup_id In (".$whr.") and users.title = '".$_POST["titleUsers"][$i]."'";
									}
									if($employee == true){
										$q="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
										inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
										users.status='Active' and users_cstm.provincecode_c='".$k."' and securitygroups_users.securitygroup_id In (".$grs.") and users.id =  '".$_SESSION['authenticated_user_id']."' and users.title = '".$_POST["titleUsers"][$i]."'";
									}
								}else{
									if($employer == true){
										$q="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
										inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
										users.status='Active' and users_cstm.provincecode_c='".$k."' and securitygroups_users.securitygroup_id In (".$grs.") and users.title = '".$_POST["titleUsers"][$i]."'";
									}
								
								}
							
							}else{
								if($employer == false || $employee == true || $leader == true){
									if($leader == true){
										$q="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
										inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
										users.status='Active' and users_cstm.provincecode_c='".$k."' and securitygroups_users.securitygroup_id In (".$whr.") and users.title = '".$_POST["titleUsers"][$i]."'";
									}
									if($employee == true){
										$q="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
										inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
										users.status='Active' and users_cstm.provincecode_c='".$k."' and users.id =  '".$_SESSION['authenticated_user_id']."' and users.title = '".$_POST["titleUsers"][$i]."'";
									}
								}else{
									if($employer == true){
										$q="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
										inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
										users.status='Active' and users_cstm.provincecode_c='".$k."'  and users.title = '".$_POST["titleUsers"][$i]."'";
									}
									
								}
								
							}
								$r=$db->query($q);
								while($rww=$db->fetchByAssoc($r)){
									if(!empty($rww)){
										if($rww["seName"]==$rww["Se"]){
											$p=$k;
											$qr="select users_cstm.provincecode_c as province, users.user_name as name,users.department as Se from users inner join securitygroups_users on users.id = securitygroups_users.user_id inner join users_cstm on users.id = users_cstm.id_c where users.deleted='0' and users.status='Active' 
											and  users.id='".$rww["id"]."' and securitygroups_users.securitygroup_id In (".$whr.")";	
											$res=$db->query($qr);
											$row=$db->fetchByAssoc($res);
											if(!empty($row)){
												if($row["province"] == $k){
												$p=$k;
												$queryLead = "select leads.* , leads_cstm.*,users.user_name,aos_products.name as proName,email_addresses.* from leads inner join leads_cstm on leads.id = leads_cstm.id_c left join aos_products_leads_1_c on leads.id = aos_products_leads_1_c.aos_products_leads_1leads_idb left join aos_products on aos_products_leads_1_c.aos_products_leads_1aos_products_ida = aos_products.id inner join users on leads.assigned_user_id = users.id left join email_addr_bean_rel on leads.id = email_addr_bean_rel.bean_id and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'  left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id where leads.assigned_user_id = '".$rww["id"]."' and leads.deleted ='0' and leads.status NOT IN ('Converted','Transaction') ".$leadWhere;
												$resultLead=$db->query($queryLead);
												while($rowLead =$db->fetchByAssoc($resultLead)){
													$rowLead["date_entered"] = date('d/m/Y H:i:s', strtotime($rowLead["date_entered"].'+ 7 hours'));
													$lead[] = $rowLead;
												}
												$queryAccount = "select accounts.* , accounts_cstm.*,email_addresses.email_address,aos_products.name as proName,users.user_name  from accounts inner join accounts_cstm on accounts.id = accounts_cstm.id_c left join email_addr_bean_rel on accounts.id = email_addr_bean_rel.bean_id and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'  left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id left join aos_products_accounts_1_c on accounts.id = aos_products_accounts_1_c.aos_products_accounts_1accounts_idb left join aos_products on aos_products_accounts_1_c.aos_products_accounts_1aos_products_ida = aos_products.id inner join users on accounts.assigned_user_id = users.idwhere accounts.assigned_user_id = '".$rww["id"]."' and accounts.deleted ='0'".$accountWhere;
												$resultAccount=$db->query($queryAccount);
												while($rowAccount =$db->fetchByAssoc($resultAccount)){
													$amountQuery = "SELECT SUM(o.amount) AS amo
													FROM opportunities o INNER JOIN accounts_opportunities ao ON o.id = ao.opportunity_id AND ao.deleted = 0
													WHERE ao.account_id='".$rowAccount['id']."' AND o.deleted = 0
													AND o.sales_stage = 'Closed Won' ".$oppWhere;
													$resultAmount=$db->query($amountQuery);
													$rowAmount =$db->fetchByAssoc($resultAmount);
													$rowAccount["amount"] = $rowAmount["amo"];
													$rowAccount["date_entered"] = date('d/m/Y H:i:s', strtotime($rowAccount["date_entered"].'+ 7 hours'));
													$account[] = $rowAccount;
												}
												$queryMeeting = "select meetings.*,leads.last_name,leads.first_name,leads.phone_mobile,accounts.name as accName,accounts.phone_alternate,email_addresses.* , users.user_name from meetings left join leads on meetings.parent_id = leads.id left join accounts on meetings.parent_id = accounts.id inner join users on users.id = meetings.assigned_user_id left join email_addr_bean_rel on (leads.id = email_addr_bean_rel.bean_id OR accounts.id = email_addr_bean_rel.bean_id) and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'   left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id   where meetings.assigned_user_id = '".$rww["id"]."' and meetings.deleted ='0'".$meetingWhere;
												$resultMeeting=$db->query($queryMeeting);
												while($rowMeeting =$db->fetchByAssoc($resultMeeting)){
													$rowMeeting["date_start"] = date('d/m/Y H:i:s', strtotime($rowMeeting["date_start"].'+ 7 hours'));
													$meeting[] = $rowMeeting;
												}
												$queryCall = "select calls.*,leads.last_name,leads.first_name,leads.phone_mobile,accounts.name as accName,accounts.phone_alternate,email_addresses.*,users.user_name  from calls left join leads on calls.parent_id = leads.id left join accounts on calls.parent_id = accounts.id inner join users on users.id = calls.assigned_user_id left join email_addr_bean_rel on (leads.id = email_addr_bean_rel.bean_id OR accounts.id = email_addr_bean_rel.bean_id) and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'   left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id where calls.assigned_user_id = '".$rww["id"]."' and calls.deleted ='0'".$callWhere;
												$resultCall=$db->query($queryCall);
												while($rowCall =$db->fetchByAssoc($resultCall)){
													$rowCall["date_start"] = date('d/m/Y H:i:s', strtotime($rowCall["date_start"].'+ 7 hours'));
													$call[] = $rowCall;
												}
												$queryOpp = "select opportunities.*, accounts.name as accName,accounts.phone_alternate,email_addresses.email_address,users.user_name ,aos_products.name as proName,accounts.name as accName from opportunities left join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id left join accounts on accounts_opportunities.account_id = accounts.id left join email_addr_bean_rel on accounts.id = email_addr_bean_rel.bean_id and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'  left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id  left join aos_products_opportunities_1_c on opportunities.id = aos_products_opportunities_1_c.aos_products_opportunities_1opportunities_idb left join aos_products on aos_products_opportunities_1_c.aos_products_opportunities_1aos_products_ida = aos_products.id inner join users on opportunities.assigned_user_id = users.id where opportunities.assigned_user_id = '".$rww["id"]."' and opportunities.deleted ='0'".$oppWhere;
												$resultOpp=$db->query($queryOpp);
												while($rowOpp =$db->fetchByAssoc($resultOpp)){
													$rowOpp["date_closed"] = date('d/m/Y H:i:s', strtotime($rowOpp["date_closed"].'+ 7 hours'));
													$opp[] = $rowOpp;
												}
													if($employee == true){
														if($rww['id'] == $_SESSION['authenticated_user_id']){	
															$user = array(
																		"user"=>$row['name'],
																		"Call"=>$call,
																		"Meeting"=>$meeting,
																		"Lead"=>$lead,
																		"Account"=>$account,
																		"Opp"=>$opp,
																	);
															if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
																$group[$row["Se"]]=array();
																$group[$row["Se"]][]=$user;
															}else{
																$group[$row["Se"]][]=$user;
															}
														}
													}else{
														$user = array(
																	"user"=>$row['name'],
																	"Call"=>$call,
																	"Meeting"=>$meeting,
																	"Lead"=>$lead,
																	"Account"=>$account,
																	"Opp"=>$opp,
																);
															if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
																$group[$row["Se"]]=array();
																$group[$row["Se"]][]=$user;
															}else{
																$group[$row["Se"]][]=$user;
															}
													}
												}
											}
										}
									}
								}
							}
						}
					}
					else
					{
						for($i=0;$i<count($_POST["users"]);$i++){
							if($employer == false || $employee == true || $leader == true){
								if($leader == true){
									$qr="select users_cstm.provincecode_c as province, users.user_name as name,users.department as Se from users inner join securitygroups_users on users.id = securitygroups_users.user_id inner join users_cstm on users.id = users_cstm.id_c where users.deleted='0' and users.status='Active' 
									and  users.id='".$_POST["users"][$i]."' and securitygroups_users.securitygroup_id In (".$whr.")";
								}
								if($employee == true){
									$qr="select users_cstm.provincecode_c as province, users.user_name as name,users.department as Se from users inner join securitygroups_users on users.id = securitygroups_users.user_id inner join users_cstm on users.id = users_cstm.id_c where users.deleted='0' and users.status='Active' 
									and  users.id='".$_POST["users"][$i]."' and users.id =  '".$_SESSION['authenticated_user_id']."'";
								}								
							}else{
								if($employer == true){
									$qr="select users_cstm.provincecode_c as province, users.user_name as name,users.department as Se from users inner join securitygroups_users on users.id = securitygroups_users.user_id inner join users_cstm on users.id = users_cstm.id_c where users.deleted='0' and users.status='Active' 
									and  users.id='".$_POST["users"][$i]."'";	
								}
								
							}
							$res=$db->query($qr);
							$row=$db->fetchByAssoc($res);
							if(!empty($row)){
								if($row["province"] == $k){
								$p=$k;
								$queryLead = "select leads.* , leads_cstm.*,users.user_name,aos_products.name as proName,email_addresses.* from leads inner join leads_cstm on leads.id = leads_cstm.id_c left join aos_products_leads_1_c on leads.id = aos_products_leads_1_c.aos_products_leads_1leads_idb left join aos_products on aos_products_leads_1_c.aos_products_leads_1aos_products_ida = aos_products.id inner join users on leads.assigned_user_id = users.id left join email_addr_bean_rel on leads.id = email_addr_bean_rel.bean_id and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'  left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id where leads.assigned_user_id = '".$_POST["users"][$i]."' and leads.deleted ='0' and leads.status NOT IN ('Converted','Transaction') ".$leadWhere;
								$resultLead=$db->query($queryLead);
								while($rowLead =$db->fetchByAssoc($resultLead)){
									$rowLead["date_entered"] = date('d/m/Y H:i:s', strtotime($rowLead["date_entered"].'+ 7 hours'));
									$lead[] = $rowLead;
								}
								$queryAccount = "select accounts.* , accounts_cstm.*,email_addresses.email_address,aos_products.name as proName,users.user_name  from accounts inner join accounts_cstm on accounts.id = accounts_cstm.id_c left join email_addr_bean_rel on accounts.id = email_addr_bean_rel.bean_id and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'  left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id left join aos_products_accounts_1_c on accounts.id = aos_products_accounts_1_c.aos_products_accounts_1accounts_idb left join aos_products on aos_products_accounts_1_c.aos_products_accounts_1aos_products_ida = aos_products.id inner join users on accounts.assigned_user_id = users.idwhere accounts.assigned_user_id = '".$_POST["users"][$i]."' and accounts.deleted ='0'".$accountWhere;
								$resultAccount=$db->query($queryAccount);
								while($rowAccount =$db->fetchByAssoc($resultAccount)){
									$amountQuery = "SELECT SUM(o.amount) AS amo
									FROM opportunities o INNER JOIN accounts_opportunities ao ON o.id = ao.opportunity_id AND ao.deleted = 0
									WHERE ao.account_id='".$rowAccount['id']."' AND o.deleted = 0
									AND o.sales_stage = 'Closed Won' ".$oppWhere;
									$resultAmount=$db->query($amountQuery);
									$rowAmount =$db->fetchByAssoc($resultAmount);
									$rowAccount["amount"] = $rowAmount["amo"];
									$rowAccount["date_entered"] = date('d/m/Y H:i:s', strtotime($rowAccount["date_entered"].'+ 7 hours'));
									$account[] = $rowAccount;
								}
								$queryMeeting = "select meetings.*,leads.last_name,leads.first_name,leads.phone_mobile,accounts.name as accName,accounts.phone_alternate,email_addresses.* , users.user_name from meetings left join leads on meetings.parent_id = leads.id left join accounts on meetings.parent_id = accounts.id inner join users on users.id = meetings.assigned_user_id left join email_addr_bean_rel on (leads.id = email_addr_bean_rel.bean_id OR accounts.id = email_addr_bean_rel.bean_id) and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'   left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id   where meetings.assigned_user_id = '".$_POST["users"][$i]."' and meetings.deleted ='0'".$meetingWhere;
								$resultMeeting=$db->query($queryMeeting);
								while($rowMeeting =$db->fetchByAssoc($resultMeeting)){
									$rowMeeting["date_start"] = date('d/m/Y H:i:s', strtotime($rowMeeting["date_start"].'+ 7 hours'));
									$meeting[] = $rowMeeting;
								}
								$queryCall = "select calls.*,leads.last_name,leads.first_name,leads.phone_mobile,accounts.name as accName,accounts.phone_alternate,email_addresses.*,users.user_name  from calls left join leads on calls.parent_id = leads.id left join accounts on calls.parent_id = accounts.id inner join users on users.id = calls.assigned_user_id left join email_addr_bean_rel on (leads.id = email_addr_bean_rel.bean_id OR accounts.id = email_addr_bean_rel.bean_id) and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'  left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id where calls.assigned_user_id = '".$_POST["users"][$i]."' and calls.deleted ='0'".$callWhere;
								$resultCall=$db->query($queryCall);
								while($rowCall =$db->fetchByAssoc($resultCall)){
									$rowCall["date_start"] = date('d/m/Y H:i:s', strtotime($rowCall["date_start"].'+ 7 hours'));
									$call[] = $rowCall;
								}
								$queryOpp = "select opportunities.*, accounts.name as accName,accounts.phone_alternate,email_addresses.email_address,users.user_name ,aos_products.name as proName,accounts.name as accName from opportunities left join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id left join accounts on accounts_opportunities.account_id = accounts.id left join email_addr_bean_rel on accounts.id = email_addr_bean_rel.bean_id and email_addr_bean_rel.primary_address = '1' and email_addr_bean_rel.deleted = '0'  left join email_addresses on email_addr_bean_rel.email_address_id = email_addresses.id  left join aos_products_opportunities_1_c on opportunities.id = aos_products_opportunities_1_c.aos_products_opportunities_1opportunities_idb left join aos_products on aos_products_opportunities_1_c.aos_products_opportunities_1aos_products_ida = aos_products.id inner join users on opportunities.assigned_user_id = users.id where opportunities.assigned_user_id = '".$_POST["users"][$i]."' and opportunities.deleted ='0'".$oppWhere;
								$resultOpp=$db->query($queryOpp);
								while($rowOpp =$db->fetchByAssoc($resultOpp)){
									$rowOpp["date_closed"] = date('d/m/Y H:i:s', strtotime($rowOpp["date_closed"].'+ 7 hours'));
									$opp[] = $rowOpp;
								}
									if($employee == true){
										if($_POST["users"][$i] == $_SESSION['authenticated_user_id']){	
											$user=array(
													"user"=>$row['name'],
													"Call"=>$call,
													"Meeting"=>$meeting,
													"Lead"=>$lead,
													"Account"=>$account,
													"Opp"=>$opp,
												);
											if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
												$group[$row["Se"]]=array();
												$group[$row["Se"]][]=$user;
											}else{
												$group[$row["Se"]][]=$user;
											}
										}
									}else{
										$user = array(
													"user"=>$row['name'],
													"Call"=>$call,
													"Meeting"=>$meeting,
													"Lead"=>$lead,
													"Account"=>$account,
													"Opp"=>$opp,
												);
											if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
												$group[$row["Se"]]=array();
												$group[$row["Se"]][]=$user;
											}else{
												$group[$row["Se"]][]=$user;
											}
									}
								}
							}
						}
					}
				
				}
				if(!empty($prov[$k]) && !empty($p)){
				$province[]=array(
					"province"=>$prov[$k],
					"data"=>$group,
				);
				}
				$group=array();
			}
			echo json_encode ($province);
		}
}
