<?php
function getDateRangeToSearchForAmount($period_list_selected = 'this_month')
{
        if(empty($period_list_selected)) $period_list_selected = 'Tháng này';
        switch ($period_list_selected) {
            case 'Tuần này':
                $datetimeTo = new DateTime("this week sunday");
               // $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("this week monday");
              //  $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Tuần trước':
                $datetimeTo = new DateTime("last week sunday");
               // $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("last week monday");
               // $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Tháng này':
                $datetimeTo = new DateTime('last day of this month');
              //  $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("first day of this month");
              //  $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Tháng trước':
                $datetimeTo = new DateTime("last day of last month");
              //  $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("first day of last month");
              //  $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Năm này':
                $datetimeTo = new DateTime('this year last day of december');
             //   $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime('this year first day of january');
             //   $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Năm trước':
                $datetimeTo = new DateTime('last year last day of december');
              //  $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime('last year first day of january');
              //  $datetimeFrom->setTime(0, 0, 0);
                break;

        }
        return array(
            'from' => $datetimeFrom,
            'to' => $datetimeTo
        );
}
function getDateTimeAsDbForAmount($period = 'this_month', $date_from = '', $date_to = '')
{
        
		$timedate = new TimeDate();
		date_default_timezone_set('Asia/Ho_Chi_Minh');
        if(empty($date_from) && empty($date_to)) {
            $range = getDateRangeToSearchForAmount($period);

            $from_by_user = $range['from']->format($timedate::DB_DATETIME_FORMAT);
            $to_by_user = $range['to']->format($timedate::DB_DATETIME_FORMAT);

        }
        else {

            $fromObj = $timedate->fromUserDate($date_from);
            $fromObj->setTime(0, 0, 0);
            $from_by_user = $fromObj->format($timedate::DB_DATETIME_FORMAT);

            $toObj = $timedate->fromUserDate($date_to);
            $toObj->setTime(23,59,59);
            $to_by_user = $toObj->format($timedate::DB_DATETIME_FORMAT);

        }

        $from_display = $timedate->to_display_date_time($from_by_user, true, false);
        $from = $timedate->to_db($from_display);

        $to_display = $timedate->to_display_date_time($to_by_user, true, false);
        $to = $timedate->to_db($to_display);

        $datetime_as_db = array(
            'from' => $from,
            'to' => $to
        );

        return $datetime_as_db;
}

if($_SERVER['REQUEST_METHOD'] == 'POST'){
	try{
		if (isset($_SERVER['HTTPS']) &&
		($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
		isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
		$_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https'){
					$protocol = 'https://';
		}
		else {
			$protocol = 'http://';
		}
		$apiUrl = $protocol.$_SERVER['HTTP_HOST']."/index.php?entryPoint=getLanguageForDropdown&id=status_activities_list";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $apiUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        $response = curl_exec($ch);
		$response = ((array)((array)(json_decode($response))[0])["language"]);
        curl_close($ch);
	global $db;
	$arr = array();
	$whr='';
	$sea = "";
	$myTask = "";
	global $current_user;
	$current_user->id = $_POST["user"];
	$start= ($_POST["page"] -1) * 200;
	if(isset($_POST['time']) && $_POST['time'] != "Tất cả"){
		$time = getDateTimeAsDbForAmount($_POST['time']);
		$from=date('Y-m-d', strtotime($time['from']));
		$to=date('Y-m-d', strtotime($time['to']));
		if(date('m', strtotime($time['to'])) == "02" && $_POST['time'] == "Tháng này"){
			$to = date('Y', strtotime($time['to']))."-02-29";
		}
		$wh = " and tasks.date_entered >= '".$from."' and tasks.date_entered <= '".$to."'";
	}else{
		$wh= '';	
	}
	if($_POST['myTask'] == "1"){
		$myTask = " and tasks.assigned_user_id = '".$_POST["user"]."'";
	}
	if(isset($_POST['search'])){
		if($_POST['search'] != ""){
			$sea .= " and (tasks.name like '%".$_POST['search']."%' OR tasks.status like '%".$_POST['search']."%' OR tasks.description like '%".$_POST['search']."%')";
		}
	}
	$where = "tasks.deleted = '0'".$wh.$sea.$myTask;
	$task = BeanFactory::newBean('Tasks');
	$queryNew = $task->create_new_list_query("tasks.date_entered DESC",$where,array());
	$query = "select tasks.*,users.user_name,leads.last_name,accounts.name as accName from ($queryNew) as tasks left join accounts on tasks.parent_id = accounts.id and accounts.deleted = '0' left join leads on tasks.parent_id = leads.id and leads.deleted = '0' left join users on tasks.assigned_user_id = users.id and users.deleted = '0' order by tasks.date_entered DESC";
	$queryCount = "select count(tasks.id) as num from tasks as taskTable inner join ($queryNew) as tasks on taskTable.id = tasks.id where tasks.deleted = '0'";
	$resCount = $task->db->query($queryCount);
	$rowCount = $task->db->fetchByAssoc($resCount);
    $limit_query = $task->db->limitQuery($query, $start, 200, false, '', false);
    $res = $task->db->query($limit_query);
	while($row = $task->db->fetchByAssoc($res)){
		$arr[] = array(
							"id"=>$row["id"],
							"name"=>$row["last_name"],
							"status"=>$row["status"],
							"description"=>$row["description"],
							"user_name"=>$row["user_name"],
							"date_entered"=>date('Y-m-d H:i:s', strtotime($row["date_entered"].' + 7 hours')),
							"date_start"=>date('Y-m-d H:i:s', strtotime($row["date_start"].' + 7 hours')),
							"accountName"=>$row["accName"],
							"lastName"=>$row["last_name"],
							"name"=>$row["name"],
							"length"=>$rowCount["num"],
						);
	}
	echo json_encode($arr);
	}
	catch(PDOException $e) {
		echo "Error: " . $e->getMessage();
	}
}