<?php
global $db;
global $app_list_strings;

$query = "select accounts.*,accounts_cstm.*,users.user_name,users.last_name as userName,email.email_address,aos_invoices.invoiceno,aos_invoices.invoice_date,aos_invoices.paymentmethod,aos_invoices.shipping_method,aos_invoices.payment_date,aos_invoices.Pi_Effective_Date,aos_invoices.invoice_date,aos_invoices.delivery_date,aos_invoices.Type_Invoice as invType,aos_invoices.name as invName  ,aos_invoices.IS_COMMERCIAL,aos_invoices.IS_NON_COMMERCIAL,aos_invoices.Pi_Effective_Date,aos_invoices.Customer_Ref,aos_invoices.shipping_amount,aos_invoices.billing_address_street as billingStreet,aos_invoices.shipping_address_street as shippingStreet from accounts inner join accounts_cstm on accounts.id = accounts_cstm.id_c inner join users on accounts.assigned_user_id = users.id left join (select email_addr_bean_rel.bean_id, email_addresses.email_address from email_addr_bean_rel inner join email_addresses on email_addresses.id = email_addr_bean_rel.email_address_id and email_addresses.deleted = '0') as email on accounts.id = email.bean_id right join aos_invoices on accounts.id = aos_invoices.billing_account_id where aos_invoices.id = '".$_POST["id"]."'";
$resAccInfo = $db->query($query);
$rowAccInfo = $db->fetchByAssoc($resAccInfo);
if($rowAccInfo["invType"] == '1'){
	$inforExcel = "select * from tttt_worldfonepbx where deleted = '0' and type_sys_app = '10'";
	$resInfo = $db->query($inforExcel);
	$rowInfo = $db->fetchByAssoc($resInfo);
}else{
	$inforExcel = "select * from tttt_worldfonepbx where deleted = '0' and type_sys_app = '11'";
	$resInfo = $db->query($inforExcel);
	$rowInfo = $db->fetchByAssoc($resInfo);
}
$queryCont ="select contacts.*,users.user_name,users.last_name as userName,email.email_address,aos_invoices.invoiceno,aos_invoices.invoice_date,aos_invoices.paymentmethod,aos_invoices.shipping_method,aos_invoices.payment_date,aos_invoices.Pi_Effective_Date,aos_invoices.invoice_date,aos_invoices.delivery_date,aos_invoices.Type_Invoice as invType,aos_invoices.name as invName  ,aos_invoices.IS_COMMERCIAL,aos_invoices.IS_NON_COMMERCIAL,aos_invoices.Pi_Effective_Date,aos_invoices.Customer_Ref,aos_invoices.shipping_amount,aos_invoices.billing_address_street as billingStreet,aos_invoices.shipping_address_street as shippingStreet from contacts inner join users on contacts.assigned_user_id = users.id left join (select email_addr_bean_rel.bean_id, email_addresses.email_address from email_addr_bean_rel inner join email_addresses on email_addresses.id = email_addr_bean_rel.email_address_id and email_addresses.deleted = '0') as email on contacts.id = email.bean_id right join aos_invoices on contacts.id = aos_invoices.billing_contact_id where aos_invoices.id = '".$_POST["id"]."'";
$resContInfo = $db->query($queryCont);
$rowContInfo = $db->fetchByAssoc($resContInfo);
$rowAccInfo["shipping_method"] = $app_list_strings["shipping_method_list"][$rowAccInfo["shipping_method"]];
$rowAccInfo["paymentmethod"] = $app_list_strings["paymentmethod_list"][$rowAccInfo["paymentmethod"]];

$rowAccInfo["product"] = array();
$q = "select aos_products_quotes.*,aos_products.unitcode from aos_products_quotes inner join aos_products on aos_products_quotes.product_id = aos_products.id where aos_products_quotes.deleted = '0' and aos_products_quotes.parent_type = 'AOS_Invoices' and aos_products_quotes.parent_id = '".$_POST["id"]."'";
$res = $db->query($q);
while($row = $db->fetchByAssoc($res)){
$rowAccInfo["product"][] = $row;
}
echo json_encode(array("account"=>$rowAccInfo,
						"contact"=>$rowContInfo,
						"info"=>$rowInfo
				));