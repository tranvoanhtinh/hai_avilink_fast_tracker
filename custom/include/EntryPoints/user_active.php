<?php
// Kiểm tra xem yêu cầu là POST hay không
if ($_SERVER["REQUEST_METHOD"] === "POST") {
    global $db;

    $sql = "SELECT id, name FROM `cs_cususer` WHERE status = 'Active' AND deleted = '0'";

    $result = $db->query($sql);

    $users = array(); // Tạo mảng để lưu trữ người dùng

    while ($row = $result->fetch_assoc()) {
        $user = array(
            'id' => $row['id'],
            'name' => $row['name']
        );
        $users[] = $user; // Thêm người dùng vào mảng
    }

    // Gửi mảng người dùng dưới dạng JSON
    header('Content-Type: application/json');
    echo json_encode($users);
    exit;
}
?>
