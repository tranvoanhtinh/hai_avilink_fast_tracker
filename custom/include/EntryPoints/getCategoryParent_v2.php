<?php

global $db;

// Khởi tạo mảng $parent_category để lưu trữ các danh mục cha và danh sách các danh mục con của chúng
$parent_category = array();

$sql = "SELECT pc.*, pcc.ordernum_c 
        FROM aos_product_categories pc
        LEFT JOIN aos_product_categories_cstm pcc ON pc.id = pcc.id_c
        WHERE pc.is_parent = 1 AND pc.deleted = 0
        ORDER BY pcc.ordernum_c ASC";

$result = $db->query($sql);

// Duyệt qua các kết quả
// Duyệt qua các kết quả
while ($rows = $db->fetchByAssoc($result)) {


    $parentCategoryId = $rows['id'];
    $parentCategoryName = $rows['name'];
    $parentCategoryOrdernum_c = $rows['ordernum_c'];
    // Lấy tất cả các danh mục con của danh mục cha hiện tại
$sql2 = "SELECT pc.*, pcc.ordernum_c 
         FROM aos_product_categories pc
         LEFT JOIN aos_product_categories_cstm pcc ON pc.id = pcc.id_c
         WHERE pc.parent_category_id = '$parentCategoryId' AND pc.deleted = 0";

    $result2 = $db->query($sql2);

    // Khởi tạo mảng để lưu trữ danh sách các danh mục con của danh mục cha hiện tại
    $subCategories = array();

    // Duyệt qua các danh mục con
    while ($subRows = $db->fetchByAssoc($result2)) {
        // Lưu thông tin về danh mục con vào mảng $subCategories
        $subCategoryId = $subRows['id'];
        $subCategoryName = $subRows['name'];
        $subCategoryOrdernum_c = $subRows['ordernum_c'];
        $subCategory = array('id' => $subCategoryId, 'name' => $subCategoryName,'ordernum_c'=>$subCategoryOrdernum_c);



        $subCategories[] = $subCategory;
    }

    // Lưu danh sách các danh mục con vào mảng $parent_category với key là id của danh mục cha
    $parentCategory = array('id' => $parentCategoryId, 'name' => $parentCategoryName, 'ordernum_c'=> $parentCategoryOrdernum_c, 'subCategories' => $subCategories);
    $parent_category[] = $parentCategory;
}
// Chuyển đổi mảng thành chuỗi JSON với ký tự Unicode không được chuyển đổi
$json_parent_category = json_encode($parent_category, JSON_UNESCAPED_UNICODE);



echo $json_parent_category;

