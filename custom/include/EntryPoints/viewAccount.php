<?php
function getDateRangeToSearchForAmount($period_list_selected = 'this_month')
{
        if(empty($period_list_selected)) $period_list_selected = 'Tháng này';
        switch ($period_list_selected) {
            case 'Tuần này':
                $datetimeTo = new DateTime("this week sunday");
               // $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("this week monday");
              //  $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Tuần trước':
                $datetimeTo = new DateTime("last week sunday");
               // $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("last week monday");
               // $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Tháng này':
                $datetimeTo = new DateTime('last day of this month');
              //  $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("first day of this month");
              //  $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Tháng trước':
                $datetimeTo = new DateTime("last day of last month");
              //  $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("first day of last month");
              //  $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Năm này':
                $datetimeTo = new DateTime('this year last day of december');
             //   $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime('this year first day of january');
             //   $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Năm trước':
                $datetimeTo = new DateTime('last year last day of december');
              //  $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime('last year first day of january');
              //  $datetimeFrom->setTime(0, 0, 0);
                break;

        }
        return array(
            'from' => $datetimeFrom,
            'to' => $datetimeTo
        );
}
function getDateTimeAsDbForAmount($period = 'this_month', $date_from = '', $date_to = '')
{
        
		$timedate = new TimeDate();
		date_default_timezone_set('Asia/Ho_Chi_Minh');
        if(empty($date_from) && empty($date_to)) {
            $range = getDateRangeToSearchForAmount($period);

            $from_by_user = $range['from']->format($timedate::DB_DATETIME_FORMAT);
            $to_by_user = $range['to']->format($timedate::DB_DATETIME_FORMAT);

        }
        else {

            $fromObj = $timedate->fromUserDate($date_from);
            $fromObj->setTime(0, 0, 0);
            $from_by_user = $fromObj->format($timedate::DB_DATETIME_FORMAT);

            $toObj = $timedate->fromUserDate($date_to);
            $toObj->setTime(23,59,59);
            $to_by_user = $toObj->format($timedate::DB_DATETIME_FORMAT);

        }

        $from_display = $timedate->to_display_date_time($from_by_user, true, false);
        $from = $timedate->to_db($from_display);

        $to_display = $timedate->to_display_date_time($to_by_user, true, false);
        $to = $timedate->to_db($to_display);

        $datetime_as_db = array(
            'from' => $from,
            'to' => $to
        );

        return $datetime_as_db;
}

if($_SERVER['REQUEST_METHOD'] == 'POST'){
	if (isset($_SERVER['HTTPS']) &&
		($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
		isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
		$_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https'){
					$protocol = 'https://';
		}
		else {
			$protocol = 'http://';
		}
			$apiUrl = $protocol.$_SERVER['HTTP_HOST']."/index.php?entryPoint=getLanguageForDropdown&id=sales_stage_dom";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_URL, $apiUrl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            $response = curl_exec($ch);
			$response = ((array)((array)(json_decode($response))[0])["language"]);
            curl_close($ch);
	try{
		global $db;
		global $current_user;
		$current_user->id = $_POST["user"];
		date_default_timezone_set('Asia/Ho_Chi_Minh');
		$q = "select email.email_address,users.user_name,accounts_cstm.shortname_c,accounts_cstm.bussiness_type_c,accounts.name,accounts_cstm.primarycontact_c, accounts.phone_alternate,accounts.billing_address_street,accounts.description,accounts.date_entered,accounts.date_modified,accounts.assigned_user_id from accounts inner join accounts_cstm on accounts.id = accounts_cstm.id_c inner join users on accounts.assigned_user_id = users.id left join (select email_addr_bean_rel.bean_id, email_addresses.email_address from email_addr_bean_rel inner join email_addresses on email_addresses.id = email_addr_bean_rel.email_address_id and email_addresses.deleted = '0') as email on accounts.id = email.bean_id where accounts.deleted = '0' and accounts.id = '".$_POST["id"]."'";
		$res = $db->query($q);
		$row = $db->fetchByAssoc($res);
		if(!empty($row)){
			$call = array();
			$calls = array();
			$meeting = array();
			$meetings = array();
			$note = array();
			$notes = array();
			$opp = array();
			$opps = array();
			$contact = array();
			$contacts = array();
			$bean = new Call();
			$queryNew2 = $bean->create_new_list_query("calls.date_modified DESC","",array());
			$query2 = "select distinct reminders.related_event_module_id, calls.*,reminders.popup,reminders.email,reminders.timer_popup,reminders.timer_email from ($queryNew2) as calls left join reminders on calls.id = reminders.related_event_module_id where calls.parent_id ='".$_POST["id"]."'   
			and calls.deleted='0' ORDER BY calls.date_start DESC limit 0,5";
			$result = $db->query($query2);
			while($row2 = $db->fetchByAssoc($result)){
				$row2["date_entered"] = date('Y-m-d H:i:s', strtotime($row2["date_entered"].' + 7 hours'));
				$row2["date_start"] = date('Y-m-d H:i:s', strtotime($row2["date_start"].' + 7 hours'));
				$calls[] = $row2;
			}
			
			$bean = BeanFactory::newBean('Meetings');
			$queryNew = $bean->create_new_list_query("meetings.date_modified DESC","",array());
			
			$query = "select distinct reminders.related_event_module_id,meetings.*,reminders.popup,reminders.email,reminders.timer_popup,reminders.timer_email from ($queryNew) as meetings  left join reminders on meetings.id = reminders.related_event_module_id where parent_id ='".$_POST["id"]."'   
			and meetings.deleted='0' ORDER BY meetings.date_start DESC limit 0,5";
			$result = $db->query($query);
			while($row2 = $db->fetchByAssoc($result)){
				$row2["date_entered"] = date('Y-m-d H:i:s', strtotime($row2["date_entered"].' + 7 hours'));
				$row2["date_start"] = date('Y-m-d H:i:s', strtotime($row2["date_start"].' + 7 hours'));
				$meetings[] = $row2;
			}
		
			$bean = BeanFactory::newBean('Notes');
			$queryNew = $bean->create_new_list_query("notes.date_modified DESC","",array());
			
			$query ="select notes.* from ($queryNew) as notes  where parent_id ='".$_POST["id"]."' and deleted ='0' and parent_type = 'Accounts' order by date_modified DESC limit 0,5";
			$result = $db->query($query);
			while($row2 = $db->fetchByAssoc($result)){
				$row2["date_entered"] = date('Y-m-d H:i:s', strtotime($row2["date_entered"].' + 7 hours'));
				$notes[] = $row2;
			}
			//
			
			$bean = BeanFactory::newBean('Contacts');
			$queryNew = $bean->create_new_list_query("contacts.date_modified DESC","",array());
			
			
			$query ="select contacts.*,email.email_address from ($queryNew) as contacts left join accounts_contacts on contacts.id = accounts_contacts.contact_id left join accounts on accounts_contacts.account_id = accounts.id left join (select email_addr_bean_rel.bean_id, email_addresses.email_address from email_addr_bean_rel inner join email_addresses on email_addresses.id = email_addr_bean_rel.email_address_id and email_addresses.deleted = '0') as email on contacts.id = email.bean_id  where accounts.id ='".$_POST["id"]."' and contacts.deleted ='0' order by contacts.date_modified DESC limit 0,5";
			$result = $db->query($query);
			while($row2 = $db->fetchByAssoc($result)){
				$row2["date_entered"] = date('Y-m-d H:i:s', strtotime($row2["date_entered"].' + 7 hours'));
				$contacts[] = $row2;
			}
			
			$bean = BeanFactory::newBean('Opportunities');
			$queryNew = $bean->create_new_list_query("opportunities.date_modified DESC","",array());
			
			$query ="select opportunities.*,aos_products.name as proName,accounts.id as accId from ($queryNew) as opportunities left join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id left join accounts on accounts_opportunities.account_id = accounts.id left join aos_products_opportunities_1_c on opportunities.id = aos_products_opportunities_1_c.aos_products_opportunities_1opportunities_idb left join aos_products on aos_products_opportunities_1_c.aos_products_opportunities_1aos_products_ida = aos_products.id where accounts.id ='".$_POST["id"]."' and opportunities.deleted ='0' order by opportunities.depositdate DESC limit 0,5";
			$result = $db->query($query);
			while($row2 = $db->fetchByAssoc($result)){
				$row2["date_entered"] = date('Y-m-d H:i:s', strtotime($row2["date_entered"].' + 7 hours'));
				$row2["sales_sta"] = $row2["sales_stage"];
				$row2["sales_stage"] = $response[$row2["sales_stage"]];
				$opps[] = $row2;
			}
			$row["call"] = $calls;
			$row["meeting"] = $meetings;
			$row["note"] = $notes;
			$row["opportunity"] = $opps;
			$row["contact"] = $contacts;
			echo json_encode($row);
		}
	}
	catch(PDOException $e) {
		echo "Error: " . $e->getMessage();
	}
}