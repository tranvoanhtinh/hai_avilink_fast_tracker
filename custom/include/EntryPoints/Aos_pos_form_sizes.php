<?php
// Kiểm tra xem yêu cầu là POST hay không
if ($_SERVER["REQUEST_METHOD"] === "POST") {
    global $db;

    // Kiểm tra xem có giá trị 'id' được gửi từ yêu cầu không
    if (isset($_POST['id'])) {
        // Lấy giá trị 'id' từ yêu cầu
        $id = $_POST['id'];
        $sql = "SELECT aos_size.*
                FROM aos_size
                JOIN aos_products_aos_size_1_c ON aos_size.id = aos_products_aos_size_1_c.aos_products_aos_size_1aos_size_idb 
                JOIN aos_products ON aos_products_aos_size_1_c.aos_products_aos_size_1aos_products_ida = aos_products.id 
                WHERE aos_products.id = '".$id."' AND aos_size.deleted = 0";

        $result = $db->query($sql);

        $sizes = array(); // Tạo mảng để lưu trữ kích thước

        while ($row = $result->fetch_assoc()) {
            // Thêm mỗi kích thước vào mảng với key là 'size' + số đếm
            if($row['size_l_cm'] != null && $row['size_w_cm'] != null && $row['size_h_cm'] != null){
            $sizes[] = array(
                'size_l' => customFormat($row['size_l_cm']),
                'size_w' => customFormat($row['size_w_cm']),
                'size_h' => customFormat($row['size_h_cm']),
            );
            }
        }



        // Gửi mảng kích thước dưới dạng JSON
        header('Content-Type: application/json');
        if($sizes){
        echo json_encode($sizes);
        }else {
        echo json_encode($sizes);
        }
    }
}

function customFormat($number) {
    // Định dạng số với một số thập phân
    return number_format((float) $number, 1);
}
?>
