<?php

global $db; // Kiểm tra xem biến $db đã được khởi tạo chưa
require_once('modules/AOS_Products_Quotes/AOS_Products_Quotes.php');
require_once('modules/AOS_Line_Item_Groups/AOS_Line_Item_Groups.php');
if ($_SERVER["REQUEST_METHOD"] === "POST") {

    $type = $_POST['type'];
    $parent_id = $_POST['previous_id'];

    if ($type == 1) {
        $parent_type = 'AOS_Inventoryinput';
    } elseif ($type == 2) {
        $parent_type = 'AOS_Inventoryoutput';
    } 
    $sql = "SELECT pg.id, pg.group_id 
            FROM aos_products_pos pg 
            LEFT JOIN aos_pos_line_items_detail lig ON pg.group_id = lig.id 
            WHERE pg.parent_type = '" . $parent_type . "' 
            AND pg.parent_id = '" . $parent_id . "' 
            AND pg.deleted = 0 
            ORDER BY lig.number ASC, pg.number ASC";

    if ($type == 3) {
        $parent_type = 'AOS_invoices';
        $sql = "SELECT pg.id, pg.group_id FROM aos_products_quotes pg LEFT JOIN aos_line_item_groups lig ON pg.group_id = lig.id WHERE pg.parent_type = '".$parent_type."' AND pg.parent_id = '".$parent_id."' AND pg.deleted = 0 ORDER BY lig.number ASC, pg.number ASC";
    }        

    $result = $db->query($sql); // Sử dụng biến $db thay vì $focus->db

    $info_line_products = array(); // Khởi tạo mảng trước khi sử dụng
    if($type == '2' || $type == '1'){
        while ($row = $db->fetchByAssoc($result)) {
            $line_item = new AOS_Products_Pos();
            $line_item->retrieve($row['id'], false);
            $line_item = $line_item->toArray();

            $group_item = 'null';
            if ($row['group_id'] != null) {
                $group_item = new AOS_Pos_Line_Items_Detail();
                $group_item->retrieve($row['group_id'], false);
                $group_item = $group_item->toArray();
            }
            // Thêm cả $line_item và $group_item vào một mảng con và gán vào $info_line_products
            $info_line_products[] = array(
                'line_item'=>$line_item, 
                'group_item'=>$group_item
            );
        }
    }
    if($type =='3'){
            while ($row = $db->fetchByAssoc($result)) {
            $line_item = new AOS_Products_Quotes();
            $line_item->retrieve($row['id'], false);
            $line_item = $line_item->toArray();

            $group_item = 'null';
            if ($row['group_id'] != null) {
                $group_item = new AOS_Line_Item_Groups();
                $group_item->retrieve($row['group_id'], false);
                $group_item = $group_item->toArray();
            }
            // Thêm cả $line_item và $group_item vào một mảng con và gán vào $info_line_products
            $info_line_products[] = array(
                'line_item'=>$line_item, 
                'group_item'=>$group_item
            );
        }
    }

    echo json_encode($info_line_products);
}
?>
