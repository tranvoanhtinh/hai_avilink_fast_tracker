<?php
function getDateRangeToSearchForAmount($period_list_selected = 'this_month')
{
        if(empty($period_list_selected)) $period_list_selected = 'Tháng này';
        switch ($period_list_selected) {
            case 'Tuần này':
                $datetimeTo = new DateTime("this week sunday");
               // $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("this week monday");
              //  $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Tuần trước':
                $datetimeTo = new DateTime("last week sunday");
               // $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("last week monday");
               // $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Tháng này':
                $datetimeTo = new DateTime('last day of this month');
              //  $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("first day of this month");
              //  $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Tháng trước':
                $datetimeTo = new DateTime("last day of last month");
              //  $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("first day of last month");
              //  $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Năm này':
                $datetimeTo = new DateTime('this year last day of december');
             //   $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime('this year first day of january');
             //   $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Năm trước':
                $datetimeTo = new DateTime('last year last day of december');
              //  $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime('last year first day of january');
              //  $datetimeFrom->setTime(0, 0, 0);
                break;

        }
        return array(
            'from' => $datetimeFrom,
            'to' => $datetimeTo
        );
}
function getDateTimeAsDbForAmount($period = 'this_month', $date_from = '', $date_to = '')
{
        
		$timedate = new TimeDate();
		date_default_timezone_set('Asia/Ho_Chi_Minh');
        if(empty($date_from) && empty($date_to)) {
            $range = getDateRangeToSearchForAmount($period);

            $from_by_user = $range['from']->format($timedate::DB_DATETIME_FORMAT);
            $to_by_user = $range['to']->format($timedate::DB_DATETIME_FORMAT);

        }
        else {

            $fromObj = $timedate->fromUserDate($date_from);
            $fromObj->setTime(0, 0, 0);
            $from_by_user = $fromObj->format($timedate::DB_DATETIME_FORMAT);

            $toObj = $timedate->fromUserDate($date_to);
            $toObj->setTime(23,59,59);
            $to_by_user = $toObj->format($timedate::DB_DATETIME_FORMAT);

        }

        $from_display = $timedate->to_display_date_time($from_by_user, true, false);
        $from = $timedate->to_db($from_display);

        $to_display = $timedate->to_display_date_time($to_by_user, true, false);
        $to = $timedate->to_db($to_display);

        $datetime_as_db = array(
            'from' => $from,
            'to' => $to
        );

        return $datetime_as_db;
}

if($_SERVER['REQUEST_METHOD'] == 'POST'){
	try{
	global $db;
	$arr = array();
	$whr='';
	$sea = "";
	$mycontact = "";
	global $current_user;
	$current_user->id = $_POST["user"];
	$start= ($_POST["page"] -1) * 200;
	if(isset($_POST['time']) && $_POST['time'] != "Tất cả"){
		$time = getDateTimeAsDbForAmount($_POST['time']);
		$from=date('Y-m-d', strtotime($time['from']));
		$to=date('Y-m-d', strtotime($time['to']));
		if(date('m', strtotime($time['to'])) == "02" && $_POST['time'] == "Tháng này"){
			$to = date('Y', strtotime($time['to']))."-02-29";
		}
		$wh = " and contacts.date_entered >= '".$from."' and contacts.date_entered <= '".$to."'";
	}else{
		$wh= '';	
	}
	if($_POST['myContact'] == "1"){
		$mycontact = " and contacts.assigned_user_id = '".$_POST["user"]."'";
	}
	if(isset($_POST['search'])){
		if($_POST['search'] != ""){
			$sea .= " and (contacts.last_name like '%".$_POST['search']."%' OR contacts.phone_mobile like '%".$_POST['search']."%' OR contacts.account_name like '%".$_POST['search']."%' OR contacts_cstm.identitycard_c like '%".$_POST['search']."%' OR contacts_cstm.vatcode_c like '%".$_POST['search']."%')";
		}
	}
	$where = "contacts.deleted = '0'".$wh.$sea.$mycontact;
	$contact = new Contact();
	//$contact->load_relationship('user_name');
	$query = $contact->create_new_list_query("contacts.date_entered DESC",$where,array());
	$query = $query." order by contacts.date_entered DESC";
	$queryCount = "select count(contacts.id) as num from contacts inner join ($query) as contact on contacts.id = contact.id where contacts.deleted = '0'";
	$resCount = $db->query($queryCount);
	$rowCount = $db->fetchByAssoc($resCount);
    $limit_query = $db->limitQuery($query, $start,200, false, '', false);
    $res = $db->query($limit_query);
	while($row = $db->fetchByAssoc($res)){
		$q2 = "select email_addresses.email_address from email_addresses inner join email_addr_bean_rel on email_addresses.id = email_addr_bean_rel.email_address_id and email_addresses.deleted = '0' inner join contacts on contacts.id =  email_addr_bean_rel.bean_id and contacts.id = '".$row["id"]."' and contacts.deleted = '0' where email_addr_bean_rel.primary_address = '1'";
		$r2 = $db->query($q2);
		$row2 = $db->fetchByAssoc($r2);
		$q3 = "select users.user_name from users inner join contacts on users.id = contacts.assigned_user_id and contacts.deleted = '0' where users.id = '".$row["assigned_user_id"]."' and users.deleted = '0'";
		$r3 = $db->query($q3);
		$row3 = $db->fetchByAssoc($r3);
		$arr[] = array(
						"id"=>$row["id"],
						"name"=>$row["last_name"],
						"phone"=>$row["phone_mobile"],
						"salutation"=>$row["salutation"],
						"user_name"=>$row3["user_name"],
						"date_entered"=>date('Y-m-d H:i:s', strtotime($row["date_entered"].' + 7 hours')),
						"accName"=>$row["accName"],
						"email"=>($row2["email_address"] != null && $row2["email_address"] != "")?$row2["email_address"]:"",
						"description"=>$row["description"],
						"length"=>$rowCount["num"]
					);
	}
	echo json_encode($arr);
	}
	catch(PDOException $e) {
		echo "Error: " . $e->getMessage();
	}
}