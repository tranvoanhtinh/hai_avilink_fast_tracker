<?php
function getDateTimeAsDb($period = 'this_month', $date_from = '', $date_to = '')
    {
        global $timedate;
		date_default_timezone_set('Asia/Ho_Chi_Minh');
        if(empty($date_from) && empty($date_to)) {
            $range = getDateRangeToSearch($period);

            $from_by_user = $range['from']->format($timedate::DB_DATETIME_FORMAT);
            $to_by_user = $range['to']->format($timedate::DB_DATETIME_FORMAT);

        }
        else {

            $fromObj = $timedate->fromUserDate($date_from);
            $fromObj->setTime(0, 0, 0);
            $from_by_user = $fromObj->format($timedate::DB_DATETIME_FORMAT);

            $toObj = $timedate->fromUserDate($date_to);
            $toObj->setTime(23,59,59);
            $to_by_user = $toObj->format($timedate::DB_DATETIME_FORMAT);

        }

        $from_display = $timedate->to_display_date_time($from_by_user, true, false);
        $from = $timedate->to_db($from_display);

        $to_display = $timedate->to_display_date_time($to_by_user, true, false);
        $to = $timedate->to_db($to_display);

        $datetime_as_db = array(
            'from' => $from,
            'to' => $to
        );

        return $datetime_as_db;
    }
function getDateRangeToSearch($period_list_selected = 'this_month')
    {
        if(empty($period_list_selected)) $period_list_selected = 'this_month';
		date_default_timezone_set('Asia/Ho_Chi_Minh');
        switch ($period_list_selected) {
            case 'this_week':
                $datetimeTo = new DateTime("next week monday");
              //  $datetimeTo->setTime(23, 59, 59);
			   
                $datetimeFrom = new DateTime("this week tuesday");
				 
               // $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'last_week':
                $datetimeTo = new DateTime("this week monday");
              //  $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("last week tuesday");
              //  $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'this_month':
                $datetimeTo = new DateTime('last day of this month');
               // $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("first day of this month");
               // $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'last_month':
                $datetimeTo = new DateTime("last day of last month");
                //$datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("first day of last month");
               // $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'this_year':
                $datetimeTo = new DateTime('this year last day of december');
                //$datetimeTo->setTime(23, 59, 59);
				$datetimeTo->add(new DateInterval('P1D'));
                $datetimeFrom = new DateTime('this year first day of january');
               // $datetimeFrom->setTime(0, 0, 0);
			    $datetimeFrom->add(new DateInterval('P1D'));
                break;
            case 'last_year':
                $datetimeTo = new DateTime('last year last day of december');
				$datetimeTo->add(new DateInterval('P1D'));
               // $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime('last year first day of january');
               // $datetimeFrom->setTime(0, 0, 0);
			    $datetimeFrom->add(new DateInterval('P1D'));
                break;

        }
        return array(
            'from' => $datetimeFrom,
            'to' => $datetimeTo
        );
    }
	
	if((!empty($_POST['time'])) && ($_POST['time'] != 'default')){
				if($_POST['time'] != "is_between"){
			$time=getDateTimeAsDb($_POST['time']);
			$from=date('Y-m-d', strtotime($time['from']));
			$to=date('Y-m-d', strtotime($time['to']));
				}else{
				$from=date('Y-m-d');
				$to=date('Y-m-d');
				}
			
	}
	echo json_encode(array(
		"from"=>date('d/m/Y', strtotime($from)),	
		"to"=>date('d/m/Y', strtotime($to))	
	));