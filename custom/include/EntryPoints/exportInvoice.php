<?php
global $current_language;
$inv_mod_strings = return_module_language($current_language, "AOS_Invoices");
global $db;
date_default_timezone_set('Asia/ho_chi_minh');  
$date = date('d/m/Y');
$api2 = "https://api-demo-longphatcrm.mifi.vn/api/v2/invoice/GetFkey";
$headers2 = ['Content-Type: application/json'];
$ch2 = curl_init();
$export = "1";
$data2 = '{
"ApiUserName":"admin",
"ApiPassword":"demo123",
"ApiInvPattern": "01GTKT0/001",
"ApiInvSerial": "AA/21E",
}';
curl_setopt($ch2, CURLOPT_HTTPHEADER, $headers2);
curl_setopt($ch2, CURLOPT_HEADER, false);
curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch2, CURLOPT_URL, $api2);
curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch2, CURLOPT_POSTFIELDS, $data2);
$response2 = curl_exec($ch2);
$response2 = json_decode($response2,true);


$q = "select * from aos_invoices where id = '".$_POST["id"]."' and deleted = '0'";
$r = $db->query($q);
$row = $db->fetchByAssoc($r);

if(!empty($row)){
	//check if status invoices
	if($row["status_invoice_ref"] == "2"){
		$export = "0";
	}
	//select account information
	$query = "select phone_alternate,name,account_code,tax_code,billing_address_street from accounts where id = '".$row["billing_account_id"]."' and deleted ='0'"; 
	$result = $db->query($query);
	$rw = $db->fetchByAssoc($result);
	$query2 = "select last_name from contacts where id = '".$row["billing_contact_id"]."' and deleted = '0'"; 
	$res2 = $db->query($query2);
	$rww = $db->fetchByAssoc($res2);
	$q1 = "select aos_products_quotes.name,aos_products_quotes.part_number,aos_products.unitcode,aos_products_quotes.product_qty,aos_products_quotes.product_list_price,aos_products_quotes.vat,aos_products_quotes.vat_amt,aos_products_quotes.product_unit_price,aos_products_quotes.product_total_price,aos_products_quotes.item_description,aos_products_quotes.product_discount from aos_products right join aos_products_quotes on aos_products_quotes.product_id = aos_products.id  inner join aos_line_item_groups  on aos_products_quotes.group_id = aos_line_item_groups.id inner join aos_invoices on aos_line_item_groups.parent_id = aos_invoices.id where aos_invoices.id = '".$_POST["id"]."' and aos_invoices.deleted = '0'";
	$r1 = $db->query($q1);
	$content = '';
	
	while($row1 = $db->fetchByAssoc($r1)){
		$discount = (int)$row1["product_discount"];
		if($discount > 0){
			$isDiscount = 1;
		}else{
			$isDiscount = 0;
		}
		if(!empty($row1["product_qty"])){
			$sum = $row1["product_list_price"] * (int)$row1["product_qty"];
		}else{
			$sum = $row1["product_list_price"];
		}
		$content.= '{
						"code":"'.$row1["part_number"].'",
						"ProdName":"'.$row1["name"].'",
						"ProdUnit":"'.$row1["unitcode"].'",
						"ProdQuantity":"'.$row1["product_qty"].'",
						"ProdPrice":"'.$row1["product_list_price"].'",
						"VATRate":"'.$row1["vat"].'",
						"VATAmount":"'.$row1["vat_amt"].'",
						"Total":"'.$sum.'",
						"Amount":"'.$row1["product_total_price"].'",
						"Remark":"'.$row1["item_description"].'",
						"IsSum":"'.$isDiscount.'",
					},';
					
		
	};
	
}

$data = '
{
"ApiUserName":"admin",
"ApiPassword":"demo123",
"ApiInvPattern": "01GTKT0/001",
"ApiInvSerial": "AA/21E", 
"fkey": "'.$response2["fkey"].'",
"MaKH": "'.$rw["account_code"].'",
"Buyer": "'.$rww["last_name"].'",
"CusName": "'.$rw["name"].'",
"CusAddress":"'.$rw["billing_address_street"].'",
"CusPhone": "'.$rw["phone_alternate"].'",
"CusTaxCode":"'.$rw["tax_code"].'",
"PaymentMethod":"TM/CK",
"ArisingDate": "'.$date.'",
"Total": "'.$row["total_amt"].'",
"VATAmount": "'.$row["tax_amount"].'",
"Amount": "'.$row["total_amount"].'",
"AmountInWords": "",
"SO": "'.$row["number"].'",
"Products": ['.$content.']
}
';
if($export == "1"){
	if($_POST["type"] == "1"){
	$api = "https://api-demo-longphatcrm.mifi.vn/api/v2/invoice/importAndPublishInv";
	// kiểm tra nếu hóa đơn chưa xuất thì mới xuất
	// nếu xuất hóa đơn đã kí, lưu invNo
	$saveInv = "1";
	}else{
	$api = "https://api-demo-longphatcrm.mifi.vn/api/v3/invoice/importInvTemp";	
	}
	$ch = curl_init();
	$headers = ['Content-Type: application/json'];
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_URL, $api);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	$response = curl_exec($ch);
	$response= json_decode($response,true);


	if($response["status"] == "OK"){
		$data3='{
			"ApiUserName":"admin",
			"ApiPassword":"demo123",
			"inv_template": "01GTKT0/001",
			"inv_serial": "AA/21E",
			"signture_type":"1",
			"fkey": "'.$response2["fkey"].'",
		}';
		$ch3 = curl_init();
		$api3 = "https://api-demo-longphatcrm.mifi.vn/api/v2/invoice/DownloadPdf";
		$headers3 = ['Content-Type: application/json'];
		curl_setopt($ch3, CURLOPT_HTTPHEADER, $headers3);
		curl_setopt($ch3, CURLOPT_HEADER, false);
		curl_setopt($ch3, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch3, CURLOPT_URL, $api3);
		curl_setopt($ch3, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch3, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch3, CURLOPT_POSTFIELDS, $data3);
		$response3 = curl_exec($ch3);
		$response3= json_decode($response3,true);
		$url = $response3["link_file"];
		if($saveInv == "1"){
			$q = "update aos_invoices set invoiceno = '".$response["data"][0]["InvNo"]."', key_invoiceref = '".$response2["fkey"]."', status_invoice_ref = '2'  where id = '".$_POST["id"]."' and deleted = '0'";
			$db->query($q);
		}else{
			$q = "update aos_invoices set key_invoiceref = '".$response2["fkey"]."', status_invoice_ref = '1'  where id = '".$_POST["id"]."' and deleted = '0'";
			$db->query($q);
		}
		echo $url;
	}else{
		echo "xuất hóa đơn thất bại";
		
	}
}else{
	echo $inv_mod_strings["LBL_EXPORT_INVOICE_PERMISSION"];
	
}



