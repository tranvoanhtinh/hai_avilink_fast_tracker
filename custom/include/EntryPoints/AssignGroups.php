<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}


class AssignGroups
{
    public function popup_select(&$bean, $event, $arguments)
    {
        global $sugar_config;

        //only process if action is Save (meaning a user has triggered this event and not the portal or automated process)
        if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'Save'
        && isset($sugar_config['securitysuite_popup_select']) && $sugar_config['securitysuite_popup_select'] == true
        && empty($bean->fetched_row['id']) && $bean->module_dir != "Users" && $bean->module_dir != "SugarFeed") {
            //Upload an attachment to an Email Template and save. If user with multi groups - popup select option
            //it will redirect to notes instead of EmailTemplate and relationship will fail...check below to avoid
            if (!empty($_REQUEST['module']) && $_REQUEST['module'] != $bean->module_dir) {
                return;
            }

            if (!empty($_REQUEST['securitygroup_list'])) {
                require_once('modules/SecurityGroups/SecurityGroup.php');
                $groupFocus = new SecurityGroup();
                $security_modules = $groupFocus->getSecurityModules();
                //sanity check
                if (in_array($bean->module_dir, array_keys($security_modules))) {
                    //add each group in securitygroup_list to new record
                    $rel_name = $groupFocus->getLinkName($bean->module_dir, "SecurityGroups");

                    $bean->load_relationship($rel_name);
                    foreach ($_REQUEST['securitygroup_list'] as $group_id) {
                        $bean->$rel_name->add($group_id);
                    }
                }
            } elseif (!empty($_REQUEST['dup_checked'])) {
                //well...ShowDuplicates doesn't pass through request vars unless they are defined in the module vardefs
                //so we are screwed here...
                global $current_language;
                $ss_mod_strings = return_module_language($current_language, 'SecurityGroups');
                unset($_SESSION['securitysuite_error']); //to be safe
                $_SESSION['securitysuite_error'] = $ss_mod_strings['LBL_ERROR_DUPLICATE'];
            }
        } elseif (isset($sugar_config['securitysuite_user_popup']) && $sugar_config['securitysuite_user_popup'] == true
        && empty($bean->fetched_row['id']) && $bean->module_dir == "Users"
        && isset($_REQUEST['action']) && $_REQUEST['action'] != 'SaveSignature') { //Bug: 589

            //$_REQUEST['return_module'] = $bean->module_dir;
            //$_REQUEST['return_action'] = "DetailView";
            //$_REQUEST['return_id'] = $bean->id;

            //$_SESSION['securitygroups_popup_'.$bean->module_dir] = $bean->id;

            if (!isset($_SESSION['securitygroups_popup'])) {
                $_SESSION['securitygroups_popup'] = array();
            }
            $_SESSION['securitygroups_popup'][] = array(
            'module' => $bean->module_dir,
            'id' => $bean->id
        );
        }
    }


    public function popup_onload($event, $arguments)
    {
        if (!empty($_REQUEST['to_pdf']) || !empty($_REQUEST['sugar_body_only'])) {
            return;
        }

        /** //test user popup
        	//always have this loaded
        	echo '<script type="text/javascript" src="modules/SecurityGroups/javascript/popup_relate.js"></script>';
        */
        global $sugar_config;

        $action = null;
        if (isset($_REQUEST['action'])) {
            $action = $_REQUEST['action'];
        } else {
            LoggerManager::getLogger()->warn('Not defined action in request');
        }

        $module = null;
        if (isset($_REQUEST['module'])) {
            $module = $_REQUEST['module'];
        } else {
            LoggerManager::getLogger()->warn('Not defined module in request');
        }


        if (isset($action) && ($action == "Save" || $action == "SetTimezone")) {
            return;
        }

        if ((
            //(isset($sugar_config['securitysuite_popup_select']) && $sugar_config['securitysuite_popup_select'] == true)
            //||
            ($module == "Users" && isset($sugar_config['securitysuite_user_popup']) && $sugar_config['securitysuite_user_popup'] == true)
        )

        //&& isset($_SESSION['securitygroups_popup_'.$module]) && !empty($_SESSION['securitygroups_popup_'.$module])
        && !empty($_SESSION['securitygroups_popup'])
    ) {
            foreach ($_SESSION['securitygroups_popup'] as $popup_index => $popup) {
                $record_id = $popup['id'];
                $module = $popup['module'];
                unset($_SESSION['securitygroups_popup'][$popup_index]);

                require_once('modules/SecurityGroups/SecurityGroup.php');
                $groupFocus = new SecurityGroup();
                if ($module == 'Users') {
                    $rel_name = "SecurityGroups";
                } else {
                    $rel_name = $groupFocus->getLinkName($module, "SecurityGroups");
                }

                //this only works if on the detail view of the record actually saved...
                //so ajaxui breaks this as it stays on the parent
                $auto_popup = <<<EOQ
<script type="text/javascript" language="javascript">
	open_popup("SecurityGroups",600,400,"",true,true,{"call_back_function":"securitysuite_set_return_and_save_background","form_name":"DetailView","field_to_name_array":{"id":"subpanel_id"},"passthru_data":{"module":"$module","record":"$record_id","child_field":"$rel_name","return_url":"","link_field_name":"$rel_name","module_name":"$rel_name","refresh_page":"1"}},"MultiSelect",true);
</script>

EOQ;

                echo $auto_popup;
            }
            unset($_SESSION['securitygroups_popup']);
        }
    }
	
    public function mass_assign($event, $arguments)
    {
        $action = null;
        if (isset($_REQUEST['action'])) {
            $action = $_REQUEST['action'];
        } else {
            LoggerManager::getLogger()->warn('Not defined action in request');
        }

        $module = null;
        if (isset($_REQUEST['module'])) {
            $module = $_REQUEST['module'];
        } else {
            LoggerManager::getLogger()->warn('Not defined module in request');
        }

  
        $no_mass_assign_list = array("Emails"=>"Emails","ACLRoles"=>"ACLRoles"); //,"Users"=>"Users");
        //check if security suite enabled
        $action = strtolower($action);
        if (isset($module) && ($action == "list" || $action == "index" || $action == "listview")
        && (!isset($_REQUEST['search_form_only']) || $_REQUEST['search_form_only'] != true)
        && !array_key_exists($module, $no_mass_assign_list)
        ) {
            global $current_user;
            if (is_admin($current_user) || ACLAction::getUserAccessLevel($current_user->id, "SecurityGroups", 'access') == ACL_ALLOW_ENABLED) {
                require_once('modules/SecurityGroups/SecurityGroup.php');
                $groupFocus = new SecurityGroup();
                $security_modules = $groupFocus->getSecurityModules();
                //if(in_array($module,$security_modules)) {
                if (in_array($module, array_keys($security_modules))) {
                    global $app_strings;

                    global $current_language;
                    $current_module_strings = return_module_language($current_language, 'SecurityGroups');

                    $form_header = get_form_header($current_module_strings['LBL_MASS_ASSIGN'], '', false);

                    $groups = $groupFocus->get_list("name", "", 0, -99, -99);
                    $options = array(""=>"");
                    foreach ($groups['list'] as $group) {
                        $options[$group->id] = $group->name;
                    }
                    $group_options =  get_select_options_with_id($options, "");

                    $mass_assign = <<<EOQ

<script type="text/javascript">
	var time_reg_format = '([0-9]{1,2}):([0-9]{1,2})';
	var date_reg_format = '([0-9]{1,2})/([0-9]{1,2})/([0-9]{4})';
	var date_reg_positions = { 'd': 1,'m': 2,'Y': 3 };
	var time_separator = ':';
	var cal_date_format = '%d/%m/%Y';
	var time_offset = 25200;
	var num_grp_sep = ',';
	var dec_sep = '.';
</script>


<script type="text/javascript" language="javascript">
function confirm_massassign(del,start_string, end_string) {
	if (del == 1) {
		return confirm( start_string + sugarListView.get_num_selected()  + end_string);
	}
	else {
		return confirm( start_string + sugarListView.get_num_selected()  + end_string);
	}
}
function convertSugarDateFormatToDatePickerFormat() {
    var date_format = 'dd/mm/yy';
    if (typeof cal_date_format != "undefined") {

        var re = new RegExp('%', 'g');
        date_format = cal_date_format;
        date_format = date_format.replace(re, '').replace('m', 'mm').replace('Y', 'yy').replace('d', 'dd');
    }
    return date_format;
}
function changeCurrency(number){
	var textEle=number;
		 var str='';
		 var i=eval(textEle.length-3);
		 var st;
		 var j;
		 var text_val;
		 var cusText;
		 cusText='';
		 str='';
		 for(var u=eval(textEle.length-1);u>=0;u--)
		 {
			str=textEle[u]+str;
			if(u == i && u > 0){
			str=","+str;	
			i=eval(i-3);
			}
		 }
		 return str;
		 
}
function loadProvince(){
	$.ajax({
                type: "POST",
                url: "index.php?entryPoint=loadProvince",
                
                dataType: "json",
                success: function(data) {
					document.getElementById("provincecode_c").value = data ;
					loadGroup();
					
                },
                error: function (error) {
                    
                }
            });
}

function showHide(key){
	var users=document.getElementsByClassName(key+"-user");
	for(var i=0;i<users.length;i++){
		if(users[i].style.display == "none"){
			users[i].style.display = "table-row";
			$("#oper-"+key).html("-");
		}else{
			users[i].style.display = "none";
			$("#oper-"+key).html("+");
		}
	}

}
function showHide2(i){
	var groups=document.getElementsByClassName(i+"-group");
	for(var j=0;j<groups.length;j++){
		if(groups[j].style.display == "none"){
			groups[j].style.display = "table-row";
			$("#oper-"+i).html("-");
			
		}else{
			groups[j].style.display = "none";
			$("#oper-"+i).html("+");
			var users=document.getElementsByClassName(i+(groups[j].id).replace(/\s/g, '')+"-user");
			for(var k=0;k<users.length;k++){
				if(users[k].style.display != "none"){
					users[k].style.display = "none";
					$("#oper-"+i+(groups[j].id).replace(/\s/g, '')).html("+");
				}
			}
		}
	}
}
function periodChange() {
    showHideDateFields($('#period').val());
}
function showHideDateFields(type) {
	if(type != "is_between"){
						  $("#date_from").datepicker("destroy");  
						   $("#date_to").datepicker("destroy"); 
						 document.getElementById('date_from').readOnly = true;
						  document.getElementById('date_to').readOnly = true;
					}else{
						$( "#date_to, #date_from").datepicker();
						$('.ui-datepicker-trigger').html('<div class="suitepicon suitepicon-module-calendar"></div>').addClass('button');
						var neww = document.getElementsByClassName('datepicker-trigger');
						var datepicker=document.getElementsByClassName('ui-datepicker-trigger');
						if(datepicker != null && datepicker != undefined){
							neww[0].appendChild(datepicker[0]);
							neww[1].appendChild(datepicker[1]);
						}
					}					
    $.ajax({
                type: "POST",
                url: "index.php?entryPoint=getDate",
                data: {
                    time:type
                },
                dataType: "json",
                success: function(data) {
				$("#date_from").val(data.from);
				$("#date_to").val(data.to);
										
                },
                error: function (error) {
                    
                }
            });
}

function send_massassign(mode, no_record_txt, start_string, end_string, del) {

	if(!sugarListView.confirm_action(del, start_string, end_string))
		return false;

	if(document.MassAssign_SecurityGroups.massassign_group.selectedIndex == 0) {
		alert("${current_module_strings['LBL_SELECT_GROUP_ERROR']}");
		return false;	
	}
	 
	if (document.MassUpdate.select_entire_list &&
		document.MassUpdate.select_entire_list.value == 1)
		mode = 'entire';
	else if (document.MassUpdate.massall.checked == true)
		mode = 'page';
	else
		mode = 'selected';

	var ar = new Array();
	if(del == 1) {
		var deleteInput = document.createElement('input');
		deleteInput.name = 'Delete';
		deleteInput.type = 'hidden';
		deleteInput.value = true;
		document.MassAssign_SecurityGroups.appendChild(deleteInput);
	}

	switch(mode) {
		case 'page':
			document.MassAssign_SecurityGroups.uid.value = '';
			for(wp = 0; wp < document.MassUpdate.elements.length; wp++) {
				if(typeof document.MassUpdate.elements[wp].name != 'undefined'
					&& document.MassUpdate.elements[wp].name == 'mass[]' && document.MassUpdate.elements[wp].checked) {
							ar.push(document.MassUpdate.elements[wp].value);
				}
			}
			document.MassAssign_SecurityGroups.uid.value = ar.join(',');
			if(document.MassAssign_SecurityGroups.uid.value == '') {
				alert(no_record_txt);
				return false;
			}
			break;
		case 'selected':
			for(wp = 0; wp < document.MassUpdate.elements.length; wp++) {
				if(typeof document.MassUpdate.elements[wp].name != 'undefined'
					&& document.MassUpdate.elements[wp].name == 'mass[]'
						&& document.MassUpdate.elements[wp].checked) {
							ar.push(document.MassUpdate.elements[wp].value);
				}
			}
			if(document.MassAssign_SecurityGroups.uid.value != '') document.MassAssign_SecurityGroups.uid.value += ',';
			document.MassAssign_SecurityGroups.uid.value += ar.join(',');
			if(document.MassAssign_SecurityGroups.uid.value == '') {
				alert(no_record_txt);
				return false;
			}
			break;
		case 'entire':
			var entireInput = document.createElement('input');
			entireInput.name = 'entire';
			entireInput.type = 'hidden';
			entireInput.value = 'index';
			document.MassAssign_SecurityGroups.appendChild(entireInput);
			//confirm(no_record_txt);
			break;
	}

	document.MassAssign_SecurityGroups.submit();
	return false;
}
function GetURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}
function openAll(){
	var checkAll=document.getElementById('checkAll'); 
	for(var i=0;i<98;i++){
		var groups=document.getElementsByClassName(i+"-group");
		if(groups !=null && groups != undefined){
			for(var j=0;j<groups.length;j++){
				if(checkAll.checked == true){
					groups[j].style.display = "table-row";
					$("#oper-"+i).html("-");
					var users=document.getElementsByClassName(i+((groups[j].id).replace(/\s/g, ''))+"-user");
					for(var k=0;k<users.length;k++){
						users[k].style.display = "table-row";
						$("#oper-"+i+((groups[j].id).replace(/\s/g, ''))).html("-");
					}
				}
				else
				{
					groups[j].style.display = "none";
					$("#oper-"+i).html("+");
					var users=document.getElementsByClassName(i+((groups[j].id).replace(/\s/g, ''))+"-user");
					for(var k=0;k<users.length;k++){
							users[k].style.display = "none";
							$("#oper-"+i+(groups[j].id).replace(/\s/g, '')).html("+");
					}
				}
			}
		
		}
	
	}
}
function fnExcelReport()
{
    var tab_text="<table border='2px'><tr bgcolor='#87AFC6'>";
    var textRange; var j=0;
    tab = document.getElementById('reportTable'); 

    for(j = 0 ; j < tab.rows.length ; j++) 
    {     
        tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
        //tab_text=tab_text+"</tr>";
    }
    tab_text = '<h2 class="module-title-text" style="font-weight: bold; text-align: center;">Báo Cáo Tổng Hợp Khách Hàng</h2>'+tab_text;
    tab_text=tab_text+"</table>";
    tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
    tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
    tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE "); 

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
    {
        txtArea1.document.open("txt/html","replace");
        txtArea1.document.write(tab_text);
        txtArea1.document.close();
        txtArea1.focus(); 
        sa=txtArea1.document.execCommand("SaveAs",true,"báo cáo tổng hợp khách hàng.xls");
         return (sa);
    }  
    else                 //other browser not tested on IE 11
       {
       	 var element = document.createElement('a');
            element.setAttribute('href', 'data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));
            element.setAttribute('download', "báo cáo tổng hợp khách hàng");
            element.style.display = 'none';
            document.body.appendChild(element);
            element.click();
            document.body.removeChild(element);
       }

   
}

function dataFilter(){
	 SUGAR.ajaxUI.showLoadingPanel();
	var province=$("#provincecode_c").val();
	var groupUsers=$("#group-users").val();
	var users=$("#users").val();
	var titleUsers=$("#title-users").val();
	var period=$("#period").val();
	var date_from = $('#date_from').val();
    var date_to = $('#date_to').val();
	var amountNew=0;
	var amountInProcess=0;
	var amountConverted=0;
	var amountTransaction=0;
	var amountDead=0;
	var amountWon=0;
	var amountLost=0;
	var amountOther=0;
	var amountCall=0;
	var amountMeeting=0;
	var amountEmail=0;
	var SumCustomer=0;
	var SumNew=0;
	var SumInpro=0;
	var SumConverted=0;
	var SumDead=0;
	var SumTransaction=0;
	var SumWon=0;
	var SumLost=0;
	var SumOther=0;
	var SumAmount=0;
	var aSum=[];
	var aSum2=[];
	$.ajax({
                type: "POST",
                url: "index.php?entryPoint=dataFilter",
                data: {
                    province:province,
					groupUsers:groupUsers,
					users:users,
					titleUsers:titleUsers,
					period:period,
					date_from:date_from,
					date_to:date_to
                },
                success: function(data) {
				if(data != null && data !=''){
					var Obj=JSON.parse(data);
					var html='';
					html+='<div style="padding-bottom:5px;padding-top:6px;width:100%;">';
					html+='<input style="margin-top:-6px;" type="checkbox" id="checkAll" onclick="openAll()">';
					html+='<span style="margin-left:5px;display:inline-block;">Open All</span>';
					html+='</div>';
					html+='<table id="reportTable" style="width:100%;">';
					html+='<tr>';
					html+='<td rowspan="5">STT</td>';
					html+='<td style="width:200px;text-align:center;font-weight:bold" rowspan="2">NHÓM BÁN HÀNG</td>';
					html+='<td style="text-align:center;font-weight:bold;font-size:16px" colspan="5">KHÁCH HÀNG</td>';
					html+='<td style="text-align:center;font-weight:bold;font-size:16px" colspan="3">DOANH SỐ</td>';
					html+='<td style="text-align:center;font-weight:bold;font-size:16px" colspan="3">HOẠT ĐỘNG</td>';
					html+='</tr>';
			html+='<tr style="background-color:#A9D0F5;font-weight:bold;text-align:center;font-size:16px;">';
				html+='<td>KHTN Mới</td>';
				html+='<td>Đang liên hệ</td>';
				html+='<td>Đã chuyển đổi</td>';
				html+='<td>Ngừng chăm sóc</td>';
				html+='<td>Phát sinh giao dịch</td>';
				html+='<td>Chốt thắng</td>';
				html+='<td>Chốt thua</td>';
				html+='<td>Khác</td>';
				html+='<td>Gọi điện</td>';
				html+='<td>Gặp mặt</td>';
				html+='<td>Gửi email</td>';
			html+='</tr>';
			html+='<tr style="font-weight:bold;text-align:center;font-size:16px;">';
			html+='<td style="background-color:#A9D0F5">Doanh số dự kiến</td>';
			html+='<td id="amountNew"></td>';
			html+='<td><span id="amountInProcess"></span></td>';
			html+='<td><span id="amountConverted"></span></td>';
			html+='<td><span id="amountDead"></span></td>';
			html+='<td><span id="amountTransaction"></span></td>';
			html+='<td><span id="amountWon"></span></td>';
			html+='<td><span id="amountLost"></span></td>';
			html+='<td><span id="amountOther"></span></td>';
			html+='<td id="amountCall" rowspan="3"></td>';
			html+='<td id="amountMeeting" rowspan="3"></td>';
			html+='<td id="amountEmail" rowspan="3"></td>';
			html+='</tr>';
			html+='<tr>';
			html+='<td></td>';
			html+='<td style="text-align:center;font-weight:bold;background-color:#E0F2F7"" id="SumCustomer" colspan="5"></td>';
			
			html+='<td style="text-align:center;font-weight:bold;background-color:#E0F2F7"" id="SumAmount" colspan="3"></td>';
			html+='</tr>';
			html+='<tr>';
			html+='<td></td>';
			html+='<td style="text-align:center;font-weight:bold" id="SumNew"></td>';
			html+='<td style="text-align:center;font-weight:bold" id="SumInpro"></td>';
			html+='<td style="text-align:center;font-weight:bold" id="SumConverted"></td>';
			html+='<td style="text-align:center;font-weight:bold" id="SumDead"></td>';
			html+='<td style="text-align:center;font-weight:bold" id="SumTransaction"></td>';
			html+='<td style="text-align:center;font-weight:bold" id="SumWon"></td>';
			html+='<td style="text-align:center;font-weight:bold" id="SumLost"></td>';
			html+='<td style="text-align:center;font-weight:bold" id="SumOther"></td>';
			html+='</tr>';
			html+='<tr>';
			html+='<td style="border-right-style:none"></td>';
			html+='<td style="border-left-style:none"></td>';
			html+='<td style="text-align:center;font-weight:bold;background-color:#E0F2F7" id="AllSumN" colspan="5"></td>';
			html+='<td style="text-align:center;font-weight:bold;background-color:#E0F2F7" id="AllSumW"colspan="3"></td>';
			html+='<td style="text-align:center;font-weight:bold;background-color:#E0F2F7" id="AllSumC" colspan="3"></td>';
			html+='</tr>';
		
					for(let i=0;i<Obj.length;i++){
						html+='<tr onclick=\"showHide2(\''+i+'\')\" style="background-color:#A9F5A9;font-weight:bold">';
						html+='<td rowspan="2">'+eval(i+1)+'</td>';
								html+='<td rowspan="2" style="font-weight:bold"><span id="oper-'+i+'">+</span>'+Obj[i].province+'</td>';
								html+='<td style="color:red;text-align:center">'+Obj[i].New+'</td>';
								html+='<td style="color:red;text-align:center">'+Obj[i].InProcess+'</td>';
								html+='<td style="color:red;text-align:center">'+Obj[i].Converted+'</td>';
								html+='<td style="color:red;text-align:center">'+Obj[i].Dead+'</td>';
								html+='<td style="color:red;text-align:center">'+Obj[i].Transaction+'</td>';
								html+='<td style="color:red;text-align:center">'+Obj[i].CloseWon+'</td>';
								html+='<td style="color:red;text-align:center">'+Obj[i].CloseLost+'</td>';
								html+='<td style="color:red;text-align:center">'+Obj[i].Other+'</td>';
								html+='<td rowspan="2" style="color:red;text-align:center">'+Obj[i].amountCall+'</td>';
								html+='<td rowspan="2" style="color:red;text-align:center">'+Obj[i].amountMeeting+'</td>';
								html+='<td rowspan="2" style="color:red;text-align:center">'+Obj[i].amountEmail+'</td>';
								html+='</tr>';
						html+='<tr onclick=\"showHide2(\''+i+'\')\" style="background-color:#A9F5A9;font-weight:bold">';
						
								html+='<td style="color:red;text-align:center">'+changeCurrency(String(Obj[i].amountNew))+'</td>';
								html+='<td style="color:red;text-align:center">'+changeCurrency(String(Obj[i].amountInProcess))+'</td>';
								html+='<td style="color:red;text-align:center">'+changeCurrency(String(Obj[i].amountConverted))+'</td>';
								html+='<td style="color:red;text-align:center">'+changeCurrency(String(Obj[i].amountDead))+'</td>';
								html+='<td style="color:red;text-align:center">'+changeCurrency(String(Obj[i].amountTransaction))+'</td>';
								html+='<td style="color:red;text-align:center">'+changeCurrency(String(Obj[i].amountWon))+'</td>';
								html+='<td style="color:red;text-align:center">'+changeCurrency(String(Obj[i].amountLost))+'</td>';
								html+='<td style="color:red;text-align:center">'+changeCurrency(String(Obj[i].amountOther))+'</td>';
								
								html+='</tr>';
								var  data = {};
								Object.keys(Obj[i].data).sort().forEach(function(key) {
									data[key] = Obj[i].data[key];
								});
							for (var key in data) {
								var New=0;
								var inProcess=0;
								var converted=0;
								var dead=0;
								var transaction=0;
								var closedWon=0;
								var closedLost=0;
								var other=0;
								var call=0;
								var meeting=0;
								var email=0;
								var amountNewG=0;
								var amountInproG=0;
								var amountConvertedG=0;
								var amountDeadG=0;
								var amountTransactionG=0;
								var amountClosedWonG=0;
								var amountClosedLostG=0;
								var amountOtherG=0;
								html+='<tr style="font-weight:bold;display:none;background-color:#F5D0A9" class="'+i+'-group" onclick=\"showHide(\''+i+key.replace(/\s/g, '')+'\')\" id="'+key.replace(/\s/g, '')+'">';
								html+='<td style="border-bottom-style:none"></td>';
								html+='<td rowspan="2" style="background-color:#F5D0A9"><span id="oper-'+i+key.replace(/\s/g, '')+'" style="text-align:left">+</span><span style="margin-left:3%">'+key+'</span></td>';
								html+='<td style="text-align:center" id="New"></td>';
								html+='<td style="text-align:center" id="InProcess"></td>';
								html+='<td style="text-align:center" id="Converted"></td>';
								html+='<td style="text-align:center" id="Dead"></td>';
								html+='<td style="text-align:center" id="Transaction"></td>';
								html+='<td style="text-align:center" id="ClosedWon"></td>';
								html+='<td style="text-align:center" id="ClosedLost"></td>';
								html+='<td style="text-align:center" id="Other"></td>';
								html+='<td rowspan="2" style="text-align:center" id="Call"></td>';
								html+='<td rowspan="2" style="text-align:center" id="Meeting"></td>';
								html+='<td rowspan="2" style="text-align:center" id="Email"></td>';
								
								html+='</tr>';
								html+='<tr style="font-weight:bold;display:none;color:red;background-color:#F5D0A9" class="'+i+'-group" onclick=\"showHide(\''+i+key.replace(/\s/g, '')+'\')\" id="'+key.replace(/\s/g, '')+'-2">';
								html+='<td style="border-top-style:none"></td>';
								
								html+='<td style="text-align:center" id="New"></td>';
								html+='<td style="text-align:center" id="InProcess"></td>';
								html+='<td style="text-align:center" id="Converted"></td>';
								html+='<td style="text-align:center" id="Dead"></td>';
								html+='<td style="text-align:center" id="Transaction"></td>';
								html+='<td style="text-align:center" id="ClosedWon"></td>';
								html+='<td style="text-align:center" id="ClosedLost"></td>';
								html+='<td style="text-align:center" id="Other"></td>';
								
								html+='</tr>';
							for(let j=0;j<data[key].length;j++){
								html+='<tr style="display:none" class="'+i+key.replace(/\s/g, '')+'-user">';
								html+='<td></td>';
								html+='<td style="text-align:right;margin-right:3px;font-weight:bold"><span style="margin-right:3px;font-weight:bold">'+data[key][j].user+'</span></td>';
								html+='<td style="text-align:right"><span style="margin-right:3px;font-weight:bold">'+data[key][j].New+'</span></td>';
								html+='<td style="text-align:right"><span style="margin-right:3px;font-weight:bold">'+data[key][j].InProcess+'</span></td>';
								html+='<td style="text-align:right"><span style="margin-right:3px;font-weight:bold">'+data[key][j].Converted+'</span></td>';
								html+='<td style="text-align:right"><span style="margin-right:3px;font-weight:bold">'+data[key][j].Dead+'</span></td>';
								html+='<td style="text-align:right"><span style="margin-right:3px;font-weight:bold">'+data[key][j].Transaction+'</span></td>';
								html+='<td style="text-align:right"><span style="margin-right:3px;font-weight:bold">'+data[key][j].ClosedWon+'</span></td>';
								html+='<td style="text-align:right"><span style="margin-right:3px;font-weight:bold">'+data[key][j].ClosedLost+'</span></td>';
								html+='<td style="text-align:right"><span style="margin-right:3px;font-weight:bold">'+data[key][j].Other+'</span></td>';
								html+='<td style="text-align:right"><span style="margin-right:3px;font-weight:bold">'+data[key][j].Call+'</span></td>';
								html+='<td style="text-align:right"><span style="margin-right:3px;font-weight:bold">'+data[key][j].Meeting+'</span></td>';
								html+='<td style="text-align:right"><span style="margin-right:3px;font-weight:bold">'+data[key][j].Email+'</span></td>';
								html+='</tr>';
								New=eval(New+parseInt(data[key][j].New));
								inProcess=eval(inProcess+parseInt(data[key][j].InProcess));
								converted=eval(converted+parseInt(data[key][j].Converted));
								dead=eval(dead+parseInt(data[key][j].Dead));
								transaction=eval(transaction+parseInt(data[key][j].Transaction));
								closedWon=eval(closedWon+parseInt(data[key][j].ClosedWon));
								closedLost=eval(closedLost+parseInt(data[key][j].ClosedLost));
								other=eval(other+parseInt(data[key][j].Other));
								call=eval(call+parseInt(data[key][j].Call));
								meeting=eval(meeting+parseInt(data[key][j].Meeting));
								email=eval(email+parseInt(data[key][j].Email));
								amountNewG=eval(amountNewG+parseInt(data[key][j].userAmountNew));
								amountInproG=eval(amountInproG+parseInt(data[key][j].userAmountInprocess));
								amountConvertedG=eval(amountConvertedG+parseInt(data[key][j].userAmountConverted));
								amountDeadG=eval(amountDeadG+parseInt(data[key][j].userAmountDead));
								amountTransactionG=eval(amountTransactionG+parseInt(data[key][j].userAmountTransaction));
								amountClosedWonG=eval(amountClosedWonG+parseInt(data[key][j].userAmountClosedWon));
								amountClosedLostG=eval(amountClosedLostG+parseInt(data[key][j].userAmountClosedLost));
								amountOtherG=eval(amountOtherG+parseInt(data[key][j].userAmountOther));
								
							}	
								aSum.push({
									group:key.replace(/\s/g, ''),
									NewC:New,
									inProcessC:inProcess,
									convertedC:converted,
									deadC:dead,
									transactionC:transaction,
									closedWonC:closedWon,
									closedLostC:closedLost,
									otherC:other,
									callC:call,
									meetingC:meeting,
									emailC:email
								});
								
								aSum2.push({
									group:key.replace(/\s/g, '')+'-2',
									amountNewC:amountNewG,
									amountInproC:amountInproG,
									amountConvertedC:amountConvertedG,
									amountDeadC:amountDeadG,
									amountTransactionC:amountTransactionG,
									amountClosedWonC:amountClosedWonG,
									amountClosedLostC:amountClosedLostG,
									amountOtherC:amountOtherG
									
								});
								
							}
							
							amountNew=eval(amountNew+Obj[i].amountNew);
							amountInProcess=eval(amountInProcess+Obj[i].amountInProcess);
							amountConverted=eval(amountConverted+Obj[i].amountConverted);
							amountDead=eval(amountDead+Obj[i].amountDead);
							amountTransaction=eval(amountTransaction+Obj[i].amountTransaction);
							amountWon=eval(amountWon+Obj[i].amountWon);
							amountLost=eval(amountLost+Obj[i].amountLost);
							amountOther=eval(amountOther+Obj[i].amountOther);
							amountCall=eval(amountCall+Obj[i].amountCall);
							amountMeeting=eval(amountMeeting+Obj[i].amountMeeting);
							amountEmail=eval(amountEmail+Obj[i].amountEmail);
							SumNew=eval(SumNew + Obj[i].New);
							SumInpro=eval(SumInpro + Obj[i].InProcess);
							SumConverted=eval(SumConverted + Obj[i].Converted);
							SumDead=eval(SumDead + Obj[i].Dead);
							SumTransaction=eval(SumTransaction + Obj[i].Transaction);
							SumWon=eval(SumWon + Obj[i].CloseWon);
							SumLost=eval(SumLost + Obj[i].CloseLost);
							SumOther=eval(SumOther + Obj[i].Other);
							
					}
					html+='</table>';
						$("#father-group").html(html);
						for(let f=0;f<aSum.length;f++){
								var a=document.getElementById(aSum[f].group);
								var tdList=a.getElementsByTagName('td');
								for(let i=2; i< tdList.length; i++){ 
									if(i == 2){
									tdList[i].innerHTML = aSum[f].NewC;
									}
									else if(i==3)
									{
									tdList[i].innerHTML = aSum[f].inProcessC;
									}else if(i==4)
									{
									tdList[i].innerHTML = aSum[f].convertedC;	
									}
									else if(i==5)
									{
									tdList[i].innerHTML = aSum[f].deadC;	
									}else if(i==6)
									{
									tdList[i].innerHTML = aSum[f].transactionC;	
									}else if(i==7)
									{
									tdList[i].innerHTML = aSum[f].closedWonC;	
									}else if(i==8)
									{
									tdList[i].innerHTML = aSum[f].closedLostC;	
									}else if(i==9)
									{
									tdList[i].innerHTML = aSum[f].otherC;	
									}else if(i==10)
									{
									tdList[i].innerHTML = aSum[f].callC;	
									}else if(i==11)
									{
									tdList[i].innerHTML = aSum[f].meetingC;	
									}else if(i==12)
									{
									tdList[i].innerHTML = aSum[f].emailC;	
									}
							}
							
						}
						for(let v=0;v<aSum2.length;v++){
								var a=document.getElementById(aSum2[v].group);
								var tdList=a.getElementsByTagName('td');
								for(let i=1; i< tdList.length; i++){ 
									if(i == 1){
									tdList[i].innerHTML = changeCurrency(String(aSum2[v].amountNewC));
									}
									else if(i==2)
									{
									tdList[i].innerHTML = changeCurrency(String(aSum2[v].amountInproC));
									}else if(i==3)
									{
									tdList[i].innerHTML = changeCurrency(String(aSum2[v].amountConvertedC));	
									}
									else if(i==4)
									{
									tdList[i].innerHTML = changeCurrency(String(aSum2[v].amountDeadC));	
									}else if(i==5)
									{
									tdList[i].innerHTML = changeCurrency(String(aSum2[v].amountTransactionC));
									}else if(i==6)
									{
									tdList[i].innerHTML = changeCurrency(String(aSum2[v].amountClosedWonC));	
									}else if(i==7)
									{
									tdList[i].innerHTML = changeCurrency(String(aSum2[v].amountClosedLostC));	
									}else if(i==8)
									{
									tdList[i].innerHTML = changeCurrency(String(aSum2[v].amountOtherC));	
									}
							}
							
						}
						document.getElementById("amountNew").innerHTML=changeCurrency(String(amountNew));
						document.getElementById("amountInProcess").innerHTML=changeCurrency(String(amountInProcess));
						document.getElementById("amountConverted").innerHTML=changeCurrency(String(amountConverted));
						document.getElementById("amountDead").innerHTML=changeCurrency(String(amountDead));
						document.getElementById("amountTransaction").innerHTML=changeCurrency(String(amountTransaction));
						document.getElementById("amountWon").innerHTML=changeCurrency(String(amountWon));
						document.getElementById("amountLost").innerHTML=changeCurrency(String(amountLost));
						document.getElementById("amountOther").innerHTML=changeCurrency(String(amountOther));
						document.getElementById("amountCall").innerHTML=amountCall;
						document.getElementById("amountMeeting").innerHTML=amountMeeting;
						document.getElementById("amountEmail").innerHTML=amountEmail;
						SumCustomer=eval(amountNew + amountInProcess + amountConverted + amountDead + amountTransaction);
						SumAmount=eval(amountWon + amountLost + amountOther);
						document.getElementById("SumCustomer").innerHTML=changeCurrency(String(SumCustomer));
						document.getElementById("SumAmount").innerHTML=changeCurrency(String(SumAmount));
						document.getElementById("SumNew").innerHTML=SumNew;
						document.getElementById("SumInpro").innerHTML=SumInpro;
						document.getElementById("SumConverted").innerHTML=SumConverted;
						document.getElementById("SumDead").innerHTML=SumDead;
						document.getElementById("SumTransaction").innerHTML=SumTransaction;
						document.getElementById("SumWon").innerHTML=SumWon;
						document.getElementById("SumLost").innerHTML=SumLost;
						document.getElementById("SumOther").innerHTML=SumOther;
						var AllSumN=eval(SumNew+SumInpro+SumConverted+SumDead+SumTransaction);
						var AllSumW=eval(SumWon+SumLost+SumOther);
						var AllSumC=eval(amountCall+amountMeeting+amountEmail);
						document.getElementById("AllSumN").innerHTML=AllSumN;
						document.getElementById("AllSumW").innerHTML=AllSumW;
						document.getElementById("AllSumC").innerHTML=AllSumC;
						 SUGAR.ajaxUI.hideLoadingPanel();
					}else{
						var html='';
					html+='<div style="padding-bottom:5px;padding-top:6px;width:100%;">';
					html+='<input style="margin-top:-6px;" type="checkbox" id="checkAll" onclick="openAll()">';
					html+='<span style="margin-left:5px;display:inline-block;">Open All</span>';
					html+='</div>';
					html+='<table id="reportTable" style="width:100%;">';
					html+='<tr>';
					html+='<td rowspan="5">STT</td>';
					html+='<td style="text-align:center;font-weight:bold" rowspan="2">NHÓM BÁN HÀNG</td>';
					html+='<td style="text-align:center;font-weight:bold;font-size:16px" colspan="5">KHÁCH HÀNG</td>';
					html+='<td style="text-align:center;font-weight:bold;font-size:16px" colspan="3">DOANH SỐ</td>';
					html+='<td style="text-align:center;font-weight:bold;font-size:16px" colspan="3">HOẠT ĐỘNG</td>';
					html+='</tr>';
			html+='<tr style="background-color:#A9D0F5;font-weight:bold;text-align:center;font-size:16px;">';
				html+='<td>KHTN Mới</td>';
				html+='<td>Đang liên hệ</td>';
				html+='<td>Đã chuyển đổi</td>';
				html+='<td>Ngừng chăm sóc</td>';
				html+='<td>Phát sinh giao dịch</td>';
				html+='<td>Chốt thắng</td>';
				html+='<td>Chốt thua</td>';
				html+='<td>Khác</td>';
				html+='<td>Gọi điện</td>';
				html+='<td>Gặp mặt</td>';
				html+='<td>Gửi email</td>';
			html+='</tr>';
			html+='<tr style="font-weight:bold;text-align:center;font-size:16px;">';
			html+='<td style="background-color:#A9D0F5">Doanh số dự kiến</td>';
			html+='<td id="amountNew"></td>';
			html+='<td><span id="amountInProcess"></span></td>';
			html+='<td><span id="amountConverted"></span></td>';
			html+='<td><span id="amountDead"></span></td>';
			html+='<td><span id="amountTransaction"></span></td>';
			html+='<td><span id="amountWon"></span></td>';
			html+='<td><span id="amountLost"></span></td>';
			html+='<td><span id="amountOther"></span></td>';
			html+='<td id="amountCall" rowspan="3"></td>';
			html+='<td id="amountMeeting" rowspan="3"></td>';
			html+='<td id="amountEmail" rowspan="3"></td>';
			html+='</tr>';
			html+='<tr>';
			html+='<td></td>';
			html+='<td style="text-align:center;font-weight:bold" id="SumCustomer" colspan="5"></td>';
			
			html+='<td style="text-align:center;font-weight:bold" id="SumAmount" colspan="3"></td>';
			html+='</tr>';
			html+='<tr>';
			html+='<td></td>';
			html+='<td style="text-align:center;font-weight:bold" id="SumNew"></td>';
			html+='<td style="text-align:center;font-weight:bold" id="SumInpro"></td>';
			html+='<td style="text-align:center;font-weight:bold" id="SumConverted"></td>';
			html+='<td style="text-align:center;font-weight:bold" id="SumDead"></td>';
			html+='<td style="text-align:center;font-weight:bold" id="SumTransaction"></td>';
			html+='<td style="text-align:center;font-weight:bold" id="SumWon"></td>';
			html+='<td style="text-align:center;font-weight:bold" id="SumLost"></td>';
			html+='<td style="text-align:center;font-weight:bold" id="SumOther"></td>';
			html+='</tr>';
			html+='<tr>';
			html+='<td></td>';
			html+='<td></td>';
			html+='<td style="text-align:center;font-weight:bold" id="AllSumN" colspan="5"></td>';
			html+='<td style="text-align:center;font-weight:bold" id="AllSumW"colspan="3"></td>';
			html+='<td style="text-align:center;font-weight:bold" id="AllSumC" colspan="3"></td>';
			html+='</tr>';
						html+='</table>';
						$("#father-group").html(html);
						 SUGAR.ajaxUI.hideLoadingPanel();
					}
                },
                error: function (error) {
                    
                }
            });
			
	
}
function loadGroup(){
	var groupId=document.getElementById("provincecode_c").value;
	$.ajax({
                type: "POST",
                url: "index.php?entryPoint=getGroup",
                data: {
                    id:groupId
                },
                dataType: "json",
                success: function(data) {
					var obj=data;
					html="";
					html+='<div>Quận/Huyện</div>';
					html+='<input style="width:250px;" placeholder="Search.." id="myInput" onkeyup="filterKey()">';
					html+='<select style="width:250px;height:100px" name="group-users[]" id="group-users" multiple="multiple" >';
					html+='<option onclick="getUser();getTitle()" value="All">All</option>';
					for(let i=0;i<obj.length;i++){
						html+='<option onclick="getUser();getTitle()" value="'+obj[i].id+'">'+obj[i].name+'</option>';
					}
                    html+='</select>';
					$('#groupUser').html(html);
					$('#users').html('');
					
                },
                error: function (error) {
                    
                }
            });
}
function getUser(){
	var province=document.getElementById("provincecode_c").value;
	var groups=$('#group-users').val();
	$.ajax({
                type: "POST",
                url: "index.php?entryPoint=getUser",
                data: {
                    id:groups,
					province:province
                },
                dataType: "json",
                success: function(data) {
					var obj=data;
					html="";
					for(let i=0;i<obj.length;i++){
						html+='<option value="'+obj[i].id+'">'+obj[i].name+(obj[i].title?'-'+obj[i].title:'')+'</option>';
					}
                    
					$('#users').html(html);
					
                },
                error: function (error) {
                    
                }
            });
			
}
function clearUser(){
	$("#users").val('');
}

function getTitle(){
	var province=document.getElementById("provincecode_c").value;
	var groups=$('#group-users').val();
	$.ajax({
                type: "POST",
                url: "index.php?entryPoint=getTitle",
                data: {
                    id:groups,
					province:province
                },
                dataType: "json",
                success: function(data) {
					var obj=data;
					html="";
					for(let i=0;i<obj.length;i++){
					if(obj[i].title != null && obj[i].title != ''){
						html+='<option value="'+obj[i].id+'">'+obj[i].title+'</option>';
					}
					}
                    
					$('#title-users').html(html);
					
                },
                error: function (error) {
                    
                }
            });
			
}
function showProvince(){
	$.ajax({
                type: "POST",
                url: "index.php?entryPoint=showProvince",
                dataType: "json",
                success: function(data) {
					for(let i=0;i<data.length;i++){
						document.getElementById(data[i]).style.display="block";
					}
                },
                error: function (error) {
                    
                }
            });
}
var moduleN=document.getElementsByName("return_module")[0].value;
console.log(moduleN);
if(moduleN == "rp_CustomReport"){
	document.getElementById("abc").style.display="none";
	document.getElementsByClassName("listViewEmpty")[0].style.display="none";
	document.getElementById("cde").style.display="block";
	$(".module-title-text").html("Báo Cáo Tổng Hợp Khách Hàng");
	document.getElementsByClassName("module-title-text")[0].style.fontWeight="bold";
	document.getElementsByClassName("module-title-text")[0].style.marginLeft="28%";
}
</script>

<style>
#cde table, #cde th,#cde td {
  border: 1px solid black;
  border-collapse: collapse;
}
.text-center {
    text-align: center;
}
label {
    display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: bold;
}
.dashletcontainer .button, .dashletcontainer input[type="submit"], .dashletcontainer input[type="button"] {
    line-height: 100%;
}
.ui-datepicker-trigger {
    margin-left: 5px !important;
	vertical-align: middle;
}
.button {
    margin-bottom: 4px;
}
.button {
    font-size: 13px;
    font-weight: 500;
    background: #f08377;
    color: #f5f5f5;
    cursor: pointer;
    padding: 0 20px 0 20px;
    margin: 0 0 0 0;
    border: none;
    border-radius: 3px;
    letter-spacing: 1px;
    line-height: 32px;
    height: 32px;
    text-transform: uppercase;
}
.suitepicon-module-calendar{
    margin-top: 30%;
}
table{
	font-size: 14px;	
}
</style>
		<div id="abc">
		<form action='index.php' method='post' name='MassAssign_SecurityGroups'  id='MassAssign_SecurityGroups'>
			<input type='hidden' name='action' value='MassAssign' />
			<input type='hidden' name='module' value='SecurityGroups' />
			<input type='hidden' name='return_action' value='${action}' />
			<input type='hidden' name='return_module' value='${module}' />
			<textarea style='display: none' name='uid'></textarea>


		<div id='massassign_form'>$form_header
		<table cellpadding='0' cellspacing='0' border='0' width='100%'>
		<tr>
		<td style='padding-bottom: 2px;' class='listViewButtons'>
		<input type='submit' name='Assign' value='${current_module_strings['LBL_ASSIGN']}' onclick="return send_massassign('selected', '{$app_strings['LBL_LISTVIEW_NO_SELECTED']}','${current_module_strings['LBL_ASSIGN_CONFIRM']}','${current_module_strings['LBL_CONFIRM_END']}',0);" class='button'>
		<input type='submit' name='Remove' value='${current_module_strings['LBL_REMOVE']}' onclick="return send_massassign('selected', '{$app_strings['LBL_LISTVIEW_NO_SELECTED']}','${current_module_strings['LBL_REMOVE_CONFIRM']}','${current_module_strings['LBL_CONFIRM_END']}',1);" class='button'>


		</td></tr></table>
		<table cellpadding='0' cellspacing='0' border='0' width='100%' class='tabForm' id='mass_update_table'>
		<tr><td><table width='100%' border='0' cellspacing='0' cellpadding='0'>
		<tr>
		<td>${current_module_strings['LBL_GROUP']}</td>
		<td><select name='massassign_group' id="massassign_group" tabindex='1'>${group_options}</select></td>
		</tr>
		</table></td></tr></table></div>			
		</form>	
</div>		
		<div id="cde" style="display:none;margin-top:20px;">
		  <div class="row" style="margin-bottom: 10px">
							<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 text-left" >
							<div>Thời gian</div>
                                <select style="width:200px" name="period" id="period" onchange="periodChange()">
									<option value="default">------</option>
									<option selected="" value="this_week">Tuần này</option>
									<option value="last_week">Tuần trước</option>
									<option value="this_month">Tháng này</option>
									<option value="last_month">Tháng trước</option>
									<option value="this_year">Năm này</option>
									<option value="last_year">Năm trước</option>
									<option value="is_between">Khoảng thời gian</option>
                                </select>
								 <div id="date_range" class="text-center" style="margin-top:20px">
                    <div class="row" style="margin-bottom: 5px">
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-left">
                            <label>Từ</label>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 text-left">
                            <input id="date_from"  name="date_from" type="text" style="width:170px;" maxlength="100" value="" title="" tabindex="-1" accesskey="9">
                            <br><span class="error" style="display: none;">Please select a date</span>
                        </div>
						 <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 datepicker-trigger">
						
						</div>
					</div>
                    <div class="row" style="margin-bottom: 5px">
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-left">
                            <label>Đến</label>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 text-left">
                            <input type="text" style="width:170px;"  id="date_to" name="date_to" maxlength="100" value="" title="" tabindex="-1" accesskey="9">
                            <br><span class="error" style="display: none;">Please select a date</span>
                        </div>
						<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 datepicker-trigger">
						
						</div>
                    </div>
                </div>
                            </div>
							
							
                            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 text-left ">
								<div>Tỉnh/TP</div>
                                <select style="width:200px" name="group-users[]" id="provincecode_c" onchange="loadGroup()">
																		<option  value="" selected="selected">----</option>							
																		<option  label="" value="All">Chọn tất cả</option>
																		<option style="display:none" label=" TP Hà Nội" value="10" id="10"> TP Hà Nội</option>
																		<option style="display:none" label=" Tỉnh Hưng Yên" value="16" id="16"> Tỉnh Hưng Yên</option>
																		<option style="display:none" label=" Tỉnh Hải Dương" value="17" id="17"> Tỉnh Hải Dương</option>
																		<option style="display:none" label=" TP Hải Phòng " value="18" id="18"> TP Hải Phòng </option>
																		<option style="display:none" label=" Tỉnh Quảng Ninh " value="20" id="20"> Tỉnh Quảng Ninh </option>
																		<option style="display:none" label=" Tỉnh Bắc Ninh " value="22" id="22"> Tỉnh Bắc Ninh </option>
																		<option style="display:none" label=" Tỉnh Bắc Giang " value="23" id="23"> Tỉnh Bắc Giang </option>
																		<option style="display:none" label=" Tỉnh Lạng Sơn " value="24" id="24"> Tỉnh Lạng Sơn </option>
																		<option style="display:none" label=" Tỉnh Thái Nguyên " value="25" id="25"> Tỉnh Thái Nguyên </option>
																		<option style="display:none" label=" Tỉnh Bắc Kạn " value="26" id="26"> Tỉnh Bắc Kạn </option>
																		<option style="display:none" label=" Tỉnh Cao Bằng " value="27" id="27"> Tỉnh Cao Bằng </option>
																		<option style="display:none" label=" Tỉnh Vĩnh Phúc " value="28" id="28"> Tỉnh Vĩnh Phúc </option>
																		<option style="display:none" label=" Tỉnh Phú Thọ " value="29" id="29"> Tỉnh Phú Thọ </option>
																		<option style="display:none" label=" Tỉnh Tuyên Quang " value="30" id="30"> Tỉnh Tuyên Quang </option>
																		<option style="display:none" label=" Tỉnh Hà Giang " value="31" id="31"> Tỉnh Hà Giang </option>
																		<option style="display:none" label=" Tỉnh Yên Bái " value="32" id="32"> Tỉnh Yên Bái </option>
																		<option style="display:none" label=" Tỉnh Lào Cai " value="33" id="33"> Tỉnh Lào Cai </option>
																		<option style="display:none" label=" Tỉnh Hoà Bình " value="35" id="35"> Tỉnh Hoà Bình </option>
																		<option style="display:none" label=" Tỉnh Sơn La " value="36" id="36"> Tỉnh Sơn La </option>
																		<option style="display:none" label=" Tỉnh Điện Biên " value="38" id="38"> Tỉnh Điện Biên </option>
																		<option style="display:none" label=" Tỉnh Lai Châu " value="39" id="39"> Tỉnh Lai Châu </option>
																		<option style="display:none" label=" Tỉnh Hà Nam " value="40" id="40"> Tỉnh Hà Nam </option>
																		<option style="display:none" label=" Tỉnh Thái Bình " value="41" id="41"> Tỉnh Thái Bình </option>
																		<option style="display:none" label=" Tỉnh Nam Định " value="42" id="42"> Tỉnh Nam Định </option>
																		<option style="display:none" label=" Tỉnh Ninh Bình " value="43" id="43"> Tỉnh Ninh Bình </option>
																		<option style="display:none" label=" Tỉnh Thanh Hoá " value="44" id="44"> Tỉnh Thanh Hoá </option>
																		<option style="display:none" label=" Tỉnh Nghệ An " value="46" id="46"> Tỉnh Nghệ An </option>
																		<option style="display:none" label=" Tỉnh Hà Tĩnh " value="48" id="48"> Tỉnh Hà Tĩnh </option>
																		<option style="display:none" label=" Tỉnh Quảng Bình " value="51" id="51"> Tỉnh Quảng Bình </option>
																		<option style="display:none" label=" Tỉnh Quảng Trị " value="52" id="52"> Tỉnh Quảng Trị </option>
																		<option style="display:none" label=" Tỉnh Thừa Thiên Huế " value="53" id="53"> Tỉnh Thừa Thiên Huế </option>
																		<option style="display:none" label=" TP Đà Nẵng " value="55" id="55"> TP Đà Nẵng </option>
																		<option style="display:none" label=" Tỉnh Quảng Nam " value="56" id="56"> Tỉnh Quảng Nam </option>
																		<option style="display:none" label=" Tỉnh Quảng Ngãi " value="57" id="57"> Tỉnh Quảng Ngãi </option>
																		<option style="display:none" label=" Tỉnh Kon Tum " value="58" id="58"> Tỉnh Kon Tum </option>
																		<option style="display:none" label=" Tỉnh Bình Định " value="59" id="59"> Tỉnh Bình Định </option>
																		<option style="display:none" label=" Tỉnh Gia Lai " value="60" id="60"> Tỉnh Gia Lai </option>
																		<option style="display:none" label=" Tỉnh Phú Yên " value="62" id="62"> Tỉnh Phú Yên </option>
																		<option style="display:none" label=" Tỉnh Đắk Lăk " value="63" id="63"> Tỉnh Đắk Lăk </option>
																		<option style="display:none" label=" Tỉnh Đắk Nông " value="64" id="64"> Tỉnh Đắk Nông </option>
																		<option style="display:none" label=" Tỉnh Khánh Hoà " value="65" id="65"> Tỉnh Khánh Hoà </option>
																		<option style="display:none" label=" Tỉnh Ninh Thuận " value="66" id="66"> Tỉnh Ninh Thuận </option>
																		<option style="display:none" label=" Tỉnh Lâm Đồng " value="67" id="67"> Tỉnh Lâm Đồng </option>
																		<option style="display:none" label=" TP Hồ Chí Minh " value="70" id="70"> TP Hồ Chí Minh </option>
																		<option style="display:none" label=" Tỉnh Bà Rịa Vũng Tàu " value="79" id="79"> Tỉnh Bà Rịa Vũng Tàu </option>
																		<option style="display:none" label=" Tỉnh Bình Thuận " value="80" id="80"> Tỉnh Bình Thuận </option>
																		<option style="display:none" label=" Tỉnh Đồng Nai " value="81" id="81"> Tỉnh Đồng Nai </option>
																		<option style="display:none" label=" Tỉnh Bình Dương " value="82" id="82"> Tỉnh Bình Dương </option>
																		<option style="display:none" label=" Tỉnh Bình Phước " value="83" id="83"> Tỉnh Bình Phước </option>
																		<option style="display:none" label=" Tỉnh Tây Ninh " value="84" id="84"> Tỉnh Tây Ninh </option>
																		<option style="display:none" label=" Tỉnh Long An " value="85" id="85"> Tỉnh Long An </option>
																		<option style="display:none" label=" Tỉnh Tiền Giang " value="86" id="86"> Tỉnh Tiền Giang </option>
																		<option style="display:none" label=" Tỉnh Đồng Tháp " value="87" id="87"> Tỉnh Đồng Tháp </option>
																		<option style="display:none" label=" Tỉnh An Giang " value="88" id="88"> Tỉnh An Giang </option>
																		<option style="display:none" label=" Tỉnh Vĩnh Long " value="89" id="89"> Tỉnh Vĩnh Long </option>
																		<option style="display:none" label=" TP Cần Thơ " value="90" id="90"> TP Cần Thơ </option>
																		<option style="display:none" label=" Tỉnh Hậu Giang " value="91" id="91"> Tỉnh Hậu Giang </option>
																		<option style="display:none" label=" Tỉnh Kiên Giang " value="92" id="92"> Tỉnh Kiên Giang </option>
																		<option style="display:none" label=" Tỉnh Bến Tre " value="93" id="93"> Tỉnh Bến Tre </option>
																		<option style="display:none" label=" Tỉnh Trà Vinh " value="94" id="94"> Tỉnh Trà Vinh </option>
																		<option style="display:none" label=" Tỉnh Sóc Trăng " value="95" id="95"> Tỉnh Sóc Trăng </option>
																		<option style="display:none" label=" Tỉnh Bạc Liêu " value="96" id="96"> Tỉnh Bạc Liêu </option>
																		<option style="display:none" label=" Tỉnh Cà Mau " value="97" id="97"> Tỉnh Cà Mau </option>
																		<option style="display:none" label=" Vnpost " value="98" id="98"> Vnpost </option>
																		<option style="display:none" label=" PPTT " value="99" id="99"> PPTT </option>
																		<option style="display:none" label=" DVBC " value="100" id="100"> DVBC </option>
																		<option style="display:none" label=" TCBC " value="101" id="101"> TCBC </option>
																		<option style="display:none" label=" DVKH " value="102" id="102"> DVKH </option>
        
									                                </select>
                            </div>
							<div id="groupUser" class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
							<div>Quận/Huyện</div>
							<input style="width:250px;" type="text" placeholder="Search.." id="myInput" onkeyup="filterKey()">
                                <select style="width:250px;height:100px" name="group-users[]" id="group-users" multiple="multiple">
                                
									                                </select>
                            </div>
							 <div id="userTitle" class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left ">
							 <div>Chức vụ</div>
							<input style="width:250px;" type="text" placeholder="Search.." id="myInput3" onkeyup="filterKey3()">
                                <select style="width:250px;height:100px;" onclick="clearUser()" name="title-users[]" id="title-users" multiple="multiple">
                                
									                                </select>
                            </div>
							<div id="listUser" class="col-xs-12 col-sm-12 col-md-2 col-lg-2 ">
							<div>Người dùng</div>
							<input style="width:250px;" type="text" placeholder="Search.." id="myInput2" onkeyup="filterKey2()">
                                <select style="width:250px;height:100px;" name="users[]" id="users" multiple="multiple">
																		
        
									                                </select>
                            </div>
							
                </div>
               
				
			  <div class="row">
                     <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center" style="margin-left:-5px">
                        <button id="filter" class="button" onclick="moduleFilter()" style="width:130px;">Thống kê</button>
                    </div>
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center" style="margin-left:5px">
                       <input type="button" onclick="refreshFilter()" class="button" value="Xóa ĐK Lọc">
                    </div>
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center">
					<input type="button" onclick="fnExcelReport()" class="button" value="Xuất excel">
                    </div>
                </div>
          <div id="father-group" style="margin-top:5px"> 
	
		</div>
		
		</div>
		<script type="text/javascript">
		if(moduleN == "rp_CustomReport"){
        $.datepicker.setDefaults({
            showOn: "both",
            dateFormat: convertSugarDateFormatToDatePickerFormat(),
        });
       
		showProvince();
		loadProvince();
		showHideDateFields($('#period').val());
		}
		
    </script>
		
EOQ;


                    echo $mass_assign;
                }
            }else{
				$mass_assign_employee = <<<EOQ
<script type="text/javascript">
	var time_reg_format = '([0-9]{1,2}):([0-9]{1,2})';
	var date_reg_format = '([0-9]{1,2})/([0-9]{1,2})/([0-9]{4})';
	var date_reg_positions = { 'd': 1,'m': 2,'Y': 3 };
	var time_separator = ':';
	var cal_date_format = '%d/%m/%Y';
	var time_offset = 25200;
	var num_grp_sep = ',';
	var dec_sep = '.';
</script>
<script type="text/javascript" language="javascript">
function confirm_massassign(del,start_string, end_string) {
	if (del == 1) {
		return confirm( start_string + sugarListView.get_num_selected()  + end_string);
	}
	else {
		return confirm( start_string + sugarListView.get_num_selected()  + end_string);
	}
}
function convertSugarDateFormatToDatePickerFormat() {
    var date_format = 'dd/mm/yy';
    if (typeof cal_date_format != "undefined") {

        var re = new RegExp('%', 'g');
        date_format = cal_date_format;
        date_format = date_format.replace(re, '').replace('m', 'mm').replace('Y', 'yy').replace('d', 'dd');
    }
    return date_format;
}
function changeCurrency(number){
	var textEle=number;
		 var str='';
		 var i=eval(textEle.length-3);
		 var st;
		 var j;
		 var text_val;
		 var cusText;
		 cusText='';
		 str='';
		 for(var u=eval(textEle.length-1);u>=0;u--)
		 {
			str=textEle[u]+str;
			if(u == i && u > 0){
			str=","+str;	
			i=eval(i-3);
			}
		 }
		 return str;
		 
}
function loadProvince(){
	$.ajax({
                type: "POST",
                url: "index.php?entryPoint=loadProvince",
                
                dataType: "json",
                success: function(data) {
					document.getElementById("provincecode_c").value = data ;
					loadGroup();
					
                },
                error: function (error) {
                    
                }
            });
}

function showHide(key){
	var users=document.getElementsByClassName(key+"-user");
	for(var i=0;i<users.length;i++){
		if(users[i].style.display == "none"){
			users[i].style.display = "table-row";
			$("#oper-"+key).html("-");
		}else{
			users[i].style.display = "none";
			$("#oper-"+key).html("+");
		}
	}

}
function showHide2(i){
	var groups=document.getElementsByClassName(i+"-group");
	for(var j=0;j<groups.length;j++){
		if(groups[j].style.display == "none"){
			groups[j].style.display = "table-row";
			$("#oper-"+i).html("-");
			
		}else{
			groups[j].style.display = "none";
			$("#oper-"+i).html("+");
			var users=document.getElementsByClassName(i+(groups[j].id).replace(/\s/g, '')+"-user");
			for(var k=0;k<users.length;k++){
				if(users[k].style.display != "none"){
					users[k].style.display = "none";
					$("#oper-"+i+(groups[j].id).replace(/\s/g, '')).html("+");
				}
			}
		}
	}
}
function periodChange() {
    showHideDateFields($('#period').val());
}
function showHideDateFields(type) {
	if(type != "is_between"){
						  $("#date_from").datepicker("destroy");  
						   $("#date_to").datepicker("destroy"); 
						 document.getElementById('date_from').readOnly = true;
						  document.getElementById('date_to').readOnly = true;
					}else{
						$( "#date_to, #date_from").datepicker();
						$('.ui-datepicker-trigger').html('<div class="suitepicon suitepicon-module-calendar"></div>').addClass('button');
						var neww = document.getElementsByClassName('datepicker-trigger');
						var datepicker=document.getElementsByClassName('ui-datepicker-trigger');
						if(datepicker != null && datepicker != undefined){
							neww[0].appendChild(datepicker[0]);
							neww[1].appendChild(datepicker[1]);
						}
					}					
    $.ajax({
                type: "POST",
                url: "index.php?entryPoint=getDate",
                data: {
                    time:type
                },
                dataType: "json",
                success: function(data) {
				$("#date_from").val(data.from);
				$("#date_to").val(data.to);
										
                },
                error: function (error) {
                    
                }
            });
    
}

function send_massassign(mode, no_record_txt, start_string, end_string, del) {

	if(!sugarListView.confirm_action(del, start_string, end_string))
		return false;

	if(document.MassAssign_SecurityGroups.massassign_group.selectedIndex == 0) {
		alert("${current_module_strings['LBL_SELECT_GROUP_ERROR']}");
		return false;	
	}
	 
	if (document.MassUpdate.select_entire_list &&
		document.MassUpdate.select_entire_list.value == 1)
		mode = 'entire';
	else if (document.MassUpdate.massall.checked == true)
		mode = 'page';
	else
		mode = 'selected';

	var ar = new Array();
	if(del == 1) {
		var deleteInput = document.createElement('input');
		deleteInput.name = 'Delete';
		deleteInput.type = 'hidden';
		deleteInput.value = true;
		document.MassAssign_SecurityGroups.appendChild(deleteInput);
	}

	switch(mode) {
		case 'page':
			document.MassAssign_SecurityGroups.uid.value = '';
			for(wp = 0; wp < document.MassUpdate.elements.length; wp++) {
				if(typeof document.MassUpdate.elements[wp].name != 'undefined'
					&& document.MassUpdate.elements[wp].name == 'mass[]' && document.MassUpdate.elements[wp].checked) {
							ar.push(document.MassUpdate.elements[wp].value);
				}
			}
			document.MassAssign_SecurityGroups.uid.value = ar.join(',');
			if(document.MassAssign_SecurityGroups.uid.value == '') {
				alert(no_record_txt);
				return false;
			}
			break;
		case 'selected':
			for(wp = 0; wp < document.MassUpdate.elements.length; wp++) {
				if(typeof document.MassUpdate.elements[wp].name != 'undefined'
					&& document.MassUpdate.elements[wp].name == 'mass[]'
						&& document.MassUpdate.elements[wp].checked) {
							ar.push(document.MassUpdate.elements[wp].value);
				}
			}
			if(document.MassAssign_SecurityGroups.uid.value != '') document.MassAssign_SecurityGroups.uid.value += ',';
			document.MassAssign_SecurityGroups.uid.value += ar.join(',');
			if(document.MassAssign_SecurityGroups.uid.value == '') {
				alert(no_record_txt);
				return false;
			}
			break;
		case 'entire':
			var entireInput = document.createElement('input');
			entireInput.name = 'entire';
			entireInput.type = 'hidden';
			entireInput.value = 'index';
			document.MassAssign_SecurityGroups.appendChild(entireInput);
			//confirm(no_record_txt);
			break;
	}

	document.MassAssign_SecurityGroups.submit();
	return false;
}
function GetURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}
function openAll(){
	var checkAll=document.getElementById('checkAll'); 
	for(var i=0;i<98;i++){
		var groups=document.getElementsByClassName(i+"-group");
		if(groups !=null && groups != undefined){
			for(var j=0;j<groups.length;j++){
				if(checkAll.checked == true){
					groups[j].style.display = "table-row";
					$("#oper-"+i).html("-");
					var users=document.getElementsByClassName(i+((groups[j].id).replace(/\s/g, ''))+"-user");
					for(var k=0;k<users.length;k++){
						users[k].style.display = "table-row";
						$("#oper-"+i+((groups[j].id).replace(/\s/g, ''))).html("-");
					}
				}
				else
				{
					groups[j].style.display = "none";
					$("#oper-"+i).html("+");
					var users=document.getElementsByClassName(i+((groups[j].id).replace(/\s/g, ''))+"-user");
					for(var k=0;k<users.length;k++){
							users[k].style.display = "none";
							$("#oper-"+i+(groups[j].id).replace(/\s/g, '')).html("+");
					}
				}
			}
		
		}
	
	}
}
function fnExcelReport()
{
    var tab_text="<table border='2px'><tr bgcolor='#87AFC6'>";
    var textRange; var j=0;
    tab = document.getElementById('reportTable'); 

    for(j = 0 ; j < tab.rows.length ; j++) 
    {     
        tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
        //tab_text=tab_text+"</tr>";
    }
    tab_text = '<h2 class="module-title-text" style="font-weight: bold; text-align: center;">Báo Cáo Tổng Hợp Khách Hàng</h2>'+tab_text;
    tab_text=tab_text+"</table>";
    tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
    tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
    tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE "); 

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
    {
        txtArea1.document.open("txt/html","replace");
        txtArea1.document.write(tab_text);
        txtArea1.document.close();
        txtArea1.focus(); 
        sa=txtArea1.document.execCommand("SaveAs",true,"báo cáo tổng hợp khách hàng.xls");
         return (sa);
    }  
    else                 //other browser not tested on IE 11
       {
       	 var element = document.createElement('a');
            element.setAttribute('href', 'data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));
            element.setAttribute('download', "báo cáo tổng hợp khách hàng");
            element.style.display = 'none';
            document.body.appendChild(element);
            element.click();
            document.body.removeChild(element);
       }

   
}
function dataFilter(){
	 SUGAR.ajaxUI.showLoadingPanel();
	var province=$("#provincecode_c").val();
	var groupUsers=$("#group-users").val();
	var users=$("#users").val();
	var titleUsers=$("#title-users").val();
	var period=$("#period").val();
	var date_from = $('#date_from').val();
    var date_to = $('#date_to').val();
	var amountNew=0;
	var amountInProcess=0;
	var amountConverted=0;
	var amountTransaction=0;
	var amountDead=0;
	var amountWon=0;
	var amountLost=0;
	var amountOther=0;
	var amountCall=0;
	var amountMeeting=0;
	var amountEmail=0;
	var SumCustomer=0;
	var SumNew=0;
	var SumInpro=0;
	var SumConverted=0;
	var SumDead=0;
	var SumTransaction=0;
	var SumWon=0;
	var SumLost=0;
	var SumOther=0;
	var SumAmount=0;
	var aSum=[];
	var aSum2=[];
	console.log(titleUsers);
	$.ajax({
                type: "POST",
                url: "index.php?entryPoint=dataFilter",
                data: {
                    province:province,
					groupUsers:groupUsers,
					users:users,
					titleUsers:titleUsers,
					period:period,
					date_from:date_from,
					date_to:date_to
                },
                success: function(data) {
				if(data != null && data !=''){
					var Obj=JSON.parse(data);
					var html='';
					html+='<div style="padding-bottom:5px;padding-top:6px;width:100%;">';
					html+='<input style="margin-top:-6px;" type="checkbox" id="checkAll" onclick="openAll()">';
					html+='<span style="margin-left:5px;display:inline-block;">Open All</span>';
					html+='</div>';
					html+='<table id="reportTable" style="width:100%;">';
					html+='<tr>';
					html+='<td rowspan="5">STT</td>';
					html+='<td style="width:200px;text-align:center;font-weight:bold" rowspan="2">NHÓM BÁN HÀNG</td>';
					html+='<td style="text-align:center;font-weight:bold;font-size:16px" colspan="5">KHÁCH HÀNG</td>';
					html+='<td style="text-align:center;font-weight:bold;font-size:16px" colspan="3">DOANH SỐ</td>';
					html+='<td style="text-align:center;font-weight:bold;font-size:16px" colspan="3">HOẠT ĐỘNG</td>';
					html+='</tr>';
			html+='<tr style="background-color:#A9D0F5;font-weight:bold;text-align:center;font-size:16px;">';
				html+='<td>KHTN Mới</td>';
				html+='<td>Đang liên hệ</td>';
				html+='<td>Đã chuyển đổi</td>';
				html+='<td>Ngừng chăm sóc</td>';
				html+='<td>Phát sinh giao dịch</td>';
				html+='<td>Chốt thắng</td>';
				html+='<td>Chốt thua</td>';
				html+='<td>Khác</td>';
				html+='<td>Gọi điện</td>';
				html+='<td>Gặp mặt</td>';
				html+='<td>Gửi email</td>';
			html+='</tr>';
			html+='<tr style="font-weight:bold;text-align:center;font-size:16px;">';
			html+='<td style="background-color:#A9D0F5">Doanh số dự kiến</td>';
			html+='<td id="amountNew"></td>';
			html+='<td><span id="amountInProcess"></span></td>';
			html+='<td><span id="amountConverted"></span></td>';
			html+='<td><span id="amountDead"></span></td>';
			html+='<td><span id="amountTransaction"></span></td>';
			html+='<td><span id="amountWon"></span></td>';
			html+='<td><span id="amountLost"></span></td>';
			html+='<td><span id="amountOther"></span></td>';
			html+='<td id="amountCall" rowspan="3"></td>';
			html+='<td id="amountMeeting" rowspan="3"></td>';
			html+='<td id="amountEmail" rowspan="3"></td>';
			html+='</tr>';
			html+='<tr>';
			html+='<td></td>';
			html+='<td style="text-align:center;font-weight:bold;background-color:#E0F2F7"" id="SumCustomer" colspan="5"></td>';
			
			html+='<td style="text-align:center;font-weight:bold;background-color:#E0F2F7"" id="SumAmount" colspan="3"></td>';
			html+='</tr>';
			html+='<tr>';
			html+='<td></td>';
			html+='<td style="text-align:center;font-weight:bold" id="SumNew"></td>';
			html+='<td style="text-align:center;font-weight:bold" id="SumInpro"></td>';
			html+='<td style="text-align:center;font-weight:bold" id="SumConverted"></td>';
			html+='<td style="text-align:center;font-weight:bold" id="SumDead"></td>';
			html+='<td style="text-align:center;font-weight:bold" id="SumTransaction"></td>';
			html+='<td style="text-align:center;font-weight:bold" id="SumWon"></td>';
			html+='<td style="text-align:center;font-weight:bold" id="SumLost"></td>';
			html+='<td style="text-align:center;font-weight:bold" id="SumOther"></td>';
			html+='</tr>';
			html+='<tr>';
			html+='<td style="border-right-style:none"></td>';
			html+='<td style="border-left-style:none"></td>';
			html+='<td style="text-align:center;font-weight:bold;background-color:#E0F2F7" id="AllSumN" colspan="5"></td>';
			html+='<td style="text-align:center;font-weight:bold;background-color:#E0F2F7" id="AllSumW"colspan="3"></td>';
			html+='<td style="text-align:center;font-weight:bold;background-color:#E0F2F7" id="AllSumC" colspan="3"></td>';
			html+='</tr>';
		
					for(let i=0;i<Obj.length;i++){
						html+='<tr onclick=\"showHide2(\''+i+'\')\" style="background-color:#A9F5A9;font-weight:bold">';
						html+='<td rowspan="2">'+eval(i+1)+'</td>';
								html+='<td rowspan="2" style="font-weight:bold"><span id="oper-'+i+'">+</span>'+Obj[i].province+'</td>';
								html+='<td style="color:red;text-align:center">'+Obj[i].New+'</td>';
								html+='<td style="color:red;text-align:center">'+Obj[i].InProcess+'</td>';
								html+='<td style="color:red;text-align:center">'+Obj[i].Converted+'</td>';
								html+='<td style="color:red;text-align:center">'+Obj[i].Dead+'</td>';
								html+='<td style="color:red;text-align:center">'+Obj[i].Transaction+'</td>';
								html+='<td style="color:red;text-align:center">'+Obj[i].CloseWon+'</td>';
								html+='<td style="color:red;text-align:center">'+Obj[i].CloseLost+'</td>';
								html+='<td style="color:red;text-align:center">'+Obj[i].Other+'</td>';
								html+='<td rowspan="2" style="color:red;text-align:center">'+Obj[i].amountCall+'</td>';
								html+='<td rowspan="2" style="color:red;text-align:center">'+Obj[i].amountMeeting+'</td>';
								html+='<td rowspan="2" style="color:red;text-align:center">'+Obj[i].amountEmail+'</td>';
								html+='</tr>';
						html+='<tr onclick=\"showHide2(\''+i+'\')\" style="background-color:#A9F5A9;font-weight:bold">';
						
								html+='<td style="color:red;text-align:center">'+changeCurrency(String(Obj[i].amountNew))+'</td>';
								html+='<td style="color:red;text-align:center">'+changeCurrency(String(Obj[i].amountInProcess))+'</td>';
								html+='<td style="color:red;text-align:center">'+changeCurrency(String(Obj[i].amountConverted))+'</td>';
								html+='<td style="color:red;text-align:center">'+changeCurrency(String(Obj[i].amountDead))+'</td>';
								html+='<td style="color:red;text-align:center">'+changeCurrency(String(Obj[i].amountTransaction))+'</td>';
								html+='<td style="color:red;text-align:center">'+changeCurrency(String(Obj[i].amountWon))+'</td>';
								html+='<td style="color:red;text-align:center">'+changeCurrency(String(Obj[i].amountLost))+'</td>';
								html+='<td style="color:red;text-align:center">'+changeCurrency(String(Obj[i].amountOther))+'</td>';
								
								html+='</tr>';
								var  data = {};
								Object.keys(Obj[i].data).sort().forEach(function(key) {
									data[key] = Obj[i].data[key];
								});
							for (var key in data) {
								var New=0;
								var inProcess=0;
								var converted=0;
								var dead=0;
								var transaction=0;
								var closedWon=0;
								var closedLost=0;
								var other=0;
								var call=0;
								var meeting=0;
								var email=0;
								var amountNewG=0;
								var amountInproG=0;
								var amountConvertedG=0;
								var amountDeadG=0;
								var amountTransactionG=0;
								var amountClosedWonG=0;
								var amountClosedLostG=0;
								var amountOtherG=0;
								html+='<tr style="font-weight:bold;display:none;background-color:#F5D0A9" class="'+i+'-group" onclick=\"showHide(\''+i+key.replace(/\s/g, '')+'\')\" id="'+key.replace(/\s/g, '')+'">';
								html+='<td style="border-bottom-style:none"></td>';
								html+='<td rowspan="2" style="background-color:#F5D0A9"><span id="oper-'+i+key.replace(/\s/g, '')+'" style="text-align:left">+</span><span style="margin-left:3%">'+key+'</span></td>';
								html+='<td style="text-align:center" id="New"></td>';
								html+='<td style="text-align:center" id="InProcess"></td>';
								html+='<td style="text-align:center" id="Converted"></td>';
								html+='<td style="text-align:center" id="Dead"></td>';
								html+='<td style="text-align:center" id="Transaction"></td>';
								html+='<td style="text-align:center" id="ClosedWon"></td>';
								html+='<td style="text-align:center" id="ClosedLost"></td>';
								html+='<td style="text-align:center" id="Other"></td>';
								html+='<td rowspan="2" style="text-align:center" id="Call"></td>';
								html+='<td rowspan="2" style="text-align:center" id="Meeting"></td>';
								html+='<td rowspan="2" style="text-align:center" id="Email"></td>';
								
								html+='</tr>';
								html+='<tr style="font-weight:bold;display:none;color:red;background-color:#F5D0A9" class="'+i+'-group" onclick=\"showHide(\''+i+key.replace(/\s/g, '')+'\')\" id="'+key.replace(/\s/g, '')+'-2">';
								html+='<td style="border-top-style:none"></td>';
								
								html+='<td style="text-align:center" id="New"></td>';
								html+='<td style="text-align:center" id="InProcess"></td>';
								html+='<td style="text-align:center" id="Converted"></td>';
								html+='<td style="text-align:center" id="Dead"></td>';
								html+='<td style="text-align:center" id="Transaction"></td>';
								html+='<td style="text-align:center" id="ClosedWon"></td>';
								html+='<td style="text-align:center" id="ClosedLost"></td>';
								html+='<td style="text-align:center" id="Other"></td>';
								
								html+='</tr>';
							for(let j=0;j<data[key].length;j++){
								html+='<tr style="display:none" class="'+i+key.replace(/\s/g, '')+'-user">';
								html+='<td></td>';
								html+='<td style="text-align:right;margin-right:3px;font-weight:bold"><span style="margin-right:3px;font-weight:bold">'+data[key][j].user+'</span></td>';
								html+='<td style="text-align:right"><span style="margin-right:3px;font-weight:bold">'+data[key][j].New+'</span></td>';
								html+='<td style="text-align:right"><span style="margin-right:3px;font-weight:bold">'+data[key][j].InProcess+'</span></td>';
								html+='<td style="text-align:right"><span style="margin-right:3px;font-weight:bold">'+data[key][j].Converted+'</span></td>';
								html+='<td style="text-align:right"><span style="margin-right:3px;font-weight:bold">'+data[key][j].Dead+'</span></td>';
								html+='<td style="text-align:right"><span style="margin-right:3px;font-weight:bold">'+data[key][j].Transaction+'</span></td>';
								html+='<td style="text-align:right"><span style="margin-right:3px;font-weight:bold">'+data[key][j].ClosedWon+'</span></td>';
								html+='<td style="text-align:right"><span style="margin-right:3px;font-weight:bold">'+data[key][j].ClosedLost+'</span></td>';
								html+='<td style="text-align:right"><span style="margin-right:3px;font-weight:bold">'+data[key][j].Other+'</span></td>';
								html+='<td style="text-align:right"><span style="margin-right:3px;font-weight:bold">'+data[key][j].Call+'</span></td>';
								html+='<td style="text-align:right"><span style="margin-right:3px;font-weight:bold">'+data[key][j].Meeting+'</span></td>';
								html+='<td style="text-align:right"><span style="margin-right:3px;font-weight:bold">'+data[key][j].Email+'</span></td>';
								html+='</tr>';
								New=eval(New+parseInt(data[key][j].New));
								inProcess=eval(inProcess+parseInt(data[key][j].InProcess));
								converted=eval(converted+parseInt(data[key][j].Converted));
								dead=eval(dead+parseInt(data[key][j].Dead));
								transaction=eval(transaction+parseInt(data[key][j].Transaction));
								closedWon=eval(closedWon+parseInt(data[key][j].ClosedWon));
								closedLost=eval(closedLost+parseInt(data[key][j].ClosedLost));
								other=eval(other+parseInt(data[key][j].Other));
								call=eval(call+parseInt(data[key][j].Call));
								meeting=eval(meeting+parseInt(data[key][j].Meeting));
								email=eval(email+parseInt(data[key][j].Email));
								amountNewG=eval(amountNewG+parseInt(data[key][j].userAmountNew));
								amountInproG=eval(amountInproG+parseInt(data[key][j].userAmountInprocess));
								amountConvertedG=eval(amountConvertedG+parseInt(data[key][j].userAmountConverted));
								amountDeadG=eval(amountDeadG+parseInt(data[key][j].userAmountDead));
								amountTransactionG=eval(amountTransactionG+parseInt(data[key][j].userAmountTransaction));
								amountClosedWonG=eval(amountClosedWonG+parseInt(data[key][j].userAmountClosedWon));
								amountClosedLostG=eval(amountClosedLostG+parseInt(data[key][j].userAmountClosedLost));
								amountOtherG=eval(amountOtherG+parseInt(data[key][j].userAmountOther));
								
							}	
								aSum.push({
									group:key.replace(/\s/g, ''),
									NewC:New,
									inProcessC:inProcess,
									convertedC:converted,
									deadC:dead,
									transactionC:transaction,
									closedWonC:closedWon,
									closedLostC:closedLost,
									otherC:other,
									callC:call,
									meetingC:meeting,
									emailC:email
								});
								
								aSum2.push({
									group:key.replace(/\s/g, '')+'-2',
									amountNewC:amountNewG,
									amountInproC:amountInproG,
									amountConvertedC:amountConvertedG,
									amountDeadC:amountDeadG,
									amountTransactionC:amountTransactionG,
									amountClosedWonC:amountClosedWonG,
									amountClosedLostC:amountClosedLostG,
									amountOtherC:amountOtherG
									
								});
								
							}
							
							amountNew=eval(amountNew+Obj[i].amountNew);
							amountInProcess=eval(amountInProcess+Obj[i].amountInProcess);
							amountConverted=eval(amountConverted+Obj[i].amountConverted);
							amountDead=eval(amountDead+Obj[i].amountDead);
							amountTransaction=eval(amountTransaction+Obj[i].amountTransaction);
							amountWon=eval(amountWon+Obj[i].amountWon);
							amountLost=eval(amountLost+Obj[i].amountLost);
							amountOther=eval(amountOther+Obj[i].amountOther);
							amountCall=eval(amountCall+Obj[i].amountCall);
							amountMeeting=eval(amountMeeting+Obj[i].amountMeeting);
							amountEmail=eval(amountEmail+Obj[i].amountEmail);
							SumNew=eval(SumNew + Obj[i].New);
							SumInpro=eval(SumInpro + Obj[i].InProcess);
							SumConverted=eval(SumConverted + Obj[i].Converted);
							SumDead=eval(SumDead + Obj[i].Dead);
							SumTransaction=eval(SumTransaction + Obj[i].Transaction);
							SumWon=eval(SumWon + Obj[i].CloseWon);
							SumLost=eval(SumLost + Obj[i].CloseLost);
							SumOther=eval(SumOther + Obj[i].Other);
							
					}
					html+='</table>';
						$("#father-group").html(html);
						for(let f=0;f<aSum.length;f++){
								var a=document.getElementById(aSum[f].group);
								var tdList=a.getElementsByTagName('td');
								for(let i=2; i< tdList.length; i++){ 
									if(i == 2){
									tdList[i].innerHTML = aSum[f].NewC;
									}
									else if(i==3)
									{
									tdList[i].innerHTML = aSum[f].inProcessC;
									}else if(i==4)
									{
									tdList[i].innerHTML = aSum[f].convertedC;	
									}
									else if(i==5)
									{
									tdList[i].innerHTML = aSum[f].deadC;	
									}else if(i==6)
									{
									tdList[i].innerHTML = aSum[f].transactionC;	
									}else if(i==7)
									{
									tdList[i].innerHTML = aSum[f].closedWonC;	
									}else if(i==8)
									{
									tdList[i].innerHTML = aSum[f].closedLostC;	
									}else if(i==9)
									{
									tdList[i].innerHTML = aSum[f].otherC;	
									}else if(i==10)
									{
									tdList[i].innerHTML = aSum[f].callC;	
									}else if(i==11)
									{
									tdList[i].innerHTML = aSum[f].meetingC;	
									}else if(i==12)
									{
									tdList[i].innerHTML = aSum[f].emailC;	
									}
							}
							
						}
						for(let v=0;v<aSum2.length;v++){
								var a=document.getElementById(aSum2[v].group);
								var tdList=a.getElementsByTagName('td');
								for(let i=1; i< tdList.length; i++){ 
									if(i == 1){
									tdList[i].innerHTML = changeCurrency(String(aSum2[v].amountNewC));
									}
									else if(i==2)
									{
									tdList[i].innerHTML = changeCurrency(String(aSum2[v].amountInproC));
									}else if(i==3)
									{
									tdList[i].innerHTML = changeCurrency(String(aSum2[v].amountConvertedC));	
									}
									else if(i==4)
									{
									tdList[i].innerHTML = changeCurrency(String(aSum2[v].amountDeadC));	
									}else if(i==5)
									{
									tdList[i].innerHTML = changeCurrency(String(aSum2[v].amountTransactionC));
									}else if(i==6)
									{
									tdList[i].innerHTML = changeCurrency(String(aSum2[v].amountClosedWonC));	
									}else if(i==7)
									{
									tdList[i].innerHTML = changeCurrency(String(aSum2[v].amountClosedLostC));	
									}else if(i==8)
									{
									tdList[i].innerHTML = changeCurrency(String(aSum2[v].amountOtherC));	
									}
							}
							
						}
						document.getElementById("amountNew").innerHTML=changeCurrency(String(amountNew));
						document.getElementById("amountInProcess").innerHTML=changeCurrency(String(amountInProcess));
						document.getElementById("amountConverted").innerHTML=changeCurrency(String(amountConverted));
						document.getElementById("amountDead").innerHTML=changeCurrency(String(amountDead));
						document.getElementById("amountTransaction").innerHTML=changeCurrency(String(amountTransaction));
						document.getElementById("amountWon").innerHTML=changeCurrency(String(amountWon));
						document.getElementById("amountLost").innerHTML=changeCurrency(String(amountLost));
						document.getElementById("amountOther").innerHTML=changeCurrency(String(amountOther));
						document.getElementById("amountCall").innerHTML=amountCall;
						document.getElementById("amountMeeting").innerHTML=amountMeeting;
						document.getElementById("amountEmail").innerHTML=amountEmail;
						SumCustomer=eval(amountNew + amountInProcess + amountConverted + amountDead + amountTransaction);
						SumAmount=eval(amountWon + amountLost + amountOther);
						document.getElementById("SumCustomer").innerHTML=changeCurrency(String(SumCustomer));
						document.getElementById("SumAmount").innerHTML=changeCurrency(String(SumAmount));
						document.getElementById("SumNew").innerHTML=SumNew;
						document.getElementById("SumInpro").innerHTML=SumInpro;
						document.getElementById("SumConverted").innerHTML=SumConverted;
						document.getElementById("SumDead").innerHTML=SumDead;
						document.getElementById("SumTransaction").innerHTML=SumTransaction;
						document.getElementById("SumWon").innerHTML=SumWon;
						document.getElementById("SumLost").innerHTML=SumLost;
						document.getElementById("SumOther").innerHTML=SumOther;
						var AllSumN=eval(SumNew+SumInpro+SumConverted+SumDead+SumTransaction);
						var AllSumW=eval(SumWon+SumLost+SumOther);
						var AllSumC=eval(amountCall+amountMeeting+amountEmail);
						document.getElementById("AllSumN").innerHTML=AllSumN;
						document.getElementById("AllSumW").innerHTML=AllSumW;
						document.getElementById("AllSumC").innerHTML=AllSumC;
						 SUGAR.ajaxUI.hideLoadingPanel();
					}else{
						var html='';
					html+='<div style="padding-bottom:5px;padding-top:6px;width:100%;">';
					html+='<input style="margin-top:-6px;" type="checkbox" id="checkAll" onclick="openAll()">';
					html+='<span style="margin-left:5px;display:inline-block;">Open All</span>';
					html+='</div>';
					html+='<table id="reportTable" style="width:100%;">';
					html+='<tr>';
					html+='<td rowspan="5">STT</td>';
					html+='<td style="text-align:center;font-weight:bold" rowspan="2">NHÓM BÁN HÀNG</td>';
					html+='<td style="text-align:center;font-weight:bold;font-size:16px" colspan="5">KHÁCH HÀNG</td>';
					html+='<td style="text-align:center;font-weight:bold;font-size:16px" colspan="3">DOANH SỐ</td>';
					html+='<td style="text-align:center;font-weight:bold;font-size:16px" colspan="3">HOẠT ĐỘNG</td>';
					html+='</tr>';
			html+='<tr style="background-color:#A9D0F5;font-weight:bold;text-align:center;font-size:16px;">';
				html+='<td>KHTN Mới</td>';
				html+='<td>Đang liên hệ</td>';
				html+='<td>Đã chuyển đổi</td>';
				html+='<td>Ngừng chăm sóc</td>';
				html+='<td>Phát sinh giao dịch</td>';
				html+='<td>Chốt thắng</td>';
				html+='<td>Chốt thua</td>';
				html+='<td>Khác</td>';
				html+='<td>Gọi điện</td>';
				html+='<td>Gặp mặt</td>';
				html+='<td>Gửi email</td>';
			html+='</tr>';
			html+='<tr style="font-weight:bold;text-align:center;font-size:16px;">';
			html+='<td style="background-color:#A9D0F5">Doanh số dự kiến</td>';
			html+='<td id="amountNew"></td>';
			html+='<td><span id="amountInProcess"></span></td>';
			html+='<td><span id="amountConverted"></span></td>';
			html+='<td><span id="amountDead"></span></td>';
			html+='<td><span id="amountTransaction"></span></td>';
			html+='<td><span id="amountWon"></span></td>';
			html+='<td><span id="amountLost"></span></td>';
			html+='<td><span id="amountOther"></span></td>';
			html+='<td id="amountCall" rowspan="3"></td>';
			html+='<td id="amountMeeting" rowspan="3"></td>';
			html+='<td id="amountEmail" rowspan="3"></td>';
			html+='</tr>';
			html+='<tr>';
			html+='<td></td>';
			html+='<td style="text-align:center;font-weight:bold" id="SumCustomer" colspan="5"></td>';
			
			html+='<td style="text-align:center;font-weight:bold" id="SumAmount" colspan="3"></td>';
			html+='</tr>';
			html+='<tr>';
			html+='<td></td>';
			html+='<td style="text-align:center;font-weight:bold" id="SumNew"></td>';
			html+='<td style="text-align:center;font-weight:bold" id="SumInpro"></td>';
			html+='<td style="text-align:center;font-weight:bold" id="SumConverted"></td>';
			html+='<td style="text-align:center;font-weight:bold" id="SumDead"></td>';
			html+='<td style="text-align:center;font-weight:bold" id="SumTransaction"></td>';
			html+='<td style="text-align:center;font-weight:bold" id="SumWon"></td>';
			html+='<td style="text-align:center;font-weight:bold" id="SumLost"></td>';
			html+='<td style="text-align:center;font-weight:bold" id="SumOther"></td>';
			html+='</tr>';
			html+='<tr>';
			html+='<td></td>';
			html+='<td></td>';
			html+='<td style="text-align:center;font-weight:bold" id="AllSumN" colspan="5"></td>';
			html+='<td style="text-align:center;font-weight:bold" id="AllSumW"colspan="3"></td>';
			html+='<td style="text-align:center;font-weight:bold" id="AllSumC" colspan="3"></td>';
			html+='</tr>';
						html+='</table>';
						$("#father-group").html(html);
						 SUGAR.ajaxUI.hideLoadingPanel();
					}
                },
                error: function (error) {
                    
                }
            });
			
	
}
function loadGroup(){
	var groupId=document.getElementById("provincecode_c").value;
	$.ajax({
                type: "POST",
                url: "index.php?entryPoint=getGroup",
                data: {
                    id:groupId
                },
                dataType: "json",
                success: function(data) {
					var obj=data;
					html="";
					html+='<div>Quận/Huyện</div>';
					html+='<input style="width:250px;" placeholder="Search.." id="myInput" onkeyup="filterKey()">';
					html+='<select style="width:250px;height:100px" name="group-users[]" id="group-users" multiple="multiple" >';
					html+='<option onclick="getUser();getTitle();" value="All">All</option>';
					for(let i=0;i<obj.length;i++){
						html+='<option onclick="getUser();getTitle()" value="'+obj[i].id+'">'+obj[i].name+'</option>';
					}
                    html+='</select>';
					$('#groupUser').html(html);
					$('#users').html('');
					
                },
                error: function (error) {
                    
                }
            });
}
function getUser(){
	var province=document.getElementById("provincecode_c").value;
	var groups=$('#group-users').val();
	$.ajax({
                type: "POST",
                url: "index.php?entryPoint=getUser",
                data: {
                    id:groups,
					province:province
                },
                dataType: "json",
                success: function(data) {
					var obj=data;
					html="";
					for(let i=0;i<obj.length;i++){
						html+='<option value="'+obj[i].id+'">'+obj[i].name+(obj[i].title?'-'+obj[i].title:'')+'</option>';
					}
                    
					$('#users').html(html);
					
                },
                error: function (error) {
                    
                }
            });
			
}
function clearUser(){
	$("#users").val('');
}
function getTitle(){
	var province=document.getElementById("provincecode_c").value;
	var groups=$('#group-users').val();
	$.ajax({
                type: "POST",
                url: "index.php?entryPoint=getTitle",
                data: {
                    id:groups,
					province:province
                },
                dataType: "json",
                success: function(data) {
					var obj=data;
					html="";
					for(let i=0;i<obj.length;i++){
					if(obj[i].title != null && obj[i].title != ''){
						html+='<option value="'+obj[i].id+'">'+obj[i].title+'</option>';
					}
					}
                    
					$('#title-users').html(html);
					
                },
                error: function (error) {
                    
                }
            });
			
}
function showProvince(){
	$.ajax({
                type: "POST",
                url: "index.php?entryPoint=showProvince",
                dataType: "json",
                success: function(data) {
					for(let i=0;i<data.length;i++){
						document.getElementById(data[i]).style.display="block";
					}
                },
                error: function (error) {
                    
                }
            });
}
var moduleN=document.getElementsByName("return_module")[0].value;
console.log(moduleN);
if(moduleN == "rp_CustomReport"){
	document.getElementsByClassName("listViewEmpty")[0].style.display="none";
	document.getElementById("cde").style.display="block";
	$(".module-title-text").html("Báo Cáo Tổng Hợp Khách Hàng");
	document.getElementsByClassName("module-title-text")[0].style.fontWeight="bold";
	document.getElementsByClassName("module-title-text")[0].style.marginLeft="28%";
}
</script>

<style>
#cde table, #cde th,#cde td {
  border: 1px solid black;
  border-collapse: collapse;
}
.text-center {
    text-align: center;
}
label {
    display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: bold;
}
.dashletcontainer .button, .dashletcontainer input[type="submit"], .dashletcontainer input[type="button"] {
    line-height: 100%;
}
.ui-datepicker-trigger {
    margin-left: 5px !important;
	vertical-align: middle;
}
.button {
    margin-bottom: 4px;
}
.button {
    font-size: 13px;
    font-weight: 500;
    background: #f08377;
    color: #f5f5f5;
    cursor: pointer;
    padding: 0 20px 0 20px;
    margin: 0 0 0 0;
    border: none;
    border-radius: 3px;
    letter-spacing: 1px;
    line-height: 32px;
    height: 32px;
    text-transform: uppercase;
}
.suitepicon-module-calendar{
    margin-top: 30%;
}
table{
	font-size: 14px;	
}

</style>
			
		<div id="cde" style="display:none;margin-top:20px;">
		  <div class="row" style="margin-bottom: 10px">
							<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 text-left" >
							<div>Thời gian</div>
                                <select style="width:200px" name="period" id="period" onchange="periodChange()">
									<option value="default">------</option>
									<option selected="" value="this_week">Tuần này</option>
									<option value="last_week">Tuần trước</option>
									<option value="this_month">Tháng này</option>
									<option value="last_month">Tháng trước</option>
									<option value="this_year">Năm này</option>
									<option value="last_year">Năm trước</option>
									<option value="is_between">Khoảng thời gian</option>
                                </select>
								 <div id="date_range" class="text-center" style="margin-top:20px">
                    <div class="row" style="margin-bottom: 5px">
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-left">
                            <label>Từ</label>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 text-left">
                            <input id="date_from"  name="date_from" type="text" style="width:170px;" maxlength="100" value="" title="" tabindex="-1" accesskey="9">
                            <br><span class="error" style="display: none;">Please select a date</span>
                        </div>
						 <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 datepicker-trigger">
						
						</div>
					</div>
                    <div class="row" style="margin-bottom: 5px">
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-left">
                            <label>Đến</label>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 text-left">
                            <input type="text" style="width:170px;"  id="date_to" name="date_to" maxlength="100" value="" title="" tabindex="-1" accesskey="9">
                            <br><span class="error" style="display: none;">Please select a date</span>
                        </div>
						<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 datepicker-trigger">
						
						</div>
                    </div>
                </div>
                            </div>
							
							
                            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 ">
							<div>Tỉnh/TP</div>
                                <select style="width:200px" name="group-users[]" id="provincecode_c" onchange="loadGroup()">
																		<option  value="" selected="selected">----</option>							
																		<option  label="" value="All">Chọn tất cả</option>
																		<option style="display:none" label=" TP Hà Nội" value="10" id="10"> TP Hà Nội</option>
																		<option style="display:none" label=" Tỉnh Hưng Yên" value="16" id="16"> Tỉnh Hưng Yên</option>
																		<option style="display:none" label=" Tỉnh Hải Dương" value="17" id="17"> Tỉnh Hải Dương</option>
																		<option style="display:none" label=" TP Hải Phòng " value="18" id="18"> TP Hải Phòng </option>
																		<option style="display:none" label=" Tỉnh Quảng Ninh " value="20" id="20"> Tỉnh Quảng Ninh </option>
																		<option style="display:none" label=" Tỉnh Bắc Ninh " value="22" id="22"> Tỉnh Bắc Ninh </option>
																		<option style="display:none" label=" Tỉnh Bắc Giang " value="23" id="23"> Tỉnh Bắc Giang </option>
																		<option style="display:none" label=" Tỉnh Lạng Sơn " value="24" id="24"> Tỉnh Lạng Sơn </option>
																		<option style="display:none" label=" Tỉnh Thái Nguyên " value="25" id="25"> Tỉnh Thái Nguyên </option>
																		<option style="display:none" label=" Tỉnh Bắc Kạn " value="26" id="26"> Tỉnh Bắc Kạn </option>
																		<option style="display:none" label=" Tỉnh Cao Bằng " value="27" id="27"> Tỉnh Cao Bằng </option>
																		<option style="display:none" label=" Tỉnh Vĩnh Phúc " value="28" id="28"> Tỉnh Vĩnh Phúc </option>
																		<option style="display:none" label=" Tỉnh Phú Thọ " value="29" id="29"> Tỉnh Phú Thọ </option>
																		<option style="display:none" label=" Tỉnh Tuyên Quang " value="30" id="30"> Tỉnh Tuyên Quang </option>
																		<option style="display:none" label=" Tỉnh Hà Giang " value="31" id="31"> Tỉnh Hà Giang </option>
																		<option style="display:none" label=" Tỉnh Yên Bái " value="32" id="32"> Tỉnh Yên Bái </option>
																		<option style="display:none" label=" Tỉnh Lào Cai " value="33" id="33"> Tỉnh Lào Cai </option>
																		<option style="display:none" label=" Tỉnh Hoà Bình " value="35" id="35"> Tỉnh Hoà Bình </option>
																		<option style="display:none" label=" Tỉnh Sơn La " value="36" id="36"> Tỉnh Sơn La </option>
																		<option style="display:none" label=" Tỉnh Điện Biên " value="38" id="38"> Tỉnh Điện Biên </option>
																		<option style="display:none" label=" Tỉnh Lai Châu " value="39" id="39"> Tỉnh Lai Châu </option>
																		<option style="display:none" label=" Tỉnh Hà Nam " value="40" id="40"> Tỉnh Hà Nam </option>
																		<option style="display:none" label=" Tỉnh Thái Bình " value="41" id="41"> Tỉnh Thái Bình </option>
																		<option style="display:none" label=" Tỉnh Nam Định " value="42" id="42"> Tỉnh Nam Định </option>
																		<option style="display:none" label=" Tỉnh Ninh Bình " value="43" id="43"> Tỉnh Ninh Bình </option>
																		<option style="display:none" label=" Tỉnh Thanh Hoá " value="44" id="44"> Tỉnh Thanh Hoá </option>
																		<option style="display:none" label=" Tỉnh Nghệ An " value="46" id="46"> Tỉnh Nghệ An </option>
																		<option style="display:none" label=" Tỉnh Hà Tĩnh " value="48" id="48"> Tỉnh Hà Tĩnh </option>
																		<option style="display:none" label=" Tỉnh Quảng Bình " value="51" id="51"> Tỉnh Quảng Bình </option>
																		<option style="display:none" label=" Tỉnh Quảng Trị " value="52" id="52"> Tỉnh Quảng Trị </option>
																		<option style="display:none" label=" Tỉnh Thừa Thiên Huế " value="53" id="53"> Tỉnh Thừa Thiên Huế </option>
																		<option style="display:none" label=" TP Đà Nẵng " value="55" id="55"> TP Đà Nẵng </option>
																		<option style="display:none" label=" Tỉnh Quảng Nam " value="56" id="56"> Tỉnh Quảng Nam </option>
																		<option style="display:none" label=" Tỉnh Quảng Ngãi " value="57" id="57"> Tỉnh Quảng Ngãi </option>
																		<option style="display:none" label=" Tỉnh Kon Tum " value="58" id="58"> Tỉnh Kon Tum </option>
																		<option style="display:none" label=" Tỉnh Bình Định " value="59" id="59"> Tỉnh Bình Định </option>
																		<option style="display:none" label=" Tỉnh Gia Lai " value="60" id="60"> Tỉnh Gia Lai </option>
																		<option style="display:none" label=" Tỉnh Phú Yên " value="62" id="62"> Tỉnh Phú Yên </option>
																		<option style="display:none" label=" Tỉnh Đắk Lăk " value="63" id="63"> Tỉnh Đắk Lăk </option>
																		<option style="display:none" label=" Tỉnh Đắk Nông " value="64" id="64"> Tỉnh Đắk Nông </option>
																		<option style="display:none" label=" Tỉnh Khánh Hoà " value="65" id="65"> Tỉnh Khánh Hoà </option>
																		<option style="display:none" label=" Tỉnh Ninh Thuận " value="66" id="66"> Tỉnh Ninh Thuận </option>
																		<option style="display:none" label=" Tỉnh Lâm Đồng " value="67" id="67"> Tỉnh Lâm Đồng </option>
																		<option style="display:none" label=" TP Hồ Chí Minh " value="70" id="70"> TP Hồ Chí Minh </option>
																		<option style="display:none" label=" Tỉnh Bà Rịa Vũng Tàu " value="79" id="79"> Tỉnh Bà Rịa Vũng Tàu </option>
																		<option style="display:none" label=" Tỉnh Bình Thuận " value="80" id="80"> Tỉnh Bình Thuận </option>
																		<option style="display:none" label=" Tỉnh Đồng Nai " value="81" id="81"> Tỉnh Đồng Nai </option>
																		<option style="display:none" label=" Tỉnh Bình Dương " value="82" id="82"> Tỉnh Bình Dương </option>
																		<option style="display:none" label=" Tỉnh Bình Phước " value="83" id="83"> Tỉnh Bình Phước </option>
																		<option style="display:none" label=" Tỉnh Tây Ninh " value="84" id="84"> Tỉnh Tây Ninh </option>
																		<option style="display:none" label=" Tỉnh Long An " value="85" id="85"> Tỉnh Long An </option>
																		<option style="display:none" label=" Tỉnh Tiền Giang " value="86" id="86"> Tỉnh Tiền Giang </option>
																		<option style="display:none" label=" Tỉnh Đồng Tháp " value="87" id="87"> Tỉnh Đồng Tháp </option>
																		<option style="display:none" label=" Tỉnh An Giang " value="88" id="88"> Tỉnh An Giang </option>
																		<option style="display:none" label=" Tỉnh Vĩnh Long " value="89" id="89"> Tỉnh Vĩnh Long </option>
																		<option style="display:none" label=" TP Cần Thơ " value="90" id="90"> TP Cần Thơ </option>
																		<option style="display:none" label=" Tỉnh Hậu Giang " value="91" id="91"> Tỉnh Hậu Giang </option>
																		<option style="display:none" label=" Tỉnh Kiên Giang " value="92" id="92"> Tỉnh Kiên Giang </option>
																		<option style="display:none" label=" Tỉnh Bến Tre " value="93" id="93"> Tỉnh Bến Tre </option>
																		<option style="display:none" label=" Tỉnh Trà Vinh " value="94" id="94"> Tỉnh Trà Vinh </option>
																		<option style="display:none" label=" Tỉnh Sóc Trăng " value="95" id="95"> Tỉnh Sóc Trăng </option>
																		<option style="display:none" label=" Tỉnh Bạc Liêu " value="96" id="96"> Tỉnh Bạc Liêu </option>
																		<option style="display:none" label=" Tỉnh Cà Mau " value="97" id="97"> Tỉnh Cà Mau </option>
																		<option style="display:none" label=" Vnpost " value="98" id="98"> Vnpost </option>
																		<option style="display:none" label=" PPTT " value="99" id="99"> PPTT </option>
																		<option style="display:none" label=" DVBC " value="100" id="100"> DVBC </option>
																		<option style="display:none" label=" TCBC " value="101" id="101"> TCBC </option>
																		<option style="display:none" label=" DVKH " value="102" id="102"> DVKH </option>
        
									                                </select>
                            </div>
							<div id="groupUser" class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
							<div>Quận/Huyện</div>
							<input style="width:250px;" type="text" placeholder="Search.." id="myInput" onkeyup="filterKey()">
                                <select style="width:250px;height:100px" name="group-users[]" id="group-users" multiple="multiple">
                                
									                                </select>
                            </div>
							 
							  <div id="userTitle" class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left ">
							  <div>Chức vụ</div>
							<input style="width:250px;" type="text" placeholder="Search.." id="myInput3" onkeyup="filterKey3()">
                                <select style="width:250px;height:100px;" name="title-users[]" onclick="clearUser()" id="title-users" multiple="multiple">
                                
									                                </select>
                            </div>
							<div id="listUser" class="col-xs-12 col-sm-12 col-md-2 col-lg-2 ">
							<div>Người dùng</div>
							<input style="width:250px;" type="text" placeholder="Search.." id="myInput2" onkeyup="filterKey2()">
                                <select style="width:250px;height:100px;" name="users[]" id="users" multiple="multiple">
																		
        
									                                </select>
                            </div>
							
                </div>
               
				
			  <div class="row">
                    
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center" style="margin-left:-5px">
                        <button id="filter" class="button" onclick="dataFilter()" style="width:130px;">Thống kê</button>
                    </div>
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center" style="margin-left:5px">
                       <input type="button" onclick="refreshFilter()" class="button" value="Xóa ĐK Lọc">
                    </div>
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center">
					<input type="button" onclick="fnExcelReport()" class="button" value="Xuất excel">
                    </div>
                </div>
          <div id="father-group" style="margin-top:5px"> 
	
		</div>
		
		</div>
		<script type="text/javascript">
		if(moduleN == "rp_CustomReport"){
        $.datepicker.setDefaults({
            showOn: "both",
            dateFormat: convertSugarDateFormatToDatePickerFormat(),
        });
        
		showProvince();
		loadProvince();
		showHideDateFields($('#period').val());
		}
		
    </script>

		
EOQ;
				
				
				
				
				
				
				
			echo $mass_assign_employee;	
			}
        }

        //if after a save...
        if (!empty($_SESSION['securitysuite_error'])) {
            $lbl_securitysuite_error = $_SESSION['securitysuite_error'];
            unset($_SESSION['securitysuite_error']);
            echo <<<EOQ
<script>
				

var oNewP = document.createElement("div");
oNewP.className = 'error';

var oText = document.createTextNode("${lbl_securitysuite_error}");
oNewP.appendChild(oText);

var beforeMe = document.getElementsByTagName("div")[0];
document.body.insertBefore(oNewP, beforeMe);

</script>
EOQ;
        }
    }
}
