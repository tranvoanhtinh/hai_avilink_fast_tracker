<?php
global $db;

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    $rows = $_POST['rows'];
    $module = $_POST['module'];
    $rows = html_entity_decode($rows);
    $rows = json_decode($rows, true);
 
    if (!is_array($rows) || empty($rows)) {
        echo json_encode(array("error" => "Invalid input data"));
        exit;
    }

    // Tạo câu lệnh SQL với các cột được cung cấp
    $columns = implode(', ', array_map(function($col) {
        return htmlspecialchars($col);
    }, $rows));

    // Xây dựng câu SQL dựa trên có hoặc không có where_id
    if (isset($_POST['where_id']) && !empty($_POST['where_id'])) {
        $id = $db->quote($_POST['where_id']); // Bảo vệ khỏi SQL injection
        $sql = "SELECT $columns FROM $module WHERE deleted = '0' AND id = '".$id."'";
    } else {
        $sql = "SELECT $columns FROM $module WHERE deleted = '0'";
    }
    // Thực hiện truy vấn
    $result = $db->query($sql);

    $$module = array();

    if ($result && $db->getRowCount($result) > 0) {
        // Lấy dữ liệu từ kết quả truy vấn
        while ($row = $db->fetchByAssoc($result)) {
            $arr = array();
            foreach ($rows as $column) {
                if (isset($row[$column])) {
                    $arr[$column] = $row[$column];
                }
            }
            $$module[] = $arr;
        }
    } else {
        echo json_encode(array("error" => "No records found"));
        exit;
    }

    // Trả về dữ liệu dưới dạng JSON
    echo json_encode($$module);
} else {
    echo json_encode(array("error" => "Invalid request method"));
}
?>
