<?php
function getDateRangeToSearchForAmount($period_list_selected = 'this_month')
{
        if(empty($period_list_selected)) $period_list_selected = 'Tháng này';
        switch ($period_list_selected) {
            case 'Tuần này':
                $datetimeTo = new DateTime("this week sunday");
               // $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("this week monday");
              //  $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Tuần trước':
                $datetimeTo = new DateTime("last week sunday");
               // $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("last week monday");
               // $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Tháng này':
                $datetimeTo = new DateTime('last day of this month');
              //  $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("first day of this month");
              //  $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Tháng trước':
                $datetimeTo = new DateTime("last day of last month");
              //  $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("first day of last month");
              //  $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Năm này':
                $datetimeTo = new DateTime('this year last day of december');
             //   $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime('this year first day of january');
             //   $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Năm trước':
                $datetimeTo = new DateTime('last year last day of december');
              //  $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime('last year first day of january');
              //  $datetimeFrom->setTime(0, 0, 0);
                break;

        }
        return array(
            'from' => $datetimeFrom,
            'to' => $datetimeTo
        );
}
function getDateTimeAsDbForAmount($period = 'this_month', $date_from = '', $date_to = '')
{
        
		$timedate = new TimeDate();
		date_default_timezone_set('Asia/Ho_Chi_Minh');
        if(empty($date_from) && empty($date_to)) {
            $range = getDateRangeToSearchForAmount($period);

            $from_by_user = $range['from']->format($timedate::DB_DATETIME_FORMAT);
            $to_by_user = $range['to']->format($timedate::DB_DATETIME_FORMAT);

        }
        else {

            $fromObj = $timedate->fromUserDate($date_from);
            $fromObj->setTime(0, 0, 0);
            $from_by_user = $fromObj->format($timedate::DB_DATETIME_FORMAT);

            $toObj = $timedate->fromUserDate($date_to);
            $toObj->setTime(23,59,59);
            $to_by_user = $toObj->format($timedate::DB_DATETIME_FORMAT);

        }

        $from_display = $timedate->to_display_date_time($from_by_user, true, false);
        $from = $timedate->to_db($from_display);

        $to_display = $timedate->to_display_date_time($to_by_user, true, false);
        $to = $timedate->to_db($to_display);

        $datetime_as_db = array(
            'from' => $from,
            'to' => $to
        );

        return $datetime_as_db;
}

if($_SERVER['REQUEST_METHOD'] == 'GET'){
	global $current_user;
	$current_user->id = $_GET["user"];
	try{
		global $db;
		$arr = array();
		$whr='';
		$sea = "";
		$myCate = "";
		$rowRes = array();
		if(isset($_GET['time']) && str_replace('.', ' ',$_GET["time"]) != "Tất cả"){
			$time = getDateTimeAsDbForAmount(str_replace('.', ' ',$_GET["time"]));
			$from=date('Y-m-d', strtotime($time['from']));
			$to=date('Y-m-d', strtotime($time['to']));
			if(date('m', strtotime($time['to'])) == "02" && $_GET['time'] == "Tháng này"){
				$to = date('Y', strtotime($time['to']))."-02-29";
			}
			$wh = " and aos_product_categories.date_entered >= '".$from."' and aos_product_categories.date_entered <= '".$to."'";
		}else{
			$wh= '';	
		}
		if($_GET['myCate'] == "1"){
			$myCate = " and aos_product_categories.assigned_user_id = '".$_GET["user"]."'";
		}
		if(isset($_GET['search'])){
			if(!empty($_GET['search'])){
				//$sea .= " and (aos_product_categories.name like '%".$_GET['search']."%' OR aos_product_categories.status like '%".$_GET['search']."%' OR aos_product_categories.description like '%".$_GET['search']."%')";
				$sea .= " and (aos_product_categories.name like '%".$_GET['search']."%')";
			}
		}
		$cate = BeanFactory::newBean('AOS_Product_Categories');
		//$lead->load_relationship('user_name');
		$where = "aos_product_categories.is_parent =  '1'".$wh.$sea.$myCate;
		$query = $cate->create_new_list_query("",$where,array());
		$query = $query." order by aos_product_categories_cstm.ordernum_c ASC";
		//$queryCount = "select count(aos_product_categories.id) as num from aos_product_categories inner join ($query) as aos_product_categorie on aos_product_categories.id = aos_product_categorie.id where aos_product_categories.deleted = '0'";
		//$resCount = $db->query($queryCount);
		//$rowCount = $db->fetchByAssoc($resCount);
		$res = $cate->db->query($query);
		while($row = $cate->db->fetchByAssoc($res)){
			$rowRes[] = $row;
		}
		echo json_encode($rowRes);
	}
	catch(PDOException $e) {
		echo "Error: " . $e->getMessage();
	}
}