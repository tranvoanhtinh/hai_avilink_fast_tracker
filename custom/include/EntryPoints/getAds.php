<?php
function getInfo_Ads()
{
	global $db;
	global $token;
	global $idForm;
	global $linkPage;
	$q = "select description,calluuid,psw_callcenter from tttt_worldfonepbx left join tttt_worldfonepbx_cstm on tttt_worldfonepbx.id = tttt_worldfonepbx_cstm.id_c   where tttt_worldfonepbx.deleted = '0' and tttt_worldfonepbx.type_sys_app = '1' and tttt_worldfonepbx_cstm.status_c = '1'";
	$r = $db->query($q);
	while($row = $db->fetchByAssoc($r)){
		if (empty($row["calluuid"])){
			$idForm = '';
		}else{
			$idForm = $row["calluuid"];
		}
		if (empty($row["description"])) {
			$linkPage = '';
		} else {
			$linkPage = $row["description"];
		}
		if (empty($row["psw_callcenter"])){
			$token = '';
		}else{
			$token = $row["psw_callcenter"];
		}
		if (!empty($idForm)) {
			try {
				process_Leads();
			} catch (Exception $ex) {
				LoggerManager::getLogger()->error($ex->getMessage());
			}
		} else {
			LoggerManager::getLogger()->error('There is no form id');
		}
	}
	return true;
}

function process_Leads($nextPage = null)
{
	global $db;
	global $token;
	global $idForm;
	global $linkPage;
    $curl = curl_init();
    if (empty($nextPage)) {
        $timestamp = getLast_Lead();
        $params = [
            'access_token' => $token,
            'fields' => 'created_time,id,ad_id,ad_name,campaign_id,campaign_name,form_id,field_data',
            'filtering' => '[{"field": "time_created", "operator": "GREATER_THAN", "value": ' . $timestamp . '}]',
        ];
        $url = 'https://graph.facebook.com/v15.0/' . $idForm . '/leads' . '?' . http_build_query($params);
    } else {
        $url = $nextPage;
    }
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
    ));

    $response = curl_exec($curl);
    $result = json_decode($response, true);

    if (!empty($result["data"])) {
        $lastCreated = '';
        foreach ($result["data"] as $lead) {
            // Kiểm tra đã tồn tại lead chưa
			$lastName = $phone = $email ='';
            foreach ($lead['field_data'] as $dt) {
                switch ($dt['name']) {
                    case 'số_điện_thoại':
                        $phone = $dt['values'][0];
                        break;
                    case 'tên_đầy_đủ':
                        $lastName = $dt['values'][0];
                        break;
					case 'email':
                        $email = $dt['values'][0];
                        break;
					case 'full_name':
                       $lastName = $dt['values'][0];
                        break;
					case 'phone_number':
                        $phone = $dt['values'][0];
                        break;
                    default:
                        // code...
                        break;
                }
            }
            $beanLead = BeanFactory::getBean('Leads');
			$beanAccount = BeanFactory::getBean('Accounts');
            $existLead = $beanLead->retrieve_by_string_fields(['refid_c' => $lead["id"]]);
			$existLeadByPhone = $beanLead->retrieve_by_string_fields(['phone_mobile'=>$phone]);
			$existLeadByConverted = $beanAccount->retrieve_by_string_fields(['phone_alternate'=>$phone]);
            if ($existLead || $existLeadByPhone || $existLeadByConverted) {
				if($existLeadByPhone || $existLeadByConverted){
					$idMeeting = create_guid();
					$time=date('Y-m-d h:i:s', strtotime(date('Y-m-d h:i:s').' + 5 hours'));
					$name="[Auto] - Khách hàng đã đăng ký dịch vụ";
					$query = "select id from leads where phone_mobile = '".$phone."' and deleted = '0'";
					$res = $db->query($query);
					$row = $db->fetchByAssoc($res);
					$qr="insert into meetings (id,name,description,parent_type,parent_id,assigned_user_id,date_start,date_end,date_entered,date_modified) values('".$idMeeting."',
					'".$name."','".$name."','Leads','".$row["id"]."','1','".$time."','".$time."','".$time."','".$time."')";
					$db->query($qr);
					$ml_id=create_guid();
					$qr="insert into meetings_leads (id,meeting_id,lead_id,date_modified) values('".$ml_id."','".$idMeeting."','".$row["id"]."','".$time."')";
					$db->query($qr);
					continue;
				}
			}
            if ($lastCreated == '') {
                $lastCreated = $lead['created_time'];
            }
            
			$id = create_guid();
			$idEmail = create_guid();
			$idEmailRelate = create_guid();
			$emailUpper = ($email != "")?strtoupper($email):"";
			$time=date('Y-m-d h:i:s', strtotime(date('Y-m-d h:i:s').' + 5 hours'));
            $newLead = BeanFactory::newBean("Leads");
			$newLead->new_with_id = true;
			$newLead->id = $id;
			$newLead->id_c = $id;
            $newLead->last_name = $lastName;
            $newLead->phone_mobile = $phone;
            $newLead->bussiness_type_c = "B2C";
            $newLead->status = "New";
            $newLead->assigned_user_id = "1";
            $newLead->refid_c = $lead["id"];
            $newLead->delivery_date = convert_DT($lead["created_time"]);
            $newLead->ads = isset($lead["ad_id"]) ? $lead["ad_id"] : '';
            $newLead->form_id_face = $idForm;
            $newLead->lead_source_description = $linkPage;
            $newLead->ads_name = isset($lead["ad_name"]) ? $lead["ad_name"] : '';
            $newLead->campaign_id_face_ads = isset($lead["campaign_id"]) ? $lead["campaign_id"] : '';
            $newLead->campaign_name_face_ads = isset($lead["campaign_name"]) ? $lead["campaign_name"] : '';
            $newLead->save();
			$leadSaved = $beanLead->retrieve_by_string_fields(['id' => $id]);
			if($leadSaved){
				if($email != ''){
					$q="insert into  email_addresses (id,email_address,email_address_caps,date_created,date_modified) values
					('".$idEmail."','".$email."','".$emailUpper."','".$time."','".$time."')";
					$db->query($q);
					$q="insert into  email_addr_bean_rel (id,email_address_id,bean_id,bean_module,date_created,date_modified) values
					('".$idEmailRelate."','".$idEmail."','".$id."','Leads','".$time."','".$time."')";
					$db->query($q);
				}
			}
        }
        // Ghi lại thời điểm lead cuối cùng được tạo
        if (empty($nextPage) && $lastCreated != '') {
            $fp = fopen('Ads.txt', 'w');
            fwrite($fp, $lastCreated);
            fclose($fp);
        }
        // Duyệt trang tiếp theo (đệ qui)
        if (!empty($result['paging']['next'])) {
            process_Leads($result['paging']['next']);
        }
    }
}

function getLast_Lead()
{
    $data = file_get_contents("Ads.txt");
    if (!empty($data)) {
        return convert_DT($data, true);
    }
    return 0;
}

function convert_DT($dateStr, $ts = false)
{
    $timezone = 'Asia/Ho_Chi_Minh';
    $date = new DateTime($dateStr, new DateTimeZone($timezone));
    if ($ts) {
        return $date->format('U');
    }
    return $date->format('Y-m-d H:i:s');
}
getInfo_Ads();