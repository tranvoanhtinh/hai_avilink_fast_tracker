<?php
global $db;

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    $rows = $_POST['rows'];
    $module = $_POST['module'];
    $booking_id = $_POST['booking_id'];

    $booking = BeanFactory::getBean('AOS_Bookings',$booking_id);
    $account_id = $booking->accounts_aos_bookings_1accounts_ida;

    $rows = html_entity_decode($rows);
    $rows = json_decode($rows, true);
 
    if (!is_array($rows) || empty($rows)) {
        echo json_encode(array("error" => "Invalid input data"));
        exit;
    }

    // Loại bỏ các phần tử rỗng trong mảng
    $rows = array_filter($rows, function($col) {
        return !empty($col);
    });

    // Tạo câu lệnh SQL với các cột được cung cấp và thêm type_currency, price
    $columns = implode(', ', array_map(function($col) {
        return htmlspecialchars($col);
    }, $rows));

      // Xây dựng câu SQL dựa trên có hoặc không có where_id
    if (isset($_POST['where_id']) && !empty($_POST['where_id'])) {
        $id = $db->quote($_POST['where_id']); // Bảo vệ khỏi SQL injection
        $sql = "SELECT $columns FROM $module WHERE deleted = '0' AND id = '".$id."'";
    } else {
        $sql = "SELECT $columns FROM $module WHERE deleted = '0'";
    }

    // Thực hiện truy vấn
    $result = $db->query($sql);

    $$module = array();

    if ($result && $db->getRowCount($result) > 0) {
        // Lấy dữ liệu từ kết quả truy vấn
        while ($row = $db->fetchByAssoc($result)) {
            $arr = array();
            $get_costprice = "SELECT * FROM aos_data_unitprice WHERE product_id = '".$row['id']."' AND account_id ='".$account_id."' AND deleted = '0'";
            $rs = $db->query($get_costprice);
            $row2 = $rs->fetch_assoc();
            foreach ($rows as $column) {
            if (isset($row[$column])) {
                $arr[$column] = $row[$column];
            }
                 $arr['price'] = isset($row2['price']) ? $row2['price'] : null;
            }
            $$module[] = $arr;
        }
    } else {
        echo json_encode(array("error" => "No records found"));
        exit;
    }

    // Trả về dữ liệu dưới dạng JSON
    echo json_encode($$module);
} else {
    echo json_encode(array("error" => "Invalid request method"));
}
