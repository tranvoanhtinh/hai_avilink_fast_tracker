<?php
global $db;
$whr='';
$q = " select id from users where id  = '".$_SESSION['authenticated_user_id']."' and is_admin = '1' and deleted = '0'";
$re = $db->query($q);
$r= $db->fetchByAssoc($re);
if(empty($r["id"])){
	$q2 = " select cs_cususer.id from cs_cususer inner join acl_roles on cs_cususer.role = acl_roles.id 
	where cs_cususer.refuser ='".$_SESSION['authenticated_user_id']."' and acl_roles.name <> 'Role cho nhan vien' and acl_roles.name <> 'TCT_ROLE'  and cs_cususer.deleted = '0'";
	$re2 = $db->query($q2);
	$r2= $db->fetchByAssoc($re2);
	if(empty($r2["id"])){
		$q3 = " select cs_cususer.id from cs_cususer inner join acl_roles on cs_cususer.role = acl_roles.id 
		where cs_cususer.refuser ='".$_SESSION['authenticated_user_id']."' and acl_roles.name = 'TCT_ROLE'  and cs_cususer.deleted = '0'";
		$re3 = $db->query($q3);
		$r3= $db->fetchByAssoc($re3);
		if(!empty($r3["id"])){
			$employee = false;
			$employer = true;
			$leader = false;
		}else{
				$employee = true;
				$employer = false;
				$leader = false;
			}
	}else{
		$employee = false;
		$employer = false;
		$leader = true;
	}
}else{
	$employee = false;
	$employer = true;
	$leader = false;
}

//$curr_id=$current_user->id;
$query="SELECT securitygroup_id FROM securitygroups_users where user_id='".$_SESSION['authenticated_user_id']."' and deleted = '0'";
$res=$db->query($query);
while($row=$db->fetchByAssoc($res)){
	$whr.="'".$row['securitygroup_id']."'";
	$whr.=",";	
}

$whr=trim($whr,",");

function getDateTimeAsDb($period = 'this_month', $date_from = '', $date_to = '')
    {
        global $timedate;
		date_default_timezone_set('Asia/Ho_Chi_Minh');
        if(empty($date_from) && empty($date_to)) {
            $range = getDateRangeToSearch($period);

            $from_by_user = $range['from']->format($timedate::DB_DATETIME_FORMAT);
            $to_by_user = $range['to']->format($timedate::DB_DATETIME_FORMAT);

        }
        else {

            $fromObj = $timedate->fromUserDate($date_from);
            $fromObj->setTime(0, 0, 0);
            $from_by_user = $fromObj->format($timedate::DB_DATETIME_FORMAT);

            $toObj = $timedate->fromUserDate($date_to);
            $toObj->setTime(23,59,59);
            $to_by_user = $toObj->format($timedate::DB_DATETIME_FORMAT);

        }

        $from_display = $timedate->to_display_date_time($from_by_user, true, false);
        $from = $timedate->to_db($from_display);

        $to_display = $timedate->to_display_date_time($to_by_user, true, false);
        $to = $timedate->to_db($to_display);

        $datetime_as_db = array(
            'from' => $from,
            'to' => $to
        );

        return $datetime_as_db;
    }
function getDateRangeToSearch($period_list_selected = 'this_month')
    {
        if(empty($period_list_selected)) $period_list_selected = 'this_month';
		date_default_timezone_set('Asia/Ho_Chi_Minh');
        switch ($period_list_selected) {
            case 'this_week':
                $datetimeTo = new DateTime("next week monday");
              //  $datetimeTo->setTime(23, 59, 59);
			   
                $datetimeFrom = new DateTime("this week tuesday");
				 
               // $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'last_week':
                $datetimeTo = new DateTime("this week monday");
              //  $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("last week tuesday");
              //  $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'this_month':
                $datetimeTo = new DateTime('last day of this month');
               // $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("first day of this month");
               // $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'last_month':
                $datetimeTo = new DateTime("last day of last month");
                //$datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("first day of last month");
               // $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'this_year':
                $datetimeTo = new DateTime('this year last day of december');
                //$datetimeTo->setTime(23, 59, 59);
				$datetimeTo->add(new DateInterval('P1D'));
                $datetimeFrom = new DateTime('this year first day of january');
               // $datetimeFrom->setTime(0, 0, 0);
			    $datetimeFrom->add(new DateInterval('P1D'));
                break;
            case 'last_year':
                $datetimeTo = new DateTime('last year last day of december');
				$datetimeTo->add(new DateInterval('P1D'));
               // $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime('last year first day of january');
               // $datetimeFrom->setTime(0, 0, 0);
			    $datetimeFrom->add(new DateInterval('P1D'));
                break;

        }
        return array(
            'from' => $datetimeFrom,
            'to' => $datetimeTo
        );
    }
	
$prov=array(
"10"=>" TP Hà Nội",
"16"=>"Tỉnh Hưng Yên",
"17"=>"Tỉnh Hải Dương",
"18"=>"Tp Hải Phòng",
"20"=>"Tỉnh Quảng Ninh",
"22"=>"Tỉnh Bắc Ninh",
"23"=>"Tỉnh Bắc Giang",
"24"=>"Tỉnh Lạng Sơn",
"25"=>"Tỉnh Thái Nguyên",
"26"=>"Tỉnh Bắc Kạn",
"27"=>"Tỉnh Cao Bằng",
"28"=>"Tỉnh Vĩnh Phúc",
"29"=>"Tỉnh Phú Thọ",
"30"=>"Tỉnh Tuyên Quang",
"31"=>"Tỉnh Hà Giang",
"32"=>"Tỉnh Yên Bái",
"33"=>"Tỉnh Lào Cai",
"35"=>"Tỉnh Hoà Bình",
"36"=>"Tỉnh Sơn La",
"38"=>"Tỉnh Điện Biên",
"39"=>"Tỉnh Lai Châu",
"40"=>"Tỉnh Hà Nam",
"41"=>"Tỉnh Thái Bình",
"42"=>"Tỉnh Nam Định",
"43"=>"Tỉnh Ninh Bình",
"44"=>"Tỉnh Thanh Hóa",
"46"=>"Tỉnh Nghệ An",
"48"=>"Tỉnh Hà Tĩnh",
"51"=>"Tỉnh Quảng Bình",
"52"=>"Tỉnh Quảng Trị",
"53"=>"Tỉnh Thừa Thiên Huế",
"55"=>"TP Đà Nẵng",
"56"=>"Tỉnh Quảng Nam",
"57"=>"Tỉnh Quảng Ngãi",
"58"=>"Tỉnh Kon Tum",
"59"=>"Tỉnh Bình Định",
"60"=>"Tỉnh Gia Lai",
"62"=>"Tỉnh Phú Yên",
"63"=>"Tỉnh Đắk Lăk",
"64"=>"Tỉnh Đắk Nông",
"65"=>"Tỉnh Khánh Hoà",
"66"=>"Tỉnh Ninh Thuận",
"67"=>"Tỉnh Lâm Đồng",
"68"=>"Tỉnh Hưng Yên",
"69"=>"Tỉnh Hưng Yên",
"70"=>"TP Hồ Chí Minh",
"79"=>"Tỉnh Bà Rịa Vũng Tàu",
"80"=>"Tỉnh Bình Thuận",
"81"=>"Tỉnh Đồng Nai",
"82"=>"Tỉnh Bình Dương",
"83"=>"Tỉnh Bình Phước",
"84"=>"Tỉnh Tây Ninh",
"85"=>"Tỉnh Long An",
"86"=>"Tỉnh Tiền Giang",
"87"=>"Tỉnh Đồng Tháp",
"88"=>"Tỉnh An Giang",
"89"=>"Tỉnh Vĩnh Long",
"90"=>"TP Cần Thơ",
"91"=>"Tỉnh Hậu Giang",
"92"=>"Tỉnh Kiên Giang",
"93"=>"Tỉnh Bến Tre",
"94"=>"Tỉnh Trà Vinh",
"95"=>"Tỉnh Sóc Trăng",
"96"=>"Tỉnh Bạc Liêu",
"97"=>"Tỉnh Cà Mau",
"98"=>"Vnpost",
"99"=>"PPTT",
"100"=>"DVBC",
"101"=>"TCBC",
"102"=>"HCC",
"103"=>"Global"
);

$user=array();
$group=array();
$province=array();
$limit=" Limit 0,20";
//$_POST['province']="10";
//$_POST['groupUsers']="All";

if((!empty($_POST['period'])) && ($_POST['period'] != 'default')){
			//if(empty($_POST['date_from']) && empty($_POST['date_to'])){
				if($_POST['period'] != "is_between"){
			$time=getDateTimeAsDb($_POST['period']);
				}
				else{
					if(empty($_POST['date_from']) && empty($_POST['date_to'])){
					$leadWhere="";
					$accountWhere="";
					$oppWhere="";
					$callWhere="";
					$meetingWhere="";
					$emailWhere="";
					}else{
						if(!empty($_POST['date_from']) && !empty($_POST['date_to'])){
							if(strtotime($_POST['date_from']) != strtotime($_POST['date_to'])){
								//$time=getDateTimeAsDb($_POST['period'],$_POST['date_from'],$_POST['date_to']);
								$from = date('Y-m-d', strtotime(str_replace("/","-",$_POST['date_from'])));	
								$to = date('Y-m-d', strtotime(str_replace("/","-",$_POST['date_to'])));	
							}else{
								$from1=date('Y-m-d', strtotime(str_replace("/","-",$_POST['date_from'])));	
							}
						}
					}
				}
				//}else{
			//$time=getDateTimeAsDb($_POST['period'],$_POST['date_from'],$_POST['date_to']);
			//	}
			if($time){
			$from=date('Y-m-d', strtotime($time['from']));
			$to=date('Y-m-d', strtotime($time['to']));
			}
			/*
			$leadWhere=" And FORMAT(leads.date_entered,'YYYY-mm-dd') >='".$from."' And FORMAT(leads.date_entered,'YYYY-mm-dd') <='".$to."'";
			$accountWhere=" And FORMAT(accounts.date_entered,'YYYY-mm-dd') >='".$from."' And FORMAT(accounts.date_entered,'YYYY-mm-dd') <='".$to."'";
			$oppWhere=" And FORMAT(opportunities.date_closed,'YYYY-mm-dd') >='".$from."' And FORMAT(opportunities.date_closed,'YYYY-mm-dd') <='".$to."'";
			$callWhere=" And FORMAT(calls.date_entered,'YYYY-mm-dd') >='".$from."' And FORMAT(calls.date_entered,'YYYY-mm-dd') <='".$to."'";
			$meetingWhere=" And FORMAT(meetings.date_entered,'YYYY-mm-dd') >='".$from."' And FORMAT(meetings.date_entered,'YYYY-mm-dd') <='".$to."'";
			$emailWhere=" And FORMAT(emails.date_entered,'YYYY-mm-dd') >='".$from."' And FORMAT(emails.date_entered,'YYYY-mm-dd') <='".$to."'";
			$accountWhereTran=" And FORMAT(opportunities.date_closed,'YYYY-mm-dd') >='".$from."' And FORMAT(opportunities.date_closed,'YYYY-mm-dd') <='".$to."'";
			*/
			if($from1){
			$leadWhere=" And DATE_FORMAT(leads.date_entered,'%Y-%m-%d') ='".$from1."'";
			$accountWhere=" And DATE_FORMAT(accounts.date_entered,'%Y-%m-%d') ='".$from1."'";
			$oppWhere=" And DATE_FORMAT(opportunities.date_closed,'%Y-%m-%d') ='".$from1."'";
			$callWhere=" And DATE_FORMAT(calls.date_start,'%Y-%m-%d') ='".$from1."'";
			$meetingWhere=" And DATE_FORMAT(meetings.date_start,'%Y-%m-%d') ='".$from1."'";
			$emailWhere=" And DATE_FORMAT(emails.date_entered,'%Y-%m-%d') ='".$from1."'";
			$accountWhereTran=" And DATE_FORMAT(opportunities.date_closed,'%Y-%m-%d') ='".$from1."'";
			$accountTransaction=" And DATE_FORMAT(accounts_cstm.datetransaction_c,'%Y-%m-%d') ='".$from1."'";
			}else{
			$leadWhere=" And DATE_FORMAT(leads.date_entered,'%Y-%m-%d') >='".$from."' And DATE_FORMAT(leads.date_entered,'%Y-%m-%d') <='".$to."'";
			$accountWhere=" And DATE_FORMAT(accounts.date_entered,'%Y-%m-%d') >='".$from."' And DATE_FORMAT(accounts.date_entered,'%Y-%m-%d') <='".$to."'";
			$oppWhere=" And DATE_FORMAT(opportunities.date_closed,'%Y-%m-%d') >='".$from."' And DATE_FORMAT(opportunities.date_closed,'%Y-%m-%d') <='".$to."'";
			$callWhere=" And DATE_FORMAT(calls.date_start,'%Y-%m-%d') >='".$from."' And DATE_FORMAT(calls.date_start,'%Y-%m-%d') <='".$to."'";
			$meetingWhere=" And DATE_FORMAT(meetings.date_start,'%Y-%m-%d') >='".$from."' And DATE_FORMAT(meetings.date_start,'%Y-%m-%d') <='".$to."'";
			$emailWhere=" And DATE_FORMAT(emails.date_entered,'%Y-%m-%d') >='".$from."' And DATE_FORMAT(emails.date_entered,'%Y-%m-%d') <='".$to."'";
			$accountWhereTran=" And DATE_FORMAT(opportunities.date_closed,'%Y-%m-%d') >='".$from."' And DATE_FORMAT(opportunities.date_closed,'%Y-%m-%d') <='".$to."'";
			$accountTransaction=" And DATE_FORMAT(accounts_cstm.datetransaction_c,'%Y-%m-%d') >='".$from."' And DATE_FORMAT(accounts_cstm.datetransaction_c,'%Y-%m-%d') <='".$to."'";
			}
}else{
	$leadWhere="";
	$accountWhere="";
	$oppWhere="";
	$callWhere="";
	$meetingWhere="";
	$emailWhere="";
}
	if($_POST["province"] != null){
		if($_POST["province"] != "All"){
			$New=0;
			$InProcess=0;
			$Converted=0;
			$Transaction=0;
			$CloseWon=0;
			$CloseLost=0;
			$Other=0;
			$Dead=0;
			$amountNew=0;
			$amountInProcess=0;
			$amountConverted=0;
			$amountTran=0;
			$amountWon=0;
			$amountLost=0;
			$amountDead=0;
			$amountCall=0;
			$amountMeeting=0;
			$amountEmail=0;
			$amountOther=0;
				if($_POST['groupUsers'] == null || $_POST['groupUsers'] == '' || $_POST['groupUsers'][0] == "All"){
					if($_POST["users"] == null || $_POST["users"] == ''){
						if($_POST["titleUsers"] == null || $_POST["titleUsers"] == ''){
						$res=$db->query($qr);
						$qr="select distinct(securitygroups.name) as seName,users.id as id,users.user_name as name,users.department as Se from users inner join users_cstm on users.id=users_cstm.id_c inner join 
						securitygroups_users on users.id=securitygroups_users.user_id inner join  securitygroups on securitygroups.id = securitygroups_users.securitygroup_id where users.deleted='0' and users.status='Active' and users_cstm.provincecode_c='".$_POST["province"]."'";
						while($row=$db->fetchByAssoc($res)){
							if(!empty($row)){
								$account = new Account();
								$accountQuery = $account->create_new_list_query("accounts.date_entered DESC","",array());
								$lead = new Lead();
								$leadQuery = $lead->create_new_list_query("leads.date_entered DESC","",array());
								$call = new Call();
								$callQuery = $call->create_new_list_query("calls.date_entered DESC","",array());$meeting = new Meeting();
								$meetingQuery = $meeting->create_new_list_query("meetings.date_entered DESC","",array());$opp = new Opportunity();
								$oppQuery = $opp->create_new_list_query("opportunities.date_entered DESC","",array());	
								$query21 = "select distinct(accounts.assigned_user_id) as assigned from opportunities inner join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id inner join 
								($accountQuery) as accounts on accounts.id = accounts_opportunities.account_id where opportunities.sales_stage = 'Closed Won' and opportunities.assigned_user_id ='".$row['id']."' and opportunities.deleted = '0'  and accounts.deleted = '0'".$accountWhereTran;
									$result21 = $db->query($query21);
									$whereAss = '';
									while($row21 = $db->fetchByAssoc($result21)){
										if(!empty($row21["assigned"])){
											$whereAss .= "'".$row21["assigned"]."',";
																		
										}
									}
															$whereAss = rtrim($whereAss,',');
									
									$query4 = $lead->create_new_list_query("leads.date_entered DESC","status = 'New' and assigned_user_id ='".$row['id']."'".$leadWhere.",".array());
									
									$result4=$db->query($query4);
									$row4 =$db->fetchByAssoc($result4);
									
									$query1="select count(id) as customer from ($leadQuery) as leads where leads.status ='In Process' and leads.deleted='0' and leads.assigned_user_id ='".$row['id']."'".$leadWhere;
									$result1=$db->query($query1);
									$row1 =$db->fetchByAssoc($result1);
										
									$query2="select count(accounts.id) as customer from ($accountQuery) as accounts inner join accounts_cstm on accounts.id=accounts_cstm.id_c inner join ($accountQuery) as account on accounts.id = account.id where accounts_cstm.lead_account_status_c 
									='Converted' and accounts.deleted='0' and accounts.assigned_user_id ='".$row['id']."'".$accountWhere;
									$result2=$db->query($query2);
									$row2 =$db->fetchByAssoc($result2);
									
									$query12="select count(id) as customer from ($queryLead) as leads inner join ($leadQuery) as lead on leads.id = lead.id where lead.status ='Dead' and deleted='0' and assigned_user_id ='".$row['id']."'".$leadWhere;
									$result12=$db->query($query12);
									$row12 =$db->fetchByAssoc($result12);
									$query5="select count(opportunities.id) as customer from opportunities inner join accounts_opportunities on opportunities.id=accounts_opportunities.opportunity_id inner join ($accountQuery) as accounts 
									on accounts_opportunities.account_id=accounts.id where opportunities.sales_stage='Closed Won' and opportunities.assigned_user_id ='".$row['id']."' and opportunities.deleted='0'".$accountWhereTran;
									$result5=$db->query($query5);
									$row5 =$db->fetchByAssoc($result5);
									
									$query6="select count(opportunities.id) as customer from opportunities inner join accounts_opportunities on opportunities.id=accounts_opportunities.opportunity_id inner join ($accountQuery) as  accounts 
									on accounts_opportunities.account_id=accounts.id where opportunities.sales_stage='Closed Lost' and accounts.assigned_user_id ='".$row['id']."' and accounts.deleted='0'".$oppWhere;
									$result6=$db->query($query6);
									$row6 =$db->fetchByAssoc($result6);
									$query7="select sum(opportunity_amount) as amount from ($leadQuery) as leads where status ='New' and deleted='0' and assigned_user_id ='".$row['id']."'".$leadWhere;
									$result7=$db->query($query7);
									$row7 =$db->fetchByAssoc($result7);
									if($row7['amount']==null || $row7['amount']==''){
										$row7['amount']=0;
									}
									$query8="select sum(opportunity_amount) as amount from  ($leadQuery) as leads where status ='In Process' and deleted='0' and assigned_user_id ='".$row['id']."'".$leadWhere;
									$result8=$db->query($query8);
									$row8 =$db->fetchByAssoc($result8);
									if($row8['amount'] == null || $row8['amount'] == ''){
										$row8['amount']=0;
									}
									$query9="select sum(amount) as amount from ($oppQuery) as opportunities where sales_stage Not In('Closed Won','Closed Lost') and assigned_user_id ='".$row['id']."' and deleted='0'".$accountWhereTran;
									$result9=$db->query($query9);
									$row9 =$db->fetchByAssoc($result9);
									if($row9['amount'] == null || $row9['amount'] == ''){
										$row9['amount']=0;
									}
									$query10="select sum(opportunities.amount) as amount  from opportunities  where opportunities.sales_stage = 'Closed Won' and opportunities.assigned_user_id ='".$row["id"]."' and opportunities.deleted = '0'".$accountWhereTran;
									$result10=$db->query($query10);
									$row10 =$db->fetchByAssoc($result10);
									if($row10['amount'] == null || $row10['amount'] == ''){
										$row10['amount']=0;
									}
									$query11="select sum(amount) as amount from opportunities where sales_stage='Closed Lost' and assigned_user_id ='".$row['id']."' and deleted='0'".$oppWhere;
									$result11=$db->query($query11);
									$row11 =$db->fetchByAssoc($result11);
									if($row11['amount'] == null || $row11['amount'] == ''){
										$row11['amount']=0;
									}
									$query13="select sum(opportunities.amount)  as amount from opportunities inner join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id
									inner join accounts_cstm on accounts_opportunities.account_id = accounts_cstm.id_c inner join ($accountQuery) as accounts on accounts.id = accounts_cstm.idc where opportunities.sales_stage NOT IN('Closed Won','Closed Lost') and opportunities.assigned_user_id ='".$row['id']."'
									and opportunities.deleted='0' and accounts_cstm.lead_account_status_c='Converted'".$oppWhere;
									$result13=$db->query($query13);
									$row13 =$db->fetchByAssoc($result13);
									if($row13['amount'] == null || $row13['amount'] == ''){
										$row13['amount']=0;
									}
									$query14="select sum(opportunity_amount) as amount from ($leadQuery) as leads where status ='Dead' and deleted='0' and assigned_user_id ='".$row['id']."'".$leadWhere;
									$result14=$db->query($query14);
									$row14 =$db->fetchByAssoc($result14);
									if($row14['amount']==null || $row14['amount']==''){
										$row14['amount']=0;
									}
									$query15="select count(id) as ca from ($callQuery) as  calls where deleted='0' and status='Held' and assigned_user_id ='".$row['id']."'".$callWhere;
									$result15=$db->query($query15);
									$row15 =$db->fetchByAssoc($result15);
									if($row15['ca']==null || $row15['ca']==''){
										$row15['ca']=0;
									}
									$query16="select count(id) as meeting from ($meetingQuery) as meetings  where deleted='0' and status='Held' and assigned_user_id ='".$row['id']."'".$meetingWhere;
									$result16=$db->query($query16);
									$row16 =$db->fetchByAssoc($result16);
									if($row16['meeting']==null || $row16['meeting']==''){
										$row16['meeting']=0;
									}
									$query17="select count(id) as email from emails where deleted='0' and assigned_user_id ='".$row['id']."'".$emailWhere;
									$result17=$db->query($query17);
									$row17 =$db->fetchByAssoc($result17);
									if($row17['email']==null || $row17['email']==''){
										$row17['email']=0;
									}
									$query18="select count(opportunities.id) as customer from opportunities inner join accounts_opportunities on opportunities.id=accounts_opportunities.opportunity_id inner join ($accountQuery) as accounts 
									on accounts_opportunities.account_id=accounts.id where opportunities.sales_stage not in('Closed Won','Closed Lost') and opportunities.assigned_user_id ='".$row['id']."' and opportunities.deleted='0'".$accountWhereTran;
									$result18=$db->query($query18);
									$row18 =$db->fetchByAssoc($result18);
									if($row18['customer']==null || $row18['customer']==''){
										$row18['customer']=0;
									}
									$query19 = "select accounts.id as id  from ($accountQuery) as accounts left join accounts_cstm on accounts_cstm.id_c = accounts.id where accounts.deleted = '0'  and accounts.assigned_user_id ='".$row["id"]."' and accounts_cstm.lead_account_status_c = 'Transaction'".$accountTransaction;
									$result19=$db->query($query19);
									$customerr = 0;
									$amountt = 0;
									while($row19 =$db->fetchByAssoc($result19)){
										$query20="select opportunities.amount as amount  from opportunities inner join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id inner join 
										accounts on accounts.id = accounts_opportunities.account_id where opportunities.sales_stage = 'Closed Won' and accounts.id ='".$row19['id']."' and opportunities.deleted = '0' ";
									$result20=$db->query($query20);
									$row20 =$db->fetchByAssoc($result20);
										if($row20['amount'] != null && $row20['amount'] != ''){
											$amountt+=$row20['amount'];
										}
									}
									$query23="select count(accounts.id) as customer  from accounts_cstm right join accounts on accounts_cstm.id_c = accounts.id where accounts.deleted = '0'  and accounts.assigned_user_id ='".$row["id"]."' and accounts_cstm.lead_account_status_c = 'Transaction'".$accountTransaction;
									$result23=$db->query($query23);
									$row23 =$db->fetchByAssoc($result23);
									if($row23['customer'] != null && $row23['customer'] != ''){
										$customerr = $row23['customer'];
									}
									if($employee == true){
										if($row["id"] == $_SESSION['authenticated_user_id']){
											$user=array(
													"user"=>$row['name'],
													"New"=>$row4["customer"],
													"userAmountNew"=>$row7['amount'],
													"InProcess"=>$row1["customer"],
													"userAmountInprocess"=>$row8['amount'],
													"Converted"=>$row2["customer"],
													"userAmountConverted"=>$row13['amount'],
													"Dead"=>$row12["customer"],
													"userAmountDead"=>$row14["amount"],
													"Transaction"=>$customerr,
													"userAmountTransaction"=>$amountt,
													"ClosedWon"=>$row5["customer"],
													"userAmountClosedWon"=>$row10['amount'],
													"Other"=>$row18["customer"],
													"userAmountOther"=>$row9['amount'],
													"ClosedLost"=>$row6["customer"],
													"userAmountClosedLost"=>$row11['amount'],
													"Call"=>$row15["ca"],
													"Meeting"=>$row16["meeting"],
													"Email"=>$row17["email"]
												);
											if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
												$group[$row["Se"]]=array();
												$group[$row["Se"]][]=$user;
											}else{
												$group[$row["Se"]][]=$user;
											}
											$New+=$row4["customer"];
											$InProcess+=$row1["customer"];
											$Dead+=$row12["customer"];
											$Converted+=$row2["customer"];
											$Transaction+=$customerr;
											$CloseWon+=$row5["customer"];
											$CloseLost+=$row6["customer"];
											$Other+=$row18["customer"];
											$amountNew+=$row7["amount"];
											$amountInProcess+=$row8["amount"];
											$amountDead+=$row14["amount"];
											$amountConverted+=$row13["amount"];
											$amountTran+=$amountt;
											$amountWon+=$row10["amount"];
											$amountLost+=$row11["amount"];
											$amountCall+=$row15["ca"];
											$amountMeeting+=$row16["meeting"];
											$amountEmail+=$row17["email"];
											$amountOther+=$row9["amount"];
										}
									}else{
										$user=array(
													"user"=>$row['name'],
													"New"=>$row4["customer"],
													"userAmountNew"=>$row7['amount'],
													"InProcess"=>$row1["customer"],
													"userAmountInprocess"=>$row8['amount'],
													"Converted"=>$row2["customer"],
													"userAmountConverted"=>$row13['amount'],
													"Dead"=>$row12["customer"],
													"userAmountDead"=>$row14["amount"],
													"Transaction"=>$customerr,
													"userAmountTransaction"=>$amountt,
													"ClosedWon"=>$row5["customer"],
													"userAmountClosedWon"=>$row10['amount'],
													"Other"=>$row18["customer"],
													"userAmountOther"=>$row9['amount'],
													"ClosedLost"=>$row6["customer"],
													"userAmountClosedLost"=>$row11['amount'],
													"Call"=>$row15["ca"],
													"Meeting"=>$row16["meeting"],
													"Email"=>$row17["email"]
												);
											if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
												$group[$row["Se"]]=array();
												$group[$row["Se"]][]=$user;
											}else{
												$group[$row["Se"]][]=$user;
											}
											$New+=$row4["customer"];
											$InProcess+=$row1["customer"];
											$Dead+=$row12["customer"];
											$Converted+=$row2["customer"];
											$Transaction+=$customerr;
											$CloseWon+=$row5["customer"];
											$CloseLost+=$row6["customer"];
											$Other+=$row18["customer"];
											$amountNew+=$row7["amount"];
											$amountInProcess+=$row8["amount"];
											$amountDead+=$row14["amount"];
											$amountConverted+=$row13["amount"];
											$amountTran+=$amountt;
											$amountWon+=$row10["amount"];
											$amountLost+=$row11["amount"];
											$amountCall+=$row15["ca"];
											$amountMeeting+=$row16["meeting"];
											$amountEmail+=$row17["email"];
											$amountOther+=$row9["amount"];
									}
									
							}
						}
					}else{
						for($i=0;$i<count($_POST["titleUsers"]);$i++){
							if($employer == false || $employee == true || $leader == true){
								if($leader == true){
									$q="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
									inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
									users.status='Active' and users_cstm.provincecode_c='".$_POST["province"]."' and securitygroups_users.securitygroup_id In (".$whr.") and users.title = '".$_POST["titleUsers"][$i]."'";
								}
								if($employee == true){
									$q="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
									inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
									users.status='Active' and users_cstm.provincecode_c='".$_POST["province"]."' and users.id =  '".$_SESSION['authenticated_user_id']."' and users.title = '".$_POST["titleUsers"][$i]."'";
								}
							}else{
								if($employer == true){
									$q="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
									inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
									users.status='Active' and users_cstm.provincecode_c='".$_POST["province"]."' and users.title = '".$_POST["titleUsers"][$i]."'";
								}
							}
							$r=$db->query($q);
							while($rww=$db->fetchByAssoc($r)){
								if(!empty($rww)){
									if($rww["seName"]==$rww["Se"]){
										$qr="select users.user_name as name,users.department as Se from users inner join securitygroups_users on users.id=securitygroups_users.user_id where users.deleted='0' and users.status='Active' and  users.id='".$rww["id"]."' and securitygroups_users.securitygroup_id In (".$whr.")";	
										$res=$db->query($qr);
										$row=$db->fetchByAssoc($res);
										if(!empty($row)){
											$query4="select count(id) as customer from ($queryLead) as leads where status ='New' and deleted='0' and assigned_user_id ='".$rww["id"]."'".$leadWhere;
											$result4=$db->query($query4);
											$row4 =$db->fetchByAssoc($result4);
											
											$query1="select count(id) as customer from ($queryLead) as leads where status ='In Process' and deleted='0' and assigned_user_id ='".$rww["id"]."'".$leadWhere;
											$result1=$db->query($query1);
											$row1 =$db->fetchByAssoc($result1);
												
											$query2="select count(accounts.id) as customer from ($accountQuery) as accounts inner join accounts_cstm on accounts.id=accounts_cstm.id_c where accounts_cstm.lead_account_status_c 
											='Converted' and accounts.deleted='0' and accounts.assigned_user_id ='".$rww["id"]."'".$accountWhere;
											$result2=$db->query($query2);
											$row2 =$db->fetchByAssoc($result2);
											
											$query12="select count(id) as customer from ($queryLead) as leads where status ='Dead' and deleted='0' and assigned_user_id ='".$rww["id"]."'".$leadWhere;
											$result12=$db->query($query12);
											$row12 =$db->fetchByAssoc($result12);
											
											
											
											$query5="select count(opportunities.id) as customer from opportunities inner join accounts_opportunities on opportunities.id=accounts_opportunities.opportunity_id inner join ($accountQuery) as accounts 
											on accounts_opportunities.account_id=accounts.id where opportunities.sales_stage='Closed Won' and opportunities.assigned_user_id ='".$rww["id"]."' and opportunities.deleted='0'".$oppWhere;
											$result5=$db->query($query5);
											$row5 =$db->fetchByAssoc($result5);
											
											$query6="select count(opportunities.id) as customer from opportunities inner join accounts_opportunities on opportunities.id=accounts_opportunities.opportunity_id inner join ($accountQuery) as accounts 
											on accounts_opportunities.account_id=accounts.id where opportunities.sales_stage='Closed Lost' and accounts.assigned_user_id ='".$rww["id"]."' and accounts.deleted='0'".$oppWhere;
											$result6=$db->query($query6);
											$row6 =$db->fetchByAssoc($result6);
											
											$query7="select sum(opportunity_amount) as amount from ($leadQuery) as leads where status ='New' and deleted='0' and assigned_user_id ='".$rww["id"]."'".$leadWhere;
											$result7=$db->query($query7);
											$row7 =$db->fetchByAssoc($result7);
											if($row7['amount']==null || $row7['amount']==''){
												$row7['amount']=0;
											}
											$query8="select sum(opportunity_amount) as amount from ($leadQuery) as leads where status ='In Process' and deleted='0' and assigned_user_id ='".$rww["id"]."'".$leadWhere;
											$result8=$db->query($query8);
											$row8 =$db->fetchByAssoc($result8);
											if($row8['amount'] == null || $row8['amount'] == ''){
												$row8['amount']=0;
											}
											$query9="select sum(amount) as amount from ($oppQuery) as opportunities where sales_stage Not In('Closed Won','Closed Lost') and assigned_user_id ='".$rww["id"]."' and deleted='0'".$oppWhere;
											$result9=$db->query($query9);
											$row9 =$db->fetchByAssoc($result9);
											if($row9['amount'] == null || $row9['amount'] == ''){
												$row9['amount']=0;
											}
											$query10="select sum(opportunities.amount) as amount  from opportunities  where opportunities.sales_stage = 'Closed Won' and opportunities.assigned_user_id ='".$rww["id"]."' and opportunities.deleted = '0'".$accountWhereTran;
											$result10=$db->query($query10);
											$row10 =$db->fetchByAssoc($result10);
											if($row10['amount'] == null || $row10['amount'] == ''){
												$row10['amount']=0;
											}
											$query11="select sum(amount) as amount from opportunities where sales_stage='Closed Lost' and assigned_user_id ='".$rww["id"]."' and deleted='0'".$oppWhere;
											$result11=$db->query($query11);
											$row11 =$db->fetchByAssoc($result11);
											if($row11['amount'] == null || $row11['amount'] == ''){
												$row11['amount']=0;
											}
											$query13="select sum(opportunities.amount)  as amount from opportunities inner join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id
												inner join accounts_cstm on accounts_opportunities.account_id = accounts_cstm.id_c where opportunities.sales_stage NOT IN('Closed Won','Closed Lost') and opportunities.assigned_user_id ='".$rww["id"]."'
												and opportunities.deleted='0' and accounts_cstm.lead_account_status_c='Converted'".$oppWhere;
											$result13=$db->query($query13);
											$row13 =$db->fetchByAssoc($result13);
											if($row13['amount'] == null || $row13['amount'] == ''){
												$row13['amount']=0;
											}
											$query14="select sum(opportunity_amount) as amount from ($leadQuery) as leads where status ='Dead' and deleted='0' and assigned_user_id ='".$rww["id"]."'".$leadWhere;
											$result14=$db->query($query14);
											$row14 =$db->fetchByAssoc($result14);
											if($row14['amount']==null || $row14['amount']==''){
												$row14['amount']=0;
											}
											$query15="select count(id) as ca from ($callQuery) as  calls where deleted='0' and status='Held' and assigned_user_id ='".$rww["id"]."'".$callWhere;
												$result15=$db->query($query15);
												$row15 =$db->fetchByAssoc($result15);
												if($row15['ca']==null || $row15['ca']==''){
													$row15['ca']=0;
											}
											$query16="select count(id) as meeting from ($meetingQuery) as meetings  where deleted='0' and status='Held' and assigned_user_id ='".$rww["id"]."'".$meetingWhere;
											$result16=$db->query($query16);
											$row16 =$db->fetchByAssoc($result16);
											if($row16['meeting']==null || $row16['meeting']==''){
												$row16['meeting']=0;
											}
											$query17="select count(id) as email from emails where deleted='0' and assigned_user_id ='".$rww["id"]."'".$emailWhere;
											$result17=$db->query($query17);
											$row17 =$db->fetchByAssoc($result17);
											if($row17['email']==null || $row17['email']==''){
												$row17['email']=0;
											}
											$query18="select count(opportunities.id) as customer from opportunities inner join accounts_opportunities on opportunities.id=accounts_opportunities.opportunity_id inner join ($accountQuery) as accounts 
											on accounts_opportunities.account_id=accounts.id where opportunities.sales_stage not in('Closed Won','Closed Lost') and accounts.assigned_user_id ='".$rww["id"]."' and accounts.deleted='0'".$oppWhere;
											$result18=$db->query($query18);
											$row18 =$db->fetchByAssoc($result18);
											if($row18['customer']==null || $row18['customer']==''){
												$row18['customer']=0;
											}
											$query19 = "select accounts.id as id  from ($accountQuery) as accounts left join accounts_cstm on accounts_cstm.id_c = accounts.id where accounts.deleted = '0'  and accounts.assigned_user_id = '".$_POST["users"][$i]."' and accounts_cstm.lead_account_status_c = 'Transaction'".$accountTransaction;
									$result19=$db->query($query19);
									$customerr = 0;
									$amountt = 0;
											while($row19 =$db->fetchByAssoc($result19)){
												 
												$query20="select opportunities.amount as amount  from opportunities inner join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id inner join 
												accounts on accounts.id = accounts_opportunities.account_id where opportunities.sales_stage = 'Closed Won' and accounts.id ='".$row19['id']."' and opportunities.deleted = '0'";
												$result20=$db->query($query20);
												$row20 =$db->fetchByAssoc($result20);
												if($row20['amount'] != null && $row20['amount'] != ''){
													$amountt+=$row20['amount'];
												}
											}
											$query23="select count(accounts.id) as customer  from accounts_cstm right join accounts on accounts_cstm.id_c = accounts.id where accounts.deleted = '0'  and accounts.assigned_user_id ='".$rww["id"]."' and accounts_cstm.lead_account_status_c = 'Transaction'".$accountTransaction;
									$result23=$db->query($query23);
									$row23 =$db->fetchByAssoc($result23);
									if($row23['customer'] != null && $row23['customer'] != ''){
										$customerr = $row23['customer'];
									}
											if($employee == true){
												if($rww["id"] == $_SESSION['authenticated_user_id']){
														$user=array(
															"user"=>$row['name'],
															"New"=>$row4["customer"],
															"userAmountNew"=>$row7['amount'],
															"InProcess"=>$row1["customer"],
															"userAmountInprocess"=>$row8['amount'],
															"Converted"=>$row2["customer"],
															"userAmountConverted"=>$row13['amount'],
															"Dead"=>$row12["customer"],
															"userAmountDead"=>$row14["amount"],
															"Transaction"=>$customerr,
															"userAmountTransaction"=>$amountt,
															"ClosedWon"=>$row5["customer"],
															"userAmountClosedWon"=>$row10['amount'],
															"Other"=>$row18["customer"],
															"userAmountOther"=>$row9['amount'],
															"ClosedLost"=>$row6["customer"],
															"userAmountClosedLost"=>$row11['amount'],
															"Call"=>$row15["ca"],
															"Meeting"=>$row16["meeting"],
															"Email"=>$row17["email"]
														);
													if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
														$group[$row["Se"]]=array();
														$group[$row["Se"]][]=$user;
													}else{
														$group[$row["Se"]][]=$user;
													}
													$New+=$row4["customer"];
													$InProcess+=$row1["customer"];
													$Dead+=$row12["customer"];
													$Converted+=$row2["customer"];
													$Transaction+=$customerr;
													$CloseWon+=$row5["customer"];
													$CloseLost+=$row6["customer"];
													$Other+=$row18["customer"];
													$amountNew+=$row7["amount"];
													$amountInProcess+=$row8["amount"];
													$amountDead+=$row14["amount"];
													$amountConverted+=$row13["amount"];
													$amountTran+=$amountt;
													$amountWon+=$row10["amount"];
													$amountLost+=$row11["amount"];
													$amountCall+=$row15["ca"];
													$amountMeeting+=$row16["meeting"];
													$amountEmail+=$row17["email"];
													$amountOther+=$row9["amount"];
												}
											}else{
												$user=array(
															"user"=>$row['name'],
															"New"=>$row4["customer"],
															"userAmountNew"=>$row7['amount'],
															"InProcess"=>$row1["customer"],
															"userAmountInprocess"=>$row8['amount'],
															"Converted"=>$row2["customer"],
															"userAmountConverted"=>$row13['amount'],
															"Dead"=>$row12["customer"],
															"userAmountDead"=>$row14["amount"],
															"Transaction"=>$customerr,
															"userAmountTransaction"=>$amountt,
															"ClosedWon"=>$row5["customer"],
															"userAmountClosedWon"=>$row10['amount'],
															"Other"=>$row18["customer"],
															"userAmountOther"=>$row9['amount'],
															"ClosedLost"=>$row6["customer"],
															"userAmountClosedLost"=>$row11['amount'],
															"Call"=>$row15["ca"],
															"Meeting"=>$row16["meeting"],
															"Email"=>$row17["email"]
														);
													if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
														$group[$row["Se"]]=array();
														$group[$row["Se"]][]=$user;
													}else{
														$group[$row["Se"]][]=$user;
													}
													$New+=$row4["customer"];
													$InProcess+=$row1["customer"];
													$Dead+=$row12["customer"];
													$Converted+=$row2["customer"];
													$Transaction+=$customerr;
													$CloseWon+=$row5["customer"];
													$CloseLost+=$row6["customer"];
													$Other+=$row18["customer"];
													$amountNew+=$row7["amount"];
													$amountInProcess+=$row8["amount"];
													$amountDead+=$row14["amount"];
													$amountConverted+=$row13["amount"];
													$amountTran+=$amountt;
													$amountWon+=$row10["amount"];
													$amountLost+=$row11["amount"];
													$amountCall+=$row15["ca"];
													$amountMeeting+=$row16["meeting"];
													$amountEmail+=$row17["email"];
													$amountOther+=$row9["amount"];
											}
										}
									}
								}
							}
						}
						
						
						
					}
						
					}
					
					
					else
					{
						for($i=0;$i<count($_POST["users"]);$i++){
							if($employer == false || $employee == true || $leader == true){
								if($leader == true){
									$qr="select users.user_name as name,users.department as Se from users inner join securitygroups_users on users.id=securitygroups_users.user_id where users.deleted='0' and users.status='Active' and  users.id='".$_POST["users"][$i]."' and securitygroups_users.securitygroup_id In (".$whr.")";	
								}
								if($employee == true){
									$qr="select users.user_name as name,users.department as Se from users inner join securitygroups_users on users.id=securitygroups_users.user_id where users.deleted='0' and users.status='Active' and  users.id='".$_POST["users"][$i]."' and users.id =  '".$_SESSION['authenticated_user_id']."'";	
								}
							}else{
								if($employer == true){
									$qr="select users.user_name as name,users.department as Se from users inner join securitygroups_users on users.id=securitygroups_users.user_id where users.deleted='0' and users.status='Active' and  users.id='".$_POST["users"][$i]."'";	
								}
								
							}
							
							$res=$db->query($qr);
							$row=$db->fetchByAssoc($res);
							if(!empty($row)){
								$query4="select count(id) as customer from ($queryLead) as leads where status ='New' and deleted='0' and assigned_user_id ='".$_POST["users"][$i]."'".$leadWhere;
								$result4=$db->query($query4);
								$row4 =$db->fetchByAssoc($result4);
								
								$query1="select count(id) as customer from ($queryLead) as leads where status ='In Process' and deleted='0' and assigned_user_id ='".$_POST["users"][$i]."'".$leadWhere;
								$result1=$db->query($query1);
								$row1 =$db->fetchByAssoc($result1);
									
								$query2="select count(accounts.id) as customer from ($accountQuery) as accounts inner join accounts_cstm on accounts.id=accounts_cstm.id_c where accounts_cstm.lead_account_status_c 
								='Converted' and accounts.deleted='0' and accounts.assigned_user_id ='".$_POST["users"][$i]."'".$accountWhere;
								$result2=$db->query($query2);
								$row2 =$db->fetchByAssoc($result2);
								
								$query12="select count(id) as customer from ($queryLead) as leads where status ='Dead' and deleted='0' and assigned_user_id ='".$_POST["users"][$i]."'".$leadWhere;
								$result12=$db->query($query12);
								$row12 =$db->fetchByAssoc($result12);
								
								
								
								$query5="select count(opportunities.id) as customer from opportunities inner join accounts_opportunities on opportunities.id=accounts_opportunities.opportunity_id inner join ($accountQuery) as accounts 
								on accounts_opportunities.account_id=accounts.id where opportunities.sales_stage='Closed Won' and opportunities.assigned_user_id ='".$_POST["users"][$i]."' and opportunities.deleted='0'".$oppWhere;
								$result5=$db->query($query5);
								$row5 =$db->fetchByAssoc($result5);
								
								$query6="select count(opportunities.id) as customer from opportunities inner join accounts_opportunities on opportunities.id=accounts_opportunities.opportunity_id inner join ($accountQuery) as accounts 
								on accounts_opportunities.account_id=accounts.id where opportunities.sales_stage='Closed Lost' and accounts.assigned_user_id ='".$_POST["users"][$i]."' and accounts.deleted='0'".$oppWhere;
								$result6=$db->query($query6);
								$row6 =$db->fetchByAssoc($result6);
								
								$query7="select sum(opportunity_amount) as amount from ($leadQuery) as leads where status ='New' and deleted='0' and assigned_user_id ='".$_POST["users"][$i]."'".$leadWhere;
								$result7=$db->query($query7);
								$row7 =$db->fetchByAssoc($result7);
								if($row7['amount']==null || $row7['amount']==''){
									$row7['amount']=0;
								}
								$query8="select sum(opportunity_amount) as amount from ($leadQuery) as leads where status ='In Process' and deleted='0' and assigned_user_id ='".$_POST["users"][$i]."'".$leadWhere;
								$result8=$db->query($query8);
								$row8 =$db->fetchByAssoc($result8);
								if($row8['amount'] == null || $row8['amount'] == ''){
									$row8['amount']=0;
								}
								$query9="select sum(amount) as amount from ($oppQuery) as opportunities where sales_stage Not In('Closed Won','Closed Lost') and assigned_user_id ='".$_POST["users"][$i]."' and deleted='0'".$oppWhere;
								$result9=$db->query($query9);
								$row9 =$db->fetchByAssoc($result9);
								if($row9['amount'] == null || $row9['amount'] == ''){
									$row9['amount']=0;
								}
								$query10="select sum(opportunities.amount) as amount  from opportunities  where opportunities.sales_stage = 'Closed Won' and opportunities.assigned_user_id ='".$_POST["users"][$i]."' and opportunities.deleted = '0'".$accountWhereTran;
								$result10=$db->query($query10);
								$row10 =$db->fetchByAssoc($result10);
								if($row10['amount'] == null || $row10['amount'] == ''){
									$row10['amount']=0;
								}
								$query11="select sum(amount) as amount from opportunities where sales_stage='Closed Lost' and assigned_user_id ='".$_POST["users"][$i]."' and deleted='0'".$oppWhere;
								$result11=$db->query($query11);
								$row11 =$db->fetchByAssoc($result11);
								if($row11['amount'] == null || $row11['amount'] == ''){
									$row11['amount']=0;
								}
								$query13="select sum(opportunities.amount)  as amount from opportunities inner join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id
									inner join accounts_cstm on accounts_opportunities.account_id = accounts_cstm.id_c where opportunities.sales_stage NOT IN('Closed Won','Closed Lost') and opportunities.assigned_user_id ='".$_POST["users"][$i]."'
									and opportunities.deleted='0' and accounts_cstm.lead_account_status_c='Converted'".$oppWhere;
								$result13=$db->query($query13);
								$row13 =$db->fetchByAssoc($result13);
								if($row13['amount'] == null || $row13['amount'] == ''){
									$row13['amount']=0;
								}
								$query14="select sum(opportunity_amount) as amount from ($leadQuery) as leads where status ='Dead' and deleted='0' and assigned_user_id ='".$_POST["users"][$i]."'".$leadWhere;
								$result14=$db->query($query14);
								$row14 =$db->fetchByAssoc($result14);
								if($row14['amount']==null || $row14['amount']==''){
									$row14['amount']=0;
								}
								$query15="select count(id) as ca from ($callQuery) as  calls where deleted='0' and status='Held' and assigned_user_id ='".$_POST["users"][$i]."'".$callWhere;
									$result15=$db->query($query15);
									$row15 =$db->fetchByAssoc($result15);
									if($row15['ca']==null || $row15['ca']==''){
										$row15['ca']=0;
								}
								$query16="select count(id) as meeting from ($meetingQuery) as meetings  where deleted='0' and status='Held' and assigned_user_id ='".$_POST["users"][$i]."'".$meetingWhere;
								$result16=$db->query($query16);
								$row16 =$db->fetchByAssoc($result16);
								if($row16['meeting']==null || $row16['meeting']==''){
									$row16['meeting']=0;
								}
								$query17="select count(id) as email from emails where deleted='0' and assigned_user_id ='".$_POST["users"][$i]."'".$emailWhere;
								$result17=$db->query($query17);
								$row17 =$db->fetchByAssoc($result17);
								if($row17['email']==null || $row17['email']==''){
									$row17['email']=0;
								}
								$query18="select count(opportunities.id) as customer from opportunities inner join accounts_opportunities on opportunities.id=accounts_opportunities.opportunity_id inner join ($accountQuery) as accounts 
								on accounts_opportunities.account_id=accounts.id where opportunities.sales_stage not in('Closed Won','Closed Lost') and accounts.assigned_user_id ='".$_POST["users"][$i]."' and accounts.deleted='0'".$oppWhere;
								$result18=$db->query($query18);
								$row18 =$db->fetchByAssoc($result18);
								if($row18['customer']==null || $row18['customer']==''){
									$row18['customer']=0;
								}
								
									$query19 = "select accounts.id as id  from ($accountQuery) as accounts left join accounts_cstm on accounts_cstm.id_c = accounts.id where accounts.deleted = '0'  and accounts.assigned_user_id ='".$_POST["users"][$i]."' and accounts_cstm.lead_account_status_c = 'Transaction'".$accountTransaction;
									$result19=$db->query($query19);
									$customerr = 0;
									$amountt = 0;
									while($row19 =$db->fetchByAssoc($result19)){
										 
										$query20="select opportunities.amount as amount  from opportunities inner join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id inner join 
										accounts on accounts.id = accounts_opportunities.account_id where opportunities.sales_stage = 'Closed Won' and accounts.id ='".$row19['id']."' and opportunities.deleted = '0' ";
									$result20=$db->query($query20);
									$row20 =$db->fetchByAssoc($result20);
										if($row20['amount'] != null && $row20['amount'] != ''){
											$amountt+=$row20['amount'];

										}
									}
									
									$query23="select count(accounts.id) as customer  from accounts_cstm right join accounts on accounts_cstm.id_c = accounts.id where accounts.deleted = '0'  and accounts.assigned_user_id ='".$_POST["users"][$i]."' and accounts_cstm.lead_account_status_c = 'Transaction'".$accountTransaction;
									$result23=$db->query($query23);
									$row23 =$db->fetchByAssoc($result23);
									if($row23['customer'] != null && $row23['customer'] != ''){
										$customerr = $row23['customer'];
									}
									if($employee == true){
										if($_POST["users"][$i] == $_SESSION['authenticated_user_id']){
											$user=array(
													"user"=>$row['name'],
													"New"=>$row4["customer"],
													"userAmountNew"=>$row7['amount'],
													"InProcess"=>$row1["customer"],
													"userAmountInprocess"=>$row8['amount'],
													"Converted"=>$row2["customer"],
													"userAmountConverted"=>$row13['amount'],
													"Dead"=>$row12["customer"],
													"userAmountDead"=>$row14["amount"],
													"Transaction"=>$customerr,
													"userAmountTransaction"=>$amountt,
													"ClosedWon"=>$row5["customer"],
													"userAmountClosedWon"=>$row10['amount'],
													"Other"=>$row18["customer"],
													"userAmountOther"=>$row9['amount'],
													"ClosedLost"=>$row6["customer"],
													"userAmountClosedLost"=>$row11['amount'],
													"Call"=>$row15["ca"],
													"Meeting"=>$row16["meeting"],
													"Email"=>$row17["email"]
												);
											if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
												$group[$row["Se"]]=array();
												$group[$row["Se"]][]=$user;
											}else{
												$group[$row["Se"]][]=$user;
											}
											$New+=$row4["customer"];
											$InProcess+=$row1["customer"];
											$Dead+=$row12["customer"];
											$Converted+=$row2["customer"];
											$Transaction+=$customerr;
											$CloseWon+=$row5["customer"];
											$CloseLost+=$row6["customer"];
											$Other+=$row18["customer"];
											$amountNew+=$row7["amount"];
											$amountInProcess+=$row8["amount"];
											$amountDead+=$row14["amount"];
											$amountConverted+=$row13["amount"];
											$amountTran+=$amountt;
											$amountWon+=$row10["amount"];
											$amountLost+=$row11["amount"];
											$amountCall+=$row15["ca"];
											$amountMeeting+=$row16["meeting"];
											$amountEmail+=$row17["email"];
											$amountOther+=$row9["amount"];
										}
									}else{
										$user=array(
													"user"=>$row['name'],
													"New"=>$row4["customer"],
													"userAmountNew"=>$row7['amount'],
													"InProcess"=>$row1["customer"],
													"userAmountInprocess"=>$row8['amount'],
													"Converted"=>$row2["customer"],
													"userAmountConverted"=>$row13['amount'],
													"Dead"=>$row12["customer"],
													"userAmountDead"=>$row14["amount"],
													"Transaction"=>$customerr,
													"userAmountTransaction"=>$amountt,
													"ClosedWon"=>$row5["customer"],
													"userAmountClosedWon"=>$row10['amount'],
													"Other"=>$row18["customer"],
													"userAmountOther"=>$row9['amount'],
													"ClosedLost"=>$row6["customer"],
													"userAmountClosedLost"=>$row11['amount'],
													"Call"=>$row15["ca"],
													"Meeting"=>$row16["meeting"],
													"Email"=>$row17["email"]
												);
											if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
												$group[$row["Se"]]=array();
												$group[$row["Se"]][]=$user;
											}else{
												$group[$row["Se"]][]=$user;
											}
											$New+=$row4["customer"];
											$InProcess+=$row1["customer"];
											$Dead+=$row12["customer"];
											$Converted+=$row2["customer"];
											$Transaction+=$customerr;
											$CloseWon+=$row5["customer"];
											$CloseLost+=$row6["customer"];
											$Other+=$row18["customer"];
											$amountNew+=$row7["amount"];
											$amountInProcess+=$row8["amount"];
											$amountDead+=$row14["amount"];
											$amountConverted+=$row13["amount"];
											$amountTran+=$amountt;
											$amountWon+=$row10["amount"];
											$amountLost+=$row11["amount"];
											$amountCall+=$row15["ca"];
											$amountMeeting+=$row16["meeting"];
											$amountEmail+=$row17["email"];
											$amountOther+=$row9["amount"];
									}
							}
						}
					}	
					
				}
				
				else
				{
					
					if($_POST["users"] == null || $_POST["users"] == ''){
						if($_POST["titleUsers"] == null || $_POST["titleUsers"] == ''){
						for($j=0;$j<count($_POST["groupUsers"]);$j++){
							$grs.="'".$_POST["groupUsers"][$j]."'";
							if($j<count($_POST["groupUsers"])-1){
								$grs.=",";	
							}
							if($_POST["groupUsers"][$j] == "All"){
								$grs="All";
								break;
							}
						}
						if($grs != "All"){
							if($employer == false || $employee == true || $leader == true){
								if($leader == true){
									$qr="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
									inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
									users.status='Active' and users_cstm.provincecode_c='".$_POST["province"]."' and securitygroups_users.securitygroup_id In (".$grs.") and securitygroups_users.securitygroup_id In (".$whr.")";
								}
								if($employee == true){
									$qr="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
									inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
									users.status='Active' and users_cstm.provincecode_c='".$_POST["province"]."' and securitygroups_users.securitygroup_id In (".$grs.") and users.id =  '".$_SESSION['authenticated_user_id']."'";
								}
							}else{
								if($employer == true){
									$qr="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
									inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
									users.status='Active' and users_cstm.provincecode_c='".$_POST["province"]."' and securitygroups_users.securitygroup_id In (".$grs.")";	
								}							
							}								
						}else{
							if($employer == false || $employee == true || $leader == true){
								$qr="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
								inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
								users.status='Active' and users_cstm.provincecode_c='".$_POST["province"]."' and securitygroups_users.securitygroup_id In (".$whr.")";	
							}else{
								if($employer == true){
									$qr="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
									inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
									users.status='Active' and users_cstm.provincecode_c='".$_POST["province"]."'";	
								}
								
							}
						}
						$res=$db->query($qr);
						while($row=$db->fetchByAssoc($res)){
							if(!empty($row)){
								if($row["seName"]==$row["Se"]){
								$query4="select count(id) as customer from ($queryLead) as leads where status ='New' and deleted='0' and assigned_user_id ='".$row['id']."'".$leadWhere;
								$result4=$db->query($query4);
								$row4 =$db->fetchByAssoc($result4);
								
								$query1="select count(id) as customer from ($queryLead) as leads where status ='In Process' and deleted='0' and assigned_user_id ='".$row['id']."'".$leadWhere;
								$result1=$db->query($query1);
								$row1 =$db->fetchByAssoc($result1);
									
								$query2="select count(accounts.id) as customer from ($accountQuery) as accounts inner join accounts_cstm on accounts.id=accounts_cstm.id_c where accounts_cstm.lead_account_status_c 
								='Converted' and accounts.deleted='0' and accounts.assigned_user_id ='".$row['id']."'".$accountWhere;
								$result2=$db->query($query2);
								$row2 =$db->fetchByAssoc($result2);
								
								$query12="select count(id) as customer from ($queryLead) as leads where status ='Dead' and deleted='0' and assigned_user_id ='".$row['id']."'".$leadWhere;
								$result12=$db->query($query12);
								$row12 =$db->fetchByAssoc($result12);
									
								
								
								$query5="select count(opportunities.id) as customer from opportunities inner join accounts_opportunities on opportunities.id=accounts_opportunities.opportunity_id inner join ($accountQuery) as accounts 
								on accounts_opportunities.account_id=accounts.id where opportunities.sales_stage='Closed Won' and opportunities.assigned_user_id ='".$row['id']."' and opportunities.deleted='0'".$oppWhere;
								$result5=$db->query($query5);
								$row5 =$db->fetchByAssoc($result5);
								
								$query6="select count(opportunities.id) as customer from opportunities inner join accounts_opportunities on opportunities.id=accounts_opportunities.opportunity_id inner join ($accountQuery) as accounts 
								on accounts_opportunities.account_id=accounts.id where opportunities.sales_stage='Closed Lost' and accounts.assigned_user_id ='".$row['id']."' and accounts.deleted='0'".$oppWhere;
								$result6=$db->query($query6);
								$row6 =$db->fetchByAssoc($result6);
								
								$query7="select sum(opportunity_amount) as amount from ($leadQuery) as leads where status ='New' and deleted='0' and assigned_user_id ='".$row['id']."'".$leadWhere;
								$result7=$db->query($query7);
								$row7 =$db->fetchByAssoc($result7);
								if($row7['amount']==null || $row7['amount']==''){
									$row7['amount']=0;
								}
								$query8="select sum(opportunity_amount) as amount from ($leadQuery) as leads where status ='In Process' and deleted='0' and assigned_user_id ='".$row['id']."'".$leadWhere;
								$result8=$db->query($query8);
								$row8 =$db->fetchByAssoc($result8);
								if($row8['amount'] == null || $row8['amount'] == ''){
										$row8['amount']=0;
								}
								$query9="select sum(amount) as amount from ($oppQuery) as opportunities where sales_stage Not In('Closed Won','Closed Lost') and assigned_user_id ='".$row['id']."' and deleted='0'".$oppWhere;
								$result9=$db->query($query9);
								$row9 =$db->fetchByAssoc($result9);
								if($row9['amount'] == null || $row9['amount'] == ''){
									$row9['amount']=0;
								}
								$query10="select sum(opportunities.amount) as amount  from opportunities  where opportunities.sales_stage = 'Closed Won' and opportunities.assigned_user_id ='".$row['id']."' and opportunities.deleted = '0'".$accountWhereTran;
								$result10=$db->query($query10);
								$row10 =$db->fetchByAssoc($result10);
								if($row10['amount'] == null || $row10['amount'] == ''){
									$row10['amount']=0;
								}
								$query11="select sum(amount) as amount from opportunities where sales_stage='Closed Lost' and assigned_user_id ='".$row['id']."' and deleted='0'".$oppWhere;
								$result11=$db->query($query11);
								$row11 =$db->fetchByAssoc($result11);
								if($row11['amount'] == null || $row11['amount'] == ''){
									$row11['amount']=0;
								}
								$query13="select sum(opportunities.amount) as amount from opportunities inner join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id
									inner join accounts_cstm on accounts_opportunities.account_id = accounts_cstm.id_c where opportunities.sales_stage NOT IN('Closed Won','Closed Lost') and opportunities.assigned_user_id ='".$row['id']."'
									and opportunities.deleted='0' and accounts_cstm.lead_account_status_c='Converted'".$oppWhere;
								$result13=$db->query($query13);
								$row13 =$db->fetchByAssoc($result13);
								if($row13['amount'] == null || $row13['amount'] == ''){
										$row13['amount']=0;
								}
								$query14="select sum(opportunity_amount) as amount from ($leadQuery) as leads where status ='Dead' and deleted='0' and assigned_user_id ='".$row['id']."'".$leadWhere;
								$result14=$db->query($query14);
								$row14 =$db->fetchByAssoc($result14);
								if($row14['amount']==null || $row14['amount']==''){
									$row14['amount']=0;
								}
								$query15="select count(id) as ca from ($callQuery) as  calls where deleted='0' and status='Held' and assigned_user_id ='".$row['id']."'".$callWhere;
								$result15=$db->query($query15);
								$row15 =$db->fetchByAssoc($result15);
								if($row15['ca']==null || $row15['ca']==''){
									$row15['ca']=0;
								}
								$query16="select count(id) as meeting from ($meetingQuery) as meetings  where deleted='0' and status='Held' and assigned_user_id ='".$row['id']."'".$meetingWhere;
								$result16=$db->query($query16);
								$row16 =$db->fetchByAssoc($result16);
								if($row16['meeting']==null || $row16['meeting']==''){
									$row16['meeting']=0;
								}
								$query17="select count(id) as email from emails where deleted='0' and assigned_user_id ='".$row['id']."'".$emailWhere;
								$result17=$db->query($query17);
								$row17 =$db->fetchByAssoc($result17);
								if($row17['email']==null || $row17['email']==''){
									$row17['email']=0;
								}
								$query18="select count(opportunities.id) as customer from opportunities inner join accounts_opportunities on opportunities.id=accounts_opportunities.opportunity_id inner join ($accountQuery) as accounts 
								on accounts_opportunities.account_id=accounts.id where opportunities.sales_stage not in('Closed Won','Closed Lost') and accounts.assigned_user_id ='".$row['id']."' and accounts.deleted='0'".$oppWhere;
								$result18=$db->query($query18);
								$row18 =$db->fetchByAssoc($result18);
								if($row18['customer']==null || $row18['customer']==''){
									$row18['customer']=0;
								}
								$query21 = "select distinct(accounts.assigned_user_id) as assigned from opportunities inner join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id inner join 
									accounts on accounts.id = accounts_opportunities.account_id where opportunities.sales_stage = 'Closed Won' and opportunities.assigned_user_id ='".$row['id']."' and opportunities.deleted = '0'  and accounts.deleted = '0'".$accountWhereTran;
									$result21 = $db->query($query21);
									$whereAss = '';
									while($row21 = $db->fetchByAssoc($result21)){
										if(!empty($row21["assigned"])){
											$whereAss .= "'".$row21["assigned"]."',";
											
										}
									}
									$query19 = "select accounts.id as id  from ($accountQuery) as accounts left join accounts_cstm on accounts_cstm.id_c = accounts.id where accounts.deleted = '0'  and accounts.assigned_user_id ='".$row["id"]."' and accounts_cstm.lead_account_status_c = 'Transaction'".$accountTransaction;
									$result19=$db->query($query19);
									$customerr = 0;
									$amountt = 0;
									while($row19 =$db->fetchByAssoc($result19)){
										 
										$query20="select opportunities.amount as amount  from opportunities inner join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id inner join 
										accounts on accounts.id = accounts_opportunities.account_id where opportunities.sales_stage = 'Closed Won' and accounts.id ='".$row19['id']."' and opportunities.deleted = '0' ";
									$result20=$db->query($query20);
									$row20 =$db->fetchByAssoc($result20);
										if($row20['amount'] != null && $row20['amount'] != ''){
											$amountt+=$row20['amount'];

										}
									}
									$query23="select count(accounts.id) as customer  from accounts_cstm right join accounts on accounts_cstm.id_c = accounts.id where accounts.deleted = '0'  and accounts.assigned_user_id ='".$row["id"]."' and accounts_cstm.lead_account_status_c = 'Transaction'".$accountTransaction;
									$result23=$db->query($query23);
									$row23 =$db->fetchByAssoc($result23);
									if($row23['customer'] != null && $row23['customer'] != ''){
										$customerr = $row23['customer'];
									}
									if($employee == true){
											if($row['id'] == $_SESSION['authenticated_user_id']){	
												$user=array(
														"user"=>$row['name'],
														"New"=>$row4["customer"],
														"userAmountNew"=>$row7['amount'],
														"InProcess"=>$row1["customer"],
														"userAmountInprocess"=>$row8['amount'],
														"Converted"=>$row2["customer"],
														"userAmountConverted"=>$row13['amount'],
														"Dead"=>$row12["customer"],
														"userAmountDead"=>$row14["amount"],
														"Transaction"=>$customerr,
														"userAmountTransaction"=>$amountt,
														"ClosedWon"=>$row5["customer"],
														"userAmountClosedWon"=>$row10['amount'],
														"Other"=>$row18["customer"],
														"userAmountOther"=>$row9['amount'],
														"ClosedLost"=>$row6["customer"],
														"userAmountClosedLost"=>$row11['amount'],
														"Call"=>$row15["ca"],
														"Meeting"=>$row16["meeting"],
														"Email"=>$row17["email"]
													);
												if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
														$group[$row["Se"]]=array();
														$group[$row["Se"]][]=$user;
												}
												else{
														$group[$row["Se"]][]=$user;
												}
												$New+=$row4["customer"];
												$InProcess+=$row1["customer"];
												$Converted+=$row2["customer"];
												$Dead+=$row12["customer"];
												$Transaction+=$customerr;
												$CloseWon+=$row5["customer"];
												$CloseLost+=$row6["customer"];
												$Other+=$row18["customer"];
												$amountNew+=$row7["amount"];
												$amountInProcess+=$row8["amount"];
												$amountDead+=$row14["amount"];
												$amountConverted+=$row13["amount"];
												$amountTran+=$amountt;
												$amountWon+=$row10["amount"];
												$amountLost+=$row11["amount"];
												$amountCall+=$row15["ca"];
												$amountMeeting+=$row16["meeting"];
												$amountEmail+=$row17["email"];
												$amountOther+=$row9["amount"];
										}
									}else{
										$user=array(
												"user"=>$row['name'],
												"New"=>$row4["customer"],
												"userAmountNew"=>$row7['amount'],
												"InProcess"=>$row1["customer"],
												"userAmountInprocess"=>$row8['amount'],
												"Converted"=>$row2["customer"],
												"userAmountConverted"=>$row13['amount'],
												"Dead"=>$row12["customer"],
												"userAmountDead"=>$row14["amount"],
												"Transaction"=>$customerr,
												"userAmountTransaction"=>$amountt,
												"ClosedWon"=>$row5["customer"],
												"userAmountClosedWon"=>$row10['amount'],
												"Other"=>$row18["customer"],
												"userAmountOther"=>$row9['amount'],
												"ClosedLost"=>$row6["customer"],
												"userAmountClosedLost"=>$row11['amount'],
												"Call"=>$row15["ca"],
												"Meeting"=>$row16["meeting"],
												"Email"=>$row17["email"]
											);
										if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
												$group[$row["Se"]]=array();
												$group[$row["Se"]][]=$user;
										}
										else{
												$group[$row["Se"]][]=$user;
										}
										$New+=$row4["customer"];
										$InProcess+=$row1["customer"];
										$Converted+=$row2["customer"];
										$Dead+=$row12["customer"];
										$Transaction+=$customerr;
										$CloseWon+=$row5["customer"];
										$CloseLost+=$row6["customer"];
										$Other+=$row18["customer"];
										$amountNew+=$row7["amount"];
										$amountInProcess+=$row8["amount"];
										$amountDead+=$row14["amount"];
										$amountConverted+=$row13["amount"];
										$amountTran+=$amountt;
										$amountWon+=$row10["amount"];
										$amountLost+=$row11["amount"];
										$amountCall+=$row15["ca"];
										$amountMeeting+=$row16["meeting"];
										$amountEmail+=$row17["email"];
										$amountOther+=$row9["amount"];
									}
								}
							}
						}
						}else{
							for($j=0;$j<count($_POST["groupUsers"]);$j++){
									$grs.="'".$_POST["groupUsers"][$j]."'";
									if($j<count($_POST["groupUsers"])-1){
										$grs.=",";	
									}
									if($_POST["groupUsers"][$j] == "All"){
										$grs="All";
										break;
									}
							}
							for($i=0;$i<count($_POST["titleUsers"]);$i++){
								if($grs != "All"){
									$q="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
									inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
									users.status='Active' and users_cstm.provincecode_c='".$_POST["province"]."' and securitygroups_users.securitygroup_id In (".$whr.") and securitygroups_users.securitygroup_id In (".$grs.") and users.title='".$_POST["titleUsers"][$i]."'";
								}else{
									$q="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
									inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
									users.status='Active' and users_cstm.provincecode_c='".$_POST["province"]."' and securitygroups_users.securitygroup_id In (".$whr.") and users.title='".$_POST["titleUsers"][$i]."'";
								}
							$r=$db->query($q);
							while($rww=$db->fetchByAssoc($r)){
								if(!empty($rww)){
									if($rww["seName"]==$rww["Se"]){
										$qr="select users.user_name as name,users.department as Se from users inner join securitygroups_users on users.id=securitygroups_users.user_id where users.deleted='0' and users.status='Active' and  users.id='".$rww["id"]."' and securitygroups_users.securitygroup_id In (".$whr.")";	
										$res=$db->query($qr);
										$row=$db->fetchByAssoc($res);
										if(!empty($row)){
											$query4="select count(id) as customer from ($queryLead) as leads where status ='New' and deleted='0' and assigned_user_id ='".$rww["id"]."'".$leadWhere;
											$result4=$db->query($query4);
											$row4 =$db->fetchByAssoc($result4);
											
											$query1="select count(id) as customer from ($queryLead) as leads where status ='In Process' and deleted='0' and assigned_user_id ='".$rww["id"]."'".$leadWhere;
											$result1=$db->query($query1);
											$row1 =$db->fetchByAssoc($result1);
												
											$query2="select count(accounts.id) as customer from ($accountQuery) as accounts inner join accounts_cstm on accounts.id=accounts_cstm.id_c where accounts_cstm.lead_account_status_c 
											='Converted' and accounts.deleted='0' and accounts.assigned_user_id ='".$rww["id"]."'".$accountWhere;
											$result2=$db->query($query2);
											$row2 =$db->fetchByAssoc($result2);
											
											$query12="select count(id) as customer from ($queryLead) as leads where status ='Dead' and deleted='0' and assigned_user_id ='".$rww["id"]."'".$leadWhere;
											$result12=$db->query($query12);
											$row12 =$db->fetchByAssoc($result12);
											
											
											
											$query5="select count(opportunities.id) as customer from opportunities inner join accounts_opportunities on opportunities.id=accounts_opportunities.opportunity_id inner join ($accountQuery) as accounts 
											on accounts_opportunities.account_id=accounts.id where opportunities.sales_stage='Closed Won' and opportunities.assigned_user_id ='".$rww["id"]."' and opportunities.deleted='0'".$oppWhere;
											$result5=$db->query($query5);
											$row5 =$db->fetchByAssoc($result5);
											
											$query6="select count(opportunities.id) as customer from opportunities inner join accounts_opportunities on opportunities.id=accounts_opportunities.opportunity_id inner join ($accountQuery) as accounts 
											on accounts_opportunities.account_id=accounts.id where opportunities.sales_stage='Closed Lost' and accounts.assigned_user_id ='".$rww["id"]."' and accounts.deleted='0'".$oppWhere;
											$result6=$db->query($query6);
											$row6 =$db->fetchByAssoc($result6);
											
											$query7="select sum(opportunity_amount) as amount from ($leadQuery) as leads where status ='New' and deleted='0' and assigned_user_id ='".$rww["id"]."'".$leadWhere;
											$result7=$db->query($query7);
											$row7 =$db->fetchByAssoc($result7);
											if($row7['amount']==null || $row7['amount']==''){
												$row7['amount']=0;
											}
											$query8="select sum(opportunity_amount) as amount from ($leadQuery) as leads where status ='In Process' and deleted='0' and assigned_user_id ='".$rww["id"]."'".$leadWhere;
											$result8=$db->query($query8);
											$row8 =$db->fetchByAssoc($result8);
											if($row8['amount'] == null || $row8['amount'] == ''){
												$row8['amount']=0;
											}
											$query9="select sum(amount) as amount from ($oppQuery) as opportunities where sales_stage Not In('Closed Won','Closed Lost') and assigned_user_id ='".$rww["id"]."' and deleted='0'".$oppWhere;
											$result9=$db->query($query9);
											$row9 =$db->fetchByAssoc($result9);
											if($row9['amount'] == null || $row9['amount'] == ''){
												$row9['amount']=0;
											}
											$query10="select sum(opportunities.amount) as amount  from opportunities  where opportunities.sales_stage = 'Closed Won' and opportunities.assigned_user_id ='".$rww["id"]."' and opportunities.deleted = '0'".$accountWhereTran;
											$result10=$db->query($query10);
											$row10 =$db->fetchByAssoc($result10);
											if($row10['amount'] == null || $row10['amount'] == ''){
												$row10['amount']=0;
											}
											$query11="select sum(amount) as amount from opportunities where sales_stage='Closed Lost' and assigned_user_id ='".$rww["id"]."' and deleted='0'".$oppWhere;
											$result11=$db->query($query11);
											$row11 =$db->fetchByAssoc($result11);
											if($row11['amount'] == null || $row11['amount'] == ''){
												$row11['amount']=0;
											}
											$query13="select sum(opportunities.amount) as amount from opportunities inner join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id
												inner join accounts_cstm on accounts_opportunities.account_id = accounts_cstm.id_c where opportunities.sales_stage NOT IN('Closed Won','Closed Lost') and opportunities.assigned_user_id ='".$rww["id"]."'
												and opportunities.deleted='0' and accounts_cstm.lead_account_status_c='Converted'".$oppWhere;
											$result13=$db->query($query13);
											$row13 =$db->fetchByAssoc($result13);
											if($row13['amount'] == null || $row13['amount'] == ''){
												$row13['amount']=0;
											}
											$query14="select sum(opportunity_amount) as amount from ($leadQuery) as leads where status ='Dead' and deleted='0' and assigned_user_id ='".$rww["id"]."'".$leadWhere;
											$result14=$db->query($query14);
											$row14 =$db->fetchByAssoc($result14);
											if($row14['amount']==null || $row14['amount']==''){
												$row14['amount']=0;
											}
											$query15="select count(id) as ca from ($callQuery) as  calls where deleted='0' and status='Held' and assigned_user_id ='".$rww["id"]."'".$callWhere;
												$result15=$db->query($query15);
												$row15 =$db->fetchByAssoc($result15);
												if($row15['ca']==null || $row15['ca']==''){
													$row15['ca']=0;
											}
											$query16="select count(id) as meeting from ($meetingQuery) as meetings  where deleted='0' and status='Held' and assigned_user_id ='".$rww["id"]."'".$meetingWhere;
											$result16=$db->query($query16);
											$row16 =$db->fetchByAssoc($result16);
											if($row16['meeting']==null || $row16['meeting']==''){
												$row16['meeting']=0;
											}
											$query17="select count(id) as email from emails where deleted='0' and assigned_user_id ='".$rww["id"]."'".$emailWhere;
											$result17=$db->query($query17);
											$row17 =$db->fetchByAssoc($result17);
											if($row17['email']==null || $row17['email']==''){
												$row17['email']=0;
											}
											$query18="select count(opportunities.id) as customer from opportunities inner join accounts_opportunities on opportunities.id=accounts_opportunities.opportunity_id inner join ($accountQuery) as accounts 
											on accounts_opportunities.account_id=accounts.id where opportunities.sales_stage not in('Closed Won','Closed Lost') and accounts.assigned_user_id ='".$rww["id"]."' and accounts.deleted='0'".$oppWhere;
											$result18=$db->query($query18);
											$row18 =$db->fetchByAssoc($result18);
											if($row18['customer']==null || $row18['customer']==''){
												$row18['customer']=0;
											}
											$query21 = "select distinct(accounts.assigned_user_id) as assigned from opportunities inner join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id inner join 
											accounts on accounts.id = accounts_opportunities.account_id where opportunities.sales_stage = 'Closed Won' and opportunities.assigned_user_id ='".$rww['id']."' and opportunities.deleted = '0'  and accounts.deleted = '0'".$accountWhereTran;
											$result21 = $db->query($query21);
											$whereAss = '';
											while($row21 = $db->fetchByAssoc($result21)){
												if(!empty($row21["assigned"])){
													$whereAss .= "'".$row21["assigned"]."',";
													
												}
											}
											$query19 = "select accounts.id as id  from ($accountQuery) as accounts left join accounts_cstm on accounts_cstm.id_c = accounts.id where accounts.deleted = '0'  and accounts.assigned_user_id ='".$rww["id"]."' and accounts_cstm.lead_account_status_c = 'Transaction'".$accountTransaction;
									$result19=$db->query($query19);
									$customerr = 0;
									$amountt = 0;
									while($row19 =$db->fetchByAssoc($result19)){
										 
										$query20="select opportunities.amount as amount  from opportunities inner join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id inner join 
										accounts on accounts.id = accounts_opportunities.account_id where opportunities.sales_stage = 'Closed Won' and accounts.id ='".$row19['id']."' and opportunities.deleted = '0' ";
									$result20=$db->query($query20);
									$row20 =$db->fetchByAssoc($result20);
										if($row20['amount'] != null && $row20['amount'] != ''){
											$amountt+=$row20['amount'];

										}
									}
											$query23="select count(accounts.id) as customer  from accounts_cstm right join accounts on accounts_cstm.id_c = accounts.id where accounts.deleted = '0'  and accounts.assigned_user_id ='".$rww["id"]."' and accounts_cstm.lead_account_status_c = 'Transaction'".$accountTransaction;
									$result23=$db->query($query23);
									$row23 =$db->fetchByAssoc($result23);
									if($row23['customer'] != null && $row23['customer'] != ''){
										$customerr = $row23['customer'];
									}
											if($employee == true){
												if($rww['id'] == $_SESSION['authenticated_user_id']){	
													$user=array(
															"user"=>$row['name'],
															"New"=>$row4["customer"],
															"userAmountNew"=>$row7['amount'],
															"InProcess"=>$row1["customer"],
															"userAmountInprocess"=>$row8['amount'],
															"Converted"=>$row2["customer"],
															"userAmountConverted"=>$row13['amount'],
															"Dead"=>$row12["customer"],
															"userAmountDead"=>$row14["amount"],
															"Transaction"=>$customerr,
															"userAmountTransaction"=>$amountt,
															"ClosedWon"=>$row5["customer"],
															"userAmountClosedWon"=>$row10['amount'],
															"Other"=>$row18["customer"],
															"userAmountOther"=>$row9['amount'],
															"ClosedLost"=>$row6["customer"],
															"userAmountClosedLost"=>$row11['amount'],
															"Call"=>$row15["ca"],
															"Meeting"=>$row16["meeting"],
															"Email"=>$row17["email"]
														);
													if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
														$group[$row["Se"]]=array();
														$group[$row["Se"]][]=$user;
													}else{
														$group[$row["Se"]][]=$user;
													}
													$New+=$row4["customer"];
													$InProcess+=$row1["customer"];
													$Dead+=$row12["customer"];
													$Converted+=$row2["customer"];
													$Transaction+=$customerr;
													$CloseWon+=$row5["customer"];
													$CloseLost+=$row6["customer"];
													$Other+=$row18["customer"];
													$amountNew+=$row7["amount"];
													$amountInProcess+=$row8["amount"];
													$amountDead+=$row14["amount"];
													$amountConverted+=$row13["amount"];
													$amountTran+=$amountt;
													$amountWon+=$row10["amount"];
													$amountLost+=$row11["amount"];
													$amountCall+=$row15["ca"];
													$amountMeeting+=$row16["meeting"];
													$amountEmail+=$row17["email"];
													$amountOther+=$row9["amount"];
												}
											}else{
												$user=array(
															"user"=>$row['name'],
															"New"=>$row4["customer"],
															"userAmountNew"=>$row7['amount'],
															"InProcess"=>$row1["customer"],
															"userAmountInprocess"=>$row8['amount'],
															"Converted"=>$row2["customer"],
															"userAmountConverted"=>$row13['amount'],
															"Dead"=>$row12["customer"],
															"userAmountDead"=>$row14["amount"],
															"Transaction"=>$customerr,
															"userAmountTransaction"=>$amountt,
															"ClosedWon"=>$row5["customer"],
															"userAmountClosedWon"=>$row10['amount'],
															"Other"=>$row18["customer"],
															"userAmountOther"=>$row9['amount'],
															"ClosedLost"=>$row6["customer"],
															"userAmountClosedLost"=>$row11['amount'],
															"Call"=>$row15["ca"],
															"Meeting"=>$row16["meeting"],
															"Email"=>$row17["email"]
														);
													if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
														$group[$row["Se"]]=array();
														$group[$row["Se"]][]=$user;
													}else{
														$group[$row["Se"]][]=$user;
													}
													$New+=$row4["customer"];
													$InProcess+=$row1["customer"];
													$Dead+=$row12["customer"];
													$Converted+=$row2["customer"];
													$Transaction+=$customerr;
													$CloseWon+=$row5["customer"];
													$CloseLost+=$row6["customer"];
													$Other+=$row18["customer"];
													$amountNew+=$row7["amount"];
													$amountInProcess+=$row8["amount"];
													$amountDead+=$row14["amount"];
													$amountConverted+=$row13["amount"];
													$amountTran+=$amountt;
													$amountWon+=$row10["amount"];
													$amountLost+=$row11["amount"];
													$amountCall+=$row15["ca"];
													$amountMeeting+=$row16["meeting"];
													$amountEmail+=$row17["email"];
													$amountOther+=$row9["amount"];
											}
										}
									}
								}
							}
						}
						}
					}
					else
					{
						for($i=0;$i<count($_POST["users"]);$i++){
							if($employer == false || $employee == true || $leader == true){
								if($leader == true){
									$qr="select users.user_name as name,users.department as Se from users inner join securitygroups_users on users.id=securitygroups_users.user_id where users.deleted='0' and users.status='Active' and  users.id='".$_POST["users"][$i]."' and securitygroups_users.securitygroup_id In (".$whr.")";	
								}
								if($employee == true){
									$qr="select users.user_name as name,users.department as Se from users inner join securitygroups_users on users.id=securitygroups_users.user_id where users.deleted='0' and users.status='Active' and  users.id='".$_POST["users"][$i]."' and users.id =  '".$_SESSION['authenticated_user_id']."'";
								}
							}else{
								if($employer == true){
									$qr="select users.user_name as name,users.department as Se from users inner join securitygroups_users on users.id=securitygroups_users.user_id where users.deleted='0' and users.status='Active' and  users.id='".$_POST["users"][$i]."'";
								}
								
							}
							
							$res=$db->query($qr);
							$row=$db->fetchByAssoc($res);
							if(!empty($row)){
								$query4="select count(id) as customer from ($queryLead) as leads where status ='New' and deleted='0' and assigned_user_id ='".$_POST["users"][$i]."'".$leadWhere;
								$result4=$db->query($query4);
								$row4 =$db->fetchByAssoc($result4);
								
								$query1="select count(id) as customer from ($queryLead) as leads where status ='In Process' and deleted='0' and assigned_user_id ='".$_POST["users"][$i]."'".$leadWhere;
								$result1=$db->query($query1);
								$row1 =$db->fetchByAssoc($result1);
									
								$query2="select count(accounts.id) as customer from ($accountQuery) as accounts inner join accounts_cstm on accounts.id=accounts_cstm.id_c where accounts_cstm.lead_account_status_c 
								='Converted' and accounts.deleted='0' and accounts.assigned_user_id ='".$_POST["users"][$i]."'".$accountWhere;
								$result2=$db->query($query2);
								$row2 =$db->fetchByAssoc($result2);
								
								$query12="select count(id) as customer from ($queryLead) as leads where status ='Dead' and deleted='0' and assigned_user_id ='".$_POST["users"][$i]."'".$leadWhere;
								$result12=$db->query($query12);
								$row12 =$db->fetchByAssoc($result12);
									
								
								
								$query5="select count(opportunities.id) as customer from opportunities inner join accounts_opportunities on opportunities.id=accounts_opportunities.opportunity_id inner join ($accountQuery) as accounts 
								on accounts_opportunities.account_id=accounts.id where opportunities.sales_stage='Closed Won' and opportunities.assigned_user_id ='".$_POST["users"][$i]."' and opportunities.deleted='0'".$oppWhere;
								$result5=$db->query($query5);
								$row5 =$db->fetchByAssoc($result5);
								
								$query6="select count(opportunities.id) as customer from opportunities inner join accounts_opportunities on opportunities.id=accounts_opportunities.opportunity_id inner join ($accountQuery) as accounts 
								on accounts_opportunities.account_id=accounts.id where opportunities.sales_stage='Closed Lost' and accounts.assigned_user_id ='".$_POST["users"][$i]."' and accounts.deleted='0'".$oppWhere;
								$result6=$db->query($query6);
								$row6 =$db->fetchByAssoc($result6);
								
								$query7="select sum(opportunity_amount) as amount from ($leadQuery) as leads where status ='New' and deleted='0' and assigned_user_id ='".$_POST["users"][$i]."'".$leadWhere;
								$result7=$db->query($query7);
								$row7 =$db->fetchByAssoc($result7);
								if($row7['amount']==null || $row7['amount']==''){
									$row7['amount']=0;
								}
								$query8="select sum(opportunity_amount) as amount from ($leadQuery) as leads where status ='In Process' and deleted='0' and assigned_user_id ='".$_POST["users"][$i]."'".$leadWhere;
								$result8=$db->query($query8);
								$row8 =$db->fetchByAssoc($result8);
								if($row8['amount'] == null || $row8['amount'] == ''){
									$row8['amount']=0;
								}
								$query9="select sum(amount) as amount from ($oppQuery) as opportunities where sales_stage Not In('Closed Won','Closed Lost') and assigned_user_id ='".$_POST["users"][$i]."' and deleted='0'".$oppWhere;
								$result9=$db->query($query9);
								$row9 =$db->fetchByAssoc($result9);
								if($row9['amount'] == null || $row9['amount'] == ''){
									$row9['amount']=0;
								}
								$query10="select sum(opportunities.amount) as amount  from opportunities  where opportunities.sales_stage = 'Closed Won' and opportunities.assigned_user_id ='".$_POST["users"][$i]."' and opportunities.deleted = '0'".$accountWhereTran;
								$result10=$db->query($query10);
								$row10 =$db->fetchByAssoc($result10);
								if($row10['amount'] == null || $row10['amount'] == ''){
									$row10['amount']=0;
								}
								$query11="select sum(amount) as amount from opportunities where sales_stage='Closed Lost' and assigned_user_id ='".$_POST["users"][$i]."' and deleted='0'".$oppWhere;
								$result11=$db->query($query11);
								$row11 =$db->fetchByAssoc($result11);
								if($row11['amount'] == null || $row11['amount'] == ''){
									$row11['amount']=0;
								}
								$query13="select sum(opportunities.amount)  as amount from opportunities inner join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id
									inner join accounts_cstm on accounts_opportunities.account_id = accounts_cstm.id_c where opportunities.sales_stage NOT IN('Closed Won','Closed Lost') and opportunities.assigned_user_id ='".$_POST["users"][$i]."'
									and opportunities.deleted='0' and accounts_cstm.lead_account_status_c='Converted'".$oppWhere;
								$result13=$db->query($query13);
								$row13 =$db->fetchByAssoc($result13);
								if($row13['amount'] == null || $row13['amount'] == ''){
									$row13['amount']=0;
								}
								$query14="select sum(opportunity_amount) as amount from ($leadQuery) as leads where status ='Dead' and deleted='0' and assigned_user_id ='".$_POST["users"][$i]."'".$leadWhere;
								$result14=$db->query($query14);
								$row14 =$db->fetchByAssoc($result14);
								if($row14['amount']==null || $row14['amount']==''){
									$row14['amount']=0;
								}
								$query15="select count(id) as ca from ($callQuery) as  calls where deleted='0' and status='Held' and assigned_user_id ='".$_POST["users"][$i]."'".$callWhere;
								$result15=$db->query($query15);
								$row15 =$db->fetchByAssoc($result15);
								if($row15['ca']==null || $row15['ca']==''){
									$row15['ca']=0;
								}
								$query16="select count(id) as meeting from ($meetingQuery) as meetings  where deleted='0' and status='Held' and assigned_user_id ='".$_POST["users"][$i]."'".$meetingWhere;
								$result16=$db->query($query16);
								$row16 =$db->fetchByAssoc($result16);
								if($row16['meeting']==null || $row16['meeting']==''){
									$row16['meeting']=0;
								}
								$query17="select count(id) as email from emails where deleted='0' and assigned_user_id ='".$_POST["users"][$i]."'".$emailWhere;
								$result17=$db->query($query17);
								$row17 =$db->fetchByAssoc($result17);
								if($row17['email']==null || $row17['email']==''){
									$row17['email']=0;
								}
								$query18="select count(opportunities.id) as customer from opportunities inner join accounts_opportunities on opportunities.id=accounts_opportunities.opportunity_id inner join ($accountQuery) as accounts 
								on accounts_opportunities.account_id=accounts.id where opportunities.sales_stage not in('Closed Won','Closed Lost') and accounts.assigned_user_id ='".$_POST["users"][$i]."' and accounts.deleted='0'".$oppWhere;
								$result18=$db->query($query18);
								$row18 =$db->fetchByAssoc($result18);
								if($row18['customer']==null || $row18['customer']==''){
									$row18['customer']=0;
								}
									$query21 = "select distinct(accounts.assigned_user_id) as assigned from opportunities inner join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id inner join 
									accounts on accounts.id = accounts_opportunities.account_id where opportunities.sales_stage = 'Closed Won' and opportunities.assigned_user_id ='".$_POST["users"][$i]."' and opportunities.deleted = '0'  and accounts.deleted = '0'".$accountWhereTran;
									$result21 = $db->query($query21);
									$whereAss = '';
									while($row21 = $db->fetchByAssoc($result21)){
										if(!empty($row21["assigned"])){
											$whereAss .= "'".$row21["assigned"]."',";
											
										}
									}
									$query19 = "select accounts.id as id  from ($accountQuery) as accounts left join accounts_cstm on accounts_cstm.id_c = accounts.id where accounts.deleted = '0'  and accounts.assigned_user_id ='".$_POST["users"][$i]."' and accounts_cstm.lead_account_status_c = 'Transaction'".$accountTransaction;
									$result19=$db->query($query19);
									$customerr = 0;
									$amountt = 0;
									while($row19 =$db->fetchByAssoc($result19)){
										 
										$query20="select opportunities.amount as amount  from opportunities inner join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id inner join 
										accounts on accounts.id = accounts_opportunities.account_id where opportunities.sales_stage = 'Closed Won' and accounts.id ='".$row19['id']."' and opportunities.deleted = '0' ";
									$result20=$db->query($query20);
									$row20 =$db->fetchByAssoc($result20);
										if($row20['amount'] != null && $row20['amount'] != ''){
											$amountt+=$row20['amount'];

										}
									}
									$query23="select count(accounts.id) as customer  from accounts_cstm right join accounts on accounts_cstm.id_c = accounts.id where accounts.deleted = '0'  and accounts.assigned_user_id ='".$_POST["users"][$i]."' and accounts_cstm.lead_account_status_c = 'Transaction'".$accountTransaction;
									$result23=$db->query($query23);
									$row23 =$db->fetchByAssoc($result23);
									if($row23['customer'] != null && $row23['customer'] != ''){
										$customerr = $row23['customer'];
									}
									if($employee == true){
										if($_POST["users"][$i] == $_SESSION['authenticated_user_id']){	
											$user=array(
												"user"=>$row['name'],
												"New"=>$row4["customer"],
												"userAmountNew"=>$row7['amount'],
												"InProcess"=>$row1["customer"],
												"userAmountInprocess"=>$row8['amount'],
												"Converted"=>$row2["customer"],
												"userAmountConverted"=>$row13['amount'],
												"Dead"=>$row12["customer"],
												"userAmountDead"=>$row14["amount"],
												"Transaction"=>$customerr,
												"userAmountTransaction"=>$amountt,
												"ClosedWon"=>$row5["customer"],
												"userAmountClosedWon"=>$row10['amount'],
												"Other"=>$row18["customer"],
												"userAmountOther"=>$row9['amount'],
												"ClosedLost"=>$row6["customer"],
												"userAmountClosedLost"=>$row11['amount'],
												"Call"=>$row15["ca"],
												"Meeting"=>$row16["meeting"],
												"Email"=>$row17["email"]
											);
										if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
											$group[$row["Se"]]=array();
											$group[$row["Se"]][]=$user;
										}else{
											$group[$row["Se"]][]=$user;
										}
										$New+=$row4["customer"];
										$InProcess+=$row1["customer"];
										$Dead+=$row12["customer"];
										$Converted+=$row2["customer"];
										$Transaction+=$customerr;
										$CloseWon+=$row5["customer"];
										$CloseLost+=$row6["customer"];
										$Other+=$row18["customer"];
										$amountNew+=$row7["amount"];
										$amountInProcess+=$row8["amount"];
										$amountDead+=$row14["amount"];
										$amountConverted+=$row13["amount"];
										$amountTran+=$amountt;
										$amountWon+=$row10["amount"];
										$amountLost+=$row11["amount"];
										$amountCall+=$row15["ca"];
										$amountMeeting+=$row16["meeting"];
										$amountEmail+=$row17["email"];
										$amountOther+=$row9["amount"];
										}
									}else{
										$user=array(
												"user"=>$row['name'],
												"New"=>$row4["customer"],
												"userAmountNew"=>$row7['amount'],
												"InProcess"=>$row1["customer"],
												"userAmountInprocess"=>$row8['amount'],
												"Converted"=>$row2["customer"],
												"userAmountConverted"=>$row13['amount'],
												"Dead"=>$row12["customer"],
												"userAmountDead"=>$row14["amount"],
												"Transaction"=>$customerr,
												"userAmountTransaction"=>$amountt,
												"ClosedWon"=>$row5["customer"],
												"userAmountClosedWon"=>$row10['amount'],
												"Other"=>$row18["customer"],
												"userAmountOther"=>$row9['amount'],
												"ClosedLost"=>$row6["customer"],
												"userAmountClosedLost"=>$row11['amount'],
												"Call"=>$row15["ca"],
												"Meeting"=>$row16["meeting"],
												"Email"=>$row17["email"]
											);
										if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
											$group[$row["Se"]]=array();
											$group[$row["Se"]][]=$user;
										}else{
											$group[$row["Se"]][]=$user;
										}
										$New+=$row4["customer"];
										$InProcess+=$row1["customer"];
										$Dead+=$row12["customer"];
										$Converted+=$row2["customer"];
										$Transaction+=$customerr;
										$CloseWon+=$row5["customer"];
										$CloseLost+=$row6["customer"];
										$Other+=$row18["customer"];
										$amountNew+=$row7["amount"];
										$amountInProcess+=$row8["amount"];
										$amountDead+=$row14["amount"];
										$amountConverted+=$row13["amount"];
										$amountTran+=$amountt;
										$amountWon+=$row10["amount"];
										$amountLost+=$row11["amount"];
										$amountCall+=$row15["ca"];
										$amountMeeting+=$row16["meeting"];
										$amountEmail+=$row17["email"];
										$amountOther+=$row9["amount"];
									}
							}
						}
					}
				//	print_r ($group);
				}
				
			
			echo json_encode (array(array(
			"province"=>$prov[$_POST["province"]],
			"data"=>$group,
			"New"=>$New,
			"InProcess"=>$InProcess,
			"Dead"=>$Dead,
			"Converted"=>$Converted,
			"Transaction"=>$Transaction,
			"CloseWon"=>$CloseWon,
			"Other"=>$Other,
			"CloseLost"=>$CloseLost,
			"amountNew"=>$amountNew,
			"amountInProcess"=>$amountInProcess,
			"amountDead"=>$amountDead,
			"amountConverted"=>$amountConverted,
			"amountTransaction"=>$amountTran,
			"amountWon"=>$amountWon,
			"amountLost"=>$amountLost,
			"amountCall"=>$amountCall,
			"amountMeeting"=>$amountMeeting,
			"amountEmail"=>$amountEmail,
			"amountOther"=>$amountOther
			)));
			
		}
		else
		{
			for($k=10;$k<105;$k++){
				$p='';
				$New=0;
				$InProcess=0;
				$Converted=0;
				$Dead=0;
				$Transaction=0;
				$CloseWon=0;
				$CloseLost=0;
				$Other=0;
				$amountNew=0;
				$amountInProcess=0;
				$amountConverted=0;
				$amountTran=0;
				$amountWon=0;
				$amountLost=0;
				$amountDead=0;
				$amountCall=0;
				$amountMeeting=0;
				$amountEmail=0;
				$amountOther=0;
				if($_POST['groupUsers'] == null || $_POST['groupUsers'] == '' || $_POST['groupUsers'][0] == "All"){
					if($_POST["users"] == null || $_POST["users"] == ''){
						if($_POST["titleUsers"] == null || $_POST["titleUsers"] == ''){
						if($employer == false || $employee == true || $leader == true){
							if($leader == true){
								$qr="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join users_cstm on users.id=users_cstm.id_c inner join securitygroups_users on users.id = securitygroups_users.user_id
								inner join  securitygroups on securitygroups.id = securitygroups_users.securitygroup_id where users.deleted='0' and users.status='Active' and users_cstm.provincecode_c='".$k."' and securitygroups_users.securitygroup_id In (".$whr.")";	
							}
							if($employee == true){
								$qr="select distinct(securitygroups.name) as seName, users.id as 	id,users.user_name as name,users.department as Se from users inner join users_cstm on users.id=users_cstm.id_c inner join securitygroups_users on users.id = securitygroups_users.user_id
								inner join  securitygroups on securitygroups.id = securitygroups_users.securitygroup_id where users.deleted='0' and users.status='Active' and users_cstm.provincecode_c='".$k."' and users.id =  '".$_SESSION['authenticated_user_id']."'";	
							}
						}else{
							if($employer == true){
								$qr="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join users_cstm on users.id=users_cstm.id_c inner join securitygroups_users on users.id = securitygroups_users.user_id
								inner join  securitygroups on securitygroups.id = securitygroups_users.securitygroup_id where users.deleted='0' and users.status='Active' and users_cstm.provincecode_c='".$k."'";
							}
						}
						$res=$db->query($qr);
						while($row=$db->fetchByAssoc($res)){
							if(!empty($row)){
								$p=$k;
								//$seName = $row["seName"];
								//if($employer == true){
								//	$row["seName"] = $row["Se"];
								//}
								if($row["seName"] == $row["Se"]){
									$query21 = "select distinct(accounts.assigned_user_id) as assigned from opportunities inner join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id inner join 
									accounts on accounts.id = accounts_opportunities.account_id where opportunities.sales_stage = 'Closed Won' and opportunities.assigned_user_id ='".$row['id']."' and opportunities.deleted = '0'  and accounts.deleted = '0'".$accountWhereTran;
									$result21 = $db->query($query21);
									$whereAss = '';
									while($row21 = $db->fetchByAssoc($result21)){
										if(!empty($row21["assigned"])){
											$whereAss .= "'".$row21["assigned"]."',";
																		
										}
									}
															$whereAss = rtrim($whereAss,',');
									
									$query4="select count(id) as customer from ($queryLead) as leads where status ='New' and deleted='0' and assigned_user_id ='".$row['id']."'".$leadWhere;
									$result4=$db->query($query4);
									$row4 =$db->fetchByAssoc($result4);
									
									$query1="select count(id) as customer from ($queryLead) as leads where status ='In Process' and deleted='0' and assigned_user_id ='".$row['id']."'".$leadWhere;
									$result1=$db->query($query1);
									$row1 =$db->fetchByAssoc($result1);
										
									$query2="select count(accounts.id) as customer from ($accountQuery) as accounts inner join accounts_cstm on accounts.id=accounts_cstm.id_c where accounts_cstm.lead_account_status_c 
									='Converted' and accounts.deleted='0' and accounts.assigned_user_id ='".$row['id']."'".$accountWhere;
									$result2=$db->query($query2);
									$row2 =$db->fetchByAssoc($result2);
									
									$query12="select count(id) as customer from ($queryLead) as leads where status ='Dead' and deleted='0' and assigned_user_id ='".$row['id']."'".$leadWhere;
									$result12=$db->query($query12);
									$row12 =$db->fetchByAssoc($result12);
									
									
									
									$query5="select count(opportunities.id) as customer from opportunities inner join accounts_opportunities on opportunities.id=accounts_opportunities.opportunity_id inner join ($accountQuery) as accounts 
									on accounts_opportunities.account_id=accounts.id where opportunities.sales_stage='Closed Won' and opportunities.assigned_user_id ='".$row['id']."' and opportunities.deleted='0'".$oppWhere;
									$result5=$db->query($query5);
									$row5 =$db->fetchByAssoc($result5);
									
									$query6="select count(opportunities.id) as customer from opportunities inner join accounts_opportunities on opportunities.id=accounts_opportunities.opportunity_id inner join ($accountQuery) as accounts 
									on accounts_opportunities.account_id=accounts.id where opportunities.sales_stage='Closed Lost' and accounts.assigned_user_id ='".$row['id']."' and accounts.deleted='0'".$oppWhere;
									$result6=$db->query($query6);
									$row6 =$db->fetchByAssoc($result6);
									
									$query7="select sum(opportunity_amount) as amount from ($leadQuery) as leads where status ='New' and deleted='0' and assigned_user_id ='".$row['id']."'".$leadWhere;
									$result7=$db->query($query7);
									$row7 =$db->fetchByAssoc($result7);
									if($row7['amount']==null || $row7['amount']==''){
										$row7['amount']=0;
									}
									$query8="select sum(opportunity_amount) as amount from ($leadQuery) as leads where status ='In Process' and deleted='0' and assigned_user_id ='".$row['id']."'".$leadWhere;
									$result8=$db->query($query8);
									$row8 =$db->fetchByAssoc($result8);
									if($row8['amount'] == null || $row8['amount'] == ''){
										$row8['amount']=0;
									}
									
									
									$query9="select sum(amount) as amount from ($oppQuery) as opportunities where sales_stage Not In('Closed Won','Closed Lost') and assigned_user_id ='".$row['id']."' and deleted='0'".$accountWhereTran;
									$result9=$db->query($query9);
									$row9 =$db->fetchByAssoc($result9);
									if($row9['amount'] == null || $row9['amount'] == ''){
										$row9['amount']=0;
									}
									
									$query10="select sum(opportunities.amount) as amount  from opportunities  where opportunities.sales_stage = 'Closed Won' and opportunities.assigned_user_id ='".$row['id']."' and opportunities.deleted = '0'".$accountWhereTran;
									$result10=$db->query($query10);
									$row10 =$db->fetchByAssoc($result10);
									if($row10['amount'] == null || $row10['amount'] == ''){
										$row10['amount']=0;
									}
									$query11="select sum(amount) as amount from opportunities where sales_stage='Closed Lost' and assigned_user_id ='".$row['id']."' and deleted='0'".$oppWhere;
									$result11=$db->query($query11);
									$row11 =$db->fetchByAssoc($result11);
									if($row11['amount'] == null || $row11['amount'] == ''){
										$row11['amount']=0;
									}
									$query13="select sum(opportunities.amount)  as amount from opportunities inner join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id
									inner join accounts_cstm on accounts_opportunities.account_id = accounts_cstm.id_c where opportunities.sales_stage NOT IN('Closed Won','Closed Lost') and opportunities.assigned_user_id ='".$row['id']."'
									and opportunities.deleted='0' and accounts_cstm.lead_account_status_c='Converted'".$oppWhere;
									$result13=$db->query($query13);
									$row13 =$db->fetchByAssoc($result13);
									if($row13['amount'] == null || $row13['amount'] == ''){
										$row13['amount']=0;
									}
									$query14="select sum(opportunity_amount) as amount from ($leadQuery) as leads where status ='Dead' and deleted='0' and assigned_user_id ='".$row['id']."'".$leadWhere;
									$result14=$db->query($query14);
									$row14 =$db->fetchByAssoc($result14);
									if($row14['amount']==null || $row14['amount']==''){
										$row14['amount']=0;
									}
									$query15="select count(id) as ca from ($callQuery) as  calls where deleted='0' and status='Held' and assigned_user_id ='".$row['id']."'".$callWhere;
									$result15=$db->query($query15);
									$row15 =$db->fetchByAssoc($result15);
									if($row15['ca']==null || $row15['ca']==''){
										$row15['ca']=0;
									}
									$query16="select count(id) as meeting from ($meetingQuery) as meetings  where deleted='0' and status='Held' and assigned_user_id ='".$row['id']."'".$meetingWhere;
									$result16=$db->query($query16);
									$row16 =$db->fetchByAssoc($result16);
									if($row16['meeting']==null || $row16['meeting']==''){
										$row16['meeting']=0;
									}
									$query17="select count(id) as email from emails where deleted='0' and assigned_user_id ='".$row['id']."'".$emailWhere;
									$result17=$db->query($query17);
									$row17 =$db->fetchByAssoc($result17);
									if($row17['email']==null || $row17['email']==''){
										$row17['email']=0;
									}
									$query18="select count(opportunities.id) as customer from opportunities inner join accounts_opportunities on opportunities.id=accounts_opportunities.opportunity_id inner join ($accountQuery) as accounts 
									on accounts_opportunities.account_id=accounts.id where opportunities.sales_stage not in('Closed Won','Closed Lost') and opportunities.assigned_user_id ='".$row['id']."' and accounts.deleted='0'".$oppWhere;
									$result18=$db->query($query18);
									$row18 =$db->fetchByAssoc($result18);
									if($row18['customer']==null || $row18['customer']==''){
										$row18['customer']=0;
									}
									$query21 = "select distinct(accounts.assigned_user_id) as assigned from opportunities inner join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id inner join 
									accounts on accounts.id = accounts_opportunities.account_id where opportunities.sales_stage = 'Closed Won' and opportunities.assigned_user_id ='".$row['id']."' and opportunities.deleted = '0'  and accounts.deleted = '0'".$accountWhereTran;
									$result21 = $db->query($query21);
									$whereAss = '';
									while($row21 = $db->fetchByAssoc($result21)){
										if(!empty($row21["assigned"])){
											$whereAss .= "'".$row21["assigned"]."',";
											
										}
									}
									$query19 = "select accounts.id as id  from ($accountQuery) as accounts left join accounts_cstm on accounts_cstm.id_c = accounts.id where accounts.deleted = '0'  and accounts.assigned_user_id ='".$row["id"]."' and accounts_cstm.lead_account_status_c = 'Transaction'".$accountTransaction;
									$result19=$db->query($query19);
									$customerr = 0;
									$amountt = 0;
									while($row19 =$db->fetchByAssoc($result19)){
										 
										$query20="select opportunities.amount as amount  from opportunities inner join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id inner join 
										accounts on accounts.id = accounts_opportunities.account_id where opportunities.sales_stage = 'Closed Won' and accounts.id ='".$row19['id']."' and opportunities.deleted = '0' ";
									$result20=$db->query($query20);
									$row20 =$db->fetchByAssoc($result20);
										if($row20['amount'] != null && $row20['amount'] != ''){
											$amountt+=$row20['amount'];

										}
									}
									$query23="select count(accounts.id) as customer  from accounts_cstm right join accounts on accounts_cstm.id_c = accounts.id where accounts.deleted = '0'  and accounts.assigned_user_id ='".$row["id"]."' and accounts_cstm.lead_account_status_c = 'Transaction'".$accountTransaction;
									$result23=$db->query($query23);
									$row23 =$db->fetchByAssoc($result23);
									if($row23['customer'] != null && $row23['customer'] != ''){
										$customerr = $row23['customer'];
									}
									if($employee == true){
										if($_POST["users"][$i] == $_SESSION['authenticated_user_id']){	
											$user=array(
												"user"=>$row['name'],
												"New"=>$row4["customer"],
												"userAmountNew"=>$row7['amount'],
												"InProcess"=>$row1["customer"],
												"userAmountInprocess"=>$row8['amount'],
												"Converted"=>$row2["customer"],
												"userAmountConverted"=>$row13['amount'],
												"Dead"=>$row12["customer"],
												"userAmountDead"=>$row14["amount"],
												"Transaction"=>$customerr,
												"userAmountTransaction"=>$amountt,
												"ClosedWon"=>$row5["customer"],
												"userAmountClosedWon"=>$row10['amount'],
												"Other"=>$row18["customer"],
												"userAmountOther"=>$row9['amount'],
												"ClosedLost"=>$row6["customer"],
												"userAmountClosedLost"=>$row11['amount'],
												"Call"=>$row15["ca"],
												"Meeting"=>$row16["meeting"],
												"Email"=>$row17["email"]
											);
											if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
												$group[$row["Se"]]=array();
												$group[$row["Se"]][]=$user;
											}else{
												$group[$row["Se"]][]=$user;
											}
											$New+=$row4["customer"];
											$InProcess+=$row1["customer"];
											$Dead+=$row12["customer"];
											$Converted+=$row2["customer"];
											$Transaction+=$customerr;
											$CloseWon+=$row5["customer"];
											$CloseLost+=$row6["customer"];
											$Other+=$row18["customer"];
											$amountNew+=$row7["amount"];
											$amountInProcess+=$row8["amount"];
											$amountDead+=$row14["amount"];
											$amountConverted+=$row13["amount"];
											$amountTran+=$amountt;
											$amountWon+=$row10["amount"];
											$amountLost+=$row11["amount"];
											$amountCall+=$row15["ca"];
											$amountMeeting+=$row16["meeting"];
											$amountEmail+=$row17["email"];
											$amountOther+=$row9["amount"];
										}
									}else{
										$user=array(
												"user"=>$row['name'],
												"New"=>$row4["customer"],
												"userAmountNew"=>$row7['amount'],
												"InProcess"=>$row1["customer"],
												"userAmountInprocess"=>$row8['amount'],
												"Converted"=>$row2["customer"],
												"userAmountConverted"=>$row13['amount'],
												"Dead"=>$row12["customer"],
												"userAmountDead"=>$row14["amount"],
												"Transaction"=>$customerr,
												"userAmountTransaction"=>$amountt,
												"ClosedWon"=>$row5["customer"],
												"userAmountClosedWon"=>$row10['amount'],
												"Other"=>$row18["customer"],
												"userAmountOther"=>$row9['amount'],
												"ClosedLost"=>$row6["customer"],
												"userAmountClosedLost"=>$row11['amount'],
												"Call"=>$row15["ca"],
												"Meeting"=>$row16["meeting"],
												"Email"=>$row17["email"]
											);
											//$row["seName"] = $seName;
											if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
												$group[$row["Se"]]=array();
												$group[$row["Se"]][]=$user;
											}else{
												$group[$row["Se"]][]=$user;
											}
											$New+=$row4["customer"];
											$InProcess+=$row1["customer"];
											$Dead+=$row12["customer"];
											$Converted+=$row2["customer"];
											$Transaction+=$customerr;
											$CloseWon+=$row5["customer"];
											$CloseLost+=$row6["customer"];
											$Other+=$row18["customer"];
											$amountNew+=$row7["amount"];
											$amountInProcess+=$row8["amount"];
											$amountDead+=$row14["amount"];
											$amountConverted+=$row13["amount"];
											$amountTran+=$amountt;
											$amountWon+=$row10["amount"];
											$amountLost+=$row11["amount"];
											$amountCall+=$row15["ca"];
											$amountMeeting+=$row16["meeting"];
											$amountEmail+=$row17["email"];
											$amountOther+=$row9["amount"];
									}
								}
							}
						  } 
						}else{
								for($i=0;$i<count($_POST["titleUsers"]);$i++){
									if($employer == false || $employee == true || $leader == true){
										if($leader == true){
											$q="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
											inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
											users.status='Active' and users_cstm.provincecode_c='".$k."' and securitygroups_users.securitygroup_id In (".$whr.") and users.title = '".$_POST["titleUsers"][$i]."'";
										}
										if($employee == true){
											$q="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
											inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
											users.status='Active' and users_cstm.provincecode_c='".$k."' and users.id =  '".$_SESSION['authenticated_user_id']."' and users.title = '".$_POST["titleUsers"][$i]."'";
										}
									}else{
										if($employer == true){
											$q="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
											inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
											users.status='Active' and users_cstm.provincecode_c='".$k."' and users.title = '".$_POST["titleUsers"][$i]."'";
										}
									}
								
								$r=$db->query($q);
								while($rww=$db->fetchByAssoc($r)){
									if(!empty($rww)){
										$p=$k;
										if($rww["seName"]==$rww["Se"]){
											$qr="select users_cstm.provincecode_c as province, users.user_name as name,users.department as Se from users inner join securitygroups_users on users.id = securitygroups_users.user_id inner join users_cstm on users.id = users_cstm.id_c where users.deleted='0' and users.status='Active' and  users.id='".$rww["id"]."' and securitygroups_users.securitygroup_id In (".$whr.")";	
											$res=$db->query($qr);
											$row=$db->fetchByAssoc($res);
											if(!empty($row)){
												if($row["province"] == $k){
												$p=$k;
												$query4="select count(id) as customer from ($queryLead) as leads where status ='New' and deleted='0' and assigned_user_id ='".$rww["id"]."'".$leadWhere;
												$result4=$db->query($query4);
												$row4 =$db->fetchByAssoc($result4);
												
												$query1="select count(id) as customer from ($queryLead) as leads where status ='In Process' and deleted='0' and assigned_user_id ='".$rww["id"]."'".$leadWhere;
												$result1=$db->query($query1);
												$row1 =$db->fetchByAssoc($result1);
													
												$query2="select count(accounts.id) as customer from ($accountQuery) as accounts inner join accounts_cstm on accounts.id=accounts_cstm.id_c where accounts_cstm.lead_account_status_c 
												='Converted' and accounts.deleted='0' and accounts.assigned_user_id ='".$rww["id"]."'".$accountWhere;
												$result2=$db->query($query2);
												$row2 =$db->fetchByAssoc($result2);
												
												$query12="select count(id) as customer from ($queryLead) as leads where status ='Dead' and deleted='0' and assigned_user_id ='".$rww["id"]."'".$leadWhere;
												$result12=$db->query($query12);
												$row12 =$db->fetchByAssoc($result12);
												
												
												
												$query5="select count(opportunities.id) as customer from opportunities inner join accounts_opportunities on opportunities.id=accounts_opportunities.opportunity_id inner join ($accountQuery) as accounts 
												on accounts_opportunities.account_id=accounts.id where opportunities.sales_stage='Closed Won' and opportunities.assigned_user_id ='".$rww["id"]."' and opportunities.deleted='0'".$oppWhere;
												$result5=$db->query($query5);
												$row5 =$db->fetchByAssoc($result5);
												
												$query6="select count(opportunities.id) as customer from opportunities inner join accounts_opportunities on opportunities.id=accounts_opportunities.opportunity_id inner join ($accountQuery) as accounts 
												on accounts_opportunities.account_id=accounts.id where opportunities.sales_stage='Closed Lost' and accounts.assigned_user_id ='".$rww["id"]."' and accounts.deleted='0'".$oppWhere;
												$result6=$db->query($query6);
												$row6 =$db->fetchByAssoc($result6);
												
												$query7="select sum(opportunity_amount) as amount from ($leadQuery) as leads where status ='New' and deleted='0' and assigned_user_id ='".$rww["id"]."'".$leadWhere;
												$result7=$db->query($query7);
												$row7 =$db->fetchByAssoc($result7);
												if($row7['amount']==null || $row7['amount']==''){
													$row7['amount']=0;
												}
												$query8="select sum(opportunity_amount) as amount from ($leadQuery) as leads where status ='In Process' and deleted='0' and assigned_user_id ='".$rww["id"]."'".$leadWhere;
												$result8=$db->query($query8);
												$row8 =$db->fetchByAssoc($result8);
												if($row8['amount'] == null || $row8['amount'] == ''){
													$row8['amount']=0;
												}
												$query9="select sum(amount) as amount from ($oppQuery) as opportunities where sales_stage Not In('Closed Won','Closed Lost') and assigned_user_id ='".$rww["id"]."' and deleted='0'".$oppWhere;
												$result9=$db->query($query9);
												$row9 =$db->fetchByAssoc($result9);
												if($row9['amount'] == null || $row9['amount'] == ''){
													$row9['amount']=0;
												}
												$query10="select sum(opportunities.amount) as amount  from opportunities  where opportunities.sales_stage = 'Closed Won' and opportunities.assigned_user_id ='".$rww["id"]."' and opportunities.deleted = '0'".$accountWhereTran;
												$result10=$db->query($query10);
												$row10 =$db->fetchByAssoc($result10);
												if($row10['amount'] == null || $row10['amount'] == ''){
													$row10['amount']=0;
												}
												$query11="select sum(amount) as amount from opportunities where sales_stage='Closed Lost' and assigned_user_id ='".$rww["id"]."' and deleted='0'".$oppWhere;
												$result11=$db->query($query11);
												$row11 =$db->fetchByAssoc($result11);
												if($row11['amount'] == null || $row11['amount'] == ''){
													$row11['amount']=0;
												}
												$query13="select sum(opportunities.amount) as amount from opportunities inner join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id
													inner join accounts_cstm on accounts_opportunities.account_id = accounts_cstm.id_c where opportunities.sales_stage NOT IN('Closed Won','Closed Lost') and opportunities.assigned_user_id ='".$rww["id"]."'
													and opportunities.deleted='0' and accounts_cstm.lead_account_status_c='Converted'".$oppWhere;
												$result13=$db->query($query13);
												$row13 =$db->fetchByAssoc($result13);
												if($row13['amount'] == null || $row13['amount'] == ''){
													$row13['amount']=0;
												}
												$query14="select sum(opportunity_amount) as amount from ($leadQuery) as leads where status ='Dead' and deleted='0' and assigned_user_id ='".$rww["id"]."'".$leadWhere;
												$result14=$db->query($query14);
												$row14 =$db->fetchByAssoc($result14);
												if($row14['amount']==null || $row14['amount']==''){
													$row14['amount']=0;
												}
												$query15="select count(id) as ca from ($callQuery) as  calls where deleted='0' and status='Held' and assigned_user_id ='".$rww["id"]."'".$callWhere;
													$result15=$db->query($query15);
													$row15 =$db->fetchByAssoc($result15);
													if($row15['ca']==null || $row15['ca']==''){
														$row15['ca']=0;
												}
												$query16="select count(id) as meeting from ($meetingQuery) as meetings  where deleted='0' and status='Held' and assigned_user_id ='".$rww["id"]."'".$meetingWhere;
												$result16=$db->query($query16);
												$row16 =$db->fetchByAssoc($result16);
												if($row16['meeting']==null || $row16['meeting']==''){
													$row16['meeting']=0;
												}
												$query17="select count(id) as email from emails where deleted='0' and assigned_user_id ='".$rww["id"]."'".$emailWhere;
												$result17=$db->query($query17);
												$row17 =$db->fetchByAssoc($result17);
												if($row17['email']==null || $row17['email']==''){
													$row17['email']=0;
												}
												$query18="select count(opportunities.id) as customer from opportunities inner join accounts_opportunities on opportunities.id=accounts_opportunities.opportunity_id inner join ($accountQuery) as accounts 
												on accounts_opportunities.account_id=accounts.id where opportunities.sales_stage not in('Closed Won','Closed Lost') and accounts.assigned_user_id ='".$rww["id"]."' and accounts.deleted='0'".$oppWhere;
												$result18=$db->query($query18);
												$row18 =$db->fetchByAssoc($result18);
												if($row18['customer']==null || $row18['customer']==''){
													$row18['customer']=0;
												}
												$query21 = "select distinct(accounts.assigned_user_id) as assigned from opportunities inner join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id inner join 
												accounts on accounts.id = accounts_opportunities.account_id where opportunities.sales_stage = 'Closed Won' and opportunities.assigned_user_id ='".$rww['id']."' and opportunities.deleted = '0'  and accounts.deleted = '0'".$accountWhereTran;
												$result21 = $db->query($query21);
												$whereAss = '';
												while($row21 = $db->fetchByAssoc($result21)){
													if(!empty($row21["assigned"])){
														$whereAss .= "'".$row21["assigned"]."',";
														
													}
												}
												$query19 = "select accounts.id as id  from ($accountQuery) as accounts left join accounts_cstm on accounts_cstm.id_c = accounts.id where accounts.deleted = '0'  and accounts.assigned_user_id ='".$rww["id"]."' and accounts_cstm.lead_account_status_c = 'Transaction'".$accountTransaction;
									$result19=$db->query($query19);
									$customerr = 0;
									$amountt = 0;
									while($row19 =$db->fetchByAssoc($result19)){
										 
										$query20="select opportunities.amount as amount  from opportunities inner join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id inner join 
										accounts on accounts.id = accounts_opportunities.account_id where opportunities.sales_stage = 'Closed Won' and accounts.id ='".$row19['id']."' and opportunities.deleted = '0' ";
									$result20=$db->query($query20);
									$row20 =$db->fetchByAssoc($result20);
										if($row20['amount'] != null && $row20['amount'] != ''){
											$amountt+=$row20['amount'];

										}
									}
												$query23="select count(accounts.id) as customer  from accounts_cstm right join accounts on accounts_cstm.id_c = accounts.id where accounts.deleted = '0'  and accounts.assigned_user_id ='".$rww["id"]."' and accounts_cstm.lead_account_status_c = 'Transaction'".$accountTransaction;
									$result23=$db->query($query23);
									$row23 =$db->fetchByAssoc($result23);
									if($row23['customer'] != null && $row23['customer'] != ''){
										$customerr = $row23['customer'];
									}
													if($employee == true){
														if($rww["id"] == $_SESSION['authenticated_user_id']){	
															$user=array(
																	"user"=>$row['name'],
																	"New"=>$row4["customer"],
																	"userAmountNew"=>$row7['amount'],
																	"InProcess"=>$row1["customer"],
																	"userAmountInprocess"=>$row8['amount'],
																	"Converted"=>$row2["customer"],
																	"userAmountConverted"=>$row13['amount'],
																	"Dead"=>$row12["customer"],
																	"userAmountDead"=>$row14["amount"],
																	"Transaction"=>$customerr,
																	"userAmountTransaction"=>$amountt,
																	"ClosedWon"=>$row5["customer"],
																	"userAmountClosedWon"=>$row10['amount'],
																	"Other"=>$row18["customer"],
																	"userAmountOther"=>$row9['amount'],
																	"ClosedLost"=>$row6["customer"],
																	"userAmountClosedLost"=>$row11['amount'],
																	"Call"=>$row15["ca"],
																	"Meeting"=>$row16["meeting"],
																	"Email"=>$row17["email"]
																);
															if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
																$group[$row["Se"]]=array();
																$group[$row["Se"]][]=$user;
															}else{
																$group[$row["Se"]][]=$user;
															}
															$New+=$row4["customer"];
															$InProcess+=$row1["customer"];
															$Dead+=$row12["customer"];
															$Converted+=$row2["customer"];
															$Transaction+=$customerr;
															$CloseWon+=$row5["customer"];
															$CloseLost+=$row6["customer"];
															$Other+=$row18["customer"];
															$amountNew+=$row7["amount"];
															$amountInProcess+=$row8["amount"];
															$amountDead+=$row14["amount"];
															$amountConverted+=$row13["amount"];
															$amountTran+=$amountt;
															$amountWon+=$row10["amount"];
															$amountLost+=$row11["amount"];
															$amountCall+=$row15["ca"];
															$amountMeeting+=$row16["meeting"];
															$amountEmail+=$row17["email"];
															$amountOther+=$row9["amount"];
														}
													}else{
														$user=array(
																	"user"=>$row['name'],
																	"New"=>$row4["customer"],
																	"userAmountNew"=>$row7['amount'],
																	"InProcess"=>$row1["customer"],
																	"userAmountInprocess"=>$row8['amount'],
																	"Converted"=>$row2["customer"],
																	"userAmountConverted"=>$row13['amount'],
																	"Dead"=>$row12["customer"],
																	"userAmountDead"=>$row14["amount"],
																	"Transaction"=>$customerr,
																	"userAmountTransaction"=>$amountt,
																	"ClosedWon"=>$row5["customer"],
																	"userAmountClosedWon"=>$row10['amount'],
																	"Other"=>$row18["customer"],
																	"userAmountOther"=>$row9['amount'],
																	"ClosedLost"=>$row6["customer"],
																	"userAmountClosedLost"=>$row11['amount'],
																	"Call"=>$row15["ca"],
																	"Meeting"=>$row16["meeting"],
																	"Email"=>$row17["email"]
																);
														if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
															$group[$row["Se"]]=array();
															$group[$row["Se"]][]=$user;
														}else{
															$group[$row["Se"]][]=$user;
														}
														$New+=$row4["customer"];
														$InProcess+=$row1["customer"];
														$Dead+=$row12["customer"];
														$Converted+=$row2["customer"];
														$Transaction+=$customerr;
														$CloseWon+=$row5["customer"];
														$CloseLost+=$row6["customer"];
														$Other+=$row18["customer"];
														$amountNew+=$row7["amount"];
														$amountInProcess+=$row8["amount"];
														$amountDead+=$row14["amount"];
														$amountConverted+=$row13["amount"];
														$amountTran+=$amountt;
														$amountWon+=$row10["amount"];
														$amountLost+=$row11["amount"];
														$amountCall+=$row15["ca"];
														$amountMeeting+=$row16["meeting"];
														$amountEmail+=$row17["email"];
														$amountOther+=$row9["amount"];
													}
												}
											}
										}
									}
								}
							}
						}
						
					}
					else
					{
						for($i=0;$i<count($_POST["users"]);$i++){
							if($employer == false || $employee == true || $leader == true){
								if($leader == true){
									$qr="select users_cstm.provincecode_c as province, users.user_name as name,users.department as Se from users inner join securitygroups_users on users.id = securitygroups_users.user_id inner join users_cstm on users.id = users_cstm.id_c where users.deleted='0' and users.status='Active' and  users.id='".$_POST["users"][$i]."' and securitygroups_users.securitygroup_id In (".$whr.")";	
								}
								if($employee == true){
									$qr="select users_cstm.provincecode_c as province, users.user_name as name,users.department as Se from users inner join securitygroups_users on users.id = securitygroups_users.user_id inner join users_cstm on users.id = users_cstm.id_c where users.deleted='0' and users.status='Active' and  users.id='".$_POST["users"][$i]."' and users.id =  '".$_SESSION['authenticated_user_id']."'";	
								}
							}else{
								if($employer == true){
									$qr="select users_cstm.provincecode_c as province, users.user_name as name,users.department as Se from users inner join securitygroups_users on users.id = securitygroups_users.user_id inner join users_cstm on users.id = users_cstm.id_c where users.deleted='0' and users.status='Active' and  users.id='".$_POST["users"][$i]."'";	
								}
								
							}
							
							$res=$db->query($qr);
							$row=$db->fetchByAssoc($res);
								if(!empty($row)){
									if($row["province"] == $k){
									$p=$k;
									$query4="select count(id) as customer from ($queryLead) as leads where status ='New' and deleted='0' and assigned_user_id ='".$_POST["users"][$i]."'".$leadWhere;
									$result4=$db->query($query4);
									$row4 =$db->fetchByAssoc($result4);
									
									$query1="select count(id) as customer from ($queryLead) as leads where status ='In Process' and deleted='0' and assigned_user_id ='".$_POST["users"][$i]."'".$leadWhere;
									$result1=$db->query($query1);
									$row1 =$db->fetchByAssoc($result1);
										
									$query2="select count(accounts.id) as customer from ($accountQuery) as accounts inner join accounts_cstm on accounts.id=accounts_cstm.id_c where accounts_cstm.lead_account_status_c 
									='Converted' and accounts.deleted='0' and accounts.assigned_user_id ='".$_POST["users"][$i]."'".$accountWhere;
									$result2=$db->query($query2);
									$row2 =$db->fetchByAssoc($result2);
									
									$query12="select count(id) as customer from ($queryLead) as leads where status ='Dead' and deleted='0' and assigned_user_id ='".$_POST["users"][$i]."'".$leadWhere;
									$result12=$db->query($query12);
									$row12 =$db->fetchByAssoc($result12);
									
									
									
									$query5="select count(opportunities.id) as customer from opportunities inner join accounts_opportunities on opportunities.id=accounts_opportunities.opportunity_id inner join ($accountQuery) as accounts 
									on accounts_opportunities.account_id=accounts.id where opportunities.sales_stage='Closed Won' and opportunities.assigned_user_id ='".$_POST["users"][$i]."' and opportunities.deleted='0'".$oppWhere;
									$result5=$db->query($query5);
									$row5 =$db->fetchByAssoc($result5);
									
									$query6="select count(opportunities.id) as customer from opportunities inner join accounts_opportunities on opportunities.id=accounts_opportunities.opportunity_id inner join ($accountQuery) as accounts 
									on accounts_opportunities.account_id=accounts.id where opportunities.sales_stage='Closed Lost' and accounts.assigned_user_id ='".$_POST["users"][$i]."' and accounts.deleted='0'".$oppWhere;
									$result6=$db->query($query6);
									$row6 =$db->fetchByAssoc($result6);
									
									$query7="select sum(opportunity_amount) as amount from ($leadQuery) as leads where status ='New' and deleted='0' and assigned_user_id ='".$_POST["users"][$i]."'".$leadWhere;
									$result7=$db->query($query7);
									$row7 =$db->fetchByAssoc($result7);
									if($row7['amount']==null || $row7['amount']==''){
										$row7['amount']=0;
									}
									$query8="select sum(opportunity_amount) as amount from ($leadQuery) as leads where status ='In Process' and deleted='0' and assigned_user_id ='".$_POST["users"][$i]."'".$leadWhere;
									$result8=$db->query($query8);
									$row8 =$db->fetchByAssoc($result8);
									if($row8['amount'] == null || $row8['amount'] == ''){
										$row8['amount']=0;
									}
									$query9="select sum(amount) as amount from ($oppQuery) as opportunities where sales_stage Not In('Closed Won','Closed Lost') and assigned_user_id ='".$_POST["users"][$i]."' and deleted='0'".$oppWhere;
									$result9=$db->query($query9);
									$row9 =$db->fetchByAssoc($result9);
									if($row9['amount'] == null || $row9['amount'] == ''){
										$row9['amount']=0;
									}
									$query10="select sum(opportunities.amount) as amount  from opportunities  where opportunities.sales_stage = 'Closed Won' and opportunities.assigned_user_id ='".$_POST["users"][$i]."' and opportunities.deleted = '0'".$accountWhereTran;
									$result10=$db->query($query10);
									$row10 =$db->fetchByAssoc($result10);
									if($row10['amount'] == null || $row10['amount'] == ''){
										$row10['amount']=0;
									}
									$query11="select sum(amount) as amount from opportunities where sales_stage='Closed Lost' and assigned_user_id ='".$_POST["users"][$i]."' and deleted='0'".$oppWhere;
									$result11=$db->query($query11);
									$row11 =$db->fetchByAssoc($result11);
									if($row11['amount'] == null || $row11['amount'] == ''){
										$row11['amount']=0;
									}
									$query13="select sum(opportunities.amount) as amount from opportunities inner join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id
									inner join accounts_cstm on accounts_opportunities.account_id = accounts_cstm.id_c where opportunities.sales_stage NOT IN('Closed Won','Closed Lost') and opportunities.assigned_user_id ='".$_POST["users"][$i]."'
									and opportunities.deleted='0' and accounts_cstm.lead_account_status_c='Converted'".$oppWhere;
									$result13=$db->query($query13);
									$row13 =$db->fetchByAssoc($result13);
									if($row13['amount'] == null || $row13['amount'] == ''){
										$row13['amount']=0;
									}
									$query14="select sum(opportunity_amount) as amount from ($leadQuery) as leads where status ='Dead' and deleted='0' and assigned_user_id ='".$_POST["users"][$i]."'".$leadWhere;
									$result14=$db->query($query14);
									$row14 =$db->fetchByAssoc($result14);
									if($row14['amount']==null || $row14['amount']==''){
										$row14['amount']=0;
									}
									$query15="select count(id) as ca from ($callQuery) as  calls where deleted='0' and status='Held' and assigned_user_id ='".$_POST["users"][$i]."'".$callWhere;
									$result15=$db->query($query15);
									$row15 =$db->fetchByAssoc($result15);
									if($row15['ca']==null || $row15['ca']==''){
										$row15['ca']=0;
									}
									$query16="select count(id) as meeting from ($meetingQuery) as meetings  where deleted='0' and status='Held' and assigned_user_id ='".$_POST["users"][$i]."'".$meetingWhere;
									$result16=$db->query($query16);
									$row16 =$db->fetchByAssoc($result16);
									if($row16['meeting']==null || $row16['meeting']==''){
										$row16['meeting']=0;
									}
									$query17="select count(id) as email from emails where deleted='0' and assigned_user_id ='".$_POST["users"][$i]."'".$emailWhere;
									$result17=$db->query($query17);
									$row17 =$db->fetchByAssoc($result17);
									if($row17['email']==null || $row17['email']==''){
										$row17['email']=0;
									}
									$query18="select count(opportunities.id) as customer from opportunities inner join accounts_opportunities on opportunities.id=accounts_opportunities.opportunity_id inner join ($accountQuery) as accounts 
									on accounts_opportunities.account_id=accounts.id where opportunities.sales_stage not in('Closed Won','Closed Lost') and accounts.assigned_user_id ='".$_POST["users"][$i]."' and accounts.deleted='0'".$oppWhere;
									$result18=$db->query($query18);
									$row18 =$db->fetchByAssoc($result18);
									if($row18['customer']==null || $row18['customer']==''){
										$row18['customer']=0;
									}
									$query21 = "select distinct(accounts.assigned_user_id) as assigned from opportunities inner join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id inner join 
									accounts on accounts.id = accounts_opportunities.account_id where opportunities.sales_stage = 'Closed Won' and opportunities.assigned_user_id ='".$_POST["users"][$i]."' and opportunities.deleted = '0'  and accounts.deleted = '0'".$accountWhereTran;
									$result21 = $db->query($query21);
									$whereAss = '';
									while($row21 = $db->fetchByAssoc($result21)){
										if(!empty($row21["assigned"])){
											$whereAss .= "'".$row21["assigned"]."',";
											
										}
									}
									$query19 = "select accounts.id as id  from ($accountQuery) as accounts left join accounts_cstm on accounts_cstm.id_c = accounts.id where accounts.deleted = '0'  and accounts.assigned_user_id ='".$_POST["users"][$i]."' and accounts_cstm.lead_account_status_c = 'Transaction'".$accountTransaction;
									$result19=$db->query($query19);
									$customerr = 0;
									$amountt = 0;
									while($row19 =$db->fetchByAssoc($result19)){
										 
										$query20="select opportunities.amount as amount  from opportunities inner join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id inner join 
										accounts on accounts.id = accounts_opportunities.account_id where opportunities.sales_stage = 'Closed Won' and accounts.id ='".$row19['id']."' and opportunities.deleted = '0' ";
									$result20=$db->query($query20);
									$row20 =$db->fetchByAssoc($result20);
										if($row20['amount'] != null && $row20['amount'] != ''){
											$amountt+=$row20['amount'];

										}
									}
									$query23="select count(accounts.id) as customer  from accounts_cstm right join accounts on accounts_cstm.id_c = accounts.id where accounts.deleted = '0'  and accounts.assigned_user_id ='".$_POST["users"][$i]."' and accounts_cstm.lead_account_status_c = 'Transaction'".$accountTransaction;
									$result23=$db->query($query23);
									$row23 =$db->fetchByAssoc($result23);
									if($row23['customer'] != null && $row23['customer'] != ''){
										$customerr = $row23['customer'];
									}
										if($employee == true){
											if($_POST["users"][$i] == $_SESSION['authenticated_user_id']){	
												$user=array(
														"user"=>$row['name'],
														"New"=>$row4["customer"],
														"userAmountNew"=>$row7['amount'],
														"InProcess"=>$row1["customer"],
														"userAmountInprocess"=>$row8['amount'],
														"Converted"=>$row2["customer"],
														"userAmountConverted"=>$row13['amount'],
														"Dead"=>$row12["customer"],
														"userAmountDead"=>$row14["amount"],
														"Transaction"=>$customerr,
														"userAmountTransaction"=>$amountt,
														"ClosedWon"=>$row5["customer"],
														"userAmountClosedWon"=>$row10['amount'],
														"Other"=>$row18["customer"],
														"userAmountOther"=>$row9['amount'],
														"ClosedLost"=>$row6["customer"],
														"userAmountClosedLost"=>$row11['amount'],
														"Call"=>$row15["ca"],
														"Meeting"=>$row16["meeting"],
														"Email"=>$row17["email"]
													);
												if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
													$group[$row["Se"]]=array();
													$group[$row["Se"]][]=$user;
												}else{
													$group[$row["Se"]][]=$user;
												}
												$New+=$row4["customer"];
												$InProcess+=$row1["customer"];
												$Dead+=$row12["customer"];
												$Converted+=$row2["customer"];
												$Transaction+=$customerr;
												$CloseWon+=$row5["customer"];
												$CloseLost+=$row6["customer"];
												$Other+=$row18["customer"];
												$amountNew+=$row7["amount"];
												$amountInProcess+=$row8["amount"];
												$amountDead+=$row14["amount"];
												$amountConverted+=$row13["amount"];
												$amountTran+=$amountt;
												$amountWon+=$row10["amount"];
												$amountLost+=$row11["amount"];
												$amountCall+=$row15["ca"];
												$amountMeeting+=$row16["meeting"];
												$amountEmail+=$row17["email"];
												$amountOther+=$row9["amount"];
											}
										}else{
											$user=array(
														"user"=>$row['name'],
														"New"=>$row4["customer"],
														"userAmountNew"=>$row7['amount'],
														"InProcess"=>$row1["customer"],
														"userAmountInprocess"=>$row8['amount'],
														"Converted"=>$row2["customer"],
														"userAmountConverted"=>$row13['amount'],
														"Dead"=>$row12["customer"],
														"userAmountDead"=>$row14["amount"],
														"Transaction"=>$customerr,
														"userAmountTransaction"=>$amountt,
														"ClosedWon"=>$row5["customer"],
														"userAmountClosedWon"=>$row10['amount'],
														"Other"=>$row18["customer"],
														"userAmountOther"=>$row9['amount'],
														"ClosedLost"=>$row6["customer"],
														"userAmountClosedLost"=>$row11['amount'],
														"Call"=>$row15["ca"],
														"Meeting"=>$row16["meeting"],
														"Email"=>$row17["email"]
													);
												if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
													$group[$row["Se"]]=array();
													$group[$row["Se"]][]=$user;
												}else{
													$group[$row["Se"]][]=$user;
												}
												$New+=$row4["customer"];
												$InProcess+=$row1["customer"];
												$Dead+=$row12["customer"];
												$Converted+=$row2["customer"];
												$Transaction+=$customerr;
												$CloseWon+=$row5["customer"];
												$CloseLost+=$row6["customer"];
												$Other+=$row18["customer"];
												$amountNew+=$row7["amount"];
												$amountInProcess+=$row8["amount"];
												$amountDead+=$row14["amount"];
												$amountConverted+=$row13["amount"];
												$amountTran+=$amountt;
												$amountWon+=$row10["amount"];
												$amountLost+=$row11["amount"];
												$amountCall+=$row15["ca"];
												$amountMeeting+=$row16["meeting"];
												$amountEmail+=$row17["email"];
												$amountOther+=$row9["amount"];
										}
									}
								}
							}
						}	
						
						
					}
					else
					{
						if($_POST["users"] == null || $_POST["users"] == ''){
							if($_POST["titleUsers"] == null || $_POST["titleUsers"] == ''){
							$grs = "";
							for($j=0;$j<count($_POST["groupUsers"]);$j++){
							$grs.="'".$_POST["groupUsers"][$j]."'";
							if($j<count($_POST["groupUsers"])-1){
								$grs.=",";	
							}
							if($_POST["groupUsers"][$j] == "All"){
								$grs="All";
								break;
							}
						}
						if($grs != "All"){
							if($employer == false || $employee == true || $leader == true){
								if($leader == true){
									$qr="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
									inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
									users.status='Active' and users_cstm.provincecode_c='".$k."' and securitygroups_users.securitygroup_id In (".$grs.") and securitygroups_users.securitygroup_id In (".$whr.")";
								}
								if($employee == true){
									$qr="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
									inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
									users.status='Active' and users_cstm.provincecode_c='".$k."' and securitygroups_users.securitygroup_id In (".$grs.") and users.id =  '".$_SESSION['authenticated_user_id']."'";
								}
							}else{
								if($employer == true){
									$qr="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
									inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
									users.status='Active' and users_cstm.provincecode_c='".$k."' and securitygroups_users.securitygroup_id In (".$grs.")";	
								}							
							}								
						}else{
							if($employer == false || $employee == true || $leader == true){
								if($leader == true){
									$qr="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
									inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
									users.status='Active' and users_cstm.provincecode_c='".$k."' and securitygroups_users.securitygroup_id In (".$whr.")";	
								}
								if($employee == true){
									$qr="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
									inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
									users.status='Active' and users_cstm.provincecode_c='".$k."' and users.id =  '".$_SESSION['authenticated_user_id']."'";	
								}
							}else{
								if($employer == true){
									$qr="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
									inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
									users.status='Active' and users_cstm.provincecode_c='".$k."'";	
								}
								
							}
						}
						$res=$db->query($qr);
						$row=$db->fetchByAssoc($res);
						if(!empty($row)){
							$p = $k;
							do{
								if($row["seName"]==$row["Se"]){
								$query4="select count(id) as customer from ($queryLead) as leads where status ='New' and deleted='0' and assigned_user_id ='".$row['id']."'".$leadWhere;
								$result4=$db->query($query4);
								$row4 =$db->fetchByAssoc($result4);
								
								$query1="select count(id) as customer from ($queryLead) as leads where status ='In Process' and deleted='0' and assigned_user_id ='".$row['id']."'".$leadWhere;
								$result1=$db->query($query1);
								$row1 =$db->fetchByAssoc($result1);
									
								$query2="select count(accounts.id) as customer from ($accountQuery) as accounts inner join accounts_cstm on accounts.id=accounts_cstm.id_c where accounts_cstm.lead_account_status_c 
								='Converted' and accounts.deleted='0' and accounts.assigned_user_id ='".$row['id']."'".$accountWhere;
								$result2=$db->query($query2);
								$row2 =$db->fetchByAssoc($result2);
								
								$query12="select count(id) as customer from ($queryLead) as leads where status ='Dead' and deleted='0' and assigned_user_id ='".$row['id']."'".$leadWhere;
								$result12=$db->query($query12);
								$row12 =$db->fetchByAssoc($result12);
									
								
								
								$query5="select count(opportunities.id) as customer from opportunities inner join accounts_opportunities on opportunities.id=accounts_opportunities.opportunity_id inner join ($accountQuery) as accounts 
								on accounts_opportunities.account_id=accounts.id where opportunities.sales_stage='Closed Won' and opportunities.assigned_user_id ='".$row['id']."' and opportunities.deleted='0'".$oppWhere;
								$result5=$db->query($query5);
								$row5 =$db->fetchByAssoc($result5);
								
								$query6="select count(opportunities.id) as customer from opportunities inner join accounts_opportunities on opportunities.id=accounts_opportunities.opportunity_id inner join ($accountQuery) as accounts 
								on accounts_opportunities.account_id=accounts.id where opportunities.sales_stage='Closed Lost' and accounts.assigned_user_id ='".$row['id']."' and accounts.deleted='0'".$oppWhere;
								$result6=$db->query($query6);
								$row6 =$db->fetchByAssoc($result6);
								
								$query7="select sum(opportunity_amount) as amount from ($leadQuery) as leads where status ='New' and deleted='0' and assigned_user_id ='".$row['id']."'".$leadWhere;
								$result7=$db->query($query7);
								$row7 =$db->fetchByAssoc($result7);
								if($row7['amount']==null || $row7['amount']==''){
									$row7['amount']=0;
								}
								$query8="select sum(opportunity_amount) as amount from ($leadQuery) as leads where status ='In Process' and deleted='0' and assigned_user_id ='".$row['id']."'".$leadWhere;
								$result8=$db->query($query8);
								$row8 =$db->fetchByAssoc($result8);
								if($row8['amount'] == null || $row8['amount'] == ''){
										$row8['amount']=0;
								}
								$query9="select sum(amount) as amount from ($oppQuery) as opportunities where sales_stage Not In('Closed Won','Closed Lost') and assigned_user_id ='".$row['id']."' and deleted='0'".$oppWhere;
								$result9=$db->query($query9);
								$row9 =$db->fetchByAssoc($result9);
								if($row9['amount'] == null || $row9['amount'] == ''){
									$row9['amount']=0;
								}
								$query10="select sum(opportunities.amount) as amount  from opportunities  where opportunities.sales_stage = 'Closed Won' and opportunities.assigned_user_id ='".$row['id']."' and opportunities.deleted = '0'".$accountWhereTran;
								$result10=$db->query($query10);
								$row10 =$db->fetchByAssoc($result10);
								if($row10['amount'] == null || $row10['amount'] == ''){
									$row10['amount']=0;
								}
								$query11="select sum(amount) as amount from opportunities where sales_stage='Closed Lost' and assigned_user_id ='".$row['id']."' and deleted='0'".$oppWhere;
								$result11=$db->query($query11);
								$row11 =$db->fetchByAssoc($result11);
								if($row11['amount'] == null || $row11['amount'] == ''){
									$row11['amount']=0;
								}
								$query13="select sum(opportunities.amount) as amount from opportunities inner join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id
									inner join accounts_cstm on accounts_opportunities.account_id = accounts_cstm.id_c where opportunities.sales_stage NOT IN('Closed Won','Closed Lost') and opportunities.assigned_user_id ='".$row['id']."'
									and opportunities.deleted='0' and accounts_cstm.lead_account_status_c='Converted'".$oppWhere;
								$result13=$db->query($query13);
								$row13 =$db->fetchByAssoc($result13);
								if($row13['amount'] == null || $row13['amount'] == ''){
										$row13['amount']=0;
								}
								$query14="select sum(opportunity_amount) as amount from ($leadQuery) as leads where status ='Dead' and deleted='0' and assigned_user_id ='".$row['id']."'".$leadWhere;
								$result14=$db->query($query14);
								$row14 =$db->fetchByAssoc($result14);
								if($row14['amount']==null || $row14['amount']==''){
									$row14['amount']=0;
								}
								$query15="select count(id) as ca from ($callQuery) as  calls where deleted='0' and status='Held' and assigned_user_id ='".$row['id']."'".$callWhere;
								$result15=$db->query($query15);
								$row15 =$db->fetchByAssoc($result15);
								if($row15['ca']==null || $row15['ca']==''){
									$row15['ca']=0;
								}
								$query16="select count(id) as meeting from ($meetingQuery) as meetings  where deleted='0' and status='Held' and assigned_user_id ='".$row['id']."'".$meetingWhere;
								$result16=$db->query($query16);
								$row16 =$db->fetchByAssoc($result16);
								if($row16['meeting']==null || $row16['meeting']==''){
									$row16['meeting']=0;
								}
								$query17="select count(id) as email from emails where deleted='0' and assigned_user_id ='".$row['id']."'".$emailWhere;
								$result17=$db->query($query17);
								$row17 =$db->fetchByAssoc($result17);
								if($row17['email']==null || $row17['email']==''){
									$row17['email']=0;
								}
								$query18="select count(opportunities.id) as customer from opportunities inner join accounts_opportunities on opportunities.id=accounts_opportunities.opportunity_id inner join ($accountQuery) as accounts 
								on accounts_opportunities.account_id=accounts.id where opportunities.sales_stage not in('Closed Won','Closed Lost') and accounts.assigned_user_id ='".$row['id']."' and accounts.deleted='0'".$oppWhere;
								$result18=$db->query($query18);
								$row18 =$db->fetchByAssoc($result18);
								if($row18['customer']==null || $row18['customer']==''){
									$row18['customer']=0;
								}
								$query21 = "select distinct(accounts.assigned_user_id) as assigned from opportunities inner join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id inner join 
									accounts on accounts.id = accounts_opportunities.account_id where opportunities.sales_stage = 'Closed Won' and opportunities.assigned_user_id ='".$row['id']."' and opportunities.deleted = '0'  and accounts.deleted = '0'".$accountWhereTran;
									$result21 = $db->query($query21);
									$whereAss = '';
									while($row21 = $db->fetchByAssoc($result21)){
										if(!empty($row21["assigned"])){
											$whereAss .= "'".$row21["assigned"]."',";
											
										}
									}
									$query19 = "select accounts.id as id  from ($accountQuery) as accounts left join accounts_cstm on accounts_cstm.id_c = accounts.id where accounts.deleted = '0'  and accounts.assigned_user_id ='".$row["id"]."' and accounts_cstm.lead_account_status_c = 'Transaction'".$accountTransaction;
									$result19=$db->query($query19);
									$customerr = 0;
									$amountt = 0;
									while($row19 =$db->fetchByAssoc($result19)){
										 
										$query20="select opportunities.amount as amount  from opportunities inner join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id inner join 
										accounts on accounts.id = accounts_opportunities.account_id where opportunities.sales_stage = 'Closed Won' and accounts.id ='".$row19['id']."' and opportunities.deleted = '0' ";
									$result20=$db->query($query20);
									$row20 =$db->fetchByAssoc($result20);
										if($row20['amount'] != null && $row20['amount'] != ''){
											$amountt+=$row20['amount'];

										}
									}
									$query23="select count(accounts.id) as customer  from accounts_cstm right join accounts on accounts_cstm.id_c = accounts.id where accounts.deleted = '0'  and accounts.assigned_user_id ='".$row["id"]."' and accounts_cstm.lead_account_status_c = 'Transaction'".$accountTransaction;
									$result23=$db->query($query23);
									$row23 =$db->fetchByAssoc($result23);
									if($row23['customer'] != null && $row23['customer'] != ''){
										$customerr = $row23['customer'];
									}
									if($employee == true){
											if($row['id'] == $_SESSION['authenticated_user_id']){	
												$user=array(
														"user"=>$row['name'],
														"New"=>$row4["customer"],
														"userAmountNew"=>$row7['amount'],
														"InProcess"=>$row1["customer"],
														"userAmountInprocess"=>$row8['amount'],
														"Converted"=>$row2["customer"],
														"userAmountConverted"=>$row13['amount'],
														"Dead"=>$row12["customer"],
														"userAmountDead"=>$row14["amount"],
														"Transaction"=>$customerr,
														"userAmountTransaction"=>$amountt,
														"ClosedWon"=>$row5["customer"],
														"userAmountClosedWon"=>$row10['amount'],
														"Other"=>$row18["customer"],
														"userAmountOther"=>$row9['amount'],
														"ClosedLost"=>$row6["customer"],
														"userAmountClosedLost"=>$row11['amount'],
														"Call"=>$row15["ca"],
														"Meeting"=>$row16["meeting"],
														"Email"=>$row17["email"]
													);
												if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
														$group[$row["Se"]]=array();
														$group[$row["Se"]][]=$user;
												}
												else{
														$group[$row["Se"]][]=$user;
												}
												$New+=$row4["customer"];
												$InProcess+=$row1["customer"];
												$Converted+=$row2["customer"];
												$Dead+=$row12["customer"];
												$Transaction+=$customerr;
												$CloseWon+=$row5["customer"];
												$CloseLost+=$row6["customer"];
												$Other+=$row18["customer"];
												$amountNew+=$row7["amount"];
												$amountInProcess+=$row8["amount"];
												$amountDead+=$row14["amount"];
												$amountConverted+=$row13["amount"];
												$amountTran+=$amountt;
												$amountWon+=$row10["amount"];
												$amountLost+=$row11["amount"];
												$amountCall+=$row15["ca"];
												$amountMeeting+=$row16["meeting"];
												$amountEmail+=$row17["email"];
												$amountOther+=$row9["amount"];
										}
									}else{
										$user=array(
												"user"=>$row['name'],
												"New"=>$row4["customer"],
												"userAmountNew"=>$row7['amount'],
												"InProcess"=>$row1["customer"],
												"userAmountInprocess"=>$row8['amount'],
												"Converted"=>$row2["customer"],
												"userAmountConverted"=>$row13['amount'],
												"Dead"=>$row12["customer"],
												"userAmountDead"=>$row14["amount"],
												"Transaction"=>$customerr,
												"userAmountTransaction"=>$amountt,
												"ClosedWon"=>$row5["customer"],
												"userAmountClosedWon"=>$row10['amount'],
												"Other"=>$row18["customer"],
												"userAmountOther"=>$row9['amount'],
												"ClosedLost"=>$row6["customer"],
												"userAmountClosedLost"=>$row11['amount'],
												"Call"=>$row15["ca"],
												"Meeting"=>$row16["meeting"],
												"Email"=>$row17["email"]
											);
										if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
												$group[$row["Se"]]=array();
												$group[$row["Se"]][]=$user;
										}
										else{
												$group[$row["Se"]][]=$user;
										}
										$New+=$row4["customer"];
										$InProcess+=$row1["customer"];
										$Converted+=$row2["customer"];
										$Dead+=$row12["customer"];
										$Transaction+=$customerr;
										$CloseWon+=$row5["customer"];
										$CloseLost+=$row6["customer"];
										$Other+=$row18["customer"];
										$amountNew+=$row7["amount"];
										$amountInProcess+=$row8["amount"];
										$amountDead+=$row14["amount"];
										$amountConverted+=$row13["amount"];
										$amountTran+=$amountt;
										$amountWon+=$row10["amount"];
										$amountLost+=$row11["amount"];
										$amountCall+=$row15["ca"];
										$amountMeeting+=$row16["meeting"];
										$amountEmail+=$row17["email"];
										$amountOther+=$row9["amount"];
									}
								}
							}while($row=$db->fetchByAssoc($res));
						}
						}else{
							for($j=0;$j<count($_POST["groupUsers"]);$j++){
							$grs.="'".$_POST["groupUsers"][$j]."'";
								if($j<count($_POST["groupUsers"])-1){
									$grs.=",";	
								}
								if($_POST["groupUsers"][$j] == "All"){
									$grs="All";
									break;
								}
							}
						for($i=0;$i<count($_POST["titleUsers"]);$i++){
							if($grs != "All"){
								if($employer == false || $employee == true || $leader == true){
									if($leader == true){
										$q="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
										inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
										users.status='Active' and users_cstm.provincecode_c='".$k."' and securitygroups_users.securitygroup_id In (".$grs.") and securitygroups_users.securitygroup_id In (".$whr.") and users.title = '".$_POST["titleUsers"][$i]."'";
									}
									if($employee == true){
										$q="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
										inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
										users.status='Active' and users_cstm.provincecode_c='".$k."' and securitygroups_users.securitygroup_id In (".$grs.") and users.id =  '".$_SESSION['authenticated_user_id']."' and users.title = '".$_POST["titleUsers"][$i]."'";
									}
								}else{
									if($employer == true){
										$q="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
										inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
										users.status='Active' and users_cstm.provincecode_c='".$k."' and securitygroups_users.securitygroup_id In (".$grs.") and users.title = '".$_POST["titleUsers"][$i]."'";
									}
								
								}
							
							}else{
								if($employer == false || $employee == true || $leader == true){
									if($leader == true){
										$q="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
										inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
										users.status='Active' and users_cstm.provincecode_c='".$k."' and securitygroups_users.securitygroup_id In (".$whr.") and users.title = '".$_POST["titleUsers"][$i]."'";
									}
									if($employee == true){
										$q="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
										inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
										users.status='Active' and users_cstm.provincecode_c='".$k."' and users.id =  '".$_SESSION['authenticated_user_id']."' and users.title = '".$_POST["titleUsers"][$i]."'";
									}
								}else{
									if($employer == true){
										$q="select distinct(securitygroups.name) as seName, users.id as id,users.user_name as name,users.department as Se from users inner join securitygroups_users on securitygroups_users.user_id=users.id 
										inner join users_cstm on users_cstm.id_c=users.id inner join securitygroups on securitygroups_users.securitygroup_id=securitygroups.id where users.deleted='0' and 
										users.status='Active' and users_cstm.provincecode_c='".$k."'  and users.title = '".$_POST["titleUsers"][$i]."'";
									}
									
								}
								
							}
								$r=$db->query($q);
								while($rww=$db->fetchByAssoc($r)){
									if(!empty($rww)){
										$p=$k;
										if($rww["seName"]==$rww["Se"]){
											$qr="select users_cstm.provincecode_c as province, users.user_name as name,users.department as Se from users inner join securitygroups_users on users.id = securitygroups_users.user_id inner join users_cstm on users.id = users_cstm.id_c where users.deleted='0' and users.status='Active' 
											and  users.id='".$rww["id"]."' and securitygroups_users.securitygroup_id In (".$whr.")";	
											$res=$db->query($qr);
											$row=$db->fetchByAssoc($res);
											if(!empty($row)){
												if($row["province"] == $k){
												$p=$k;
												$query4="select count(id) as customer from ($queryLead) as leads where status ='New' and deleted='0' and assigned_user_id ='".$rww["id"]."'".$leadWhere;
												$result4=$db->query($query4);
												$row4 =$db->fetchByAssoc($result4);
												
												$query1="select count(id) as customer from ($queryLead) as leads where status ='In Process' and deleted='0' and assigned_user_id ='".$rww["id"]."'".$leadWhere;
												$result1=$db->query($query1);
												$row1 =$db->fetchByAssoc($result1);
													
												$query2="select count(accounts.id) as customer from ($accountQuery) as accounts inner join accounts_cstm on accounts.id=accounts_cstm.id_c where accounts_cstm.lead_account_status_c 
												='Converted' and accounts.deleted='0' and accounts.assigned_user_id ='".$rww["id"]."'".$accountWhere;
												$result2=$db->query($query2);
												$row2 =$db->fetchByAssoc($result2);
												
												$query12="select count(id) as customer from ($queryLead) as leads where status ='Dead' and deleted='0' and assigned_user_id ='".$rww["id"]."'".$leadWhere;
												$result12=$db->query($query12);
												$row12 =$db->fetchByAssoc($result12);
												
												
												
												$query5="select count(opportunities.id) as customer from opportunities inner join accounts_opportunities on opportunities.id=accounts_opportunities.opportunity_id inner join ($accountQuery) as accounts 
												on accounts_opportunities.account_id=accounts.id where opportunities.sales_stage='Closed Won' and opportunities.assigned_user_id ='".$rww["id"]."' and opportunities.deleted='0'".$oppWhere;
												$result5=$db->query($query5);
												$row5 =$db->fetchByAssoc($result5);
												
												$query6="select count(opportunities.id) as customer from opportunities inner join accounts_opportunities on opportunities.id=accounts_opportunities.opportunity_id inner join ($accountQuery) as accounts 
												on accounts_opportunities.account_id=accounts.id where opportunities.sales_stage='Closed Lost' and accounts.assigned_user_id ='".$rww["id"]."' and accounts.deleted='0'".$oppWhere;
												$result6=$db->query($query6);
												$row6 =$db->fetchByAssoc($result6);
												
												$query7="select sum(opportunity_amount) as amount from ($leadQuery) as leads where status ='New' and deleted='0' and assigned_user_id ='".$rww["id"]."'".$leadWhere;
												$result7=$db->query($query7);
												$row7 =$db->fetchByAssoc($result7);
												if($row7['amount']==null || $row7['amount']==''){
													$row7['amount']=0;
												}
												$query8="select sum(opportunity_amount) as amount from ($leadQuery) as leads where status ='In Process' and deleted='0' and assigned_user_id ='".$rww["id"]."'".$leadWhere;
												$result8=$db->query($query8);
												$row8 =$db->fetchByAssoc($result8);
												if($row8['amount'] == null || $row8['amount'] == ''){
													$row8['amount']=0;
												}
												$query9="select sum(amount) as amount from ($oppQuery) as opportunities where sales_stage Not In('Closed Won','Closed Lost') and assigned_user_id ='".$rww["id"]."' and deleted='0'".$oppWhere;
												$result9=$db->query($query9);
												$row9 =$db->fetchByAssoc($result9);
												if($row9['amount'] == null || $row9['amount'] == ''){
													$row9['amount']=0;
												}
												$query10="select sum(opportunities.amount) as amount  from opportunities  where opportunities.sales_stage = 'Closed Won' and opportunities.assigned_user_id ='".$rww["id"]."' and opportunities.deleted = '0'".$accountWhereTran;
												$result10=$db->query($query10);
												$row10 =$db->fetchByAssoc($result10);
												if($row10['amount'] == null || $row10['amount'] == ''){
													$row10['amount']=0;
												}
												$query11="select sum(amount) as amount from opportunities where sales_stage='Closed Lost' and assigned_user_id ='".$rww["id"]."' and deleted='0'".$oppWhere;
												$result11=$db->query($query11);
												$row11 =$db->fetchByAssoc($result11);
												if($row11['amount'] == null || $row11['amount'] == ''){
													$row11['amount']=0;
												}
												$query13="select sum(opportunities.amount) as amount from opportunities inner join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id
													inner join accounts_cstm on accounts_opportunities.account_id = accounts_cstm.id_c where opportunities.sales_stage NOT IN('Closed Won','Closed Lost') and opportunities.assigned_user_id ='".$rww["id"]."'
													and opportunities.deleted='0' and accounts_cstm.lead_account_status_c='Converted'".$oppWhere;
												$result13=$db->query($query13);
												$row13 =$db->fetchByAssoc($result13);
												if($row13['amount'] == null || $row13['amount'] == ''){
													$row13['amount']=0;
												}
												$query14="select sum(opportunity_amount) as amount from ($leadQuery) as leads where status ='Dead' and deleted='0' and assigned_user_id ='".$rww["id"]."'".$leadWhere;
												$result14=$db->query($query14);
												$row14 =$db->fetchByAssoc($result14);
												if($row14['amount']==null || $row14['amount']==''){
													$row14['amount']=0;
												}
												$query15="select count(id) as ca from ($callQuery) as  calls where deleted='0' and status='Held' and assigned_user_id ='".$rww["id"]."'".$callWhere;
													$result15=$db->query($query15);
													$row15 =$db->fetchByAssoc($result15);
													if($row15['ca']==null || $row15['ca']==''){
														$row15['ca']=0;
												}
												$query16="select count(id) as meeting from ($meetingQuery) as meetings  where deleted='0' and status='Held' and assigned_user_id ='".$rww["id"]."'".$meetingWhere;
												$result16=$db->query($query16);
												$row16 =$db->fetchByAssoc($result16);
												if($row16['meeting']==null || $row16['meeting']==''){
													$row16['meeting']=0;
												}
												$query17="select count(id) as email from emails where deleted='0' and assigned_user_id ='".$rww["id"]."'".$emailWhere;
												$result17=$db->query($query17);
												$row17 =$db->fetchByAssoc($result17);
												if($row17['email']==null || $row17['email']==''){
													$row17['email']=0;
												}
												$query18="select count(opportunities.id) as customer from opportunities inner join accounts_opportunities on opportunities.id=accounts_opportunities.opportunity_id inner join ($accountQuery) as accounts 
												on accounts_opportunities.account_id=accounts.id where opportunities.sales_stage not in('Closed Won','Closed Lost') and accounts.assigned_user_id ='".$rww["id"]."' and accounts.deleted='0'".$oppWhere;
												$result18=$db->query($query18);
												$row18 =$db->fetchByAssoc($result18);
												if($row18['customer']==null || $row18['customer']==''){
													$row18['customer']=0;
												}
												$query21 = "select distinct(accounts.assigned_user_id) as assigned from opportunities inner join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id inner join 
												accounts on accounts.id = accounts_opportunities.account_id where opportunities.sales_stage = 'Closed Won' and opportunities.assigned_user_id ='".$rww['id']."' and opportunities.deleted = '0'  and accounts.deleted = '0'".$accountWhereTran;
												$result21 = $db->query($query21);
												$whereAss = '';
												while($row21 = $db->fetchByAssoc($result21)){
													if(!empty($row21["assigned"])){
														$whereAss .= "'".$row21["assigned"]."',";
														
													}
												}
												$query19 = "select accounts.id as id  from ($accountQuery) as accounts left join accounts_cstm on accounts_cstm.id_c = accounts.id where accounts.deleted = '0'  and accounts.assigned_user_id ='".$rww["id"]."' and accounts_cstm.lead_account_status_c = 'Transaction'".$accountTransaction;
									$result19=$db->query($query19);
									$customerr = 0;
									$amountt = 0;
									while($row19 =$db->fetchByAssoc($result19)){
										 
										$query20="select opportunities.amount as amount  from opportunities inner join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id inner join 
										accounts on accounts.id = accounts_opportunities.account_id where opportunities.sales_stage = 'Closed Won' and accounts.id ='".$row19['id']."' and opportunities.deleted = '0' ";
									$result20=$db->query($query20);
									$row20 =$db->fetchByAssoc($result20);
										if($row20['amount'] != null && $row20['amount'] != ''){
											$amountt+=$row20['amount'];

										}
									}
												$query23="select count(accounts.id) as customer  from accounts_cstm right join accounts on accounts_cstm.id_c = accounts.id where accounts.deleted = '0'  and accounts.assigned_user_id ='".$rww["id"]."' and accounts_cstm.lead_account_status_c = 'Transaction'".$accountTransaction;
									$result23=$db->query($query23);
									$row23 =$db->fetchByAssoc($result23);
									if($row23['customer'] != null && $row23['customer'] != ''){
										$customerr = $row23['customer'];
									}
													if($employee == true){
														if($rww['id'] == $_SESSION['authenticated_user_id']){	
															$user=array(
																	"user"=>$row['name'],
																	"New"=>$row4["customer"],
																	"userAmountNew"=>$row7['amount'],
																	"InProcess"=>$row1["customer"],
																	"userAmountInprocess"=>$row8['amount'],
																	"Converted"=>$row2["customer"],
																	"userAmountConverted"=>$row13['amount'],
																	"Dead"=>$row12["customer"],
																	"userAmountDead"=>$row14["amount"],
																	"Transaction"=>$customerr,
																	"userAmountTransaction"=>$amountt,
																	"ClosedWon"=>$row5["customer"],
																	"userAmountClosedWon"=>$row10['amount'],
																	"Other"=>$row18["customer"],
																	"userAmountOther"=>$row9['amount'],
																	"ClosedLost"=>$row6["customer"],
																	"userAmountClosedLost"=>$row11['amount'],
																	"Call"=>$row15["ca"],
																	"Meeting"=>$row16["meeting"],
																	"Email"=>$row17["email"]
																);
															if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
																$group[$row["Se"]]=array();
																$group[$row["Se"]][]=$user;
															}else{
																$group[$row["Se"]][]=$user;
															}
															$New+=$row4["customer"];
															$InProcess+=$row1["customer"];
															$Dead+=$row12["customer"];
															$Converted+=$row2["customer"];
															$Transaction+=$customerr;
															$CloseWon+=$row5["customer"];
															$CloseLost+=$row6["customer"];
															$Other+=$row18["customer"];
															$amountNew+=$row7["amount"];
															$amountInProcess+=$row8["amount"];
															$amountDead+=$row14["amount"];
															$amountConverted+=$row13["amount"];
															$amountTran+=$amountt;
															$amountWon+=$row10["amount"];
															$amountLost+=$row11["amount"];
															$amountCall+=$row15["ca"];
															$amountMeeting+=$row16["meeting"];
															$amountEmail+=$row17["email"];
															$amountOther+=$row9["amount"];
														}
													}else{
														$user=array(
																	"user"=>$row['name'],
																	"New"=>$row4["customer"],
																	"userAmountNew"=>$row7['amount'],
																	"InProcess"=>$row1["customer"],
																	"userAmountInprocess"=>$row8['amount'],
																	"Converted"=>$row2["customer"],
																	"userAmountConverted"=>$row13['amount'],
																	"Dead"=>$row12["customer"],
																	"userAmountDead"=>$row14["amount"],
																	"Transaction"=>$customerr,
																	"userAmountTransaction"=>$amountt,
																	"ClosedWon"=>$row5["customer"],
																	"userAmountClosedWon"=>$row10['amount'],
																	"Other"=>$row18["customer"],
																	"userAmountOther"=>$row9['amount'],
																	"ClosedLost"=>$row6["customer"],
																	"userAmountClosedLost"=>$row11['amount'],
																	"Call"=>$row15["ca"],
																	"Meeting"=>$row16["meeting"],
																	"Email"=>$row17["email"]
																);
															if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
																$group[$row["Se"]]=array();
																$group[$row["Se"]][]=$user;
															}else{
																$group[$row["Se"]][]=$user;
															}
															$New+=$row4["customer"];
															$InProcess+=$row1["customer"];
															$Dead+=$row12["customer"];
															$Converted+=$row2["customer"];
															$Transaction+=$customerr;
															$CloseWon+=$row5["customer"];
															$CloseLost+=$row6["customer"];
															$Other+=$row18["customer"];
															$amountNew+=$row7["amount"];
															$amountInProcess+=$row8["amount"];
															$amountDead+=$row14["amount"];
															$amountConverted+=$row13["amount"];
															$amountTran+=$amountt;
															$amountWon+=$row10["amount"];
															$amountLost+=$row11["amount"];
															$amountCall+=$row15["ca"];
															$amountMeeting+=$row16["meeting"];
															$amountEmail+=$row17["email"];
															$amountOther+=$row9["amount"];
													}
												}
											}
										}
									}
								}
							}
						}
					}
					else
					{
						for($i=0;$i<count($_POST["users"]);$i++){
							if($employer == false || $employee == true || $leader == true){
								if($leader == true){
									$qr="select users_cstm.provincecode_c as province, users.user_name as name,users.department as Se from users inner join securitygroups_users on users.id = securitygroups_users.user_id inner join users_cstm on users.id = users_cstm.id_c where users.deleted='0' and users.status='Active' 
									and  users.id='".$_POST["users"][$i]."' and securitygroups_users.securitygroup_id In (".$whr.")";	
								}
								if($employee == true){
									$qr="select users_cstm.provincecode_c as province, users.user_name as name,users.department as Se from users inner join securitygroups_users on users.id = securitygroups_users.user_id inner join users_cstm on users.id = users_cstm.id_c where users.deleted='0' and users.status='Active' 
									and  users.id='".$_POST["users"][$i]."' and users.id =  '".$_SESSION['authenticated_user_id']."'";
								}
							}else{
								if($employer == true){
									$qr="select users_cstm.provincecode_c as province, users.user_name as name,users.department as Se from users inner join securitygroups_users on users.id = securitygroups_users.user_id inner join users_cstm on users.id = users_cstm.id_c where users.deleted='0' and users.status='Active' 
									and  users.id='".$_POST["users"][$i]."'";	
								}
								
							}
							
							$res=$db->query($qr);
							$row=$db->fetchByAssoc($res);
							if(!empty($row)){
								if($row["province"] == $k){
								$p=$k;
								$query4="select count(id) as customer from ($queryLead) as leads where status ='New' and deleted='0' and assigned_user_id ='".$_POST["users"][$i]."'".$leadWhere;
								$result4=$db->query($query4);
								$row4 =$db->fetchByAssoc($result4);
								
								$query1="select count(id) as customer from ($queryLead) as leads where status ='In Process' and deleted='0' and assigned_user_id ='".$_POST["users"][$i]."'".$leadWhere;
								$result1=$db->query($query1);
								$row1 =$db->fetchByAssoc($result1);
									
								$query2="select count(accounts.id) as customer from ($accountQuery) as accounts inner join accounts_cstm on accounts.id=accounts_cstm.id_c where accounts_cstm.lead_account_status_c 
								='Converted' and accounts.deleted='0' and accounts.assigned_user_id ='".$_POST["users"][$i]."'".$accountWhere;
								$result2=$db->query($query2);
								$row2 =$db->fetchByAssoc($result2);
								
								$query12="select count(id) as customer from ($queryLead) as leads where status ='Dead' and deleted='0' and assigned_user_id ='".$_POST["users"][$i]."'".$leadWhere;
								$result12=$db->query($query12);
								$row12 =$db->fetchByAssoc($result12);
									
								
								
								$query5="select count(opportunities.id) as customer from opportunities inner join accounts_opportunities on opportunities.id=accounts_opportunities.opportunity_id inner join ($accountQuery) as accounts 
								on accounts_opportunities.account_id=accounts.id where opportunities.sales_stage='Closed Won' and opportunities.assigned_user_id ='".$_POST["users"][$i]."' and opportunities.deleted='0'".$oppWhere;
								$result5=$db->query($query5);
								$row5 =$db->fetchByAssoc($result5);
							
								$query6="select count(opportunities.id) as customer from opportunities inner join accounts_opportunities on opportunities.id=accounts_opportunities.opportunity_id inner join ($accountQuery) as accounts 
								on accounts_opportunities.account_id=accounts.id where opportunities.sales_stage='Closed Lost' and accounts.assigned_user_id ='".$_POST["users"][$i]."' and accounts.deleted='0'".$oppWhere;
								$result6=$db->query($query6);
								$row6 =$db->fetchByAssoc($result6);
								
								$query7="select sum(opportunity_amount) as amount from ($leadQuery) as leads where status ='New' and deleted='0' and assigned_user_id ='".$_POST["users"][$i]."'".$leadWhere;
								$result7=$db->query($query7);
								$row7 =$db->fetchByAssoc($result7);
								if($row7['amount']==null || $row7['amount']==''){
									$row7['amount']=0;
								}
								$query8="select sum(opportunity_amount) as amount from ($leadQuery) as leads where status ='In Process' and deleted='0' and assigned_user_id ='".$_POST["users"][$i]."'".$leadWhere;
								$result8=$db->query($query8);
								$row8 =$db->fetchByAssoc($result8);
								if($row8['amount'] == null || $row8['amount'] == ''){
									$row8['amount']=0;
								}
								$query9="select sum(amount) as amount from ($oppQuery) as opportunities where sales_stage Not In('Closed Won','Closed Lost') and assigned_user_id ='".$_POST["users"][$i]."' and deleted='0'".$oppWhere;
								$result9=$db->query($query9);
								$row9 =$db->fetchByAssoc($result9);
								if($row9['amount'] == null || $row9['amount'] == ''){
									$row9['amount']=0;
								}
								$query10="select sum(opportunities.amount) as amount  from opportunities  where opportunities.sales_stage = 'Closed Won' and opportunities.assigned_user_id ='".$_POST["users"][$i]."' and opportunities.deleted = '0'".$accountWhereTran;
								$result10=$db->query($query10);
								$row10 =$db->fetchByAssoc($result10);
								if($row10['amount'] == null || $row10['amount'] == ''){
									$row10['amount']=0;
								}
								$query11="select sum(amount) as amount from opportunities where sales_stage='Closed Lost' and assigned_user_id ='".$_POST["users"][$i]."' and deleted='0'".$oppWhere;
								$result11=$db->query($query11);
								$row11 =$db->fetchByAssoc($result11);
								if($row11['amount'] == null || $row11['amount'] == ''){
									$row11['amount']=0;
								}
								$query13="select sum(opportunities.amount) as amount from opportunities inner join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id
								inner join accounts_cstm on accounts_opportunities.account_id = accounts_cstm.id_c where opportunities.sales_stage NOT IN('Closed Won','Closed Lost') and opportunities.assigned_user_id ='".$_POST["users"][$i]."'
								and opportunities.deleted='0' and accounts_cstm.lead_account_status_c='Converted'".$oppWhere;
								$result13=$db->query($query13);
								$row13 =$db->fetchByAssoc($result13);
								if($row13['amount'] == null || $row13['amount'] == ''){
									$row13['amount']=0;
								}
								$query14="select sum(opportunity_amount) as amount from ($leadQuery) as leads where status ='Dead' and deleted='0' and assigned_user_id ='".$_POST["users"][$i]."'".$leadWhere;
								$result14=$db->query($query14);
								$row14 =$db->fetchByAssoc($result14);
								if($row14['amount']==null || $row14['amount']==''){
									$row14['amount']=0;
								}
								$query15="select count(id) as ca from ($callQuery) as  calls where deleted='0' and status='Held' and assigned_user_id ='".$_POST["users"][$i]."'".$callWhere;
								$result15=$db->query($query15);
								$row15 =$db->fetchByAssoc($result15);
								if($row15['ca']==null || $row15['ca']==''){
									$row15['ca']=0;
								}
								$query16="select count(id) as meeting from ($meetingQuery) as meetings  where deleted='0' and status='Held' and assigned_user_id ='".$_POST["users"][$i]."'".$meetingWhere;
								$result16=$db->query($query16);
								$row16 =$db->fetchByAssoc($result16);
								if($row16['meeting']==null || $row16['meeting']==''){
									$row16['meeting']=0;
								}
								$query17="select count(id) as email from emails where deleted='0' and assigned_user_id ='".$_POST["users"][$i]."'".$emailWhere;
								$result17=$db->query($query17);
								$row17 =$db->fetchByAssoc($result17);
								if($row17['email']==null || $row17['email']==''){
									$row17['email']=0;
								}
								$query18="select count(opportunities.id) as customer from opportunities inner join accounts_opportunities on opportunities.id=accounts_opportunities.opportunity_id inner join ($accountQuery) as accounts 
								on accounts_opportunities.account_id=accounts.id where opportunities.sales_stage not in('Closed Won','Closed Lost') and accounts.assigned_user_id ='".$_POST["users"][$i]."' and accounts.deleted='0'".$oppWhere;
								$result18=$db->query($query18);
								$row18 =$db->fetchByAssoc($result18);
								if($row18['customer']==null || $row18['customer']==''){
									$row18['customer']=0;
								}
									$query21 = "select distinct(accounts.assigned_user_id) as assigned from opportunities inner join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id inner join 
									accounts on accounts.id = accounts_opportunities.account_id where opportunities.sales_stage = 'Closed Won' and opportunities.assigned_user_id ='".$_POST["users"][$i]."' and opportunities.deleted = '0'  and accounts.deleted = '0'".$accountWhereTran;
									$result21 = $db->query($query21);
									$whereAss = '';
									while($row21 = $db->fetchByAssoc($result21)){
										if(!empty($row21["assigned"])){
											$whereAss .= "'".$row21["assigned"]."',";
											
										}
									}
									$query19 = "select accounts.id as id  from ($accountQuery) as accounts left join accounts_cstm on accounts_cstm.id_c = accounts.id where accounts.deleted = '0'  and accounts.assigned_user_id ='".$_POST["users"][$i]."' and accounts_cstm.lead_account_status_c = 'Transaction'".$accountTransaction;
									$result19=$db->query($query19);
									$customerr = 0;
									$amountt = 0;
									while($row19 =$db->fetchByAssoc($result19)){
										 
										$query20="select opportunities.amount as amount  from opportunities inner join accounts_opportunities on opportunities.id = accounts_opportunities.opportunity_id inner join 
										accounts on accounts.id = accounts_opportunities.account_id where opportunities.sales_stage = 'Closed Won' and accounts.id ='".$row19['id']."' and opportunities.deleted = '0' ";
									$result20=$db->query($query20);
									$row20 =$db->fetchByAssoc($result20);
										if($row20['amount'] != null && $row20['amount'] != ''){
											$amountt+=$row20['amount'];

										}
									}
									$query23="select count(accounts.id) as customer  from accounts_cstm right join accounts on accounts_cstm.id_c = accounts.id where accounts.deleted = '0'  and accounts.assigned_user_id ='".$_POST["users"][$i]."' and accounts_cstm.lead_account_status_c = 'Transaction'".$accountTransaction;
									$result23=$db->query($query23);
									$row23 =$db->fetchByAssoc($result23);
									if($row23['customer'] != null && $row23['customer'] != ''){
										$customerr = $row23['customer'];
									}
									
									if($employee == true){
										if($_POST["users"][$i] == $_SESSION['authenticated_user_id']){	
											$user=array(
													"user"=>$row['name'],
													"New"=>$row4["customer"],
													"userAmountNew"=>$row7['amount'],
													"InProcess"=>$row1["customer"],
													"userAmountInprocess"=>$row8['amount'],
													"Converted"=>$row2["customer"],
													"userAmountConverted"=>$row13['amount'],
													"Dead"=>$row12["customer"],
													"userAmountDead"=>$row14["amount"],
													"Transaction"=>$customerr,
													"userAmountTransaction"=>$amountt,
													"ClosedWon"=>$row5["customer"],
													"userAmountClosedWon"=>$row10['amount'],
													"Other"=>$row18["customer"],
													"userAmountOther"=>$row9['amount'],
													"ClosedLost"=>$row6["customer"],
													"userAmountClosedLost"=>$row11['amount'],
													"Call"=>$row15["ca"],
													"Meeting"=>$row16["meeting"],
													"Email"=>$row17["email"]
												);
											if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
												$group[$row["Se"]]=array();
												$group[$row["Se"]][]=$user;
											}else{
												$group[$row["Se"]][]=$user;
											}
											$New+=$row4["customer"];
											$InProcess+=$row1["customer"];
											$Dead+=$row12["customer"];
											$Converted+=$row2["customer"];
											$Transaction+=$customerr;
											$CloseWon+=$row5["customer"];
											$CloseLost+=$row6["customer"];
											$Other+=$row18["customer"];
											$amountNew+=$row7["amount"];
											$amountInProcess+=$row8["amount"];
											$amountDead+=$row14["amount"];
											$amountConverted+=$row13["amount"];
											$amountTran+=$amountt;
											$amountWon+=$row10["amount"];
											$amountLost+=$row11["amount"];
											$amountCall+=$row15["ca"];
											$amountMeeting+=$row16["meeting"];
											$amountEmail+=$row17["email"];
											$amountOther+=$row9["amount"];
										}
									}else{
										$user=array(
													"user"=>$row['name'],
													"New"=>$row4["customer"],
													"userAmountNew"=>$row7['amount'],
													"InProcess"=>$row1["customer"],
													"userAmountInprocess"=>$row8['amount'],
													"Converted"=>$row2["customer"],
													"userAmountConverted"=>$row13['amount'],
													"Dead"=>$row12["customer"],
													"userAmountDead"=>$row14["amount"],
													"Transaction"=>$customerr,
													"userAmountTransaction"=>$amountt,
													"ClosedWon"=>$row5["customer"],
													"userAmountClosedWon"=>$row10['amount'],
													"Other"=>$row18["customer"],
													"userAmountOther"=>$row9['amount'],
													"ClosedLost"=>$row6["customer"],
													"userAmountClosedLost"=>$row11['amount'],
													"Call"=>$row15["ca"],
													"Meeting"=>$row16["meeting"],
													"Email"=>$row17["email"]
												);
											if($group[$row["Se"]]=='' || $group[$row["Se"]] == null){
												$group[$row["Se"]]=array();
												$group[$row["Se"]][]=$user;
											}else{
												$group[$row["Se"]][]=$user;
											}
											$New+=$row4["customer"];
											$InProcess+=$row1["customer"];
											$Dead+=$row12["customer"];
											$Converted+=$row2["customer"];
											$Transaction+=$customerr;
											$CloseWon+=$row5["customer"];
											$CloseLost+=$row6["customer"];
											$Other+=$row18["customer"];
											$amountNew+=$row7["amount"];
											$amountInProcess+=$row8["amount"];
											$amountDead+=$row14["amount"];
											$amountConverted+=$row13["amount"];
											$amountTran+=$amountt;
											$amountWon+=$row10["amount"];
											$amountLost+=$row11["amount"];
											$amountCall+=$row15["ca"];
											$amountMeeting+=$row16["meeting"];
											$amountEmail+=$row17["email"];
											$amountOther+=$row9["amount"];
									}
								}
							}
						}
					}
				
				}
				if(!empty($prov[$k]) && !empty($p)){
				$province[]=array(
					"province"=>$prov[$k],
					"data"=>$group,
					"New"=>$New,
					"InProcess"=>$InProcess,
					"Dead"=>$Dead,
					"Converted"=>$Converted,
					"Transaction"=>$Transaction,
					"CloseWon"=>$CloseWon,
					"Other"=>$Other,
					"CloseLost"=>$CloseLost,
					"amountNew"=>$amountNew,
					"amountInProcess"=>$amountInProcess,
					"amountDead"=>$amountDead,
					"amountConverted"=>$amountConverted,
					"amountTransaction"=>$amountTran,
					"amountWon"=>$amountWon,
					"amountLost"=>$amountLost,
					"amountCall"=>$amountCall,
					"amountMeeting"=>$amountMeeting,
					"amountEmail"=>$amountEmail,
					"amountOther"=>$amountOther
				);
				}
				$group=array();
			}
			
			echo json_encode ($province);
			
		}
		
	}		
		
		

