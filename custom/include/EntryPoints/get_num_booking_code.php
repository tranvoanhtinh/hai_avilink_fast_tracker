<?php
global $db, $sugar_config;

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    $account_id = $_POST['account_id'];
    $type_booking = $_POST['type_booking'];
    $date_str = $_POST['date'].':00';
    if(isset($account_id) && isset($type_booking) && isset($date_str)) {
        $dateTime = DateTime::createFromFormat($sugar_config['default_date_format'] . ' H:i:s', $date_str);


        if ($dateTime !== false) {
            $formatted_date = $dateTime->format('Y-m-d');
            $get_account_code = "SELECT name , account_code FROM accounts WHERE deleted = 0 AND id = '".$account_id."'";
            $result0 = $db->query($get_account_code);
            if($row0 = $result0->fetch_assoc()) {
                $account_code = $row0['account_code'];
               $get_autonum = "
                    SELECT b.* 
                    FROM aos_bookings b
                    JOIN accounts_aos_bookings_1_c ab 
                        ON ab.accounts_aos_bookings_1aos_bookings_idb = b.id
                    WHERE DATE(b.date_of_service) = '".$formatted_date."'
                      AND ab.accounts_aos_bookings_1accounts_ida = '".$account_id."'
                      AND b.deleted = 0
                      AND b.type_booking = '".$type_booking."'
                ";

                $result = $db->query($get_autonum);
                $largest_number = null;


                
                // Duyệt qua kết quả để tìm số lớn nhất
                while ($row = $db->fetchByAssoc($result)) {
                    $name = $row['name'];
                    $parts = explode('.', $name);
                    $number = end($parts);
                    if (ctype_digit($number)) {
                        if ($largest_number === null || intval($number) > intval($largest_number)) {
                            $largest_number = $number;
                        }
                    }
                }
                $new_number = sprintf('%02d', intval($largest_number) + 1);
                echo json_encode([
                    'account_code' => $account_code,
                    'stt' => $new_number,
                ], JSON_UNESCAPED_UNICODE);
                
            } else {
                echo -1; // Không tìm thấy mã tài khoản
            }
        } else {
            echo -2; // Lỗi chuyển đổi định dạng ngày tháng
        }
        
    } else {
        echo -3; // Thiếu thông tin cần thiết từ form
    }
}
?>
