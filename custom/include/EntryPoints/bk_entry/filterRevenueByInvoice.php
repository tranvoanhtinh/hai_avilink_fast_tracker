<?php
try{
global $current_user;
global $db;
$current_user->id = $_SESSION['authenticated_user_id'];

if((!empty($_POST['period'])) && ($_POST['period'] != 'default')){
	if(!empty($_POST['date_from']) && !empty($_POST['date_to'])){
				if($_POST['date_from'] != $_POST['date_to']){
					$from = date('Y-m-d', strtotime(str_replace("/","-",$_POST['date_from'])));	
					$to = date('Y-m-d', strtotime(str_replace("/","-",$_POST['date_to'])));	
				}else{
					$from1=date('Y-m-d', strtotime(str_replace("/","-",$_POST['date_from'])));	
				}
	}
	//if($time){
	//	$from=date('Y-m-d', strtotime($time['from']));
	//	$to=date('Y-m-d', strtotime($time['to']));
	//}
	if($from1){
		$invoiceWhere=" DATE_FORMAT(aos_invoices.invoice_date,'%Y-%m-%d') ='".$from1."'";
		$billingWhere=" And DATE_FORMAT(bill_billing.billingdate,'%Y-%m-%d') ='".$from1."'";
	}else{
		$invoiceWhere=" DATE_FORMAT(aos_invoices.invoice_date,'%Y-%m-%d') >='".$from."' And DATE_FORMAT(aos_invoices.invoice_date,'%Y-%m-%d') <='".$to."'";
		$billingWhere=" And DATE_FORMAT(bill_billing.billingdate,'%Y-%m-%d') >='".$from."' And DATE_FORMAT(bill_billing.billingdate,'%Y-%m-%d') <='".$to."'";
	}
}else{
	$invoiceWhere="";
	$accountWhere="";
	$billingWhere="";
}	
	$whereProvince = '';
	$whereGroup = '';
	$whereTitle = '';
	$whereId = '';
	$user='';
	$whereAll = '';
	$queryArray = array();
	if($_POST["province"] != "All"){
		$que = "select id from users inner join users_cstm on users.id = users_cstm.id_c where users_cstm.provincecode_c = '".$_POST["province"]."' and users.deleted = '0'"; 
		$res = $db->query($que);
		while($row = $db->fetchByAssoc($res)){
			if(!empty($row["id"])){
				$whereProvince .= "'".$row["id"]."',";
			}
		}
		if($whereProvince != ''){
			$whereProvince = trim($whereProvince,",");
			$whereProvince = "IN (".$whereProvince.")";
			$queryArray[] = $whereProvince;
		}
	}else{
		$que = "select id from users inner join users_cstm on users.id = users_cstm.id_c where users.deleted = '0'"; 
		$res = $db->query($que);
		while($row = $db->fetchByAssoc($res)){
			if(!empty($row["id"])){
				$whereProvince .= "'".$row["id"]."',";
			}
		}
		if($whereProvince != ''){
			$whereProvince = trim($whereProvince,",");
			$whereProvince = "IN (".$whereProvince.")";
			$queryArray[] = $whereProvince;
		}
		
	}
	if($_POST["groupUsers"]){
		if(array_search('All',$_POST["groupUsers"]) == false && count($_POST["groupUsers"])>0){
			for($i = 0;$i<count($_POST["groupUsers"]);$i++){
				$que = "select id from users inner join users_cstm on users.id = users_cstm.id_c where users.department = '".$_POST["groupUsers"][$i]."' and users.deleted = '0'"; 
				$res = $db->query($que);
				while($row = $db->fetchByAssoc($res)){
					if(!empty($row["id"])){
						$whereGroup .= "'".$row["id"]."',";
					}
				}
			}
			if($whereGroup != ''){
				$whereGroup = trim($whereGroup,",");
				$whereGroup = "IN (".$whereGroup.")";
				$queryArray[] = $whereGroup;
			}
		}else{
			for($i = 0;$i<count($_POST["groupUsers"]);$i++){
				$que = "select id from users inner join users_cstm on users.id = users_cstm.id_c where users.deleted = '0'";
				$res = $db->query($que);
				while($row = $db->fetchByAssoc($res)){
					if(!empty($row["id"])){
						$whereGroup .= "'".$row["id"]."',";
					}
				}
			}
			if($whereGroup != ''){
				$whereGroup = trim($whereGroup,",");
				$whereGroup = "IN (".$whereGroup.")";
				$queryArray[] = $whereGroup;
			}
		}
	}
	if($_POST["titleUsers"]){
		for($i = 0;$i<count($_POST["titleUsers"]);$i++){
			$que = "select id from users inner join users_cstm on users.id = users_cstm.id_c where users.title = '".$_POST["titleUsers"][$i]."' and users.deleted = '0'"; 
			$res = $db->query($que);
			while($row = $db->fetchByAssoc($res)){
				if(!empty($row["id"])){
					$whereTitle .= "'".$row["id"]."',";
				}
			}
		}
		if($whereTitle != ''){
			$whereTitle = trim($whereTitle,",");
			$whereTitle = "IN (".$whereTitle.")";
			$queryArray[] = $whereTitle;
		
		}
	}
	
	if($_POST["users"]){
		for($i = 0;$i<count($_POST["users"]);$i++){
			$que = "select id from users inner join users_cstm on users.id = users_cstm.id_c where users.id = '".$_POST["users"][$i]."' and users.deleted = '0'"; 
			$res = $db->query($que);
			while($row = $db->fetchByAssoc($res)){
				if(!empty($row["id"])){
					$whereId  .= "'".$row["id"]."',";
				}
			}
		}
		if($whereId != ''){
			$whereId  = trim($whereId,",");
			$whereId = "IN (".$whereId.")";
			$queryArray[] = $whereId;
		}
	}
	$whereAllOpp = '';
	$whereAllInv = '';
	$whereAllBill = '';
	for($i=0;$i<count($queryArray);$i++){
		if($i < count($queryArray) -1){
			$whereAllOpp .= "opportunities.assigned_user_id ".$queryArray[$i]." and ";
			$whereAllInv .= "aos_invoices.assigned_user_id ".$queryArray[$i]." and ";
			$whereAllBill .= "bill_billing.assigned_user_id ".$queryArray[$i]." and ";
		}
		else{
			$whereAllOpp .= "opportunities.assigned_user_id ".$queryArray[$i];
			$whereAllInv .= "aos_invoices.assigned_user_id ".$queryArray[$i];
			$whereAllBill .= "bill_billing.assigned_user_id ".$queryArray[$i];
		}
	}
	if($invoiceWhere != ''){
		if($whereAllInv != ''){
			$whereAllInv = $whereAllInv." and ".$invoiceWhere;
		}else{
			$whereAllInv .= $invoiceWhere;
		}
	}
		$invoices = BeanFactory::newBean('AOS_Invoices');
		$queryNew = $invoices->create_new_list_query("",$whereAllInv,array());
		
		$q3 = "SELECT contacts.last_name as contactName ,accounts.*,SUM(aos_invoices.total_amount) AS invTotalAmount,aos_invoices.name as invName,aos_invoices.invoice_date,aos_invoices.id as invId 
		FROM ($queryNew) as aos_invoices 
		left JOIN accounts ON aos_invoices.billing_account_id = accounts.id 
		AND accounts.deleted = 0 left join contacts on aos_invoices.billing_contact_id  = contacts.id and contacts.deleted = '0' WHERE aos_invoices.deleted = '0' group by aos_invoices.id order by aos_invoices.invoice_date DESC ";
		$r3 = $invoices->db->query($q3);
		$allOppTotal = 0;
		$allInvTotal = 0;
		$allReceipts = 0;
		$allPayment = 0;
		$allProfitByOpp = 0;
		$allDebtByOpp = 0;	
		$allProfitByInvoices = 0;
		$allDebtByInvoices = 0;
		while($invoicesAmount = $invoices->db->fetchByAssoc($r3)){
		$q2 = "select email_addresses.email_address from email_addresses inner join email_addr_bean_rel on email_addresses.id = email_addr_bean_rel.email_address_id and email_addresses.deleted = '0' inner join accounts on accounts.id =  email_addr_bean_rel.bean_id and accounts.id = '".$invoicesAmount["id"]."' and accounts.deleted = '0' where email_addr_bean_rel.primary_address = '1'";
		$r2 = $db->query($q2);
		$row2 = $db->fetchByAssoc($r2);
		/*
		left join (select bill_billing.type,bill_billing.billing_status,SUM(bill_billing.billingamount) AS billtotalAmount, bill_billing_accounts_c.bill_billing_accountsaccounts_ida from bill_billing_accounts_c left join bill_billing on  bill_billing_accounts_c.bill_billing_accountsbill_billing_idb = bill_billing.id and bill_billing.deleted = '0' where bill_billing_accounts_c.deleted = '0'".$billingWhere." group by bill_billing.billing_status,bill_billing.type,bill_billing_accounts_c.bill_billing_accountsaccounts_ida) as bill_billing  
		*/
		$billQuery = "select bill_billing.type,bill_billing.billing_status,SUM(bill_billing.billingamount) AS billtotalAmount from bill_billing left join bill_billing_aos_invoices_c on  bill_billing_aos_invoices_c.bill_billing_aos_invoicesbill_billing_idb = bill_billing.id and bill_billing_aos_invoices_c.deleted = '0' where bill_billing.deleted = '0' and bill_billing_aos_invoices_c.bill_billing_aos_invoicesaos_invoices_ida = '".$invoicesAmount["invId"]."' group by bill_billing.billing_status,bill_billing.type";
		$billRes = $db->query($billQuery);
		$payment = 0;
		$receipts = 0;
		while($billRow = $db->fetchByAssoc($billRes)){
			if($billRow["billing_status"] == "completed" && $billRow["type"] == "payment"){
				$payment = $billRow["billtotalAmount"];
			
			}
			if($billRow["billing_status"] == "completed" && $billRow["type"] == "receipts"){
				$receipts = $billRow["billtotalAmount"];
			
			}
		}			
		
		
		//Lợi nhuận 
		$profitByInvoices = ($invoicesAmount["invTotalAmount"] > 0)?$invoicesAmount["invTotalAmount"] - $payment:0;	
		//Công nợ 
		$debtByInvoices = ($invoicesAmount["invTotalAmount"] > 0)?$invoicesAmount["invTotalAmount"] - $receipts:0 ;
		
		
		$allInvTotal += $invoicesAmount["invTotalAmount"]?$invoicesAmount["invTotalAmount"]:0;
		$allReceipts += $receipts?$receipts:0;
		$allPayment += $payment?$payment:0;
		$allProfitByInvoices += $profitByInvoices?$profitByInvoices:0;
		$allDebtByInvoices += $debtByInvoices?$debtByInvoices:0;
		$arr[] = array(
			"name"=>$invoicesAmount["name"],			
			"phone"=>$invoicesAmount["phone_alternate"],
			"email"=>$row2["email_address"],
			"address"=>$invoicesAmount["billing_address_street"],
			"invoice"=>$invoicesAmount["invTotalAmount"],
			"invoiceName"=>$invoicesAmount["invName"],
			"contact"=>$invoicesAmount["contactName"],
			"name"=>$invoicesAmount["name"],
			"billingReceipts"=>$receipts,
			"billingPayment"=>$payment,
			"invoice_date"=>date('d-m-Y', strtotime($invoicesAmount["invoice_date"])),
			"profitByInvoices"=>$profitByInvoices,
			"debtByInvoices"=>$debtByInvoices
		);
	}
	
	$arr[] = array(
			"allInvTotal" => $allInvTotal,
			"allReceipts" => $allReceipts,
			"allPayment" => $allPayment,
			"allProfitByInvoices" => $allProfitByInvoices,
			"allDebtByInvoices" => $allDebtByInvoices
			);
			
	echo json_encode($arr);
}catch(Exception $e) {
	echo json_encode($e->getMessage());
}
		