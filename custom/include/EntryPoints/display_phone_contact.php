<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
global $db;
if ($_SERVER["REQUEST_METHOD"] === "POST") {
    // Kiểm tra xem biến $_POST['type'] và $_POST['record'] có tồn tại không
    if(isset($_POST['type']) && isset($_POST['record'])) {

        if($_POST['type'] == 'id') {
            $sql = "SELECT * FROM contacts WHERE id = '".$_POST['record']."' AND deleted = 0";
        }
        if($_POST['type'] == 'phone') {
            $sql = "SELECT * FROM contacts WHERE phone_mobile = '".$_POST['record']."' AND deleted = 0";
        }

        // Thực hiện truy vấn
        $result = $db->query($sql);

        // Kiểm tra xem truy vấn có thành công không
        if($result) {
            // Lấy dòng dữ liệu đầu tiên từ kết quả truy vấn
            $row = $result->fetch_assoc();

            // Tạo mảng thông tin
            $info = array(
                'id' => $row['id'],
                'phone' => $row['phone_mobile'],
                'billing_contact' => $row['last_name'],
            );

            // Trả về dữ liệu dưới dạng JSON
            echo json_encode($info);
        } 
    }
}