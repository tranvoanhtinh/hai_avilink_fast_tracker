<?php
global $db;
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $module_dir = $_POST['module_dir'];
    $module = strtolower($_POST['module_dir']);

    $name = $_POST['voucherno'];

    $data = array(); // Khai báo mảng $data trước khi sử dụng

    // Sử dụng prepared statement để tránh các vấn đề bảo mật
    $sql = "SELECT id FROM $module WHERE name = '".$name."'";
      
    $result = $db->query($sql);

    if ($result->num_rows > 0) {
        // Fetch dòng đầu tiên từ kết quả và lấy giá trị 'id'
        $row = $result->fetch_assoc();
        $id = $row["id"];

        // Gán giá trị vào mảng $data
        $data['id'] = $id;
        $data['module_dir'] = $module_dir;
    } elseif($module_dir == 'AOS_Warehouse_Transfer'){
        $sql = "SELECT id FROM aos_inventoryinput WHERE name = '".$name."'";
        $result = $db->query($sql);
        if ($result->num_rows > 0) {
        // Fetch dòng đầu tiên từ kết quả và lấy giá trị 'id'
        $row = $result->fetch_assoc();
        $data['id'] = $row['id'];
        $data['module_dir'] = 'AOS_Inventoryinput';
    }else{
        $sql = "SELECT id FROM aos_inventoryoutput WHERE name = '".$name."'";
        $result = $db->query($sql);
        $row = $result->fetch_assoc();
        $data['id'] = $row['id'];
        $data['module_dir'] = 'AOS_Inventoryoutput';
    }
    } else{
        $module_dir = 'AOS_Invoices';

        $sql = "SELECT id FROM aos_invoices WHERE name = '".$name."'";
      
        $result = $db->query($sql);

        if ($result->num_rows > 0) {
            // Fetch dòng đầu tiên từ kết quả và lấy giá trị 'id'
            $row = $result->fetch_assoc();
            // Gán giá trị vào mảng $data
            $data['id'] = $row['id'];
            $data['module_dir'] = $module_dir;
        }
    }

    // Trả về dữ liệu dưới dạng JSON
    echo json_encode($data);
}
?>
