<?php
global $current_language;
$lead_mod_strings = return_module_language($current_language, "Leads");
echo json_encode($lead_mod_strings);