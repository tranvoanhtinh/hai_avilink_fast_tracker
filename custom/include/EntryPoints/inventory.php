<?php

global $db;

// Câu truy vấn để kiểm tra các tên trùng lặp
$query = "
    SELECT name, COUNT(*) as count
    FROM aos_bookings
    where deleted = '0'
    GROUP BY name

    HAVING COUNT(*) > 1
";

// Thực hiện câu truy vấn
$result = $db->query($query);

if ($result) {
    // Hiển thị kết quả
    echo "<table border='1'>";
    echo "<tr><th>Name</th><th>Count</th></tr>";
    
    while ($row = $result->fetch_assoc()) {
        echo "<tr>";
        echo "<td>" . htmlspecialchars($row['name']) . "</td>";
        echo "<td>" . htmlspecialchars($row['count']) . "</td>";
        echo "</tr>";
    }
    
    echo "</table>";
} else {
    // Hiển thị thông báo lỗi nếu có lỗi xảy ra
    echo "Error: " . $db->error;
}

?>
