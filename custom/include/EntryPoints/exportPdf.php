<?php
global $db,$locale;
$user = $_SESSION['authenticated_user_id'];
$product = array();
$group = array();
$que = "select tttt_worldfonepbx.*,tttt_worldfonepbx_cstm.* from tttt_worldfonepbx inner join tttt_worldfonepbx_users_1_c on tttt_worldfonepbx_users_1_c.tttt_worldfonepbx_users_1tttt_worldfonepbx_ida = tttt_worldfonepbx.id and tttt_worldfonepbx_users_1_c.deleted = '0' left join tttt_worldfonepbx_cstm on tttt_worldfonepbx.id = tttt_worldfonepbx_cstm.id_c where tttt_worldfonepbx_users_1_c.tttt_worldfonepbx_users_1users_idb = '".$user."' and tttt_worldfonepbx_cstm.status_c = '1' and tttt_worldfonepbx.type_sys_app = '6'";
$ress = $db->query($que);
$roww = $db->fetchByAssoc($ress);
$q = "select aos_products_quotes.*,aos_invoices.date_entered,aos_invoices.remaining_money, aos_invoices.advance_payment ,aos_invoices.invoice_date,aos_invoices.total_discount_amount,aos_invoices.receive_amount,aos_invoices.return_amount,aos_invoices.invoiceno,aos_invoices.number,aos_invoices.total_amt,aos_invoices.discount_amount,aos_invoices.total_amount,users.user_name,accounts.name as accName,accounts.phone_alternate,accounts.billing_address_street,aos_line_item_groups.name as groupName from aos_invoices left join aos_products_quotes on aos_invoices.id = aos_products_quotes.parent_id  and aos_products_quotes.deleted = '0' left join aos_products on  aos_products_quotes.product_id = aos_products.id and aos_products.deleted = '0' left join accounts on aos_invoices.billing_account_id = accounts.id left join aos_line_item_groups on aos_invoices.id =  aos_line_item_groups.parent_id left join users on aos_invoices.modified_user_id = users.id where  aos_invoices.deleted = '0' and aos_invoices.id ='".$_GET["id"]."' group BY aos_products_quotes.id  Order by aos_line_item_groups.name ASC";
$res = $db->query($q);
while($row = $db->fetchByAssoc($res)){
	$invoices = array(
		"modBy"=>$row["user_name"], 
		//"date"=>date_format(date_create($row["date_entered"]),"d/m/Y H:i:s"),
		"date"=>date_format(date_create($row["invoice_date"]),"d/m/Y"),
		"number"=>$row["number"],
		"account"=>$row["accName"],
		"phone"=>$row["phone_alternate"],
		"amt"=>$row["total_amt"],
		"discount"=>$row["discount_amount"],
		"total"=>$row["total_amount"],
		"address"=>$row["billing_address_street"],
		"receive_amount"=> $row["receive_amount"],
		"return_amount"=>$row["return_amount"],
		"invoiceno" =>$row["invoiceno"],
		"name" =>$row["number"],
		"advance_payment"=> $row['advance_payment'],
		"remaining_money"=> $row['remaining_money'],
		"discount2"=> $row['total_discount_amount'],
		"sig"=>$locale->getPrecision()
		
	);
	if($roww){
		if(!empty($roww["companylogo_c"])){
			$a="";
			for($i = 0; $i<strlen($roww["companylogo_c"]);$i++){
				if($roww["companylogo_c"][$i] != "."){
					//if($roww["companylogo_c"][$i] != "_"){
						$a.= $roww["companylogo_c"][$i];
					//}else{
					//	continue;
					//}
				}else{
					break;
				}
			}
			if (isset($_SERVER['HTTPS']) &&
				($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
				isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
				$_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https'){
					$protocol = 'https://';
				}
			else {
				$protocol = 'http://';
			}
		    $url = $protocol.$_SERVER['HTTP_HOST'].
			$a ="index.php?entryPoint=download&id=".$roww["id_c"]."_companylogo_c"."&type=tttt_WorldfonePBX";
			$b = "index.php?entryPoint=download&id=".$roww["id_c"]."_qrcode_c"."&type=tttt_WorldfonePBX";
		}
		$invoices["companyName"]=$roww["company_name"];
		$invoices["companyWeb"]=$roww["company_website"];
		$invoices["companyAddress"]=$roww["company_address"];
		$invoices["companyLoGo"]=$a;
		$invoices["companyPhone"]= $roww["phone_work"];
		$invoices["sologun"] = $roww["company_sologun"];
		$invoices["qrcode_c"] = $b;
		$invoices["bankinginfo"] = $roww["bankinginfo"];
		$invoices["bankno"] = $roww["bankno"];
		$invoices["bankname"] = $roww["bankname"];
		$invoices["bankaccount"] = $roww["bankaccount"];
	}
	if($group[$row["groupName"]] == null){
		$group[$row["groupName"]] = array();
		$group[$row["groupName"]][] = array(
										"name"=>$row["name"],
										"quantity"=>$row["product_qty"],
										"listPrice"=>$row["product_list_price"],
										"discount"=>$row["product_discount"],
										"totalPrice"=>$row["product_total_price"],
									    "product_unit_price"=>  $row['product_unit_price'],
						
				
										
									);
	}else{
		$group[$row["groupName"]][] = array(
										"name"=>$row["name"],
										"quantity"=>$row["product_qty"],
										"listPrice"=>$row["product_list_price"],
										"discount"=>$row["product_discount"],
										"totalPrice"=>$row["product_total_price"],
										"product_unit_price"=>  $row['product_unit_price'],
					
									);
	}
}
$invoices["group"] = $group;
echo json_encode($invoices);