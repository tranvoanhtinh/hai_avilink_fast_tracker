
<?php
function getDateRangeToSearchForAmount($period_list_selected = 'this_month')
{
        if(empty($period_list_selected)) $period_list_selected = 'Tháng này';
        switch ($period_list_selected) {
            case 'Tuần này':
                $datetimeTo = new DateTime("this week sunday");
               // $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("this week monday");
              //  $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Tuần trước':
                $datetimeTo = new DateTime("last week sunday");
               // $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("last week monday");
               // $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Tháng này':
                $datetimeTo = new DateTime('last day of this month');
              //  $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("first day of this month");
              //  $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Tháng trước':
                $datetimeTo = new DateTime("last day of last month");
              //  $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("first day of last month");
              //  $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Năm này':
                $datetimeTo = new DateTime('this year last day of december');
             //   $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime('this year first day of january');
             //   $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Năm trước':
                $datetimeTo = new DateTime('last year last day of december');
              //  $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime('last year first day of january');
              //  $datetimeFrom->setTime(0, 0, 0);
                break;

        }
        return array(
            'from' => $datetimeFrom,
            'to' => $datetimeTo
        );
}
function getDateTimeAsDbForAmount($period = 'this_month', $date_from = '', $date_to = '')
{
        
		$timedate = new TimeDate();
		date_default_timezone_set('Asia/Ho_Chi_Minh');
        if(empty($date_from) && empty($date_to)) {
            $range = getDateRangeToSearchForAmount($period);

            $from_by_user = $range['from']->format($timedate::DB_DATETIME_FORMAT);
            $to_by_user = $range['to']->format($timedate::DB_DATETIME_FORMAT);

        }
        else {

            $fromObj = $timedate->fromUserDate($date_from);
            $fromObj->setTime(0, 0, 0);
            $from_by_user = $fromObj->format($timedate::DB_DATETIME_FORMAT);

            $toObj = $timedate->fromUserDate($date_to);
            $toObj->setTime(23,59,59);
            $to_by_user = $toObj->format($timedate::DB_DATETIME_FORMAT);

        }

        $from_display = $timedate->to_display_date_time($from_by_user, true, false);
        $from = $timedate->to_db($from_display);

        $to_display = $timedate->to_display_date_time($to_by_user, true, false);
        $to = $timedate->to_db($to_display);

        $datetime_as_db = array(
            'from' => $from,
            'to' => $to
        );

        return $datetime_as_db;
}

if($_SERVER['REQUEST_METHOD'] == 'POST'){
	try{
	global $db;
	$arr = array();
	$whr='';
	$sea='';
	global $current_user;
	$current_user->id = $_POST["user"];
	$rowRes = array();
	$rowRes2 = array();
	$rowRes3 = array();
	$myProduct = "";
	$start= ($_POST["page"] -1) * $_POST["recordPerPage"];
	if(isset($_POST['time']) && $_POST['time'] != "Tất cả" &&  $_POST['time'] != "" &&  $_POST['time'] != null){
		$time = getDateTimeAsDbForAmount($_POST['time']);
		$from=date('Y-m-d', strtotime($time['from']));
		$to=date('Y-m-d', strtotime($time['to']));
		if(date('m', strtotime($time['to'])) == "02" && $_POST['time'] == "Tháng này"){
			$to = date('Y', strtotime($time['to']))."-02-29";
		}
		$wh = " aos_products.date_entered >= '".$from."' and aos_products.date_entered <= '".$to."'";
	}else{
		$wh= '';	
	}
	if(isset($_POST['search'])){
			if($_POST['search'] != ""){
				if($_POST['type'] == "1"){
					$sea .= " and (aos_products.name like '%".$_POST['search']."%' OR aos_products.part_number like '%".$_POST['search']."%')";
				}else{
					$sea .= " and (aos_products.aos_product_category_id like '%".$_POST['search']."%' )";
				}
			}
	}
	if($_POST['myProduct'] == "1"){
		$myProduct = "aos_products.assigned_user_id = '".$_POST["user"]."'";
	}
	
	$where = $wh;
	if(!empty($where)){
		$myProduct = !empty($myProduct)?" and ".$myProduct:'';
		$where .= $myProduct;
	}else{
		$where .= $myProduct;
	}
	
	
	$product = BeanFactory::newBean('AOS_Products');
	//$lead->load_relationship('user_name');
	$query = $product->create_new_list_query("aos_products.date_entered DESC",$where,array());
	
	$query2 = "select aos_products.*,aos_products_cstm.*,documents.document_revision_id,document_revisions.filename,document_revisions.file_ext,document_revisions.file_mime_type,document_revisions.id as docId,documents_cstm.documenttype_c from  aos_products left join aos_products_cstm on aos_products.id = aos_products_cstm.id_c left join aos_product_categories on aos_products.aos_product_category_id = aos_product_categories.id left join aos_products_documents_1_c on aos_products.id =   aos_products_documents_1_c.aos_products_documents_1aos_products_ida left join documents on aos_products_documents_1_c.aos_products_documents_1documents_idb = documents.id and documents.deleted = '0' left join document_revisions on documents.id = document_revisions.document_id and document_revisions.deleted='0' left join documents_cstm on (documents.id = documents_cstm.id_c or aos_products_cstm.document_id_c = documents_cstm.id_c) inner join ($query) as products on aos_products.id = products.id  where aos_products.deleted = '0' ".$sea." order by aos_products.date_entered DESC";
    $res = $product->db->query($query2);
	while($row = $product->db->fetchByAssoc($res)){
		$rowRes[] = $row;
	}
	// gom hình sản phẩm phụ vào 1 sản phẩm
	for($i = 0;$i<count($rowRes);$i++){
		if($rowRes[$i]["id"] != "" && $rowRes[$i]["id"] != null){
			$rowRes[$i]["totalDocId"] = array();
				if($rowRes[$i]["docId"] != null && $rowRes[$i]["docId"] != ""){
					$rowRes[$i]["totalDocId"][] = $rowRes[$i]["docId"];
				}
				for($j = $i+1;$j<count($rowRes);$j++){
					if($rowRes[$i]["id"] == $rowRes[$j]["id"]){
						if($rowRes[$j]["docId"] != null && $rowRes[$j]["docId"] != "" && $rowRes[$j]["docId"] != $rowRes[$j-1]["docId"]){
							$rowRes[$i]["totalDocId"][]=$rowRes[$j]["docId"];
						}
						$rowRes[$j]["id"] = "";
					}
					
				}
					
			$rowRes2[] = $rowRes[$i];
		}
	}
	
		echo json_encode($rowRes2);
	}
	catch(PDOException $e) {
		echo "Error: " . $e->getMessage();
	}
	
	//
	
	

}		