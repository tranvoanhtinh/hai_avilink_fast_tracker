<?php 
global $current_language;
global $db;
global $sugar_config;
$payroll_mod_strings = return_module_language($current_language, "rp_Profitsdebtbycustomer");
$array = array(
	"language" => $payroll_mod_strings,
	"digit"=>$sugar_config["default_currency_significant_digits_payroll"]
	
);
echo json_encode($array);