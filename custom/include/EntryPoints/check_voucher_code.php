<?php
global $db;
if ($_SERVER["REQUEST_METHOD"] === "POST") {
    // Kiểm tra xem biến $_POST['type'] và $_POST['record'] có tồn tại không
    if(isset($_POST['record'])&& isset($_POST['module_table']) && isset($_POST['col'])){
    	$record = $_POST['record'];
    	$module_table = $_POST['module_table'];
    	$col = $_POST['col'];

    	$recordy= str_replace(' ', '',$record);
		$sql =  "SELECT $col FROM $module_table WHERE deleted = 0";
    	$result = $db->query($sql);
    	while ($row = $db->fetchByAssoc($result)){
    		 $partNumber = str_replace(' ', '', $row[$col]);
    		 // Chuyển đổi cả $partNumber và $record về Unicode NFC trước khi so sánh
			if(mb_strtolower($partNumber) == mb_strtolower($recordy)){
			    $record = -1;
			}
    	}

    	echo $record;
    }
}
?>
