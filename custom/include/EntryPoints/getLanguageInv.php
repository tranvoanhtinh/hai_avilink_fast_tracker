<?php
global $current_language;
$inv_mod_strings = return_module_language($current_language, "AOS_Invoices");
echo json_encode($inv_mod_strings);