<?php
try{
global $current_user;
global $db;
$current_user->id = $_SESSION['authenticated_user_id'];

if((!empty($_POST['period'])) && ($_POST['period'] != 'default')){

	if(!empty($_POST['date_from']) && !empty($_POST['date_to'])){
				if($_POST['date_from'] != $_POST['date_to']){
					$from = date('Y-m-d', strtotime(str_replace("/","-",$_POST['date_from'])));	
					$to = date('Y-m-d', strtotime(str_replace("/","-",$_POST['date_to'])));	
				}else{
					$from1=date('Y-m-d', strtotime(str_replace("/","-",$_POST['date_from'])));	
				}
	}

	if($from1){
		$invoiceWhere=" And DATE_FORMAT(aos_invoices.invoice_date,'%Y-%m-%d') ='".$from1."'";
		$oppWhere=" DATE_FORMAT(opportunities.depositdate,'%Y-%m-%d') ='".$from1."'";
		$accountWhere=" DATE_FORMAT(accounts.date_entered,'%Y-%m-%d') = DATE_FORMAT('".$from1."','%Y-%m-%d')";
		$billingWhere=" And DATE_FORMAT(bill_billing.billingdate,'%Y-%m-%d') ='".$from1."'";
	}else{
		$invoiceWhere=" And DATE_FORMAT(aos_invoices.invoice_date,'%Y-%m-%d') >='".$from."' And DATE_FORMAT(aos_invoices.invoice_date,'%Y-%m-%d') <='".$to."'";
		$oppWhere=" DATE_FORMAT(opportunities.depositdate,'%Y-%m-%d') >='".$from."' And DATE_FORMAT(opportunities.depositdate,'%Y-%m-%d') <='".$to."'";
		$accountWhere=" DATE_FORMAT(accounts.date_entered,'%Y-%m-%d') >= DATE_FORMAT('".$from."' ,'%Y-%m-%d') And DATE_FORMAT(accounts.date_entered,'%Y-%m-%d') <= DATE_FORMAT('".$to."','%Y-%m-%d')";
		$billingWhere=" And DATE_FORMAT(bill_billing.billingdate,'%Y-%m-%d') >='".$from."' And DATE_FORMAT(bill_billing.billingdate,'%Y-%m-%d') <='".$to."'";
	}
}else{
	$invoiceWhere="";
	$accountWhere="";
	$oppWhere="";
	$billingWhere="";
}	
	$whereProvince = '';
	$whereGroup = '';
	$whereTitle = '';
	$whereId = '';
	$user='';
	$whereAll = '';
	$queryArray = array();
	if($_POST["province"] != "All"){
		$que = "select id from users inner join users_cstm on users.id = users_cstm.id_c where users_cstm.provincecode_c = '".$_POST["province"]."' and users.deleted = '0'"; 
		$res = $db->query($que);
		while($row = $db->fetchByAssoc($res)){
			if(!empty($row["id"])){
				$whereProvince .= "'".$row["id"]."',";
			}
		}
		if($whereProvince != ''){
			$whereProvince = trim($whereProvince,",");
			$whereProvince = "IN (".$whereProvince.")";
			$queryArray[] = $whereProvince;
		}
	}else{
		$que = "select id from users inner join users_cstm on users.id = users_cstm.id_c where users.deleted = '0'"; 
		$res = $db->query($que);
		while($row = $db->fetchByAssoc($res)){
			if(!empty($row["id"])){
				$whereProvince .= "'".$row["id"]."',";
			}
		}
		if($whereProvince != ''){
			$whereProvince = trim($whereProvince,",");
			$whereProvince = "IN (".$whereProvince.")";
			$queryArray[] = $whereProvince;
		}
		
	}
	if($_POST["groupUsers"]){
		if(array_search('All',$_POST["groupUsers"]) == false && count($_POST["groupUsers"])>0){
			for($i = 0;$i<count($_POST["groupUsers"]);$i++){
				$que = "select id from users inner join users_cstm on users.id = users_cstm.id_c where users.department = '".$_POST["groupUsers"][$i]."' and users.deleted = '0'"; 
				$res = $db->query($que);
				while($row = $db->fetchByAssoc($res)){
					if(!empty($row["id"])){
						$whereGroup .= "'".$row["id"]."',";
					}
				}
			}
			if($whereGroup != ''){
				$whereGroup = trim($whereGroup,",");
				$whereGroup = "IN (".$whereGroup.")";
				$queryArray[] = $whereGroup;
			}
		}else{
			for($i = 0;$i<count($_POST["groupUsers"]);$i++){
				$que = "select id from users inner join users_cstm on users.id = users_cstm.id_c where users.deleted = '0'";
				$res = $db->query($que);
				while($row = $db->fetchByAssoc($res)){
					if(!empty($row["id"])){
						$whereGroup .= "'".$row["id"]."',";
					}
				}
			}
			if($whereGroup != ''){
				$whereGroup = trim($whereGroup,",");
				$whereGroup = "IN (".$whereGroup.")";
				$queryArray[] = $whereGroup;
			}
		}
	}
	if($_POST["titleUsers"]){
		for($i = 0;$i<count($_POST["titleUsers"]);$i++){
			$que = "select id from users inner join users_cstm on users.id = users_cstm.id_c where users.title = '".$_POST["titleUsers"][$i]."' and users.deleted = '0'"; 
			$res = $db->query($que);
			while($row = $db->fetchByAssoc($res)){
				if(!empty($row["id"])){
					$whereTitle .= "'".$row["id"]."',";
				}
			}
		}
		if($whereTitle != ''){
			$whereTitle = trim($whereTitle,",");
			$whereTitle = "IN (".$whereTitle.")";
			$queryArray[] = $whereTitle;
		
		}
	}
	
	if($_POST["users"]){
		for($i = 0;$i<count($_POST["users"]);$i++){
			$que = "select id from users inner join users_cstm on users.id = users_cstm.id_c where users.id = '".$_POST["users"][$i]."' and users.deleted = '0'"; 
			$res = $db->query($que);
			while($row = $db->fetchByAssoc($res)){
				if(!empty($row["id"])){
					$whereId  .= "'".$row["id"]."',";
				}
			}
		}
		if($whereId != ''){
			$whereId  = trim($whereId,",");
			$whereId = "IN (".$whereId.")";
			$queryArray[] = $whereId;
		}
	}
	$whereAllAccount = '';
	$whereAllOpp = '';
	$whereAllInv = '';
	$whereAllBill = '';
	for($i=0;$i<count($queryArray);$i++){
		if($i < count($queryArray) -1){
			$whereAllOpp .= "opportunities.assigned_user_id ".$queryArray[$i]." and ";
			$whereAllAccount .= "accounts.assigned_user_id ".$queryArray[$i]." and ";
			$whereAllInv .= "aos_invoices.assigned_user_id ".$queryArray[$i]." and ";
			$whereAllBill .= "bill_billing.assigned_user_id ".$queryArray[$i]." and ";
		}
		else{
			$whereAllOpp .= "opportunities.assigned_user_id ".$queryArray[$i];
			$whereAllAccount .= "accounts.assigned_user_id ".$queryArray[$i];
			$whereAllInv .= "aos_invoices.assigned_user_id ".$queryArray[$i];
			$whereAllBill .= "bill_billing.assigned_user_id ".$queryArray[$i];
		}
	}
	

	$account = BeanFactory::newBean('Accounts');
		$queryNew = $account->create_new_list_query("accounts.date_entered DESC","",array());
		
		$q3 = "SELECT accounts.*,SUM(opportunities.amount) AS OppTotalAmount,invoices.invTotalAmount
		FROM ($queryNew) as accounts 
		left JOIN accounts_opportunities  ON accounts.id = accounts_opportunities.account_id 
		AND accounts_opportunities.deleted = 0 left join opportunities on accounts_opportunities.opportunity_id  = opportunities.id and opportunities.deleted = '0' AND opportunities.sales_stage = 'Closed Won'AND ".$oppWhere."
		left join (select accounts.id, SUM(aos_invoices.total_amount) AS invTotalAmount  FROM ($queryNew) as accounts left join aos_invoices on accounts.id = aos_invoices.billing_account_id and aos_invoices.deleted = '0'".$invoiceWhere." WHERE accounts.deleted = 0 and accounts.lead_account_status_c = 'Transaction' GROUP BY accounts.id) as invoices on accounts.id = invoices.id WHERE accounts.deleted = 0 and accounts.lead_account_status_c = 'Transaction' GROUP BY accounts.id";
		$r3 = $account->db->query($q3);
		$allOppTotal = 0;
		$allInvTotal = 0;
		$allReceipts = 0;
		$allPayment = 0;
		$allProfitByOpp = 0;
		$allDebtByOpp = 0;	
		$allProfitByInvoices = 0;
		$allDebtByInvoices = 0;
		
		while($closedWon = $account->db->fetchByAssoc($r3)){
		$q2 = "select email_addresses.email_address from email_addresses inner join email_addr_bean_rel on email_addresses.id = email_addr_bean_rel.email_address_id and email_addresses.deleted = '0' inner join accounts on accounts.id =  email_addr_bean_rel.bean_id and accounts.id = '".$closedWon["id"]."' and accounts.deleted = '0' where email_addr_bean_rel.primary_address = '1'";
		$r2 = $db->query($q2);
		$row2 = $db->fetchByAssoc($r2);
		/*
		left join (select bill_billing.type,bill_billing.billing_status,SUM(bill_billing.billingamount) AS billtotalAmount, bill_billing_accounts_c.bill_billing_accountsaccounts_ida from bill_billing_accounts_c left join bill_billing on  bill_billing_accounts_c.bill_billing_accountsbill_billing_idb = bill_billing.id and bill_billing.deleted = '0' where bill_billing_accounts_c.deleted = '0'".$billingWhere." group by bill_billing.billing_status,bill_billing.type,bill_billing_accounts_c.bill_billing_accountsaccounts_ida) as bill_billing  
		*/
		$billQuery = "select bill_billing.type,bill_billing.billing_status,SUM(bill_billing.billingamount) AS billtotalAmount, bill_billing_accounts_c.bill_billing_accountsaccounts_ida from bill_billing_accounts_c left join bill_billing on  bill_billing_accounts_c.bill_billing_accountsbill_billing_idb = bill_billing.id and bill_billing.deleted = '0' where bill_billing_accounts_c.deleted = '0'".$billingWhere." and bill_billing_accounts_c.bill_billing_accountsaccounts_ida = '".$closedWon["id"]."' group by bill_billing.billing_status,bill_billing.type";
		$billRes = $db->query($billQuery);
		$payment = 0;
		$receipts = 0;
		while($billRow = $db->fetchByAssoc($billRes)){
			if($billRow["billing_status"] == "completed" && $billRow["type"] == "payment"){
				$payment = $billRow["billtotalAmount"];
			
			}
			if($billRow["billing_status"] == "completed" && $billRow["type"] == "receipts"){
				$receipts = $billRow["billtotalAmount"];
			
			}
		}			
		
		//Lợi nhuận theo cơ hội
		$profitByOpp = ($closedWon["OppTotalAmount"]>0)?$closedWon["OppTotalAmount"]  - $payment:0;	
		//Công nợ theo cơ hội
		$debtByOpp = ($closedWon["OppTotalAmount"]>0)?$closedWon["OppTotalAmount"] - $receipts:0;
		//Lợi nhuận theo đơn hàng
		$profitByInvoices = ($closedWon["invTotalAmount"] > 0)?$closedWon["invTotalAmount"] - $payment:0;	
		//Công nợ theo đơn hàng
		$debtByInvoices = ($closedWon["invTotalAmount"] > 0)?$closedWon["invTotalAmount"] - $receipts:0 ;
		
		$allOppTotal += $closedWon["OppTotalAmount"]?$closedWon["OppTotalAmount"]:0;
		$allInvTotal += $closedWon["invTotalAmount"]?$closedWon["invTotalAmount"]:0;
		$allReceipts += $receipts?$receipts:0;
		$allPayment += $payment?$payment:0;
		$allProfitByOpp += $profitByOpp?$profitByOpp:0;
		$allDebtByOpp += $debtByOpp?$debtByOpp:0;	
		$allProfitByInvoices += $profitByInvoices?$profitByInvoices:0;
		$allDebtByInvoices += $debtByInvoices?$debtByInvoices:0;
		if ($closedWon["OppTotalAmount"] != 0 || $closedWon["invTotalAmount"] != 0){ 
			$arr[] = array(
				"name"=>$closedWon["name"],			
				"phone"=>$closedWon["phone_alternate"],
				"email"=>$row2["email_address"],
				"address"=>$closedWon["billing_address_street"],
				"closedWon"=>$closedWon["OppTotalAmount"],
				"invoices"=>$closedWon["invTotalAmount"],
				"billingReceipts"=>$receipts,
				"billingPayment"=>$payment,
				"profitByOpp"=>$profitByOpp,
				"debtByOpp"=>$debtByOpp,	
				"profitByInvoices"=>$profitByInvoices,
				"debtByInvoices"=>$debtByInvoices
			);
		}
	}
	
	$arr[] = array(
			"allOppTotal" => $allOppTotal,
			"allInvTotal" => $allInvTotal,
			"allReceipts" => $allReceipts,
			"allPayment" => $allPayment,
			"allProfitByOpp" => $allProfitByOpp,
			"allDebtByOpp" => $allDebtByOpp,
			"allProfitByInvoices" => $allProfitByInvoices,
			"allDebtByInvoices" => $allDebtByInvoices
			);
			
	echo json_encode($arr);
}catch(Exception $e) {
	echo json_encode($e->getMessage());
}
