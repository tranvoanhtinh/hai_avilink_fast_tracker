<?php
global $db;
if ($_SERVER["REQUEST_METHOD"] === "POST") {
    // Kiểm tra xem biến $_POST['type'] và $_POST['record'] có tồn tại không
    // if (isset($_POST['type']) && isset($_POST['record']) && isset($_POST['md'])) {
        if ($_POST['md'] == 'Accounts') {
            if ($_POST['type'] == 'id') {
                $sql = "SELECT * FROM accounts WHERE id = '" . $_POST['record'] . "' AND deleted = 0";
            }
            if ($_POST['type'] == 'phone') {
                $sql = "SELECT * FROM accounts WHERE phone_alternate = '" . $_POST['record'] . "' AND deleted = 0";
            }
        } 
        if($_POST['md'] == 'AOS_Contacts')  {

            if ($_POST['type'] == 'id') {
                $sql = "SELECT * FROM contacts WHERE id = '" . $_POST['record'] . "' AND deleted = 0";
            }
            if ($_POST['type'] == 'phone') {
                $sql = "SELECT * FROM contacts WHERE phone_mobile = '" . $_POST['record'] . "' AND deleted = 0";
            }
        }

        // Thực hiện truy vấn
        $result = $db->query($sql);
        $num_rows = mysqli_num_rows($result);
        // Kiểm tra xem truy vấn có thành công không
        if ($result) {
            // Kiểm tra xem có bản ghi nào trả về không
            if ($num_rows > 0) {
                // Lấy dòng dữ liệu đầu tiên từ kết quả truy vấn
                // $row = $result->fetch_assoc();
                $row = $db -> fetchByAssoc($result);
                // Tạo mảng thông tin
                if($_POST['md'] === 'Accounts')
                {
                    $info = array(
                        'id' => $row['id'],
                        'phone' => $row['phone_alternate'],
                        'billing_account' => $row['name'],
                        'billing_address_street' => $row['billing_address_street'],
                        'shipping_address_street' => $row['shipping_address_street']
                    );
                    echo json_encode($info);
                }
                if($_POST['md'] === 'AOS_Contacts'){
                    $info = array(
                        'id' => $row['id'],
                        'phone' => $row['phone_mobile'],
                        'billing_account' => $row['last_name'],
                    );
                    echo json_encode($info);
                }
               
            } else {

                echo -1;
            }
        } else {
            echo -1;
        }
    // }
}
