<?php
function getDateRangeToSearchForAmount($period_list_selected = 'this_month')
{
        if(empty($period_list_selected)) $period_list_selected = 'Tháng này';
        switch ($period_list_selected) {
            case 'Tuần này':
                $datetimeTo = new DateTime("this week sunday");
               // $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("this week monday");
              //  $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Tuần trước':
                $datetimeTo = new DateTime("last week sunday");
               // $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("last week monday");
               // $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Tháng này':
                $datetimeTo = new DateTime('last day of this month');
              //  $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("first day of this month");
              //  $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Tháng trước':
                $datetimeTo = new DateTime("last day of last month");
              //  $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime("first day of last month");
              //  $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Năm này':
                $datetimeTo = new DateTime('this year last day of december');
             //   $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime('this year first day of january');
             //   $datetimeFrom->setTime(0, 0, 0);
                break;
            case 'Năm trước':
                $datetimeTo = new DateTime('last year last day of december');
              //  $datetimeTo->setTime(23, 59, 59);
                $datetimeFrom = new DateTime('last year first day of january');
              //  $datetimeFrom->setTime(0, 0, 0);
                break;

        }
        return array(
            'from' => $datetimeFrom,
            'to' => $datetimeTo
        );
}
function getDateTimeAsDbForAmount($period = 'this_month', $date_from = '', $date_to = '')
{
        
		$timedate = new TimeDate();
		date_default_timezone_set('Asia/Ho_Chi_Minh');
        if(empty($date_from) && empty($date_to)) {
            $range = getDateRangeToSearchForAmount($period);

            $from_by_user = $range['from']->format($timedate::DB_DATETIME_FORMAT);
            $to_by_user = $range['to']->format($timedate::DB_DATETIME_FORMAT);

        }
        else {

            $fromObj = $timedate->fromUserDate($date_from);
            $fromObj->setTime(0, 0, 0);
            $from_by_user = $fromObj->format($timedate::DB_DATETIME_FORMAT);

            $toObj = $timedate->fromUserDate($date_to);
            $toObj->setTime(23,59,59);
            $to_by_user = $toObj->format($timedate::DB_DATETIME_FORMAT);

        }

        $from_display = $timedate->to_display_date_time($from_by_user, true, false);
        $from = $timedate->to_db($from_display);

        $to_display = $timedate->to_display_date_time($to_by_user, true, false);
        $to = $timedate->to_db($to_display);

        $datetime_as_db = array(
            'from' => $from,
            'to' => $to
        );

        return $datetime_as_db;
}

if($_SERVER['REQUEST_METHOD'] == 'POST'){
	try{
		if (isset($_SERVER['HTTPS']) &&
		($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
		isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
		$_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https'){
					$protocol = 'https://';
		}
		else {
			$protocol = 'http://';
		}
		$apiUrl = $protocol.$_SERVER['HTTP_HOST']."/index.php?entryPoint=getLanguageForDropdown&id=status_activities_list";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $apiUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        $response = curl_exec($ch);
		$response = ((array)((array)(json_decode($response))[0])["language"]);
        curl_close($ch);
	global $db;
	$arr = array();
	$whr='';
	$sea = "";
	$myMeeting = "";
	global $current_user;
	$current_user->id = $_POST["user"];
	$start= ($_POST["page"] -1) * 200;
	if(isset($_POST['time']) && $_POST['time'] != "Tất cả"){
		$time = getDateTimeAsDbForAmount($_POST['time']);
		$from=date('Y-m-d', strtotime($time['from']));
		$to=date('Y-m-d', strtotime($time['to']));
		if(date('m', strtotime($time['to'])) == "02" && $_POST['time'] == "Tháng này"){
			$to = date('Y', strtotime($time['to']))."-02-29";
		}
		$wh = " and meetings.date_entered >= '".$from."' and meetings.date_entered <= '".$to."'";
	}else{
		$wh= '';	
	}
	if($_POST['myMeeting'] == "1"){
		$myMeeting = " and meetings.assigned_user_id = '".$_POST["user"]."'";
	}
	if(isset($_POST['search'])){
		if($_POST['search'] != ""){
			$sea .= " and (meetings.name like '%".$_POST['search']."%' OR meetings.status like '%".$_POST['search']."%' OR meetings.description like '%".$_POST['search']."%')";
		}
	}
	$where = "meetings.deleted = '0'".$wh.$sea.$myMeeting;
	$meeting = BeanFactory::newBean('Meetings');
	$queryNew = $meeting->create_new_list_query("meetings.date_entered DESC",$where,array());
	$query = "select meetings.*,users.user_name,leads.last_name,accounts.name as accName from ($queryNew) as meetings left join accounts on meetings.parent_id = accounts.id and accounts.deleted = '0' left join meetings_leads on meetings.id = meetings_leads.meeting_id left join leads on meetings_leads.lead_id = leads.id and leads.deleted = 0 left join users on meetings.assigned_user_id = users.id and users.deleted = '0' order by meetings.date_entered DESC";
	$queryCount = "select count(meetings.id) as num from meetings as tableMeeting inner join ($queryNew) as meetings on meetings.id = tableMeeting.id where meetings.deleted = '0'";
	$resCount = $db->query($queryCount);
	$rowCount = $db->fetchByAssoc($resCount);
    $limit_query = $db->limitQuery($query, $start, 200, false, '', false);
    $res = $db->query($limit_query);
	while($row = $db->fetchByAssoc($res)){
		$arr[] = array(
							"id"=>$row["id"],
							"name"=>$row["last_name"],
							"status"=>$row["status"],
							"date_entered"=>date('Y-m-d H:i:s', strtotime($row["date_entered"].' + 7 hours')),
							"user_name"=>$row["user_name"],
							"date_start"=>date('Y-m-d H:i:s', strtotime($row["date_start"].' + 7 hours')),
							"accountName"=>$row["accName"],
							"img"=>$row["image_checkin"],
							"locationa"=>$row["location_a"],
							"locationb"=>$row["location_b"],
							"daycheckin"=>$row["daycheckin"],
							"lastName"=>$row["last_name"],
							"description"=>$row["description"],
							"status_activites"=>$response[$row["status_activites"]],
							"length"=>$rowCount["num"],
						);
	}
	echo json_encode($arr);
	}
	catch(PDOException $e) {
		echo "Error: " . $e->getMessage();
	}
}