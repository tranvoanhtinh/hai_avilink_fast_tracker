<?php
// created: 2024-06-14 03:29:34
$GLOBALS['tabStructure'] = array (
  'LBL_TABGROUP_SALES' => 
  array (
    'label' => 'LBL_TABGROUP_SALES',
    'modules' => 
    array (
      0 => 'Home',
      1 => 'AOS_Bookings',
      2 => 'Accounts',
      3 => 'AOS_Passengers',
      4 => 'bill_Billing',
      5 => 'spa_ActionService',
    ),
  ),
  'LBL_GROUPTAB5_1718328528' => 
  array (
    'label' => 'LBL_GROUPTAB5_1718328528',
    'modules' => 
    array (
      0 => 'Home',
      1 => 'Accounts',
      2 => 'Contacts',
      3 => 'AOS_Passengers',
    ),
  ),
  'LBL_GROUPTAB4_1717035958' => 
  array (
    'label' => 'LBL_GROUPTAB4_1717035958',
    'modules' => 
    array (
      0 => 'Home',
      1 => 'Accounts',
      2 => 'Contacts',
      3 => 'AOS_Cost_unitprice',
      4 => 'AOS_Data_unitprice',
    ),
  ),
  'LBL_TABGROUP_REPORTS' => 
  array (
    'label' => 'LBL_TABGROUP_REPORTS',
    'modules' => 
    array (
      0 => 'Home',
      1 => 'rls_Reports',
    ),
  ),
  'LBL_GROUPTAB6_1648088410' => 
  array (
    'label' => 'LBL_GROUPTAB6_1648088410',
    'modules' => 
    array (
      0 => 'Home',
      1 => 'AOS_Product_Categories',
      2 => 'AOS_Products',
    ),
  ),
  'LBL_GROUPTAB6_1615864794' => 
  array (
    'label' => 'LBL_GROUPTAB6_1615864794',
    'modules' => 
    array (
      0 => 'Home',
      1 => 'cs_cusUser',
      2 => 'tttt_WorldfonePBX',
      3 => 'SecurityGroups',
      4 => 'AOW_WorkFlow',
      5 => 'jckl_DashboardTemplates',
      6 => 'la_LoginAudit',
    ),
  ),
);