<?php

class LeadCardLogicHook {
    public function autoSetStatus(&$bean, $event, $args)
    {
        if(!in_array($bean->module_dir, array('Calls', 'Meetings'))) return;
        // Check if field present on form
        if(isset($_REQUEST['is_completed'])) {
            if($bean->is_completed)
            {
                $bean->status = 'Held';
            }
            else {
                $bean->status = 'Planned';
            }
        }
        // If direction is not present on form => auto set is Out
        if($bean->module_dir == 'Calls' && !isset($_REQUEST['direction'])) {
            $bean->direction = 'Outbound';
        }
        $GLOBALS['log']->fatal($bean->status);
        // If status is Held or Not Held => should change to complete
        if($bean->status == 'Held' || $bean->status == 'Not Held') {
            $bean->is_completed = 1;
        }
    }
}