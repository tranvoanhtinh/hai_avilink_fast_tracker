<?php


class MTSSecurityGroupHook
{
    function updateSecurityGroupOnRecord(&$bean, $event, $args = array())
    {
        if(!empty($bean->assigned_user_id)) {
            $groups = $this->getUserSecurityGroups($bean->assigned_user_id);
				$sc = new SecurityGroup();
				if(($groups["parent"] != null && $groups["parent"] != "") || ($groups["only"] != null && $groups["only"] != "")){
					$sc->deleteGroupFromRecord($bean->module_dir, $bean->id, $groups["parent"]);
				}
			
        }
    }

    public function getUserSecurityGroups($user_id = '')
    {
        if(empty($user_id)) return false;
        global $db;
		$groupParent = "";
		$groupOnly = "";
		//$group = "";
		
        $query = 'select securitygroups.id, securitygroups.name, securitygroups.only_assign_record_to_this_group, is_parent_group from securitygroups_users '
            . 'inner join securitygroups on securitygroups_users.securitygroup_id = securitygroups.id '
            . '      and securitygroups.deleted = 0 '
            . "where securitygroups_users.user_id='$user_id' and securitygroups_users.deleted = 0 "
            . 'order by securitygroups.name asc ';
        $result = $db->query($query, true, 'Error finding the full membership list for a user: ');

        $group_array = array();
        while ($row = $db->fetchByAssoc($result)){
			if($row["is_parent_group"] == 1){
				$groupParent = $row["id"];
				break;
			}else{
				if($row["only_assign_record_to_this_group"] == 1){
				$groupOnly = $row["id"];
				}
			}
        }
        return array("parent" => $groupParent,
					"only" => 	$groupOnly
					);
    }
}