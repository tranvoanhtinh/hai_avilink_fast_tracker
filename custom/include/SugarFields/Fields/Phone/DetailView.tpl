{*
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/

*}
{if !empty({{sugarvar key='value' string=true}})}
{assign var="phone_value" value={{sugarvar key='value' string=true}} }
 
{if $phone_value neq ''}
<img style='cursor:pointer;vertical-align: sub;' src="custom/themes/default/images/messages.png" onclick="smstonumber('{$phone_value}','{$module}','{$id}');">
{if $fields.phone_mobile != ""}
						<a style="cursor:pointer;" onclick="clicktocall('{$phone_value}','{$fields.name.value}','{$fields.id.value}','{$fields.account_name.value}')" >
							        {$phone_value}
						</a>
						{/if}
						{if $fields.phone_alternate != ""}
						<a style="cursor:pointer;" onclick="clicktocall('{$phone_value}','{$fields.name.value}','{$fields.id.value}','{$fields.primarycontact_c.value}')" >
							        {$phone_value}
							        </a>
									
						{/if}
{/if}


{{if !empty($displayParams.enableConnectors)}}
{{sugarvar_connector view='DetailView'}}
{{/if}}
{/if}