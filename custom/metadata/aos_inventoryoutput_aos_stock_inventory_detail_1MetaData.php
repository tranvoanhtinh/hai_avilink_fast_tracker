<?php
// created: 2024-02-26 09:02:10
$dictionary["aos_inventoryoutput_aos_stock_inventory_detail_1"] = array (
  'true_relationship_type' => 'many-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'aos_inventoryoutput_aos_stock_inventory_detail_1' => 
    array (
      'lhs_module' => 'AOS_Inventoryoutput',
      'lhs_table' => 'aos_inventoryoutput',
      'lhs_key' => 'id',
      'rhs_module' => 'AOS_Stock_Inventory_Detail',
      'rhs_table' => 'aos_stock_inventory_detail',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'aos_inventoryoutput_aos_stock_inventory_detail_1_c',
      'join_key_lhs' => 'aos_invent74a4youtput_ida',
      'join_key_rhs' => 'aos_inventc914_detail_idb',
    ),
  ),
  'table' => 'aos_inventoryoutput_aos_stock_inventory_detail_1_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'aos_invent74a4youtput_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'aos_inventc914_detail_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'aos_inventoryoutput_aos_stock_inventory_detail_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'aos_inventoryoutput_aos_stock_inventory_detail_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'aos_invent74a4youtput_ida',
        1 => 'aos_inventc914_detail_idb',
      ),
    ),
  ),
);