<?php
// created: 2023-05-23 08:57:15
$dictionary["spa_actionservice_cs_cususer"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'spa_actionservice_cs_cususer' => 
    array (
      'lhs_module' => 'cs_cusUser',
      'lhs_table' => 'cs_cususer',
      'lhs_key' => 'id',
      'rhs_module' => 'spa_ActionService',
      'rhs_table' => 'spa_actionservice',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'spa_actionservice_cs_cususer_c',
      'join_key_lhs' => 'spa_actionservice_cs_cususercs_cususer_ida',
      'join_key_rhs' => 'spa_actionservice_cs_cususerspa_actionservice_idb',
    ),
  ),
  'table' => 'spa_actionservice_cs_cususer_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'spa_actionservice_cs_cususercs_cususer_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'spa_actionservice_cs_cususerspa_actionservice_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'spa_actionservice_cs_cususerspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'spa_actionservice_cs_cususer_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'spa_actionservice_cs_cususercs_cususer_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'spa_actionservice_cs_cususer_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'spa_actionservice_cs_cususerspa_actionservice_idb',
      ),
    ),
  ),
);