<?php
// created: 2021-08-18 06:32:45
$dictionary["bill_billing_accounts"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'bill_billing_accounts' => 
    array (
      'lhs_module' => 'Accounts',
      'lhs_table' => 'accounts',
      'lhs_key' => 'id',
      'rhs_module' => 'bill_Billing',
      'rhs_table' => 'bill_billing',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'bill_billing_accounts_c',
      'join_key_lhs' => 'bill_billing_accountsaccounts_ida',
      'join_key_rhs' => 'bill_billing_accountsbill_billing_idb',
    ),
  ),
  'table' => 'bill_billing_accounts_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'bill_billing_accountsaccounts_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'bill_billing_accountsbill_billing_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'bill_billing_accountsspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'bill_billing_accounts_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'bill_billing_accountsaccounts_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'bill_billing_accounts_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'bill_billing_accountsbill_billing_idb',
      ),
    ),
  ),
);