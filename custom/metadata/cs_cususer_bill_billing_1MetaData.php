<?php
// created: 2023-05-23 06:52:50
$dictionary["cs_cususer_bill_billing_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'cs_cususer_bill_billing_1' => 
    array (
      'lhs_module' => 'cs_cusUser',
      'lhs_table' => 'cs_cususer',
      'lhs_key' => 'id',
      'rhs_module' => 'bill_Billing',
      'rhs_table' => 'bill_billing',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'cs_cususer_bill_billing_1_c',
      'join_key_lhs' => 'cs_cususer_bill_billing_1cs_cususer_ida',
      'join_key_rhs' => 'cs_cususer_bill_billing_1bill_billing_idb',
    ),
  ),
  'table' => 'cs_cususer_bill_billing_1_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'cs_cususer_bill_billing_1cs_cususer_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'cs_cususer_bill_billing_1bill_billing_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'cs_cususer_bill_billing_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'cs_cususer_bill_billing_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'cs_cususer_bill_billing_1cs_cususer_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'cs_cususer_bill_billing_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'cs_cususer_bill_billing_1bill_billing_idb',
      ),
    ),
  ),
);