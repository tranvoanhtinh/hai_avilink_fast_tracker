<?php
// created: 2022-10-11 09:31:13
$dictionary["tttt_worldfonepbx_users_1"] = array (
  'true_relationship_type' => 'many-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'tttt_worldfonepbx_users_1' => 
    array (
      'lhs_module' => 'tttt_WorldfonePBX',
      'lhs_table' => 'tttt_worldfonepbx',
      'lhs_key' => 'id',
      'rhs_module' => 'Users',
      'rhs_table' => 'users',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'tttt_worldfonepbx_users_1_c',
      'join_key_lhs' => 'tttt_worldfonepbx_users_1tttt_worldfonepbx_ida',
      'join_key_rhs' => 'tttt_worldfonepbx_users_1users_idb',
    ),
  ),
  'table' => 'tttt_worldfonepbx_users_1_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'tttt_worldfonepbx_users_1tttt_worldfonepbx_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'tttt_worldfonepbx_users_1users_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'tttt_worldfonepbx_users_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'tttt_worldfonepbx_users_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'tttt_worldfonepbx_users_1tttt_worldfonepbx_ida',
        1 => 'tttt_worldfonepbx_users_1users_idb',
      ),
    ),
  ),
);