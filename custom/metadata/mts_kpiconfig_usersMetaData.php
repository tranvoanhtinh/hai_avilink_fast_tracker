<?php
// created: 2018-09-02 09:33:00
$dictionary["mts_kpiconfig_users"] = array (
  'true_relationship_type' => 'many-to-many',
  'relationships' => 
  array (
    'mts_kpiconfig_users' => 
    array (
      'lhs_module' => 'MTS_KPIConfig',
      'lhs_table' => 'mts_kpiconfig',
      'lhs_key' => 'id',
      'rhs_module' => 'Users',
      'rhs_table' => 'users',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'mts_kpiconfig_users_c',
      'join_key_lhs' => 'mts_kpiconfig_usersmts_kpiconfig_ida',
      'join_key_rhs' => 'mts_kpiconfig_usersusers_idb',
    ),
  ),
  'table' => 'mts_kpiconfig_users_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'mts_kpiconfig_usersmts_kpiconfig_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'mts_kpiconfig_usersusers_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'mts_kpiconfig_usersspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'mts_kpiconfig_users_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'mts_kpiconfig_usersmts_kpiconfig_ida',
        1 => 'mts_kpiconfig_usersusers_idb',
      ),
    ),
  ),
);