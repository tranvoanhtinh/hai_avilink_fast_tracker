<?php
// created: 2023-05-23 08:57:15
$dictionary["spa_actionservice_aos_products"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'spa_actionservice_aos_products' => 
    array (
      'lhs_module' => 'AOS_Products',
      'lhs_table' => 'aos_products',
      'lhs_key' => 'id',
      'rhs_module' => 'spa_ActionService',
      'rhs_table' => 'spa_actionservice',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'spa_actionservice_aos_products_c',
      'join_key_lhs' => 'spa_actionservice_aos_productsaos_products_ida',
      'join_key_rhs' => 'spa_actionservice_aos_productsspa_actionservice_idb',
    ),
  ),
  'table' => 'spa_actionservice_aos_products_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'spa_actionservice_aos_productsaos_products_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'spa_actionservice_aos_productsspa_actionservice_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'spa_actionservice_aos_productsspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'spa_actionservice_aos_products_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'spa_actionservice_aos_productsaos_products_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'spa_actionservice_aos_products_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'spa_actionservice_aos_productsspa_actionservice_idb',
      ),
    ),
  ),
);