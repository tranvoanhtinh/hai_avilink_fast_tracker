<?php
// created: 2023-06-06 10:14:10
$dictionary["lm_leavemanagerment_cs_cususer"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'lm_leavemanagerment_cs_cususer' => 
    array (
      'lhs_module' => 'cs_cusUser',
      'lhs_table' => 'cs_cususer',
      'lhs_key' => 'id',
      'rhs_module' => 'lm_LeaveManagerment',
      'rhs_table' => 'lm_leavemanagerment',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'lm_leavemanagerment_cs_cususer_c',
      'join_key_lhs' => 'lm_leavemanagerment_cs_cususercs_cususer_ida',
      'join_key_rhs' => 'lm_leavemanagerment_cs_cususerlm_leavemanagerment_idb',
    ),
  ),
  'table' => 'lm_leavemanagerment_cs_cususer_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'lm_leavemanagerment_cs_cususercs_cususer_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'lm_leavemanagerment_cs_cususerlm_leavemanagerment_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'lm_leavemanagerment_cs_cususerspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'lm_leavemanagerment_cs_cususer_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'lm_leavemanagerment_cs_cususercs_cususer_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'lm_leavemanagerment_cs_cususer_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'lm_leavemanagerment_cs_cususerlm_leavemanagerment_idb',
      ),
    ),
  ),
);