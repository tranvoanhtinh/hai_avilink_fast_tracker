<?php
// created: 2023-05-12 03:38:53
$dictionary["pr_payrollparam_cs_cususer"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'pr_payrollparam_cs_cususer' => 
    array (
      'lhs_module' => 'pr_PayrollParam',
      'lhs_table' => 'pr_payrollparam',
      'lhs_key' => 'id',
      'rhs_module' => 'cs_cusUser',
      'rhs_table' => 'cs_cususer',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'pr_payrollparam_cs_cususer_c',
      'join_key_lhs' => 'pr_payrollparam_cs_cususerpr_payrollparam_ida',
      'join_key_rhs' => 'pr_payrollparam_cs_cususercs_cususer_idb',
    ),
  ),
  'table' => 'pr_payrollparam_cs_cususer_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'pr_payrollparam_cs_cususerpr_payrollparam_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'pr_payrollparam_cs_cususercs_cususer_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'pr_payrollparam_cs_cususerspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'pr_payrollparam_cs_cususer_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'pr_payrollparam_cs_cususerpr_payrollparam_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'pr_payrollparam_cs_cususer_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'pr_payrollparam_cs_cususercs_cususer_idb',
      ),
    ),
  ),
);