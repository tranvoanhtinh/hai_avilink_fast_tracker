<?php
// created: 2024-02-23 10:04:37
$dictionary["aos_stock_inventory_aos_products_1"] = array (
  'true_relationship_type' => 'many-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'aos_stock_inventory_aos_products_1' => 
    array (
      'lhs_module' => 'AOS_Stock_Inventory',
      'lhs_table' => 'aos_stock_inventory',
      'lhs_key' => 'id',
      'rhs_module' => 'AOS_Products',
      'rhs_table' => 'aos_products',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'aos_stock_inventory_aos_products_1_c',
      'join_key_lhs' => 'aos_stock_inventory_aos_products_1aos_stock_inventory_ida',
      'join_key_rhs' => 'aos_stock_inventory_aos_products_1aos_products_idb',
    ),
  ),
  'table' => 'aos_stock_inventory_aos_products_1_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'aos_stock_inventory_aos_products_1aos_stock_inventory_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'aos_stock_inventory_aos_products_1aos_products_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'aos_stock_inventory_aos_products_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'aos_stock_inventory_aos_products_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'aos_stock_inventory_aos_products_1aos_stock_inventory_ida',
        1 => 'aos_stock_inventory_aos_products_1aos_products_idb',
      ),
    ),
  ),
);