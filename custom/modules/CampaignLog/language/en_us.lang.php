<?php
// created: 2024-06-05 06:17:11
$mod_strings = array (
  'LBL_CREATED_LEAD' => 'Created Lead',
  'LBL_CREATED_CONTACT' => 'Created Contact',
  'LBL_CREATED_OPPORTUNITY' => 'Created Opportunity',
  'LBL_CAMPAIGN_NAME' => 'Name:',
  'LBL_CAMPAIGNS' => 'Campaigns',
);