<?php
// created: 2019-05-13 18:13:10
$mod_strings = array (
  'LBL_CREATED_LEAD' => 'Created Lead',
  'LBL_CREATED_OPPORTUNITY' => 'Created Opportunity',
  'LBL_CREATED_CONTACT' => 'Created Contact',
  'LBL_CAMPAIGN_NAME' => 'Name:',
  'LBL_CAMPAIGNS' => 'Campaigns',
);