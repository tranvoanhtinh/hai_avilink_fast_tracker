<?php
// created: 2019-05-13 18:13:10
$mod_strings = array (
  'LBL_PARENT_NAME' => 'Project Name',
  'LBL_NOTES' => 'Notes',
  'LBL_TASKS' => 'Tasks',
  'LBL_MEETINGS' => 'Meetings',
  'LBL_CALLS' => 'Calls',
  'LBL_LIST_PARENT_NAME' => 'Project',
  'LBL_PROJECT_NAME' => 'Project Name',
);