<?php
// created: 2021-01-06 04:28:30
$mod_strings = array (
  'LNK_NEW_RECORD' => 'Create Dynamic  Reports',
  'LNK_LIST' => 'View Dynamic  Reports',
  'LNK_IMPORT_RLS_REPORTS' => 'Import Dynamic  Reports',
  'LBL_LIST_FORM_TITLE' => 'Dynamic Reports List',
  'LBL_SEARCH_FORM_TITLE' => 'Search Dynamic Reports',
  'LBL_HOMEPAGE_TITLE_CHART' => 'CE Report Chart',
  'LBL_HOMEPAGE_TITLE' => 'My Dynamic  Reports',
);