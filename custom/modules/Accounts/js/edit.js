

var check_duplicate = 0;
var duplicate = document.querySelector('input[name="duplicateSave"]');
if(duplicate){
    if(duplicate.value =='true'){
        check_duplicate = 1;  // đang trùng lập
    }
}

var input_name = document.getElementById('account_code'); // Lấy thẻ input bằng ID
function get_voucher_code() {
    var currentDate = new Date(); // Lấy ngày hiện tại
    var day = currentDate.getDate();
    var month = currentDate.getMonth() + 1; // Tháng bắt đầu từ 0 nên cần cộng thêm 1
    var year = currentDate.getFullYear();
    var formattedDate = (day < 10 ? '0' : '') + day + '/' + (month < 10 ? '0' : '') + month + '/' + year;

    $.ajax({
        type: "POST",
        url: "index.php?entryPoint=get_voucher_code",
        data: {
            module: 'accounts',
            voucherdate: formattedDate,
        },
        success: function(data) {
            input_name.value = data;
            input_name.style.color = "blue";
            input_name.style.fontWeight = "bold";
        },
        error: function(error) {
            // Xử lý lỗi nếu cần
        }
    });
}
if(input_name.value ==''){
    get_voucher_code();
}
if(check_duplicate == 1){
    get_voucher_code();
}

var fieldcode = 'account_code';
var oldValue ='';
var editViewFieldDiv = document.querySelector('.edit-view-field[type="varchar"][field="account_code"]');
if (editViewFieldDiv) {
    var pElement = document.createElement('p');
    pElement.textContent = '';
    pElement.id = 'remaining';
    editViewFieldDiv.appendChild(pElement);
}

if(check_duplicate == 1){
        firstvalue = -999;
}else {
    var firstvalue; 
    document.addEventListener("DOMContentLoaded", function() {
        var inputElement = document.getElementById(fieldcode);
        if (inputElement) {
            firstvalue = inputElement.value; 
        }
    });
}

document.getElementById(fieldcode).addEventListener("focus", function() {
    // Lưu trữ giá trị hiện tại của input khi focus vào
    oldValue = this.value;
    this.style.color = "black";
    console.log(firstvalue);
    check_voucher_code(fieldcode,'accounts',oldValue,firstvalue);
});

document.getElementById(fieldcode).addEventListener("blur", function() {
      console.log(firstvalue);
    check_voucher_code(fieldcode,'accounts',oldValue,firstvalue);

});
