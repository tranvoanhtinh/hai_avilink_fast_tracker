<?php
/**
 * Created by PhpStorm.
 * User: nhatthieu
 * Date: 9/19/18
 * Time: 8:12 PM
 */

class LeadCardAccountHook
{
    public function linkAccountToOpportunities(&$bean)
    {
        if($bean->fetched_row['account_code'] != $bean->account_code && !empty($bean->account_code)) {
            $sql = "SELECT id FROM opportunities WHERE deleted=0 AND customer_id='{$bean->account_code}'";
            $res = $bean->db->query($sql);

            $opps = array();
            while ($opp = $bean->db->fetchByAssoc($res)) {
                $opps[] = $opp['id'];
            }

            if(!empty($opps)) {
                $bean->load_relationship('opportunities');
                $bean->opportunities->add($opps);
            }
        }
    }

    public function changeLeadStatus(&$bean, $event, $args)
    {
        $link = $args['link'];
        if(!empty($link) && $link == 'opportunities') {
            $opportunity = $args['related_bean'];
            if($opportunity->sales_stage == 'Closed Won') {
                $bean->db->query("UPDATE leads SET status='Transaction' WHERE account_id = '{$bean->id}'");
            }
        }
    }

    public function sumRevenueOfLead(&$bean, $event, $args)
    {
        $link = $args['link'];
        if(!empty($link) && $link == 'opportunities') {
            $amount = $this->sumOpportunityAmount($bean->id) + $args['related_bean']->amount;
            $bean->db->query("UPDATE leads SET opportunity_amount = '{$amount}' WHERE account_id = '{$bean->id}'");
        }
    }

    public function sumOpportunityAmount($account_id)
    {
        $sql = "SELECT
                    SUM(op.amount)
                FROM
                    opportunities op
                INNER JOIN accounts_opportunities ac_op ON
                    op.id = ac_op.opportunity_id AND ac_op.deleted = 0
                WHERE
                    op.deleted = 0 AND op.sales_stage <> 'Closed Lost' AND ac_op.account_id = '{$account_id}'";
        return $GLOBALS['db']->getOne($sql);
    }
}