<?php
// created: 2024-06-05 06:17:26
$mod_strings = array (
  'LBL_DOCUMENTS_SUBPANEL_TITLE' => 'Documents',
  'LBL_CHARTS' => 'Charts',
  'LBL_DEFAULT' => 'Views',
  'ERR_DELETE_RECORD' => 'You must specify a record number in order to delete the account.',
  'LBL_ACCOUNT_INFORMATION' => 'OVERVIEW',
  'LBL_ACCOUNT_NAME' => 'Account Name:',
  'LBL_ACCOUNT' => 'Account:',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
  'LBL_ADDRESS_INFORMATION' => 'Address Information',
  'LBL_ANNUAL_REVENUE' => 'Annual Revenue:',
  'LBL_ANY_ADDRESS' => 'Any Address:',
  'LBL_ANY_EMAIL' => 'Any Email:',
  'LBL_ANY_PHONE' => 'Any Phone:',
  'LBL_ASSIGNED_TO_NAME' => 'Assigned to:',
  'LBL_ASSIGNED_TO_ID' => 'Assigned User:',
  'LBL_BILLING_ADDRESS_CITY' => 'Billing City:',
  'LBL_BILLING_ADDRESS_COUNTRY' => 'Billing Country:',
  'LBL_BILLING_ADDRESS_POSTALCODE' => 'Billing Ward:',
  'LBL_BILLING_ADDRESS_STATE' => 'Billing District:',
  'LBL_BILLING_ADDRESS_STREET_2' => 'Billing Street 2',
  'LBL_BILLING_ADDRESS_STREET_3' => 'Billing Street 3',
  'LBL_BILLING_ADDRESS_STREET_4' => 'Billing Street 4',
  'LBL_BILLING_ADDRESS_STREET' => 'Billing Street:',
  'LBL_BILLING_ADDRESS' => 'Billing Address:',
  'LBL_BUGS_SUBPANEL_TITLE' => 'Bugs',
  'LBL_CAMPAIGN_ID' => 'Campaign ID',
  'LBL_CASES_SUBPANEL_TITLE' => 'Ticketss',
  'LBL_CITY' => 'City:',
  'LBL_CONTACTS_SUBPANEL_TITLE' => 'Contactss',
  'LBL_COUNTRY' => 'Country:',
  'LBL_DATE_ENTERED' => 'Date Created:',
  'LBL_DATE_MODIFIED' => 'Date Modified:',
  'LBL_DEFAULT_SUBPANEL_TITLE' => 'Accounts',
  'LBL_DESCRIPTION_INFORMATION' => 'Description Information',
  'LBL_DESCRIPTION' => 'Description:',
  'LBL_DUPLICATE' => 'Possible Duplicate Account',
  'LBL_EMAIL' => 'Email Address:',
  'LBL_EMAIL_OPT_OUT' => 'Email Opt Out:',
  'LBL_EMAIL_ADDRESSES' => 'Email Addresses',
  'LBL_EMPLOYEES' => 'Employees:',
  'LBL_FAX' => 'Fax:',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'History',
  'LBL_HOMEPAGE_TITLE' => 'My Accounts',
  'LBL_INDUSTRY' => 'Industry:',
  'LBL_INVALID_EMAIL' => 'Invalid Email:',
  'LBL_INVITEE' => 'Contacts',
  'LBL_LEADS_SUBPANEL_TITLE' => 'Leads',
  'LBL_LIST_ACCOUNT_NAME' => 'Name',
  'LBL_LIST_CITY' => 'City',
  'LBL_LIST_CONTACT_NAME' => 'Contact Name',
  'LBL_LIST_EMAIL_ADDRESS' => 'Email Address',
  'LBL_LIST_FORM_TITLE' => 'Agent List',
  'LBL_LIST_PHONE' => 'Office Phone',
  'LBL_LIST_STATE' => 'State',
  'LBL_MEMBER_OF' => 'Thành viên của:',
  'LBL_MEMBER_ORG_SUBPANEL_TITLE' => 'Member Organizations',
  'LBL_MODULE_NAME' => 'Agent',
  'LBL_MODULE_TITLE' => 'Accounts: Home',
  'LBL_MODULE_ID' => 'Accounts',
  'LBL_NAME' => 'Agency name',
  'LBL_NEW_FORM_TITLE' => 'New Account',
  'LBL_OPPORTUNITIES_SUBPANEL_TITLE' => 'Opportunities',
  'LBL_OTHER_EMAIL_ADDRESS' => 'Other Email:',
  'LBL_OTHER_PHONE' => 'Alternate Phone:',
  'LBL_OWNERSHIP' => 'Ownership:',
  'LBL_PARENT_ACCOUNT_ID' => 'Parent Agent ID',
  'LBL_PHONE_ALT' => 'Alternate Phone:',
  'LBL_PHONE_FAX' => 'Phone Fax:',
  'LBL_PHONE_OFFICE' => 'Office Phone:',
  'LBL_PHONE' => 'Office Phone::',
  'LBL_POSTAL_CODE' => 'Ward:',
  'LBL_PRODUCTS_TITLE' => 'Products',
  'LBL_PROJECTS_SUBPANEL_TITLE' => 'Projects',
  'LBL_PUSH_CONTACTS_BUTTON_LABEL' => 'Copy to Contacts',
  'LBL_PUSH_CONTACTS_BUTTON_TITLE' => 'Copy...',
  'LBL_RATING' => 'Rating:',
  'LBL_SAVE_ACCOUNT' => 'Save Agent',
  'LBL_SEARCH_FORM_TITLE' => 'Agent Search',
  'LBL_SHIPPING_ADDRESS_CITY' => 'Shipping City:',
  'LBL_SHIPPING_ADDRESS_COUNTRY' => 'Shipping Country:',
  'LBL_SHIPPING_ADDRESS_POSTALCODE' => 'Shipping Ward:',
  'LBL_SHIPPING_ADDRESS_STATE' => 'Shipping District:',
  'LBL_SHIPPING_ADDRESS_STREET_2' => 'Shipping Street 2',
  'LBL_SHIPPING_ADDRESS_STREET_3' => 'Shipping Street 3',
  'LBL_SHIPPING_ADDRESS_STREET_4' => 'Shipping Street 4',
  'LBL_SHIPPING_ADDRESS_STREET' => 'Shipping Street:',
  'LBL_SHIPPING_ADDRESS' => 'Shipping Address:',
  'LBL_SIC_CODE' => 'Bank Account:',
  'LBL_STATE' => 'District:',
  'LBL_TICKER_SYMBOL' => 'Ticker Symbol:',
  'LBL_TYPE' => 'Type Account:',
  'LBL_WEBSITE' => 'Website:',
  'LBL_CAMPAIGNS' => 'Campaigns',
  'LNK_ACCOUNT_LIST' => 'View Agent',
  'LNK_NEW_ACCOUNT' => 'Create Agent',
  'LNK_IMPORT_ACCOUNTS' => 'Import Agent',
  'MSG_DUPLICATE' => 'The account record you are about to create might be a duplicate of an account record that already exists. Account records containing similar names are listed below.Click Create Account to continue creating this new account, or select an existing account listed below.',
  'MSG_SHOW_DUPLICATES' => 'The account record you are about to create might be a duplicate of an account record that already exists. Agent records containing similar names are listed below.Click Save to continue creating this new account, or click Cancel to return to the module without creating the account.',
  'LBL_ASSIGNED_USER_NAME' => 'Assigned to:',
  'LBL_PROSPECT_LIST' => 'Prospect List',
  'LBL_ACCOUNTS_SUBPANEL_TITLE' => 'Accounts',
  'LBL_PROJECT_SUBPANEL_TITLE' => 'Projects',
  'LBL_PARENT_ID' => 'Parent ID',
  'LBL_PRODUCTS_SERVICES_PURCHASED_SUBPANEL_TITLE' => 'Products and Services Purchased',
  'LBL_AOS_CONTRACTS' => 'Contracts',
  'LBL_AOS_INVOICES' => 'Invoices',
  'LBL_AOS_QUOTES' => 'Quotes',
  'LBL_OPPORTUNITY_AMOUNT' => 'Opportunity Amount',
  'LBL_LIST_LEAD_SOURCE' => 'Lead Source',
  'LBL_STATUS' => 'Status',
  'DB_NAME' => 'Account Name',
  'DB_WEBSITE' => 'Website',
  'DB_BILLING_ADDRESS_CITY' => 'City',
  'LBL_TAX_CODE' => 'Tax Code',
  'LBL_ACCOUNT_CODE' => 'Agent code',
  'LBL_CONTACT_NAME' => 'Contact Name',
  'LBL_LEADSOURCE' => 'Lead Source',
  'LBL_SECURITYGROUPS_SUBPANEL_TITLE' => 'Security Groups',
  'LBL_MEMBERS' => 'Thành viên',
  'LBL_LEADS' => 'Leads',
  'LBL_EDITVIEW_PANEL1' => 'ADDRESS INFORMATION',
  'LBL_DETAILVIEW_PANEL1' => 'Add Information',
  'LBL_CONTACTS' => 'Liên hệ',
  'LBL_OPPORTUNITY' => 'Opportunity',
  'LBL_CAMPAIGN' => 'Campaign:',
  'LBL_CASES' => 'Ticketss',
  'LBL_TASKS' => 'Tasks',
  'LBL_NOTES' => 'Notess',
  'LBL_MEETINGS' => 'Meetings',
  'LBL_CALLS' => 'Calls',
  'LBL_PROJECTS' => 'Projects',
  'LBL_BUSSINESS_TYPE' => 'Bussiness Type',
  'LBL_QUICKCREATE_PANEL1' => 'MORE INFO',
  'LBL_CLASSIFY_ACCOUNT' => 'Classify Account',
  'LBL_SHORTNAME' => 'ShortName',
  'LBL_SKYPENAME' => 'SkypeName',
  'LBL_DATEACCOUNT' => 'DateAccount',
  'LBL_STATUSACCOUNT' => 'Status Account',
  'LBL_RATINGACCOUNT' => 'Rating Account',
  'LBL_STATUSDESCRIPTION' => 'Status Description',
  'LBL_PRIMARYCONTACT' => 'Primary Contact ',
  'LBL_PLANDESCRIPTION' => 'PlanDescription',
  'LBL_IDENTITYCARD' => 'Identity Card',
  'LBL_ERP_ID' => 'ERP ID',
  'LBL_LASTDATEACTIVE' => 'LastDateActive',
  'LBL_PLANDATEACTIVE' => 'PlanDateActive',
  'LBL_COMPANY' => 'Company',
  'LBL_EDITVIEW_PANEL2' => 'Personal Information',
  'LBL_PANEL_ADVANCED' => 'Organization Information',
  'LBL_BIRTHDATE' => 'Birthdate:',
  'LBL_DETAILVIEW_PANEL2' => 'Personal Information',
  'LBL_LEAD_ACCOUNT_STATUS' => 'IsCustommer?',
  'LBL_PROJECTNAME' => 'ProjectName',
  'LBL_BANKACCOUNT' => 'BankAccount Num',
  'LBL_DATETRANSACTION' => 'DateTransaction',
  'LBL_BANKNAME' => 'BankName',
  'LBL_BANKADDRESS' => 'BankAddress',
  'LBL_ACCOUNTS_TTTT_HISTORY_CALLS_1_FROM_TTTT_HISTORY_CALLS_TITLE' => 'History Calls',
  'LBL_DATECONVERTED' => 'DateConverted',
  'LBL_OPPORTUNITIESAMOUNT' => 'OpportunitiesAmount',
  'LBL_SERVICETYPE' => 'ServiceType',
  'LBL_ACCOUNTS_BILL_BILLING_1_FROM_BILL_BILLING_TITLE' => 'LBL_ACCOUNTS_BILL_BILLING_1_FROM_BILL_BILLING_TITLE',
  'LBL_AOS_PRODUCTS_ACCOUNTS_1_FROM_AOS_PRODUCTS_TITLE' => 'Products / Services',
  'LBL_ACCOUNTS_PO_PO_1_FROM_PO_PO_TITLE' => 'PO',
  'LBL_BILL_BILLING_ACCOUNTS_FROM_BILL_BILLING_TITLE' => 'Billings',
  'LBL_STATUS_ACTIVITIES' => 'Status Activities',
  'LBL_COMPANY_TYPE' => 'Company Type',
  'LBL_COMPANY_LEGAL' => 'Legal',
  'LBL_COMPANY_LOCATION' => 'Location',
  'LBL_SEX' => 'Sex',
  'LBL_SCANNER' => 'Scanner',
  'LBL_CONSULTANT' => 'Consultant',
  'LBL_RELATIVES' => 'Relatives',
  'LBL_Deposits_Amount' => 'Deposits Amount',
  'LBL_Payments_Amount' => 'Payments Amount',
  'LBL_LIST_AGE' => 'List Age',
  'LBL_SOCIAL_ACCOUNT' => 'Social Account',
  'LBL_OBJECT_CUSTOMER' => 'Object Customer',
  'LBL_COUNTRIES' => 'Countries',
  'LBL_CLASSIFY_ACCOUNT_LEVEL' => 'Classify Level',
  'LBL_IS_SINGLE' => 'Is Singe',
  'LBL_PHOTO' => 'Avatar',
  'LBL_CHARACTER_ERROR' => 'Special character error',
  'LBL_ALREADY-EXISTS' => 'Code already exists',
  'LBL_SAVE' => 'Save',
  'LBL_PICKUP_FT_QTE' => 'Pick up FT Qte',
  'LBL_VIPB' => 'VIP B',
  'LBL_SPONSORSHIP' => 'Sponsorship',
  'LBL_PICKUP_FT_QTE_VISA' => 'Pick up FT Qte + visa sticker',
  'LBL_SEEOFF_FT_QTE' => 'See off FT Qte',
  'LBL_FAST_ENTRY' => 'Fast entry',
  'LBL_FAST_EXIT' => 'Fast exit',
  'LBL_DOMESTIC' => 'Domestic',
  'LBL_ACCOUNTS_AOS_PASSENGERS_1_FROM_AOS_PASSENGERS_TITLE' => 'Agent',
);