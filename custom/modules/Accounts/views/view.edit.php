<?php

require_once 'modules/Accounts/views/view.edit.php';

class CustomAccountsViewEdit extends AccountsViewEdit {

    public function display()
    {
        parent::display();
        ?>
         <script src="include/javascript/custom/view.edit.js"></script>
         <script src="custom/modules/Accounts/js/edit.js"></script>
        <?php
    }
	
	
}