<?php


class ViewQuickViewDetail extends ViewDetail {

    public function preDisplay()
    {
        $metadataFile = $this->getMetaDataFile();
        $this->dv = new DetailView2();
        $this->dv->ss =&  $this->ss;
        $this->dv->view = 'QuickViewDetail';
        $this->dv->setup($this->module, $this->bean, $metadataFile, get_custom_file_if_exists('include/QuickViewDetail/QuickViewDetail.tpl'));
		echo '<link rel="stylesheet" type="text/css"
href="custom/modules/Accounts/css/style.css"></link>';
		//echo '<script type="text/javascript"
//src="custom/modules/Accounts/js/script.js"></script>';
    }
    public function getMetaDataFile()
    {
        return 'custom/modules/Accounts/metadata/quickdetailviewdefs.php';
    }
    public function display()
    {
        $lead = $this->getLeadInfo($this->bean->id);
        $this->bean->status = 'Transaction';
        $this->bean->lead_source = (!empty($lead)) ? $lead['lead_source']: '';

        $opp_amount = $this->getOpportunityAmount($this->bean->id);
        $contact_name = $this->getContactName($lead['contact_id']);

        $this->bean->opportunity_amount = (!empty($opp_amount)) ? $opp_amount : 0;
        $this->bean->contact_name = $contact_name;
		
        parent::display(); // TODO: Change the autogenerated stub
        //We want to display subset of available, panels, so we will call subpanel
        //object directly instead of using sugarview.
        $GLOBALS['focus'] = $this->bean;
		
        require_once('include/SubPanel/SubPanelTiles.php');
        // Force to only show activities, history subpanels
        $_REQUEST['subpanels'] = 'activities,history,opportunities,cases,contacts,documents,account_aos_contracts,account_aos_quotes,account_aos_invoices,bill_billing_accounts';
        $subpanel = new SubPanelTiles($this->bean, $this->module);
       $subpanel_content =  $subpanel->display();
		
     $this->ss->assign('sub_panels', $subpanel_content);
     $this->ss->display('custom/modules/Leads/tpls/SubPanel.tpl');
    }

    public function getLeadInfo($account_id = '')
    {
        if(empty($account_id)) return false;
        $sql = "SELECT lead_source, contact_id FROM leads WHERE deleted=0 AND account_id='{$account_id}'";
        $res = $this->bean->db->query($sql);
        $row = $this->bean->db->fetchRow($res);
        return $row;
    }

    public function getOpportunityAmount($account_id = '')
    {
        if(empty($account_id)) return false;
        $sql = "SELECT SUM(op.amount) as amount FROM opportunities op INNER JOIN accounts_opportunities ao ON op.id = ao.opportunity_id AND ao.deleted=0
                WHERE op.deleted=0 AND ao.account_id='{$account_id}' AND op.sales_stage = 'Closed Won'";
        return $this->bean->db->getOne($sql);
    }

    function getContactName($id = '')
    {
        if(empty($id)) return '';
        $contact = new Contact();
        $contact->retrieve($id);
        return $contact->full_name;
    }
	
	
}