<?php
$dashletData['AccountsDashlet']['searchFields'] = array (
  'account_code' => 
  array (
    'default' => '',
  ),
  'name' => 
  array (
    'default' => '',
  ),
);
$dashletData['AccountsDashlet']['columns'] = array (
  'account_code' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ACCOUNT_CODE',
    'width' => '10%',
    'default' => true,
    'name' => 'account_code',
  ),
  'name' => 
  array (
    'width' => '40%',
    'label' => 'LBL_LIST_ACCOUNT_NAME',
    'link' => true,
    'default' => true,
    'name' => 'name',
  ),
  'email1' => 
  array (
    'width' => '8%',
    'label' => 'LBL_EMAIL_ADDRESS_PRIMARY',
    'name' => 'email1',
    'default' => true,
  ),
  'phone_alternate' => 
  array (
    'width' => '8%',
    'label' => 'LBL_OTHER_PHONE',
    'name' => 'phone_alternate',
    'default' => true,
  ),
  'assigned_user_name' => 
  array (
    'width' => '8%',
    'label' => 'LBL_LIST_ASSIGNED_USER',
    'name' => 'assigned_user_name',
    'default' => true,
  ),
  'skypename_c' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_SKYPENAME',
    'width' => '10%',
    'name' => 'skypename_c',
  ),
  'company_c' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_COMPANY',
    'width' => '10%',
    'name' => 'company_c',
  ),
  'dateaccount_c' => 
  array (
    'type' => 'date',
    'default' => false,
    'label' => 'LBL_DATEACCOUNT',
    'width' => '10%',
    'name' => 'dateaccount_c',
  ),
  'tax_code' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_TAX_CODE',
    'width' => '10%',
    'default' => false,
    'name' => 'tax_code',
  ),
  'status_activites' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_STATUS_ACTIVITIES',
    'width' => '10%',
    'default' => false,
    'name' => 'status_activites',
  ),
  'ownership' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_OWNERSHIP',
    'width' => '10%',
    'default' => false,
    'name' => 'ownership',
  ),
  'phone_fax' => 
  array (
    'width' => '8%',
    'label' => 'LBL_PHONE_FAX',
    'name' => 'phone_fax',
    'default' => false,
  ),
  'primarycontact_c' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_PRIMARYCONTACT',
    'width' => '10%',
    'name' => 'primarycontact_c',
  ),
  'lead_source' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_LIST_LEAD_SOURCE',
    'width' => '10%',
    'default' => false,
    'name' => 'lead_source',
  ),
  'website' => 
  array (
    'width' => '8%',
    'label' => 'LBL_WEBSITE',
    'default' => false,
    'name' => 'website',
  ),
  'type_c' => 
  array (
    'type' => 'multienum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_TYPE',
    'width' => '10%',
    'name' => 'type_c',
  ),
  'industry' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_INDUSTRY',
    'width' => '10%',
    'default' => false,
    'name' => 'industry',
  ),
  'description' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
    'name' => 'description',
  ),
  'phone_office' => 
  array (
    'width' => '15%',
    'label' => 'LBL_LIST_PHONE',
    'default' => false,
    'name' => 'phone_office',
  ),
  'parent_name' => 
  array (
    'width' => '15%',
    'label' => 'LBL_MEMBER_OF',
    'sortable' => false,
    'name' => 'parent_name',
    'default' => false,
  ),
  'birthdate_c' => 
  array (
    'type' => 'date',
    'default' => false,
    'label' => 'LBL_BIRTHDATE',
    'width' => '10%',
    'name' => 'birthdate_c',
  ),
  'date_entered' => 
  array (
    'width' => '15%',
    'label' => 'LBL_DATE_ENTERED',
    'name' => 'date_entered',
    'default' => false,
  ),
  'date_modified' => 
  array (
    'width' => '15%',
    'label' => 'LBL_DATE_MODIFIED',
    'name' => 'date_modified',
    'default' => false,
  ),
  'created_by' => 
  array (
    'width' => '8%',
    'label' => 'LBL_CREATED',
    'name' => 'created_by',
    'default' => false,
  ),
);
