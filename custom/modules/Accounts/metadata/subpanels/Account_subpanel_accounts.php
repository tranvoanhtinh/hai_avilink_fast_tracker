<?php
// created: 2019-05-01 18:58:28
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_LIST_ACCOUNT_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '45%',
    'default' => true,
  ),
  'account_code' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_ACCOUNT_CODE',
    'width' => '10%',
    'default' => true,
  ),
  'tax_code' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_TAX_CODE',
    'width' => '10%',
    'default' => true,
  ),
  'leadsource_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_LEADSOURCE',
    'width' => '10%',
  ),
  'phone_office' => 
  array (
    'vname' => 'LBL_LIST_PHONE',
    'width' => '20%',
    'default' => true,
  ),
  'edit_button' => 
  array (
    'vname' => 'LBL_EDIT_BUTTON',
    'widget_class' => 'SubPanelEditButton',
    'width' => '4%',
    'default' => true,
  ),
  'remove_button' => 
  array (
    'vname' => 'LBL_REMOVE',
    'widget_class' => 'SubPanelRemoveButtonAccount',
    'width' => '4%',
    'default' => true,
  ),
);