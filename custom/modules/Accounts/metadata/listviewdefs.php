<?php
$listViewDefs ['Accounts'] = 
array (
  'BUSSINESS_TYPE_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_BUSSINESS_TYPE',
    'width' => '10%',
  ),
  'ACCOUNT_CODE' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ACCOUNT_CODE',
    'width' => '10%',
    'default' => true,
  ),
  'NAME' => 
  array (
    'width' => '20%',
    'label' => 'LBL_LIST_ACCOUNT_NAME',
    'link' => true,
    'default' => true,
  ),
  'BILLING_ADDRESS_STREET' => 
  array (
    'width' => '15%',
    'label' => 'LBL_BILLING_ADDRESS_STREET',
    'default' => true,
  ),
  'PRIMARYCONTACT_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_PRIMARYCONTACT',
    'width' => '10%',
  ),
  'DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '10%',
    'label' => 'LBL_LIST_ASSIGNED_USER',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
  'AOS_PRODUCTS_ACCOUNTS_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_AOS_PRODUCTS_ACCOUNTS_1_FROM_AOS_PRODUCTS_TITLE',
    'id' => 'AOS_PRODUCTS_ACCOUNTS_1AOS_PRODUCTS_IDA',
    'width' => '10%',
    'default' => false,
  ),
  'AOS_PRODUCT_CATEGORIES_ACCOUNTS_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_AOS_PRODUCT_CATEGORIES_ACCOUNTS_1_FROM_AOS_PRODUCT_CATEGORIES_TITLE',
    'id' => 'AOS_PRODUCT_CATEGORIES_ACCOUNTS_1AOS_PRODUCT_CATEGORIES_IDA',
    'width' => '10%',
    'default' => false,
  ),
  'PHOTO' => 
  array (
    'type' => 'image',
    'width' => '10%',
    'studio' => 
    array (
      'listview' => true,
    ),
    'label' => 'LBL_PHOTO',
    'default' => false,
  ),
  'ACCOUNT_TYPE' => 
  array (
    'width' => '10%',
    'label' => 'LBL_TYPE',
    'default' => false,
  ),
  'STATUS_ACTIVITES' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_STATUS_ACTIVITIES',
    'width' => '10%',
    'default' => false,
  ),
  'CAMPAIGN_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CAMPAIGN',
    'id' => 'CAMPAIGN_ID',
    'width' => '10%',
    'default' => false,
  ),
  'OPPORTUNITY_AMOUNT' => 
  array (
    'type' => 'decimal',
    'label' => 'LBL_OPPORTUNITY_AMOUNT',
    'width' => '10%',
    'default' => false,
  ),
  'CONSULTANT' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_CONSULTANT',
    'width' => '10%',
    'default' => false,
  ),
  'IS_SINGLE' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'LBL_IS_SINGLE',
    'width' => '10%',
  ),
  'OBJECT_CUSTOMER' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_OBJECT_CUSTOMER',
    'width' => '10%',
    'default' => false,
  ),
  'LIST_AGE' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_LIST_AGE',
    'width' => '10%',
    'default' => false,
  ),
  'DEPOSITSAMOUNT' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_Deposits_Amount',
    'currency_format' => true,
    'width' => '10%',
    'default' => false,
  ),
  'PAYMENTSAMOUNT' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_Payments_Amount',
    'currency_format' => true,
    'width' => '10%',
    'default' => false,
  ),
  'PROJECTNAME_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_PROJECTNAME',
    'width' => '10%',
  ),
  'OPPORTUNITYAMOUNT' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_OPPORTUNITY_AMOUNT',
    'currency_format' => true,
    'width' => '10%',
    'default' => false,
  ),
  'SOCIAL_ACCOUNT' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_SOCIAL_ACCOUNT',
    'width' => '10%',
    'default' => false,
  ),
  'COUNTRIES' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_COUNTRIES',
    'width' => '10%',
    'default' => false,
  ),
  'CLASSIFY_ACCOUNT_LEVEL' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_CLASSIFY_ACCOUNT_LEVEL',
    'width' => '10%',
    'default' => false,
  ),
  'TAX_CODE' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_TAX_CODE',
    'width' => '10%',
    'default' => false,
  ),
  'SERVICETYPE_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_SERVICETYPE',
    'width' => '10%',
  ),
  'DATETRANSACTION_C' => 
  array (
    'type' => 'date',
    'default' => false,
    'label' => 'LBL_DATETRANSACTION',
    'width' => '10%',
  ),
  'COMPANY_LEGAL' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_COMPANY_LEGAL',
    'width' => '10%',
    'default' => false,
  ),
  'DATECONVERTED_C' => 
  array (
    'type' => 'date',
    'default' => false,
    'label' => 'LBL_DATECONVERTED',
    'width' => '10%',
  ),
  'COMPANY_LOCATION' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_COMPANY_LOCATION',
    'width' => '10%',
    'default' => false,
  ),
  'RELATIVES' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_RELATIVES',
    'width' => '10%',
    'default' => false,
  ),
  'COMPANY_TYPE' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_COMPANY_TYPE',
    'width' => '10%',
    'default' => false,
  ),
  'SCANNER' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_SCANNER',
    'width' => '10%',
    'default' => false,
  ),
  'LEAD_ACCOUNT_STATUS_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_LEAD_ACCOUNT_STATUS',
    'width' => '10%',
  ),
  'SEX' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_SEX',
    'width' => '10%',
    'default' => false,
  ),
  'BIRTHDATE_C' => 
  array (
    'type' => 'date',
    'default' => false,
    'label' => 'LBL_BIRTHDATE',
    'width' => '10%',
  ),
  'CLASSIFY_ACCOUNT_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_CLASSIFY_ACCOUNT',
    'width' => '10%',
  ),
  'COMPANY_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_COMPANY',
    'width' => '10%',
  ),
  'LASTDATEACTIVE_C' => 
  array (
    'type' => 'datetimecombo',
    'default' => false,
    'label' => 'LBL_LASTDATEACTIVE',
    'width' => '10%',
  ),
  'DATEACCOUNT_C' => 
  array (
    'type' => 'date',
    'default' => false,
    'label' => 'LBL_DATEACCOUNT',
    'width' => '10%',
  ),
  'PLANDATEACTIVE_C' => 
  array (
    'type' => 'datetimecombo',
    'default' => false,
    'label' => 'LBL_PLANDATEACTIVE',
    'width' => '10%',
  ),
  'DATE_ENTERED' => 
  array (
    'width' => '5%',
    'label' => 'LBL_DATE_ENTERED',
    'default' => false,
  ),
  'PLANDESCRIPTION_C' => 
  array (
    'type' => 'text',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_PLANDESCRIPTION',
    'sortable' => false,
    'width' => '10%',
  ),
  'IDENTITYCARD_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_IDENTITYCARD',
    'width' => '10%',
  ),
  'LEAD_SOURCE' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_LIST_LEAD_SOURCE',
    'width' => '10%',
    'default' => false,
  ),
  'SIC_CODE' => 
  array (
    'width' => '10%',
    'label' => 'LBL_SIC_CODE',
    'default' => false,
  ),
  'ANNUAL_REVENUE' => 
  array (
    'width' => '10%',
    'label' => 'LBL_ANNUAL_REVENUE',
    'default' => false,
  ),
  'PHONE_FAX' => 
  array (
    'width' => '10%',
    'label' => 'LBL_PHONE_FAX',
    'default' => false,
  ),
  'LEADSOURCE_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_LEADSOURCE',
    'width' => '10%',
  ),
  'PHONE_OFFICE' => 
  array (
    'width' => '10%',
    'label' => 'LBL_LIST_PHONE',
    'default' => false,
  ),
  'CREATED_BY_NAME' => 
  array (
    'width' => '10%',
    'label' => 'LBL_CREATED',
    'default' => false,
  ),
  'BILLING_ADDRESS_COUNTRY' => 
  array (
    'width' => '10%',
    'label' => 'LBL_BILLING_ADDRESS_COUNTRY',
    'default' => false,
  ),
  'PHONE_ALTERNATE' => 
  array (
    'width' => '10%',
    'label' => 'LBL_OTHER_PHONE',
    'default' => false,
  ),
  'BILLING_ADDRESS_CITY' => 
  array (
    'width' => '10%',
    'label' => 'LBL_LIST_CITY',
    'default' => false,
  ),
  'EMAIL1' => 
  array (
    'width' => '15%',
    'label' => 'LBL_EMAIL_ADDRESS',
    'sortable' => false,
    'link' => true,
    'customCode' => '{$EMAIL1_LINK}',
    'default' => false,
  ),
  'INDUSTRY' => 
  array (
    'width' => '10%',
    'label' => 'LBL_INDUSTRY',
    'default' => false,
  ),
  'BILLING_ADDRESS_STATE' => 
  array (
    'width' => '7%',
    'label' => 'LBL_BILLING_ADDRESS_STATE',
    'default' => false,
  ),
  'BILLING_ADDRESS_POSTALCODE' => 
  array (
    'width' => '10%',
    'label' => 'LBL_BILLING_ADDRESS_POSTALCODE',
    'default' => false,
  ),
  'SHIPPING_ADDRESS_STREET' => 
  array (
    'width' => '15%',
    'label' => 'LBL_SHIPPING_ADDRESS_STREET',
    'default' => false,
  ),
  'SHIPPING_ADDRESS_CITY' => 
  array (
    'width' => '10%',
    'label' => 'LBL_SHIPPING_ADDRESS_CITY',
    'default' => false,
  ),
  'SHIPPING_ADDRESS_STATE' => 
  array (
    'width' => '7%',
    'label' => 'LBL_SHIPPING_ADDRESS_STATE',
    'default' => false,
  ),
  'SHIPPING_ADDRESS_POSTALCODE' => 
  array (
    'width' => '10%',
    'label' => 'LBL_SHIPPING_ADDRESS_POSTALCODE',
    'default' => false,
  ),
  'SHIPPING_ADDRESS_COUNTRY' => 
  array (
    'width' => '10%',
    'label' => 'LBL_SHIPPING_ADDRESS_COUNTRY',
    'default' => false,
  ),
  'RATING' => 
  array (
    'width' => '10%',
    'label' => 'LBL_RATING',
    'default' => false,
  ),
  'WEBSITE' => 
  array (
    'width' => '10%',
    'label' => 'LBL_WEBSITE',
    'default' => false,
  ),
  'OWNERSHIP' => 
  array (
    'width' => '10%',
    'label' => 'LBL_OWNERSHIP',
    'default' => false,
  ),
  'EMPLOYEES' => 
  array (
    'width' => '10%',
    'label' => 'LBL_EMPLOYEES',
    'default' => false,
  ),
  'DATE_MODIFIED' => 
  array (
    'width' => '5%',
    'label' => 'LBL_DATE_MODIFIED',
    'default' => false,
  ),
  'SKYPENAME_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_SKYPENAME',
    'width' => '10%',
  ),
  'SHORTNAME_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_SHORTNAME',
    'width' => '10%',
  ),
  'MODIFIED_BY_NAME' => 
  array (
    'width' => '10%',
    'label' => 'LBL_MODIFIED',
    'default' => false,
  ),
  'PARENT_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_MEMBER_OF',
    'id' => 'PARENT_ID',
    'width' => '10%',
    'default' => false,
  ),
  'STATUS' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_STATUS',
    'width' => '10%',
    'default' => false,
  ),
  'STATUSACCOUNT_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_STATUSACCOUNT',
    'width' => '10%',
  ),
  'CONTACT_NAME' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_CONTACT_NAME',
    'width' => '10%',
    'default' => false,
  ),
  'BANKNAME_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_BANKNAME',
    'width' => '10%',
  ),
  'BANKADDRESS_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_BANKADDRESS',
    'width' => '10%',
  ),
  'RATINGACCOUNT_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_RATINGACCOUNT',
    'width' => '10%',
  ),
  'TYPE_C' => 
  array (
    'type' => 'multienum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_TYPE',
    'width' => '10%',
  ),
  'BANKACCOUNT_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_BANKACCOUNT',
    'width' => '10%',
  ),
  'STATUSDESCRIPTION_C' => 
  array (
    'type' => 'text',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_STATUSDESCRIPTION',
    'sortable' => false,
    'width' => '10%',
  ),
  'ERP_ID_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_ERP_ID',
    'width' => '10%',
  ),
);
;
?>
