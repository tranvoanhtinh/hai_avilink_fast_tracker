<?php
$popupMeta = array (
    'moduleMain' => 'Account',
    'varName' => 'ACCOUNT',
    'orderBy' => 'name',
    'whereClauses' => array (
  'name' => 'accounts.name',
  'account_code' => 'accounts.account_code',
  'phone_alternate' => 'accounts.phone_alternate',
),
    'searchInputs' => array (
  0 => 'name',
  9 => 'account_code',
  15 => 'phone_alternate',
),
    'create' => array (
  'formBase' => 'AccountFormBase.php',
  'formBaseClass' => 'AccountFormBase',
  'getFormBodyParams' => 
  array (
    0 => '',
    1 => '',
    2 => 'AccountSave',
  ),
  'createButton' => 'LNK_NEW_ACCOUNT',
),
    'searchdefs' => array (
  'account_code' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ACCOUNT_CODE',
    'width' => '10%',
    'name' => 'account_code',
  ),
  'name' => 
  array (
    'name' => 'name',
    'width' => '10%',
  ),
  'phone_alternate' => 
  array (
    'type' => 'phone',
    'label' => 'LBL_PHONE_ALT',
    'width' => '10%',
    'name' => 'phone_alternate',
  ),
),
    'listviewdefs' => array (
  'ACCOUNT_CODE' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ACCOUNT_CODE',
    'width' => '10%',
    'default' => true,
    'name' => 'account_code',
  ),
  'NAME' => 
  array (
    'width' => '40%',
    'label' => 'LBL_LIST_ACCOUNT_NAME',
    'link' => true,
    'default' => true,
    'name' => 'name',
  ),
  'PHONE_ALTERNATE' => 
  array (
    'type' => 'phone',
    'label' => 'LBL_PHONE_ALT',
    'width' => '10%',
    'default' => true,
    'name' => 'phone_alternate',
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '2%',
    'label' => 'LBL_LIST_ASSIGNED_USER',
    'default' => true,
    'name' => 'assigned_user_name',
  ),
),
);
