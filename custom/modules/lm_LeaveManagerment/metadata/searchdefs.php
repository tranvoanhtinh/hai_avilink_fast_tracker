<?php
$module_name = 'lm_LeaveManagerment';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'status' => 
      array (
        'type' => 'enum',
        'studio' => 'visible',
        'label' => 'LBL_STATUS',
        'width' => '10%',
        'default' => true,
        'name' => 'status',
      ),
      'reason' => 
      array (
        'type' => 'enum',
        'studio' => 'visible',
        'label' => 'LBL_REASON',
        'width' => '10%',
        'default' => true,
        'name' => 'reason',
      ),
      'current_user_only' => 
      array (
        'name' => 'current_user_only',
        'label' => 'LBL_CURRENT_USER_FILTER',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
      ),
    ),
    'advanced_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'fromdate' => 
      array (
        'type' => 'date',
        'label' => 'LBL_FROMDATE',
        'width' => '10%',
        'default' => true,
        'name' => 'fromdate',
      ),
      'todate' => 
      array (
        'type' => 'date',
        'label' => 'LBL_TODATE',
        'width' => '10%',
        'default' => true,
        'name' => 'todate',
      ),
      'status' => 
      array (
        'type' => 'enum',
        'studio' => 'visible',
        'label' => 'LBL_STATUS',
        'width' => '10%',
        'default' => true,
        'name' => 'status',
      ),
      'reason' => 
      array (
        'type' => 'enum',
        'studio' => 'visible',
        'label' => 'LBL_REASON',
        'width' => '10%',
        'default' => true,
        'name' => 'reason',
      ),
      'lm_leavemanagerment_cs_cususer_name' => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_LM_LEAVEMANAGERMENT_CS_CUSUSER_FROM_CS_CUSUSER_TITLE',
        'id' => 'LM_LEAVEMANAGERMENT_CS_CUSUSERCS_CUSUSER_IDA',
        'width' => '10%',
        'default' => true,
        'name' => 'lm_leavemanagerment_cs_cususer_name',
      ),
      'assigned_user_id' => 
      array (
        'name' => 'assigned_user_id',
        'label' => 'LBL_ASSIGNED_TO',
        'type' => 'enum',
        'function' => 
        array (
          'name' => 'get_user_array',
          'params' => 
          array (
            0 => false,
          ),
        ),
        'default' => true,
        'width' => '10%',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
;
?>
