<?php
$module_name = 'lm_LeaveManagerment';
$viewdefs [$module_name] = 
array (
  'QuickCreate' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'lm_leavemanagerment_cs_cususer_name',
            'label' => 'LBL_LM_LEAVEMANAGERMENT_CS_CUSUSER_FROM_CS_CUSUSER_TITLE',
          ),
          1 => 'name',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'fromdate',
            'label' => 'LBL_FROMDATE',
          ),
          1 => 
          array (
            'name' => 'todate',
            'label' => 'LBL_TODATE',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'status',
            'studio' => 'visible',
            'label' => 'LBL_STATUS',
          ),
          1 => 
          array (
            'name' => 'reason',
            'studio' => 'visible',
            'label' => 'LBL_REASON',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'coefficient',
            'label' => 'LBL_COEFFICIENT',
          ),
          1 => 'assigned_user_name',
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'description',
            'comment' => 'Full text of the note',
            'label' => 'LBL_DESCRIPTION',
          ),
          1 => 
          array (
            'name' => 'lm_leavemanagerment_aos_products_name',
            'label' => 'LBL_LM_LEAVEMANAGERMENT_AOS_PRODUCTS_FROM_AOS_PRODUCTS_TITLE',
          ),
        ),
      ),
    ),
  ),
);
;
?>
