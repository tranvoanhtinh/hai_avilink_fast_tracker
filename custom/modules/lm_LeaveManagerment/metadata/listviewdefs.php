<?php
$module_name = 'lm_LeaveManagerment';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'FROMDATE' => 
  array (
    'type' => 'date',
    'label' => 'LBL_FROMDATE',
    'width' => '10%',
    'default' => true,
  ),
  'TODATE' => 
  array (
    'type' => 'date',
    'label' => 'LBL_TODATE',
    'width' => '10%',
    'default' => true,
  ),
  'STATUS' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_STATUS',
    'width' => '10%',
    'default' => true,
  ),
  'REASON' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_REASON',
    'width' => '10%',
    'default' => true,
  ),
  'COEFFICIENT' => 
  array (
    'type' => 'decimal',
    'label' => 'LBL_COEFFICIENT',
    'width' => '10%',
    'default' => true,
  ),
  'LM_LEAVEMANAGERMENT_CS_CUSUSER_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_LM_LEAVEMANAGERMENT_CS_CUSUSER_FROM_CS_CUSUSER_TITLE',
    'id' => 'LM_LEAVEMANAGERMENT_CS_CUSUSERCS_CUSUSER_IDA',
    'width' => '10%',
    'default' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
  'MODIFIED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_MODIFIED_NAME',
    'id' => 'MODIFIED_USER_ID',
    'width' => '10%',
    'default' => false,
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => false,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => false,
  ),
  'CREATED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'default' => false,
  ),
  'LM_LEAVEMANAGERMENT_AOS_PRODUCTS_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_LM_LEAVEMANAGERMENT_AOS_PRODUCTS_FROM_AOS_PRODUCTS_TITLE',
    'id' => 'LM_LEAVEMANAGERMENT_AOS_PRODUCTSAOS_PRODUCTS_IDA',
    'width' => '10%',
    'default' => false,
  ),
  'DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
);
;
?>
