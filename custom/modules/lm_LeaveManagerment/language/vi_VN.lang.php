<?php
// created: 2023-05-22 09:55:12
$mod_strings = array (
  'LBL_FROMDATE' => 'Từ ngày',
  'LBL_LM_LEAVEMANAGERMENT_AOS_PRODUCTS_FROM_AOS_PRODUCTS_TITLE' => 'Thợ',
  'LBL_REASON' => 'Lý do',
  'LBL_STATUS' => 'Tình trạng',
  'LBL_TODATE' => 'Đến ngày',
  'LNK_NEW_RECORD' => 'Tạo Quản lý phép',
  'LNK_LIST' => 'Xem Quản lý phép',
  'LNK_IMPORT_LM_LEAVEMANAGERMENT' => 'Nhập Quản lý phép',
  'LBL_LIST_FORM_TITLE' => 'Quản lý phép Danh sách',
  'LBL_SEARCH_FORM_TITLE' => 'Tìm kiếm Quản lý phép',
  'LBL_HOMEPAGE_TITLE' => 'Của tôi Quản lý phép',
'LBL_LM_LEAVEMANAGERMENT_CS_CUSUSER_FROM_CS_CUSUSER_TITLE' => 'Nhân viên',
'LBL_COEFFICIENT' => 'Hệ số nghỉ / tăng ca',
);