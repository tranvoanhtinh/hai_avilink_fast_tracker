<?php
// created: 2024-06-14 03:32:19
$mod_strings = array (
  'LBL_ACCOUNTS' => 'Đại lý',
  'LBL_CAMPAIGN_ACCOUNTS_SUBPANEL_TITLE' => 'Đại lý',
  'LBL_LEADS' => 'Leads',
  'LBL_CAMPAIGN_LEAD_SUBPANEL_TITLE' => 'KH Tiềm năng',
  'LBL_LOG_ENTRIES_LEAD_TITLE' => 'KH Tiềm năng đã tạo',
  'LBL_OPPORTUNITIES' => 'Opportunity',
  'LBL_OPPORTUNITY_SUBPANEL_TITLE' => 'Cơ hội bán hàng',
  'LBL_CONTACTS' => 'Người liên hệ đại lý',
  'LNK_NEW_CAMPAIGN' => 'Tạo chiến dịch (Cơ bản)',
  'LNK_CAMPAIGN_LIST' => 'Xem chiến dịch',
  'LNK_IMPORT_CAMPAIGNS' => 'Nhập chiến dịch',
  'LBL_LIST_FORM_TITLE' => 'Danh sách',
  'LBL_SEARCH_FORM_TITLE' => 'Tìm kiếm',
  'LBL_PROSPECT_LISTS' => 'Prospect Lists',
  'LBL_PROSPECT_LIST_SUBPANEL_TITLE' => 'Danh sách đối tượng',
  'LBL_LOG_ENTRIES_CONTACT_TITLE' => 'Người liên hệ đại lý đã được tạo',
);