<?php
// created: 2024-06-05 06:17:28
$mod_strings = array (
  'LBL_LEADS' => 'Leads',
  'LBL_ACCOUNTS' => 'Đại lý',
  'LBL_CAMPAIGN_ACCOUNTS_SUBPANEL_TITLE' => 'Agent',
  'LBL_CAMPAIGN_LEAD_SUBPANEL_TITLE' => 'Leads',
  'LBL_CONTACTS' => 'Liên hệ',
  'LBL_LOG_ENTRIES_CONTACT_TITLE' => 'Contacts Created',
  'LBL_OPPORTUNITIES' => 'Opportunities',
  'LBL_OPPORTUNITY_SUBPANEL_TITLE' => 'Opportunities',
  'LNK_NEW_CAMPAIGN' => 'Create Campaign (Classic)',
  'LNK_CAMPAIGN_LIST' => 'View Campaign',
  'LNK_IMPORT_CAMPAIGNS' => 'Import Campaign',
  'LBL_LIST_FORM_TITLE' => 'Campaign List',
  'LBL_SEARCH_FORM_TITLE' => 'Campaign Search',
  'LBL_TOP_CAMPAIGNS' => 'Top Campaigns',
  'LBL_MODULE_NAME' => 'Campaign',
  'LBL_PROSPECT_LISTS' => 'Prospect Lists',
  'LBL_PROSPECT_LIST_SUBPANEL_TITLE' => 'Target List',
);