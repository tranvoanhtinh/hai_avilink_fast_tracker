<?php
// created: 2024-06-17 10:00:40
$mod_strings = array (
  'LBL_NAME' => 'Booking number',
  'LBL_TYPE_BOOKING' => 'Booking type',
  'LBL_STATION' => 'Station',
  'LBL_AIRPORT' => 'Airport',
  'LBL_BOOKING_DATE' => 'Booking date',
  'LBL_TYPE_SEVICE' => 'Type service',
  'LBL_ACCOUNTS_AOS_BOOKINGS_1_FROM_ACCOUNTS_TITLE' => 'Agent',
  'LBL_LINE_ITEMS' => 'Passengers',
  'LBL_OTHER_SEVICE' => 'Other service',
  'LBL_EXPORT_EXCEL' => 'Print Excel',
  'LBL_CONTACTS_AOS_BOOKINGS_1_FROM_CONTACTS_TITLE' => 'Contact',
  'LBL_AOS_PRODUCTS_AOS_BOOKINGS_1_FROM_AOS_PRODUCTS_TITLE' => 'Service',
  'LBL_DATE_OF_SERVICE' => 'Date of service',
  'LBL_TEST_DATE_TIME' => 'Test date time',
  'LBL_DATE_OF_SERVICE2'=>'Date of service',
  'LBL_EXCHANGE_RATE' => 'Exchange rate',
  'LBL_INVOICE_ISSUED' => 'Invoice status',
  'LBL_INVOICE_NO'=> 'Invoice No.',
  'LBL_CHARACTER_ERROR' => 'Special character error',
  'LBL_ALREADY-EXISTS' => 'Code already exists',
);