<?php
$module_name = 'AOS_Bookings';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'BOOKING_DATE' => 
  array (
    'type' => 'date',
    'label' => 'LBL_BOOKING_DATE',
    'width' => '10%',
    'default' => true,
  ),
  'DATE_OF_SERVICE' => 
  array (
    'type' => 'date',
    'label' => 'LBL_DATE_OF_SERVICE',
    'width' => '10%',
    'default' => true,
  ),
  'TYPE_BOOKING' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_TYPE_BOOKING',
    'width' => '10%',
    'default' => true,
  ),
  'STATION' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_STATION',
    'width' => '10%',
    'default' => true,
  ),
  'AIRPORT' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_AIRPORT',
    'width' => '10%',
    'default' => true,
  ),
  'ACCOUNTS_AOS_BOOKINGS_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_ACCOUNTS_AOS_BOOKINGS_1_FROM_ACCOUNTS_TITLE',
    'id' => 'ACCOUNTS_AOS_BOOKINGS_1ACCOUNTS_IDA',
    'width' => '10%',
    'default' => true,
  ),
  'BOOKING_STATUS' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_BOOKING_STATUS',
    'width' => '10%',
    'default' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
  'CONTACT' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_CONTACT',
    'width' => '10%',
    'default' => false,
  ),
  'TYPE_SERVICE' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_TYPE_SEVICE',
    'width' => '10%',
    'default' => false,
  ),
  'INVOICE_ISSUED' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_INVOICE_ISSUED',
    'width' => '10%',
    'default' => false,
  ),
  'INVOICE_NO' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_INVOICE_NO',
    'width' => '10%',
    'default' => false,
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => false,
  ),
  'CONTACTS_AOS_BOOKINGS_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CONTACTS_AOS_BOOKINGS_1_FROM_CONTACTS_TITLE',
    'id' => 'CONTACTS_AOS_BOOKINGS_1CONTACTS_IDA',
    'width' => '10%',
    'default' => false,
  ),
  'EXCHANGE_RATE' => 
  array (
    'type' => 'currency',
    'default' => false,
    'label' => 'LBL_EXCHANGE_RATE',
    'currency_format' => true,
    'width' => '10%',
  ),
  'CREATED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'default' => false,
  ),
  'MODIFIED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_MODIFIED_NAME',
    'id' => 'MODIFIED_USER_ID',
    'width' => '10%',
    'default' => false,
  ),
  'LINE_ITEMS' => 
  array (
    'type' => 'function',
    'label' => 'LBL_LINE_ITEMS',
    'width' => '10%',
    'default' => false,
  ),
  'DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'OTHER_SERVICE' => 
  array (
    'type' => 'text',
    'label' => 'LBL_OTHER_SEVICE',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'AOS_PRODUCTS_AOS_BOOKINGS_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_AOS_PRODUCTS_AOS_BOOKINGS_1_FROM_AOS_PRODUCTS_TITLE',
    'id' => 'AOS_PRODUCTS_AOS_BOOKINGS_1AOS_PRODUCTS_IDA',
    'width' => '10%',
    'default' => false,
  ),
);
;
?>
