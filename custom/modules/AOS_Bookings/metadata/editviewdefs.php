<?php
$module_name = 'AOS_Bookings';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => '',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'booking_date',
            'label' => 'LBL_BOOKING_DATE',
          ),
          1 => 
          array (
            'name' => 'date_of_service2',
            'label' => 'LBL_DATE_OF_SERVICE2',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'accounts_aos_bookings_1_name',
            'label' => 'LBL_ACCOUNTS_AOS_BOOKINGS_1_FROM_ACCOUNTS_TITLE',
          ),
          1 => 
          array (
            'name' => 'contact',
            'label' => 'LBL_CONTACT',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'type_booking',
            'studio' => true,
            'label' => 'LBL_TYPE_BOOKING',
          ),
          1 => 
          array (
            'name' => 'type_service',
            'studio' => true,
            'label' => 'LBL_TYPE_SEVICE',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'station',
            'studio' => true,
            'label' => 'LBL_STATION',
          ),
          1 => 
          array (
            'name' => 'airport',
            'studio' => true,
            'label' => 'LBL_AIRPORT',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'aos_products_aos_bookings_1_name',
            'label' => 'LBL_AOS_PRODUCTS_AOS_BOOKINGS_1_FROM_AOS_PRODUCTS_TITLE',
          ),
          1 => 'assigned_user_name',
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'booking_status',
            'studio' => true,
            'label' => 'LBL_BOOKING_STATUS',
          ),
          1 => 
          array (
            'name' => 'invoice_issued',
            'label' => 'LBL_INVOICE_ISSUED',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'exchange_rate',
            'label' => 'LBL_EXCHANGE_RATE',
          ),
          1 => 
          array (
            'name' => 'invoice_no',
            'label' => 'LBL_INVOICE_NO',
          ),
        ),
        8 => 
        array (
          0 => 'description',
          1 => '',
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'line_items',
            'label' => 'LBL_LINE_ITEMS',
          ),
        ),
      ),
    ),
  ),
);
;
?>
