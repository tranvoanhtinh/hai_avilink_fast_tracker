<?php
$dashletData['AOS_BookingsDashlet']['searchFields'] = array (
  'name' => 
  array (
    'default' => '',
  ),
  'accounts_aos_bookings_1_name' => 
  array (
    'default' => '',
  ),
  'type_booking' => 
  array (
    'default' => '',
  ),
  'assigned_user_id' => 
  array (
    'default' => '',
  ),
);
$dashletData['AOS_BookingsDashlet']['columns'] = array (
  'name' => 
  array (
    'width' => '40%',
    'label' => 'LBL_LIST_NAME',
    'link' => true,
    'default' => true,
    'name' => 'name',
  ),
  'accounts_aos_bookings_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_ACCOUNTS_AOS_BOOKINGS_1_FROM_ACCOUNTS_TITLE',
    'id' => 'ACCOUNTS_AOS_BOOKINGS_1ACCOUNTS_IDA',
    'width' => '10%',
    'default' => true,
    'name' => 'accounts_aos_bookings_1_name',
  ),
  'type_booking' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_TYPE_BOOKING',
    'width' => '10%',
    'default' => true,
    'name' => 'type_booking',
  ),
  'date_of_service' => 
  array (
    'type' => 'datetimecombo',
    'default' => true,
    'label' => 'LBL_DATE_OF_SERVICE',
    'width' => '10%',
    'name' => 'date_of_service',
  ),
  'assigned_user_name' => 
  array (
    'width' => '8%',
    'label' => 'LBL_LIST_ASSIGNED_USER',
    'name' => 'assigned_user_name',
    'default' => true,
  ),
  'date_modified' => 
  array (
    'width' => '15%',
    'label' => 'LBL_DATE_MODIFIED',
    'name' => 'date_modified',
    'default' => false,
  ),
  'created_by' => 
  array (
    'width' => '8%',
    'label' => 'LBL_CREATED',
    'name' => 'created_by',
    'default' => false,
  ),
  'station' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_STATION',
    'width' => '10%',
    'default' => false,
    'name' => 'station',
  ),
  'date_entered' => 
  array (
    'width' => '15%',
    'label' => 'LBL_DATE_ENTERED',
    'default' => false,
    'name' => 'date_entered',
  ),
  'line_items' => 
  array (
    'type' => 'function',
    'label' => 'LBL_LINE_ITEMS',
    'width' => '10%',
    'default' => false,
    'name' => 'line_items',
  ),
);
