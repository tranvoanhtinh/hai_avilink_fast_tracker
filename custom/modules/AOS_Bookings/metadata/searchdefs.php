<?php
$module_name = 'AOS_Bookings';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      0 => 'name',
      1 => 
      array (
        'name' => 'current_user_only',
        'label' => 'LBL_CURRENT_USER_FILTER',
        'type' => 'bool',
      ),
    ),
    'advanced_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'accounts_aos_bookings_1_name' => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_ACCOUNTS_AOS_BOOKINGS_1_FROM_ACCOUNTS_TITLE',
        'id' => 'ACCOUNTS_AOS_BOOKINGS_1ACCOUNTS_IDA',
        'width' => '10%',
        'default' => true,
        'name' => 'accounts_aos_bookings_1_name',
      ),
      'booking_date' => 
      array (
        'type' => 'date',
        'label' => 'LBL_BOOKING_DATE',
        'width' => '10%',
        'default' => true,
        'name' => 'booking_date',
      ),
      'date_of_service' => 
      array (
        'type' => 'datetimecombo',
        'default' => true,
        'label' => 'LBL_DATE_OF_SERVICE',
        'width' => '10%',
        'name' => 'date_of_service',
      ),
      'type_booking' => 
      array (
        'type' => 'enum',
        'label' => 'LBL_TYPE_BOOKING',
        'width' => '10%',
        'default' => true,
        'name' => 'type_booking',
      ),
      'invoice_no' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_INVOICE_NO',
        'width' => '10%',
        'default' => true,
        'name' => 'invoice_no',
      ),
      'invoice_issued' => 
      array (
        'type' => 'enum',
        'label' => 'LBL_INVOICE_ISSUED',
        'width' => '10%',
        'default' => true,
        'name' => 'invoice_issued',
      ),
      'station' => 
      array (
        'type' => 'enum',
        'label' => 'LBL_STATION',
        'width' => '10%',
        'default' => true,
        'name' => 'station',
      ),
      'airport' => 
      array (
        'type' => 'enum',
        'label' => 'LBL_AIRPORT',
        'width' => '10%',
        'default' => true,
        'name' => 'airport',
      ),
      'contact' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_CONTACT',
        'width' => '10%',
        'default' => true,
        'name' => 'contact',
      ),
      'type_service' => 
      array (
        'type' => 'enum',
        'label' => 'LBL_TYPE_SEVICE',
        'width' => '10%',
        'default' => true,
        'name' => 'type_service',
      ),
      'booking_status' => 
      array (
        'type' => 'enum',
        'label' => 'LBL_BOOKING_STATUS',
        'width' => '10%',
        'default' => true,
        'name' => 'booking_status',
      ),
      'assigned_user_id' => 
      array (
        'name' => 'assigned_user_id',
        'label' => 'LBL_ASSIGNED_TO',
        'type' => 'enum',
        'function' => 
        array (
          'name' => 'get_user_array',
          'params' => 
          array (
            0 => false,
          ),
        ),
        'default' => true,
        'width' => '10%',
      ),
      'current_user_only' => 
      array (
        'label' => 'LBL_CURRENT_USER_FILTER',
        'type' => 'bool',
        'width' => '10%',
        'default' => true,
        'name' => 'current_user_only',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
;
?>
