<?php

require_once('include/MVC/View/views/view.detail.php');

class CustomAOS_BookingsViewDetail extends ViewDetail
{
    function display()
    {
        parent::display(); // Gọi phương thức hiển thị từ lớp cha
         ?>

<style type="text/css">
    .bold-text {
        font-weight: bold;
    }
</style>
<table style="display: none;" id="data-table">
    <tr><td></td></tr>
    <tr><td></td></tr>

    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td id = 'booking_code'></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td><img src="themes/SuiteP/images/company_logo.png"></td>

    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td id ='agent_booking'></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td id = 'date_booking'></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td id ='Contact_information'></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
</table>
<script src="custom/modules/AOS_Bookings/js/FileSaver.min.js"></script>
<script src="custom/modules/AOS_Bookings/js/exceljs.min.js"></script>
<script src="custom/modules/AOS_Bookings/js/detail.js"></script>

<?php
    }
}
