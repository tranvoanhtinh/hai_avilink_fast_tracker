// Tạo nút "Print Excel" và thêm vào thanh điều hướng
var currentURL = window.location.href;
var url = new URL(currentURL);
var recordId = url.searchParams.get("record");
var newLi1 = document.createElement("li");
newLi1.classList.add("nav-item");
var btnPrintexcel = document.createElement("a");
btnPrintexcel.classList.add("nav-link");
btnPrintexcel.innerHTML = SUGAR.language.get(module_sugar_grp1, "LBL_EXPORT_EXCEL");
newLi1.appendChild(btnPrintexcel);
btnPrintexcel.style.backgroundColor = "#3cd73c";
// btnPrintexcel.style.fontSize ="14px";
var ul = document.querySelector(".nav.nav-tabs");
ul.appendChild(newLi1);

// Thêm sự kiện nhấp vào nút để xuất bảng ra tệp Excel
btnPrintexcel.onclick = function () {
    exportToExcel();
};

var sourceTable = document.getElementById("table_addpassengers");
var destinationTable = document.getElementById("data-table");
var children = sourceTable.children;
for (var i = 0; i < children.length; i++) {
    destinationTable.appendChild(children[i].cloneNode(true));
}

document.getElementById('booking_code').textContent = document.getElementById('name').textContent;
document.getElementById('date_booking').textContent =  document.getElementById('booking_date').textContent;
document.getElementById('Contact_information').textContent =  document.getElementById('contact').textContent;
var account_id = document.getElementById('accounts_aos_bookings_1accounts_ida').getAttribute('data-id-value');
$.ajax({
    type: "POST",
    url: "index.php?entryPoint=get_info_accounts",
    data: {
        status: 'get_account_code',
        account_id: account_id, // Thêm dấu chấm phẩy vào dòng này
    },
    success: function(data) {
        console.log(data);
        document.getElementById('agent_booking').textContent = data;
    },
    error: function(error) {
        console.error('Error data: ', error);
    }
});


async function exportToExcel() {
    // Tạo workbook và worksheet
    var workbook = new ExcelJS.Workbook();
    var worksheet = workbook.addWorksheet('Bookings');

    // Lấy bảng dữ liệu
    var table = document.getElementById('data-table');
    var count = 0;
    // Lặp qua các hàng của bảng
    for (var i = 0, row; row = table.rows[i]; i++) {
        // Tạo một hàng trong worksheet
        var excelRow = worksheet.addRow();

        // Lặp qua các ô trong hàng
        for (var j = 0, cell; cell = row.cells[j]; j++) {
            // Thêm giá trị của ô vào hàng của worksheet
            if (cell.getElementsByTagName('img').length > 0) {
                // Nếu ô chứa hình ảnh, chuyển đổi hình ảnh thành base64
                var img = cell.getElementsByTagName('img')[0];
                await addImageToWorksheet(img, workbook, worksheet, i, j);
            } else {
                excelRow.getCell(j + 1).value = cell.innerText;
                excelRow.getCell(j + 1).font = {name:'Arial', size: 9 };
            }
             count =  i ;
             excelRow.getCell(j + 1).alignment = { vertical: 'middle', horizontal: 'center', wrapText: true};
        }

        worksheet.getRow(i + 1).height = 30; // Thiết lập chiều cao cho hàng (i + 1 vì hàng bắt đầu từ 1 trong Excel)
    
}


    count = count+1;
    if(count == 8){
        worksheet.getRow(8).height = '';
    }
    if(count == 9){
        worksheet.getRow(8).height = 60;
        worksheet.getRow(9).height = 60;
    }
    if(count ==10){
        worksheet.getRow(8).height = 40;
        worksheet.getRow(9).height = 40;
        worksheet.getRow(10).height = 40;
    }

    worksheet.mergeCells('J8:J'+count);
    worksheet.getCell('J8').alignment = { vertical: 'middle', horizontal: 'center', wrapText: true };
    for (var i = 7; i <= count; i++) { // Hàng từ 7 đến 10
        for (var j = 1; j <= 10; j++) { // Cột từ 1 (A) đến 10 (J)
            worksheet.getCell(i, j).border = {
                top: { style: 'thin' },
                left: { style: 'thin' },
                bottom: { style: 'thin' },
                right: { style: 'thin' }
            };
        }
    }
    //note

    var count2 = count + 2; 
    worksheet.mergeCells('A'+ count2 + ':B'+count2);
    var startText = "LƯU Ý /";
    var endText = "NOTE";
    worksheet.getCell('A'+count2).value = {
        richText: [
            { text: startText, font: { color: {argb: 'FF000000' }, bold: true, size: 9 } }, // Phần đầu với chữ đen, đậm, kích thước 14px
            { text: endText, font: { color: {argb: 'FFFF0000' }, bold: true, size: 9 } } // Phần cuối với chữ đỏ, đậm, kích thước 14px
        ]
    };
    worksheet.getCell('A'+count2).fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'FFFF00' } // Mã màu vàng
    };
    worksheet.getCell('A'+count2).font = {name:'Arial'};


    var data = [
        {
            startText: '***',
            endText: 'Thông tin liên hệ: Hotline (24/7) 0962.088.148 / 0934.019.961 Mr Minh / 0931.436.382 Ms Huyền',
            rowHeight: 15,
            color: 'FF000000', // đen
            bold: true,
        },
        {
            startText: '(01)',
            endText: 'Mọi thay đổi thông tin đăng ký (nếu có), hành khách phải báo thông báo cho Avilink trước 06 giờ (hoặc 24 giờ đối với dịch vụ tiễn VIP B) so với Thời điểm phục vụ, kể cả thông báo hủy dịch vụ. Sau mốc thời gian 06 giờ (hoặc 24 giờ đối với dịch vụ tiễn VIP B) so với thời điểm phục vụ, Avilink sẽ bắt đầu tính phí dịch vụ theo booking đã đặt.',
            rowHeight: 30,
            color: 'FF000000', // đen
            bold: false,
        },
        {
            startText: '(01)',
            endText: 'Any changes to the registration information (if applicable) must be reported to Avilink at least 6 hours in advance (or 24 hours for VIP B farewell service) before the Service Time, including service cancellation notifications. After the 6-hour mark (or 24 hours for VIP B farewell service) before the Service Time, Avilink will begin to charge service fees based on the originally booked services.',
            rowHeight: 30,
            color: 'DD0000', // đỏ
            bold: false,
        },
        {
            startText: '(02)',
            endText: 'Trường hợp đến Thời điểm phục vụ, hành khách không sử dụng dịch vụ mà không thông báo hủy trước 06 giờ (hoặc 24 giờ đối với dịch vụ tiễn VIP B), thì Avilink vẫn tính phí dịch vụ theo booking đã đặt trước đó;',
            rowHeight: 15,
            color: 'FF000000',
            bold: false, // đỏ
        },
        {
            startText: '(02)',
            endText: 'In the event that, at the Service Time, passenger does not use the service without prior cancellation notification at least 6 hours in advance (or 24 hours for VIP B drop off service), Avilink will still charge service fees based on the previously booked services.',
            rowHeight: 30,
            color: 'DD0000',
            bold: false, // đỏ
        }, 
        {
            startText: '(03)',
            endText: 'Trường hợp đến Thời điểm phục vụ, hành khách không phối hợp khiến nhân viên của Avilink không thể đón/gặp được hành khách khi đến sân bay (nếu là dịch vụ tiễn), bước vào khu vực nhập cảnh (nếu là dịch vụ đón chuyến bay quốc tế),… thì Avilink vẫn tính phí dịch vụ theo booking đã đặt trước đó. Các hình thức không phối hợp bao gồm:\n- Không thông báo thay đổi lịch bay;\n- Đến sai thời gian và địa điểm hẹn;\n- Vô tình hoặc cố tình không nhìn thấy nhân viên phục vụ;\n- Từ chối xác nhận danh tính;\n- Từ chối phục vụ.',

            rowHeight: 90,
            color: 'FF000000',
            bold: false, // đỏ
        },
        {
            startText: '(03)',
            endText: 'In the event that, at the Service Time, passenger does not cooperate, causing Avilink\'s staff to be unable to meet the passenger (for farewell services), enter the immigration area (for international arrival services), etc., Avilink will still charge service fees based on the previously booked services. Forms of non-cooperation include:\n- Failure to notify flight schedule changes.\n- Arriving at the wrong time and location.\n- Unintentionally or deliberately not recognizing the service staff.\n- Refusing to confirm identity.\n- Refusing service.',
            rowHeight: 90,
            color: 'DD0000',
            bold: false, // đỏ
        },

        {
            startText: '(04)',
            endText: 'Trường hợp đến Thời điểm phục vụ, Avilink không gặp được hành khách tại điểm hẹn đồng thời Avilink mất liên lạc được với hành khách trong khoản thời gian sau đây thì xem như dịch vụ tự động được hoàn thành:\n- Với dịch vụ tiễn chuyến bay quốc tế: tối đa 60 phút,\n- Với các dịch vụ đón: cho đến khi toàn bộ hành khách và phi hành đoàn của chuyến bay di chuyển vào nhà ga cộng thêm 20 phút tìm kiếm tại các khu vực cấp thị thực tại chổ, khu vực xếp hàng nhập cảnh, khu vực băng chuyền.',
            rowHeight: 60,
            color: 'FF000000',
            bold: false, // đỏ
        }, 
        {
            startText: '(04)',
            endText: 'In the event that, at the Service Time, Avilink does not meet the passenger at the designated location, and Avilink cannot establish contact with the passenger within the following time frames, the service will be considered automatically completed:\n- For international drop off services: a maximum of 60 minutes.\n- For pickup services: until all passengers and the flight crew of the flight have moved to the terminal, plus an additional 20 minutes of searching in the area.',
            rowHeight: 50,
            color: 'DD0000',
            bold: false, // đỏ
        },
       {
            startText: '(05)',
            endText: 'Trường hợp đến Thời điểm phục vụ, hành khách gặp các vấn đề sau đây, thì Avilink vẫn tính phí dịch vụ theo booking đã đặt trước đó:\n- Đến trễ sau thời gian quầy thủ tục đóng cửa; \n- Giấy tờ tùy thân như: CCCD, Passport, Visa, Hồ sơ xuất cảnh, hồ sơ nhập cảnh không hợp pháp, hợp lệ;\n- Vé máy bay không hợp lệ;\n- Hành lý bị vi phạm các quy định về hải quan và an ninh an toàn hàng không;\n- Thuộc diện cấm xuất cảnh, nhập cảnh theo quy định của nhà nước Việt Nam;\n- Can thiệp từ các cơ quan nhà nước;\nAvilink không chịu trách nhiệm về tình trạng vé máy bay của hành khách. Quý đơn vị và hành khách phải chủ động kiểm tra với đơn vị cung cấp vé máy bay trước thời điểm phục vụ.\nAvilink không chịu trách nhiệm về tính hợp pháp, hợp lý của các giấy tờ tùy thân của hành khách: CCCD, Passport, Visa, Hồ sơ xuất cảnh, hồ sơ nhập cảnh,... Quý đơn vị và hành khách phải chủ động kiểm tra hoặc liên hệ với các đơn vị cung cấp các dịch vụ giấy tờ tùy thân để kiểm tra tính hợp pháp, hợp lý trước thời điểm phục vụ.\nAvilink không chịu trách nhiệm về tính hợp pháp, hợp lý của hành lý của hành khách.',
            rowHeight: 135,
            color: '#FF000000',
            bold: false,
        },
        {
            startText: '(05)',
            endText: 'In the event that, at the Service Time, passengers encounter the following issues, Avilink will still charge service fees based on the previously booked services:\n- Arriving late after the check-in counter closing time.\n- Having personal documents such as ID cards, passports, visas, exit profiles, or entry profiles that are invalid or illegal.\n- Holding invalid flight tickets.\n- Having baggage that violates customs and aviation safety and security regulations.\n- Falling under the entry or exit prohibition as per the regulations of the Vietnamese government.\n- State intervention from government agencies.\nAvilink is not responsible for the status of passengers\' flight tickets. Your organization and passengers must proactively check with the flight ticket provider before the service time.\nAvilink is not responsible for the legality and validity of passengers\' personal documents: ID cards, passports, visas, exit profiles, entry profiles, etc. Your organization and passengers must proactively verify or contact the relevant agencies providing these document services to check their legality and validity before the service time.\nAvilink is not responsible for the legality and validity of passengers\' baggage.',
            rowHeight: 135,
            color: 'DD0000',
            bold: false, // đỏ
        },

        {
            startText: '(06)',
            endText: '"Ngoại trừ dịch vụ tiễn VIP B, các dịch vụ khác của Avilink sẽ được diễn ra liên tục, và chỉ bị gián đoạn trong các bước thực hiện các thủ tục hàng không, ví dụ như:\n- Hãng hàng không kiểm tra thông tin vé máy bay. Hoặc,\n- Cơ quan xuất nhập cảnh kiểm tra hồ sơ xuất nhập cảnh. Hoặc,\n- Hải quan kiểm tra hành lý. Hoặc,\n- Hành khách gặp các vấn đề về sức khỏe, cần kiểm tra y tế. Hoặc,\n- Hanh khách có nhu cầu vệ sinh cá nhân, v/v.\nNgoài ra, Avilink không chấp nhận việc hành khách yêu cầu gián đoạn như: \n- Với các dịch vụ tiễn, sau khi checkin ở quầy thủ tục xong, hành khách lại đi ngược ra ngoài xử lý việc cá nhân hơn 20 phút rồi mới quay lại làm các thủ tục xuất cảnh, kiểm soát an ninh. Hoặc,\n- Với dịch vụ đón, sau khi qua bục nhập cảnh ưu tiên xong, khách muốn chờ đợi một ai đó đi cùng chuyến bay, hoặc có việc cá nhân cần xử lý rồi mới lấy hành lý ký gửi ở băng chuyền.\nKhi hành khách có nhu cầu gián đoạn như trên, Avilink sẽ kết thúc dịch vụ và xem như dịch vụ đã hoàn thành."',
            rowHeight: 130,
            color: '#FF000000',
            bold: false,
        },
        {
            startText: '(06)',
            endText: 'Except for the VIP B farewell service, all other Avilink services will proceed continuously and will only be interrupted during the execution of aviation procedures, such as:\n- Airlines checking flight ticket information. Or,\n- Immigration authorities checking exit and entry profiles. Or,\n- Customs inspecting baggage. Or,\n- Passengers encountering health issues requiring medical examination. Or,\n- Passengers needing personal hygiene, ect.\nIn addition, Avilink does not accept passenger requests for interruptions such as:\n- For drop off services, after completing check-in at the check-in counter, passengers leave for personal matters for more than 20 minutes before returning to complete exit procedures and security checks. Or,\n- For pickup services, after passing through the priority immigration counter, passengers want to wait for someone traveling on the same flight or have personal matters to attend to before retrieving checked baggage from the baggage carousel.\nWhen passengers have a need for interruptions as mentioned above, Avilink will terminate the service and consider it as completed.',
            rowHeight: 145,
            color: 'DD0000',
            bold: false, // đỏ
        },
        {
            startText: "(07)",
            endText: "Tại mỗi khu vực làm thủ tục hàng không, sẽ chỉ có một vài quầy/bục dành cho các hành khách được phục vụ ưu tiên, bao gồm: các hành khách được ưu tiên theo chính sách của nhà nước, các hành khách được ưu tiên theo chính sách của hãng hàng không và các hành khách sử dụng dịch mà Avilink cung cấp. Do đó, trong một số thời điểm, hành khách vẫn phải xếp hàng tại các quầy/bục ưu tiên. Tuy nhiên, việc xếp hàng này sẽ chỉ diễn ra trong thời gian ngắn. Và việc xếp hàng này sẽ không là cơ sở để đánh giá chất lượng dịch vụ của Avilink.",
            rowHeight: 40,
            color: "FF000000",
            bold: false,
        },
        {
            startText: "(07)",
            endText: "At each airport aviation procedure area, there will be only a few counters/booths designated for prioritized service passengers, including passengers prioritized according to the state's policy, passengers prioritized according to the airline's policy, and passengers using services provided by Avilink. Therefore, at certain times, passengers may still have to queue at priority counters/booths. However, this queuing will only take place for a short time. This queuing will not be a basis for evaluating the quality of Avilink's services.",
            rowHeight: 40,
            color: "DD0000",
            bold: false,
        },
        {
            startText: '(08)',
            endText: 'Trong suốt quá trình phục vụ, nếu hành khách không được Avilink phục vụ như mô tả (không bao gồm các trường hợp xếp hàng như ở mục 7) thì Avilink sẽ chỉ tính phí dịch vụ cho những phần đã phục cho hành khách hoặc miễn phí toàn bộ booking, tùy vào tình hình thực tế.\nAvilink sẽ hoàn lại hoặc bảo lưu một phần hoặc toàn bộ phí dịch vụ mà Quý đơn vị đã thanh toán trước đó, tùy vào mức độ thiếu sót mà Avilink không thực hiện được trong quá trình phục vụ.',
            rowHeight: 40,
            color: 'FF000000',
            bold: false,
        },
        {
            startText: '(08)',
            endText: 'Throughout the service process, if a passenger is not served by Avilink as described (excluding cases of queuing as in section 7), Avilink will only charge the service fee for the parts that have been provided to the passenger or provide the entire booking for free, depending on the actual situation.\nAvilink will refund or retain a portion or all of the service fees that the Client has paid in advance, depending on the extent of the deficiencies that Avilink could not fulfill during the service.',
            rowHeight: 40,
            color: "DD0000",
            bold: false,
        },
        {
            startText: '(10)',
            endText: 'Nếu là chuyến bay đi, hành khách phải có mặt tại sân bay tối thiểu 60 phút trước giờ bay của chặng bay nội địa, trước 120 phút trước giờ bay của chặng bay quốc tế;',
            rowHeight: 15,
            color: 'FF000000',
            bold: false,
        },
        {
            startText: '(10)',
            endText: 'For departing flights, passengers must be at the airport at least 60 minutes before the departure time for domestic flights and at least 120 minutes before the departure time for international flights.',
            rowHeight: 15,
            color: 'DD0000',
            bold: false,
        },
        {
            startText: '(11)',
            endText: 'Vì một số lý do khách quan như thiên tai, dịch bệnh, yêu cầu của các cơ quan chức năng sân bay, cơ quan nhà nước,... khiến dịch vụ không thể thực hiện được, thì Avilink sẽ không tính phí dịch vụ và không chịu bất kỳ trách nhiệm gì.',
            rowHeight: 30,
            color: 'FF000000',
            bold: false,
        },
        {
            startText: '(11)',
            endText: 'Due to some unforeseen circumstances such as natural disasters, pandemics, requirements from airport authorities, government agencies, etc., which make it impossible to provide the service, Avilink will not charge a service fee and will not be held responsible.',
            rowHeight: 30,
            color: 'DD0000',
            bold: false,
        }
    ];

    data.forEach(item => {
        count2++;
        worksheet.getCell('A' + count2).value = { richText: [{ text: item.startText}] };
        worksheet.mergeCells('B' + count2 + ':J' + count2);
        worksheet.getCell('B' + count2).value = { richText: [{ text: item.endText}] };
        worksheet.getCell('A' + count2).font = {name:'Arial', color: { argb: item.color }, size: 9, bold: item.bold };
        worksheet.getCell('B' + count2).font = {name:'Arial', color: { argb: item.color }, size: 9, bold: item.bold };
        worksheet.getCell('B' + count2).alignment = { wrapText: true };
        worksheet.getCell('A' + count2).alignment = { vertical: 'middle', horizontal: 'center' };
        worksheet.getRow(count2).height = item.rowHeight;
    });


    var startText = "BOOKING DỊCH VỤ HỖ TRỢ BAY - ";
    var endText = "AIRPORT FAST TRACK";

    worksheet.getCell('A1').value = {
        richText: [
            { text: startText, font: { color: { argb: 'FF000000' }, bold: true, size: 14 } }, // Phần đầu với chữ đen, đậm, kích thước 14px
            { text: endText, font: { color: { argb: 'FFFF0000' }, bold: true, size: 14 } } // Phần cuối với chữ đỏ, đậm, kích thước 14px
        ]
    };
// Định dạng căn giữa và nền màu cho ô A1
    worksheet.getCell('A1').alignment = { vertical: 'middle', horizontal: 'center' }; // Căn giữa văn bản
    worksheet.getCell('A1').fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'FFFF00' } // Mã màu vàng
    };

// Gộp ô A1:K1
    worksheet.mergeCells('A1:J1');
    worksheet.getRow(1).height = 35;
    worksheet.getRow(2).height = 10;
    worksheet.getRow(3).height = 35;
    worksheet.getRow(4).height = 17;
    worksheet.getRow(5).height = 17;
    worksheet.getRow(6).height = 35;
    worksheet.getRow(7).height = 35;


    var startText = "Số booking: ";
    var endText = "Booking number:";

// Tạo giá trị richText cho ô B3
    worksheet.getCell('B3').value = {
        richText: [
            { text: startText, font: { color: { argb: 'FF000000' }, bold: true, size: 9 } },
            { text: "\n" }, // Tạo dòng mới
            { text: endText, font: { color: { argb: 'FFFF0000' }, size: 9 } } // Phần "Booking number:" với chữ đỏ và đậm
        ]
    };
    worksheet.mergeCells('J2:J6');
    worksheet.mergeCells('B3:C3');
    worksheet.mergeCells('B4:C4');
    worksheet.mergeCells('B5:C5');
    worksheet.mergeCells('B6:C6');
    worksheet.getCell('B3').alignment = {wrapText: true };
    worksheet.mergeCells('D3:I3');
    worksheet.mergeCells('D4:I4');
    worksheet.mergeCells('D5:I5');
    worksheet.mergeCells('D6:I6');
    worksheet.getCell('D3').font = {name:'Arial', bold: true, size: 14 };
    worksheet.getCell('D3').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('D4').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('D5').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('D6').alignment = { vertical: 'middle', horizontal: 'center' };

    var startText = "Người (";
    var midText = "person";
    var endText = ") booking:";

    worksheet.getCell('B4').value = {
        richText: [
            { text: startText, font: { color: { argb: 'FF000000' }, bold: true, size:9 } },
            { text: midText, font: { color: { argb: 'FFFF0000' }, size: 9 } },
            { text: endText, font: { color: { argb: 'FF000000' }, bold: true, size:9 } }
        ]
    };


    var startText = "Ngày (";
    var midText = "date";
    var endText = ") booking:";

    worksheet.getCell('B5').value = {
        richText: [
            { text: startText, font: { color: { argb: 'FF000000' }, bold: true, size: 9 } },
            { text: midText, font: { color: { argb: 'FFFF0000' }, size: 9 } },
            { text: endText, font: { color: { argb: 'FF000000' }, bold: true, size: 9 } }
        ]
    };



    worksheet.getColumn('A').width = 7;
    worksheet.getColumn('B').width = 25;
    worksheet.getColumn('C').width = 15;
    worksheet.getColumn('D').width = 20;
    worksheet.getColumn('E').width = 15;
    worksheet.getColumn('F').width = 10;
    worksheet.getColumn('G').width = 10;
    worksheet.getColumn('H').width = 20;
    worksheet.getColumn('I').width = 25;
    worksheet.getColumn('J').width = 25;

    var startText = "Thông tin liên hệ:";
    var endText = "Contact information: ";

// Tạo giá trị richText cho ô B6
    worksheet.getCell('B6').value = {
        richText: [
            { text: startText, font: { color: { argb: 'FF000000' }, bold: true, size: 9 } },
            { text: "\n" }, // Tạo dòng mới
            { text: endText, font: { color: { argb: 'FFFF0000' }, size: 9 } } // Phần "Contact information:" với chữ đỏ và kích thước 12px
        ]
    };
    worksheet.getCell('B6').alignment = { wrapText: true };



    var startText = "STT";
    var endText = "NO.";
    worksheet.getCell('A7').value = {
        richText: [
            { text: startText, font: { color: { argb: 'FF000000' }, size: 9, bold: true } },
            { text: "\n" }, // Tạo dòng mới
            { text: endText, font: { color: { argb: 'FFFF0000' }, size: 9 } } // Phần "Contact information:" với chữ đỏ và kích thước 12px
        ]
    };

    worksheet.getCell('C7').font = {name:'Arial' ,size:9, bold: true };

    var startText = "Tên hành khách";
    var endText = "Passenger's name";

// Tạo giá trị richText cho ô B6
    worksheet.getCell('B7').value = {
        richText: [
            { text: startText, font: { color: { argb: 'FF000000' }, size: 9, bold: true } },
            { text: "\n" }, // Tạo dòng mới
            { text: endText, font: { color: { argb: 'FFFF0000' }, size: 9 , bold: true} } // Phần "Contact information:" với chữ đỏ và kích thước 12px
        ]
    };
// Thiết lập căn giữa, wrap text và chữ in đậm cho dòng số 7
    worksheet.getRow(7).alignment = { vertical: 'middle', horizontal: 'center', wrapText: true };



    var startText = "Quốc tịch";
    var endText = "Nationality";
    worksheet.getCell('D7').value = {
        richText: [
            { text: startText, font: { color: { argb: 'FF000000' }, size: 9, bold: true } },
            { text: "\n" }, // Tạo dòng mới
            { text: endText, font: { color: { argb: 'FFFF0000' }, size: 9 , bold: true} } // Phần "Contact information:" với chữ đỏ và kích thước 12px
        ]
    };

    var startText = "Chuyến bay";
    var endText = "Flight";
    worksheet.getCell('E7').value = {
        richText: [
            { text: startText, font: { color: { argb: 'FF000000' }, size: 9, bold: true } },
            { text: "\n" }, // Tạo dòng mới
            { text: endText, font: { color: { argb: 'FFFF0000' }, size: 9 , bold: true} } // Phần "Contact information:" với chữ đỏ và kích thước 12px
        ]
    };

    var startText = "Từ";
    var endText = "From";
    worksheet.getCell('F7').value = {
        richText: [
            { text: startText, font: { color: { argb: 'FF000000' }, size: 9, bold: true } },
            { text: "\n" }, // Tạo dòng mới
            { text: endText, font: { color: { argb: 'FFFF0000' }, size: 9 , bold: true} } // Phần "Contact information:" với chữ đỏ và kích thước 12px
        ]
    };

    var startText = "Đến";
    var endText = "To";
    worksheet.getCell('G7').value = {
        richText: [
            { text: startText, font: { color: { argb: 'FF000000' }, size: 9, bold: true } },
            { text: "\n" }, // Tạo dòng mới
            { text: endText, font: { color: { argb: 'FFFF0000' }, size: 9 , bold: true} } // Phần "Contact information:" với chữ đỏ và kích thước 12px
        ]
    };



    var startText = "Ngày bay đáp";
    var endText = "STD / STA";
    worksheet.getCell('H7').value = {
        richText: [
            { text: startText, font: { color: { argb: 'FF000000' }, size: 9, bold: true } },
            { text: "\n" }, // Tạo dòng mới
            { text: endText, font: { color: { argb: 'FFFF0000' }, size: 9 , bold: true} } // Phần "Contact information:" với chữ đỏ và kích thước 12px
        ]
    };


    var startText = "Mã đặt chỗ";
    var endText = "PNR";
    worksheet.getCell('I7').value = {
        richText: [
            { text: startText, font: { color: { argb: 'FF000000' }, size: 9, bold: true } },
            { text: "\n" }, // Tạo dòng mới
            { text: endText, font: { color: { argb: 'FFFF0000' }, size: 9 , bold: true} } // Phần "Contact information:" với chữ đỏ và kích thước 12px
        ]
    };


var startText = "Yêu cầu phục vụ";
var endText = "Request services";
worksheet.getCell('J7').value = {
    richText: [
        { text: startText, font: { name: 'Arial', color: { argb: 'FF000000' }, size: 9, bold: true } },
        { text: "\n" }, // Tạo dòng mới
        { text: endText, font: { name: 'Arial', color: { argb: 'FFFF0000' }, size: 9, bold: true } }
    ]
};

    worksheet.getCell('C4').alignment = { vertical: 'middle', horizontal: 'left' };
    worksheet.getCell('C5').alignment = { vertical: 'middle', horizontal: 'left' };
    worksheet.getCell('B3').alignment = { vertical: 'middle', horizontal: 'left', wrapText: true };
    worksheet.getCell('B6').alignment = { vertical: 'middle', horizontal: 'left', wrapText: true };


    // Lưu workbook thành file Excel
    workbook.xlsx.writeBuffer().then(function(buffer) {
        var blob = new Blob([buffer], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
        name = document.getElementById('name').textContent;
        saveAs(blob, name+'.xlsx');
    });
}

async function addImageToWorksheet(img, workbook, worksheet, row, col) {
    return new Promise((resolve, reject) => {
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext('2d');
        var image = new Image();
        image.crossOrigin = 'Anonymous';
        image.src = img.src;

        image.onload = function() {
            canvas.width = image.width;
            canvas.height = image.height;
            ctx.drawImage(image, 0, 0);
            var dataURL = canvas.toDataURL('image/png');
            var imageId = workbook.addImage({
                base64: dataURL,
                extension: 'png',
            });

            // Kích thước mong muốn cho hình ảnh
            var targetWidth = 150; // Chiều rộng mong muốn (px)
            var targetHeight = 100; // Chiều cao mong muốn (px)

            // Tính toán tỷ lệ giữ nguyên tỷ lệ của hình ảnh
            var aspectRatio = image.width / image.height;
            if (targetWidth / targetHeight > aspectRatio) {
                targetWidth = targetHeight * aspectRatio;
            } else {
                targetHeight = targetWidth / aspectRatio;
            }

            worksheet.addImage(imageId, {
                tl: { col: col, row: row },
                ext: { width: targetWidth, height: targetHeight }
            });
            resolve();
        };

        image.onerror = function() {
            reject(new Error('Could not load image'));
        };
    });
}

//=========================