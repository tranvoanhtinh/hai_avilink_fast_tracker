
var date_of_service2 = document.getElementById('date_of_service2');
if (date_of_service2.value === '') {
    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var year = now.getFullYear();
    var hours = ("0" + now.getHours()).slice(-2);
    var minutes = ("0" + now.getMinutes()).slice(-2);
    var currentDateTime = day + '-' + month + '-' + year + ' ' + hours + ':' + minutes;
    
    date_of_service2.value = currentDateTime;
    var dateInputs = document.querySelectorAll('input.date-time');
    dateInputs.forEach(function(input) {
        input.value = currentDateTime;
    });
    
}

var check = false;
var first_code = '';
var type_code = '';
var end_code = '';
var date_code = '';
window.onload = function() {
    // Lấy phần tử có ID là 'name'
    var element = document.getElementById('name');
    if (element) {
        var code = element.value;
        var charCount = code.length;

        if (charCount >= 16) {
     
            if (first_code == '' && end_code == '' && type_code == '') {
                first_code = code.substring(0, 3);
                type_code = code.substring(11, 14);
                date_code = code.substring(4,10);
                end_code = code.substring(15);
                check = true;
            }
        }
    }
};



let type_service = document.getElementById('type_service');
type_service.addEventListener('change', function() {
    let selectedValue = type_service.value;
    let type_booking = document.getElementById('type_booking').value;
    type_booking = replaceCharAt(type_booking, 0, selectedValue);
    document.getElementById('type_booking').value = type_booking;
    auto_code();
});

let station = document.getElementById('station');
station.addEventListener('change', function() {
    let selectedValue = station.value;
    let type_booking = document.getElementById('type_booking').value;
    type_booking = replaceCharAt(type_booking, 1, selectedValue);
    document.getElementById('type_booking').value = type_booking;

    auto_code();
});

let airport = document.getElementById('airport');
airport.addEventListener('change', function() {
    let selectedValue = airport.value;
    let type_booking = document.getElementById('type_booking').value;
    type_booking = replaceCharAt(type_booking, 2, selectedValue);
    document.getElementById('type_booking').value = type_booking;
    auto_code();
});

let type_booking = document.getElementById('type_booking');
type_booking.addEventListener('change', function() {
    let selectedValue = type_booking.value;
    let service = selectedValue.charAt(0); // Ký tự đầu tiên
    let station = selectedValue.charAt(1); // Ký tự thứ hai
    let airport = selectedValue.charAt(2); // Ký tự thứ ba
    document.getElementById('type_service').value = service;
    document.getElementById('station').value = station;
    document.getElementById('airport').value = airport;
    auto_code();
});

function replaceCharAt(str, index, char) {
    if (index > str.length - 1) {
        str = str.padEnd(index, ' ');
    }
    return str.substr(0, index) + char + str.substr(index + 1);
}

if (document.getElementById('date_of_service2')) {
    document.getElementById('date_of_service2').addEventListener('change', function() {
        auto_code();
    });
}

var intervalId1;
var Account = "custom_accounts_aos_bookings_1_name";
var Account_id = 'accounts_aos_bookings_1accounts_ida';
var btn_choose = 'btn_accounts_aos_bookings_1_name';
var btn_choose_clear = 'btn_clr_accounts_aos_bookings_1_name';
var id2 = document.getElementById(Account_id).value;
const getElement = (id) => document.getElementById(id);
const handleClick = () => {
    intervalId1 = setInterval(check_hidden_id, 500);
};
const handleFocus = () => {
    intervalId1 = setInterval(check_hidden_id, 500);
};
const handleBlur = () => {
    intervalId1 = setInterval(check_hidden_id, 500);
};

const check_hidden_id = () => {
    var id = getElement(Account_id);
    if (id.value !== id2) {
        auto_code();
        id2 = id.value;
        clearInterval(intervalId1);
    }
};

getElement(btn_choose).addEventListener("click", handleClick);
getElement(Account).addEventListener('focus', handleFocus);
getElement(Account).addEventListener('blur', handleBlur);

function auto_code() {

    var station = document.getElementById('station').value;
    var airport = document.getElementById('airport').value;
    var type_service = document.getElementById('type_service').value;
    var type_booking = document.getElementById('type_booking').value;
    var date_of_service2 = document.getElementById('date_of_service2').value;  //vd: 17-06-2024 16:05
    var account = document.getElementById('custom_accounts_aos_bookings_1_name').value;
    var account_id = document.getElementById('accounts_aos_bookings_1accounts_ida').value;
    var product_id = document.getElementById('aos_products_aos_bookings_1aos_products_ida').value;
    var parts = date_of_service2.split(' ');
    if (parts.length === 2) {
        var date_parts = parts[0].split('/'); // Tách ngày tháng năm từ phần đầu tiên
        var time_parts = parts[1].split(':'); // Tách giờ phút từ phần thứ hai

        if (date_parts.length === 3 && time_parts.length === 2) {
            var day = date_parts[0];
            var month = date_parts[1];
            var year_full = date_parts[2];
            var year = year_full.substring(2); // Lấy 2 ký tự cuối của year
            var hour = time_parts[0];
            var minute = time_parts[1];

            var date = year + month + day; 
            var time = hour + minute;
        }
    }

    if (typeof date === 'undefined') {
         var code = type_service + station + airport;
    }else {

        var code = date + '.' + type_service + station + airport;
    }
    if (account_id !== '' && type_booking !== '' && date_of_service2 !== '') {
        console.log('account_id:'+account_id);
        console.log('type_booking:'+type_booking);     
        console.log('date_of_service2:'+date_of_service2);               
        $.ajax({
            type: "POST",
            url: "index.php?entryPoint=get_num_booking_code",
            data: {
                account_id: account_id,
                type_booking: type_booking,
                date: date_of_service2,
            },
            success: function(data) {
                console.log(data);
                if (data != -1) {
                    datas = JSON.parse(data);
                    if (check == true && type_booking == type_code && datas.account_code == first_code && date_code == date) {
                        document.getElementById('name').value = datas.account_code +'.'+ code + '.' + end_code;
                        document.getElementById('name').style.color = "black";
                        document.getElementById('name').style.fontWeight = "normal";
                    }else{
                        document.getElementById('name').value = datas.account_code +'.'+ code + '.' + datas.stt;
                        document.getElementById('name').style.color = "blue";
                        document.getElementById('name').style.fontWeight = "bold";
                     }
                }
            },
            error: function(error) {
                console.error("Error: ", error);
            }
        });
    }
    document.getElementById('name').value = code;
}




//---check_voucher_code

var check_duplicate = 0;
var duplicate = document.querySelector('input[name="duplicateSave"]');
if (duplicate) {
  if (duplicate.value == "true") {
    check_duplicate = 1; // đang trùng lập
  }
}

var fieldcode = "name";
var oldValue = "";
var editViewFieldDiv = document.querySelector(
  '.edit-view-field[type="' + fieldcode + '"][field="' + fieldcode + '"]'
);
if (editViewFieldDiv) {
  var pElement = document.createElement("p");
  pElement.textContent = "";
  pElement.id = "remaining";
  editViewFieldDiv.appendChild(pElement);
}

if (check_duplicate == 1) {
  firstvalue = -999;
} else {
  var firstvalue;
  document.addEventListener("DOMContentLoaded", function () {
    var inputElement = document.getElementById(fieldcode);
    if (inputElement) {
      firstvalue = inputElement.value;
    }
  });
}

document.getElementById(fieldcode).addEventListener("focus", function () {
  // Lưu trữ giá trị hiện tại của input khi focus vào
  oldValue = this.value;
  this.style.color = "black";
  console.log(firstvalue);
  check_voucher_code(fieldcode, "aos_bookings", oldValue, firstvalue);
});

document.getElementById(fieldcode).addEventListener("blur", function () {
  console.log(firstvalue);
  check_voucher_code(fieldcode, "aos_bookings", oldValue, firstvalue);
});

