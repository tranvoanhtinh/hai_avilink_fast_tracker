<?php
require_once ('custom/modules/Leads/Dashlets/LeadCardDashlet/LeadCardDashletHelper.php');

$helper = new LeadCardDashletHelper();
$type = $_REQUEST['type'];
$keyword = isset($_REQUEST['keyword']) ? $_REQUEST['keyword'] : '';
$status = isset($_REQUEST['status']) ? $_REQUEST['status'] : '';

global $current_user;

if(!empty($keyword)) {
    $module = ($status != 'Transaction' && $status != 'Converted') ? 'Leads' : 'Accounts';
    $where = $helper->getWhereClause($keyword, $module);
}
else {
    $where = '';
}

if(!empty($where)) {
    $where = "($where)";
}

if($type == 'Search') {
    global $current_user;
    // Stored to use later
    $current_user->setPreference('LeadCardSaveSearch' . $status, $keyword);
}
$res = array();
if(!empty($status)) {
    switch ($type) {
        case 'Search':
            $res = $helper->getLeads($status, $where);
            break;
        case 'LoadMore':
            $exclude_ids = $_REQUEST['exclude_ids'];
            if(!empty($exclude_ids)) {
                $ids_str = " leads.id NOT IN ('" . implode("','", $exclude_ids) . "') ";
				
                if ($status == 'Converted' || $status == 'Transaction') {
                    $ids_str = " accounts.id NOT IN ('" . implode("','", $exclude_ids) . "') ";
                }
                if(!empty($where)) {
                    $where .= " AND " . $ids_str;
                }
				
                else {
                    $where = $ids_str;
                }
                $res = $helper->getLeads($status, $where);
            }
            break;
        default:
            break;
    }
}
echo json_encode($res);
exit;
