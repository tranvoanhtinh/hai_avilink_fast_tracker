<?php
include('../../../data/BeanFactory.php');

class MTS_LeadHook
{
    public function convertLead(&$bean)
    {
        global $db;
        // Only convert if the user change lead status to converted
        if ($bean->fetched_row['status'] != $bean->status && $bean->status == 'Converted') {
            //Check if account_id is not exist
            if (!empty($bean->account_id) && !empty($bean->contact_id)) {
                return;
            }
            if (empty($bean->account_id)) {
                $account = $this->createAccount($bean);
                $bean->account_id = $account->id;
            }
            if ($bean->bussiness_type_c == 'B2B') {
                if (empty($bean->contact_id)) {
                    $contact = $this->createContact($bean, $bean->account_id);
                    $bean->contact_id = $contact->id;
                    // Link contact to account
                    if ($account) {
                        $account->load_relationship('contacts');
                        $account->contacts->add($contact->id);
                    }
                }
            }

            if ($account) {
                $account->load_relationship('leads');
                $account->leads->add($bean->id);
            }

            $lead = new Lead();
            $lead->retrieve($bean->id);
            // Prevent process hook
            $lead->processed = true;
            $lead->account_id = $bean->account_id;
            $lead->contact_id = $bean->contact_id;
            $lead->save();
            $q = "select * from aos_products_leads_1_c where aos_products_leads_1leads_idb = '" . $bean->id . "' and deleted = '0'";
            $r = $db->query($q);
            while ($row = $db->fetchByAssoc($r)) {
                date_default_timezone_set('Asia/Ho_Chi_Minh');
                $time = date('Y-m-d h:i:s');
                $id = create_guid();
                $q1 = "insert into aos_products_accounts_1_c (id,date_modified,aos_products_accounts_1aos_products_ida,aos_products_accounts_1accounts_idb)
				values ('" . $id . "','" . $time . "','" . $row["aos_products_leads_1aos_products_ida"] . "','" . $bean->account_id . "')";
                $db->query($q1);
                //$q3=" update accounts set aos_product_id = '".$row["aos_products_leads_1aos_products_ida"]."' where id = '".$bean->account_id."' and deleted = '0'";
                //$db->query($q3);
            }
            //Copy activity
            $arr_account = array($account);
            $this->handleActivities($bean, $arr_account);

            $this->convertEmail($bean);
        }
    }

    public function addID($id)
    {

        global $db;
        $call_meeting = array();
        $tables = array("calls", "meetings");

        foreach ($tables as $table) {
            if ($table == "calls") {
                $col_id = "call_id";
            } else {

                $col_id = "meeting_id";
            }

            $q = "select " . $col_id . " from " . $table . "_leads where lead_id='" . $id . "'";
            $result = $db->query($q);
            while ($row = $db->fetchByAssoc($result)) {
                if ($row[$col_id] != null) {
                    $query = "Update " . $table . " set parent_id = '" . $id . "' where id='" . $row[$col_id] . "' ";
                    $db->query($query);
                    $call_meeting[$table] = $row[$col_id];
                }
            }
        }
        $quer = "select id from notes  where parent_id='" . $id . "'";
        $resu = $db->query($quer);
        $r = $db->fetchByAssoc($resu);
        if (!empty($r)) {
            $query = "select account_id from leads where id='" . $id . "' and status='Converted' and deleted ='0'";
            $res = $db->query($query);
            $ro = $db->fetchByAssoc($res);
            if (!empty($ro)) {
                $qr = "update notes set parent_type='Accounts',parent_id='" . $ro['account_id'] . "' where parent_id='" . $id . "' ";
                $db->query($qr);
            }
        }
        return $call_meeting;
    }

    public function createAccount($bean)
    {
        global $db;
        $q = "select * from aos_products_leads_1_c where aos_products_leads_1leads_idb = '" . $bean->id . "' and deleted = '0'";
        $r = $db->query($q);
        while ($row = $db->fetchByAssoc($r)) {
            $aos_product_id = $row["aos_products_leads_1aos_products_ida"];
        }
        if ($bean->bussiness_type_c == 'B2B') {
            $field_maps = array(
                (($bean->account_name == null) ? 'last_name' : 'account_name') => 'name',
                'name' => 'primarycontact_c',
                'phone_work' => 'phone_office',
                'website' => 'website',
                'description' => 'description',
                'phone_fax' => 'phone_fax',
                'phone_mobile' => 'phone_alternate',
                'email1' => 'email1',
                'bussiness_type_c' => 'bussiness_type_c',
                'lead_source' => 'leadsource_c',
                'shortname_c' => 'shortname_c',
                'skypename_c' => 'skypename_c',
                'vatcode_c' =>  'tax_code',
                'birthdate' => 'birthdate_c',
                'dateaccount_c' => 'dateaccount_c',
                'campaign_id' => 'campaign_id',
                'ratinglead_c' => 'ratingaccount_c',
                'identitycard_c' => 'identitycard_c',
                'bankaccount_c' => 'bankaccount_c',
                'date_converted' => 'dateconverted_c',
                'opportunity_amount' => 'opportunitiesamount_c',
                'servicetype_c' => 'servicetype_c',
                'company_location' => 'company_location',
                'industry' => 'industry',
                'status_activites' => 'status_activites',
                'account_code' => 'account_code',
                'opportunity_amount' => 'opportunitiesamount',
                'paymentsamount' => 'paymentsamount',
                'depositsamount' => 'depositsamount',
                'consultant' => 'consultant',
                'relatives' => 'relatives',
                'scanner' => 'scanner',
                'countries' => 'countries',
                'social_account' => 'social_account',
                'sex' => 'sex',
                'object_customer' => 'object_customer',
                'list_age' => 'list_age',
                'is_single' => 'is_single',
                'photo' => 'photo',


            );
        } else {
            $field_maps = array(
                'last_name' => 'name',
                'account_name' => 'company_c',
                'name' => 'primarycontact_c',
                'phone_work' => 'phone_office',
                'website' => 'website',
                'description' => 'description',
                'phone_fax' => 'phone_fax',
                'phone_mobile' => 'phone_alternate',
                'email1' => 'email1',
                'bussiness_type_c' => 'bussiness_type_c',
                'lead_source' => 'leadsource_c',
                'shortname_c' => 'shortname_c',
                'skypename_c' => 'skypename_c',
                'vatcode_c' =>  'tax_code',
                'birthdate' => 'birthdate_c',
                'dateaccount_c' => 'dateaccount_c',
                'campaign_id' => 'campaign_id',
                'ratinglead_c' => 'ratingaccount_c',
                'identitycard_c' => 'identitycard_c',
                'bankaccount_c' => 'bankaccount_c',
                'date_converted' => 'dateconverted_c',
                'opportunity_amount' => 'opportunitiesamount_c',
                'servicetype_c' => 'servicetype_c',
                'company_location' => 'company_location',
                'industry' => 'industry',
                'status_activites' => 'status_activites',
                'account_code' => 'account_code',
                'opportunity_amount' => 'opportunitiesamount',
                'paymentsamount' => 'paymentsamount',
                'depositsamount' => 'depositsamount',
                'consultant' => 'consultant',
                'relatives' => 'relatives',
                'scanner' => 'scanner',
                'countries' => 'countries',
                'social_account' => 'social_account',
                'sex' => 'sex',
                'object_customer' => 'object_customer',
                'list_age' => 'list_age',
                'is_single' => 'is_single',
                'photo' => 'photo',

            );
        }


        $account = new Account();

        $star_filename = $account->id;
        $oldFilePath = "/upload/{$bean->id}_photo";


        if (file_exists($oldFilePath)) {
            $end_filename = strrchr($oldFilePath, '_');
            if ($end_filename !== false) {
                $newFilePath = $star_filename . $end_filename; // Kết hợp $name với phần cuối cùng của tên file
                rename($oldFilePath, $newFilePath);
            }
        }

        foreach ($field_maps as $lead_field => $account_field) {
            $account->{$account_field} = $bean->{$lead_field};
            //if($bean->{$lead_field}== null && $lead_field=="account_name"){
            //	$fie="last_name";
            //	 $account->{$account_field}= $bean->{$fie};


            //}
        }
        if (!empty($bean->assigned_user_id)) {
            $account->assigned_user_id = $bean->assigned_user_id;
        } else {
            $account->assigned_user_id = $GLOBALS['current_user']->id;
        }
        if (!empty($bean->otherassigned)) {
            $account->otherassigned = $bean->otherassigned;
        }

        $account->aos_product_id = $aos_product_id;
        $this->copyAddress($bean, $account);
        $account->save();

        //chuyển file hình leed -> accounts 
        $star_filename = $account->id;
        $oldFilePath = 'upload/' . $bean->id . '_photo';

        $newDirectory = 'upload/';  // Thêm đường dẫn thư mục mới ở đây

        if (file_exists($oldFilePath)) {
            $end_filename = strrchr($oldFilePath, '_');
            if ($end_filename !== false) {
                $newFilePath = $newDirectory . $star_filename . $end_filename; // Kết hợp $name với phần cuối cùng của tên file
                copy($oldFilePath, $newFilePath);
            }
        }
        return $account;
    }

    public function createContact($bean, $account_id = '')
    {
        $field_maps = array(
            'first_name' => 'first_name',
            'title' => 'title',
            'last_name' => 'last_name',
            'salutation' => 'salutation',
            'department' => 'department',
            'phone_work' => 'phone_work',
            'phone_mobile' => 'phone_mobile',
            'phone_fax' => 'phone_fax',
            'lead_source' => 'lead_source',
            'email1' => 'email1',
            'description' => 'description',
            'phone_other' => 'phone_other',
            'phone_home' => 'phone_home',
            'skypename_c' => 'skypename_c',
            'birthdate' => 'birthdate',
            'campaign_id' => 'campaign_id',
            'identitycard_c' => 'identitycard_c',
            'bankaccount_c' => 'bankaccount_c',
            'photo' => 'photo',
        );
        $contact = new Contact();
        foreach ($field_maps as $lead_field => $contact_field) {
            $contact->{$contact_field} = $bean->{$lead_field};
        }
        if (!empty($bean->assigned_user_id)) {
            $contact->assigned_user_id = $bean->assigned_user_id;
        } else {
            $contact->assigned_user_id = $GLOBALS['current_user']->id;
        }
        $this->copyAddress($bean, $contact);
        $contact->account_id = $account_id;
        $contact->save();


        //chuyển file hình leed -> contact 
        $star_filename = $contact->id;
        $oldFilePath = 'upload/' . $bean->id . '_photo';

        $newDirectory = 'upload/';  // Thêm đường dẫn thư mục mới ở đây

        if (file_exists($oldFilePath)) {
            $end_filename = strrchr($oldFilePath, '_');
            if ($end_filename !== false) {
                $newFilePath = $newDirectory . $star_filename . $end_filename; // Kết hợp $name với phần cuối cùng của tên file
                copy($oldFilePath, $newFilePath);
            }
        }


        return $contact;
    }

    public function cloneRelationships()
    {
    }

    public function copyAddress($lead, &$new_bean)
    {

        $address_fields = array(
            '_address_street', '_address_city', '_address_state', '_address_country', '_address_postalcode'
        );
        // Copy for account
        if ($new_bean->module_dir == 'Accounts') {
            $address_types = array(
                'primary' => 'billing',
                'alt' => 'shipping'
            );
        }
        // Copy for contact
        else if ($new_bean->module_dir == 'Contacts') {
            $address_types = array(
                'primary' => 'primary',
                'alt' => 'alt'
            );
        } else {
            $address_types = array();
        }

        foreach ($address_types as $lead_type => $new_bean_type) {
            foreach ($address_fields as $field) {
                $new_bean->{$new_bean_type . $field} = $lead->{$lead_type . $field};
            }
        }
    }

    protected function getActivitiesFromLead(
        $lead
    ) {
        if (!$lead) return;

        global $beanList, $db;

        $activitesList = array("Calls", "Meetings", "Emails");
        $activities = array();

        foreach ($activitesList as $module) {
            $beanName = $beanList[$module];
            $activity = new $beanName();
            $query = "SELECT id FROM {$activity->table_name} WHERE parent_id = '{$lead->id}' AND parent_type = 'Leads' AND deleted = 0";
            $result = $db->query($query, true);
            while ($row = $db->fetchByAssoc($result)) {
                $activity = new $beanName();
                $activity->retrieve($row['id']);
                $activity->fixUpFormatting();
                $activities[] = $activity;
            }
        }

        return $activities;
    }

    protected function handleActivities($lead, $beans = array())
    {

        global $app_list_strings;
        global $sugar_config;
        global $app_strings;
        global $db;
        $call_meeting = array();
        $parent_types = $app_list_strings['record_type_display'];
        $call_meeting = $this->addID($lead->id);
        $activities = $this->getActivitiesFromLead($lead);
        foreach ($beans as $bean) {
            foreach ($activities as $activity) {
                $this->copyActivityAndRelateToBean($activity, $bean);
            }
        }
        foreach ($call_meeting as $table => $value) {
            $query = "Update " . $table . " set deleted = '1' where id='" . $value . "' ";
            $db->query($query);
        }
    }

    protected function copyActivityAndRelateToBean($activity, $bean)
    {
        global $beanList;

        $newActivity = clone $activity;
        $newActivity->id = create_guid();
        $newActivity->new_with_id = true;

        //set the parent id and type if it was passed in, otherwise use blank to wipe it out
        $parentID = $bean->id;
        $parentType = $bean->module_dir;
        //Special case to prevent duplicated tasks from appearing under Contacts multiple times
        if ($newActivity->module_dir == "Tasks" && $bean->module_dir != "Contacts") {
            $newActivity->contact_id = $newActivity->contact_name = "";
        }

        if ($rel = $this->findRelationship($newActivity, $bean)) {
            if (isset($newActivity->$rel)) {
                // this comes form $activity, get rid of it and load our own
                $newActivity->$rel = '';
            }

            $newActivity->load_relationship($rel);
            $relObj = $newActivity->$rel->getRelationshipObject();
            if ($relObj->relationship_type == 'one-to-one' || $relObj->relationship_type == 'one-to-many') {
                $key = $relObj->rhs_key;
                $newActivity->$key = $bean->id;
            }

            //check users connected to bean
            if ($activity->load_relationship("users")) {
                $userList = $activity->users->getBeans();
                if (count($userList) > 0 && $newActivity->load_relationship("users")) {
                    foreach ($userList as $user) {
                        $newActivity->users->add($user->id);
                    }
                }
            }
            //parent (related to field) should be blank unless it is explicitly sent in
            //it is not sent in unless the account is being created as well during lead conversion
            $newActivity->parent_id =  $parentID;
            $newActivity->parent_type = $parentType;

            $newActivity->update_date_modified = false; //bug 41747
            $newActivity->save();
            $newActivity->$rel->add($bean);
        }
    }

    protected function findRelationship(
        $from,
        $to
    ) {
        global $dictionary;
        require_once("modules/TableDictionary.php");
        foreach ($from->field_defs as $field => $def) {
            if (isset($def['type']) && $def['type'] == "link" && isset($def['relationship'])) {
                $rel_name = $def['relationship'];
                $rel_def = "";
                if (isset($dictionary[$from->object_name]['relationships']) && isset($dictionary[$from->object_name]['relationships'][$rel_name])) {
                    $rel_def = $dictionary[$from->object_name]['relationships'][$rel_name];
                } else if (isset($dictionary[$to->object_name]['relationships']) && isset($dictionary[$to->object_name]['relationships'][$rel_name])) {
                    $rel_def = $dictionary[$to->object_name]['relationships'][$rel_name];
                } else if (
                    isset($dictionary[$rel_name]) && isset($dictionary[$rel_name]['relationships'])
                    && isset($dictionary[$rel_name]['relationships'][$rel_name])
                ) {
                    $rel_def = $dictionary[$rel_name]['relationships'][$rel_name];
                }
                if (!empty($rel_def)) {
                    if ($rel_def['lhs_module'] == $from->module_dir && $rel_def['rhs_module'] == $to->module_dir) {
                        return $field;
                    } else if ($rel_def['rhs_module'] == $from->module_dir && $rel_def['lhs_module'] == $to->module_dir) {
                        return $field;
                    }
                }
            }
        }
        return false;
    }

    public function updateDateStatus(&$bean)
    {
        $old_status = isset($bean->fetched_row['status']) ? $bean->fetched_row['status'] : '';
        if (!empty($bean->status) && $old_status != $bean->status) {
            global $timedate;
            switch ($bean->status) {
                case 'New':
                    $bean->date_new = $timedate->nowDbDate();
                    break;
                case 'In Process':
                    $bean->date_processed = $timedate->nowDbDate();
                    break;
                case 'Converted':
                    $bean->date_converted = $timedate->nowDbDate();
                    break;
            }
        }
    }

    public function convertEmail(&$bean)
    {
        global $db;
        //date_default_timezone_set('Asia/Ho_Chi_Minh');
        //$date = date('Y-m-d h:i:s', time());
        //$date->add(new DateInterval("PT7H"));

        $query = "SELECT * FROM email_addr_bean_rel WHERE bean_id='" . $bean->id . "' AND primary_address='0'";
        $result = $db->query($query);

        while ($row = $db->fetchByAssoc($result)) {
            if (!empty($row)) {
                /*
				$quer="insert into email_addr_bean_rel (id,email_address_id,bean_id,bean_module,primary_address,reply_to_address,deleted)
				values ('143','".$row['email_address_id']."','".$bean->account_id."','Accounts','0','0','0')";	
				$db->query($quer);
				if($bean->business_type_c="B2B"){
				$qr="insert into email_addr_bean_rel (email_address_id,bean_id,bean_module,primary_address,reply_to_address,deleted)
				values ('".$row['email_address_id']."','".$bean->contact_id."','Contacts','0','0','0')";
				$db->query($qr);
				}
			*/
                if ($row['deleted'] == 1) {
                    $quer = "update email_addr_bean_rel set deleted='0' where email_address_id='" . $row['email_address_id'] . "' and bean_id='" . $bean->id . "' and bean_module='Leads'";
                    $db->query($quer);
                }
                $email = BeanFactory::getBean('EmailAddresses', $row['email_address_id']);
                $accountBean = BeanFactory::getBean('Accounts', $bean->account_id);
                $accountBean->load_relationship('email_addresses');
                $accountBean->email_addresses->add($email);
                $qr = "update email_addr_bean_rel set date_created=date_modified where email_address_id='" . $row['email_address_id'] . "' and bean_id='" . $bean->account_id . "' and bean_module='Accounts'";
                $db->query($qr);
                if ($bean->business_type_c = "B2B") {
                    $contactBean = BeanFactory::getBean('Contacts', $bean->contact_id);
                    $contactBean->load_relationship('email_addresses');
                    $contactBean->email_addresses->add($email);
                    $qr = "update email_addr_bean_rel set date_created=date_modified where email_address_id='" . $row['email_address_id'] . "' and bean_id='" . $bean->contact_id . "' and bean_module='Contacts'";
                    $db->query($qr);
                }
            }
        }
    }
}
