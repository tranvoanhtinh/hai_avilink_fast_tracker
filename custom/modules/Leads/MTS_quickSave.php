<?php

require_once('modules/AOW_WorkFlow/aow_utils.php');
$res = array(
    'is_saved' => 0,
    'msg' => '',
    'value' => '',
);

$able_to_save = true;
if(!isset($_REQUEST['record']) || empty($_REQUEST['record'])) {
    $res['msg'] = 'Record does not exist';
    $able_to_save = false;
}
if(!isset($_REQUEST['target_module']) || empty($_REQUEST['target_module'])) {
    $res['msg'] = 'Module does not exist';
    $able_to_save = false;
}

if($able_to_save) {
    $field_name = $_REQUEST['field_name'];
    try {
        $bean = BeanFactory::getBean($_REQUEST['target_module']);
        $bean->retrieve($_REQUEST['record']);
        $bean->{$field_name} = $_REQUEST['field_value'];
        $bean->save();
    }
    catch (Exception $e) {
        $res['msg'] = $e->getMessage();
    }
    $res['value'] = html_entity_decode($bean->{$field_name});
    $res['is_saved'] = 1;
}

echo json_encode($res);
exit;



