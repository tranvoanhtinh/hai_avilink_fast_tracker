<?php


$parent_type = $_REQUEST['parent_type'];
$res = array(
    'status' => 0,
    'msg' => ''
);
$_REQUEST['module'] = $parent_type;
$_POST['module'] = $parent_type;
try {
    if($parent_type == 'Leads')
    {
        require_once('include/formbase.php');
    
        $focus = new Lead();
        $focus = populateFromPost('', $focus);
        if(!$focus->ACLAccess('Save')){
            $res['msg'] = "You've not permission to save";
        }
        else {

            if (!isset($_POST[$prefix.'email_opt_out'])) $focus->email_opt_out = 0;
            if (!isset($_POST[$prefix.'do_not_call'])) $focus->do_not_call = 0;
        
            if(!empty($GLOBALS['check_notify'])) {
                    $focus->save($GLOBALS['check_notify']);
                }
            else {
                    $focus->save(FALSE);
            }
        
            $return_id = $focus->id;
        
            if(isset($_REQUEST['inbound_email_id']) && !empty($_REQUEST['inbound_email_id'])) {
                if(!isset($current_user)) {
                    global $current_user;
                }
        
                $email = new Email();
                $email->retrieve($_REQUEST['inbound_email_id']);
                $email->parent_type = 'Leads';
                $email->parent_id = $focus->id;
                $email->assigned_user_id = $current_user->id;
                $email->status = 'read';
                $email->save();
                $email->load_relationship('leads');
                $email->leads->add($focus->id);
        
            }
            $res = array(
                'status' => 1,
                'msg' => 'Save successful'
            );
            $GLOBALS['log']->debug("Saved record with id of ".$return_id);
        }
    }
    // Save account
    else if($parent_type == 'Accounts') {

        require_once('include/formbase.php');
        $focus = new Account();

        $focus = populateFromPost('', $focus);

        if (isset($GLOBALS['check_notify'])) {
            $check_notify = $GLOBALS['check_notify'];
        }
        else {
            $check_notify = FALSE;
        }
        if(!$focus->ACLAccess('Save')){
            $res['msg'] = "You've not permission to save";
        }
        else {

            $focus->save($check_notify);
            $return_id = $focus->id;
            $res = array(
                'status' => 1,
                'msg' => 'Save successful'
            );
        }
    
    $GLOBALS['log']->debug("Saved record with id of ".$return_id);
    }
}
catch(Exception $e) {
    $res['msg'] = $e->getMessage();
}

echo json_encode($res); exit;