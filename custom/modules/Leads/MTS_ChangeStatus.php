<?php

$res = array(
    'is_updated' => 0,
    'message' => 'Could not found Lead or status incorrect'
);
if(isset($_POST['id']) && isset($_POST['status'])) {
    if(empty($_POST['id']) || empty($_POST['status'])) {
       /*
        * Do nothing
        */
    }
    else {
        try {
            $lead = BeanFactory::getBean("Leads");
            $lead->retrieve($_POST['id']);
            $lead->status = $_POST['status'];
            $lead->save();
            $res = array(
                'is_updated' => 1,
                'message' => 'Update successful'
            );
        }
        catch (Exception $e) {
            $res = array(
                'is_updated' => 0,
                'message' => $e->getMessage()
            );
        }
    }
}
echo json_encode($res);
exit;