<?php
$searchdefs ['Leads'] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'search_name' => 
      array (
        'name' => 'search_name',
        'label' => 'LBL_NAME',
        'type' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'account_name' => 
      array (
        'name' => 'account_name',
        'default' => true,
        'width' => '10%',
      ),
      'phone' => 
      array (
        'name' => 'phone',
        'label' => 'LBL_ANY_PHONE',
        'type' => 'name',
        'default' => true,
        'width' => '10%',
      ),
    ),
    'advanced_search' => 
    array (
      'campaign_name' => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_CAMPAIGN',
        'id' => 'CAMPAIGN_ID',
        'width' => '10%',
        'default' => true,
        'name' => 'campaign_name',
      ),
      'email' => 
      array (
        'name' => 'email',
        'label' => 'LBL_ANY_EMAIL',
        'type' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'phone' => 
      array (
        'name' => 'phone',
        'label' => 'LBL_ANY_PHONE',
        'type' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'address_street' => 
      array (
        'name' => 'address_street',
        'label' => 'LBL_ANY_ADDRESS',
        'type' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'company_location' => 
      array (
        'type' => 'enum',
        'label' => 'LBL_COMPANY_LOCATION',
        'width' => '10%',
        'default' => true,
        'name' => 'company_location',
      ),
      'industry' => 
      array (
        'type' => 'enum',
        'label' => 'LBL_INDUSTRY',
        'width' => '10%',
        'default' => true,
        'name' => 'industry',
      ),
      'bussiness_type_c' => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_BUSSINESS_TYPE',
        'width' => '10%',
        'name' => 'bussiness_type_c',
      ),
      'ratinglead_c' => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_RATINGLEAD',
        'width' => '10%',
        'name' => 'ratinglead_c',
      ),
      'status' => 
      array (
        'name' => 'status',
        'default' => true,
        'width' => '10%',
      ),
      'sex' => 
      array (
        'type' => 'enum',
        'label' => 'LBL_SEX',
        'width' => '10%',
        'default' => true,
        'name' => 'sex',
      ),
      'status_activites' => 
      array (
        'type' => 'enum',
        'label' => 'LBL_STATUS_ACTIVITIES',
        'width' => '10%',
        'default' => true,
        'name' => 'status_activites',
      ),
      'identitycard_c' => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_IDENTITYCARD',
        'width' => '10%',
        'name' => 'identitycard_c',
      ),
      'is_single' => 
      array (
        'type' => 'bool',
        'default' => true,
        'label' => 'LBL_IS_SINGLE',
        'width' => '10%',
        'name' => 'is_single',
      ),
      'lead_source' => 
      array (
        'name' => 'lead_source',
        'default' => true,
        'width' => '10%',
      ),
      'list_age' => 
      array (
        'type' => 'enum',
        'label' => 'LBL_LIST_AGE',
        'width' => '10%',
        'default' => true,
        'name' => 'list_age',
      ),
      'object_customer' => 
      array (
        'type' => 'enum',
        'label' => 'LBL_OBJECT_CUSTOMER',
        'width' => '10%',
        'default' => true,
        'name' => 'object_customer',
      ),
      'social_account' => 
      array (
        'type' => 'enum',
        'label' => 'LBL_SOCIAL_ACCOUNT',
        'width' => '10%',
        'default' => true,
        'name' => 'social_account',
      ),
      'countries' => 
      array (
        'type' => 'enum',
        'label' => 'LBL_COUNTRIES',
        'width' => '10%',
        'default' => true,
        'name' => 'countries',
      ),
      'aos_product_categories_leads_1_name' => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_AOS_PRODUCT_CATEGORIES_LEADS_1_FROM_AOS_PRODUCT_CATEGORIES_TITLE',
        'id' => 'AOS_PRODUCT_CATEGORIES_LEADS_1AOS_PRODUCT_CATEGORIES_IDA',
        'width' => '10%',
        'default' => true,
        'name' => 'aos_product_categories_leads_1_name',
      ),
      'assigned_user_id' => 
      array (
        'name' => 'assigned_user_id',
        'type' => 'enum',
        'label' => 'LBL_ASSIGNED_TO',
        'function' => 
        array (
          'name' => 'get_user_array',
          'params' => 
          array (
            0 => false,
          ),
        ),
        'default' => true,
        'width' => '10%',
      ),
      'created_by' => 
      array (
        'type' => 'assigned_user_name',
        'label' => 'LBL_CREATED',
        'width' => '10%',
        'default' => true,
        'name' => 'created_by',
      ),
      'modified_user_id' => 
      array (
        'type' => 'assigned_user_name',
        'label' => 'LBL_MODIFIED',
        'width' => '10%',
        'default' => true,
        'name' => 'modified_user_id',
      ),
      'date_entered' => 
      array (
        'type' => 'datetime',
        'label' => 'LBL_DATE_ENTERED',
        'width' => '10%',
        'default' => true,
        'name' => 'date_entered',
      ),
      'date_modified' => 
      array (
        'type' => 'datetime',
        'label' => 'LBL_DATE_MODIFIED',
        'width' => '10%',
        'default' => true,
        'name' => 'date_modified',
      ),
      'lastdateactive_c' => 
      array (
        'type' => 'datetimecombo',
        'default' => true,
        'label' => 'LBL_LASTDATEACTIVE',
        'width' => '10%',
        'name' => 'lastdateactive_c',
      ),
      'plandateactive_c' => 
      array (
        'type' => 'datetimecombo',
        'default' => true,
        'label' => 'LBL_PLANDATEACTIVE',
        'width' => '10%',
        'name' => 'plandateactive_c',
      ),
      'delivery_date' => 
      array (
        'type' => 'datetimecombo',
        'label' => 'LBL_DELIVERY_DATE',
        'width' => '10%',
        'default' => true,
        'name' => 'delivery_date',
      ),
      'ads' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_ADS',
        'width' => '10%',
        'default' => true,
        'name' => 'ads',
      ),
      'ads_name' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_ADS_NAME',
        'width' => '10%',
        'default' => true,
        'name' => 'ads_name',
      ),
      'campaign_id_face_ads' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_CAMPAIGN_ID_FACE_ADS',
        'width' => '10%',
        'default' => true,
        'name' => 'campaign_id_face_ads',
      ),
      'current_user_only' => 
      array (
        'label' => 'LBL_CURRENT_USER_FILTER',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
        'name' => 'current_user_only',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
;
?>
