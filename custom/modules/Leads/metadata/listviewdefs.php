<?php
$listViewDefs ['Leads'] = 
array (
  'NAME' => 
  array (
    'width' => '10%',
    'label' => 'LBL_LIST_NAME',
    'link' => true,
    'orderBy' => 'name',
    'default' => true,
    'related_fields' => 
    array (
      0 => 'first_name',
      1 => 'last_name',
      2 => 'salutation',
    ),
  ),
  'STATUS' => 
  array (
    'width' => '7%',
    'label' => 'LBL_LIST_STATUS',
    'default' => true,
  ),
  'PHONE_MOBILE' => 
  array (
    'width' => '10%',
    'label' => 'LBL_MOBILE_PHONE',
    'default' => true,
  ),
  'EMAIL1' => 
  array (
    'width' => '16%',
    'label' => 'LBL_LIST_EMAIL_ADDRESS',
    'sortable' => false,
    'customCode' => '{$EMAIL1_LINK}',
    'default' => true,
  ),
  'STATUS_ACTIVITES' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_STATUS_ACTIVITIES',
    'width' => '10%',
    'default' => true,
  ),
  'ACCOUNT_NAME' => 
  array (
    'width' => '15%',
    'label' => 'LBL_LIST_ACCOUNT_NAME',
    'default' => true,
    'related_fields' => 
    array (
      0 => 'account_id',
    ),
  ),
  'CAMPAIGN_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CAMPAIGN',
    'id' => 'CAMPAIGN_ID',
    'width' => '10%',
    'default' => true,
  ),
  'DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '5%',
    'label' => 'LBL_LIST_ASSIGNED_USER',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
  'DATE_ENTERED' => 
  array (
    'width' => '10%',
    'label' => 'LBL_DATE_ENTERED',
    'default' => true,
  ),
  'PHOTO' => 
  array (
    'type' => 'image',
    'width' => '10%',
    'studio' => 
    array (
      'listview' => true,
    ),
    'label' => 'LBL_PHOTO',
    'default' => false,
  ),
  'AOS_PRODUCT_CATEGORIES_LEADS_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_AOS_PRODUCT_CATEGORIES_LEADS_1_FROM_AOS_PRODUCT_CATEGORIES_TITLE',
    'id' => 'AOS_PRODUCT_CATEGORIES_LEADS_1AOS_PRODUCT_CATEGORIES_IDA',
    'width' => '10%',
    'default' => false,
  ),
  'BANKACCOUNT_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_BANKACCOUNT',
    'width' => '10%',
  ),
  'ACCOUNT_CODE' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ACCOUNT_CODE',
    'width' => '10%',
    'default' => false,
  ),
  'RECEIVENAME_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_RECEIVENAME',
    'width' => '10%',
  ),
  'OPPORTUNITY_NAME' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_OPPORTUNITY_NAME',
    'width' => '10%',
    'default' => false,
  ),
  'ACCOUNT_DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_ACCOUNT_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'BIRTHDATE' => 
  array (
    'type' => 'date',
    'label' => 'LBL_BIRTHDATE',
    'width' => '10%',
    'default' => false,
  ),
  'AOS_PRODUCTS_LEADS_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_AOS_PRODUCTS_LEADS_1_FROM_AOS_PRODUCTS_TITLE',
    'id' => 'AOS_PRODUCTS_LEADS_1AOS_PRODUCTS_IDA',
    'width' => '10%',
    'default' => false,
  ),
  'OPPORTUNITY_AMOUNT' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_OPPORTUNITY_AMOUNT',
    'currency_format' => true,
    'width' => '10%',
    'default' => false,
  ),
  'PAYMENTSAMOUNT' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_Payments_Amount',
    'currency_format' => true,
    'width' => '10%',
    'default' => false,
  ),
  'LIST_AGE' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_LIST_AGE',
    'width' => '10%',
    'default' => false,
  ),
  'OBJECT_CUSTOMER' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_OBJECT_CUSTOMER',
    'width' => '10%',
    'default' => false,
  ),
  'SCANNER' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_SCANNER',
    'width' => '10%',
    'default' => false,
  ),
  'SOCIAL_ACCOUNT' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_SOCIAL_ACCOUNT',
    'width' => '10%',
    'default' => false,
  ),
  'RELATIVES' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_RELATIVES',
    'width' => '10%',
    'default' => false,
  ),
  'IS_SINGLE' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'LBL_IS_SINGLE',
    'width' => '10%',
  ),
  'COUNTRIES' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_COUNTRIES',
    'width' => '10%',
    'default' => false,
  ),
  'CONSULTANT' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_CONSULTANT',
    'width' => '10%',
    'default' => false,
  ),
  'COMPANY_LOCATION' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_COMPANY_LOCATION',
    'width' => '10%',
    'default' => false,
  ),
  'DEPOSITSAMOUNT' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_Deposits_Amount',
    'currency_format' => true,
    'width' => '10%',
    'default' => false,
  ),
  'CREATED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'default' => false,
  ),
  'BUSSINESS_TYPE_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_BUSSINESS_TYPE',
    'width' => '10%',
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => false,
  ),
  'SERVICETYPE_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_SERVICETYPE',
    'width' => '10%',
  ),
  'IDENTITYCARD_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_IDENTITYCARD',
    'width' => '10%',
  ),
  'PLANDESCRIPTION_C' => 
  array (
    'type' => 'text',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_PLANDESCRIPTION',
    'sortable' => false,
    'width' => '10%',
  ),
  'VATCODE_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_VATCODE',
    'width' => '10%',
  ),
  'ASSISTANT' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ASSISTANT',
    'width' => '10%',
    'default' => false,
  ),
  'ASSISTANT_PHONE' => 
  array (
    'type' => 'phone',
    'label' => 'LBL_ASSISTANT_PHONE',
    'width' => '10%',
    'default' => false,
  ),
  'SKYPENAME_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_SKYPENAME',
    'width' => '10%',
  ),
  'SHORTNAME_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_SHORTNAME',
    'width' => '10%',
  ),
  'CAMPAIGN_NAME_FACE_ADS' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_CAMPAIGN_NAME_FACE_ADS',
    'width' => '10%',
    'default' => false,
  ),
  'DATEACCOUNT_C' => 
  array (
    'type' => 'date',
    'default' => false,
    'label' => 'LBL_DATEACCOUNT',
    'width' => '10%',
  ),
  'SEX' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_SEX',
    'width' => '10%',
    'default' => false,
  ),
  'INDUSTRY' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_INDUSTRY',
    'width' => '10%',
    'default' => false,
  ),
  'TITLE' => 
  array (
    'width' => '10%',
    'label' => 'LBL_TITLE',
    'default' => false,
  ),
  'WEBSITE' => 
  array (
    'type' => 'url',
    'label' => 'LBL_WEBSITE',
    'width' => '10%',
    'default' => false,
  ),
  'REPORT_TO_NAME' => 
  array (
    'type' => 'relate',
    'label' => 'LBL_REPORTS_TO',
    'id' => 'REPORTS_TO_ID',
    'link' => true,
    'width' => '10%',
    'default' => false,
  ),
  'STATUS_DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_STATUS_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'LEAD_SOURCE_DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_LEAD_SOURCE_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'PHONE_WORK' => 
  array (
    'width' => '15%',
    'label' => 'LBL_LIST_PHONE',
    'default' => false,
  ),
  'REFERED_BY' => 
  array (
    'width' => '10%',
    'label' => 'LBL_REFERED_BY',
    'default' => false,
  ),
  'LEAD_SOURCE' => 
  array (
    'width' => '10%',
    'label' => 'LBL_LEAD_SOURCE',
    'default' => false,
  ),
  'DEPARTMENT' => 
  array (
    'width' => '10%',
    'label' => 'LBL_DEPARTMENT',
    'default' => false,
  ),
  'DO_NOT_CALL' => 
  array (
    'width' => '10%',
    'label' => 'LBL_DO_NOT_CALL',
    'default' => false,
  ),
  'PHONE_HOME' => 
  array (
    'width' => '10%',
    'label' => 'LBL_HOME_PHONE',
    'default' => false,
  ),
  'PHONE_OTHER' => 
  array (
    'width' => '10%',
    'label' => 'LBL_OTHER_PHONE',
    'default' => false,
  ),
  'PHONE_FAX' => 
  array (
    'width' => '10%',
    'label' => 'LBL_FAX_PHONE',
    'default' => false,
  ),
  'PRIMARY_ADDRESS_COUNTRY' => 
  array (
    'width' => '10%',
    'label' => 'LBL_PRIMARY_ADDRESS_COUNTRY',
    'default' => false,
  ),
  'PRIMARY_ADDRESS_STREET' => 
  array (
    'width' => '10%',
    'label' => 'LBL_PRIMARY_ADDRESS_STREET',
    'default' => false,
  ),
  'PRIMARY_ADDRESS_CITY' => 
  array (
    'width' => '10%',
    'label' => 'LBL_PRIMARY_ADDRESS_CITY',
    'default' => false,
  ),
  'PRIMARY_ADDRESS_STATE' => 
  array (
    'width' => '10%',
    'label' => 'LBL_PRIMARY_ADDRESS_STATE',
    'default' => false,
  ),
  'PRIMARY_ADDRESS_POSTALCODE' => 
  array (
    'width' => '10%',
    'label' => 'LBL_PRIMARY_ADDRESS_POSTALCODE',
    'default' => false,
  ),
  'ALT_ADDRESS_COUNTRY' => 
  array (
    'width' => '10%',
    'label' => 'LBL_ALT_ADDRESS_COUNTRY',
    'default' => false,
  ),
  'ALT_ADDRESS_STREET' => 
  array (
    'width' => '10%',
    'label' => 'LBL_ALT_ADDRESS_STREET',
    'default' => false,
  ),
  'ALT_ADDRESS_CITY' => 
  array (
    'width' => '10%',
    'label' => 'LBL_ALT_ADDRESS_CITY',
    'default' => false,
  ),
  'ALT_ADDRESS_STATE' => 
  array (
    'width' => '10%',
    'label' => 'LBL_ALT_ADDRESS_STATE',
    'default' => false,
  ),
  'ALT_ADDRESS_POSTALCODE' => 
  array (
    'width' => '10%',
    'label' => 'LBL_ALT_ADDRESS_POSTALCODE',
    'default' => false,
  ),
  'CREATED_BY' => 
  array (
    'width' => '10%',
    'label' => 'LBL_CREATED',
    'default' => false,
  ),
  'DELIVERY_DATE' => 
  array (
    'type' => 'datetimecombo',
    'label' => 'LBL_DELIVERY_DATE',
    'width' => '10%',
    'default' => false,
  ),
  'LASTDATEACTIVE_C' => 
  array (
    'type' => 'datetimecombo',
    'default' => false,
    'label' => 'LBL_LASTDATEACTIVE',
    'width' => '10%',
  ),
  'PLANDATEACTIVE_C' => 
  array (
    'type' => 'datetimecombo',
    'default' => false,
    'label' => 'LBL_PLANDATEACTIVE',
    'width' => '10%',
  ),
  'RATINGLEAD_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_RATINGLEAD',
    'width' => '10%',
  ),
  'ADS_NAME' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ADS_NAME',
    'width' => '10%',
    'default' => false,
  ),
  'ADS' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ADS',
    'width' => '10%',
    'default' => false,
  ),
  'MODIFIED_BY_NAME' => 
  array (
    'width' => '5%',
    'label' => 'LBL_MODIFIED',
    'default' => false,
  ),
  'PROJECTNAME_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_PROJECTNAME',
    'width' => '10%',
  ),
  'REFID_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_REFID',
    'width' => '10%',
  ),
  'CAMPAIGN_ID_FACE_ADS' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_CAMPAIGN_ID_FACE_ADS',
    'width' => '10%',
    'default' => false,
  ),
  'FORM_ID_FACE' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_FORM_ID_FACE',
    'width' => '10%',
    'default' => false,
  ),
);
;
?>
