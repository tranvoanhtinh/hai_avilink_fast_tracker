<?php
$viewdefs ['Leads'] = 
array (
  'QuickCreate' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'hidden' => 
        array (
          0 => '<input type="hidden" name="prospect_id" value="{if isset($smarty.request.prospect_id)}{$smarty.request.prospect_id}{else}{$bean->prospect_id}{/if}">',
          1 => '<input type="hidden" name="contact_id" value="{if isset($smarty.request.contact_id)}{$smarty.request.contact_id}{else}{$bean->contact_id}{/if}">',
          2 => '<input type="hidden" name="opportunity_id" value="{if isset($smarty.request.opportunity_id)}{$smarty.request.opportunity_id}{else}{$bean->opportunity_id}{/if}">',
          3 => '<input type="hidden" name="account_id" value="{if isset($smarty.request.account_id)}{$smarty.request.account_id}{else}{$bean->account_id}{/if}">',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'javascript' => '<script type="text/javascript" language="Javascript">function copyAddressRight(form)  {ldelim} form.alt_address_street.value = form.primary_address_street.value;form.alt_address_city.value = form.primary_address_city.value;form.alt_address_state.value = form.primary_address_state.value;form.alt_address_postalcode.value = form.primary_address_postalcode.value;form.alt_address_country.value = form.primary_address_country.value;return true; {rdelim} function copyAddressLeft(form)  {ldelim} form.primary_address_street.value =form.alt_address_street.value;form.primary_address_city.value = form.alt_address_city.value;form.primary_address_state.value = form.alt_address_state.value;form.primary_address_postalcode.value =form.alt_address_postalcode.value;form.primary_address_country.value = form.alt_address_country.value;return true; {rdelim} </script>',
      'useTabs' => false,
      'tabDefs' => 
      array (
        'LBL_CONTACT_INFORMATION' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_QUICKCREATE_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'collapsed',
        ),
      ),
    ),
    'panels' => 
    array (
      'lbl_contact_information' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'first_name',
            'customCode' => '{html_options name="salutation" id="salutation" options=$fields.salutation.options selected=$fields.salutation.value}&nbsp;<input name="first_name"  id="first_name" size="25" maxlength="25" type="text" value="{$fields.first_name.value}">',
          ),
          1 => 
          array (
            'name' => 'bussiness_type_c',
            'studio' => 'visible',
            'label' => 'LBL_BUSSINESS_TYPE',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'last_name',
            'displayParams' => 
            array (
              'required' => true,
            ),
          ),
          1 => 
          array (
            'name' => 'relatives',
            'label' => 'LBL_RELATIVES',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'shortname_c',
            'label' => 'LBL_SHORTNAME',
          ),
          1 => 
          array (
            'name' => 'consultant',
            'label' => 'LBL_CONSULTANT',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'phone_mobile',
            'type' => 'dupdetector',
          ),
          1 => 
          array (
            'name' => 'phone_other',
            'type' => 'dupdetector',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'account_name',
          ),
          1 => 
          array (
            'name' => 'vatcode_c',
            'type' => 'dupdetector',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'email1',
          ),
          1 => 
          array (
            'name' => 'primary_address_street',
            'comment' => 'Street address for primary address',
            'label' => 'LBL_PRIMARY_ADDRESS_STREET',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'refered_by',
          ),
          1 => 
          array (
            'name' => 'lead_source',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'aos_product_categories_leads_1_name',
            'label' => 'LBL_AOS_PRODUCT_CATEGORIES_LEADS_1_FROM_AOS_PRODUCT_CATEGORIES_TITLE',
          ),
          1 => 
          array (
            'name' => 'aos_products_leads_1_name',
            'label' => 'LBL_AOS_PRODUCTS_LEADS_1_FROM_AOS_PRODUCTS_TITLE',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'opportunity_name',
            'comment' => 'Opportunity name associated with lead',
            'label' => 'LBL_OPPORTUNITY_NAME',
          ),
          1 => 
          array (
            'name' => 'website',
            'comment' => 'URL of website for the company',
            'label' => 'LBL_WEBSITE',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'opportunity_amount',
            'comment' => 'Amount of the opportunity',
            'label' => 'LBL_OPPORTUNITY_AMOUNT',
          ),
          1 => 
          array (
            'name' => 'servicetype_c',
            'studio' => 'visible',
            'label' => 'LBL_SERVICETYPE',
          ),
        ),
        10 => 
        array (
          0 => 
          array (
            'name' => 'depositsamount',
            'label' => 'LBL_Deposits_Amount',
          ),
          1 => 
          array (
            'name' => 'paymentsamount',
            'label' => 'LBL_Payments_Amount',
          ),
        ),
        11 => 
        array (
          0 => 
          array (
            'name' => 'industry',
            'label' => 'LBL_INDUSTRY',
          ),
          1 => 
          array (
            'name' => 'status_activites',
            'label' => 'LBL_STATUS_ACTIVITIES',
          ),
        ),
        12 => 
        array (
          0 => 
          array (
            'name' => 'description',
            'comment' => 'Full text of the note',
            'label' => 'LBL_DESCRIPTION',
          ),
          1 => 
          array (
            'name' => 'status_description',
            'comment' => 'Description of the status of the lead',
            'label' => 'LBL_STATUS_DESCRIPTION',
          ),
        ),
        13 => 
        array (
          0 => 
          array (
            'name' => 'assigned_user_name',
          ),
          1 => 
          array (
            'name' => 'campaign_name',
            'label' => 'LBL_CAMPAIGN',
          ),
        ),
      ),
      'lbl_quickcreate_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'bankaccount_c',
            'label' => 'LBL_BANKACCOUNT',
          ),
          1 => 
          array (
            'name' => 'projectname_c',
            'label' => 'LBL_PROJECTNAME',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'company_location',
            'label' => 'LBL_COMPANY_LOCATION',
          ),
          1 => 
          array (
            'name' => 'ratinglead_c',
            'studio' => 'visible',
            'label' => 'LBL_RATINGLEAD',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'skypename_c',
            'label' => 'LBL_SKYPENAME',
          ),
          1 => 
          array (
            'name' => 'identitycard_c',
            'type' => 'dupdetector',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'dateaccount_c',
            'label' => 'LBL_DATEACCOUNT',
          ),
          1 => 
          array (
            'name' => 'birthdate',
            'comment' => 'The birthdate of the contact',
            'label' => 'LBL_BIRTHDATE',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'sex',
            'label' => 'LBL_SEX',
          ),
          1 => 
          array (
            'name' => 'assistant',
            'comment' => 'Name of the assistant of the contact',
            'label' => 'LBL_ASSISTANT',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'countries',
            'label' => 'LBL_COUNTRIES',
          ),
          1 => 
          array (
            'name' => 'list_age',
            'label' => 'LBL_LIST_AGE',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'object_customer',
            'label' => 'LBL_OBJECT_CUSTOMER',
          ),
          1 => 
          array (
            'name' => 'social_account',
            'label' => 'LBL_SOCIAL_ACCOUNT',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'is_single',
            'label' => 'LBL_IS_SINGLE',
          ),
          1 => 
          array (
            'name' => 'do_not_call',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'phone_work',
          ),
          1 => 
          array (
            'name' => 'phone_fax',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'department',
          ),
          1 => 
          array (
            'name' => 'title',
          ),
        ),
        10 => 
        array (
          0 => 
          array (
            'name' => 'alt_address_street',
            'comment' => 'Street address for alternate address',
            'label' => 'LBL_ALT_ADDRESS_STREET',
          ),
          1 => 
          array (
            'name' => 'lead_source_description',
            'comment' => 'Description of the lead source',
            'label' => 'LBL_LEAD_SOURCE_DESCRIPTION',
          ),
        ),
        11 => 
        array (
          0 => 
          array (
            'name' => 'account_code',
            'label' => 'LBL_ACCOUNT_CODE',
          ),
          1 => 
          array (
            'name' => 'ads',
            'label' => 'LBL_ADS',
          ),
        ),
      ),
    ),
  ),
);
;
?>
