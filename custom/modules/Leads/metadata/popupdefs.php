<?php
$popupMeta = array (
    'moduleMain' => 'Lead',
    'varName' => 'LEAD',
    'orderBy' => 'last_name, first_name',
    'whereClauses' => array (
  'status' => 'leads.status',
  'account_name' => 'leads.account_name',
  'assigned_user_id' => 'leads.assigned_user_id',
  'name' => 'leads.name',
  'email' => 'leads.email',
  'phone_mobile' => 'leads.phone_mobile',
  'bussiness_type_c' => 'leads_cstm.bussiness_type_c',
  'lead_source' => 'leads.lead_source',
  'shortname_c' => 'leads_cstm.shortname_c',
  'vatcode_c' => 'leads_cstm.vatcode_c',
  'ratinglead_c' => 'leads_cstm.ratinglead_c',
  'sex' => 'leads.sex',
),
    'searchInputs' => array (
  3 => 'status',
  4 => 'account_name',
  5 => 'assigned_user_id',
  6 => 'name',
  7 => 'email',
  8 => 'phone_mobile',
  9 => 'bussiness_type_c',
  10 => 'lead_source',
  11 => 'shortname_c',
  12 => 'vatcode_c',
  13 => 'ratinglead_c',
  14 => 'sex',
),
    'searchdefs' => array (
  'shortname_c' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_SHORTNAME',
    'width' => '10%',
    'name' => 'shortname_c',
  ),
  'name' => 
  array (
    'type' => 'name',
    'link' => true,
    'label' => 'LBL_NAME',
    'width' => '10%',
    'name' => 'name',
  ),
  'email' => 
  array (
    'name' => 'email',
    'width' => '10%',
  ),
  'phone_mobile' => 
  array (
    'type' => 'phone',
    'label' => 'LBL_MOBILE_PHONE',
    'width' => '10%',
    'name' => 'phone_mobile',
  ),
  'account_name' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ACCOUNT_NAME',
    'width' => '10%',
    'name' => 'account_name',
  ),
  'vatcode_c' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_VATCODE',
    'width' => '10%',
    'name' => 'vatcode_c',
  ),
  'bussiness_type_c' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_BUSSINESS_TYPE',
    'width' => '10%',
    'name' => 'bussiness_type_c',
  ),
  'status' => 
  array (
    'name' => 'status',
    'width' => '10%',
  ),
  'sex' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_SEX',
    'width' => '10%',
    'name' => 'sex',
  ),
  'ratinglead_c' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_RATINGLEAD',
    'width' => '10%',
    'name' => 'ratinglead_c',
  ),
  'assigned_user_id' => 
  array (
    'name' => 'assigned_user_id',
    'type' => 'enum',
    'label' => 'LBL_ASSIGNED_TO',
    'function' => 
    array (
      'name' => 'get_user_array',
      'params' => 
      array (
        0 => false,
      ),
    ),
    'width' => '10%',
  ),
  'lead_source' => 
  array (
    'name' => 'lead_source',
    'width' => '10%',
  ),
),
    'listviewdefs' => array (
  'BUSSINESS_TYPE_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_BUSSINESS_TYPE',
    'width' => '10%',
    'name' => 'bussiness_type_c',
  ),
  'NAME' => 
  array (
    'width' => '30%',
    'label' => 'LBL_LIST_NAME',
    'link' => true,
    'default' => true,
    'related_fields' => 
    array (
      0 => 'first_name',
      1 => 'last_name',
      2 => 'salutation',
    ),
    'name' => 'name',
  ),
  'PHONE_MOBILE' => 
  array (
    'type' => 'phone',
    'label' => 'LBL_MOBILE_PHONE',
    'width' => '10%',
    'default' => true,
    'name' => 'phone_mobile',
  ),
  'EMAIL1' => 
  array (
    'type' => 'varchar',
    'studio' => 
    array (
      'editview' => true,
      'editField' => true,
      'searchview' => false,
      'popupsearch' => false,
    ),
    'label' => 'LBL_EMAIL_ADDRESS',
    'width' => '10%',
    'default' => true,
    'name' => 'email1',
  ),
  'ACCOUNT_NAME' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ACCOUNT_NAME',
    'width' => '10%',
    'default' => true,
    'name' => 'account_name',
  ),
  'STATUS' => 
  array (
    'width' => '10%',
    'label' => 'LBL_LIST_STATUS',
    'default' => true,
    'name' => 'status',
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '10%',
    'label' => 'LBL_LIST_ASSIGNED_USER',
    'default' => true,
    'name' => 'assigned_user_name',
  ),
),
);
