<div class ="seach-body">

            <div class="modal-body text-center">
                <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 text-left">
                                <label><button onclick="openFilter()" class="button"><span class="glyphicon glyphicon-filter"></span></button></label>
								<span style="font-style:italic" id="showPeriod"></span>
                            </div>
							<form>
							 <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 text-left">
                              <span> <input type="text" id="keyword" style="font-style: italic;font-size:13px;width:190px" placeholder="Tìm theo mobile, name, vatcode,..." name="keyword"  onkeypress="Search(event)">
								</span>
								<span id="extDropdown"></span>
								<span id="callButt" style="font-size:26px;color:blue;margin-left:3px;" class="suitepicon suitepicon-action-schedule-call" onclick="clicktocall2()"></span>
							</div>
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 text-left" style="margin-top:4px;">
							<span style="margin-right:5px;"><input type="checkbox" id="customerStatusBox" name="customerStatus"  onclick="changeCustomerStatus()"></span>
                                <select name="period1" id="period1" name="time" onclick="FilterTime(this.value)" >
<option value="default">------</option>                                    
<option value="this_week">7 ngày qua</option>
<option value="this_month">Tháng này</option>
<option value="this_year">Năm này</option>
<option style="display:none" id="30days" value="30_days">Kế hoạch 30 ngày tới</option>
                                </select>
								<span style="font-style: italic;"  id="customerStatus">( KH không được chăm sóc )</span>
                            </div>
							
							 <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1 " style="margin-top:2px;margin-left:-3px">
                              <input type="button"  onclick="refreshPage()" class="button" value="Clear">
                            </div>
                </div>
                
                <div class="row">
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left">
                    </div>
                    
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left" style="margin-top:10px">
                        
                    </div>
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left">
                       
                    </div>
                </div>
				
				
            
        </div>

</form>
 <div class="row">
 <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-left">
 <div>
 <input style="margin-left:5px" type="checkbox" id="NewOpt" value="Dead" onclick="showNew();"> KHNT Mới
 </div>
 <div>
<input style="margin-left:5px" type="checkbox" id="deadOpt" value="Dead" onclick="showDead();"> KHTN ngừng chăm sóc
</div>
</div>
<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-left">
<div>
 <input style="margin-left:5px" id="myCustomer" type="checkbox" value="myCustomer" onclick="showMyCustomer();"> Khách hàng của tôi
 </div>
</div>
<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-left">
<div id="my-act-div">
 


 </div>
</div>
<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-left">
 
</div>
</div>
</div>
{literal}
    <script type="text/javascript">
	

	function filterCustomer(){
	var myCusVal;
	var myCus=document.getElementById('myCustomer');
	if(myCus.checked == true){
		myCusVal= true;
	}else{
		myCusVal= false;
	}
	var valueStatus = document.getElementById("myAct").value;
	var myCus=document.getElementById('myCustomer');
	var newOpt=document.getElementById('NewOpt');
	var deadOpt=document.getElementById('deadOpt');
	if(newOpt.checked == true){
		var New = true;
	}
	if(deadOpt.checked == true){
		var Dead = true;
	}
	SUGAR.ajaxUI.showLoadingPanel();
	$.ajax({
            type: "POST",
            url: "index.php?entryPoint=retrieve_dash_page",
			data:{
				page_id : 1,
				act:valueStatus
			},
			dataType: "html",
            success: function(data) {
				$('#tab_content_1').html(data);
					 $(document).ready(function(){
						var abf = setInterval(function(){
							if(document.getElementById("myAct")){
								document.getElementById("myAct").value = valueStatus;
								clearInterval(abf);
							}
						},1000);
						if(myCusVal == true){
							document.getElementById('myCustomer').checked = true;
						}
						
						if(New == true){
							document.getElementById('NewOpt').checked = true;
							
						}
						if(Dead == true){
							document.getElementById('deadOpt').checked =true;
						}
					  });
					 SUGAR.ajaxUI.hideLoadingPanel();
			},
                error: function (error) {
                    console.log(error);
                }
        });
		
	}
	$(document).ready(function(){
	$.ajax({
            type: "GET",
            url: "index.php?entryPoint=getLanguageForDropdown",
			data:{
				id:"status_activities_list"
			},
			dataType:"json",
            success: function(data) {
			var arrayStatus= data[0]["language"];
<!--			console.log(arrayStatus); -->
			const sortable = Object.entries(arrayStatus)
    .sort(([,a],[,b]) => a-b)
    .reduce((r, [k, v]) => ({ ...r, [k]: v }), {});

<!-- console.log(sortable); -->
	
			 var html = '';
			 html+='<select style="margin-left:5px" id="myAct" onchange="filterCustomer();">';
			 html+='<option value="All">';
					html+='</option>';
			 for(let key in arrayStatus){
				if(arrayStatus[key] != ""){
					html+='<option value='+key+'>';
					html+=arrayStatus[key];
					html+='</option>';
				}
			 }
			  html+='</select>';
			  $("#my-act-div").html(html);
			},
                error: function (error) {
                    console.log("error");
                }
        });
		
		
		$.ajax({
            type: "POST",
            url: "index.php?entryPoint=getExt",
            success: function(data) {
			var data = JSON.parse(data);
				var html = "";
				html += '<select id="extList" style="width:250px">';
				html+='<option value="" selected="selected">----</option>';
				for(var i=0;i<data.length;i++){
					html+='<option value="'+data[i].description+'" >'+data[i].description+'-'+data[i].name+'</option>';
					
				}
				html += "</select>";
				$("#extDropdown").html(html);
				$(document).ready(function(){
					$("#extDropdown").on("change",function(){
						$("#callButt").attr("onclick","");
						$("#callButt").on("click",function(){
							if($("#keyword").val() != null && $("#keyword").val() != ''){
								var phoneNumber = $("#keyword").val();
							}else{
								if($("#extList").val() != null && $("#extList").val() != ''){
									phoneNumber = $("#extList").val();
								}
							}
							sec=0;
							hr=0;
							min=0;
							$("#endCall").attr('onclick','handleButtonHangupClick()');
							 document.getElementById('input-phone').value = phoneNumber;
								handleButtonCallClick();
								setCookie('phone',phoneNumber);
								setCookie("alreadyCall","");
								var html='';
									html+='<a type="button" id="quick_create_meeting" title="Quick Create Meeting" class="btn btn-primary action-btn1" ><i class="fa fa-users" aria-hidden="true"></i></a>';
									html+='<a type="button" id="quick_create_opportunities" title ="Quick Create Opportunities" class="btn btn-primary action-btn2"><i class="fa fa-usd" ></i></a>';
									html+='<a type="button" id="quick_create_case" title ="Quick Create Case" class="btn btn-primary action-btn2"><i class="fa fa-folder" aria-hidden="true"></i></a>';
									html+='<a type="button" style="width: auto" id="save_popup" title ="Lưu" class="btn btn-primary">Lưu thông tin</a>';
									html+='<a type="button" id="eCall" onclick="handleButtonHangupClick2()" style="width: auto;margin-right:3px;" id="Decline" title ="Lưu" class="btn btn-primary">Kết thúc</a>'
									html+='<a type="button" id="callTransfer" onclick="handleButtonTransferClick()" style="width: auto;display:none"  title ="chuyển tiếp" class="btn btn-primary">Chuyển tiếp</a>';
								
								
								var type1=document.getElementsByName("parent_type")[0];
								if(type1 !=null && type1 != undefined){
									type2=type1.value;
								}else{
									type2=GetURLParameter("module");
								}
								$("a:not('.utilsLink')").attr("target","_blank");
								$(".panel-body a").attr("target","_self");
								$(".panel-body span").attr("target","_self");
								$("span").attr("target","_blank");
								$('#detailModal').find('.modal-body').html('');
								$('#detailModal').modal('hide');
								stop=0;
								document.getElementById("time_count").innerHTML='<span id="hrs">00</span>:' + '<span id="mins">00</span>:' + '<span id="sec">00</span>';
								$("#direction").html("Cuộc gọi đi");
								$("#statuscall").html("");
								var label=document.getElementById("with-label");
								var accountLabel=label.getAttribute('title');
								var user = document.getElementById("userextension").value;
								setCookie("phoneNum",phoneNumber);
								$("#actionButton").html('');	
								$("#show").html("Đang gọi "+phoneNumber);
								$("#phoneForm").css('display', 'block');	
								$("#myForm").css("display", "block");
								$("#actionButton").html(html);
								$("#actionButton").css("display", "block");
								$("#phone-mobile-popup").html(phoneNumber);
								$("#userextension-popup").html('<span class="glyphicon glyphicon-user"></span>'+accountLabel);
								$("#tenkh").html("Không xác định");	
								
							
							});
						});
					});
				},
                error: function (error) {
                    console.log("bbbb");
                }
        });
	});
		 </script>
{/literal}