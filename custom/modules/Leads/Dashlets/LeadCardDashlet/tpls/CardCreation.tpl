
<div id="creationModal" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" style="width: 80%">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header formHeader">
                <button type="button" class="close"  onclick="closeModal('creationModal')">&times;</button>
                <h3 class="modal-title text-uppercase text-black">{$cardMod.LBL_ADD_NEW_CARD}</h3>
            </div>
            <div class="modal-body">
             <p class="error" id="error" style="display:none;"></p>
             <div id="frmBody"></div>
            </div>
            <div class="modal-footer">
                 <button type="button" class="btn btn-default" id="closeButton" onclick="closeModal('creationModal')">Close</button>
            </div>
        </div>

    </div>
</div>