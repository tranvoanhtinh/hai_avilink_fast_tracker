
<div id="activityCreationModal" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header formHeader">
                <button type="button" class="close" onclick="closeModal('activityCreationModal')">&times;</button>
                <h3 class="modal-title text-uppercase text-black"></h3>
            </div>
            <div class="modal-body" style="padding-top: 0px!important;">
            </div>
            <!--<div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div> -->
        </div>

    </div>
</div>
<style>
{literal}
    #activityCreationModal input,  #activityCreationModal #status, #activityCreationModal #direction, #activityCreationModal #reminder_add_btn, #activityCreationModal #parent_type,  #activityCreationModal button{
        float: left !important;
        margin-right: 10px !important;
    }
    #activityCreationModal .buttons {
        margin-top:20px !important;
        height: 40px !important;
    }
    #activityCreationModal #name {
        width: 100% !important;
    }
{/literal}
</style>