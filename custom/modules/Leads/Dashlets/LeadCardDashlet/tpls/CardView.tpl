<div id="detailModal" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" style="width: 80%">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header formHeader">
                <button type="button" class="close" onclick="closedetailModal()">&times;</button>
                <h3 class="modal-title text-uppercase text-black">{$cardMod.LBL_DETAIL_LABEL}</h3>
            </div>
            <div class="modal-body" style="height: 500px; overflow-y: auto;overflow-x: hidden;">

            </div>
            <div class="modal-footer" style=" padding: 5px 5px 5px 5px !important;">
                <button type="button" class="btn btn-default" onclick="closedetailModal()">Close</button>
            </div>
        </div>

    </div>
</div>
