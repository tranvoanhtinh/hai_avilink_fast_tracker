<div class="col-lg-3 col-sm-6 col-xs-12">
				<div class="panel leadColumn" >
					<div class="panel-heading leadColumnHeader text-left text-uppercase text-black" style="background-color: {$config.column_background_color} !important;"><b>{$config.column_title}</b><span onclick="showCreationModal('{if $config.key == 'Transaction' || $config.key == 'Converted' }Accounts{else}Leads{/if}', '{$config.key}')" {if $config.key != 'Transaction'} class="glyphicon glyphicon-plus add-new-card" {/if}></span></div>
					<div class="panel-body ">
						<div class="row summaryColumn" style="background-color: {$config.column_background_color} !important;">
							<div class="row totalAmountColumn">
								<div class="col-xs-6 col-sm-6 col-md-6 text-center totalLead" data-toggle="tooltip" data-placement="top" title="{$config.total_records}">
									<label id="total_records" class="text-black">{$config.total_records}</label>
								</div>

								<div class="col-xs-6 col-sm-6 col-md-6 text-left totalAmount">
									<div class="row text-black" style="font-size: 12px;">{$config.total_amount_title}</div>
									<div class="row">
										<b style="font-size: 13px; width: 100%; display: block; text-align: left;" class="text-right text-black" id="total_amount">{ $config.amount_formatted }</b>
										{if isset($config.amount_rest_formatted)}
										<b style="color:blue;font-size: 13px; width: 100%; display: block; text-align: left;" class="text-right" id="total_amount_rest">({ $config.amount_rest_formatted })</b>
										{/if}
									</div>
								</div>
							</div>
							<div class="row activityColumn">
								<div class="col-xs-3" title="{$activity_mods.Calls}"><span class="suitepicon suitepicon-action-schedule-call text-black"></span> <span class="badge notification-counter total_call">{$config.total_call}</span> </div>
								<div class="col-xs-3" title="{$activity_mods.Email_Normal}"><span class="suitepicon suitepicon-admin-email-settings text-black"></span> <span class="badge notification-counter total_email ">{$config.total_email}</span></div>
								<div class="col-xs-3" title="{$activity_mods.Meetings}" ><span class="suitepicon suitepicon-action-schedule-meeting text-black"></span><span class="badge notification-counter total_meeting">{$config.total_meeting}</span></div>
								<div class="col-xs-3" title="{$activity_mods.Email_Quotation}"><span class="suitepicon suitepicon-admin-inbound-email text-black"></span><span class="badge notification-counter total_email_quotation">{$config.total_email_quotation}</span></div>
							</div>
						</div>
						<div class="row searchRow">
							<div class="col-sm-1 col-xs-1 col-lg-1 col-md-1 arrow-icon">
								<span class="suitepicon suitepicon-action-sorting-descending text-black" style="display: {if $config.is_asc} none; {else} block;{/if}" title="Sort"></span>
								<span class="suitepicon suitepicon-action-sorting-ascending text-black" style="display: {if !$config.is_asc} none; {else} block;{/if}" title="Sort"></span>
							</div>
							<div class="col-sm-4 col-xs-4 col-lg-4 col-md-4 card-order-column">
								<select name="{$config.key}_order_by" id="{$config.key}_order_by" class="card_order_by text-black" onchange="sortColumn(event, this, '{$config.key}');">
									{$config.order_by_options}
								</select>
							</div>
							<div class="col-sm-6 col-xs-6 col-lg-6 col-md-6">
								<input type="text" class="keyword searchColumn"  placeholder="{$config.search_title}" onkeypress="searchLead(event, this, '{$config.key}')" value="{$config.keyword}">
							</div>
							<div class="col-sm-1 col-xs-1 col-lg-1 col-md-1 filter-icon">
								<a href="javascript:void(0);" style="color: unset!important;" is_asc="{$config.is_asc}" id="{$config.key}_sort_direction" class="direction" onclick="sortColumn(event, this, '{$config.key}');">
										<span class="glyphicon glyphicon-sort-by-alphabet text-black" id="direction_az"></span>
								</a>
							</div>
						</div>
						<div class="cards {if $config.key != 'Transaction'}drop-target{/if} style-1" onscroll="loadMore(this)" status="{$config.key}" total-records="{ $config.total_records }" total-amount="{ $config.amount }">
							{$config.content}
						</div>
					</div>
				</div>
</div>
{literal}
    <script type="text/javascript">
       var x = document.getElementById("In Process_order_by");
		x.remove(2);
	   $("#Converted_order_by option[value='status']").remove();
	   $("#Transaction_order_by option[value='status']").remove();
    </script>
{/literal}