<div id="card_{$lead.id}" lead-status="{$lead.status}" lead-id="{$lead.id}"  lead-amount='{$lead.amount}' class="panel leadCard" style="position: relative; border-bottom: 2px solid {$lead.bottom_color}">
    <div class="panel-body" style="padding-left: 10px;">
        <div class="row">
            <div class="col-sm-11 col-sm-11 col-xs-11">
                <div class="row">
                    <div class="col-xs-10 col-sm-10 col-xs-10 text-left" id="card_name">
                        <a href="javascript:void(0);" onclick="openDetailView('{$lead.id}', '{$lead.module}', '{$lead.account_name}', '{$lead.account_id}', '{$lead.status}')" style="color: unset !important;">
                            <b class="text-black">{$lead.account_name}</b>
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-10 col-sm-10 col-xs-10 text-left" id="card_name">
                        <a href="javascript:void(0);" onclick="openDetailView('{$lead.id}', '{$lead.module}', '{$lead.account_name}', '{$lead.account_id}', '{$lead.status}')" style="color: unset !important;">
						   <p class="text-black">{$lead.contact_name}</p>
						   
											   
                        </a>
                    </div>
                </div> 
				 <div class="row">
                    <div class="col-xs-10 col-sm-10 col-xs-10 text-left" id="card_phone">
                        <a href="javascript:void(0);" onclick="openDetailView('{$lead.id}', '{$lead.module}', '{$lead.account_name}', '{$lead.account_id}', '{$lead.status}')" style="color: unset !important;">
                            <p class="text-black">{$lead.phone}</p>
                        </a>
                    </div>
					
                </div>
				
				 <div class="row">
                    <div class="col-xs-10 col-sm-10 col-xs-10 text-left" id="card_email">
                        <a href="javascript:void(0);" onclick="openDetailView('{$lead.id}', '{$lead.module}', '{$lead.account_name}', '{$lead.account_id}', '{$lead.status}')" style="color: unset !important;">
                            <p class="text-black">{$lead.email}</p>
                        </a>
                    </div>
					
                </div>
				<div class="row">
                    <div class="col-xs-10 col-sm-10 col-xs-10 text-left" id="card_project">
                        <a href="javascript:void(0);" onclick="openDetailView('{$lead.id}', '{$lead.module}', '{$lead.account_name}', '{$lead.account_id}', '{$lead.status}')" style="color: unset !important;">
                            <p class="text-black">{$lead.project_name}</p>
                        </a>
                    </div>
					
					
                </div>
				{if $lead.status == 'New'}
				<div class="row">
                    <div class="col-xs-10 col-sm-10 col-xs-10 text-left" id="card_status">
                        <a href="javascript:void(0);" onclick="openDetailView('{$lead.id}', '{$lead.module}', '{$lead.account_name}', '{$lead.account_id}', '{$lead.status}')" style="color: unset !important;">
                            <p style="color:#2EFE2E;font-weight:bold">{$lead.status}</p>
                        </a>
                    </div>
                </div>
				{/if}
				
				{if $lead.status == 'Dead'}
				<div class="row">
                    <div class="col-xs-10 col-sm-10 col-xs-10 text-left" id="card_status">
                        <a href="javascript:void(0);" onclick="openDetailView('{$lead.id}', '{$lead.module}', '{$lead.account_name}', '{$lead.account_id}', '{$lead.status}')" style="color: unset !important;">
                            <p class="text-black" style="color:black;font-weight:bold;text-decoration: line-through;">{$lead.status}</p>
                        </a>
                    </div>
                </div>
				{/if}
				
				<div class="row">
                    <div class="col-xs-10 col-sm-10 col-xs-10 text-left" id="card_servicetype">
                        <a href="javascript:void(0);" onclick="openDetailView('{$lead.id}', '{$lead.module}', '{$lead.account_name}', '{$lead.account_id}', '{$lead.status}')" style="color: unset !important;">
                            <p style="font-style:italic">{$lead.servicetype}</p>
                        </a>
                    </div>
                </div>
				
				 <div class="row">
                    <div class="col-xs-10 col-sm-10 col-xs-10 text-left" id="card_assigned">
                        <a href="javascript:void(0);" onclick="openDetailView('{$lead.id}', '{$lead.module}', '{$lead.account_name}', '{$lead.account_id}', '{$lead.status}')" style="color: unset !important;">
                            <p class="text-black"><span style="background-color:#FF8000;color:white;">{$lead.assigned}</span>-<span>{$lead.date_created}</span></p> 
                        </a>
                    </div>
					
                </div>
				 <div class="row">
                    <div class="col-xs-10 col-sm-10 col-xs-10 text-left" id="card_status_act">
                        <a href="javascript:void(0);" onclick="openDetailView('{$lead.id}', '{$lead.module}', '{$lead.account_name}', '{$lead.account_id}', '{$lead.status}')" style="color: unset !important;">
                            <p class="text-black"><span style="color:blue;font-weight:bold">{$lead.status_act}</span></p> 
                        </a>
                    </div>
					
                </div>
				 <div class="row">
                    <div class="col-xs-10 col-sm-10 col-xs-10 text-left" id="card_name">
                        <a href="javascript:void(0);" onclick="openDetailView('{$lead.id}', '{$lead.module}', '{$lead.account_name}', '{$lead.account_id}', '{$lead.status}')" style="color: unset !important;">
						{if $lead.lastdateactive_c !=''}
                         <span class="suitepicon suitepicon-module-notes" style="display:inline-block;color:#000033;"></span>&nbsp
						  <i class="text-blue" style="font-size: 14px; color:#306EFF;">{$lead.status_description}</i>
							
							<i class="text-blue" style="font-size: 11.5px; color:#306EFF;"> - { $lead.lastdateactive_c}</i>
							 <br>
							 {/if}
						
						{if $lead.lastdateactive_c_1 !=''}
                         <span class="suitepicon suitepicon-action-schedule-call" style="display:inline-block;color:#000033;"></span>&nbsp
						  <i class="text-blue" style="font-size: 14px; color:#306EFF;">{$lead.status_description_1}</i>
							
							<i class="text-blue" style="font-size: 11.5px; color:#306EFF;"> - { $lead.lastdateactive_c_1}</i>
							 <br>
							 {/if}
							
							 {if $lead.lastdateactive_c_2 !=''}
							<span class="suitepicon suitepicon-action-schedule-meeting" style="display:inline-block;color:#000033;"></span>&nbsp
							 <i class="text-blue" style="font-size: 14px; color:#306EFF;">{$lead.status_description_2}</i>
							
							<i class="text-blue" style="font-size: 11.5px; color:#306EFF;"> - { $lead.lastdateactive_c_2}</i>
							 {/if}
                        </a>
                    </div>
					
                </div>
				 <div class="row">
                    <div class="col-xs-10 col-sm-10 col-xs-10 text-left" id="card_name">
                        <a href="javascript:void(0);" onclick="openDetailView('{$lead.id}', '{$lead.module}', '{$lead.account_name}', '{$lead.account_id}', '{$lead.status}')" style="color: unset !important;">
						{if $lead.plandateactive_c !=''}
                           <span class="suitepicon suitepicon-action-schedule-call" style="display:inline-block;color:#00CC00;" ></span>
						    
							<i class="text-red plandesc-{$lead.id}" style="font-size: 14px; color:#ff0000;" >{$lead.plandescription_c}</i>
							
							<i class="text-blue plandate-{$lead.id} " style="font-size: 11.5px; color:#ff0000;" > - { $lead.plandateactive_c}</i>
							<br>
							 {/if}
							 
							 {if $lead.plandateactive_c_2 !=''}
							<span class="suitepicon suitepicon-action-schedule-meeting" style="display:inline-block;color:#00CC00;" ></span>
							
							<i class="text-red plandesc-{$lead.id}" style="font-size: 14px; color:#ff0000;" >{$lead.plandescription_c_2}</i>
							
							<i class="text-blue plandate-{$lead.id} " style="font-size: 11.5px; color:#ff0000;" > - { $lead.plandateactive_c_2}</i>
							 {/if}
                        </a>
                    </div>
					
                </div>
				
                <div class="row">
                    <div class="col-xs-7 col-sm-7 col-xs-7 text-left" id="card_amount">
                        {$lead.amount_formatted}
                    </div>
                    <div class="col-xs-5 col-sm-5 col-xs-5 " id="card_activity" >
                        <div class="row">
						 {if $lead.status != 'Converted' && $lead.status != 'Transaction'}
                            <div class="col-xs-3" title="{$activity_mods.Calls}"><span onclick="doAction1('Calls','{$lead.id}','{$lead.module}','{$lead.name}', '{$lead.account_id}','{$lead.status}', 'form_SubpanelQuickCreate_Calls', 'Calls_subpanel_save_button');" style="color:blue;" class="  {if $lead.calls} text-black{/if}"><img style= "margin-top:-11px;" src='custom/modules/Leads/Dashlets/LeadCardDashlet/css/calls.png' width='13' height='13'></span></div>
                            
                            <div class="col-xs-3" title="{$activity_mods.Meetings}" ><span onclick="doAction1('Meetings','{$lead.id}','{$lead.module}','{$lead.name}', '{$lead.account_id}','{$lead.status}', 'form_SubpanelQuickCreate_Meetings', 'Meetings_subpanel_save_button');" style="color: blue" class="{if $lead.meetings} text-black{/if}"><img style= "margin-top:-11px;" src='custom/modules/Leads/Dashlets/LeadCardDashlet/css/icon_meeting.png' width='13' height='13'></span></div>
							{/if}
							{if $lead.status == 'Converted' || $lead.status == 'Transaction'}
							
							 <div class="col-xs-2" title="{$activity_mods.Calls}"><span onclick="doAction1('Calls','{$lead.id}','{$lead.module}','{$lead.account_name}', '{$lead.account_id}','{$lead.status}', 'form_SubpanelQuickCreate_Calls', 'Calls_subpanel_save_button');" style="color:blue" class="{if $lead.calls} text-black{/if}"><img style= "margin-top:-11px;" src='custom/modules/Leads/Dashlets/LeadCardDashlet/css/calls.png' width='13' height='13'></span></div>
                            
                            <div class="col-xs-2" title="{$activity_mods.Meetings}" ><span onclick="doAction1('Meetings','{$lead.id}','{$lead.module}','{$lead.account_name}', '{$lead.account_id}','{$lead.status}', 'form_SubpanelQuickCreate_Meetings', 'Meetings_subpanel_save_button');" style="color: blue" class="{if $lead.meetings} text-black{/if}"><img style= "margin-top:-11px;" src='custom/modules/Leads/Dashlets/LeadCardDashlet/css/icon_meeting.png' width='13' height='13'></span></div>
							<div class="col-xs-2" title="Opportunity" ><span style="color: blue" onclick="showOpportunityModal('{$lead.id}','{$lead.account_name}')" style="color: blue" class="{if $lead.opportunities} text-black{/if}"><img style= "margin-top:-11px;" src='custom/modules/Leads/Dashlets/LeadCardDashlet/css/icon_opportunities.png' width='13' height='13'></span></div>
							
							
                             
							 {/if}
							 {if $lead.status == 'Converted' || $lead.status == 'Transaction' }
							<div class="col-xs-2" title="Case" ><span style="color: blue" onclick="doAction1('Cases','{$lead.id}','{$lead.module}','{$lead.account_name}', '{$lead.account_id}','{$lead.status}', 'form_SubpanelQuickCreate_Cases', 'Cases_subpanel_save_button');"><img style= "margin-top:-11px;" src='custom/modules/Leads/Dashlets/LeadCardDashlet/css/ticket.jpg' width='13' height='13'></span></div>
							{else}
							<div class="col-xs-3" title="Notes" ><span onclick="doAction1('Notes','{$lead.id}','{$lead.module}','{$lead.account_name}', '{$lead.account_id}','{$lead.status}', 'form_SubpanelQuickCreate_Notes', 'Notes_subpanel_save_button');" style="color: blue"><img style= "margin-top:-11px;" src='custom/modules/Leads/Dashlets/LeadCardDashlet/css/icon_note.png' width='13' height='13'></span></div>
							{/if}

					   {if $lead.status != 'Converted' && $lead.status != 'Transaction'}
							 <div class="col-xs-3" title="{$activity_mods.Email_Normal}" ><span onclick="$(document).openComposeViewModal(this);" 
							 data-module="{$lead.module}" data-record-id="{$lead.id}" data-module-name="{$lead.contact_name}"
							 data-email-address="{$lead.email}"><img style= "margin-top:-11px;" src='custom/modules/Leads/Dashlets/LeadCardDashlet/css/icon_email.png' width='13' height='13'></span></div>
							
							 {/if}
							  {if $lead.status == 'Converted'}
							  {*
							 <div class="col-xs-2" title="{$activity_mods.Email_Normal}" ><span onclick="$(document).openComposeViewModal(this);"  
							 data-module="{$lead.module}" data-record-id="{$lead.id}" data-module-name="{$lead.contact_name}"
							 data-email-address="{$lead.email}"><img style= "margin-top:-11px;" src='custom/modules/Leads/Dashlets/LeadCardDashlet/css/icon_email.png' width='13' height='13'></span></div>
							 *}
								<div class="col-xs-2" title="Contact" ><span style="color: blue" onclick="doAction1('Contacts','{$lead.id}','{$lead.module}','{$lead.account_name}', '{$lead.account_id}','{$lead.status}', 'form_SubpanelQuickCreate_Contacts', 'Contacts_subpanel_save_button');"><img style= "margin-top:-11px;" src='custom/modules/Leads/Dashlets/LeadCardDashlet/css/icon_contact.png' width='13' height='13'></span></div>
							 {/if}
							 {if $lead.status == 'Transaction'}
							 <div class="col-xs-2" title="Billing" ><span style="color: blue" onclick="doAction1('bill_Billing','{$lead.id}','{$lead.module}','{$lead.account_name}', '{$lead.account_id}','{$lead.status}', 'form_SubpanelQuickCreate_bill_Billing', 'bill_Billing_subpanel_save_button');"><img style= "margin-top:-11px;" src='custom/modules/Leads/Dashlets/LeadCardDashlet/css/icon_billing.png' width='13' height='13'></span></div>
							 {/if}
							  {if $lead.status == 'Converted' || $lead.status == 'Transaction' }
							   <div class="col-xs-2" title="Notes" ><span onclick="doAction1('Notes','{$lead.id}','{$lead.module}','{$lead.account_name}', '{$lead.account_id}','{$lead.status}', 'form_SubpanelQuickCreate_Notes', 'Notes_subpanel_save_button');"><img style= "margin-top:-11px;" src='custom/modules/Leads/Dashlets/LeadCardDashlet/css/icon_note.png' width='13' height='13'></span></div>
							  {/if}

							 
						</div>
                    </div>
                </div>
            </div>
            <div class="col-sm-1 col-sm-1 col-xs-1">
                {if $lead.is_case_opening }
                    <div class="caseOpening">
                        !
                    </div>
                {/if}
                <a id="card_action" style="color: #554d66 !important; margin-top: 12px !important;" class="glyphicon glyphicon-option-vertical" onclick="clickCardMenu(this)" href="javascript:void(0);"></a>
                <ul class="card-actions">
                   <!-- dropdown menu links -->
					{if $lead.status == 'New' || $lead.status == 'In Process'}
					<li class="card-action" id="clickCall"><a href="javascript:void(0);" onclick="clicktocall('{$lead.phone}', '{$lead.contact_name}', '{$lead.id}', '{$lead.account_name}','{$lead.module}','{$lead.status}','{$lead.account_id}')"><span style="display:inline-block;margin-top:2px;" class="suitepicon suitepicon-action-schedule-call text-black"></span></a></li>
					{/if}
					{if $lead.status == 'Converted' || $lead.status == 'Transaction'}
					<li class="card-action" id="clickCall"><a href="javascript:void(0);" onclick="clicktocall('{$lead.phone}', '{$lead.primarycontact_c}', '{$lead.id}', '{$lead.account_name}','{$lead.module}','{$lead.status}','{$lead.account_id}')"><span style="display:inline-block;margin-top:2px;" class="suitepicon suitepicon-action-schedule-call text-black"></span></a></li>
					{/if}
                    <li class="card-action" id="viewDetailOption"><a href="javascript:void(0);" onclick="openDetailView('{$lead.id}', '{$lead.module}', '{$lead.account_name}', '{$lead.account_id}', '{$lead.status}')"><span class="glyphicon glyphicon-eye-open text-black"></span></a></li>
                    
					{if $lead.status == 'New' || $lead.status == 'In Process'}
                    <li class="card-action" id="failLead"><a href="javascript:void(0);" onclick="changeToDead('{$lead.id}', this)"><span class="glyphicon glyphicon-ban-circle text-black"></span></a></li>
                    {/if}
					<li class="card-action"><a href="javascript:void(0);" onclick="customerRei('{$lead.id}', '{$lead.module}')"><span style="display:inline-block;margin-top:2px;" class="suitepicon suitepicon-action-edit text-black"></span></a></li>
                </ul>
            </div>
        </div>
    </div>
    {*<div class="panel-footer">*}
        {*<span class="pull-left"></span>*}
        {*<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>*}
        {*<div class="clearfix"></div>*}
    {*</div>*}
</div>