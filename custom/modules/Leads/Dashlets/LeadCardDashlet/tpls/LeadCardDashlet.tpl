
<link rel="stylesheet" type="text/css" href="custom/modules/Leads/Dashlets/LeadCardDashlet/css/LeadCardDashlet.css">

<script type="text/javascript" src="custom/modules/Leads/Dashlets/LeadCardDashlet/js/LeadCardDashlet.js"></script>
<script type="text/javascript" src="jssource/src_files/include/SubPanel/SubPanelTiles.js"></script>
<div class="leadCardContainer" style="padding-top:5px;margin-bottom:-20px;">
{include file="custom/modules/Leads/Dashlets/LeadCardDashlet/tpls/Search.tpl"}
	<div class="row">
		<div class="col-xs-6 col-sm-4 col-md-4 text-left" style="margin-left: 5px;">
			
		</div>
		<div class="col-xs-6 col-sm-4 col-md-4 text-left" style="margin-left: 5px;text-align: center;">
			<span class="home-spinner" style="display: none"><img src="themes/SuiteP/images/loading.gif" width="60" height="60" align="baseline" border="0" alt=""></span>		</div>
	</div>
		<div class="row" >
			{$dashlet_content}
		</div>

	<!-- Detail modal-->

	{include file="custom/modules/Leads/Dashlets/LeadCardDashlet/tpls/CardView.tpl"}

	<!-- Actions template-->
	{include file="custom/modules/Leads/Dashlets/LeadCardDashlet/tpls/Actions.tpl"}
	<!-- End-->

	<!-- Filter popup -->
	{include file="custom/modules/Leads/Dashlets/LeadCardDashlet/tpls/Filter.tpl"}
	<!-- Card creation -->
	{include file="custom/modules/Leads/Dashlets/LeadCardDashlet/tpls/CardCreation.tpl"}

	<!-- Activity creation -->
	{include file="custom/modules/Leads/Dashlets/LeadCardDashlet/tpls/ActivityCreation.tpl"}
</div>

{*<div class="MTSSpinner">*}
	{*<img src="themes/SuiteP/images/loading.gif" width="48" height="48" align="baseline" border="0" alt="">*}
{*</div>*}
