{foreach from=$fields item=field}
<div class="row">
    <div class="col-xs-12 col-sm-3 col-md-3 text-left">
    <label>{$field.label}</p>
    </div>
    <div class="col-xs-12 col-sm-9 col-md-9 detail-view-field text-left">
    {$field.html}
        {if $field.editable}
            <div class="row">
                <div class="col-xs-11 col-sm-11 col-md-11">
                    <div id="editForm" style="display: none;">
                        <textarea name="{$field.name}_editable" id="{$field.name}_editable" style="width: 100%">{$field.value}</textarea>
                        <a href="javascript:void(0);" class="btn-save" onclick="quickSave('{$field.id}', '{$field.module}', '{$field.name}', this)"><span class="glyphicon glyphicon-ok"></span></a><a class="btn-cancel" href="javascript:void(0);" onclick="cancelEdit(this)" style="margin-left: 10px;"><span class="glyphicon glyphicon-remove"></span></a>
                        <span class="spinner" style="display: none"><img src="themes/SuiteP/images/loading.gif" width="30" height="30" align="baseline" border="0" alt=""></span>
                    </div>
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1">
                    <div class="row">
                        <a href="javascript:void(0);" style="float: right;" class="edit-button" onclick="showEditForm('{$field.module}', '{$field.name}', this)"><span class="glyphicon glyphicon-pencil"></span></a>
                    </div>
                    <div class="row">
                        {if ($field.module == 'Calls' || $field.module == 'Meetings') && !$field.is_completed}
                            <a href="javascript:void(0);" style="float: right;" id="btnCloseActivity" title="{$APP.LBL_CLOSE}" onclick="closeActivity('{$field.id}', '{$field.module}', 'Held', this)"><span class="glyphicon glyphicon-remove"></span></a><span class="spinner" style="display: none;"><img src="themes/SuiteP/images/loading.gif" width="40" height="40" align="baseline" border="0" alt=""></span>
                        {/if}
                    </div>
                </div>
            </div>
        {/if}
    </div>
</div>
{/foreach}