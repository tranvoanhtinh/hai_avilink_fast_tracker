SUGAR.ajaxUI.showLoadingPanel = function() {
        SUGAR.ajaxUI.loadingPanel = new YAHOO.widget.Panel("ajaxloading",{
            width: "60px",
            fixedcenter: true,
            close: false,
            draggable: false,
            constraintoviewport: false,
            modal: true,
            visible: false
        });
        SUGAR.ajaxUI.loadingPanel.setBody('<div id="loadingPage" align="center" style="vertical-align:middle;"><img src="themes/SuiteP/images/loading.gif" width="40" height="40" align="baseline" border="0" alt=""></div>');
        SUGAR.ajaxUI.loadingPanel.render(document.body);
    if (document.getElementById('ajaxloading_c'))
        document.getElementById('ajaxloading_c').style.display = '';
    SUGAR.ajaxUI.loadingPanel.show();
};
var updating = false;
var is_opening = false;
var is_loadmore = false;
var is_searching = false;
var old_column = null;
var subpanel_is_loaded = true;
var beginnn = 1;
if (module_sugar_grp1 == undefined) {
    var module_sugar_grp1 = 'Leads';
}

initDragAndDrop();

function changeLeadStatus(id, status, cb) {
    if (!updating) {
        updating = true;
        SUGAR.ajaxUI.showLoadingPanel();
        setTimeout(function () {
            $.ajax({
                type: "POST",
                url: "index.php?module=Leads&action=MTS_ChangeStatus&sugar_body_only=1",
                data: {"id": id, "status": status},
                dataType: "JSON",
                success: function (response) {
                    if (response.is_updated) {
                        $('#card_' + id).attr('lead-status', status);
                        $('#card_' + id).css("border-color", column_colors[status]);
                        updating = false;
                        updateSumary();
                        if (typeof cb == "function") {
                            cb(true);
                        }
                    }
                    SUGAR.ajaxUI.hideLoadingPanel();
                },
                error: function (error) {
                    alert(error.message);
                    updating = false;
                    updateSumary();
                    if (typeof cb == "function") {
                        cb(true);
                    }
                    SUGAR.ajaxUI.hideLoadingPanel();
                }
            });
        }, 300);
    }
}

function loadMore(col) {
    if ($(col).scrollTop() + $(col).innerHeight() + 5 >= $(col)[0].scrollHeight) {
        if (!is_loadmore) {
            is_loadmore = true;
            SUGAR.ajaxUI.showLoadingPanel();
            setTimeout(function () {
                var exclude_ids = [];
                var card_leads = $(col).find('.leadCard');
                for (var i = 0; i < card_leads.length; i++) {
                    var lead_id = $(card_leads[i]).attr('lead-id');
                    if (!!lead_id) {
                        exclude_ids.push(lead_id);
                    }
                }
                $.ajax({
                    type: "POST",
                    url: "index.php?module=Leads&action=MTS_getLeads&sugar_body_only=1&type=LoadMore",
                    data: {
                        "exclude_ids": exclude_ids,
                        "keyword": $(col).closest(".leadColumn").find(".keyword").val(),
                        "status": $(col).attr("status")
                    },
                    dataType: "JSON",
                    success: function (res) {

                        $(col).append(res.card_list);
                        updateSumary();

                        SUGAR.ajaxUI.hideLoadingPanel();
                        is_loadmore = false;
                    },
                    error: function (error) {
                        SUGAR.ajaxUI.hideLoadingPanel();
                        alert(error.message);
                        is_loadmore = false;
                    }

                })
            }, 300);
        }
    }
}
function customerRei(id,mod){
	$.ajax({
        type: "POST",
        url: "index.php?entryPoint=getLeadLanguage",
        dataType: "json",
        success: function (data) {
            if(confirm(data.LBL_QUESTION_GET_LEAD) == true){
				SUGAR.ajaxUI.showLoadingPanel();
			   $.ajax({
					type: "POST",
					url: "index.php?entryPoint=customerRei",
					data: {
						id:id,
						mod:mod
					},
					success: function (res) {
						refreshLeadCard(function (res) {
                            SUGAR.ajaxUI.hideLoadingPanel();
                        });
					},
					error: function (error) {
					   console.log(error);
					}
				});
				 
		    }
        },
        error: function (error) {
           console.log(error);
        }
	});
}
function changeSta(name,i,id){
	var s=document.getElementById("subpanel_opportunities");
	var ss=s.getElementsByClassName(name);
	
	var t3 = ss[i].getElementsByTagName("td")[3];
	var t1 = ss[i].getElementsByTagName("td")[1];
	var	t7=ss[i].getElementsByTagName("td")[4];
	
	 if(confirm("Are you sure want to Close this Opportunities") == true){
			$.ajax({
				type: "POST",
				url: "index.php?entryPoint=updateProduct",
				data: {
					type:"2",
					id:id
				},
                    
                    success: function (res) {
						if(t7.innerText.trim()=="Edit"){
							t1.innerText="";
							var statusEle = '<span class="statusText">Closed Won</span>';
								t3.innerHTML = statusEle;
								t3.getElementsByClassName("statusText")[0].style.backgroundColor = "#FF8000";
								t3.getElementsByClassName("statusText")[0].style.borderRadius = "10px";
								t3.getElementsByClassName("statusText")[0].style.textAlign = "center";
								t3.getElementsByClassName("statusText")[0].style.fontWeight = "bold";
								t3.getElementsByClassName("statusText")[0].style.color = "white";
						}else{
							t1.innerText="";
							var statusEle = '<span class="statusText">Chốt Thắng</span>';
								t3.innerHTML = statusEle;
								t3.getElementsByClassName("statusText")[0].style.backgroundColor = "#FF8000";
								t3.getElementsByClassName("statusText")[0].style.borderRadius = "10px";
								t3.getElementsByClassName("statusText")[0].style.textAlign = "center";
								t3.getElementsByClassName("statusText")[0].style.fontWeight = "bold";
								t3.getElementsByClassName("statusText")[0].style.color = "white";
						}
						
                    }
                   

                })
	
	 }
	 
	
	
}

function changeStatusCases(name,i,id){
	var s=document.getElementById("subpanel_cases");
	var ss=s.getElementsByClassName(name);
	
	var t3 = ss[i].getElementsByTagName("td")[3];
	var t1 = ss[i].getElementsByTagName("td")[1];
	var	t6=ss[i].getElementsByTagName("td")[5];
	
	 if(confirm("Are you sure want to Close this Case") == true){
			$.ajax({
				type: "POST",
				url: "index.php?entryPoint=changeStatusCases",
				data: {
					id:id
				},
                    
                    success: function (res) {
						if(t6.innerText.trim()=="Edit"){
							t3.innerText="Closed";
							t1.innerText="";
							
							
						}else{
							t3.innerText="Đóng";
							t1.innerText="";
							
							
							
						}
						
                    }
                   

                })
	
	 }
	 
	
	
}



function showSubPanel_re(child_field, url, force_load, layout_def_key) {
	sessionStorage.setItem('loading', '1');
  var inline = 1;
  if (typeof(force_load) == 'undefined' || force_load == null) {
    force_load = false;
  }

  function checkRefreshPage(url) {
    if (typeof(url) != 'undefined' && url != null && url.indexOf('refresh_page=1') > 0) {
      document.location.reload();
    }
  }

  if (force_load || typeof( child_field_loaded[child_field] ) == 'undefined') {
    request_map[request_id] = child_field;
    if (typeof (url) == 'undefined' || url == null) {
      var module = get_module_name();
      var id = get_record_id();
      if (typeof(layout_def_key) == 'undefined' || layout_def_key == null) {
        layout_def_key = get_layout_def_key();
      }

      url = 'index.php?sugar_body_only=1&module=' + module + '&subpanel=' + child_field + '&action=SubPanelViewer&inline=' + inline + '&record=' + id + '&layout_def_key=' + layout_def_key;
    }

    if (url.indexOf('http://') != 0 && url.indexOf('https://') != 0) {
      url = '' + url;
    }

    current_subpanel_url = url;

    var loadingImg = '<img src="themes/' + SUGAR.themes.theme_name + '/images/loading.gif">';
    $("#list_subpanel_" + child_field.toLowerCase()).html(loadingImg);

    $.ajax({
      type: "GET",
      async: true,
      cache: false,
      url: url + '&inline=' + inline + '&ajaxSubpanel=true',
      success: function(data) {
        request_map[request_id] = child_field;
        var returnstuff = {
          "responseText": data,
          "responseXML": '',
          "request_id": request_id
        };
        got_data(returnstuff, inline);
        if ($('#whole_subpanel_' + child_field).hasClass('useFooTable')) {
          $('#whole_subpanel_' + child_field + ' .table-responsive').footable();
        }
        request_id++;

        checkRefreshPage(url);
			setTimeout(function(){
				if(document.getElementById('act_action').style.display == "none"){
					document.getElementById('act_action').style.display = "block";
					$(".close").css("display","block");
				}
			},2000);
			$(document).ready(function(){
				if(child_field == "opportunities"){
				var checkb;
				var st;
				var st1;
				var st3;
				var len1;
				var td2;
				var td1;
				var td3;
				var td7;
				var len2;
				var td33;
				var td11;
				var td77;
				var id;
				var id1;
				var checkboxx;
				$(".listViewTdToolsS1").attr('target','_blank');
					// xử lý record opportunities
					st=document.getElementById("subpanel_opportunities");
					if(st != null && st != undefined){
						st1=st.getElementsByClassName("oddListRowS1");
						st2=st.getElementsByClassName("evenListRowS1");
						checkboxx=st.getElementsByClassName("checkbox");
						for(var c=0;c<checkboxx.length;c++){
							checkboxx[c].setAttribute("type", "hidden");
						}
						checkb=st.getElementsByClassName('listViewThLinkS1');
						//checkb[0].innerHTML='';
						 
						if(st1!=null){
							len1=st1.length;
						for (var i=0;i<len1;i++){
							
							td3 = st1[i].getElementsByTagName("td")[2];
							
							td1 = st1[i].getElementsByTagName("td")[1];
							td7=st1[i].getElementsByTagName("td")[4];
							
							if(td3.innerText.trim() != "Chốt Thắng" && td3.innerText.trim() != "Closed Won" ){
								
								 
								
								id=td7.getElementsByClassName('clickMenu')[0].id;
								td1.innerHTML='<input type="button" value="X" onclick=changeSta("oddListRowS1","'+i+'","'+id+'") style="text-align:center;width:50px;">';
								
							}
							
							
						}
								
						}
						
						if(st2!=null){
							 len2=st2.length;
						for ( var j=0;j<len2;j++){
							td33 = st2[j].getElementsByTagName("td")[2];
							td11 = st2[j].getElementsByTagName("td")[1];
							td77=st2[j].getElementsByTagName("td")[4];
							if(td33.innerText.trim() != "Chốt Thắng" && td33.innerText.trim() != "Closed Won"){
								
								
								id1=td77.getElementsByClassName('clickMenu')[0].id;
								td11.innerHTML='<input type="button" value="X" onclick=changeSta("evenListRowS1","'+j+'","'+id1+'") style="text-align:center;width:50px;">';
								

							}
							
							
						}
						
						}
						
					
					
					var ButtonNextAttr=document.getElementsByName('listViewNextButton');
					var leng=ButtonNextAttr.length;
						
						for(var k=0;k<leng;k++){
							if(ButtonNextAttr[k].getAttribute("onclick") != null){
								if(ButtonNextAttr[k].getAttribute("onclick").search("opportunities") != null){
									var NewButt=ButtonNextAttr[k].getAttribute("onclick").replace("showSubPanel", "showSubPanel_re");
									ButtonNextAttr[k].setAttribute("onclick",NewButt);
							
									
								}
					
							}
						}
							
					var ButtonEndAttr=document.getElementsByName('listViewEndButton');
					var leng2=ButtonEndAttr.length;
						for(var m=0;m<leng2;m++){
							if(ButtonEndAttr[m].getAttribute("onclick") != null){	
								if((ButtonEndAttr[m].getAttribute("onclick").search("opportunities")) != null){	
									var	NewButt2=ButtonEndAttr[m].getAttribute("onclick").replace("showSubPanel", "showSubPanel_re");
									ButtonEndAttr[m].setAttribute("onclick",NewButt2);
									
								}
							}
							
						}	
					
					
					var ButtonPrevAttr=document.getElementsByName('listViewPrevButton');
					var leng3=ButtonPrevAttr.length;
						for(var n=0;n<leng3;n++){
							if(ButtonPrevAttr[n].getAttribute("onclick") != null){	
								if((ButtonPrevAttr[n].getAttribute("onclick").search("opportunities")) != null){	
									var	NewButt3=ButtonPrevAttr[n].getAttribute("onclick").replace("showSubPanel", "showSubPanel_re");
									ButtonPrevAttr[n].setAttribute("onclick",NewButt3);
									
								}
							}
							
						}	
					
					var ButtonStartAttr=document.getElementsByName('listViewStartButton');
					var leng4=ButtonStartAttr.length;
						for(var l=0;l<leng4;l++){
							if(ButtonStartAttr[l].getAttribute("onclick") != null){	
								if((ButtonStartAttr[l].getAttribute("onclick").search("opportunities")) != null){	
									var	NewButt4=ButtonStartAttr[l].getAttribute("onclick").replace("showSubPanel", "showSubPanel_re");
									ButtonStartAttr[l].setAttribute("onclick",NewButt4);
									
								}
							}
							
						}
						
					var billButton = document.getElementById("accounts_opportunities_tạo_button");
						if(billButton == null || billButton == undefined){
						billButton = document.getElementById("accounts_opportunities_create_button");
						}
						if(billButton != null && billButton != undefined){
						billButton.style.display = "none";
						}
					}
					//
						var s=document.getElementById("subpanel_opportunities");
						var ss=s.getElementsByClassName("oddListRowS1");
						for(var i = 0; i < ss.length; i++){
							var t3 = ss[i].getElementsByTagName("td")[3];
							var statusText = t3.innerText;
							var statusEle = '<span class="statusText">'+statusText+'</span>';
							t3.innerHTML = statusEle;
							switch(t3.innerText){
								case "Closed Won":
								t3.getElementsByClassName("statusText")[0].style.backgroundColor = "#FF8000";
								t3.getElementsByClassName("statusText")[0].style.borderRadius = "10px";
								t3.getElementsByClassName("statusText")[0].style.textAlign = "center";
								t3.getElementsByClassName("statusText")[0].style.fontWeight = "bold";
								t3.getElementsByClassName("statusText")[0].style.color = "white";
								break;
							  case "Chốt mua":
								t3.getElementsByClassName("statusText")[0].style.backgroundColor = "#FF8000";
								t3.getElementsByClassName("statusText")[0].style.borderRadius = "10px";
								t3.getElementsByClassName("statusText")[0].style.textAlign = "center";
								t3.getElementsByClassName("statusText")[0].style.fontWeight = "bold";
								t3.getElementsByClassName("statusText")[0].style.color = "white";
								break;
							  case "Chốt Thắng":
								t3.getElementsByClassName("statusText")[0].style.backgroundColor = "#FF8000";
								t3.getElementsByClassName("statusText")[0].style.borderRadius = "10px";
								t3.getElementsByClassName("statusText")[0].style.textAlign = "center";
								t3.getElementsByClassName("statusText")[0].style.fontWeight = "bold";
								t3.getElementsByClassName("statusText")[0].style.color = "white";
								break;
							 case "Proposal/Price Quote":
								t3.getElementsByClassName("statusText")[0].style.backgroundColor = "#F7D358";
								t3.getElementsByClassName("statusText")[0].style.borderRadius = "10px";
								t3.getElementsByClassName("statusText")[0].style.textAlign = "center";
								t3.getElementsByClassName("statusText")[0].style.fontWeight = "bold";
								t3.getElementsByClassName("statusText")[0].style.color = "white";
								break;
								case "Giữ chỗ":
								t3.getElementsByClassName("statusText")[0].style.backgroundColor = "#F7D358";
								t3.getElementsByClassName("statusText")[0].style.borderRadius = "10px";
								t3.getElementsByClassName("statusText")[0].style.textAlign = "center";
								t3.getElementsByClassName("statusText")[0].style.fontWeight = "bold";
								t3.getElementsByClassName("statusText")[0].style.color = "white";
								break;
								case "Đề xuất báo giá":
								t3.getElementsByClassName("statusText")[0].style.backgroundColor = "#F7D358";
								t3.getElementsByClassName("statusText")[0].style.borderRadius = "10px";
								t3.getElementsByClassName("statusText")[0].style.textAlign = "center";
								t3.getElementsByClassName("statusText")[0].style.fontWeight = "bold";
								t3.getElementsByClassName("statusText")[0].style.color = "white";
								break;
								case "Negotiation/Review":
								t3.getElementsByClassName("statusText")[0].style.backgroundColor = "#FF8000";
								t3.getElementsByClassName("statusText")[0].style.borderRadius = "10px";
								t3.getElementsByClassName("statusText")[0].style.textAlign = "center";
								t3.getElementsByClassName("statusText")[0].style.fontWeight = "bold";
								t3.getElementsByClassName("statusText")[0].style.color = "white";
								break;
								case "Đặt cọc":
								t3.getElementsByClassName("statusText")[0].style.backgroundColor = "#FF8000";
								t3.getElementsByClassName("statusText")[0].style.borderRadius = "10px";
								t3.getElementsByClassName("statusText")[0].style.textAlign = "center";
								t3.getElementsByClassName("statusText")[0].style.fontWeight = "bold";
								t3.getElementsByClassName("statusText")[0].style.color = "white";
								break;
								case "Thương lượng đàm phán":
								t3.getElementsByClassName("statusText")[0].style.backgroundColor = "#FFBF00";
								t3.getElementsByClassName("statusText")[0].style.borderRadius = "10px";
								t3.getElementsByClassName("statusText")[0].style.textAlign = "center";
								t3.getElementsByClassName("statusText")[0].style.fontWeight = "bold";
								t3.getElementsByClassName("statusText")[0].style.color = "white";
								break;
								case "Closed Lost":
								t3.getElementsByClassName("statusText")[0].style.backgroundColor = "#00BFFF";
								t3.getElementsByClassName("statusText")[0].style.borderRadius = "10px";
								t3.getElementsByClassName("statusText")[0].style.textAlign = "center";
								t3.getElementsByClassName("statusText")[0].style.fontWeight = "bold";
								t3.getElementsByClassName("statusText")[0].style.color = "white";
								break;
								case "Hủy cọc":
								t3.getElementsByClassName("statusText")[0].style.backgroundColor = "#00BFFF";
								t3.getElementsByClassName("statusText")[0].style.borderRadius = "10px";
								t3.getElementsByClassName("statusText")[0].style.textAlign = "center";
								t3.getElementsByClassName("statusText")[0].style.fontWeight = "bold";
								t3.getElementsByClassName("statusText")[0].style.color = "white";
								break;
								case "Chốt Thua":
								t3.getElementsByClassName("statusText")[0].style.backgroundColor = "#00BFFF";
								t3.getElementsByClassName("statusText")[0].style.borderRadius = "10px";
								t3.getElementsByClassName("statusText")[0].style.textAlign = "center";
								t3.getElementsByClassName("statusText")[0].style.fontWeight = "bold";
								t3.getElementsByClassName("statusText")[0].style.color = "white";
								break;
								case "Tìm hiểu nhu cầu":
								t3.getElementsByClassName("statusText")[0].style.backgroundColor = "#3ADF00";
								t3.getElementsByClassName("statusText")[0].style.borderRadius = "10px";
								t3.getElementsByClassName("statusText")[0].style.textAlign = "center";
								t3.getElementsByClassName("statusText")[0].style.fontWeight = "bold";
								t3.getElementsByClassName("statusText")[0].style.color = "white";
								break;
								case "Prospecting":
								t3.getElementsByClassName("statusText")[0].style.backgroundColor = "#3ADF00";
								t3.getElementsByClassName("statusText")[0].style.borderRadius = "10px";
								t3.getElementsByClassName("statusText")[0].style.textAlign = "center";
								t3.getElementsByClassName("statusText")[0].style.fontWeight = "bold";
								t3.getElementsByClassName("statusText")[0].style.color = "white";
								break;
							
								default:
								
							}
						}
							ss=s.getElementsByClassName("evenListRowS1");
							for(var i = 0; i < ss.length; i++){
								var t3 = ss[i].getElementsByTagName("td")[3];
								var statusText = t3.innerText;
								var statusEle = '<span class="statusText">'+statusText+'</span>';
								t3.innerHTML = statusEle;
							switch(t3.innerText){
								case "Closed Won":
								t3.getElementsByClassName("statusText")[0].style.backgroundColor = "#FF8000";
								t3.getElementsByClassName("statusText")[0].style.borderRadius = "10px";
								t3.getElementsByClassName("statusText")[0].style.textAlign = "center";
								t3.getElementsByClassName("statusText")[0].style.fontWeight = "bold";
								t3.getElementsByClassName("statusText")[0].style.color = "white";
								break;
							  case "Chốt mua":
								t3.getElementsByClassName("statusText")[0].style.backgroundColor = "#FF8000";
								t3.getElementsByClassName("statusText")[0].style.borderRadius = "10px";
								t3.getElementsByClassName("statusText")[0].style.textAlign = "center";
								t3.getElementsByClassName("statusText")[0].style.fontWeight = "bold";
								t3.getElementsByClassName("statusText")[0].style.color = "white";
								break;
							  case "Chốt Thắng":
								t3.getElementsByClassName("statusText")[0].style.backgroundColor = "#FF8000";
								t3.getElementsByClassName("statusText")[0].style.borderRadius = "10px";
								t3.getElementsByClassName("statusText")[0].style.textAlign = "center";
								t3.getElementsByClassName("statusText")[0].style.fontWeight = "bold";
								t3.getElementsByClassName("statusText")[0].style.color = "white";
								break;
							 case "Proposal/Price Quote":
								t3.getElementsByClassName("statusText")[0].style.backgroundColor = "#F7D358";
								t3.getElementsByClassName("statusText")[0].style.borderRadius = "10px";
								t3.getElementsByClassName("statusText")[0].style.textAlign = "center";
								t3.getElementsByClassName("statusText")[0].style.fontWeight = "bold";
								t3.getElementsByClassName("statusText")[0].style.color = "white";
								break;
								case "Giữ chỗ":
								t3.getElementsByClassName("statusText")[0].style.backgroundColor = "#F7D358";
								t3.getElementsByClassName("statusText")[0].style.borderRadius = "10px";
								t3.getElementsByClassName("statusText")[0].style.textAlign = "center";
								t3.getElementsByClassName("statusText")[0].style.fontWeight = "bold";
								t3.getElementsByClassName("statusText")[0].style.color = "white";
								break;
								case "Đề xuất báo giá":
								t3.getElementsByClassName("statusText")[0].style.backgroundColor = "#F7D358";
								t3.getElementsByClassName("statusText")[0].style.borderRadius = "10px";
								t3.getElementsByClassName("statusText")[0].style.textAlign = "center";
								t3.getElementsByClassName("statusText")[0].style.fontWeight = "bold";
								t3.getElementsByClassName("statusText")[0].style.color = "white";
								break;
								case "Negotiation/Review":
								t3.getElementsByClassName("statusText")[0].style.backgroundColor = "#FF8000";
								t3.getElementsByClassName("statusText")[0].style.borderRadius = "10px";
								t3.getElementsByClassName("statusText")[0].style.textAlign = "center";
								t3.getElementsByClassName("statusText")[0].style.fontWeight = "bold";
								t3.getElementsByClassName("statusText")[0].style.color = "white";
								break;
								case "Đặt cọc":
								t3.getElementsByClassName("statusText")[0].style.backgroundColor = "#FF8000";
								t3.getElementsByClassName("statusText")[0].style.borderRadius = "10px";
								t3.getElementsByClassName("statusText")[0].style.textAlign = "center";
								t3.getElementsByClassName("statusText")[0].style.fontWeight = "bold";
								t3.getElementsByClassName("statusText")[0].style.color = "white";
								break;
								case "Thương lượng đàm phán":
								t3.getElementsByClassName("statusText")[0].style.backgroundColor = "#FFBF00";
								t3.getElementsByClassName("statusText")[0].style.borderRadius = "10px";
								t3.getElementsByClassName("statusText")[0].style.textAlign = "center";
								t3.getElementsByClassName("statusText")[0].style.fontWeight = "bold";
								t3.getElementsByClassName("statusText")[0].style.color = "white";
								break;
								case "Closed Lost":
								t3.getElementsByClassName("statusText")[0].style.backgroundColor = "#00BFFF";
								t3.getElementsByClassName("statusText")[0].style.borderRadius = "10px";
								t3.getElementsByClassName("statusText")[0].style.textAlign = "center";
								t3.getElementsByClassName("statusText")[0].style.fontWeight = "bold";
								t3.getElementsByClassName("statusText")[0].style.color = "white";
								break;
								case "Hủy cọc":
								t3.getElementsByClassName("statusText")[0].style.backgroundColor = "#00BFFF";
								t3.getElementsByClassName("statusText")[0].style.borderRadius = "10px";
								t3.getElementsByClassName("statusText")[0].style.textAlign = "center";
								t3.getElementsByClassName("statusText")[0].style.fontWeight = "bold";
								t3.getElementsByClassName("statusText")[0].style.color = "white";
								break;
								case "Chốt Thua":
								t3.getElementsByClassName("statusText")[0].style.backgroundColor = "#00BFFF";
								t3.getElementsByClassName("statusText")[0].style.borderRadius = "10px";
								t3.getElementsByClassName("statusText")[0].style.textAlign = "center";
								t3.getElementsByClassName("statusText")[0].style.fontWeight = "bold";
								t3.getElementsByClassName("statusText")[0].style.color = "white";
								break;
								case "Tìm hiểu nhu cầu":
								t3.getElementsByClassName("statusText")[0].style.backgroundColor = "#3ADF00";
								t3.getElementsByClassName("statusText")[0].style.borderRadius = "10px";
								t3.getElementsByClassName("statusText")[0].style.textAlign = "center";
								t3.getElementsByClassName("statusText")[0].style.fontWeight = "bold";
								t3.getElementsByClassName("statusText")[0].style.color = "white";
								break;
								case "Prospecting":
								t3.getElementsByClassName("statusText")[0].style.backgroundColor = "#3ADF00";
								t3.getElementsByClassName("statusText")[0].style.borderRadius = "10px";
								t3.getElementsByClassName("statusText")[0].style.textAlign = "center";
								t3.getElementsByClassName("statusText")[0].style.fontWeight = "bold";
								t3.getElementsByClassName("statusText")[0].style.color = "white";
								break;
							  default:
							
							}
							
						}
					
					//
				}
				else if (child_field == "cases"){
					var checkb;
					var st;
					var st1;
					var st3;
					var len1;
					var td2;
					var td1;
					var td3;
					var td6;
					var len2;
					var td33;
					var td11;
					var td66;
					var id;
					var id1;
					var checkboxx2;
				$(".listViewTdToolsS1").attr('target','_blank');
						st=document.getElementById("subpanel_cases");
						st1=st.getElementsByClassName("oddListRowS1");
						st2=st.getElementsByClassName("evenListRowS1");
						checkb=st.getElementsByClassName('listViewThLinkS1');
						checkboxx2=st.getElementsByClassName("checkbox");
						for(var d=0;d<checkboxx2.length;d++){
							
							checkboxx2[d].setAttribute("type", "hidden");
							
						}
						//checkb[0].innerHTML=''; 
						if(st1!=null){
							len1=st1.length;
						for (var i=0;i<len1;i++){
							
							td3 = st1[i].getElementsByTagName("td")[3];
							
							td1 = st1[i].getElementsByTagName("td")[1];
							td6=st1[i].getElementsByTagName("td")[5];
							
							
								if(td3.innerText.trim() != "Closed"){
								 
								
								id=td6.getElementsByClassName('clickMenu')[0].id;
								td1.innerHTML='<input type="button" value="X" onclick=changeStatusCases("oddListRowS1","'+i+'","'+id+'") style="text-align:center;width:50px;">';
								
								}
							
							
						}
								
						}
						
						if(st2!=null){
							 len2=st2.length;
						for ( var j=0;j<len2;j++){
							td33 = st2[j].getElementsByTagName("td")[3];
							td11 = st2[j].getElementsByTagName("td")[1];
							td66=st2[j].getElementsByTagName("td")[5];
							
								if(td33.innerText.trim() != "Closed"){
								
								id1=td66.getElementsByClassName('clickMenu')[0].id;
								td11.innerHTML='<input type="button" value="X" onclick=changeStatusCases("evenListRowS1","'+j+'","'+id1+'") style="text-align:center;width:50px;">';
								

								}
							
							
						}
						
						}
						
					
					
					var ButtonNextAttr=document.getElementsByName('listViewNextButton');
					var leng=ButtonNextAttr.length;
						
						for(var k=0;k<leng;k++){
							if(ButtonNextAttr[k].getAttribute("onclick") != null){
								if(ButtonNextAttr[k].getAttribute("onclick").search("cases") != null){
									var NewButt=ButtonNextAttr[k].getAttribute("onclick").replace("showSubPanel", "showSubPanel_re");
									ButtonNextAttr[k].setAttribute("onclick",NewButt);
							
									
								}
					
							}
						}
							
					var ButtonEndAttr=document.getElementsByName('listViewEndButton');
					var leng2=ButtonEndAttr.length;
						for(var m=0;m<leng2;m++){
							if(ButtonEndAttr[m].getAttribute("onclick") != null){	
								if((ButtonEndAttr[m].getAttribute("onclick").search("cases")) != null){	
									var	NewButt2=ButtonEndAttr[m].getAttribute("onclick").replace("showSubPanel", "showSubPanel_re");
									ButtonEndAttr[m].setAttribute("onclick",NewButt2);
									
								}
							}
							
						}	
					
					
					var ButtonPrevAttr=document.getElementsByName('listViewPrevButton');
					var leng3=ButtonPrevAttr.length;
						for(var n=0;n<leng3;n++){
							if(ButtonPrevAttr[n].getAttribute("onclick") != null){	
								if((ButtonPrevAttr[n].getAttribute("onclick").search("cases")) != null){	
									var	NewButt3=ButtonPrevAttr[n].getAttribute("onclick").replace("showSubPanel", "showSubPanel_re");
									ButtonPrevAttr[n].setAttribute("onclick",NewButt3);
									
								}
							}
							
						}	
					
					var ButtonStartAttr=document.getElementsByName('listViewStartButton');
					var leng4=ButtonStartAttr.length;
						for(var l=0;l<leng4;l++){
							if(ButtonStartAttr[l].getAttribute("onclick") != null){	
								if((ButtonStartAttr[l].getAttribute("onclick").search("cases")) != null){	
									var	NewButt4=ButtonStartAttr[l].getAttribute("onclick").replace("showSubPanel", "showSubPanel_re");
									ButtonStartAttr[l].setAttribute("onclick",NewButt4);
									
								}
							}
							
						}
				
					var billButton = document.getElementById("account_cases_tạo_button");
					if(billButton == null || billButton == undefined){
						billButton = document.getElementById("account_cases_create_button");
					}
						
					if(billButton != null && billButton != undefined){
						billButton.style.display = "none";
					}
						var subBill = document.getElementById("whole_subpanel_cases");
						if(subBill != null && subBill != undefined){
							subBill.getElementsByClassName("sugar_action_button")[0].style.display = "none";
						}
				}else if(child_field == "account_aos_contracts"){
					$("#formaccount_aos_contracts").attr("target","_blank");
					var billButton = document.getElementById("AOS_Contracts_tạo_button");
					if(billButton == null || billButton == undefined){
						billButton = document.getElementById("AOS_Contracts_create_button");
					}
					if(billButton != null && billButton != undefined){
						billButton.style.display = "none";
					}
						var subBill = document.getElementById("whole_subpanel_account_aos_contracts");
						if(subBill != null && subBill != undefined){
							subBill.getElementsByClassName("sugar_action_button")[0].style.display = "none";
						}
				}else if(child_field == "account_aos_quotes"){
					$("#formaccount_aos_quotes").attr("target","_blank");
					var billButton = document.getElementById("AOS_Quotes_tạo_button");
					if(billButton == null || billButton == undefined){
						billButton = document.getElementById("AOS_Quotes_create_button");
					}
					if(billButton != null && billButton != undefined){
						billButton.style.display = "none";
					}
						var subBill = document.getElementById("whole_subpanel_account_aos_quotes");
						if(subBill != null && subBill != undefined){
							subBill.getElementsByClassName("sugar_action_button")[0].style.display = "none";
						}
				}else if(child_field == "account_aos_invoices"){
					$("#formaccount_aos_invoices").attr("target","_blank");
					var billButton = document.getElementById("AOS_Invoices_tạo_button");
					if(billButton == null || billButton == undefined){
						billButton = document.getElementById("AOS_Invoices_create_button");
					}
					if(billButton != null && billButton != undefined){
						billButton.style.display = "none";
					}
						var subBill = document.getElementById("whole_subpanel_account_aos_invoices");
						if(subBill != null && subBill != undefined){
							subBill.getElementsByClassName("sugar_action_button")[0].style.display = "none";
						}
				}
				
				else if(child_field == "bill_billing_accounts"){
						var billButton = document.getElementById("bill_billing_accounts_tạo_button");
						if(billButton == null || billButton == undefined){
						billButton = document.getElementById("bill_billing_accounts_create_button");
						}
						if(billButton != null && billButton == undefined){
							billButton.style.display = "none";
						}
							var subBill = document.getElementById("subpanel_bill_billing_accounts");
							if(subBill != null && subBill != undefined){
								subBill.getElementsByClassName("sugar_action_button")[0].style.display = "none";
							}
						
				}
				
				else if(child_field == "history"){
						var billButton = document.getElementById("History_tạoghichúhoặcđínhkèm_button");
						if(billButton == null || billButton == undefined){
							billButton = document.getElementById("History_createnotesorattachment_button");
						}
						if(billButton != null && billButton != undefined){
							billButton.style.display = "none";
						}
						var subBill = document.getElementById("whole_subpanel_history");
						if(subBill != null && subBill != undefined){
							subBill.getElementsByClassName("sugar_action_button")[0].style.display = "none";
						}
				}
				else if(child_field == "contacts"){
						var billButton = document.getElementById("accounts_contacts_tạo_button");
						if(billButton == null || billButton == undefined){
							billButton = document.getElementById("accounts_contacts_create_button");
						}
						if(billButton != null && billButton != undefined){
							billButton.style.display = "none";
						}
						var subBill = document.getElementById("whole_subpanel_contacts");
						if(subBill != null && subBill != undefined){
							subBill.getElementsByClassName("sugar_action_button")[0].style.display = "none";
						}
				}
				else if(child_field == "documents"){
						var billButton = document.getElementById("documents_accounts_tạo_button");
						if(billButton == null || billButton == undefined){
							billButton = document.getElementById("documents_accounts_create_button");
						}
						if(billButton != null && billButton != undefined){
							billButton.style.display = "none";
						}
						var subBill = document.getElementById("whole_subpanel_documents");
						if(subBill != null && subBill != undefined){
							subBill.getElementsByClassName("sugar_action_button")[0].style.display = "none";
						}
				}
				
				 sessionStorage.setItem('loading', '0');
			});
			
        }
    });

  } else {

    var subpanel = document.getElementById('subpanel_' + child_field);
    subpanel.style.display = '';

    set_div_cookie(subpanel.cookie_name, '');

    if (current_child_field != '' && child_field != current_child_field) {
      hideSubPanel(current_child_field);
    }

    current_child_field = child_field;

    checkRefreshPage(url);
  }
  
}


var hideUpdateImage = 'themes/default/images/basic_search.gif?v=golOgr3LHb5uVI36fQV1hQ';
var showUpdateImage = 'themes/default/images/advanced_search.gif?v=golOgr3LHb5uVI36fQV1hQ';
function collapseAllUpdates(){
    $('.caseUpdateImage').attr("src",showUpdateImage);
    $('.caseUpdate').slideUp('fast');
}
function expandAllUpdates(){
    $('.caseUpdateImage').attr("src",hideUpdateImage);
    $('.caseUpdate').slideDown('fast');
}
function toggleCaseUpdate(updateId){
    var id = 'caseUpdate'+updateId;
    var updateElem = $('#'+id);
    var imageElem = $('#'+id+"Image");

    if(updateElem.is(":visible")){
        imageElem.attr("src",showUpdateImage);
    }else{
        imageElem.attr("src",hideUpdateImage);
    }
    updateElem.slideToggle('fast');
}
function leadUpdates(record,module){
	var update_data = document.getElementById('update_text').value;
	$.ajax({
        type: "POST",
        url: "index.php?entryPoint=saveNote",
		data:{
			id:record,
			module:module,
			data:update_data
			
		},
		dataType:"json",
        success: function (res) {
			document.getElementById('update_text').value = "";
			html = '';
			for(let i = 0 ;i < res.length; i++){
				html += '<div>';
				html += '<div id="lessmargin">';
				html += '<div id="caseStyleUser">';
				html += '<a href="" onclick="toggleCaseUpdate(\''+res[i].id+'\');return false;">';
				html += '<img id="caseUpdate'+res[i].id+'Image" class="caseUpdateImage" src="themes/default/images/advanced_search.gif?v=golOgr3LHb5uVI36fQV1hQ">';
				html += '</a>';
				html += '<span>';
				html += res[i].username+" "+ res[i].time;
				html += '</span>';
				html += '<br>';
				html += '<div id="caseUpdate'+res[i].id+'" class="caseUpdate" style="overflow-wrap: break-word;display: block;">';
				html += res[i].data;
				html += '</div>';
				html += '</div>';
				html += '</div>';
				html += '</div>';
			}
			document.getElementById("noteContent").innerHTML = html;
			
        },
        error: function (error) {
            alert(error.message);
        }
    });
	/*
    loadingMessgPanl = new YAHOO.widget.SimpleDialog('loading', {
                    width: '200px',
                    close: true,
                    modal: true,
                    visible: true,
                    fixedcenter: true,
                    constraintoviewport: true,
                    draggable: false
                });
    loadingMessgPanl.setHeader(SUGAR.language.get('app_strings', 'LBL_EMAIL_PERFORMING_TASK'));
    loadingMessgPanl.setBody(SUGAR.language.get('app_strings', 'LBL_EMAIL_ONE_MOMENT'));
    loadingMessgPanl.render(document.body);
    loadingMessgPanl.show();
	*/
    /*
    var checkbox = document.getElementById('internal').checked;
    var internal = "";
    if(checkbox){
        internal=1;
    }

    //Post parameters

    var params =
        "record="+record+"&module=Leads&return_module=Leads&action=Save&return_id="+record+"&return_action=DetailView&relate_to=Leads&relate_id="+record+"&offset=1&update_text="
        + update_data + "&internal=" + internal;

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", "index.php", true);


    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.setRequestHeader("Content-length", params.length);
    xmlhttp.setRequestHeader("Connection", "close");

    //When button is clicked
    xmlhttp.onreadystatechange = function() {

        if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {


            showSubPanel('history', null, true);
            //Reload the case updates stream and history panels
		    $("#LBL_AOP_CASE_UPDATES").load("index.php?module=Leads&action=DetailView&record="+record + " #LBL_AOP_CASE_UPDATES", function(){


            //Collapse all except newest update
            $('.caseUpdateImage').attr("src",showUpdateImage);
            $('.caseUpdate').slideUp('fast');

            var id = $('.caseUpdate').last().attr('id');
            if(id){
            toggleCaseUpdate(id.replace('caseUpdate',''));
            }


            //loadingMessgPanl.hide();

            }

        );
	}
}

        xmlhttp.send(params);

*/

}

function openDetailView(lead_id, module, name, account_id, status) {
	$(".close").css("display","none");
	var height = isNaN(window.innerHeight) ? window.clientHeight : window.innerHeight;
	//console.log(height);
	$(".modal-content").css("marginTop","-28px");
	$(".modal-content").eq(0).css("height",height - 20);
	var footer  = document.getElementsByClassName("modal-footer");
	footer[0].remove();
    if (!!lead_id && !is_opening) {
        is_opening = true;
        showSpinner('detailModal');
		$('#detailModal').find('.modal-body').css("height",height - 65);
        var detail_url = "index.php?module=" + module + "&action=DetailView&record=" + lead_id;
        if(status == 'Converted') {
            detail_url = "index.php?module=Accounts&action=DetailView&record=" + account_id;
        }
		var html = '';
						html += '<div id="LBL_AOP_CASE_UPDATES" style="margin-top:-55px">';
						html += '<span id="aop_case_updates_threaded_span">';
						
						html += '<a href="" onclick="collapseAllUpdates(); return false;">Thu gọn tất cả</a>';
						html += '<a href="" onclick="expandAllUpdates(); return false;">Mở rộng tất cả</a>';
						html += '<div id="noteContent" style="height:300px;overflow:auto"></div><form id="case_updates" enctype="multipart/form-data">';
						html += '<div style="margin-top:10px"><textarea id="update_text" name="update_text" cols="160" rows="4" style="width:88%;margin-left:-50px;"></textarea></div>';
						html += '<input type="button" value="Lưu" onclick="leadUpdates(\''+lead_id+'\',\''+module+'\')" title="Lưu" name="button">';
						html += '<br>';
						html += '</form>';
						
						html += '</span>';
						html += '</div>';
        setTimeout(function() {
            $.ajax({
                type: "GET",
                url: "index.php?module=" + module + "&action=quickviewdetail&record=" + lead_id + "&sugar_body_only=1",
                success: function (res) {
					
                    $('#detailModal').find('.modal-body').html(res);
					
                    $('#detailModal').append('<script type="text/javascript" src="custom/modules/Leads/Dashlets/LeadCardDashlet/js/CustomSubpanelJS.js"></script>');
					$('#detailModal').append('<link rel="stylesheet" href="custom/modules/Leads/Dashlets/LeadCardDashlet/css/reponsive.css" />');
					//$('#detailModal').append('<script stype="text/javascript" src="custom/modules/Accounts/js/script.js"></script>');
                    $('#detailModal').find('.modal-body').find('#tab-actions, .inlineEditIcon').hide();
                    $('#detailModal').find('.modal-body').find('.nav-tabs').append('<li><a target="_blank" href="' + detail_url + '">' + lead_mods.LBL_FULL_VIEW + '</a></li>');
                    $('#detailModal').find('.modal-body').append('<input type="hidden" id="data-record-id" value="' + lead_id + '">');
                    $('#detailModal').find('.modal-body').append('<input type="hidden" id="data-record-module" value="' + module + '">');
                    $('#detailModal').find('.modal-body').append('<input type="hidden" id="data-record-name" value="' + name + '">');
                    $('#detailModal').find('#preview').find('.panel-heading').html(lead_mods.LBL_DETAIL_LABEL);
                    $('#detailModal').find('#preview').find('.panel-content').html(lead_mods.LBL_CHOOSE_A_RECORD);
                    processSubPanels($('#detailModal').find('.modal-body').find('#list_subpanel_history'), false);
                    processSubPanels($('#detailModal').find('.modal-body').find('#list_subpanel_activities'), true, account_id, name, status);//testing
					var tab2 = document.getElementById("tab-content-1");
						var email = tab2.getElementsByClassName("detail-view-row")[1];
						var statuss = tab2.getElementsByClassName("detail-view-row")[5];
						var email2 = email.cloneNode(true);
						var statuss2 = statuss.cloneNode(true);	
						email.remove();
						statuss.remove();						
						document.getElementById("tab-content-0").innerHTML = '<div style="float:left;width:50%">'+document.getElementById("tab-content-0").innerHTML+'</div><div id="rightCol" style="float:right;width:50%"></div><div style="clear:both"></div>';
						document.getElementById("rightCol").appendChild(email2);
						document.getElementById("rightCol").appendChild(statuss2);
						document.getElementById("tab-content-1").innerHTML = '<div style="float:left;width:50%">'+document.getElementById("tab-content-1").innerHTML+'</div><div style="float:right;width:50%">'+html+'</div><div style="clear:both"></div>';
						
						collapseAllUpdates();
						var id = $('.caseUpdate').last().attr('id');
						if(id){
							toggleCaseUpdate(id.replace('caseUpdate',''));
						}
						leadUpdates(lead_id,module);
					$(document).ready(function(){
						is_opening = false;
						showSubPanel_re('activities');
						showSubPanel_re('history');
						if(module == "Accounts"){
							showSubPanel_re('cases');
							showSubPanel_re('opportunities');
							showSubPanel_re('contacts');
							showSubPanel_re('documents');
							showSubPanel_re('account_aos_contracts');
							showSubPanel_re('account_aos_quotes');
							showSubPanel_re('account_aos_invoices');
							showSubPanel_re('bill_billing_accounts');
						}
						if(status != 'Converted' && status != 'Transaction'){
							var act=document.getElementById('activity_actions');
							var cas=document.getElementById('case');
								cas.parentNode.remove();
								var cont = document.getElementById('contact');
								cont.parentNode.remove();
								var doc=document.getElementById('document');
								doc.parentNode.remove();
								
								var bill = document.getElementById('invoice');
								bill.parentNode.remove();
								document.getElementById('contract').parentNode.remove();
								document.getElementById('quote').parentNode.remove();
								document.getElementById('bill').parentNode.remove();
						}
						if(status == 'Converted' || status == 'Transaction'){
							var old = document.getElementById('whole_subpanel_opportunities');
							var neww = document.getElementById('preview');
							var old2=document.getElementById('whole_subpanel_cases');
							var old3=document.getElementById('whole_subpanel_contacts');
							var old4=document.getElementById('whole_subpanel_bill_billing_accounts');
							if((neww != null && neww != undefined)&&(old != null && old != undefined)&&(old2 != null && old2 != undefined)&&(old3 != null && old3 != undefined)&&(old4 != null && old4 != undefined)){
							neww.appendChild(old);
							neww.appendChild(old2);
							neww.appendChild(old3);
							neww.appendChild(old4);
							}
							$("#contract").click(function() {
								$("#formaccount_aos_contracts").submit();
							});
							$("#aosQuotes").click(function() {
								$("#formaccount_aos_quotes").submit();
							});
							$("#aosInvoices").click(function() {
								$("#formaccount_aos_invoices").submit();
							});
							
						}
						setTimeout(function(){
							var headerForm = document.getElementsByClassName("formHeader")[1];
							var closedButton = headerForm.getElementsByTagName("button")[0].cloneNode(true);
							closedButton.innerHTML = '<div style =" margin-left:-5px">X<div>';
							/*
							closedButton.setAttribute("onlick", "closeModal('detailModal');"+"clearInterval(process_subpanel);"+"SUGAR.ajaxUI.showLoadingPanel();"+"setTimeout(function(){refreshLeadCard(function(res){"+"SUGAR.ajaxUI.hideLoadingPanel();});},300);");
							*/
							closedButton.setAttribute("class","button primary");
							closedButton.style.display = "block";
							closedButton.style.height = "20px";
							closedButton.style.width = "10px";
							var liMenu = document.createElement("div");
							liMenu.style.bottom = "0";
							liMenu.style.right = "0";
							liMenu.style.position = "fixed";
							liMenu.style.marginRight = "5px";
							liMenu.appendChild(closedButton);
							document.getElementsByClassName("modal-body")[1].appendChild(liMenu);
						},6000);
					});
					
                },
                error: function (error) {
                    SUGAR.ajaxUI.hideLoadingPanel();
                    alert(error.message);
                    is_opening = false;
                }
            });
        }, 500);

    }
    else {
        return false;
    }

}



function showOpportunityModal2(account_id, account_name)
{
    showSpinner('activityCreationModal');
    prepareParamsForOppForm(account_id, account_name);
    setTimeout(function () {
        $.ajax({
            type: "POST",
            url: "index.php",
            data: $('#formformaccounts_opportunities').serialize(),
            success: function(res)
            {
				
                var modal_title = lead_mods.LBL_NEW_OPPORTUNITY;
                var full_form_btn = 'Opportunities_subpanel_full_form_button';
                var cancel_btn = 'Opportunities_subpanel_cancel_button';
                var save_btn_name = 'Opportunities_subpanel_save_button';
                $('#activityCreationModal').find('.modal-body').html(res);
                $('#activityCreationModal').find('.modal-title').text(modal_title);
                $('#activityCreationModal').find('[name=' + full_form_btn + ']').remove();
                $('#activityCreationModal').find('.modal-body').find('[name=' + save_btn_name + ']').attr('onclick', "saveOpportunityFromModal('"+account_id+"','form_SubpanelQuickCreate_Opportunities')").attr('type', 'button');
                $('#activityCreationModal').find('.modal-body').find('[name=' + cancel_btn + ']').attr('onclick', '$("#activityCreationModal").modal("hide");').attr('type', 'button');
                $('#activityCreationModal').find('.modal-body').find('.panel-content').find('div:first').remove();
                $('#activityCreationModal').modal('show');
				changeCur("amount");
				changeCur("depositsamount");
				changeCur("paymentsamount");
				$("#amount").css("background-color", "yellow");
				$("#depositsamount").css("background-color", "yellow");
				$("#paymentsamount").css("background-color", "yellow");
				$("#amount").css("color", "red");
				$("#depositsamount").css("color", "red");
				$("#paymentsamount").css("color", "red");
				$("#amount").css("font-weight", "bold");
				$("#depositsamount").css("font-weight", "bold");
				$("#paymentsamount").css("font-weight", "bold");
                addSpinner(cancel_btn);
				$(document).ready(function(){
					var oppStatus = document.getElementsByClassName("edit-view-row-item");
					oppStatus[3].style.display = "none";
					//$("#sales_stage").on("change",function(){
					//	$("#sales_stage").val("Prospecting");
					//})
					var deposite;
					$.ajax({
						type: "POST",
						url: "index.php?entryPoint=getDeposite",
						success: function(pData) {
							deposite = pData;
							var proVal = $("#aos_products_opportunities_1_name").val();
							var proAmo = $("#amount").val();
							var depoAmount = $("#depositsamount").val();
							setInterval(function(){
								if($("#date_closed").val() != null && $("#date_closed").val() != "" ){
									if($("#aos_products_opportunities_1_name").val() != ""){
										$("[id=name]:eq(1)").val($("#aos_products_opportunities_1_name").val()+"_"+$("#date_closed").val().replace(/\//g,""));
									}else{
										$("[id=name]:eq(1)").val($("#date_closed").val().replace(/\//g,""));	
									}
								}
								if($("#aos_products_opportunities_1_name").val() != null){
									if($("#aos_products_opportunities_1_name").val() != proVal){
										if($("#aos_products_opportunities_1_name").val() != ""){
											if($("#date_closed").val() != ""){
												$("[id=name]:eq(1)").val($("#aos_products_opportunities_1_name").val()+"_"+$("#date_closed").val().replace(/\//g,""));
											}else{
												$("[id=name]:eq(1)").val($("#aos_products_opportunities_1_name").val());	
											}
											$.ajax({
												type: "POST",
												url: "index.php?entryPoint=loadPrice",
												dataType:"json",
												data:{
													name:$("#aos_products_opportunities_1_name").val()
												},
												success: function(data) {
													if(data != null && data != '' && data != undefined){
													var amount = parseInt(data);
													$("#amount").val(changeCurrency(String(parseInt(data))));
													proVal = $("#aos_products_opportunities_1_name").val();
													proAmo = $("#amount").val();
													var depositsAmount = amount * (deposite / 100);
													$("#depositsamount").val(changeCurrency(String(parseInt(depositsAmount))));
													var paymentsAmount = amount - parseInt(depositsAmount);
													$("#paymentsamount").val(changeCurrency(String(paymentsAmount)));
													}else{
													$("#amount").val(0);
													proVal = 0;
													proAmo = 0;
													$("#depositsamount").val(0);
													$("#paymentsamount").val(0);
													}
												},
												error: function (error) {
													
												}
											});
										}else{
											if($("#amount").val() != proAmo){
												proVal = $("#aos_products_opportunities_1_name").val();
												proAmo = $("#amount").val();
												if(proAmo != "" && proAmo != null &&  proAmo != undefined ){
													var proAmo2 = proAmo.replace(/,/g,'');
													var depositsAmount = proAmo2 * (deposite / 100);
													$("#depositsamount").val(changeCurrency(String(parseInt(depositsAmount))));
													var paymentsAmount = proAmo2 - parseInt(depositsAmount);
													$("#paymentsamount").val(changeCurrency(String(paymentsAmount)));
												}
											}
										}
									}else{
										if($("#amount").val() != proAmo){
											proVal = $("#aos_products_opportunities_1_name").val();
											proAmo = $("#amount").val();
											if(proAmo != "" && proAmo != null &&  proAmo != undefined ){
												var proAmo2 = proAmo.replace(/,/g,'');
												var depositsAmount = proAmo2 * (deposite / 100);
												$("#depositsamount").val(changeCurrency(String(parseInt(depositsAmount))));
												var paymentsAmount = proAmo2 - parseInt(depositsAmount);
												$("#paymentsamount").val(changeCurrency(String(paymentsAmount)));
											}
										}
									}
								}
								
								if($("#depositsamount").val() != depoAmount){
									depoAmount = $("#depositsamount").val();
									proAmo = $("#amount").val();
									if(proAmo != "" && proAmo != null &&  proAmo != undefined ){
										var proAmo2 = proAmo.replace(/,/g,'');
										var paymentsAmount = proAmo2 - parseInt(depoAmount.replace(/,/g,''));
										$("#paymentsamount").val(changeCurrency(String(paymentsAmount)));
									}
								}
								
								
							},1000);
						}
					});
					document.getElementById('paymentsamount').readOnly = true;
				});
            }
        });

    }, 300);
	
}


function updateColumnInfo(col) {
    // Count current cards
    var cards = $(col).find('.leadCard');
    if (cards.length == 0) {
        $(col).html('<div class="virtualCard"></div>');
    }
    else {
        $(col).find('.virtualCard').remove();
    }
}

function updateSumary() {
    var columns = $('.cards');
    $.get('index.php?module=Leads&action=MTS_updateSummary&sugar_body_only=1', function (res) {
        if (res) {
            var summary_info = JSON.parse(res);
            for (var i = 0; i < columns.length; i++) {
                var column_key = $(columns[i]).attr('status');
                updateColumnInfo(columns[i]);
                var summary_ele = $(columns[i]).closest('.leadColumn').find('.summaryColumn');
                $(summary_ele).find('#total_records').text(summary_info[column_key]['total_records']);
                $(summary_ele).find('#total_amount').text(summary_info[column_key]['amount_formatted']);
                $(summary_ele).find('.total_call').text(summary_info[column_key]['total_call']);
                $(summary_ele).find('.total_email').text(summary_info[column_key]['total_email']);
                $(summary_ele).find('.total_meeting').text(summary_info[column_key]['total_meeting']);
                $(summary_ele).find('.total_email_quotation').text(summary_info[column_key]['total_email_quotation']);
            }
        }
    })
}

function searchLead(event, current, status) {
    //event.preventDefault();
    if (is_searching) {
        return true;
    }
    else {
        if (event.keyCode == 13) {
            is_searching = true;
            SUGAR.ajaxUI.showLoadingPanel();
            var col = $(current).closest('.leadColumn').find('.cards');
            setTimeout(function () {
                $.ajax({
                    type: "POST",
                    url: "index.php?module=Leads&action=MTS_getLeads&sugar_body_only=1&type=Search",
                    data: {
                        "keyword": $(current).val(),
                        "status": status
                    },
                    dataType: "JSON",
                    success: function (res) {
                        var content = '<p class="not_found_data">' + lead_mods.LBL_NO_DATA + '</p>';
                        if (!!res.card_list) {
                            content = res.card_list;
                        }
                        $(col).html(content);
                        updateSumary();

                        SUGAR.ajaxUI.hideLoadingPanel();
                        is_searching = false;
                    },
                    error: function (error) {
                        SUGAR.ajaxUI.hideLoadingPanel();
                        alert(error.message);
                        is_searching = false;
                    }

                });
            }, 300);
        }
    }
}

function previewActivity(event, ele) {
    if (event.target.className == 'sugar_action_button' || $(event.target).closest('.sugar_action_button').length > 0) {
        // do nothing
    }
    else {
        event.preventDefault();
        var target_module = $(ele).attr('target-module');
        var record = $(ele).attr('record');
        var url = 'index.php?module=Leads&action=MTS_preview&sugar_body_only=1&target_module=' + target_module + '&record=' + record;
        if (!!url) {
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function (res) {
                        $('#preview').find('.panel-content').html(res);

                    },
                    error: function (error) {
                        alert(error.message);
                    }
                });
        }
    }
}

function processSubPanels(ele, is_activities, account_id, account_name, status) {

    addClickToPreview(ele);
    if (is_activities) {
        addActivityButton(ele, account_id, account_name, status);
    }

}

function addClickToPreview(ele, is_activities) {
    var trs = $(ele).find('table>tbody>tr');
    for (var i = 0; i < trs.length; i++) {
        if($(trs[i]).attr('onclick') == 'previewActivity(event, this)') continue;
        var name_col = $(trs[i]).find('td')[2];
        var detail_link = $(name_col).find('a').attr('href');
        if (detail_link != undefined) {
            detail_link = detail_link.replace('?action=ajaxui#ajaxUILoc=', '');
            detail_link = decodeURIComponent(detail_link);
            var regex = /[?&]([^=#]+)=([^&#]*)/g,
            params = {},
            match;
            while(match = regex.exec(detail_link)) {
                params[match[1]] = match[2];
            }
            $(trs[i]).attr('onclick', 'previewActivity(event, this)');
            $(trs[i]).attr('detail-link', detail_link);
            $(trs[i]).attr('target-module', params['module']);
            $(trs[i]).attr('record', params['record']);
        }
        $(trs[i]).find('a').attr('href', 'javascript:void(0);');
    }
}

function sortColumn(event, current, status) {
    //event.preventDefault();
    if (is_searching) {
        return true;
    }
    else {
        is_searching = true;
        SUGAR.ajaxUI.showLoadingPanel();
        var col = $(current).closest('.leadColumn').find('.cards');
        var order_by_ele = $(current).closest('.leadColumn').find('.card_order_by');
        var order_by = $(order_by_ele).val();
        
        var is_asc = parseInt($(current).closest('.leadColumn').find('a.direction').attr('is_asc'));
        if($(current).attr('name') == status + '_order_by') {
            if(is_asc == 1) {
                is_asc = 0;
            }
            else {
                is_asc = 1;
            }
        }
        setTimeout(function () {
            $.ajax({
                type: "POST",
                url: "index.php?module=Leads&action=MTS_getLeads&sugar_body_only=1&type=Search",
                data: {
                    "keyword": $(current).closest('.leadColumn').find('.keyword').val(),
                    "status": status,
                    "order_by": order_by,
                    "is_asc": is_asc
                },
                dataType: "JSON",
                success: function (res) {
                    var content = '<p class="not_found_data">Not found data</p>';
                    if (!!res.card_list) {
                        content = res.card_list;
                    }
                    $(col).html(content);
                    updateSumary();

                    is_searching = false;
                    $(current).closest('.leadColumn').find('.direction').attr('is_asc', (is_asc == 1) ? 0 : 1);
                    if (is_asc == 1) {
                        $(current).closest('.leadColumn').find('.suitepicon-action-sorting-ascending').hide();
                        $(current).closest('.leadColumn').find('.suitepicon-action-sorting-descending').show();
                    }
                    else {
                        $(current).closest('.leadColumn').find('.suitepicon-action-sorting-ascending').show();
                        $(current).closest('.leadColumn').find('.suitepicon-action-sorting-descending').hide();
                    }
                    SUGAR.ajaxUI.hideLoadingPanel();
                },
                error: function (error) {
                    SUGAR.ajaxUI.hideLoadingPanel();
                    alert(error.message);
                    is_searching = false;
                }

            });
        }, 300);
    }
}

function addActivityButton(ele, account_id, account_name, status) {
    $(ele).find('tr[role="presentation"]').find('td[align=left]').find('ul').find('li').hide();
    var actity_actions_template = $('#activity_actions').clone();
    console.log(status);
    if(status != 'Converted' && status != 'Transaction') {
        $(actity_actions_template).find('#btn_opp').remove();
    }
    else {
        $(actity_actions_template).find('#btn_opp').find('.activity_action').attr('onclick', 'showOpportunityModal2("' + account_id +'","' + account_name + '")')
    }
    if((ele).closest('#pagecontent').find('#activity_actions:visible').length == 0) {
        $(actity_actions_template).show().insertAfter($(ele).closest('#pagecontent').find('.detail-view'));
    }
}

function prepareParamsForOppForm(account_id, account_name)
{
    $('#formformaccounts_opportunities').find('input[name="account_id"], input[name="return_id"], input[name="parent_id"]').val(account_id);
    $('#formformaccounts_opportunities').find('input[name="account_name"], input[name="accounts_opportunities_name"], input[name="return_name"], input[name="parent_name"]').val(account_name);
}

function doAction(createFormId, submitFormId, save_btn_name) {//formformcall
	
    if (typeof createFormId == 'undefined') {
        return false;
    }
    else {
		
        showSpinner('activityCreationModal');
		var isLoading = sessionStorage.getItem('loading');
			var cde = setInterval(function(){
				isLoading = sessionStorage.getItem('loading');
				if(isLoading == 0){
					setTimeout(function () {
						$.ajax({
							type: "POST",
							url: "index.php",
							data: $('#' + createFormId).serialize(),//
							success: function(res)
							{
								var modal_title = '';
								var full_form_btn = '';
								var cancel_btn = '';
								if(createFormId == 'formformCalls') {
									modal_title = lead_mods.LBL_ADD_CALL_MODAL_TITLE;//tạo cuộc gọi
									
									full_form_btn = 'Calls_subpanel_full_form_button';
									cancel_btn = 'Calls_subpanel_cancel_button';
								}
								else if (createFormId == 'formformMeetings') {
									modal_title = lead_mods.LBL_ADD_MEETING_MODAL_TITLE;
									full_form_btn = 'Meetings_subpanel_full_form_button';
									cancel_btn = 'Meetings_subpanel_cancel_button';
								}
								else if (createFormId == 'formformaccounts_contacts'){
									modal_title = "new contacts";
									full_form_btn = 'Contacts_subpanel_full_form_button';
									cancel_btn = 'Contacts_subpanel_cancel_button';
									
								}
								else if (createFormId == 'formformaccount_cases'){
									modal_title = "new tickets";
									full_form_btn = 'Cases_subpanel_full_form_button';
									cancel_btn = 'Cases_subpanel_cancel_button';
								}
								else if (createFormId == 'formaccount_aos_contracts'){
									modal_title = "new contracts";
									full_form_btn = 'AOS_contracts_subpanel_full_form_button';
									cancel_btn = 'AOS_contracts_subpanel_cancel_button';
								}
								else if (createFormId == 'formformNotes'){
									modal_title = "new notes";
									full_form_btn = 'Notes_subpanel_full_form_button';
									cancel_btn = 'Notes_subpanel_cancel_button';
								}
								else if (createFormId == 'formformbill_billing_accounts'){
									modal_title = "new Bill";
									full_form_btn = 'bill_Billing_subpanel_full_form_button';
									cancel_btn = 'bill_Billing_subpanel_cancel_button';
								}
								else{
									
									modal_title = "new document";
									full_form_btn = 'Documents_subpanel_full_form_button';
									cancel_btn = 'Documents_subpanel_cancel_button';
										
								}
								$('#activityCreationModal').find('.modal-body').html(res);
								$('#activityCreationModal').find('.modal-title').text(modal_title);
								if(createFormId != 'formaccount_aos_contracts'){
								$('#activityCreationModal').find('[name=' + full_form_btn + ']').remove();
								}
								//if(createFormId != "formformdocuments_accounts"){
								$('#activityCreationModal').find('.modal-body').find('[name=' + save_btn_name + ']').attr('onclick', 'saveActivityFromModal("' + submitFormId + '")').attr('type', 'button');
								$('#activityCreationModal').find('.modal-body').find('[name=' + cancel_btn + ']').attr('onclick', '$("#activityCreationModal").modal("hide");').attr('type', 'button');
								//}else{
								$(document).ready(function(){
									if(createFormId == 'formformbill_billing_accounts'){
										var textEle=document.getElementById("billingamount");
										if(textEle != null && textEle != undefined){
											 var str;
											 var i;
											 var st;
											 var j;
											 var text_val;
											 var cusText;
											 textEle.onkeyup=function(){
											 cusText='';
											 str='';
											 text_val=textEle.value;
											  console.log(text_val);
											 for(var u=0;u<text_val.length;u++)
											 {
											 if(text_val[u]!=",")
											  cusText+=text_val[u];
											 
											 }
											 console.log(cusText);
												
												i=eval(cusText.length-1);
												j=eval(cusText.length-4);
											while(i>-1){
											if(i==j && cusText.length > 3 ){
												j-=3;
											 str=cusText[i]+","+str;
											
											 }else{
											 str=cusText[i]+str;
											 }
											 i--;
											  }
											
											
											 textEle.value=str;
											 }
										}
									}
								});
								//$('#activityCreationModal').find('.modal-body').find('[name=' + save_btn_name + ']').attr('onclick', "var _form = document.getElementById('form_SubpanelQuickCreate_Documents'); disableOnUnloadEditView(); _form.action.value='Save';if(check_form('form_SubpanelQuickCreate_Documents'))return SUGAR.subpanelUtils.inlineSave(_form.id, 'Documents_subpanel_save_button');return false;");
								//$('#Documents_subpanel_save_button').on("click",function(){
								//	setTimeout(function(){
								//if(check_form("form_SubpanelQuickCreate_Documents")) {	
								//var Call = document.getElementById('formformCalls');
							//	$('#activityCreationModal').modal('hide');
							//	},2000);
								/*
								  if(Call != null || Call != undefined){
								  showSubPanel('history', null, true);
								  showSubPanel('activities', null, true);
								  showSubPanel('documents', null, true);
								  showSubPanel_re('cases', null, true);
								  showSubPanel_re('opportunities', null, true);
								  showSubPanel_re('contacts', null, true);
								  }else{
								 
									SUGAR.ajaxUI.showLoadingPanel();
									setTimeout(function () {
									refreshLeadCard(function (res) {
									SUGAR.ajaxUI.hideLoadingPanel();
									});
									}, 300);
						
								}  
								*/
								//}
							//	});						
									
								//});	
							//	}
							//  $('#activityCreationModal').find('.modal-body').find('.panel-content').find('div:first').remove();
							
								$('#activityCreationModal').modal('show');
								addSpinner(cancel_btn);
							}
						});

					}, 300);
					clearInterval(cde);
				}
			},1300);
    }
	//
	
}

function customComposeEmail(type) {
    if (typeof type == "undefined") {
        return false;
    }
    else {
        var data_module = $('#detailModal').find('#data-record-module').val();
        var data_record_id = $('#detailModal').find('#data-record-id').val();
        var data_module_name = $('#detailModal').find('#data-record-name').val();
        if(typeof $('.email-link').attr('data-module') == 'undefined' || $('.email-link').attr('data-module') == '') {
            $('.email-link').attr('data-module', data_module);
        }

        if(typeof $('.email-link').attr('data-module-name') == 'undefined' || $('.email-link').attr('data-module-name') == '') {
            $('.email-link').attr('data-module-name', data_module_name);
        }

        if(typeof $('.email-link').attr('data-record-id') == 'undefined' || $('.email-link').attr('data-record-id') == '') {
            $('.email-link').attr('data-record-id', data_record_id);
        }

        // if(typeof $('.email-link').attr('data-email-address') == 'undefined' || $('.email-link').attr('data-email-address') == '') {
        //     $('.email-link').attr('data-email-address', 'Email is empty');
        // }

        $(document).openComposeViewModal($('.email-link'), type);
    }
}

function clickCardMenu(ele) {
    if($(ele).closest('.leadCard').attr('lead-status') == 'Converted' || $(ele).closest('.leadCard').attr('lead-status') == 'Transaction') {
        $(ele).closest('div').find('.card-actions').css('width', '30px');
        $(ele).closest('div').find('.card-actions').find('#failLead').remove();
    }
    $(ele).closest('div').find('.card-actions').toggle();
}

function clickAction(ele) {
    $(ele).closest('.card-actions').hide();
}

function changeToDead(id, ele) {
    var mb = messageBox({
        size: 'sm',
        headerContent: '<button type="button" class="close btn-cancel" aria-label="Close"><span aria-hidden="true">×</span></button><h3 class="modal-title text-uppercase text-black"></h3>'
    });
    mb.setTitle(SUGAR.language.get("app_strings", "LBL_CLOSE_ACTIVITY_HEADER"));
    mb.setBody(SUGAR.language.translate('', 'LBL_CONFIRM_DEAD_LEAD'));
    mb.on('ok', function() {
        mb.remove();
            changeLeadStatus(id, 'Dead', function (res) {
                if (res) {
                    $(ele).closest('.leadCard').remove();
                }
            });
    });
    mb.on('cancel', function() {
        mb.remove();
    });
    mb.show();

}

function openFilter() {
    $('#filterModal').modal('show');
}

function convertSugarDateFormatToDatePickerFormat() {
    var date_format = 'dd/mm/yy';
    if (typeof cal_date_format != "undefined") {

        var re = new RegExp('%', 'g');
        date_format = cal_date_format;
        date_format = date_format.replace(re, '').replace('m', 'mm').replace('Y', 'yy').replace('d', 'dd');
    }
    return date_format;
}

function showHideDateFields(type) {
    if (typeof type == "undefined") {
        type = 'is_between';
    }
    if (type == 'is_between') {
       $( "#date_to, #date_from").datepicker();
        $('.ui-datepicker-trigger').html('<span class="suitepicon suitepicon-module-calendar"></span>').addClass('button');
    }
     $.ajax({
                type: "POST",
                url: "index.php?entryPoint=getDate",
                data: {
                    time:type
                },
                dataType: "json",
                success: function(data) {
				$("#date_from").val(data.from);
				$("#date_to").val(data.to);
										
                },
                error: function (error) {
                    
                }
            });
}

function periodChange() {
    showHideDateFields($('#period').val());
}

function clickToFilter(evt, ele) {
	var province=document.getElementById("provincecode_c").value;
    if (validateFilterForm()) {
		if($('#users').val() != null){
			nameTag = '';
			for(let i = 0; i<$('#users').val().length; i++){
				if(i > 0){
				nameTag += ","+$('#users option[value="' + $('#users').val()[i] + '"]').text();
				}else{
				nameTag += $('#users option[value="' + $('#users').val()[i] + '"]').text();
				}
			}
		}else 
		{
			if($('#group-users').val() != null && $('#group-users').val() != ''){
				nameTag = '';
				for(let i = 0; i<$('#group-users').val().length; i++){
					if(i > 0){
					nameTag += ","+$('#group-users option[value="' + $('#group-users').val()[i] + '"]').text();
					}else{
					nameTag += $('#group-users option[value="' + $('#group-users').val()[i] + '"]').text();
					}
				}
			
			}else{
				nameTag = '';
				if(province == "All"){
					nameTag = province;
				}else{
					
						nameTag += $('#provincecode_c option[value="' + $('#provincecode_c').val() + '"]').text();
				}
				
			}
		}
        $('#filterModal').modal('hide');

       SUGAR.ajaxUI.showLoadingPanel();
        setTimeout(function() {
            $.ajax({
                type: "POST",
                url: "index.php?module=Leads&action=MTS_filter&sugar_body_only=1",
                dataType: "html",
                data: {
					"page_id": 1,
					"province": province,
                    "date_from": $('#filterModal').find('#date_from').val(),
                    "date_to": $('#filterModal').find('#date_to').val(),
                    "period": $('#filterModal').find('#period').val(),
					"groups":$('#filterModal').find('#group-users').val(),
					"users":$('#filterModal').find('#users').val()
                },
                success: function (dashlet) {
                    $(ele).closest('.leadCardContainer').find('.home-spinner').hide();
                    $(ele).closest('.bd-center').html(dashlet);
					$(document).ready(function(){
						
						$("#showPeriod").html("("+$("#period option:selected").text()+")-"+nameTag);
						
					});
                    SUGAR.ajaxUI.hideLoadingPanel();
                },
                error: function (error) {
                    $(ele).closest('.leadCardContainer').find('.home-spinner').hide();
                    console.log(error);
                    SUGAR.ajaxUI.hideLoadingPanel();
                }
            });
            return true;
        }, 300);
    }
}

function validateFilterForm() {
    $('#filterModal').find('.error').hide();
    var is_valid = true;
    if ($('#period').val() == 'is_between') {
        var fields = ["date_from", "date_to"];
        for (var i = 0; i < fields.length; i++) {
            var field = $('#' + fields[i]);
            if (field.val() == '' || typeof field.val() == "undefined") {
                $(field).closest('.row').find('.error').show();
                is_valid = false;
            }
        }
    }
    return is_valid;
}

function initDragAndDrop() {

    $('div[status="New"]').sortable({
        connectWith: ".drop-target",
        scroll: true,
        // delay: 1000,
        stop: function (event, ui) {
            var lead_id = $(ui.item).attr('lead-id');
            var current_status = $(ui.item).attr('lead-status');
            var new_status = $(ui.item).closest('.cards').attr('status');
            if(current_status != new_status) {
                if(new_status == 'Converted') {
                    var mb = messageBox({
                        size: 'sm',
                        headerContent: '<button type="button" class="close btn-cancel" aria-label="Close"><span aria-hidden="true">×</span></button><h3 class="modal-title text-uppercase text-black"></h3>'
                    });
                    mb.setTitle(SUGAR.language.get("app_strings", "LBL_CLOSE_ACTIVITY_HEADER"));
                    mb.setBody(SUGAR.language.translate('', 'LBL_CONFIRM_CONVERTED_LEAD'));
                    mb.on('ok', function() {
                        mb.remove();
                        changeLeadStatus(lead_id, new_status, function () {
                            setTimeout(function () {
                                refreshLeadCard(function (res) {
                                    SUGAR.ajaxUI.hideLoadingPanel();
                                });
                            }, 300);
                        });
                    });
                    mb.on('cancel', function() {
                        mb.remove();
                        SUGAR.ajaxUI.showLoadingPanel();
                        setTimeout(function () {
                            refreshLeadCard(function (res) {
                                SUGAR.ajaxUI.hideLoadingPanel();
                            });
                        }, 300);
                    });
                    mb.show();
                }
                else {
                    changeLeadStatus(lead_id, new_status);
                }
            }
        }
    });

    $('div[status="In Process"]').sortable({
        connectWith: ".drop-target",
        scroll: true,
        // delay: 1000,
        stop: function (event, ui) {
            var lead_id = $(ui.item).attr('lead-id');
            var current_status = $(ui.item).attr('lead-status');
            var new_status = $(ui.item).closest('.cards').attr('status');
            if(current_status != new_status) {
                if(new_status == 'Converted') {
                    var mb = messageBox({
                        size: 'sm',
                        headerContent: '<button type="button" class="close btn-cancel" aria-label="Close"><span aria-hidden="true">×</span></button><h3 class="modal-title text-uppercase text-black"></h3>'
                    });
                    mb.setTitle(SUGAR.language.get("app_strings", "LBL_CLOSE_ACTIVITY_HEADER"));
                    mb.setBody(SUGAR.language.translate('', 'LBL_CONFIRM_CONVERTED_LEAD'));
                    mb.on('ok', function() {
                        mb.remove();
                        changeLeadStatus(lead_id, new_status, function () {
                            setTimeout(function () {
                                refreshLeadCard(function (res) {
                                    SUGAR.ajaxUI.hideLoadingPanel();
                                });
                            }, 300);
                        });
                    });
                    mb.on('cancel', function() {
                        mb.remove();
                        SUGAR.ajaxUI.showLoadingPanel();
                        setTimeout(function () {
                            refreshLeadCard(function (res) {
                                SUGAR.ajaxUI.hideLoadingPanel();
                            });
                        }, 300);
                    });
                    mb.show();
                }
                else {
                    changeLeadStatus(lead_id, new_status);
                }
            }
        }
    });

    $('div[status="Converted"], div[status="Transaction"]').sortable({ cancel: '.leadCard' });
    $('div[status="Converted"], div[status="Transaction"]').disableSelection();

}

function changeCur(id){
	
	var textEle=document.getElementById(id);
		 var str;
		 var i;
		 var st;
		 var j;
		 var text_val;
		 var cusText;
		 textEle.onkeyup=function(){
		 cusText='';
		 str='';
		 text_val=textEle.value;
		  console.log(text_val);
		 for(var u=0;u<text_val.length;u++)
		 {
		 if(text_val[u]!=",")
		  cusText+=text_val[u];
		 
		 }
		 console.log(cusText);
			
			i=eval(cusText.length-1);
			j=eval(cusText.length-4);
		while(i>-1){
		if(i==j && cusText.length > 3 ){
			j-=3;
		 str=cusText[i]+","+str;
		
		 }else{
		 str=cusText[i]+str;
		 }
		 i--;
		  }
		  console.log("noi lan thu"+i+":"+str);
		
		 textEle.value=str;
		 }
	
	
}

function showCreationModal(type, status) {
   showSpinner('creationModal');
    setTimeout(function() {
        var url = "index.php?module=Leads&action=QuickCreate&sugar_body_only=1";
        if(type == 'Leads') {
            url = "index.php?module=Leads&action=QuickCreate&sugar_body_only=1";
        }
        else {
            url = "index.php?module=Accounts&action=QuickCreate&sugar_body_only=1";
        }
        $.get(url, function (res) {
            $('#creationModal').find('.modal-body').html(res);
            $('#creationModal').find('.modal-body').find('#save_and_continue, #Leads_subpanel_full_form_button, #Leads_dcmenu_cancel_button, #Accounts_subpanel_full_form_button, #Accounts_dcmenu_cancel_button').remove();
            $('#creationModal').find('.modal-body').find('#status').val(status).prop('disabled', true);
			 $('#creationModal').find('.modal-body').find('#' + type + '_dcmenu_save_button').attr('onclick', "handleSaveLead('" + type + "','" + status + "')").attr('type', 'button');
            $('#creationModal').find('.modal-body').append('<script type="text/javascript" src="custom/modules/Leads/js/Validate.js"></script>');
			$(document).ready(function(){
				//if(cloneButt == null || cloneButt == undefined){
					
				var butt=document.getElementsByName(type+'_dcmenu_save_button');
				var butt1=document.getElementsByName('Accounts_dcmenu_save_button');
				var butt2=document.getElementsByName('Leads_dcmenu_save_button');
				
				var cloneButt=butt[0].cloneNode(true);
				if(butt1 != null && butt1 != undefined){
					for(let n = 0; n< butt1.length;n++){
						butt1[n].remove();
					}
				}
				if(butt2 != null && butt2 != undefined){
					for(let m = 0; m< butt2.length;m++){
						butt2[m].remove();
					}
				}
				if(butt1.length == 0 && butt2.length == 0){
				var footer  = document.getElementsByClassName("modal-footer");
				footer[1].insertBefore(cloneButt,document.getElementById("closeButton"));
				}	
				 $('#creationModal').find('.modal-footer').find('#' + type + '_dcmenu_save_button').attr('onclick', "handleSaveLead('" + type + "','" + status + "')").attr('type', 'button');
				if(type == "Opportunities"){				
				changeCur("opportunity_amount");
				}
				if(type == "Leads"){
					
				changeCur("opportunity_amount");
				//var proVal = $("#aos_products_leads_1_name").val();
				changeCur("opportunity_amount");
				changeCur("depositsamount");
				changeCur("paymentsamount");
				$("#opportunity_name").css("width", "82%");
				$("#opportunity_name").css("margin-left", "-11px");
				$('<span  style="display:inline-block;margin-left:5px"><input id="getNameCheck" type="checkbox" checked="true"></span>').insertAfter($("#opportunity_name"));
				$("#opportunity_amount").css("background-color", "yellow");
				$("#depositsamount").css("background-color", "yellow");
				$("#paymentsamount").css("background-color", "yellow");
				$("#opportunity_amount").css("color", "red");
				$("#depositsamount").css("color", "red");
				$("#paymentsamount").css("color", "red");
				$("#opportunity_amount").css("font-weight", "bold");
				$("#depositsamount").css("font-weight", "bold");
				$("#paymentsamount").css("font-weight", "bold");
				
				$("#phone_mobile").on("keyup", function(){
					
					if($("#getNameCheck").is(":checked")){
						if($("#aos_products_leads_1_name").val() != ""){
						$("#opportunity_name").val($("#aos_products_leads_1_name").val()+"_"+$("#phone_mobile").val());
						}else{
						$("#opportunity_name").val($("#phone_mobile").val());	
						}
					}
				});
				var deposite;
				$.ajax({
					type: "POST",
					url: "index.php?entryPoint=getDeposite",
					success: function(pData) {
						deposite = pData;
						var proVal = $("#aos_products_leads_1_name").val();
						var proAmo = $("#opportunity_amount").val();
						var depoAmount = $("#depositsamount").val();
						setInterval(function(){
							if($("#aos_products_leads_1_name").val() != null){
								if($("#aos_products_leads_1_name").val() != proVal){
									if($("#aos_products_leads_1_name").val() != ""){
										if($("#getNameCheck").is(":checked")){
											if($("#phone_mobile").val() != ""){
											$("#opportunity_name").val($("#aos_products_leads_1_name").val()+"_"+$("#phone_mobile").val());
											}else{
											$("#opportunity_name").val($("#aos_products_leads_1_name").val());	
											}
										}
										$.ajax({
														type: "POST",
														url: "index.php?entryPoint=loadPrice",
														dataType:"json",
														data:{
															name:$("#aos_products_leads_1_name").val()
														},
														success: function(data) {
															if(data != null && data != '' && data != undefined){
																var amount = parseInt(data);
																$("#opportunity_amount").val(changeCurrency(String(parseInt(data))));
																proVal = $("#aos_products_leads_1_name").val();
																proAmo = $("#opportunity_amount").val();
																var depositsAmount = amount * (deposite / 100);
																$("#depositsamount").val(changeCurrency(String(parseInt(depositsAmount))));
																var paymentsAmount = amount - parseInt(depositsAmount);
																$("#paymentsamount").val(changeCurrency(String(paymentsAmount)));
															}else{
																$("#opportunity_amount").val(0);
																proVal = 0;
																proAmo = 0;
																$("#depositsamount").val(0);
																$("#paymentsamount").val(0);
															}
														},
														error: function (error) {
												
														}
										});
									}else{
										if($("#opportunity_amount").val() != proAmo){
											proVal = $("#aos_products_leads_1_name").val();
											proAmo = $("#opportunity_amount").val();
											if(proAmo != "" && proAmo != null &&  proAmo != undefined ){
												var proAmo2 = proAmo.replace(/,/g,'');
												var depositsAmount = proAmo2 * (deposite / 100);
												$("#depositsamount").val(changeCurrency(String(parseInt(depositsAmount))));
												var paymentsAmount = proAmo2 - parseInt(depositsAmount);
												$("#paymentsamount").val(changeCurrency(String(paymentsAmount)));
											}
										}
									}
								}else{
									if($("#opportunity_amount").val() != proAmo){
										proVal = $("#aos_products_leads_1_name").val();
										proAmo = $("#opportunity_amount").val();
										if(proAmo != "" && proAmo != null &&  proAmo != undefined ){
											var proAmo2 = proAmo.replace(/,/g,'');
											var depositsAmount = proAmo2 * (deposite / 100);
											$("#depositsamount").val(changeCurrency(String(parseInt(depositsAmount))));
											var paymentsAmount = proAmo2 - parseInt(depositsAmount);
											$("#paymentsamount").val(changeCurrency(String(paymentsAmount)));
										}
									}
								}
							}
							if($("#depositsamount").val() != depoAmount && $("#depositsamount").val() != ''){
								depoAmount = $("#depositsamount").val();
								proAmo = $("#opportunity_amount").val();
								var proAmo2 = proAmo.replace(/,/g,'');
								var paymentsAmount = proAmo2 - parseInt(depoAmount.replace(/,/g,''));
								$("#paymentsamount").val(changeCurrency(String(paymentsAmount)));
							}
							
						},1000);
					}
				});
				 document.getElementById('paymentsamount').readOnly = true;
				var account_name=document.getElementsByClassName('edit-view-row-item')[6];
				var account_name_label=account_name.getElementsByClassName("label")[0];
				var requ=account_name_label.getElementsByClassName("required")[0];
				if($('#bussiness_type_c').val() == "B2B"){
					var req=document.getElementsByClassName('required')[0];
					var clone=req.cloneNode(true);
					account_name_label.appendChild(clone);
				}
				$('#bussiness_type_c').on('change',function(){
					account_name=document.getElementsByClassName('edit-view-row-item')[6];
					account_name_label=account_name.getElementsByClassName("label")[0];
					requ=account_name_label.getElementsByClassName("required")[0];
					if($('#bussiness_type_c').val() == "B2B"){
						if(requ == null || requ == undefined){
						var req=document.getElementsByClassName('required')[0];
						var clone=req.cloneNode(true);
						account_name_label.appendChild(clone);
						}
					}
					else{
						
						if(requ != null && requ != undefined){
						
						requ.remove();
						
						}
						
					}
				// <span class="required">*</span>
				});
				//start
				//var emailInput=document.querySelector("[id^=Leads1emailAddress]");
				var pb=document.getElementById('phone_mobile_dup_detector_info');
				var show_info = pb.cloneNode(true);
				show_info.setAttribute("id", "show_info1");
				//
				
				var email_address=document.getElementsByClassName("email-address-input-group");
				for (var i=1;i<email_address.length;i++){
					
					email_address[i].appendChild(show_info);
				
					
					
					var leads2=email_address[i].querySelector("[id^='Leads']");
					
					leads2.onblur=function(){
						/*
						if(typeof(total_element) == "undefined")
						var total_element;
				if (typeof (total_element) == 'undefined')
					total_element = 0;
					total_element++;
					
					inside_submit = true;
					*/
					$('#creationModal').find('.modal-body').find('#error').hide();
					
					var class_info2 = "show_info1";// viết thêm class show lỗi
					var form_name2 = 'form_DCQuickCreate_' + type;//name form
					var module_name2 = type;
					var field_value2 = encodeURIComponent(leads2.value);
					
					var field_name2 = leads2.id;
					var record_id2 = $('#' + form_name2 + ' input[name="record"]').val();
					var data2 = "record=" + record_id2 + "&module_name=" + module_name2 + "&field_name=" + field_name2 + "&field_value=" + field_value2;
					/*
					if (form_name2.substring(0, 16) == "form_QuickCreate" || form_name2.substring(0, 24) == "form_SubpanelQuickCreate" || form_name2.substring(0, 18) == "form_DCQuickCreate")
						var curSubmit = $("input[name$='save_button']", $('#' + form_name2));
					else
						var curSubmit = $("input[type=submit]", $('#' + form_name2));
					*/
					if (field_value2 != '') {
           // $("#" + class_info).show();
            $("#" + class_info2).fadeIn(400).html('<img src="custom/include/SugarFields/Fields/Dupdetector/image/ajax-loading.gif" />');
					$.ajax({
                type: "POST",
                url: "index.php?module=Dupdetector&action=check_dup&to_pdf=1&sugar_body_only=true&record_id=" + record_id2,
                data: data2,
                cache: false,
                async: true,
                success: function(result) {
                    if (result != '') {
                        res = eval('(' + result + ')');
                        total_element--;
                        if (res.status == '') {
                            $("#" + class_info2).html('');
                            
                        } else {
							inside_submit == true
							//alert(res.status);
                            ele_status[field_name2] = res.status;
                            var more_info_msg2 = '<img border="0"  id ="tooltip_' + field_name2 + '" onclick="return SUGAR.util.showHelpTips(this,ele_status['+field_name2+']);" src="themes/default/images/helpInline.gif" >';
                            $("#" + class_info2).html('<img src="custom/include/SugarFields/Fields/Dupdetector/image/error.gif" /> Khách hàng này đã tồn tại ! ' + more_info_msg2);
                            SUGAR.util.evalScript(more_info_msg2);
                            
                        }
                    }
                    //SUGAR.ajaxUI.hideLoadingPanel();
                   // if (total_element <= 0 && inside_submit == true) {
                    //    inside_submit = false;
                    //    $(curSubmit).trigger("click");
                   // }
                },
                failure: function() {
                    //SUGAR.ajaxUI.hideLoadingPanel();
                }
            });
			
			
					}
					else{
						
						$("#" + class_info2).html('');
						
						
					}
						
					};
					
				}
				
				
					
				
				
				$('.email-address-add-button').on("click",function(){
					
					//while(email_address.length<5){
					//email_address=document.getElementsByClassName("email-address-input-group");
					
					
					var show_inf = pb.cloneNode(true);
					show_inf.setAttribute("id", "show_info"+eval(email_address.length-1));
					email_address[email_address.length-1].appendChild(show_inf);
					//alert("sucessful");
					var leads1=email_address[email_address.length-1].querySelector("[id^='Leads']");
					//alert(leads.id);
					
					leads1.onblur=function(){
						/*
						if(typeof(total_element) == "undefined")
						var total_element;
				if (typeof (total_element) == 'undefined')
					total_element = 0;
					total_element++;
					
					inside_submit = true;
					*/
					$('#creationModal').find('.modal-body').find('#error').hide();
					
					// viết thêm class show lỗi
					var form_name = 'form_DCQuickCreate_' + type;//name form
					var module_name1 = type;
					var field_value1 = encodeURIComponent(this.value);
					
					var field_name1 = this.id;
					var e=parseInt(field_name1.slice(18));
					var class_info1 = "show_info"+eval(e+1);
					var record_id1 = $('#' + form_name + ' input[name="record"]').val();
					var data1 = "record=" + record_id1 + "&module_name=" + module_name1 + "&field_name=" + field_name1 + "&field_value=" + field_value1;
					/*
					if (form_name.substring(0, 16) == "form_QuickCreate" || form_name.substring(0, 24) == "form_SubpanelQuickCreate" || form_name.substring(0, 18) == "form_DCQuickCreate")
						var curSubmit = $("input[name$='save_button']", $('#' + form_name));
					else
						var curSubmit = $("input[type=submit]", $('#' + form_name));
					*/
					if (field_value1 != '') {
            //$("#" + class_info).show();
			
            $("#" + class_info1).fadeIn(400).html('<img src="custom/include/SugarFields/Fields/Dupdetector/image/ajax-loading.gif" />');
				$.ajax({
                type: "POST",
                url: "index.php?module=Dupdetector&action=check_dup&to_pdf=1&sugar_body_only=true&record_id=" + record_id1,
                data: data1,
                cache: false,
                async: true,
                success: function(result) {
                    if (result != '') {
                        res = eval('(' + result + ')');
                        //total_element--;
                        if (res.status == '') {
                            $("#" + class_info1).html('');
                            
                        } else {
							
							//alert(res.status);
                            ele_status[field_name1] = res.status;
                            var more_info_msg = '<img border="0"  id ="tooltip_' + field_name1 + '" onclick="return SUGAR.util.showHelpTips(this,ele_status['+field_name1+']);" src="themes/default/images/helpInline.gif" >';
                            $("#" + class_info1).html('<img src="custom/include/SugarFields/Fields/Dupdetector/image/error.gif" /> Khách hàng này đã tồn tại ! ' + more_info_msg);
                            SUGAR.util.evalScript(more_info_msg);
                            
                        }
                    }
                    //SUGAR.ajaxUI.hideLoadingPanel();
                   // if (total_element <= 0 && inside_submit == true) {
                    //    inside_submit = false;
                   //     $(curSubmit).trigger("click");
                   // }
                },
                failure: function() {
                   // SUGAR.ajaxUI.hideLoadingPanel();
                }
            });
			
			
					}
					else{

						$("#" + class_info1).html('');

					}						
					};
					
					//}
				
				});
				
			}
			
			});
			// $('#creationModal').modal('show');
        });
    }, 300);
}

function showOpportunityModal(account_id, account_name)
{
    showSpinner('activityCreationModal');
    prepareParamsForOppForm(account_id, account_name);
    setTimeout(function () {
        $.ajax({
            type: "POST",
            url: "index.php",
            data: $('#formformaccounts_opportunities').serialize(),
            success: function(res)
            {
				
                var modal_title = lead_mods.LBL_NEW_OPPORTUNITY;
                var full_form_btn = 'Opportunities_subpanel_full_form_button';
                var cancel_btn = 'Opportunities_subpanel_cancel_button';
                var save_btn_name = 'Opportunities_subpanel_save_button';
                $('#activityCreationModal').find('.modal-body').html(res);
                $('#activityCreationModal').find('.modal-title').text(modal_title);
                $('#activityCreationModal').find('[name=' + full_form_btn + ']').remove();
                $('#activityCreationModal').find('.modal-body').find('[name=' + save_btn_name + ']').attr('onclick', "saveOpportunityFromModal('"+account_id+"','form_SubpanelQuickCreate_Opportunities')").attr('type', 'button');
                $('#activityCreationModal').find('.modal-body').find('[name=' + cancel_btn + ']').attr('onclick', '$("#activityCreationModal").modal("hide");').attr('type', 'button');
                $('#activityCreationModal').find('.modal-body').find('.panel-content').find('div:first').remove();
                $('#activityCreationModal').modal('show');
				changeCur("amount");
				changeCur("depositsamount");
				changeCur("paymentsamount");
				$("#amount").css("background-color", "yellow");
				$("#depositsamount").css("background-color", "yellow");
				$("#paymentsamount").css("background-color", "yellow");
				$("#amount").css("color", "red");
				$("#depositsamount").css("color", "red");
				$("#paymentsamount").css("color", "red");
				$("#amount").css("font-weight", "bold");
				$("#depositsamount").css("font-weight", "bold");
				$("#paymentsamount").css("font-weight", "bold");
                addSpinner(cancel_btn);
				$(document).ready(function(){
					var oppStatus = document.getElementsByClassName("edit-view-row-item");
					//oppStatus[3].style.display = "none";
					//$("#sales_stage").on("change",function(){
					//	$("#sales_stage").val("Prospecting");
					//})
					$("#sales_stage").val("Prospecting");
					var deposite;
					$.ajax({
						type: "POST",
						url: "index.php?entryPoint=getDeposite",
						success: function(pData) {
							deposite = pData;
							var proVal = $("#aos_products_opportunities_1_name").val();
							var proAmo = $("#amount").val();
							var depoAmount = $("#depositsamount").val();
							setInterval(function(){
								if($("#date_closed").val() != null && $("#date_closed").val() != "" ){
									if($("#aos_products_opportunities_1_name").val() != ""){
										$("#name").val($("#aos_products_opportunities_1_name").val()+"_"+$("#date_closed").val().replace(/\//g,""));
									}else{
										$("#name").val($("#date_closed").val().replace(/\//g,""));	
									}
								}
								if($("#aos_products_opportunities_1_name").val() != null){
									if($("#aos_products_opportunities_1_name").val() != proVal){
										if($("#aos_products_opportunities_1_name").val() != ""){
											if($("#date_closed").val() != ""){
												$("#name").val($("#aos_products_opportunities_1_name").val()+"_"+$("#date_closed").val().replace(/\//g,""));
											}else{
												$("#name").val($("#aos_products_opportunities_1_name").val());	
											}
											$.ajax({
												type: "POST",
												url: "index.php?entryPoint=loadPrice",
												dataType:"json",
												data:{
													name:$("#aos_products_opportunities_1_name").val()
												},
												success: function(data) {
													if(data != null && data != '' && data != undefined){
													var amount = parseInt(data);
													$("#amount").val(changeCurrency(String(parseInt(data))));
													proVal = $("#aos_products_opportunities_1_name").val();
													proAmo = $("#amount").val();
													var depositsAmount = amount * (deposite / 100);
													$("#depositsamount").val(changeCurrency(String(parseInt(depositsAmount))));
													var paymentsAmount = amount - parseInt(depositsAmount);
													$("#paymentsamount").val(changeCurrency(String(paymentsAmount)));
													}else{
													$("#amount").val(0);
													proVal = 0;
													proAmo = 0;
													$("#depositsamount").val(0);
													$("#paymentsamount").val(0);
													}
												},
												error: function (error) {
													
												}
											});
										}else{
											if($("#amount").val() != proAmo){
												proVal = $("#aos_products_opportunities_1_name").val();
												proAmo = $("#amount").val();
												if(proAmo != "" && proAmo != null &&  proAmo != undefined ){
													var proAmo2 = proAmo.replace(/,/g,'');
													var depositsAmount = proAmo2 * (deposite / 100);
													$("#depositsamount").val(changeCurrency(String(parseInt(depositsAmount))));
													var paymentsAmount = proAmo2 - parseInt(depositsAmount);
													$("#paymentsamount").val(changeCurrency(String(paymentsAmount)));
												}
											}
										}
									}else{
										if($("#amount").val() != proAmo){
											proVal = $("#aos_products_opportunities_1_name").val();
											proAmo = $("#amount").val();
											if(proAmo != "" && proAmo != null &&  proAmo != undefined ){
												var proAmo2 = proAmo.replace(/,/g,'');
												var depositsAmount = proAmo2 * (deposite / 100);
												$("#depositsamount").val(changeCurrency(String(parseInt(depositsAmount))));
												var paymentsAmount = proAmo2 - parseInt(depositsAmount);
												$("#paymentsamount").val(changeCurrency(String(paymentsAmount)));
											}
										}
									}
								}
								if($("#depositsamount").val() != depoAmount){
									depoAmount = $("#depositsamount").val();
									proAmo = $("#amount").val();
									if(proAmo != "" && proAmo != null &&  proAmo != undefined ){
										var proAmo2 = proAmo.replace(/,/g,'');
										var paymentsAmount = proAmo2 - parseInt(depoAmount.replace(/,/g,''));
										$("#paymentsamount").val(changeCurrency(String(paymentsAmount)));
									}
								}
								
							},1000);
						}
					});
					document.getElementById('paymentsamount').readOnly = true;
				});
            }
        });

    }, 300);
	
	
}

function handleSaveLead(type, status) {
	var aError=document.getElementById("aError");
	/*
	if(typeof(total_element) == "undefined")
    var total_element;
if (typeof (total_element) == 'undefined')
        total_element = 0;
    total_element++;
	*/
	var save;
	var save1;
	var save2;
	//inside_submit = true;
    $('#creationModal').find('.modal-body').find('#error').hide();
	var class_info = $("#phone_mobile").attr('id') + "_dup_detector_info";
    var form_name = 'form_DCQuickCreate_' + type;//name form
	var module_name = type;
	var field_value = encodeURIComponent($("#phone_mobile").val());
	var field_name = $("#phone_mobile").attr('id');
	var record_id = $('#' + form_name + ' input[name="record"]').val();
	var data = "record=" + record_id + "&module_name=" + module_name + "&field_name=" + field_name + "&field_value=" + field_value;
	  if (form_name.substring(0, 16) == "form_QuickCreate" || form_name.substring(0, 24) == "form_SubpanelQuickCreate" || form_name.substring(0, 18) == "form_DCQuickCreate")
        var curSubmit = $("input[name$='save_button']", $('#' + form_name));
    else
        var curSubmit = $("input[type=submit]", $('#' + form_name));
    if(check_form(form_name)) {
        $('#' + form_name).find('input[name=action]').val('MTS_saveLead');
        $('#' + form_name).find('input[name=module]').val('Leads');
		//start
		
		$.ajax({
                type: "POST",
                url: "index.php?module=Dupdetector&action=check_dup&to_pdf=1&sugar_body_only=true&record_id=" + record_id,
                data: data,
                cache: false,
                async: true,
                success: function(result) {
                    if (result != '') {
                        res = eval('(' + result + ')');
                        total_element--;
						
                        if (res.status == '') {
							//fix
							if($("#bussiness_type_c").val() == "B2B"){
								if($("#account_name").val() != ""){
									
							if (aError != undefined){
							aError.style.display = "none";
							}		
							
							//
							save=1;
							
					//$('#creationModal').find('.modal-body').find('#error').hide();
					
							
							//
							
							
							
							
								}//account name != ""
								else
								{
								//$("#" + class_info).html('<img src="custom/include/SugarFields/Fields/Dupdetector/image/error.gif" /> account name không được để trống ! ');
										save=0;
										
										if (aError == undefined){
											if($('#Leads_dcmenu_save_button').val()=="Save"){
												$('#account_name').after('<p style="color:red" id="aError">Missing required field: Account Name</p>');
											}
											else{
												$('#account_name').after('<p style="color:red" id="aError">	Thiếu trường bắt buộc: Tên công ty</p>');
											
											}	
											
										}
								}
						}else   // type=b2c
						{
							if (aError != undefined){
							aError.style.display = "none";
							}
						save=1;	
							//
							
						//
						


						}							//alert("luu");
                           


						$("#" + class_info).html('');
                            if (res.settings == true) {
                                for (validate_dup_key in validate['form_DCQuickCreate_Leads']) {
                                    if (typeof (validate['form_DCQuickCreate_Leads'][validate_dup_key][0]) != 'undefined' &&
                                            validate['form_DCQuickCreate_Leads'][validate_dup_key][0] == 'phone_mobile' && validate['form_DCQuickCreate_Leads'][validate_dup_key][1] == 'error') {
                                    validate['form_DCQuickCreate_Leads'].splice(validate_dup_key, 1);
                                    }
                                }
							}
						   
                        } 
						else
						{
							// case trung so phone
							save=1;
							//save=1;
							if (aError != undefined){
								aError.style.display = "none";
							}
							ele_status['phone_mobile'] = res.status;
							var more_info_msg = '<img border="0"  id ="tooltip_' + field_name + '" onclick="return SUGAR.util.showHelpTips(this,ele_status[\'phone_mobile\']);" src="themes/default/images/helpInline.gif" >';
							$("#" + class_info).html('<img src="custom/include/SugarFields/Fields/Dupdetector/image/error.gif" /> Khách hàng này đã tồn tại ! ' + more_info_msg);
							SUGAR.util.evalScript(more_info_msg);
							if (res.settings == true) {
							for (validate_dup_key in validate['form_DCQuickCreate_Leads']) {
								if (typeof (validate['form_DCQuickCreate_Leads'][validate_dup_key][0]) != 'undefined' &&
								validate['form_DCQuickCreate_Leads'][validate_dup_key][0] == 'phone_mobile' && validate['form_DCQuickCreate_Leads'][validate_dup_key][1] == 'error') {
								validate['form_DCQuickCreate_Leads'].splice(validate_dup_key, 1);
								}
							}
								addToValidate('form_DCQuickCreate_Leads', 'phone_mobile', 'error', true, 'Please resolve conflict');
							}
							
                        }
                    }
					/*
                    SUGAR.ajaxUI.hideLoadingPanel();
                    if (total_element <= 0 && inside_submit == true) {
                        inside_submit = false;
                        $(curSubmit).trigger("click");
                    }
					*/
                },
                failure: function() {
                    SUGAR.ajaxUI.hideLoadingPanel();
                }
            });
		
		//alert('hehe'+save);
		//end
		var email_address3=document.getElementsByClassName("email-address-input-group");
		var y=1;
		var x=1;			
		var ar=[];		
					if(email_address3.length >1){
					var cde= setInterval(function(){
						if(x==y){
							
					var leads3=email_address3[x].querySelector("[id^='Leads']");
					x++;
					var more_info_msg3;
					var form_name3 = 'form_DCQuickCreate_' + type;//name form
					var module_name3 = type;
					var field_value3 = leads3?(encodeURIComponent(leads3.value)):'';
					
					var field_name3 = leads3?(leads3.id):'';
					var f=parseInt(field_name3.slice(18));
					var class_inf3 = "show_info"+eval(f+1);
					var record_id3 = $('#' + form_name3 + ' input[name="record"]').val();
					var data3 = "record=" + record_id3 + "&module_name=" + module_name3 + "&field_name=" + field_name3 + "&field_value=" + field_value3;
					
					
					if (field_value3 != '') {
            //$("#" + class_info).show();
			
            $("#" + class_inf3).fadeIn(400).html('<img src="custom/include/SugarFields/Fields/Dupdetector/image/ajax-loading.gif" />');
				$.ajax({
                type: "POST",
                url: "index.php?module=Dupdetector&action=check_dup&to_pdf=1&sugar_body_only=true&record_id=" + record_id3,
                data: data3,
                cache: false,
                async: true,
                success: function(result1) {
                    if (result1 != '') {
                        res1 = eval('(' + result1 + ')');
                        total_element--;
                        if (res1.status == '') {
                            $("#" + class_inf3).html('');
                            ar[y]=1;
							console.log(res1.status);
							
                        } else {
							ar[y]=0;
							//alert(res.status);
                            ele_status[field_name3] = res1.status;
                             more_info_msg3 = '<img border="0"  id ="tooltip_' + field_name3 + '" onclick="return SUGAR.util.showHelpTips(this,ele_status['+field_name3+']);" src="themes/default/images/helpInline.gif" >';
                            $("#" + class_inf3).html('<img src="custom/include/SugarFields/Fields/Dupdetector/image/error.gif" /> Khách hàng này đã tồn tại ! ' + more_info_msg3);
                            SUGAR.util.evalScript(more_info_msg3);
                            
                        }
						
                    }
					y++;
					
                    //SUGAR.ajaxUI.hideLoadingPanel();
                    //if (total_element <= 0 && inside_submit == true) {
                    //    inside_submit = false;
                  //      $(curSubmit).trigger("click");
                   // }
				   if(y==email_address3.length){
				   for(var o=0;o<ar.length;o++){
						
						if(ar[o]==0){
						save1=ar[o];
						break;
						}
						if(o==eval(ar.length-1)){
							if(ar[o]==1){
								save1=1;
							}
							
						}
					}
					
				}
                },
                failure: function() {
                   // SUGAR.ajaxUI.hideLoadingPanel();
                }
            });
				
						}
						else{
							 $("#" + class_inf3).html('');
							y++;
							ar[0]=1;
							 if(y==email_address3.length){
					for(var o=0;o<ar.length;o++){
						
						if(ar[o]==0){
						save1=ar[o];
						break;
						}
						if(o==eval(ar.length-1)){
							if(ar[o]==1){
								save1=1;
							}
							
						}
					}
					
				}
						}
						
					
						
					}
				if(x==email_address3.length){
						
						 clearInterval(cde);
					
						
				}	
					
				}, 50);
			//}
						
				}
				
				else{
					 
							ar[0]=1;
							 if(y==email_address3.length){
					for(var o=0;o<ar.length;o++){
						
						if(ar[o]==0){
						save1=ar[o];
						break;
						}
						if(o==eval(ar.length-1)){
							if(ar[o]==1){
								save1=1;
							}
							
						}
					}
					
				}
					
					
					
				}
					
				
				//while(save==undefined || save1==undefined ){
					/*
					var abc= setInterval(function(){
						
					}, 50);
					*/
					
		var abc= setInterval(function(){
			
		if(save!=undefined && save1 != undefined  && y==email_address3.length){
			
				if(save==1 && save1==1 ){
							
							$.ajax({
							type: "POST",
							url: "index.php?module=Leads&action=MTS_saveLead&sugar_body_only=1",
							data: $('#' + form_name).serialize() + '&parent_type=' + type + '&status=' + status,
							dataType: "JSON",
							success: function(res){
							if(res.status == 1) {
							var mobileFlow = document.getElementById("phone_mobile");
							if(mobileFlow != null && mobileFlow != undefined){
								//localStorage.setItem("mobileFlow",JSON.stringify(mobileFlow.value));
								
							}else{
								setCookie("mobileAccountFlow",$("#phone_alternate").val());//localStorage.setItem("mobileFlow",JSON.stringify($("#phone_alternate").val()));
							}	
							if(document.getElementById("tenkh") != null && document.getElementById("tenkh") != undefined){
								$('#tenkh').html($('#last_name').val());
							}
							if(localStorage.getItem("call") == "1"){
									$.ajax({
										type: "POST",
										url: "index.php?entryPoint=getAccount",
										data: {
											fromNumber:$("#phone_mobile").val()
					  
										},
										dataType:"json",
										success: function(res) {
											$("#tenkh").click(function(){
												window.open("https://"+window.location.hostname+"/index.php?module=Leads&offset=1&return_module=Leads&action=DetailView&record="+res.id,'abc');
											});
											localStorage.setItem("call","0");
										}
									});
							}				
							$('#creationModal').modal('hide');
							refreshLeadCard();
							}
							else {
							$('#creationModal').find('.modal-body').find('#error').text(res.msg).show();
							}
						},
						error: function(error)
						{
                    alert(error.message);
						}
							});
							
				}
			
		 clearInterval(abc);
		}
		}, 50);
			//	}			
							
		
	}
}
function retrievePage2(page_id, callback){
    setTimeout(function(){
        retrieveData2(page_id, callback);
    }, 300);
}

function retrieveData2(page_id, callback){
	var myCusVal;
	var New = false;
	var Dead = false;
	var myCus=document.getElementById('myCustomer');
	if(myCus.checked == true){
		myCusVal= true;
	}else{
		myCusVal= false;
	}
	var valueStatus = document.getElementById("myAct").value;
	var newOpt=document.getElementById('NewOpt');
	var deadOpt=document.getElementById('deadOpt');
	if(newOpt.checked == true){
		var New = true;
	}
	if(deadOpt.checked == true){
		var Dead = true;
	}
    var _cb = typeof callback != 'undefined' ? callback : false;
    $("#pageContainer").html('<img src="themes/SuiteP/images/loading.gif" width="48" height="48" align="baseline" border="0" alt="">');
    if(page_id == "1"){
  $.ajax({
                type: "POST",
                url: "index.php?entryPoint=retrieve_dash_page",
                data: {
					page_id : 1,
					myCustomer:(myCusVal == true)?1:0,
					act:valueStatus,
					getOnlyNew:(New == true)?1:0,
					getOnlyDead:(Dead == true)?1:0,
                },
               
                success: function(data) {
           var pageContent = data;
            outputPage(page_id,pageContent);
			$(document).ready(function(){
			$("#showPeriod").html("("+$("#period option:selected").text()+")");
				var abf = setInterval(function(){
					if(document.getElementById("myAct")){
						document.getElementById("myAct").value = valueStatus;
						clearInterval(abf);
					}
				},1000);
					if(myCusVal == true){
						document.getElementById('myCustomer').checked = true;
					}
					if(New == true){
						document.getElementById('NewOpt').checked = true;
							
					}
					if(Dead == true){
						document.getElementById('deadOpt').checked =true;
					}
					
			});
            if(_cb) _cb();
          
                },
                error: function (error) {
                    
                }
            });
  }else if(page_id == "4"){
	   $.ajax({
                type: "POST",
                url: "index.php?entryPoint=getDate",
                data: {
                    time:"this_month"
                },
                dataType: "json",
                success: function(data) {
					$.ajax({

					url : "index.php?entryPoint=retrieve_dash_page",
					dataType: 'html',
					type: 'POST',
					data : {
						'page_id' : page_id,
						"date_from2": data.from,
						"date_to2": data.to,
						"period2": "this_month"
					},

				success : function(dat) {
					var pageContent = dat;

					outputPage(page_id,pageContent);
					$(document).ready(function(){
						document.getElementById("period2").value = "this_month";
						$("#showPeriod2").html("("+$("#period2 option:selected").text()+")");
			
					});
					if(_cb) _cb();
				},
				error : function(request,error)
				{
					if(_cb) _cb();
				}
				});
									
                },
                error: function (error) {
                    
                }
            });
			
	   
  }else
  {
    $.ajax({

        url : "index.php?entryPoint=retrieve_dash_page",
        dataType: 'html',
        type: 'POST',
        data : {
            'page_id' : page_id
        },

        success : function(data) {
            var pageContent = data;

            outputPage(page_id,pageContent);
			
            if(_cb) _cb();
        },
        error : function(request,error)
        {
            if(_cb) _cb();
        }
    })
}
}
function refreshLeadCard(cb)
{
	
    var pageNum = parseInt($('ul.nav.nav-tabs.nav-dashboard li.active a').first().attr('id').substring(3));
    retrievePage(pageNum, function (res) {
        if(cb) {
            cb(res);
        }
    });
	
}
function refreshLeadCard2(cb)
{
	
    var pageNum = parseInt($('ul.nav.nav-tabs.nav-dashboard li.active a').first().attr('id').substring(3));
    retrievePage2(pageNum, function (res) {
        if(cb) {
            cb(res);
        }
    });
	
}

function closedetailModal()
{
    closeModal('detailModal');
    clearInterval(process_subpanel);
    SUGAR.ajaxUI.showLoadingPanel();
   setTimeout(function () {
        refreshLeadCard(function (res) {
            SUGAR.ajaxUI.hideLoadingPanel();
      });
   }, 300);

}


//Override email compose
$.fn.openComposeViewModal = function(source, type) {
    "use strict";
    var self = this;
    self.emailComposeView = null;
    var opts = $.extend({}, $.fn.EmailsComposeViewModal.defaults);
    var composeBox = $('<div></div>').appendTo(opts.contentSelector);
    composeBox.messageBox({
        "showHeader": false,
        "showFooter": false,
        "size": 'lg'
    });
    composeBox.setBody('<div class="email-in-progress"><img src="themes/' + SUGAR.themes.theme_name + '/images/loading.gif"></div>');
    composeBox.show();
    var ids = '&ids=';
	var currentModule=$(source).attr('data-module');
    if ($(source).attr('data-record-id') !== '') {
        ids = ids + $(source).attr('data-record-id');
    } else {
        var inputs = document.MassUpdate.elements;
        for (var i = 0; i < inputs.length; i++) {
            if (inputs[i].name === 'mass[]' && inputs[i].checked) {
                ids = ids + inputs[i].value + ',';
            }
        }
    }
    setTimeout(function(){
        var url = 'index.php?module=Emails&action=ComposeView&in_popup=1&targetModule=' + currentModule + ids;
        $.ajax({
            type: "GET",
            cache: false,
            url: url
        }).done(function(data) {
            if (data.length === 0) {
                console.error("Unable to display ComposeView");
                composeBox.setBody(SUGAR.language.translate('', 'ERR_AJAX_LOAD'));
                return;
            }
            composeBox.setBody(data);
            $('.btn-save-draft, .btn-disregard-draft').remove();
            // custom
            if(type == 'Normal') {
                $('#email_type').val('');
            }
            else {
                $('#email_type').val('Quotation');
            }

            $('#parent_type, #date_sent').closest('.edit-view-row-item').hide();
            //end
            self.emailComposeView = composeBox.controls.modal.body.find('.compose-view').EmailsComposeView();
            var targetCount = 0;
            var targetList = '';
            var populateModuleName = '';
            var populateEmailAddress = '';
            var populateModule = '';
            var populateModuleRecord = '';
            $('.email-compose-view-to-list').each(function() {
                populateModuleName = $(this).attr('data-record-name');
                populateEmailAddress = $(this).attr('data-record-email');
                populateModule = $(this).attr('data-record-module');
                populateModuleRecord = $(this).attr('data-record-id');
                if (targetCount > 0) {
                    targetList = targetList + ',';
                }
                if (populateModuleName == '') {
                    populateModuleName = populateEmailAddress;
                }
                targetList = targetList + populateModuleName + ' <' + populateEmailAddress + '>';
                targetCount++;
            });
            if (targetCount > 0) {
                $(self.emailComposeView).find('#to_addrs_names').val(targetList);
                if (targetCount < 2) {
                    $(self.emailComposeView).find('#parent_type').val(populateModule);
                    $(self.emailComposeView).find('#parent_name').val(populateModuleName);
                    $(self.emailComposeView).find('#parent_id').val(populateModuleRecord);
                }
            }
            $(self.emailComposeView).on('sentEmail', function(event, composeView) {
                composeBox.hide();
                composeBox.remove();
            });
            $(self.emailComposeView).on('disregardDraft', function(event, composeView) {
                if (typeof messageBox !== "undefined") {
                    var mb = messageBox({
                        size: 'lg'
                    });
                    mb.setTitle(SUGAR.language.translate('', 'LBL_CONFIRM_DISREGARD_DRAFT_TITLE'));
                    mb.setBody(SUGAR.language.translate('', 'LBL_CONFIRM_DISREGARD_DRAFT_BODY'));
                    mb.on('ok', function() {
                        mb.remove();
                        composeBox.hide();
                        composeBox.remove();
                    });
                    mb.on('cancel', function() {
                        mb.remove();
                    });
                    mb.show();
                } else {
                    if (confirm(self.translatedErrorMessage)) {
                        composeBox.hide();
                        composeBox.remove();
                    }
                }
            });
            composeBox.on('cancel', function() {
                composeBox.remove();
            });
            composeBox.on('hide.bs.modal', function() {
                composeBox.remove();
            });
        }).fail(function(data) {
            composeBox.controls.modal.content.html(SUGAR.language.translate('', 'LBL_EMAIL_ERROR_GENERAL_TITLE'));
        });
        return $(self);
    }, 300);
}

function saveActivityFromModal(theForm)
{
	
	if(theForm != "form_SubpanelQuickCreate_Documents" && theForm != "form_SubpanelQuickCreate_Notes"){
	var Call = document.getElementById('formformCalls');
    var _form = document.getElementById(theForm);
     disableOnUnloadEditView();
      _form.action.value='Save';
	  console.log($('#' + theForm).serialize());
      if(check_form(theForm)) {
          $('#activityCreationModal').find('.spinner').show();
          setTimeout(function () {
              $.ajax({
                  type: "POST",
                  url: "index.php",
                  data: $('#' + theForm).serialize(),
                  success: function(res) {
                      $('#activityCreationModal').modal('hide');
					  if(Call != null || Call != undefined){
						if(theForm == "form_SubpanelQuickCreate_Calls" || theForm == "form_SubpanelQuickCreate_Meetings"){
							showSubPanel_re('history', null, true);
							showSubPanel_re('activities', null, true);  
						}
                        if(theForm == "form_SubpanelQuickCreate_Documents"){
							showSubPanel_re('documents', null, true);
						}
						
						if(theForm == "form_SubpanelQuickCreate_Cases"){
							showSubPanel_re('cases', null, true);
						}
						if(theForm == "form_SubpanelQuickCreate_Opportunities"){
							showSubPanel_re('opportunities', null, true);
						}
						if(theForm == "form_SubpanelQuickCreate_Contacts"){
							showSubPanel_re('contacts', null, true);
						}
						//if(theForm == "form_SubpanelQuickCreate_Contracts"){
						//	showSubPanel_re('account_aos_contracts', null, true);
						//}
						if(theForm == "form_SubpanelQuickCreate_bill_Billing"){
							showSubPanel_re('bill_billing_accounts', null, true);
						}
						
					  }else{
					 
						SUGAR.ajaxUI.showLoadingPanel();
						setTimeout(function () {
						refreshLeadCard2(function (res)
						{
						 SUGAR.ajaxUI.hideLoadingPanel();
						}); 
						}, 300);

					}  
		  
                  },
                  error: function(error) {
                      alert(error.message);
                  }
              });
          }, 300);
		  
      }
	}else{
		var Call = document.getElementById('formformCalls');
		var _form = document.getElementById(theForm);
		disableOnUnloadEditView();
		 _form.action.value='Save';
		 if(check_form(theForm)) {
          $('#activityCreationModal').find('.spinner').show();
		  if(theForm == "form_SubpanelQuickCreate_Documents"){
			SUGAR.subpanelUtils.inlineSave(_form.id, 'Documents_subpanel_save_button');
		  }else{
			SUGAR.subpanelUtils.inlineSave(_form.id, 'Notes_subpanel_save_button');  
		  }
		setTimeout(function(){
		 $('#activityCreationModal').modal('hide');
					  if(Call != null || Call != undefined){
                    showSubPanel_re('history', null, true);
                    showSubPanel_re('activities', null, true);
					showSubPanel_re('documents', null, true);
					showSubPanel_re('cases', null, true);
					showSubPanel_re('opportunities', null, true);
					showSubPanel_re('contacts', null, true);
					showSubPanel_re('account_aos_contracts', null, true);
					showSubPanel_re('account_aos_quotes', null, true);
					showSubPanel_re('account_aos_invoices', null, true);
					showSubPanel_re('bill_billing_accounts', null, true);
					  }else{
					 
						SUGAR.ajaxUI.showLoadingPanel();
						setTimeout(function () {
						refreshLeadCard(function (res) {
						SUGAR.ajaxUI.hideLoadingPanel();
						}); 
						}, 300);

					} 
		
   
		},3000);	
	}	
	}
	
}

function saveOpportunityFromModal(id,theForm)
{
	var Call = document.getElementById('formformCalls');
    var _form = document.getElementById(theForm);
    disableOnUnloadEditView();
    _form.action.value='Save';
    if(check_form(theForm)) {
        $('#activityCreationModal').find('.spinner').show();
        setTimeout(function () {
			
            $.ajax({
                type: "POST",
                url: "index.php",
                data: $('#' + theForm).serialize(),
                success: function(res) {
					//
					if($("#sales_stage").val() == "Closed Won"){
					$.ajax({
				type: "POST",
				url: "index.php?entryPoint=changeSta",
				data: {
					accId:id
				},
                   success: function (res) {
					$('#activityCreationModal').modal('hide');
                   	if(Call != null || Call != undefined){
                    showSubPanel_re('history', null, true);
                    showSubPanel_re('activities', null, true);
					showSubPanel_re('documents', null, true);
					showSubPanel_re('cases', null, true);
					showSubPanel_re('opportunities', null, true);
					showSubPanel_re('contacts', null, true);
					showSubPanel_re('account_aos_contracts', null, true);
					showSubPanel_re('account_aos_quotes', null, true);
					showSubPanel_re('account_aos_invoices', null, true);
					showSubPanel_re('bill_billing_accounts', null, true);
					  }else{
						SUGAR.ajaxUI.showLoadingPanel();
						setTimeout(function () {
						refreshLeadCard(function (res) {
						SUGAR.ajaxUI.hideLoadingPanel();
						});
						}, 300);

					}  
                   }
                   

                })
					}else{
			$('#activityCreationModal').modal('hide');
                   	if(Call != null || Call != undefined){
                    showSubPanel_re('history', null, true);
                    showSubPanel_re('activities', null, true);
					showSubPanel_re('documents', null, true);
					showSubPanel_re('cases', null, true);
					showSubPanel_re('opportunities', null, true);
					showSubPanel_re('contacts', null, true);
					showSubPanel_re('account_aos_contracts', null, true);
					showSubPanel_re('account_aos_quotes', null, true);
					showSubPanel_re('account_aos_invoices', null, true);
					showSubPanel_re('bill_billing_accounts', null, true);
					  }else{
						SUGAR.ajaxUI.showLoadingPanel();
						setTimeout(function () {
						refreshLeadCard(function (res) {
						SUGAR.ajaxUI.hideLoadingPanel();
						});
						}, 300);

					} 
		}	//
					
					
                },
                error: function(error) {
                    alert(error.message);
                }
            });
		
        }, 300);
    }
}

function showSpinner(modal_id) {
    $('#' + modal_id).find('.modal-body').html('<img src="themes/SuiteP/images/loading.gif" width="48" height="48" align="baseline" border="0" alt="">');
    $('#' + modal_id).modal('show');
}

function closeModal(modal_id) {
	if(localStorage.getItem("call") == "1"){
		localStorage.setItem("call","0");
	}		
    $('#' + modal_id).find('.modal-body').html('');
    $('#' + modal_id).modal('hide');
}

function showEditForm(module, field, ele) {
    $('#preview').find('#editForm').show();
    $(ele).hide();
    $(ele).closest('#preview').find('#' + field).hide();
}

function cancelEdit(ele) {
    hideEditForm(ele);
}

function hideEditForm(ele) {
    $(ele).closest('#editForm').hide();
    $(ele).closest('#preview').find('.edit-button').show();
}

function quickSave(id, module, field, ele)
{
    $(ele).closest('#editForm').find('.btn-save,.btn-cancel').hide();
    $(ele).closest('#editForm').find('.spinner').show();
    setTimeout(function () {
        var field_value = $(ele).closest('#editForm').find('#' + field + '_editable').val();
        $.ajax({
            type: "POST",
            url: "index.php?module=Leads&action=MTS_quickSave&sugar_body_only=1",
            data: {
                target_module: module,
                record: id,
                field_name: field,
                field_value: field_value
            },
            dataType: "JSON",
            success: function (res) {
                if(res.is_saved) {
                    $(ele).closest('#editForm').find('#' + field + '_editable').val(res.value);
                    $(ele).closest('#preview').find('#' + field).html(res.value).show();
                    $(ele).closest('#editForm').find('.spinner').hide();
                    $(ele).closest('#editForm').find('.btn-save,.btn-cancel').show();
                    hideEditForm(ele);
                }
                else {
                    alert(res.msg);
                }
            },
            error: function (error) {
                alert(error.message);
            }
        })
    }, 300);
}

function addSpinner(ele)
{
    $('<span class="spinner" style="float: left; display: none;"><img src="themes/SuiteP/images/loading.gif" width="40" height="40" align="baseline" border="0" alt=""></span>').insertAfter($('[name=' + ele + ']'));
}

function closeActivity(id, module, new_status, ele) {

    var mb = messageBox({
        size: 'sm',
        headerContent: '<button type="button" class="close btn-cancel" aria-label="Close"><span aria-hidden="true">×</span></button><h3 class="modal-title text-uppercase text-black"></h3>'
    });
    mb.setTitle(SUGAR.language.get("app_strings", "LBL_CLOSE_ACTIVITY_HEADER"));
    mb.setBody(SUGAR.language.translate('', 'LBL_CONFIRM_CLOSE_ACTIVITY_QUESTION'));
    mb.on('ok', function() {
        mb.remove();
       $(ele).next('.spinner').show();
        $(ele).hide();
        setTimeout(function () {
            var url = "index.php?module=Leads&action=MTS_quickSave&sugar_body_only=1"
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    target_module: module,
                    record: id,
                    field_name: "status",
                    field_value: new_status
                },
                dataType: "JSON",
                success: function() {
                    $(ele).next('.spinner').remove();
                    $(ele).remove();
                    showSubPanel_re('history', null, true);
                    showSubPanel_re('activities', null, true);
					showSubPanel_re('documents', null, true);
					showSubPanel_re('cases', null, true);
					showSubPanel_re('opportunities', null, true);
					showSubPanel_re('contacts', null, true);
					showSubPanel_re('account_aos_contracts', null, true);
					showSubPanel_re('account_aos_quotes', null, true);
					showSubPanel_re('account_aos_invoices', null, true);
					showSubPanel_re('bill_billing_accounts', null, true);
                },
                error: function (error) {
                    $(ele).next('.spinner').remove();
                    mb.setBody(error.message);
                    $(ele).show();
                }
            });
        }, 300);
    });
    mb.on('cancel', function() {
        mb.remove();
    });
    mb.show();
}
function doAction1(target,lead_id, module, name, account_id,status, submitFormId, save_btn_name) {
        showSpinner('activityCreationModal');
		if(status=="Converted" || status=="Transaction"){
        setTimeout(function () {
            $.ajax({
                type: "POST",
                url: "index.php",
                data:{
					target_module:target,
					account_id:lead_id,
					account_name:name,
					to_pdf:true,
					tpl:'QuickCreate.tpl',
					return_module:'Accounts',
					return_action:'SubPanelViewer',
					return_id:lead_id,
					record:null,
					action:'SubpanelCreates',
					module:'Home',
					target_action:'QuickCreate',
					parent_type:module,
					parent_name:name,
					parent_id:account_id
					
				},
                success: function(res)
                {
                    var modal_title = '';
                    var full_form_btn = '';
                    var cancel_btn = '';
                    if(target == 'Calls') {
                        modal_title = lead_mods.LBL_ADD_CALL_MODAL_TITLE;//tạo cuộc gọi
						
                        full_form_btn = 'Calls_subpanel_full_form_button';
                        cancel_btn = 'Calls_subpanel_cancel_button';
                    }
                    else if(target == 'Meetings'){
                        modal_title = lead_mods.LBL_ADD_MEETING_MODAL_TITLE;
                        full_form_btn = 'Meetings_subpanel_full_form_button';
                        cancel_btn = 'Meetings_subpanel_cancel_button';
                    }else if(target == 'Contacts'){
                        modal_title = "new contact";
                        full_form_btn = 'Contacts_subpanel_full_form_button';
                        cancel_btn = 'Contacts_subpanel_cancel_button';
                    }
					else if(target == 'Notes'){
						 modal_title = "new note";
                        full_form_btn = 'Notes_subpanel_full_form_button';
                        cancel_btn = 'Notes_subpanel_cancel_button';
						
						
						
					}
					else if(target == 'bill_Billing'){
						 modal_title = "new bill";
                        full_form_btn = 'bill_Billing_subpanel_full_form_button';
                        cancel_btn = 'bill_Billing_subpanel_cancel_button';
						
						
						
					}
					else{
						
						modal_title = "new tickets";
                        full_form_btn = 'Cases_subpanel_full_form_button';
                        cancel_btn = 'Cases_subpanel_cancel_button';
						
						
						
					}
                    $('#activityCreationModal').find('.modal-body').html(res);
                    $('#activityCreationModal').find('.modal-title').text(modal_title);
                    $('#activityCreationModal').find('[name=' + full_form_btn + ']').remove();
                    $('#activityCreationModal').find('.modal-body').find('[name=' + save_btn_name + ']').attr('onclick', 'saveActivityFromModal("' + submitFormId + '")').attr('type', 'button');
                    $('#activityCreationModal').find('.modal-body').find('[name=' + cancel_btn + ']').attr('onclick', '$("#activityCreationModal").modal("hide");').attr('type', 'button');
                    $('#activityCreationModal').find('.modal-body').find('.panel-content').find('div:first').remove();
                    $('#activityCreationModal').modal('show');
                    addSpinner(cancel_btn);
					
					$(document).ready(function(){
						if(target == 'Notes'){
							$("select option[value='Dead']").css("display","none");
						}	
						if(target == 'bill_Billing'){
							var textEle=document.getElementById("billingamount");
							if(textEle != null && textEle != undefined){
								 var str;
								 var i;
								 var st;
								 var j;
								 var text_val;
								 var cusText;
								 textEle.onkeyup=function(){
								 cusText='';
								 str='';
								 text_val=textEle.value;
								  console.log(text_val);
								 for(var u=0;u<text_val.length;u++)
								 {
								 if(text_val[u]!=",")
								  cusText+=text_val[u];
								 
								 }
								 console.log(cusText);
									
									i=eval(cusText.length-1);
									j=eval(cusText.length-4);
								while(i>-1){
								if(i==j && cusText.length > 3 ){
									j-=3;
								 str=cusText[i]+","+str;
								
								 }else{
								 str=cusText[i]+str;
								 }
								 i--;
								  }
								
								
								 textEle.value=str;
								 }
							}
						}
					
						});
					
                }
            });

        }, 300);
		}
		else{
			
			 setTimeout(function () {
			
            $.ajax({
                type: "POST",
                url: "index.php",
                data:{
					target_module:target,
					lead_id:lead_id,
					lead_name:name,
					to_pdf:true,
					tpl:'QuickCreate.tpl',
					return_module:'Leads',
					return_action:'SubPanelViewer',
					return_id:lead_id,
					record:null,
					action:'SubpanelCreates',
					module:'Home',
					target_action:'QuickCreate',
					parent_type:module,
					parent_name:name,
					parent_id:lead_id
					
					//$('#' + createFormId).serialize()
					
					
					
				},
                success: function(res)
                {
                    var modal_title = '';
                    var full_form_btn = '';
                    var cancel_btn = '';
                    if(target == 'Calls') {
                        modal_title = lead_mods.LBL_ADD_CALL_MODAL_TITLE;//tạo cuộc gọi
						
                        full_form_btn = 'Calls_subpanel_full_form_button';
                        cancel_btn = 'Calls_subpanel_cancel_button';
                    }
					else if(target == 'Notes'){
						 modal_title = "new note";
                        full_form_btn = 'Notes_subpanel_full_form_button';
                        cancel_btn = 'Notes_subpanel_cancel_button';
						
						
						
					}

                    else {
                        modal_title = lead_mods.LBL_ADD_MEETING_MODAL_TITLE;
                        full_form_btn = 'Meetings_subpanel_full_form_button';
                        cancel_btn = 'Meetings_subpanel_cancel_button';
                    }
                    $('#activityCreationModal').find('.modal-body').html(res);
                    $('#activityCreationModal').find('.modal-title').text(modal_title);
                    $('#activityCreationModal').find('[name=' + full_form_btn + ']').remove();
                    $('#activityCreationModal').find('.modal-body').find('[name=' + save_btn_name + ']').attr('onclick', 'saveActivityFromModal("' + submitFormId + '")').attr('type', 'button');
                    $('#activityCreationModal').find('.modal-body').find('[name=' + cancel_btn + ']').attr('onclick', '$("#activityCreationModal").modal("hide");').attr('type', 'button');
                    $('#activityCreationModal').find('.modal-body').find('.panel-content').find('div:first').remove();
                    $('#activityCreationModal').modal('show');
                    addSpinner(cancel_btn);
					if(target == 'Notes'){
					$(document).ready(function(){
							$("select option[value='Unactivate']").css("display","none");
						});
					}
                }
            });
			
        }, 300);
			
		}
       
}

 function checkEditView(){
	if(typeof(total_element) == "undefined")
    var total_element;
if (typeof (total_element) == 'undefined')
        total_element = 0;
		total_element++;
		inside_submit = true;
       var save=1;
        var class_info = $("#phone_alternate").attr('id') + "_dup_detector_info";
        var module_name = 'Accounts';
		var form_name = 'form_DCQuickCreate_' + module_name;
        var field_value = encodeURIComponent($("#phone_alternate").val());
        var field_name = $("#phone_alternate").attr('id');
        var availname = field_value;
		var record_id = $('#' + form_name + ' input[name="record"]').val();
        if (field_value != '') {
            $("#" + class_info).show();
            $("#" + class_info).fadeIn(400).html('<img src="custom/include/SugarFields/Fields/Dupdetector/image/ajax-loading.gif" />');
            var record_id = $('#' + form_name + ' input[name="record"]').val();
            var data = "record=" + record_id + "&module_name=" + module_name + "&field_name=" + field_name + "&field_value=" + field_value;
            SUGAR.ajaxUI.showLoadingPanel();
            $.ajax({
                type: "POST",
                url: "index.php?module=Dupdetector&action=check_dup&to_pdf=1&sugar_body_only=true&record_id=" + record_id,
                data: data,
                cache: false,
                async: true,
                success: function(result) {
                    if (result != '') {
                        res = eval('(' + result + ')');
                        total_element--;
                        if (res.status == '') {
                            $("#" + class_info).html('');
                            if (res.settings == true) {
                                for (validate_dup_key in validate['EditView']) {
                                    if (typeof (validate['EditView'][validate_dup_key][0]) != 'undefined' &&
                                            validate['EditView'][validate_dup_key][0] == 'account_code' && validate['EditView'][validate_dup_key][1] == 'error') {
                                    validate['EditView'].splice(validate_dup_key, 1);
                                    }
                                }
                            }
                        } else {
							save=0;
                            ele_status['account_code'] = res.status;
                            var more_info_msg = '<img border="0"  id ="tooltip_' + field_name + '" onclick="return SUGAR.util.showHelpTips(this,ele_status[\'account_code\']);" src="themes/default/images/helpInline.gif" >';
                            $("#" + class_info).html('<img src="custom/include/SugarFields/Fields/Dupdetector/image/error.gif" /> Khách hàng này đã tồn tại ! ' + more_info_msg);
                            SUGAR.util.evalScript(more_info_msg);
                            if (res.settings == true) {
                                for (validate_dup_key in validate['EditView']) {
                                    if (typeof (validate['EditView'][validate_dup_key][0]) != 'undefined' &&
                                            validate['EditView'][validate_dup_key][0] == 'account_code' && validate['EditView'][validate_dup_key][1] == 'error') {
                                    validate['EditView'].splice(validate_dup_key, 1);
                                    }
                                }
                                addToValidate('EditView', 'account_code', 'error', true, 'Please resolve conflict');
                            }
                        }
                    }
                    SUGAR.ajaxUI.hideLoadingPanel();
                    if (total_element <= 0 && inside_submit == true) {
                        inside_submit = false;
                        $(curSubmit).trigger("click");
                    }
                },
                failure: function() {
                    SUGAR.ajaxUI.hideLoadingPanel();
                }
            });
        } else {
            $("#" + class_info).html('');
            if (inside_submit == true)
                $(curSubmit).trigger("click");
        }
   

	if(save===1){
		
		return true;
	}else{
		
		return false;
		
	}
	
	
}
function toTitleCase(str) {
    return str.replace(/\w\S*/g, function(txt){
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}
function boldString(str, substr) {
  var strRegExp = new RegExp(substr, 'g');
  return str.replace(strRegExp, '<span style="background-color:yellow">'+substr+'</span>');
}
function boldString2(str, substr) {
  var strRegExp = new RegExp(substr.toUpperCase(), 'g');
  return str.replace(strRegExp, '<span style="background-color:yellow">'+substr.toUpperCase()+'</span>');
}
function boldString3(str, substr) {
  var strRegExp = new RegExp(toTitleCase(substr), 'g');
  return  str.replace(strRegExp, '<span style="background-color:yellow">'+toTitleCase(substr)+'</span>');
}
 function Search(event){
	 if(event.which == 13 || event.keyCode == 13){
		event.preventDefault();
		
		var keyword=document.getElementById("keyword");
		var value=keyword.value;
		var tabContent1=document.getElementById("tab_content_1");
		var dropdown=document.getElementById("period1").value;
		var customerStatusBox=document.getElementById('customerStatusBox');
		if(customerStatusBox.checked == false){
		var statusCus=0;
		}else{
		var statusCus=1;		
		}
		// var row=leadCardContainer[0].getElementsByClassName("row");
		 var myCus=document.getElementById('myCustomer');
		var newOpt=document.getElementById('NewOpt');
		var deadOpt=document.getElementById('deadOpt');
		if(newOpt.checked == true){
		var New = true;
	}
		if(deadOpt.checked == true){
		var Dead = true;
		}
		if(myCus.checked == true){
		var Cus = true;
		}
		if(keyword.value != null && keyword.value != undefined && keyword.value != '' ){
			SUGAR.ajaxUI.showLoadingPanel();
		 $.ajax({
					type: "POST",
					url: "index.php?entryPoint=retrieve_dash_page",
					data: {
						'key':keyword.value,
						'page_id' : 1
					},
					dataType: "html",
					success: function(data) {
						var value1=value;
						var value2=value.toUpperCase();
						var value3=toTitleCase(value);
					   $('#tab_content_1').html(data);
					   $(document).ready(function(){
						document.querySelectorAll("b").forEach(function(element){
							if(element.textContent.includes(value1) == true || element.textContent.includes(value2) == true || element.textContent.includes(value3) == true ){
								if(element.textContent.includes(value1) == true){
								element.innerHTML = boldString(element.textContent, value1);
								}
								if(element.textContent.includes(value2) == true){
								element.innerHTML = boldString2(element.textContent, value1);
								}
								if(element.textContent.includes(value3) == true){
								element.innerHTML = boldString3(element.textContent, value1);
								}
							}
						});   
						document.querySelectorAll("p").forEach(function(element){
							if(element.innerHTML.includes(value1) == true || element.innerHTML.includes(value2) == true || element.innerHTML.includes(value3) == true ){
								if(element.innerHTML.includes(value1) == true){
								element.innerHTML = boldString(element.innerHTML, value1);
								}
								if(element.innerHTML.includes(value2) == true){
								element.innerHTML = boldString2(element.innerHTML, value1);
								}
								if(element.innerHTML.includes(value3) == true){
								element.innerHTML = boldString3(element.innerHTML, value1);
								}
							}
						});     
						keyword=document.getElementById("keyword");
						keyword.value=value;
						 $("#period1").val(dropdown).change();
						 $("#showPeriod").html("("+$("#period option:selected").text()+")");
						  customerStatusBox=document.getElementById('customerStatusBox');
						if(statusCus == 0){
							customerStatusBox.checked=false;
							$('#customerStatus').html("( KH không được chăm sóc )");
							document.getElementById("30days").style.display='none';
						}else{
							
							customerStatusBox.checked=true;	
							$('#customerStatus').html("( KH được chăm sóc )");
							document.getElementById("30days").style.display='block';						
						}
						if(New == true){
							document.getElementById('NewOpt').checked = true;
							
						}
						if(Dead == true){
							document.getElementById('deadOpt').checked =true;
						}
						if(Cus == true){
							document.getElementById('myCustomer').checked =true;
						}
						});
					  SUGAR.ajaxUI.hideLoadingPanel(); 
					},
					error: function (error) {
						
					}
				});
		}else{
			SUGAR.ajaxUI.showLoadingPanel();
			$.ajax({
					type: "POST",
					url: "index.php?entryPoint=retrieve_dash_page",
					data: {
						
						'page_id' : 1
					},
					dataType: "html",
					success: function(data) {
						 $('#tab_content_1').html(data);
						 $(document).ready(function(){
					  if(New == true){
							document.getElementById('NewOpt').checked = true;
							
						}
						if(Dead == true){
							document.getElementById('deadOpt').checked =true;
						}
						if(Cus == true){
							document.getElementById('myCustomer').checked =true;
						}
						 });
					   SUGAR.ajaxUI.hideLoadingPanel(); 
					},
					error: function (error) {
						
					}
				});
			
			
			
		}
	 }
	 
 }
var dropdown_default="default";
function refreshPage(){
	
	 SUGAR.ajaxUI.showLoadingPanel();
	 var tabContent1=document.getElementById("tab_content_1");
	$.ajax({
                type: "POST",
                url: "index.php?entryPoint=retrieve_dash_page",
                data: {
                    'clear':2,
					'page_id' : 1
                },
                dataType: "html",
                success: function(data) {
					console.log(data);
                   
				    $('#tab_content_1').html(data);
				 
				    $(document).ready(function(){
					 $("#period1").val("default").change();
					 $("#showPeriod").html("("+$("#period option[value='this_month']").text()+")");
					 dropdown_default="default";
				
					});
				   SUGAR.ajaxUI.hideLoadingPanel();
                },
                error: function (error) {
                    
                }
            });
			
}

function FilterTime(name){
	
	var keyword=document.getElementById("keyword");
	var value=keyword.value;
	var statusCustomer=$('#customerStatus').html();
	var customerStatusBox=document.getElementById('customerStatusBox');
	if(customerStatusBox.checked == false){
	var statusCus=0;
	}else{
	var statusCus=1;		
	}
	if(name!="default" && name!=dropdown_default){
	 var period=document.getElementById("period1");
	 var tabContent1=document.getElementById("tab_content_1");
	
	if(period.value != null && period.value != undefined && period.value != '' ){
		SUGAR.ajaxUI.showLoadingPanel();
	 $.ajax({
                type: "POST",
                url: "index.php?entryPoint=retrieve_dash_page",
                data: {
                    'period1':period.value,
					'page_id' : 1,
					'statusCus':statusCus
                },
                dataType: "html",
                success: function(data) {
					
                  
				   $('#tab_content_1').html(data);
				  
				   $(document).ready(function(){
					
					 $("#period1").val(name).change();
					 $("#showPeriod").html("("+$("#period option:selected").text()+")");
					  dropdown_default=name;
					  customerStatusBox=document.getElementById('customerStatusBox');
					if(statusCus == 0){
						customerStatusBox.checked=false;
						$('#customerStatus').html("( KH không được chăm sóc )");
						document.getElementById("30days").style.display='none';
					}else{
						
						customerStatusBox.checked=true;	
						$('#customerStatus').html("( KH được chăm sóc )");	
						document.getElementById("30days").style.display='block';						
					}
					keyword=document.getElementById("keyword");
					keyword.value=value;
					});
				   SUGAR.ajaxUI.hideLoadingPanel();
                },
                error: function (error) {
                    
                }
            });
	}
	
	}else{
		if(name=="default" && name!=dropdown_default){
			SUGAR.ajaxUI.showLoadingPanel();
			var tabContent1=document.getElementById("tab_content_1");
			$.ajax({
                type: "POST",
                url: "index.php?entryPoint=retrieve_dash_page",
                data: {
                    'clear':1,
					'page_id' : 1
                },
                dataType: "html",
                success: function(data) {
					 $('#tab_content_1').html(data);
				   
				    $(document).ready(function(){
					$("#period1").val("default").change();
					$("#showPeriod").html("("+$("#period option:selected").text()+")");
					dropdown_default="default";
					keyword=document.getElementById("keyword");
					keyword.value=value;
					});
				    SUGAR.ajaxUI.hideLoadingPanel();
                },
                error: function (error) {
                    
                }
            });
		}		
		
	}
	
}
function refreshFilter(){
	$("#group-users option:selected").removeAttr("selected");
	$("#users option:selected").removeAttr("selected");
	$('#period option[value="default"]').attr("selected", "selected");
	}

function changeCustomerStatus(){
	var keyword=document.getElementById("keyword");
	var value=keyword.value;
	var statusCustomer=$('#customerStatus').html();
	var customerStatusBox=document.getElementById('customerStatusBox');
	if(customerStatusBox.checked==false){
	var statusCus=0;
	$('#customerStatus').html("( KH không được chăm sóc )");
	document.getElementById("30days").style.display='none';
	}else{
	var statusCus=1;	
	$('#customerStatus').html("( KH được chăm sóc )");
	document.getElementById("30days").style.display='block';
	}
	var name=$('#period1').val();
	if(name!="default" && name!="default"){
	 var period=document.getElementById("period1");
	 var tabContent1=document.getElementById("tab_content_1");
	
	if(period.value != null && period.value != undefined && period.value != '' ){
		SUGAR.ajaxUI.showLoadingPanel();
	 $.ajax({
                type: "POST",
                url: "index.php?entryPoint=retrieve_dash_page",
                data: {
                    'period1':period.value,
					'page_id' : 1,
					'statusCus':statusCus
                },
                dataType: "html",
                success: function(data) {
					
                  
				   $('#tab_content_1').html(data);
				  
				   $(document).ready(function(){
					
					 $("#period1").val(name).change();
					 $("#showPeriod").html("("+$("#period option:selected").text()+")");
					  dropdown_default=name;
					  customerStatusBox=document.getElementById('customerStatusBox');
					if(statusCus==0){
						customerStatusBox.checked=false;
						$('#customerStatus').html("( KH không được chăm sóc )");
						document.getElementById("30days").style.display='none';
					}else{
						customerStatusBox.checked=true;	
						$('#customerStatus').html("( KH được chăm sóc )");	
						document.getElementById("30days").style.display='block';						
					}
					keyword=document.getElementById("keyword");
					keyword.value=value;
					});
				    SUGAR.ajaxUI.hideLoadingPanel();
                },
                error: function (error) {
                    
                }
            });
	}
	
	}else{
		if(name=="default" && name!=dropdown_default){
			SUGAR.ajaxUI.showLoadingPanel();
			var tabContent1=document.getElementById("tab_content_1");
			$.ajax({
                type: "POST",
                url: "index.php?entryPoint=retrieve_dash_page",
                data: {
                    'clear':1,
					'page_id' : 1
                },
                dataType: "html",
                success: function(data) {
					 $('#tab_content_1').html(data);
				   
				    $(document).ready(function(){
					$("#period1").val("default").change();
					$("#showPeriod").html("("+$("#period option:selected").text()+")");
					dropdown_default="default";
					keyword=document.getElementById("keyword");
					keyword.value=value;
					});
				   SUGAR.ajaxUI.hideLoadingPanel();
                },
                error: function (error) {
                    
                }
            });
		}	
	}
}
function loadGroup(){
	var groupId=document.getElementById("provincecode_c").value;
	
	$.ajax({
                type: "POST",
                url: "index.php?entryPoint=getGroup",
                data: {
                    id:groupId
                },
                dataType: "json",
                success: function(data) {
					var obj=data;
					html="";
					html+='<input style="width:130px;" type="text" placeholder="Search.." id="myInput" onkeyup="filterKey()">'
					html+='<select style="width:130px;height:180px" name="group-users[]" id="group-users" multiple="multiple" >';
					//html+='<option onclick="getUser()" value="All">All</option>';
					if(obj != null){
					for(let i=0;i<obj.length;i++){
						html+='<option onclick="getUser()" value="'+obj[i].id+'">'+obj[i].name+'</option>';
					}
                    html+='</select>';
					$('#groupUser').html(html);
					$('#users').html('');
					}
					
                },
                error: function (error) {
                    
                }
            });
}

function getUser(){
	var province=document.getElementById("provincecode_c").value;
	var groups=$('#group-users').val();
	
	$.ajax({
                type: "POST",
                url: "index.php?entryPoint=getUser",
                data: {
                    id:groups,
					province:province
                },
                dataType: "json",
                success: function(data) {
					var obj=data;
					html="";
					for(let i=0;i<obj.length;i++){
						html+='<option value="'+obj[i].id+'">'+obj[i].name+(obj[i].title?'-'+obj[i].title:'')+'</option>';
					}
                    
					$('#users').html(html);
					
                },
                error: function (error) {
                    
                }
            });
			
}
function showProvince(){
	$.ajax({
                type: "POST",
                url: "index.php?entryPoint=showProvince",
                dataType: "json",
                success: function(data) {
					for(let i=0;i<data.length;i++){
						document.getElementById(data[i]).style.display="block";
					}
                },
                error: function (error) {
                    
                }
            });
}
function clicktocall2(){
	$.ajax({
		type:'post',
		url:"index.php?entryPoint=checkSession",
		success:function(response) {
			if(response.length < 50 && response != null && response != undefined && response != ''){
				if($("#keyword").val() != null && $("#keyword").val() != ''){
				var phoneNumber = $("#keyword").val();
				sec=0;
				hr=0;
				min=0;
				$("#endCall").attr('onclick','handleButtonHangupClick()');
				 document.getElementById('input-phone').value = phoneNumber;
				//if(user != null && user != undefined && user != ''){
				//permission=getCookie("permission");
				//setCookie("hangup","No");
				//if(permission == 1 || permission == null || permission == undefined){
					//var top1 = window.innerHeight-height;
					//var left1 = window.innerWidth-width;
					//setCookie("permission",2);
					//window.open("https://demo.crmonline.vn/webrtc2",'abc', 'location=no,height=10,width=10,scrollbars=no,toolbar=no,menubar=no,status=no,top='+window.innerHeight+',left='+window.innerWidth);
					handleButtonCallClick();
					setCookie('phone',phoneNumber);
					setCookie("alreadyCall","");
					var html='';
						html+='<a type="button" id="quick_create_meeting" title="Quick Create Meeting" class="btn btn-primary action-btn1" ><i class="fa fa-users" aria-hidden="true"></i></a>';
						html+='<a type="button" id="quick_create_opportunities" title ="Quick Create Opportunities" class="btn btn-primary action-btn2"><i class="fa fa-usd" ></i></a>';
						html+='<a type="button" id="quick_create_case" title ="Quick Create Case" class="btn btn-primary action-btn2"><i class="fa fa-folder" aria-hidden="true"></i></a>';
						html+='<a type="button" style="width: auto" id="save_popup" title ="Lưu" class="btn btn-primary">Lưu thông tin</a>';
						html+='<a type="button" id="eCall" onclick="handleButtonHangupClick2()" style="width: auto;margin-right:3px;" id="Decline" title ="Lưu" class="btn btn-primary">Kết thúc</a>'
						html+='<a type="button" id="callTransfer" onclick="handleButtonTransferClick()" style="width: auto;display:none"  title ="chuyển tiếp" class="btn btn-primary">Chuyển tiếp</a>';
					
					
					var type1=document.getElementsByName("parent_type")[0];
					if(type1 !=null && type1 != undefined){
						type2=type1.value;
					}else{
						type2=GetURLParameter("module");
					}
					$("a:not('.utilsLink')").attr("target","_blank");
					$(".panel-body a").attr("target","_self");
					$(".panel-body span").attr("target","_self");
					$("span").attr("target","_blank");
					$('#detailModal').find('.modal-body').html('');
					$('#detailModal').modal('hide');
					stop=0;
					//us_id=id;
				document.getElementById("time_count").innerHTML='<span id="hrs">00</span>:' + '<span id="mins">00</span>:' + '<span id="sec">00</span>';
				$("#direction").html("Cuộc gọi đi");
				$("#statuscall").html("");
				var label=document.getElementById("with-label");
				var accountLabel=label.getAttribute('title');
				var user = document.getElementById("userextension").value;
				setCookie("phoneNum",phoneNumber);
				$("#actionButton").html('');	
					$("#show").html("Đang gọi "+phoneNumber);
					$("#phoneForm").css('display', 'block');	
					$("#myForm").css("display", "block");
					$("#actionButton").html(html);
					$("#actionButton").css("display", "block");
					$("#phone-mobile-popup").html(phoneNumber);
					$("#userextension-popup").html('<span class="glyphicon glyphicon-user"></span>'+accountLabel);
					$("#tenkh").html("Không xác định");	
					
				}
			}
		}
	});
	
}
function showDead(){
	begin = 0;
	var valueStatus = document.getElementById("myAct").value;
	var myCus=document.getElementById('myCustomer');
	var deadOpt=document.getElementById('deadOpt');
	if(myCus.checked == true){
		var Cus = true;
	}
	SUGAR.ajaxUI.showLoadingPanel();
	if(deadOpt.checked == true){
		$.ajax({
                type: "POST",
                url: "index.php?entryPoint=retrieve_dash_page",
                data: {
					'page_id' : 1,
					'getOnlyDead': 1
                },
               
                success: function(data) {
					 $('#tab_content_1').html(data);
					  $(document).ready(function(){
						  var abf = setInterval(function(){
							if(document.getElementById("myAct")){
								document.getElementById("myAct").value = valueStatus;
								clearInterval(abf);
							}
						},1000);
						document.getElementById('deadOpt').checked = true;
						if(Cus == true){
							document.getElementById('myCustomer').checked = true;
							
						}
					  });
					  SUGAR.ajaxUI.hideLoadingPanel();
                },
                error: function (error) {
                    
                }
            });
	}else{
		$.ajax({
                type: "POST",
                url: "index.php?entryPoint=retrieve_dash_page",
                data: {
					'page_id' : 1,
					'clear': 3
                },
               
                success: function(data) {
					 $('#tab_content_1').html(data);
					 var abf = setInterval(function(){
							if(document.getElementById("myAct")){
								document.getElementById("myAct").value = valueStatus;
								clearInterval(abf);
							}
						},1000);
					 if(Cus == true){
							document.getElementById('myCustomer').checked = true;
							
						}
					 SUGAR.ajaxUI.hideLoadingPanel();
                },
                error: function (error) {
                    
                }
            });
		
	}
	
}
function showNew(){
	begin = 0;
	var valueStatus = document.getElementById('myAct').value;
	var myCus=document.getElementById('myCustomer');
	var newOpt=document.getElementById('NewOpt');
	SUGAR.ajaxUI.showLoadingPanel();
	if(myCus.checked == true){
		var Cus = true;
	}
	if(newOpt.checked == true){
		
		$.ajax({
                type: "POST",
                url: "index.php?entryPoint=retrieve_dash_page",
                data: {
					'page_id' : 1,
					'getOnlyNew': 1
                },
               
                success: function(data) {
					 $('#tab_content_1').html(data);
					  $(document).ready(function(){
						  var abf = setInterval(function(){
							if(document.getElementById("myAct")){
								document.getElementById("myAct").value = valueStatus;
								clearInterval(abf);
							}
						},1000);
						document.getElementById('NewOpt').checked = true;
						if(Cus == true){
							document.getElementById('myCustomer').checked = true;
							
						}
					  });
					  SUGAR.ajaxUI.hideLoadingPanel();
                },
                error: function (error) {
                    
                }
            });
				
	}else{
		$.ajax({
                type: "POST",
                url: "index.php?entryPoint=retrieve_dash_page",
                data: {
					'page_id' : 1,
					'clear': 3
                },
               
                success: function(data) {
					 $('#tab_content_1').html(data);
					 var abf = setInterval(function(){
							if(document.getElementById("myAct")){
								document.getElementById("myAct").value = valueStatus;
								clearInterval(abf);
							}
						},1000);
					 if(Cus == true){
							document.getElementById('myCustomer').checked = true;
							
						}
					 SUGAR.ajaxUI.hideLoadingPanel();
                },
                error: function (error) {
                    
                }
            });
		
	}
	
}
function showMyCustomer(){
	var valueStatus = document.getElementById("myAct").value;
	var myCus=document.getElementById('myCustomer');
	var newOpt=document.getElementById('NewOpt');
	var deadOpt=document.getElementById('deadOpt');
	if(newOpt.checked == true){
		var New = true;
	}
	if(deadOpt.checked == true){
		var Dead = true;
	}
	SUGAR.ajaxUI.showLoadingPanel();
	if(myCus.checked == true){
	$.ajax({
                type: "POST",
                url: "index.php?entryPoint=retrieve_dash_page",
                data: {
					'page_id' : 1,
					'myCustomer': 1
                },
               
                success: function(data) {
					 $('#tab_content_1').html(data);
					 $(document).ready(function(){
						 var abf = setInterval(function(){
							if(document.getElementById("myAct")){
								document.getElementById("myAct").value = valueStatus;
								clearInterval(abf);
							}
						},1000);
						document.getElementById('myCustomer').checked = true;
						if(New == true){
							document.getElementById('NewOpt').checked = true;
							
						}
						if(Dead == true){
							document.getElementById('deadOpt').checked =true;
						}
					  });
					 SUGAR.ajaxUI.hideLoadingPanel();
                },
                error: function (error) {
                    
                }
            });
	}else{
		$.ajax({
                type: "POST",
                url: "index.php?entryPoint=retrieve_dash_page",
                data: {
					'page_id' : 1,
					'clear': 4
                },
               
                success: function(data) {
					 $('#tab_content_1').html(data);
					 var abf = setInterval(function(){
							if(document.getElementById("myAct")){
								document.getElementById("myAct").value = valueStatus;
								clearInterval(abf);
							}
						},1000);
					 if(New == true){
							document.getElementById('NewOpt').checked = true;
							
						}
						if(Dead == true){
							document.getElementById('deadOpt').checked =true;
						}
					 SUGAR.ajaxUI.hideLoadingPanel();
                },
                error: function (error) {
                    
                }
            });
		
	}
	
}
$(document).ready(function(){
    $('.totalLead').tooltip({
        position: { my: "center top", at: "center top" }
    });
	
});






