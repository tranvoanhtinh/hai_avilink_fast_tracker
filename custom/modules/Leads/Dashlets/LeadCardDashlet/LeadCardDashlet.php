<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * SuiteCRM is an extension to SugarCRM Community Edition developed by Salesagility Ltd.
 * Copyright (C) 2011 - 2014 Salesagility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 ********************************************************************************/


require_once('include/Dashlets/Dashlet.php');
require_once('custom/modules/Leads/Dashlets/LeadCardDashlet/LeadCardDashletHelper.php');


class LeadCardDashlet extends Dashlet
{

    var $width = '100%';
    var $height = '100%';
    var $ss;
    var $configureTpl = 'custom/modules/Leads/Dashlets/LeadCardDashlet/tpls/LeadCardDashletConfigure.tpl';
    var $lc_helper;

    /**
     * Constructor
     *
     * @global string current language
     * @param guid $id id for the current dashlet (assigned from Home module)
     * @param report_id $report_id id of the saved report
     * @param array $def options saved for this dashlet
     */
    function __construct($id = null, $options = null)
    {
        parent::__construct($id);
        $this->isConfigurable = true;
        if(isset($options)) {
            if(!empty($options['filters'])) $this->filters = $options['filters'];
            if(!empty($options['title'])) $this->title = $options['title'];
            if(!empty($options['displayRows'])) $this->displayRows = $options['displayRows'];
            if(!empty($options['displayColumns'])) $this->displayColumns = $options['displayColumns'];
            if(isset($options['myItemsOnly'])) $this->myItemsOnly = $options['myItemsOnly'];
            if(isset($options['autoRefresh'])) $this->autoRefresh = $options['autoRefresh'];
        }
        $this->ss = new Sugar_Smarty();
        $this->lc_helper = new LeadCardDashletHelper();

    }



    /**
     * Displays the dashlet
     *
     * @return string html to display dashlet
     */
    function display()
    {

        $parent_content = parent::display();
        $parent_content .= $this->lc_helper->displayDashlet();
        return $parent_content;
    }

    /**
     * Displays the javascript for the dashlet
     *
     * @return string javascript to use with this dashlet
     */
    function displayScript()
    {
        return '';
    }

    function processDisplayOptions() {
        require_once('include/templates/TemplateGroupChooser.php');

        $this->configureSS = new Sugar_Smarty();
        // column chooser
        $chooser = new TemplateGroupChooser();

        $chooser->args['id'] = 'edit_tabs';

        $chooser->args['title'] =  '';
       // $this->configureSS->assign('columnChooser', $chooser->display());


        $this->configureSS->assign('strings', array('general' => $GLOBALS['mod_strings']['LBL_DASHLET_CONFIGURE_GENERAL'],
            'filters' => $GLOBALS['mod_strings']['LBL_DASHLET_CONFIGURE_FILTERS'],
            'myItems' => $GLOBALS['mod_strings']['LBL_DASHLET_CONFIGURE_MY_ITEMS_ONLY'],
            'displayRows' => $GLOBALS['mod_strings']['LBL_DASHLET_CONFIGURE_DISPLAY_ROWS'],
            'title' => $GLOBALS['mod_strings']['LBL_DASHLET_CONFIGURE_TITLE'],
            'save' => $GLOBALS['app_strings']['LBL_SAVE_BUTTON_LABEL'],
            'clear' => $GLOBALS['app_strings']['LBL_CLEAR_BUTTON_LABEL'],
            'autoRefresh' => $GLOBALS['app_strings']['LBL_DASHLET_CONFIGURE_AUTOREFRESH'],
        ));
        $this->configureSS->assign('id', $this->id);
        $this->configureSS->assign('showMyItemsOnly', $this->showMyItemsOnly);
        $this->configureSS->assign('myItemsOnly', $this->myItemsOnly);
        $this->configureSS->assign('searchFields', $this->currentSearchFields);
        $this->configureSS->assign('showClearButton', $this->isConfigPanelClearShown);
        // title
        $this->configureSS->assign('dashletTitle', $this->title);

        // display rows
        $displayRowOptions = $GLOBALS['sugar_config']['dashlet_display_row_options'];
        $this->configureSS->assign('displayRowOptions', $displayRowOptions);
        $this->configureSS->assign('displayRowSelect', $this->displayRows);

        if($this->isAutoRefreshable()) {
            $this->configureSS->assign('isRefreshable', true);
            $this->configureSS->assign('autoRefreshOptions', $this->getAutoRefreshOptions());
            $this->configureSS->assign('autoRefreshSelect', $this->autoRefresh);
        }
    }
    /**
     * Displays the options for this Dashlet
     *
     * @return string HTML that shows options
     */
    function displayOptions() {
        $this->processDisplayOptions();
        return parent::displayOptions() . $this->configureSS->fetch($this->configureTpl);
    }


    /**
     * called to filter out $_REQUEST object when the user submits the configure dropdown
     *
     * @param array $req $_REQUEST
     * @return array filtered options to save
     */
    function saveOptions($req)
    {
        $options = array();

        if(!empty($req['dashletTitle'])) {
            $options['title'] = $req['dashletTitle'];
        }
        $options['displayRows'] = empty($req['displayRows']) ? '10' : $req['displayRows'];

        $options['autoRefresh'] = empty($req['autoRefresh']) ? '0' : $req['autoRefresh'];
        return $options;
    }

    function setConfigureIcon()
    {

        $additionalTitle = '';
        return $additionalTitle;
    }

    function setRefreshIcon()
    {


        $additionalTitle = '';

        return $additionalTitle;
    }
}

