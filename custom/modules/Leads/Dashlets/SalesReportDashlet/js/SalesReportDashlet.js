SUGAR.ajaxUI.showLoadingPanel = function() {
        SUGAR.ajaxUI.loadingPanel = new YAHOO.widget.Panel("ajaxloading",{
            width: "240px",
            fixedcenter: true,
            close: false,
            draggable: false,
            constraintoviewport: false,
            modal: true,
            visible: false
        });
        SUGAR.ajaxUI.loadingPanel.setBody('<div id="loadingPage" align="center" style="vertical-align:middle;"><img src="themes/SuiteP/images/loading.gif" width="40" height="40" align="baseline" border="0" alt=""></div>');
        SUGAR.ajaxUI.loadingPanel.render(document.body);
    if (document.getElementById('ajaxloading_c'))
        document.getElementById('ajaxloading_c').style.display = '';
    SUGAR.ajaxUI.loadingPanel.show();
};
Chart.plugins.register({
    afterDatasetsDraw: function(chart) {
        var ctx = chart.ctx;
        if(chart.config.type == 'pie') {
            chart.data.datasets.forEach(function(dataset, i) {
                var meta = chart.getDatasetMeta(i);
                if (!meta.hidden) {
                    meta.data.forEach(function(element, index) {
                        // Draw the text in black, with the specified font
                        ctx.fillStyle = 'rgb(0, 0, 0)';
                        var fontSize = 16;
                        var fontStyle = 'normal';
                        var fontFamily = 'Helvetica Neue';
                        ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);
                        // Just naively convert to string for now
                        var dataString = dataset.data[index].toString() + '%';
                        if(index == 1) dataString = '';
                        // Make sure alignment settings are correct
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'middle';
                        var padding = 5;
                        var position = element.tooltipPosition();
                        ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding);
                    });
                }
            });
        }
        if(chart.config.type == 'line') {
            chart.data.datasets.forEach(function(dataset, i) {
                var meta = chart.getDatasetMeta(i);
                if (!meta.hidden) {
                    meta.data.forEach(function(element, index) {
                        // Draw the text in black, with the specified font
                        ctx.fillStyle = '#000000';
                        var fontSize = 13;
                        var fontStyle = 'normal';
                        var fontFamily = 'Helvetica Neue';
                        ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);
                        // Just naively convert to string for now
                        var dataString = dataset.data[index].toString();
                        // Make sure alignment settings are correct
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'middle';
                        var padding = 5;
                        var position = element.tooltipPosition();
                        ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding);
                    });
                }
            });
        }
    }
});

SUGAR.util.doWhen(function () {
    return $('.salesReportDashlet').length > 0;
}, function () {
    getNumberOfContractsChart(document.getElementById("numberOfContractsChart").getContext("2d"), number_contracts_data);
    getPercentNumberOfCustomersAreInProcessChart(document.getElementById("percentNumberOfCustomersAreInProcessChart").getContext("2d"), inprocess_lead_data);
    getPercentNumberOfContractChart(document.getElementById("percentNumberOfContractChart").getContext("2d"), converted_lead_data);
    getPercentOfRevenueChart(document.getElementById("percentOfRevenueChart").getContext("2d"), revenue_data);

});

function getNumberOfContractsChart(ctx, rp_data)
{
    var config = {
        type: 'line',
        data: {
            labels: rp_data.months,
            datasets: [{
                label: '',
                backgroundColor: '#FFD800',
                borderColor: '#FFD800',
                pointBackgroundColor: '#000000',
                pointBorderColor: '#000000',
                spanGaps: true,
                data: rp_data.number_contracts,
                fill: false,
            }]
        },
        options: {
            responsive: true,
            title: {
                display: false,
                text: ''
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: rp_data.lblx
                    },
                    ticks: {
                        autoSkip: true
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: rp_data.lbly
                    }
                }]
            },
            legend: {
                display: false
            }
        }
    };

    // Draw index data on line
    var chart = new Chart(ctx, config);
}

function getPercentNumberOfCustomersAreInProcessChart(ctx, rp_data)
{

    var config = {
        type: 'pie',
        data: {
            datasets: [{
                data: rp_data.percent,
                backgroundColor: rp_data.backgroundColor,
            }],
            labels: rp_data.labels
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            legend: {
                display: false
            },
            tooltips: {
                enabled: false,
            }
        }
    };


    var percentNumberOfCustomersAreInProcessChart = new Chart(ctx, config);
}

function getPercentNumberOfContractChart(ctx, rp_data)
{
    var config = {
        type: 'pie',
        data: {
            datasets: [{
                data: rp_data.percent,
                backgroundColor: rp_data.backgroundColor,
            }],
            labels: rp_data.labels
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            legend: {
                display: false
            },
            tooltips: {
                enabled: false,
            }
        }
    };
    var percentNumberOfContractChart = new Chart(ctx, config);
}

function getPercentOfRevenueChart(ctx, rp_data)
{
    var config = {
        type: 'pie',
        data: {
            datasets: [{
                data: rp_data.percent,
                backgroundColor: rp_data.backgroundColor,
            }],
            labels: rp_data.labels
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            legend: {
                display: false
            },
            tooltips: {
                enabled: false,
            }
        }
    };

    var chart = new Chart(ctx, config);
    // Define a plugin to provide data labels
}

function openReportFilter() {
    $('#filterReportModal').modal('show');
}

function convertSugarDateFormatToDatePickerFormat() {
    var date_format = 'dd/mm/yy';
    if (typeof cal_date_format != "undefined") {

        var re = new RegExp('%', 'g');
        date_format = cal_date_format;
        date_format = date_format.replace(re, '').replace('m', 'mm').replace('Y', 'yy').replace('d', 'dd');
    }
    return date_format;
}

function showHideDateFieldsOnReport(type) {
    if (typeof type == "undefined") {
        type = 'is_between';
    }
    if (type == 'is_between') {
        $('#date_range').show();
    }
    else {
        $('#date_range').hide();
    }
}

function periodChangeOnReport() {
    showHideDateFields($('#period').val());
}

function clickToFilter2(evt, ele) {
	
	var province=document.getElementById("provincecode_c").value;
	var period = $('#period2').val();
	var nameTag;
    if (validateReportFilterForm()) {
		if($('#users').val() != null){
			nameTag = '';
			for(let i = 0; i<$('#users').val().length; i++){
				if(i > 0){
				nameTag += ","+$('#users option[value="' + $('#users').val()[i] + '"]').text();
				}else{
				nameTag += $('#users option[value="' + $('#users').val()[i] + '"]').text();
				}
			}
		}else 
		{
			if($('#group-users').val() != null && $('#group-users').val() != ''){
				nameTag = '';
				for(let i = 0; i<$('#group-users').val().length; i++){
					if(i > 0){
					nameTag += ","+$('#group-users option[value="' + $('#group-users').val()[i] + '"]').text();
					}else{
					nameTag += $('#group-users option[value="' + $('#group-users').val()[i] + '"]').text();
					}
				}
			
			}else{
				nameTag = '';
				if(province == "All"){
					nameTag = province;
				}else{
					
						nameTag += $('#provincecode_c option[value="' + $('#provincecode_c').val() + '"]').text();
				}
				
			}
		}
        $('#filterReportModal').modal('hide');
       SUGAR.ajaxUI.showLoadingPanel();
        setTimeout(function() {
			
            $.ajax({
                type: "POST",
                url: "index.php?module=Leads&action=MTS_reportFilter&sugar_body_only=1",
                dataType: "html",
                data: {
					'page_id' : 4,
					"province2": province,
                    "date_from2": $('#date_from2').val(),
                    "date_to2": $('#date_to2').val(),
                    "period2": $('#period2').val(),
					"groups2":$('#group-users').val(),
					"users2":$('#users').val()
                },
                success: function (dashlet) {
                   $(ele).closest('.bd-center').html(dashlet);
				   $(document).ready(function(){
					   document.getElementById("period2").value = period;
						$("#showPeriod2").html("("+$("#period2 option:selected").text()+")-"+nameTag);
						showHideDateFields2(period);
					});
                    SUGAR.ajaxUI.hideLoadingPanel();
                },
                error: function (error) {
                    console.log(error);
                    //SUGAR.ajaxUI.hideLoadingPanel();
                }
            });
            return true;
        }, 300);
    }
}
function showHideDateFields2(type) {
    if (typeof type == "undefined") {
        type = 'is_between';
    }
    if (type == 'is_between') {
       $( "#date_to2, #date_from2").datepicker();
        $('.ui-datepicker-trigger').html('<span class="suitepicon suitepicon-module-calendar"></span>').addClass('button');
    }
     $.ajax({
                type: "POST",
                url: "index.php?entryPoint=getDate",
                data: {
                    time:type
                },
                dataType: "json",
                success: function(data) {
				$("#date_from2").val(data.from);
				$("#date_to2").val(data.to);
										
                },
                error: function (error) {
                    
                }
            });
}

function periodChange2(){
    showHideDateFields2($('#period2').val());
}
function showProvince(){
	$.ajax({
                type: "POST",
                url: "index.php?entryPoint=showProvince",
                dataType: "json",
                success: function(data) {
					if(data != null && data != undefined){
						for(let i=0;i<data.length;i++){
							
							document.getElementById(data[i]).style.display="block";
						}
					}
                },
                error: function (error) {
                    
                }
            });
}
function loadGroup(){
	var groupId=document.getElementById("provincecode_c").value;
	
	$.ajax({
                type: "POST",
                url: "index.php?entryPoint=getGroup",
                data: {
                    id:groupId
                },
                dataType: "json",
                success: function(data) {
					var obj=data;
					html="";
					html+='<input style="width:130px;" type="text" placeholder="Search.." id="myInput" onkeyup="filterKey()">'
					html+='<select style="width:130px;height:180px" name="group-users[]" id="group-users" multiple="multiple" >';
					//html+='<option onclick="getUser()" value="All">All</option>';
					for(let i=0;i<obj.length;i++){
						html+='<option onclick="getUser()" value="'+obj[i].id+'">'+obj[i].name+'</option>';
					}
                    html+='</select>';
					$('#groupUser').html(html);
					$('#users').html('');
					
                },
                error: function (error) {
                    
                }
            });
}
function validateReportFilterForm() {
    $('#filterModal').find('.error').hide();
    var is_valid = true;
    if ($('#period').val() == 'is_between') {
        var fields = ["date_from", "date_to"];
        for (var i = 0; i < fields.length; i++) {
            var field = $('#' + fields[i]);
            if (field.val() == '' || typeof field.val() == "undefined") {
                $(field).closest('.row').find('.error').show();
                is_valid = false;
            }
        }
    }
    return is_valid;
}

