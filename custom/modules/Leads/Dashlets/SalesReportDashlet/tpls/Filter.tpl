<div id="filterReportModal" class="modal fade" role="dialog" data-backdrop="false" data-dismiss="modal">
    <div class="modal-dialog modal-small">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header formHeader">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title text-uppercase text-black">{$RP_MOD.LBL_FILTER_LABEL}</h3>
            </div>
            <div class="modal-body text-center">
                <div class="row" style="margin-bottom: 5px">
                            
							<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left" >
                                <select style="width:140px" name="period2" id="period2" onchange="periodChange2()" >
								<option value="default">------</option>
                                    {$search_date_range_options}
                                </select>
								 <div id="date_range" style="margin-top:15px" >
                    <div class="row" style="margin-bottom: 5px">
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left">
                            <label>{if $RP_MOD.LBL_DATE_FROM}{$RP_MOD.LBL_DATE_FROM}{/if}</label>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 text-left">
                            <input id="date_from2" readonly name="date_from2" type="text" size="19" maxlength="100" value="{$filter_params.from}" title="" tabindex="-1" accesskey="9">
                            <br><span class="error" style="display: none;">{$cardMod.LBL_PLEASE_SELECT_A_DATE}</span>
                        </div>
                    </div>
                    <div class="row" style="margin-bottom: 5px">
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left">
                            <label>{if $RP_MOD.LBL_DATE_TO}{$RP_MOD.LBL_DATE_TO}{/if}</label>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 text-left">
                            <input type="text" size="19" readonly id="date_to2" name="date_to2" maxlength="100" value="{$filter_params.to}" title="" tabindex="-1" accesskey="9">
                            <br><span class="error" style="display: none;">{$cardMod.LBL_PLEASE_SELECT_A_DATE}</span>
                        </div>
                    </div>
                </div>
                            </div>
							<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 " >
                            <select style="width:130px" name="provincecode_c" id="provincecode_c" title="" onchange="loadGroup()" >
<option label="" value="" selected="selected">-----</option>							
<option label="" value="All">All</option>
<option style="display:none" label=" TP Hà Nội" value="10" id="10"> TP Hà Nội</option>
																		<option style="display:none" label=" Tỉnh Hưng Yên" value="16" id="16"> Tỉnh Hưng Yên</option>
																		<option style="display:none" label=" Tỉnh Hải Dương" value="17" id="17"> Tỉnh Hải Dương</option>
																		<option style="display:none" label=" TP Hải Phòng " value="18" id="18"> TP Hải Phòng </option>
																		<option style="display:none" label=" Tỉnh Quảng Ninh " value="20" id="20"> Tỉnh Quảng Ninh </option>
																		<option style="display:none" label=" Tỉnh Bắc Ninh " value="22" id="22"> Tỉnh Bắc Ninh </option>
																		<option style="display:none" label=" Tỉnh Bắc Giang " value="23" id="23"> Tỉnh Bắc Giang </option>
																		<option style="display:none" label=" Tỉnh Lạng Sơn " value="24" id="24"> Tỉnh Lạng Sơn </option>
																		<option style="display:none" label=" Tỉnh Thái Nguyên " value="25" id="25"> Tỉnh Thái Nguyên </option>
																		<option style="display:none" label=" Tỉnh Bắc Kạn " value="26" id="26"> Tỉnh Bắc Kạn </option>
																		<option style="display:none" label=" Tỉnh Cao Bằng " value="27" id="27"> Tỉnh Cao Bằng </option>
																		<option style="display:none" label=" Tỉnh Vĩnh Phúc " value="28" id="28"> Tỉnh Vĩnh Phúc </option>
																		<option style="display:none" label=" Tỉnh Phú Thọ " value="29" id="29"> Tỉnh Phú Thọ </option>
																		<option style="display:none" label=" Tỉnh Tuyên Quang " value="30" id="30"> Tỉnh Tuyên Quang </option>
																		<option style="display:none" label=" Tỉnh Hà Giang " value="31" id="31"> Tỉnh Hà Giang </option>
																		<option style="display:none" label=" Tỉnh Yên Bái " value="32" id="32"> Tỉnh Yên Bái </option>
																		<option style="display:none" label=" Tỉnh Lào Cai " value="33" id="33"> Tỉnh Lào Cai </option>
																		<option style="display:none" label=" Tỉnh Hoà Bình " value="35" id="35"> Tỉnh Hoà Bình </option>
																		<option style="display:none" label=" Tỉnh Sơn La " value="36" id="36"> Tỉnh Sơn La </option>
																		<option style="display:none" label=" Tỉnh Điện Biên " value="38" id="38"> Tỉnh Điện Biên </option>
																		<option style="display:none" label=" Tỉnh Lai Châu " value="39" id="39"> Tỉnh Lai Châu </option>
																		<option style="display:none" label=" Tỉnh Hà Nam " value="40" id="40"> Tỉnh Hà Nam </option>
																		<option style="display:none" label=" Tỉnh Thái Bình " value="41" id="41"> Tỉnh Thái Bình </option>
																		<option style="display:none" label=" Tỉnh Nam Định " value="42" id="42"> Tỉnh Nam Định </option>
																		<option style="display:none" label=" Tỉnh Ninh Bình " value="43" id="43"> Tỉnh Ninh Bình </option>
																		<option style="display:none" label=" Tỉnh Thanh Hoá " value="44" id="44"> Tỉnh Thanh Hoá </option>
																		<option style="display:none" label=" Tỉnh Nghệ An " value="46" id="46"> Tỉnh Nghệ An </option>
																		<option style="display:none" label=" Tỉnh Hà Tĩnh " value="48" id="48"> Tỉnh Hà Tĩnh </option>
																		<option style="display:none" label=" Tỉnh Quảng Bình " value="51" id="51"> Tỉnh Quảng Bình </option>
																		<option style="display:none" label=" Tỉnh Quảng Trị " value="52" id="52"> Tỉnh Quảng Trị </option>
																		<option style="display:none" label=" Tỉnh Thừa Thiên Huế " value="53" id="53"> Tỉnh Thừa Thiên Huế </option>
																		<option style="display:none" label=" TP Đà Nẵng " value="55" id="55"> TP Đà Nẵng </option>
																		<option style="display:none" label=" Tỉnh Quảng Nam " value="56" id="56"> Tỉnh Quảng Nam </option>
																		<option style="display:none" label=" Tỉnh Quảng Ngãi " value="57" id="57"> Tỉnh Quảng Ngãi </option>
																		<option style="display:none" label=" Tỉnh Kon Tum " value="58" id="58"> Tỉnh Kon Tum </option>
																		<option style="display:none" label=" Tỉnh Bình Định " value="59" id="59"> Tỉnh Bình Định </option>
																		<option style="display:none" label=" Tỉnh Gia Lai " value="60" id="60"> Tỉnh Gia Lai </option>
																		<option style="display:none" label=" Tỉnh Phú Yên " value="62" id="62"> Tỉnh Phú Yên </option>
																		<option style="display:none" label=" Tỉnh Đắk Lăk " value="63" id="63"> Tỉnh Đắk Lăk </option>
																		<option style="display:none" label=" Tỉnh Đắk Nông " value="64" id="64"> Tỉnh Đắk Nông </option>
																		<option style="display:none" label=" Tỉnh Khánh Hoà " value="65" id="65"> Tỉnh Khánh Hoà </option>
																		<option style="display:none" label=" Tỉnh Ninh Thuận " value="66" id="66"> Tỉnh Ninh Thuận </option>
																		<option style="display:none" label=" Tỉnh Lâm Đồng " value="67" id="67"> Tỉnh Lâm Đồng </option>
																		<option style="display:none" label=" TP Hồ Chí Minh " value="70" id="70"> TP Hồ Chí Minh </option>
																		<option style="display:none" label=" Tỉnh Bà Rịa Vũng Tàu " value="79" id="79"> Tỉnh Bà Rịa Vũng Tàu </option>
																		<option style="display:none" label=" Tỉnh Bình Thuận " value="80" id="80"> Tỉnh Bình Thuận </option>
																		<option style="display:none" label=" Tỉnh Đồng Nai " value="81" id="81"> Tỉnh Đồng Nai </option>
																		<option style="display:none" label=" Tỉnh Bình Dương " value="82" id="82"> Tỉnh Bình Dương </option>
																		<option style="display:none" label=" Tỉnh Bình Phước " value="83" id="83"> Tỉnh Bình Phước </option>
																		<option style="display:none" label=" Tỉnh Tây Ninh " value="84" id="84"> Tỉnh Tây Ninh </option>
																		<option style="display:none" label=" Tỉnh Long An " value="85" id="85"> Tỉnh Long An </option>
																		<option style="display:none" label=" Tỉnh Tiền Giang " value="86" id="86"> Tỉnh Tiền Giang </option>
																		<option style="display:none" label=" Tỉnh Đồng Tháp " value="87" id="87"> Tỉnh Đồng Tháp </option>
																		<option style="display:none" label=" Tỉnh An Giang " value="88" id="88"> Tỉnh An Giang </option>
																		<option style="display:none" label=" Tỉnh Vĩnh Long " value="89" id="89"> Tỉnh Vĩnh Long </option>
																		<option style="display:none" label=" TP Cần Thơ " value="90" id="90"> TP Cần Thơ </option>
																		<option style="display:none" label=" Tỉnh Hậu Giang " value="91" id="91"> Tỉnh Hậu Giang </option>
																		<option style="display:none" label=" Tỉnh Kiên Giang " value="92" id="92"> Tỉnh Kiên Giang </option>
																		<option style="display:none" label=" Tỉnh Bến Tre " value="93" id="93"> Tỉnh Bến Tre </option>
																		<option style="display:none" label=" Tỉnh Trà Vinh " value="94" id="94"> Tỉnh Trà Vinh </option>
																		<option style="display:none" label=" Tỉnh Sóc Trăng " value="95" id="95"> Tỉnh Sóc Trăng </option>
																		<option style="display:none" label=" Tỉnh Bạc Liêu " value="96" id="96"> Tỉnh Bạc Liêu </option>
																		<option style="display:none" label=" Tỉnh Cà Mau " value="97" id="97"> Tỉnh Cà Mau </option>
</select>   
                            </div>
							
                            <div id="groupUser" class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left ">
							<input style="width:130px;" type="text" placeholder="Search.." id="myInput" onkeyup="filterKey()">
                                <select style="width:130px;height:180px" name="group-users[]" id="group-users[]" multiple="multiple">
                                </select>
                            </div>
							 
							<div id="listUser" class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left">
							<input style="width:130px;" type="text" placeholder="Search.." id="myInput2" onkeyup="filterKey2()">
                                <select style="width:130px;height:180px" name="users[]" id="users" multiple="multiple">
                                </select>
                            </div>
							
                </div>
                <div class="row">
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left">
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-left" style="margin-left:-5px">
                         <button id="filter" class="button" onclick="clickToFilter2(event, this)">{$RP_MOD.LBL_BTN_FILTER}</button>
                    </div>
					<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-left" style="margin-left:5px">
                       <input type="button" onclick="refreshFilter()" class="button" value="Clear">
                    </div>
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left">
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<input type="hidden" value="{$province}" id="province_d">
{literal}
    <script type="text/javascript">
        $.datepicker.setDefaults({
            showOn: "both",
            dateFormat: convertSugarDateFormatToDatePickerFormat(),
        });
 
		showProvince();
		
		var provin=$("#province_d").val();
		document.getElementById("provincecode_c").value = provin ;
		loadGroup();
		showHideDateFields2("this_month");
    </script>
{/literal}