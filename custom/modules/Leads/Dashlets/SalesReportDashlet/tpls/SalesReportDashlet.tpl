
<link rel="stylesheet" type="text/css" href="custom/modules/Leads/Dashlets/SalesReportDashlet/css/SalesReportDashlet.css">
<script>
    {literal}
    var converted_lead_data = {/literal} {$REPORT_DATA.converted_lead.chart_data} {literal};
    var inprocess_lead_data = {/literal} {$REPORT_DATA.inprocess_lead.chart_data} {literal};
    var revenue_data = {/literal} {$REPORT_DATA.revenue.chart_data} {literal};
    var number_contracts_data = {/literal} {$REPORT_DATA.number_contracts} {literal};
    {/literal}
</script>
<div class="salesReportDashlet">
        <div class="row">
            <div class="col-xs-6 col-sm-4 col-md-4 text-left" style="margin-left: 5px; margin-top: 5px;">
                <button onclick="openReportFilter()" class="button"><span class="glyphicon glyphicon-filter"></span></button>
				<span id="showPeriod2">
				</span>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-4 text-left" style="margin-left: 5px; margin-top: 5px; text-align: center;">
                {if $IS_KPI_INCORRECT}
                    <span class="error" id="report_error_msg">{$APP.LBL_KPI_CONFIG_INCORRECT}</span>
                {/if}
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-3 col-md-3">
                <div class="panel panel-default">
                    <div class="panel-heading chart-header">
                        <div class="chart-title text-uppercase text-left">{$APP.LBL_KP_COMMISSION_TITLE}</div>
                    </div>
                    <div class="panel-body" style="height: 290px; border-radius: 5px">
                        <div class="text-center" style="margin-top:20%">
                            <div style="border-bottom:1px black solid;padding-bottom:10px;font-size:30px;color:#ff9a18;font-weight:bold">{$REPORT_DATA.commission}</div>
							
                            <div style="margin-top:10px;font-size:30px;color:red">{$discount}</div>
                        </div>
                    </div>
                </div>
            </div>
			
            <div class="col-xs-12 col-sm-5 col-md-5">
                <div class="panel panel-default" style="border-radius: 5px; position: relative;">
                    <div class="panel-heading chart-header">
                        <div class="chart-title text-uppercase text-left">{$APP.LBL_NUM_CONVERTED_CUSTOMER_REPORT_TITLE}</div>
                    </div>
                    <div class="panel-body" style="padding-top: 100px;">
                        <canvas id="numberOfContractsChart" height="111%"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-2 col-md-2">
                <div class="panel panel-default" style="width: 101%">
                    <div class="panel-heading chart-header" >
                        <div class="chart-title text-uppercase text-left">{$APP.KPI_CUSTOMER}</div>
                    </div>
                    <div class="panel-body" style="height: 290px; border-radius: 5px">
                        <div class="kpiArea">
                            <div class="row" style="height: 18%; width: 100%; padding-top: 15px; padding-left: 25px;">
                                <div class="form-group">
                                    <label style="font-size: 16px;margin-bottom:-5px;font-weight:normal" class="text-black">{$REPORT_DATA.customer_kpi.new_kpi.label}</label> <br/>
                                    <div style="text-align:right"><b class="text-black" style="font-size: 20px">( </b><b style="font-size: 20px; color: #ff9a18">{$REPORT_DATA.customer_kpi.new_kpi.number_new_customer}</b><span> /<span><b class="text-black" style="font-size: 20px">  {$REPORT_DATA.customer_kpi.new_kpi.number_new_in_config} )</b></div>
                                </div>
                            </div>
                            <div class="row" style="height: 18%; width: 100%; padding-top: 15px; padding-left: 25px;">
                                <div class="form-group">
                                    <label style="font-size: 16px;margin-bottom:-5px;font-weight:normal" class="text-black">{$REPORT_DATA.customer_kpi.inprocess_kpi.label}</label> <br/>
                                    <div style="text-align:right"><b class="text-black" style="font-size: 20px">( </b><b style="font-size: 20px; color: #ff9a18">{$REPORT_DATA.customer_kpi.inprocess_kpi.number_inprocess_customer}</b><span> /<span><b style="font-size: 20px" class="text-black">  {$REPORT_DATA.customer_kpi.inprocess_kpi.number_inprocess_in_config} )</b></div>
                                </div>
                            </div>
							<div class="row" style="height: 18%; width: 100%; padding-top: 15px; padding-left: 25px;">
                                <div class="form-group">
                                    <label style="font-size: 16px;margin-bottom:-5px;font-weight:normal" class="text-black">{$REPORT_DATA.customer_kpi.converted_kpi.label}</label> <br/>
                                    <div style="text-align:right"><b class="text-black" style="font-size: 20px">( </b><b style="font-size: 20px; color: #ff9a18">{$REPORT_DATA.customer_kpi.converted_kpi.number_converted_customer}</b><span> /<span><b class="text-black" style="font-size: 20px">  {$REPORT_DATA.customer_kpi.converted_kpi.number_converted_in_config} )</b></div>
                                </div>
                            </div>
                            <div class="row" style="height: 18%; width: 100%; padding-top: 15px; padding-left: 25px;">
                                <div class="form-group">
                                    <label style="font-size: 16px;margin-bottom:-5px;font-weight:normal" class="text-black">{$REPORT_DATA.customer_kpi.transaction_kpi.label}</label> <br/>
                                    <div style="text-align:right"><b class="text-black" style="font-size: 20px">( </b><b style="font-size: 20px; color: #ff9a18">{$REPORT_DATA.customer_kpi.transaction_kpi.number_transaction_customer}</b><span> /<span><b style="font-size: 20px" class="text-black">  {$REPORT_DATA.customer_kpi.transaction_kpi.number_transaction_in_config} )</b></div>
                                </div>
                            </div>
							<div class="row" style="height: 18%; width: 100%; padding-top: 15px; padding-left: 25px;">
                                <div class="form-group">
                                    <label style="font-size: 16px;margin-bottom:-5px;font-weight:normal" class="text-black">{$REPORT_DATA.customer_kpi.closed_kpi.label}</label> <br/>
                                    <div style="text-align:right"><b class="text-black" style="font-size: 20px">( </b><b style="font-size: 20px; color: #ff9a18">{$REPORT_DATA.customer_kpi.closed_kpi.number_closed_customer}</b><span> /<span><b style="font-size: 20px" class="text-black"> {$REPORT_DATA.customer_kpi.closed_kpi.number_closed_in_config} )</b></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<div class="col-xs-12 col-sm-2 col-md-2">
                <div class="panel panel-default">
                    <div class="panel-heading chart-header">
                        <div class="chart-title text-uppercase text-left">{$APP.KPI_ACTIVITIES}</div>
                    </div>
                    <div class="panel-body" style="height: 290px; border-radius: 5px">
                        <div class="kpiArea">
                            <div class="row" style="height: 18%; width: 100%; padding-top: 15px; padding-left: 25px;">
                                <div class="form-group">
                                    <label style="font-size: 16px;margin-bottom:-5px;font-weight:normal" class="text-black">{$REPORT_DATA.customer_kpi.call_kpi.label}</label> <br/>
                                    <div style="text-align:right"><b class="text-black" style="font-size: 20px">( </b><b style="font-size: 20px; color: #ff9a18">{$REPORT_DATA.customer_kpi.call_kpi.number_call_customer}</b><span> /<span><b class="text-black" style="font-size: 20px">  {$REPORT_DATA.customer_kpi.call_kpi.number_call_in_config} )</b></div>
                                </div>
                            </div>
                            <div class="row" style="height: 18%; width: 100%; padding-top: 15px; padding-left: 25px;">
                                <div class="form-group">
                                    <label style="font-size: 16px;margin-bottom:-5px;font-weight:normal" class="text-black">{$REPORT_DATA.customer_kpi.meeting_kpi.label}</label> <br/>
                                    <div style="text-align:right"><b class="text-black" style="font-size: 20px">( </b><b style="font-size: 20px; color: #ff9a18">{$REPORT_DATA.customer_kpi.meeting_kpi.number_meeting_customer}</b><span> /<span><b style="font-size: 20px" class="text-black">  {$REPORT_DATA.customer_kpi.meeting_kpi.number_meeting_in_config} )</b></div>
                                </div>
                            </div>
							<div class="row" style="height: 18%; width: 100%; padding-top: 15px; padding-left: 25px;">
                                <div class="form-group">
                                    <label style="font-size: 16px;margin-bottom:-5px;font-weight:normal" class="text-black">{$REPORT_DATA.customer_kpi.email_kpi.label}</label> <br/>
                                    <div style="text-align:right"><b class="text-black" style="font-size: 20px">( </b><b style="font-size: 20px; color: #ff9a18">{$REPORT_DATA.customer_kpi.email_kpi.number_email_customer}</b><span> /<span><b class="text-black" style="font-size: 20px">  {$REPORT_DATA.customer_kpi.email_kpi.number_email_in_config} )</b></div>
                                </div>
                            </div>
                            <div class="row" style="height: 18%; width: 100%; padding-top: 15px; padding-left: 25px;">
                                <div class="form-group">
                                    <label style="font-size: 16px;margin-bottom:-5px;font-weight:normal" class="text-black">{$REPORT_DATA.customer_kpi.email_kpi2.label}</label> <br/>
                                     <div style="text-align:right"><b class="text-black" style="font-size: 20px">( </b><b style="font-size: 20px; color: #ff9a18">{$REPORT_DATA.customer_kpi.email_kpi2.number_email_customer2}</b><span> /<span><b class="text-black" style="font-size: 20px">  {$REPORT_DATA.customer_kpi.email_kpi2.number_email_in_config2} )</b></div>
                                </div>
                            </div>
							<div class="row" style="height: 18%; width: 100%; padding-top: 15px; padding-left: 25px;">
                                <div class="form-group">
                                    <label style="font-size: 16px;margin-bottom:-5px;font-weight:normal" class="text-black">{$REPORT_DATA.customer_kpi.history_call_kpi.label}</label> <br/>
                                    <div style="text-align:right"><b class="text-black" style="font-size: 20px">( </b><b style="font-size: 20px; color: #ff9a18">{$REPORT_DATA.customer_kpi.history_call_kpi.number_history_call_customer}</b><span> /<span><b style="font-size: 20px" class="text-black">  {$REPORT_DATA.customer_kpi.history_call_kpi.number_history_call_in_config} )</b></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading text-center text-uppercase chart-header">
                        <div class="chart-title">{$APP.LBL_INPROCESS_CUSTOMER_REPORT_TITLE}</div>
                    </div>
                    <div class="panel-body">
                        <div class="chart-area">
                            <div class="row">
                                <canvas id="percentNumberOfCustomersAreInProcessChart" width="250px" height="250px"></canvas>
                            </div>
                            <div class="row text-center">
                                <b class="text-uppercase" style="font-size: 20px; color: #ff9a18;">{$REPORT_DATA.inprocess_lead.legend.number[0]}</b><span style="font-size: 20px"> / {$REPORT_DATA.inprocess_lead.legend.number[1]}</span>
                            </div>
                            <div class="row">
                                <div class="row">
                                    <div class="col-sm-1 col-xs-1 col-md-1"><div class="chart-legend" style="background-color: #ff9a18"></div></div>
                                    <div class="col-sm-11 col-xs-11 col-md-11">{$REPORT_DATA.inprocess_lead.legend.labels[0]}</div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-1 col-xs-1 col-md-1"><div class="chart-legend" style="background-color: #cccccc"></div></div>
                                    <div class="col-sm-11 col-xs-11 col-md-11">{$REPORT_DATA.inprocess_lead.legend.labels[1]}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading text-center text-uppercase chart-header">
                        <div class="chart-title">{$APP.LBL_CONVERTED_CUSTOMER_REPORT_TITLE}</div>
                    </div>
                    <div class="panel-body">
                        <div class="chart-area">
                            <div class="row">
                                <canvas id="percentNumberOfContractChart" width="250px" height="250px"></canvas>
                            </div>
                            <div class="row text-center">
                                <b class="text-uppercase" style="font-size: 20px; color: #ff9a18;">{$REPORT_DATA.converted_lead.legend.number[0]}</b><span style="font-size: 20px"> / {$REPORT_DATA.converted_lead.legend.number[1]}</span>
                            </div>
                            <div class="row">
                                <div class="row">
                                    <div class="col-sm-1 col-xs-1 col-md-1"><div class="chart-legend" style="background-color: #ff9a18"></div></div>
                                    <div class="col-sm-11 col-xs-11 col-md-11">{$REPORT_DATA.converted_lead.legend.labels[0]}</div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-1 col-xs-1 col-md-1"><div class="chart-legend" style="background-color: #cccccc"></div></div>
                                    <div class="col-sm-11 col-xs-11 col-md-11">{$REPORT_DATA.converted_lead.legend.labels[1]}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading text-center text-uppercase chart-header">
                        <div class="chart-title">{$APP.LBL_REVENUE_REPORT_TITLE}</div>
                    </div>
                    <div class="panel-body">
                        <div class="chart-area">
                            <div class="row">
                                <canvas id="percentOfRevenueChart" width="250px" height="250px"></canvas>
                            </div>
                            <div class="row text-center">
                                <b class="text-uppercase" style="font-size: 20px; color: #ff9a18;">{$REPORT_DATA.revenue.legend.number[0]}</b><span style="font-size: 20px"> / {$REPORT_DATA.revenue.legend.number[1]}</span>
                            </div>
                            <div class="row">
                                <div class="row">
                                        <div class="col-sm-1 col-xs-1 col-md-1"><div class="chart-legend" style="background-color: #ff9a18"></div></div>
                                        <div class="col-sm-11 col-xs-11 col-md-11">{$REPORT_DATA.revenue.legend.labels[0]}</div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-1 col-xs-1 col-md-1"><div class="chart-legend" style="background-color: #cccccc"></div></div>
                                    <div class="col-sm-11 col-xs-11 col-md-11">{$REPORT_DATA.revenue.legend.labels[1]}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
<!-- Filter popup -->
<script type="text/javascript" src="custom/modules/Leads/Dashlets/SalesReportDashlet/js/lib/Chart.min.js"></script>
<script type="text/javascript" src="custom/modules/Leads/Dashlets/SalesReportDashlet/js/SalesReportDashlet.js"></script>
{include file="custom/modules/Leads/Dashlets/SalesReportDashlet/tpls/Filter.tpl"}
</div>
