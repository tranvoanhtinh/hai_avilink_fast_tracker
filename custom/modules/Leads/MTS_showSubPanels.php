<?php

require_once ('custom/modules/Leads/Dashlets/LeadCardDashlet/LeadCardDashletHelper.php');

$helper = new LeadCardDashletHelper();
$lead = BeanFactory::getBean('Leads');
$lead->retrieve($_REQUEST['record']);
echo $helper->getSubpanels($lead);