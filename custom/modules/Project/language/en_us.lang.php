<?php
// created: 2024-06-05 06:17:26
$mod_strings = array (
  'LBL_ACCOUNTS' => 'Agent',
  'LBL_ACCOUNTS_SUBPANEL_TITLE' => 'Agent',
  'LBL_CONTACTS' => 'Contacts',
  'LBL_PROJECT_CONTACTS_1_FROM_CONTACTS_TITLE' => 'Project Contacts from Contacts Title',
  'LBL_CONTACTS_SUBPANEL_TITLE' => 'Contactss',
  'LBL_OPPORTUNITIES' => 'Opportunities',
  'LBL_OPPORTUNITIES_SUBPANEL_TITLE' => 'Opportunities',
  'LBL_NOTES' => 'Notess',
  'LBL_TASKS' => 'Tasks',
  'LBL_MEETINGS' => 'Meetings',
  'LBL_CALLS' => 'Calls',
  'LBL_CASES' => 'Ticketss',
  'LBL_CASES_SUBPANEL_TITLE' => 'Ticketss',
  'LNK_NEW_PROJECT' => 'Create Project',
  'LNK_PROJECT_LIST' => 'View Project List',
  'LBL_LIST_FORM_TITLE' => 'Project List',
  'LBL_SEARCH_FORM_TITLE' => 'Project Search',
  'LBL_AOS_QUOTES_PROJECT' => 'Quotes: Project',
  'LBL_LIST_MY_PROJECT' => 'My Projects',
);