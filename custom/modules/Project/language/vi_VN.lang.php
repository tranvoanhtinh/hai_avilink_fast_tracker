<?php
// created: 2024-06-14 03:32:18
$mod_strings = array (
  'LBL_ACCOUNTS' => 'Accounts',
  'LBL_ACCOUNTS_SUBPANEL_TITLE' => 'Đại lý',
  'LBL_OPPORTUNITIES' => 'Opportunity',
  'LBL_OPPORTUNITIES_SUBPANEL_TITLE' => 'Cơ hội bán hàng',
  'LBL_CONTACTS' => 'Contacts',
  'LBL_PROJECT_CONTACTS_1_FROM_CONTACTS_TITLE' => 'Project Contacts from Contacts Title',
  'LBL_CONTACTS_SUBPANEL_TITLE' => 'Người liên hệ đại lý',
  'LNK_NEW_PROJECT' => 'Khởi tạo một dự án',
  'LNK_PROJECT_LIST' => 'Xem danh mục dự án',
  'LBL_LIST_FORM_TITLE' => 'Danh sách dự án',
  'LBL_SEARCH_FORM_TITLE' => 'Tìm kiếm dự án',
  'LBL_NOTES' => 'Notes',
  'LBL_TASKS' => 'Tasks',
  'LBL_MEETINGS' => 'Meetings',
  'LBL_CALLS' => 'Calls',
  'LBL_CASES' => 'Cases',
  'LBL_CASES_SUBPANEL_TITLE' => 'Tickets',
  'LBL_AOS_QUOTES_PROJECT' => 'Báo giá: Dự án',
);