<?php
// created: 2022-04-04 05:39:49
$mod_strings = array (
  'LNK_NEW_RECORD' => 'Create SummarDetail Data',
  'LNK_LIST' => 'View SummarDetail Data',
  'LNK_IMPORT_REPOR_REPORTSUMMARDETAIL' => 'Import SummarDetail Data',
  'LBL_LIST_FORM_TITLE' => 'SummarDetail Data List',
  'LBL_SEARCH_FORM_TITLE' => 'Search SummarDetail Data',
  'LBL_HOMEPAGE_TITLE' => 'My SummarDetail Data',
);