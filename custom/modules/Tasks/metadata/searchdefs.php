<?php
$searchdefs ['Tasks'] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'task_type' => 
      array (
        'type' => 'enum',
        'label' => 'LBL_TASK_TYPE',
        'width' => '10%',
        'default' => true,
        'name' => 'task_type',
      ),
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'current_user_only' => 
      array (
        'name' => 'current_user_only',
        'label' => 'LBL_CURRENT_USER_FILTER',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
      ),
    ),
    'advanced_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'contact_name' => 
      array (
        'name' => 'contact_name',
        'label' => 'LBL_CONTACT_NAME',
        'type' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'contact_phone' => 
      array (
        'type' => 'phone',
        'studio' => 
        array (
          'listview' => true,
        ),
        'label' => 'LBL_CONTACT_PHONE',
        'width' => '10%',
        'default' => true,
        'name' => 'contact_phone',
      ),
      'status' => 
      array (
        'name' => 'status',
        'default' => true,
        'width' => '10%',
      ),
      'priority' => 
      array (
        'type' => 'enum',
        'label' => 'LBL_PRIORITY',
        'width' => '10%',
        'default' => true,
        'name' => 'priority',
      ),
      'task_type' => 
      array (
        'type' => 'enum',
        'label' => 'LBL_TASK_TYPE',
        'width' => '10%',
        'default' => true,
        'name' => 'task_type',
      ),
      'parent_name' => 
      array (
        'type' => 'parent',
        'label' => 'LBL_LIST_RELATED_TO',
        'width' => '10%',
        'default' => true,
        'name' => 'parent_name',
      ),
      'date_start' => 
      array (
        'type' => 'datetimecombo',
        'studio' => 
        array (
          'required' => true,
          'no_duplicate' => true,
        ),
        'label' => 'LBL_START_DATE',
        'width' => '10%',
        'default' => true,
        'name' => 'date_start',
      ),
      'date_due' => 
      array (
        'type' => 'datetimecombo',
        'studio' => 
        array (
          'required' => true,
          'no_duplicate' => true,
        ),
        'label' => 'LBL_DUE_DATE',
        'width' => '10%',
        'default' => true,
        'name' => 'date_due',
      ),
      'assigned_user_id' => 
      array (
        'name' => 'assigned_user_id',
        'type' => 'enum',
        'label' => 'LBL_ASSIGNED_TO',
        'function' => 
        array (
          'name' => 'get_user_array',
          'params' => 
          array (
            0 => false,
          ),
        ),
        'default' => true,
        'width' => '10%',
      ),
      'current_user_only' => 
      array (
        'name' => 'current_user_only',
        'label' => 'LBL_CURRENT_USER_FILTER',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
;
?>
