<?php
// created: 2024-06-05 06:17:28
$mod_strings = array (
  'LBL_ACCOUNT' => 'Account',
  'LBL_LEADS' => 'Leads',
  'LBL_CONTACT_NAME' => 'Người liên hệ',
  'LBL_CONTACT' => 'Liên hệ',
  'LBL_OPPORTUNITY' => 'Opportunity',
  'LNK_NEW_TASK' => 'Create Task',
  'LNK_TASK_LIST' => 'View Task',
  'LNK_IMPORT_TASKS' => 'Import Task',
  'LBL_LIST_FORM_TITLE' => ' Task List',
  'LBL_SEARCH_FORM_TITLE' => ' Task Search',
  'LBL_CASE' => 'Tickets',
  'LBL_NOTES' => 'Notess',
  'LBL_LIST_MY_TASKS' => 'My Open Tasks',
  'LBL_MODULE_NAME' => 'Task',
  'LBL_PROJECTS' => 'Projects',
  'LBL_DUE_DATE' => 'Due Date',
  'LBL_ASSIGNED_TO_NAME' => 'Assigned to',
  'LBL_TASK_TYPE' => 'type of job',
  'LBL_CONTACT_ID' => 'Contact ID:',
  'LBL_CONTACT_PHONE' => 'Contact Phone:',
);