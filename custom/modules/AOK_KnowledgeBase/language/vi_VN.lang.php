<?php
// created: 2020-11-27 15:22:50
$mod_strings = array (
  'LNK_NEW_RECORD' => 'Tạo Kiến thức căn bản',
  'LNK_LIST' => 'Xem Kiến thức căn bản',
  'LBL_LIST_FORM_TITLE' => 'Danh sách Kiến thức căn bản',
  'LBL_SEARCH_FORM_TITLE' => 'Tìm Kiến thức căn bản',
  'LBL_NAME' => 'Tên',
  'LBL_DOCUMENTTYPE' => 'Loại tài liệu',
  'LBL_SUBTYPEDOCUMENT' => 'Loại tài liệu con',
  'LBL_FILENAME' => 'Tên File',
);