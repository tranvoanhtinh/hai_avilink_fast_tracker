<?php
$popupMeta = array (
    'moduleMain' => 'Prospect',
    'varName' => 'PROSPECT',
    'orderBy' => 'prospects.last_name, prospects.first_name',
    'whereClauses' => array (
  'phone' => 'prospects.phone',
  'email' => 'prospects.email',
  'do_not_call' => 'prospects.do_not_call',
  'assigned_user_id' => 'prospects.assigned_user_id',
  'full_name' => 'prospects.full_name',
  'account_name' => 'prospects.account_name',
  'leadsource_c' => 'prospects_cstm.leadsource_c',
  'shortname_c' => 'prospects_cstm.shortname_c',
  'vatcode_c' => 'prospects_cstm.vatcode_c',
),
    'searchInputs' => array (
  3 => 'phone',
  4 => 'email',
  5 => 'do_not_call',
  6 => 'assigned_user_id',
  7 => 'full_name',
  8 => 'account_name',
  9 => 'leadsource_c',
  10 => 'shortname_c',
  11 => 'vatcode_c',
),
    'create' => array (
  'formBase' => 'ProspectFormBase.php',
  'formBaseClass' => 'ProspectFormBase',
  'getFormBodyParams' => 
  array (
    0 => '',
    1 => '',
    2 => 'ProspectSave',
  ),
  'createButton' => 'LNK_NEW_PROSPECT',
),
    'searchdefs' => array (
  'full_name' => 
  array (
    'type' => 'fullname',
    'studio' => 
    array (
      'listview' => false,
    ),
    'label' => 'LBL_NAME',
    'width' => '10%',
    'name' => 'full_name',
  ),
  'shortname_c' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_SHORTNAME',
    'width' => '10%',
    'name' => 'shortname_c',
  ),
  'account_name' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ACCOUNT_NAME',
    'width' => '10%',
    'name' => 'account_name',
  ),
  'vatcode_c' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_VATCODE',
    'width' => '10%',
    'name' => 'vatcode_c',
  ),
  'phone' => 
  array (
    'name' => 'phone',
    'label' => 'LBL_ANY_PHONE',
    'type' => 'name',
    'width' => '10%',
  ),
  'email' => 
  array (
    'name' => 'email',
    'label' => 'LBL_ANY_EMAIL',
    'type' => 'name',
    'width' => '10%',
  ),
  'assigned_user_id' => 
  array (
    'name' => 'assigned_user_id',
    'type' => 'enum',
    'label' => 'LBL_ASSIGNED_TO',
    'function' => 
    array (
      'name' => 'get_user_array',
      'params' => 
      array (
        0 => false,
      ),
    ),
    'width' => '10%',
  ),
  'leadsource_c' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_LEADSOURCE',
    'width' => '10%',
    'name' => 'leadsource_c',
  ),
  'do_not_call' => 
  array (
    'name' => 'do_not_call',
    'width' => '10%',
  ),
),
);
