<?php
$searchdefs ['Prospects'] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'full_name' => 
      array (
        'type' => 'fullname',
        'studio' => 
        array (
          'listview' => false,
        ),
        'label' => 'LBL_NAME',
        'width' => '10%',
        'default' => true,
        'name' => 'full_name',
      ),
      'shortname_c' => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_SHORTNAME',
        'width' => '10%',
        'name' => 'shortname_c',
      ),
      'email' => 
      array (
        'name' => 'email',
        'label' => 'LBL_ANY_EMAIL',
        'type' => 'name',
        'width' => '10%',
        'default' => true,
      ),
      'phone' => 
      array (
        'name' => 'phone',
        'label' => 'LBL_ANY_PHONE',
        'type' => 'name',
        'width' => '10%',
        'default' => true,
      ),
      'do_not_call' => 
      array (
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
        'label' => 'LBL_DO_NOT_CALL',
        'name' => 'do_not_call',
      ),
      'current_user_only' => 
      array (
        'name' => 'current_user_only',
        'label' => 'LBL_CURRENT_USER_FILTER',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
      ),
    ),
    'advanced_search' => 
    array (
      'full_name' => 
      array (
        'type' => 'fullname',
        'studio' => 
        array (
          'listview' => false,
        ),
        'label' => 'LBL_NAME',
        'width' => '10%',
        'default' => true,
        'name' => 'full_name',
      ),
      'shortname_c' => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_SHORTNAME',
        'width' => '10%',
        'name' => 'shortname_c',
      ),
      'phone' => 
      array (
        'name' => 'phone',
        'label' => 'LBL_ANY_PHONE',
        'type' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'email' => 
      array (
        'name' => 'email',
        'label' => 'LBL_ANY_EMAIL',
        'type' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'address_street' => 
      array (
        'name' => 'address_street',
        'label' => 'LBL_ANY_ADDRESS',
        'type' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'bussiness_type_c' => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_BUSSINESS_TYPE',
        'width' => '10%',
        'name' => 'bussiness_type_c',
      ),
      'vatcode_c' => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_VATCODE',
        'width' => '10%',
        'name' => 'vatcode_c',
      ),
      'leadsource_c' => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_LEADSOURCE',
        'width' => '10%',
        'name' => 'leadsource_c',
      ),
      'assigned_user_id' => 
      array (
        'name' => 'assigned_user_id',
        'type' => 'enum',
        'label' => 'LBL_ASSIGNED_TO',
        'function' => 
        array (
          'name' => 'get_user_array',
          'params' => 
          array (
            0 => false,
          ),
        ),
        'default' => true,
        'width' => '10%',
      ),
      'date_entered' => 
      array (
        'type' => 'datetime',
        'label' => 'LBL_DATE_ENTERED',
        'width' => '10%',
        'default' => true,
        'name' => 'date_entered',
      ),
      'date_modified' => 
      array (
        'type' => 'datetime',
        'label' => 'LBL_DATE_MODIFIED',
        'width' => '10%',
        'default' => true,
        'name' => 'date_modified',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
;
?>
