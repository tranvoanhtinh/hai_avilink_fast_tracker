<?php
$module_name = 'AOS_Booking_detail';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
  'BOOKING_DATE' => 
  array (
    'type' => 'date',
    'label' => 'LBL_BOOKING_DATE',
    'width' => '10%',
    'default' => true,
  ),
  'DATE_STD_STA' => 
  array (
    'type' => 'datetimecombo',
    'default' => true,
    'label' => 'LBL_DATE_STD_STA',
    'width' => '10%',
  ),
);
;
?>
