<?php
$dashletData['AOS_InventoryinputDashlet']['searchFields'] = array (
  'billing_account' => 
  array (
    'default' => '',
  ),
  'mobileref' => 
  array (
    'default' => '',
  ),
  'take_from' => 
  array (
    'default' => '',
  ),
  'voucherdate' => 
  array (
    'default' => '',
  ),
  'assigned_user_id' => 
  array (
    'default' => '',
  ),
);
$dashletData['AOS_InventoryinputDashlet']['columns'] = array (
  'name' => 
  array (
    'width' => '40%',
    'label' => 'LBL_LIST_NAME',
    'link' => true,
    'default' => true,
    'name' => 'name',
  ),
  'billing_account' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'label' => 'LBL_BILLING_ACCOUNT',
    'id' => 'BILLING_ACCOUNT_ID',
    'link' => true,
    'width' => '10%',
    'default' => true,
  ),
  'mobileref' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_MOBILE_REF',
    'width' => '10%',
    'default' => true,
  ),
  'date_entered' => 
  array (
    'width' => '15%',
    'label' => 'LBL_DATE_ENTERED',
    'default' => true,
    'name' => 'date_entered',
  ),
  'date_modified' => 
  array (
    'width' => '15%',
    'label' => 'LBL_DATE_MODIFIED',
    'name' => 'date_modified',
    'default' => false,
  ),
  'billing_address_street' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_BILLING_ADDRESS_STREET',
    'width' => '10%',
    'default' => false,
  ),
  'shipping_address_street' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_SHIPPING_ADDRESS_STREET',
    'width' => '10%',
    'default' => false,
  ),
  'total_amt' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_TOTAL_AMT',
    'currency_format' => true,
    'width' => '10%',
    'default' => false,
  ),
  'odn' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ODN',
    'width' => '10%',
    'default' => false,
  ),
  'billing_contact' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'label' => 'LBL_BILLING_CONTACT',
    'id' => 'BILLING_CONTACT_ID',
    'link' => true,
    'width' => '10%',
    'default' => false,
  ),
  'created_by' => 
  array (
    'width' => '8%',
    'label' => 'LBL_CREATED',
    'name' => 'created_by',
    'default' => false,
  ),
  'assigned_user_name' => 
  array (
    'width' => '8%',
    'label' => 'LBL_LIST_ASSIGNED_USER',
    'name' => 'assigned_user_name',
    'default' => false,
  ),
);
