<?php
$module_name = 'AOS_Inventoryinput';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'mobileref' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_MOBILE_REF',
        'width' => '10%',
        'default' => true,
        'name' => 'mobileref',
      ),
      'billing_account' => 
      array (
        'type' => 'relate',
        'studio' => 'visible',
        'label' => 'LBL_BILLING_ACCOUNT',
        'id' => 'BILLING_ACCOUNT_ID',
        'link' => true,
        'width' => '10%',
        'default' => true,
        'name' => 'billing_account',
      ),
    ),
    'advanced_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'billing_account' => 
      array (
        'type' => 'relate',
        'studio' => 'visible',
        'label' => 'LBL_BILLING_ACCOUNT',
        'link' => true,
        'width' => '10%',
        'default' => true,
        'id' => 'BILLING_ACCOUNT_ID',
        'name' => 'billing_account',
      ),
      'mobileref' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_MOBILE_REF',
        'width' => '10%',
        'default' => true,
        'name' => 'mobileref',
      ),
      'billing_contact' => 
      array (
        'type' => 'relate',
        'studio' => 'visible',
        'label' => 'LBL_BILLING_CONTACT',
        'id' => 'BILLING_CONTACT_ID',
        'link' => true,
        'width' => '10%',
        'default' => true,
        'name' => 'billing_contact',
      ),
      'odn' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_ODN',
        'width' => '10%',
        'default' => true,
        'name' => 'odn',
      ),
      'is_invoice' => 
      array (
        'type' => 'bool',
        'default' => true,
        'label' => 'LBL_ISINVOICE',
        'width' => '10%',
        'name' => 'is_invoice',
      ),
      'status_inventory' => 
      array (
        'type' => 'enum',
        'label' => 'LBL_STATUS_INVENTORY',
        'width' => '10%',
        'default' => true,
        'name' => 'status_inventory',
      ),
      'take_from' => 
      array (
        'type' => 'enum',
        'label' => 'LBL_TAKE_FROM',
        'width' => '10%',
        'default' => true,
        'name' => 'take_from',
      ),
      'voucherdate' => 
      array (
        'type' => 'date',
        'label' => 'LBL_VOUCHER_DATE',
        'width' => '10%',
        'default' => true,
        'name' => 'voucherdate',
      ),
      'assigned_user_id' => 
      array (
        'name' => 'assigned_user_id',
        'label' => 'LBL_ASSIGNED_TO',
        'type' => 'enum',
        'function' => 
        array (
          'name' => 'get_user_array',
          'params' => 
          array (
            0 => false,
          ),
        ),
        'default' => true,
        'width' => '10%',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
;
?>
