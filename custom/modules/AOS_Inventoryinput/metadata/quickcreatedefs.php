<?php
$module_name = 'AOS_Inventoryinput';
$viewdefs [$module_name] = 
array (
  'QuickCreate' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'LBL_PANEL_OVERVIEW' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_LINE_ITEMS' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'LBL_PANEL_OVERVIEW' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 
          array (
            'name' => 'voucherdate',
            'label' => 'LBL_VOUCHER_DATE',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'status_inventory',
            'studio' => true,
            'label' => 'LBL_STATUS_INVENTORY',
          ),
          1 => 
          array (
            'name' => 'take_from',
            'label' => 'LBL_TAKE_FROM',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'billing_account',
            'studio' => 'visible',
            'label' => 'LBL_BILLING_ACCOUNT',
          ),
          1 => 
          array (
            'name' => 'mobileref',
            'label' => 'LBL_MOBILE_REF',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'billing_contact',
            'studio' => 'visible',
            'label' => 'LBL_BILLING_CONTACT',
          ),
          1 => 
          array (
            'name' => 'paymentmethod',
            'studio' => true,
            'label' => 'LBL_PAYMENTMETHOD',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'is_invoice',
            'label' => 'LBL_ISINVOICE',
          ),
          1 => 
          array (
            'name' => 'odn',
            'label' => 'LBL_ODN',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'description',
            'comment' => 'Full text of the note',
            'label' => 'LBL_DESCRIPTION',
          ),
          1 => 'assigned_user_name',
        ),
      ),
      'lbl_line_items' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'currency_id',
            'studio' => 'visible',
            'label' => 'LBL_CURRENCY',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'pos_line_items',
            'label' => 'LBL_POS_LINE_ITEMS',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'lp_total_amt',
            'label' => 'LBL_LP_TOTAL_AMT',
          ),
          1 => '',
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'lp_discount_amount',
            'label' => 'LBL_LP_DISCOUNT_AMOUNT',
          ),
          1 => '',
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'lp_subtotal_amount',
            'label' => 'LBL_LP_SUBTOTAL_AMOUNT',
          ),
          1 => '',
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'lp_shipping_amount',
            'label' => 'LBL_LP_SHIPPING_AMOUNT',
          ),
          1 => '',
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'shipping_tax_amt',
            'label' => 'LBL_SHIPPING_TAX_AMT',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'lp_tax_amount',
            'label' => 'LBL_LP_TAX_AMOUNT',
          ),
          1 => '',
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'lp_total_amount',
            'label' => 'LBL_LP_TOTAL_AMOUNT',
          ),
          1 => '',
        ),
      ),
    ),
  ),
);
;
?>
