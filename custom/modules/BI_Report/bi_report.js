// Lấy tất cả các thẻ div có class là "label" và có nội dung là "LBL_BUSINESS_REPORT:"
var labelDivs = document.querySelectorAll(".label");

// Lặp qua từng thẻ div
labelDivs.forEach(function (labelDiv) {
  // Kiểm tra xem nội dung của thẻ có phải là "LBL_BUSINESS_REPORT:" không
  if (
    labelDiv.textContent.trim() === "LBL_BUSINESS_REPORT:" ||
    labelDiv.textContent.trim() === "LBL_CHOOSE_TIME:"
  ) {
    // Ẩn thẻ div
    labelDiv.style.display = "none";
  }
});

var element = document.querySelector(
  'div.col-xs-12.col-sm-10.detail-view-field[type="function"][field="business_report"][colspan="3"]'
);

// Kiểm tra xem phần tử có tồn tại không
if (element) {
  // Gán chiều rộng là 130%
  element.style.width = "100%";
  element.style.marginTop = "10px";
}
var element = document.querySelector(
  'div.col-xs-12.col-sm-10.detail-view-field[type="function"][field="choose_time"][colspan="3"]'
);

// Kiểm tra xem phần tử có tồn tại không
if (element) {
  // Gán chiều rộng là 130%
  element.style.width = "100%";
}
