<?php
function inventory_report_PI($focus, $field, $value, $view){
    global $mod_strings, $sugar_config, $db;
    $take_from = $GLOBALS['app_list_strings']['take_from_list'];
    if($view == 'DetailView'){

        $price = 0;
        $total_price = 0;
        $total_beging = 0;
        $total_end =0;
        $quantity_in = 0;
        $quantity_out = 0;
        $type = -1;
        $currentDate = date('d/m/Y');
        $dateParts = explode('/', $currentDate);
        $day = $dateParts[0];
        $month = $dateParts[1];
        $year = $dateParts[2];
        $html = '';
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $date_from = isset($_POST["datefrom"]) ? $_POST["datefrom"] : "";
            $date_to = isset($_POST["dateto"]) ? $_POST["dateto"] : "";
            $warehouse = isset($_POST['warehouse']) ? $_POST['warehouse'] : "";

            $parts_from = explode("/", $date_from);
            $day_from = isset($parts_from[0]) ? $parts_from[0] : "";
            $month_from = isset($parts_from[1]) ? $parts_from[1] : "";
            $year_from = isset($parts_from[2]) ? $parts_from[2] : "";

            // Phân tích ngày kết thúc
            $parts_to = explode("/", $date_to);
            $day_to = isset($parts_to[0]) ? $parts_to[0] : "";
            $month_to = isset($parts_to[1]) ? $parts_to[1] : "";
            $year_to = isset($parts_to[2]) ? $parts_to[2] : "";
 
             $html.= '<span class="title">';

            if ($date_from == 1 && $day_to > 28 && $month_from == $month_to && $year_from == $year_to) {
                $html .= 'Thông kê theo tháng ' . $month_from . ' năm ' . $year_from;
                $type = 1;
                
                $query = "SELECT * FROM `aos_stock_inventory` 
                          WHERE month = '".$month_from."' AND year = '".$year_from."' AND deleted = '0'";
                
                // Nếu $warehouse không rỗng
                if (!empty($warehouse)) {
                    // Thêm điều kiện WHERE cho kho
                    if (is_array($warehouse)) {
                        // Nếu $warehouse là một mảng, chuyển đổi nó thành chuỗi và thêm vào câu truy vấn
                        $warehouse_list = implode("','", $warehouse);
                        $query .= " AND warehouse IN ('" . $warehouse_list . "')";
                    } else {
                        // Nếu $warehouse là một giá trị đơn, thêm nó vào câu truy vấn
                        $query .= " AND warehouse = '".$warehouse."'";
                    }
                }

                $query .= " ORDER BY name ASC";
            }
            elseif($date_from ==1 && $day_to > 28 && $month_from == 1 && $month_to ==12 && $year_from == $year_to){
                $html.= 'Thông kê theo năm '. $year_from;
                $type = 2;

                $query = "SELECT product_id, SUM(import) AS import , SUM(export) AS export , AVG(price) AS price, SUM(beginning_balance) AS beginning_balance, SUM(ending_balance) AS ending_balance , warehouse, name , maincode
                          FROM `aos_stock_inventory`
                          WHERE year = '2024' AND deleted = '0'";

                // Check if warehouse condition needs to be applied
                if (!empty($warehouse)) {
                    // Thêm điều kiện WHERE cho kho
                    if (is_array($warehouse)) {
                        // Nếu $warehouse là một mảng, chuyển đổi nó thành chuỗi và thêm vào câu truy vấn
                        $warehouse_list = implode("','", $warehouse);
                        $query .= " AND warehouse IN ('" . $warehouse_list . "')";
                    } else {
                        // Nếu $warehouse là một giá trị đơn, thêm nó vào câu truy vấn
                        $query .= " AND warehouse = '".$warehouse."'";
                    }
                }

                $query .= " GROUP BY product_id, warehouse
                            ORDER BY name ASC";



            }else{
                $html.= 'Thông kê từ ngày '.$date_from. ' đến ngày '. $date_to;
                $type = 3;
            }
            $html.= '</span>';
            $html.= $query;

            $html .= '<div style="height: 600px; overflow: auto;">'; // Thẻ div với chiều cao 400px và thanh cuộn
            $html .= '<table id ="table-inventory" class="table table-striped table-bordered table-hover">';
            $html .= '<tr>';
            $html .='<th style="width: 8%; position: sticky; top: 0; background-color: white;">STT</th>';
            $html .='<th style="width: 8%; position: sticky; top: 0; background-color: white;">'.$mod_strings['LBL_MAINCODE'].'</th>
                <th style="width: 8%; position: sticky; top: 0; background-color: white;">'.$mod_strings['LBL_PRODUCT_NAME'].'</th>
                <th style="width: 8%; position: sticky; top: 0; background-color: white;">'.$mod_strings['LBL_BEGINNING_QUANTITY'].'</th>
                <th style="width: 8%; position: sticky; top: 0; background-color: white;">'.$mod_strings['LBL_QUANTITY_IN'].'</th>
                <th style="width: 8%; position: sticky; top: 0; background-color: white;">'.$mod_strings['LBL_PRICE'].'</th>
                <th style="width: 8%; position: sticky; top: 0; background-color: white;">'.$mod_strings['LBL_TOTAL_PRICE'].'</th>
                <th style="width: 8%; position: sticky; top: 0; background-color: white;">'.$mod_strings['LBL_QUANTITY_OUT'].'</th>
                <th style="width: 8%; position: sticky; top: 0; background-color: white;">'.$mod_strings['LBL_END_QUANTITY'].'</th>
                <th style="width: 8%; position: sticky; top: 0; background-color: white;">'.$mod_strings['LBL_WAREHOUSE'].'</th>';
            $html .= '</tr>';
            $result = $db->query($query);
            $i = 1;
            while($rows = $db->fetchByAssoc($result)){
                $html .= '<tr>';
                $html .= '<td>'.$i.'</td>';
                $html .= '<td>'.$rows['maincode'].'</td>';
                $html .= '<td>'.$rows['name'].'</td>';
                $html .= '<td id="beginning_balance_'.+$i.'" class="td-mid beginning_balance">'.$rows['beginning_balance'].'</td>';
                $html .= '<td class="td-mid">'.$rows['import'].'</td>';
                $html .= '<td>'.currency_format_number($rows['price']).'</td>';
                $total_price =  round($rows["price"]) * $rows['import'];

                $html .= '<td>'.currency_format_number($total_price).'</td>';

                $html .= '<td class="td-mid">'.$rows['export'].'</td>';
                $html .= '<td  id="ending_balance_'.+$i.'" class="td-mid ending_balance">'.$rows['ending_balance'].'</td>';
                $html .= '<td>'.$take_from[$rows['warehouse']].'</td>';
                $html .= '</tr>';     
                $i++ ;            
            }
            $html .= '</table>';
            $html .= '</div>'; // Kết thúc thẻ div



        }
        return $html;
    }
}




function inventory_choostime($focus, $field, $value, $view){
 global $mod_strings, $sugar_config, $db;
    if ($view == 'DetailView') {
        $html = '';
        $html .= '<link rel="stylesheet" type="text/css" href="custom/modules/BI_Report/css/inventory_style.css">';
        // Add a dropdown filter
        $html .= '<table id="table_search">';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<select name="time_filter" id="time_filter" style="width: 150px;" onchange="handleChange(this)">';
        $html .= '<option value="-1">-- &nbspLọc theo&nbsp --</option>'; // Thêm tùy chọn lọc theo tất cả
        $html .= '<option value="month">Tháng</option>';
        $html .= '<option value="year">Năm</option>';
        $html .= '<option value="time_range">Khác</option>';
        $html .= '</select>';
        $html .= '</td>';

        // Thêm hai cột nữa vào bảng
        $html .= '<td><input name="submit" onclick="submitForm()" id="statistics" type="submit" value="'.$mod_strings['LBL_STATISTICS'].'" style="height: 30px;"></td>';


        $html .= '<td>';
        $html .= '<input type="button" value="'.$mod_strings['LBL_CLEAR_FILTERS'].'" onclick="clearFilters()" style="margin-top: 5px; height: 30px;">';
        $html .= '</td>';



        $html .= '</tr>';
        $html .= '</table>';


        $html .= '<form id="from_statistics" action="" method="post">';
        $html .= '<table id ="table_from_search">';
        $html .= '<tr>';
        $html .= '<td>';
        if ($mod_strings['LBL_MONTH'] == 'Tháng') {
            // Tiếng Việt
            $monthNames = array(
                1 => "Tháng 1",
                2 => "Tháng 2",
                3 => "Tháng 3",
                4 => "Tháng 4",
                5 => "Tháng 5",
                6 => "Tháng 6",
                7 => "Tháng 7",
                8 => "Tháng 8",
                9 => "Tháng 9",
                10 => "Tháng 10",
                11 => "Tháng 11",
                12 => "Tháng 12"
            );
            $html .= '<select name="month" id="choose_month" onchange="setDate2(this.value)">'; 

            $html .= '<option value="">' . $mod_strings['LBL_SELECT_MONTH'] . '</option>';
            for ($i = 1; $i <= 12; $i++) {
                $selected = isset($_POST['month']) && $_POST['month'] == $i ? 'selected' : '';
                $html .= '<option value="' . $i . '" ' . $selected . '>' . $monthNames[$i] . '</option>';
            }
            $html .= '</select>';
        } else {
            // Tiếng Anh
            $html .= '<select name="month">';
            $html .= '<option value="">- Month -</option>';
            for ($i = 1; $i <= 12; $i++) {
                $month = date('F', mktime(0, 0, 0, $i, 1));
                $selected = isset($_POST['month']) && $_POST['month'] == $i ? 'selected' : '';
                $html .= '<option value="' . $i . '" ' . $selected . '>' . $month . '</option>';
            }
            $html .= '</select>';
        }
        $html .= '</td>';

        $html .= '<td>';
        $html .= $mod_strings['LBL_FROM'];
        $html .= '</td>';

        $html .= '<td>';
        $html .= '&nbsp<input type="text" name="datefrom" id ="datefrom" value="' . (isset($_POST['datefrom']) ? $_POST['datefrom'] : '') . '">';
        $html .= '</td>';

        $html .= '<td>';
        $html .= $mod_strings['LBL_TO'];
        $html .= '</td>';

        $html .= '<td>';
        $html .= '&nbsp<input type="text" name="dateto" id="dateto" value="' . (isset($_POST['dateto']) ? $_POST['dateto'] : '') . '">';
        $html .= '</td>';

        $html .= '<td>';
        $html .= $mod_strings['LBL_YEAR'];
        $html .= '</td>';

        $html .= '<td>';
        $html .= '&nbsp<input type="text" id="year" name="year" onblur="setDate(this.value)" value="' . (isset($_POST['year']) ? $_POST['year'] : date("Y")) . '" style="background-color: white;">';
        $html .= '</td>';


        $html .= '<td> &nbsp </td>';
        $html .= '<td>';
        $html .= '<select name="warehouse[]" id ="warehouse" multiple>';

        foreach ($GLOBALS['app_list_strings']['take_from_list'] as $key => $value) {
            if ($key != 0) {
                $selected = isset($_POST['warehouse']) && in_array($key, $_POST['warehouse']) ? 'selected' : '';
                $html .= '<option value="' . $key . '" ' . $selected . '>' . $value . '</option>';
            }
        }

        $html .= '</select>';
        $html .= '</table>';
        $html .= '</form>';

    // Thêm dữ liệu vào đây nếu cần


        return $html;
    }

}


function getdateYmd($date) {
    $dateObj = DateTime::createFromFormat('d/m/Y', $date);
    return $dateObj->format('Y-m-d');
}
function getdatedmY($date) {
    $dateObj = DateTime::createFromFormat('Y-m-d', $date);
    return $dateObj->format('d/m/Y');
}
