<?php



function summary_report_PI ($focus, $field, $value, $view){

    global $mod_strings; $sugar_config; $db;
    if ($view == 'DetailView') {
        if (file_exists(getcwd() . '/config.php')) {
            require(getcwd() . '/config.php');
        }
        // if ($_SERVER["REQUEST_METHOD"] == "POST") {
        //     // Kiểm tra xem ô nhập liệu "Từ" và "Đến" có được gửi đi không
        //     if (isset($_POST["date_from2"]) && isset($_POST["date_to2"])) {
        // Lấy giá trị từ ô nhập liệu "Từ"

        $dateFrom = $_POST["date_from"];
        if ($dateFrom == '') {
           $dateFrom = date('d/m/Y', strtotime('monday this week'));
        }

        // Lấy giá trị từ ô nhập liệu "Đến"
        $dateTo = $_POST["date_to"];
        if ($dateTo == '') {
            $dateTo = date('d/m/Y', strtotime('sunday this week'));
        }


        $dateFromObj = DateTime::createFromFormat('d/m/Y', $dateFrom);
        $dateToObj = DateTime::createFromFormat('d/m/Y', $dateTo);

        $dateFrom2 = $dateFromObj->format('Y-m-d');
        $dateTo2 = $dateToObj->format('Y-m-d');



        require_once('custom/modules/BI_Report/data_summary/Get_data_summary.php');
        $api = new Get_data_summary();
        $array = $api->summary_for_dashboard('-1',$dateFrom2,$dateTo2,$_POST['users']);


        // $userAuth = [
        //     "user_name" => $sugar_config['api_username'],
        //     "password" => $sugar_config['api_password'],
        // ];
        // $appName = "CRMOnline";
        // $nameValueList = [];

        // $args = [
        //     "user_auth" => $userAuth,
        //     "application_name" => $appName,
        //     "name_value_list" => $nameValueList,
        // ];

        // $result = restRequest("login", $args);
        // $sessId = $result["id"];


        // $entryArgs = [
        //     "session" => $sessId,
        //     "from_date" => $dateFrom2,
        //     "to_date" => $dateTo2,
        //     "assigned_user_id"=> $_POST['users'],
        // ];
        // $result2 = restRequest("summary_for_dashboard", $entryArgs);
        // $array = $result2;

        // //     }
        // // }
        $fields = ['sales_opportunity_summary', 'order_summary', 'quotation_summary', 'contract_summary', 'call_summary', 'meet_summary', 'task_summary', 'ticket_summary'];
        $html='';
        $html .= '<style>';
        $html .= '  .custom-table { width: 100%; background-color: #; table-layout: fixed; }';
        $html .= '  .custom-column { width: 30%; display: inline-block; margin: 1%; vertical-align: top;}';
        $html .= '  span{font-size: 16px}';
        $html .= '  .custom-column .custom-inner-table {';
        $html .= '      width: 100%;';
        $html .= '      border-collapse: collapse;';
        $html .= '  }';

        $html .= '  .custom-column .custom-inner-table th,';
        $html .= '  .custom-column .custom-inner-table td {';

        $html .= '      padding: 8px;';
        $html .= '  }';

        $html .= '  .custom-column .custom-inner-table td:nth-child(1),';
        $html .= '  .custom-column .custom-inner-table th:nth-child(1) {';
        $html .= '      width: 40%;';
        $html .= '  }';

        $html .= '  .custom-column .custom-inner-table td:nth-child(2),';
        $html .= '  .custom-column .custom-inner-table th:nth-child(2) {';
        $html .= '      width: 20%;';
        $html .= '  }';

        $html .= '  .custom-column .custom-inner-table td:nth-child(3),';
        $html .= '  .custom-column .custom-inner-table th:nth-child(3) {';
        $html .= '      width: 40%;';
        $html .= '  }';
        $html .= '.format_currency {font-weight: bold; font-size: 14.5px;}';
        $html .= '.font15 {font-weight: bold; font-size: 16px;}';
        $html .= '.count {font-weight: bold; font-size: 16px;}';

        $html .= '</style>';


        $html .= '<table class="custom-table">';


        $html .= '<td class="custom-column">';
        $html .= '<table class="custom-inner-table">';

        $html .= '<tr>';
        $html .= '<td colspan="3" class="font15" style="background-color: #CD181818 ;padding: 1px; font-weight: bold;">' . $mod_strings['LBL_LEAD'] . '</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        if (
            isset($array['num_new_lead'], $array['new_lead_amount']) &&
            $array['num_new_lead'] !== 0 || $array['new_lead_amount'] !== 0
        ) {
            $html .= '<td><span class="" style="color: white;background-color: #3ADF00; display: inline-block; padding: 3px; border-radius: 5px;">' . $mod_strings['LBL_NEW_POTENTIAL'] . '</span></td>';
            $html .= '<td class="count"style="text-align: right">' . $array['num_new_lead'] . ' <span class="suitepicon suitepicon-admin-user-management"></span></td>';
            $html .= '<td class="format_currency" style="text-align: right;">' . number_format($array['new_lead_amount']) . ' </td>';
        }
        $html .= '</tr>';

        $html .= '<tr>';
        if (isset($array['num_in_process_lead'], $array['in_process_lead_amount']) && $array['num_in_process_lead'] !== 0 || $array['in_process_lead_amount'] !== 0) {
            $html .= '<td><span style="color: white;background-color: #FF0000; display: inline-block; padding: 3px; border-radius: 5px;">' . $mod_strings['LBL_CONTACTING'] . '</span></td>';
            $html .= '<td class="count"style="text-align: right">' . $array['num_in_process_lead'] . ' <span class="suitepicon suitepicon-admin-user-management"></span></td>';
            $html .= '<td class="format_currency" style="text-align: right;">' . number_format($array['in_process_lead_amount']) . ' </td>';
        }
        $html .= '</tr>';

        $html .= '<tr>';
        if (isset($array['num_dead_lead'], $array['dead_lead_amount']) && $array['num_dead_lead'] !== 0 || $array['dead_lead_amount'] !== 0) {
            $html .= '<td><span style="color: white;background-color: #00BFFF; display: inline-block; padding: 3px; border-radius: 5px;">' . $mod_strings['LBL_STOPCARING'] . '</span></td>';
            $html .= '<td class="count" style="text-align: right">' . $array['num_dead_lead'] . ' <span class="suitepicon suitepicon-admin-user-management"></span></td>';
            $html .= '<td class="format_currency" style="text-align: right;">' . number_format($array['dead_lead_amount']) . ' </td>';
        }
        $html .= '</tr>';
        $html .= '<tr>';

        $totalCount = $array['num_dead_lead'] + $array['num_in_process_lead'] + $array['num_new_lead'];
        $totalAmount = $array['new_lead_amount'] + $array['in_process_lead_amount'] + $array['dead_lead_amount'];

        if ($totalCount != 0) {
            $html .= '<td style="color: blue; font-size: 16px;"><strong>'.$mod_strings['LBL_TOTAL'].'</strong></td>';
            $html .= '<td class="count" style="text-align: right; color: blue; font-size: 16px;">' . $totalCount . ' <span class="suitepicon suitepicon-admin-user-management"></span></td>';
            if ($totalAmount != 0) {
                $html .= '<td class="format_currency" style="text-align: right; color: blue; font-size: 16px">' . number_format($totalAmount) . '</td>';
            }
        }
        $html .= '</tr>';


        $html .= '</table>';
        $html .= '</td>';
//======================================
        $html .= '<td class="custom-column">';
        $html .= '<table class="custom-inner-table">';

        $html .= '<tr>';
        $html .= '<td colspan="3" class="font15" style="background-color: #CD181818 ;padding: 1px; font-weight: bold;">' . $mod_strings['LBL_ACCOUNT'] . '</td>';
        $html .= '</tr>';


        $html .= '<tr>';
        if (isset($array['num_account'], $array['account_opportunity_amount']) && $array['num_account'] !== 0 || $array['account_opportunity_amount'] !== 0) {
            $html .= '<td><span style="color: white;background-color: #FFBF00; display: inline-block; padding: 3px; border-radius: 5px;">' . $mod_strings['LBL_NEEDARISES'] . '</span></td>';
            $html .= '<td class="count"style="text-align: right">' . $array['num_account'] . ' <span class="suitepicon suitepicon-admin-user-management"></span></td>';
            $html .= '<td class="format_currency" style="text-align: right;">' . number_format($array['account_opportunity_amount']) . ' </td>';
        }
        $html .= '</tr>';

        $html .= '<tr>';
        if (isset($array['num_account_has_transaction'], $array['account_opportunity_won_amount']) && $array['num_account_has_transaction'] !== 0 || $array['account_opportunity_won_amount'] !== 0) {
            $html .= '<td><span style="color: white;background-color: #FF8000; display: inline-block; padding: 3px; border-radius: 5px;">' . $mod_strings['PURCHASED'] . '</span></td>';
            $html .= '<td class="count"style="text-align: right">' . $array['num_account_has_transaction'] . ' <span class="suitepicon suitepicon-admin-user-management"></span></td>';
            $html .= '<td class="format_currency" style="text-align: right;">' . number_format($array['account_opportunity_won_amount']) . ' </td>';
        }
        $html .= '</tr>';
        $html .= '<tr>';

        $totalCount = $array['num_account_has_transaction'] + $array['num_account'];
        $totalAmount = $array['account_opportunity_amount'] + $array['account_opportunity_won_amount'];

        if ($totalCount != 0) {
            $html .= '<td style="color: blue; font-size: 16px;"><strong>'.$mod_strings['LBL_TOTAL'].'</strong></td>';
            $html .= '<td class="count" style="text-align: right; color: blue; font-size: 16px;">' . $totalCount . ' <span class="suitepicon suitepicon-admin-user-management"></span></td>';
            if ($totalAmount != 0) {
                $html .= '<td class="format_currency" style="text-align: right; color: blue; font-size: 16px">' . number_format($totalAmount) . '</td>';
            }
        }
        $html .= '</tr>';


        $html .= '</table>';
        $html .= '</td>';


        $fieldxs = [
            'billing_3_summary',
            'billing_payment_summary',
            'billing_receipts_summary',
            // Add other keys as needed
        ];

        foreach ($fieldxs as $field) {
            $html .= '<td class="custom-column">';
            $html .= '<table class="custom-inner-table">';

            // Add title row
            $html .= '<tr>';
            $html .= '<td colspan="3" class="font15"  style="background-color: #CD181818 ;padding: 1px; font-weight: bold; font-weight: bold">' . $mod_strings[$field] . '</td>';
            $html .= '</tr>';

            // Check if the field exists in the array and is an array
            if (isset($array['billing_summary'][$field]) && is_array($array['billing_summary'][$field])) {
                foreach ($array['billing_summary'][$field] as $item) {
                    $html .= '<tr>';
                    $html .= '<td style="border-radius: 10px;">';
                    $html .= '<span  class="" style="color: white; background-color: ' . $item['bgcolor'] . '; display: inline-block; padding: 3px; border-radius: 5px;">' . $item['label'] . '</span>';
                    $html .= '</td>';
                    $html .= '<td class="count" style="text-align: right;">' . $item['count'] . ' <span class="suitepicon suitepicon-admin-user-management"></span></td>';
                    $html .= '<td class="format_currency" style="text-align: right;">' . number_format($item['amount']) . ' </td>';
                    $html .= '</tr>';
                }

                // Calculate total count and total amount
                $totalCount = 0;
                $totalAmount = 0;
                foreach ($array['billing_summary'][$field] as $item) {
                    $totalCount += $item['count'];
                    $totalAmount += $item['amount'];
                }

                // Add total row
                if ($totalCount != 0) {
                    $html .= '<tr>';
                    $html .= '<td style="color: blue; font-size: 16px; font-weight: bold;">'.$mod_strings['LBL_TOTAL'].'</td>';
                    $html .= '<td class="count" style="text-align: right; color: blue; font-size: 16px;">' . $totalCount . ' <span class="suitepicon suitepicon-admin-user-management"></span></td>';
                    if ($totalAmount != 0) {
                        $html .= '<td class="format_currency" style="text-align: right; color: blue; font-size: 16px;">' . number_format($totalAmount) . '</td>';
                    }
                    $html .= '</tr>';
                }

            }

            $html .= '</table>';
            $html .= '</td>';
        }


        foreach ($fields as $field) {
            $html .= '<td class="custom-column">';
            $html .= '<table class="custom-inner-table">';

            $html .= '<tr>';
            $html .= '<td colspan="3"  class="font15"style="background-color: #CD181818 ;padding: 1px; font-weight: bold;">' . $mod_strings[$field] . '</td>';
            $html .= '</tr>';

            $totalCount = 0;
            $totalAmount = 0;

            if (isset($array[$field]) && is_array($array[$field])) {
                foreach ($array[$field] as $item) {
                    $html .= '<tr>';
                    $columnCount = 0; // Số cột đã thêm vào trong mỗi hàng
                    foreach ($item as $itemKey => $itemValue) {
                        if ($itemKey == 'label') {
                            $html .= '<td style="border-radius: 10px;">';
                            $html .= '<span class="" style="color: white;background-color: ' . $item['bgcolor'] . '; display: inline-block; padding: 3px; border-radius: 5px;">' . $itemValue . '</span>';
                            $html .= '</td>';
                            $columnCount++;
                        }

                        if ($itemKey == 'count') {
                            $html .= '<td class="count" style="padding: 1px; text-align: right;">' . $itemValue . ' <span class="suitepicon suitepicon-admin-user-management"></span></td>';
                            $columnCount++;
                            $totalCount += $itemValue; // Tính '.$mod_strings['LBL_TOTAL'].' count
                        }

                        if ($itemKey == 'amount') {
                            $html .= '<td class="format_currency" style="padding: 1px; text-align: right;">' . number_format($itemValue) . ' </td>';
                            $columnCount++;
                            $totalAmount += $itemValue; // Tính '.$mod_strings['LBL_TOTAL'].' amount
                        }
                    }

                    // Nếu số cột chưa đạt đến 3, thêm các cột rỗng
                    while ($columnCount < 3) {
                        $html .= '<td></td>';
                        $columnCount++;
                    }

                    $html .= '</tr>';
                }
            }

// Thêm hàng "'.$mod_strings['LBL_TOTAL'].'" nếu $totalCount khác 0
            if ($totalCount != 0) {
                $html .= '<tr>';
                $html .= '<td style="color: blue; font-size: 16px"><strong>'.$mod_strings['LBL_TOTAL'].'</strong></td>';
                $html .= '<td class="count" style="padding: 1px; text-align: right; color: blue;font-size: 16px">' . $totalCount . ' <span class="suitepicon suitepicon-admin-user-management"></span></td>';
                if ($totalAmount != 0) {
                    $html .= '<td class="format_currency" style="padding: 1px; text-align: right; color: blue;font-size: 16px">' . number_format($totalAmount) . '</td>';
                }
                $html .= '</tr>';
            }


            $html .= '</table>';
            $html .= '</td>';
        }


// Đóng thẻ trước khi kết thúc
        $html .= '</table>';
        $html.='<div class="circle" id="circle">
                <div class="loader"></div>
            </div>';


        return $html;
    }

}

// function restRequest($method, $arguments)
// {

//          if (file_exists(getcwd() . '/config.php')) {
//                 require(getcwd() . '/config.php');
//             }
//             global $sugar_config;

//   $url = $sugar_config['site_url'] . '/custom/service/v4_1_custom/rest.php';
//   //          $url = 'https://dev.crmonline.vn/custom/service/v4_1_custom/rest.php';

//     $curl = curl_init($url);
//     curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
//     $post = [
//         "method" => $method,
//         "input_type" => "JSON",
//         "response_type" => "JSON",
//         "rest_data" => json_encode($arguments),
//     ];

//     curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
//     curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
//     curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

//     $result = curl_exec($curl);
//     curl_close($curl);
//     return json_decode($result, 1);
// }

function summary_choostime($focus, $field, $value, $view){

    global $mod_strings, $db;

    $arr = array();
    $user = BeanFactory::newBean('cs_cusUser');

    $query = $user->create_new_list_query("cs_cususer.date_entered DESC","cs_cususer.status = 'Active'",array());

    $res = $user->db->query($query);

    $html = "";

    $html .= '<style>

        #period2 option {
            font-size: 16px;
        }

        tr td {
            padding: 5px;
        }
        .circle {
            display: none;
            position: fixed;
            top: 48%;
            left: 50%;
            transform: translate(-50%, -50%);
        }

        .loader {
            border: 16px solid white; /* Màu nền của spinner */
            border-top: 16px solid blue; /* Màu của phần spinner */
            border-radius: 50%;
            width: 3px;
            height: 30px;
            animation: spin 0.4s linear infinite; /* Sử dụng animation có tên "spin" */
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg); /* Góc bắt đầu là 0 độ */
            }
            100% {
                transform: rotate(360deg); /* Góc kết thúc là 360 độ */
            }
        }
    </style>';

    // Lấy giá trị đã chọn từ $_POST
    $selectedOption = isset($_POST['period2']) ? $_POST['period2'] : 'default';
    $dateFromValue = isset($_POST['date_from']) ? htmlspecialchars($_POST['date_from']) : '';
    $dateToValue = isset($_POST['date_to']) ? htmlspecialchars($_POST['date_to']) : '';

    $dateFromValue = !empty($_POST['date_from']) ? htmlspecialchars($_POST['date_from']) : date('d/m/Y', strtotime('monday this week'));
    $dateToValue = !empty($_POST['date_to']) ? htmlspecialchars($_POST['date_to']) : date('d/m/Y', strtotime('sunday this week'));


    $html .= '<div id="cde" style="margin-top:20px;">';
    $html .= '<div class="row" style="margin-bottom: 5px">';
    $html .= '<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 text-left" >';

    $html .= '<form method="post" action="">';

    $html .= '<table>'; //
    $html .= '  <tr>';
    $html .= '    <td>';
    $html .= '<select style="width:180px; font-size: 16px;" name="period2" id="period2" onchange="periodChange2()">';

    $html .= '<option value="this_week" ' . ($selectedOption === 'this_week' ? 'selected' : '') . '>'.$mod_strings['LBL_THISWEEK'].'</option>';
    $html .= '<option value="last_week" ' . ($selectedOption === 'last_week' ? 'selected' : '') . '>'.$mod_strings['LBL_LASTWEEK'].'</option>';
    $html .= '<option value="this_month" ' . ($selectedOption === 'this_month' ? 'selected' : '') . '>'.$mod_strings['LBL_THISMONTH'].'</option>';
    $html .= '<option value="last_month" ' . ($selectedOption === 'last_month' ? 'selected' : '') . '>'.$mod_strings['LBL_LASTMONTH'].'</option>';
    $html .= '<option value="this_year" ' . ($selectedOption === 'this_year' ? 'selected' : '') . '>'.$mod_strings['LBL_THISYEAR'].'</option>';
    $html .= '<option value="last_year" ' . ($selectedOption === 'last_year' ? 'selected' : '') . '>'.$mod_strings['LBL_LASTYEAR'].'</option>';
    $html .= '<option value="is_between" ' . ($selectedOption === 'is_between' ? 'selected' : '') . '>'.$mod_strings['LBL_IS_BETWEEN'].'</option>';
    $html .= '</select>';
    $html .= '    </td>';


    $html .= '    <td>';
    $html .= '      <label>'.$mod_strings['LBL_FROM'].'</label>';
    $html .= '    </td>';
    $html .= '    <td>';
    $html .= '      <input id="date_from" name="date_from" type="text" maxlength="100" value="' . $dateFromValue . '" title="" tabindex="-1" accesskey="9" style="width: 200px; height: 30px; font-size: 16px; font-weight: bold;">';
    $html .= '    </td>';

    $html .= '    <td>';
    $html .= '      <label>'.$mod_strings['LBL_TO'].'&nbsp;&nbsp;</label>';
    $html .= '    </td>';
    $html .= '    <td>';
    $html .= '      <input type="text" id="date_to" name="date_to" maxlength="100" value="' . $dateToValue . '" title="" tabindex="-1" accesskey="9" style="width: 200px; height: 30px; font-size: 16px; font-weight: bold;">';
    $html .= '    </td>';


    $html .= '<td>
                <select name="users[]" multiple style="height: 150px; width: 180px; font-size: 15px;">';
    while($row = $user->db->fetchByAssoc($res)){
        $selected = (isset($_POST['users']) && in_array($row['refuser'], $_POST['users'])) ? 'selected' : '';
        $html .= '<option value="' . $row['refuser'] . '" ' . $selected . '>' . $row['name'] . '</option>';
    }
    $html .= '</select>
              </td>';



    $html .= '<td>'; // Căn phải cho ô
    $html .= '<input type="submit" value="'.$mod_strings['LBL_STATISTICAL'].'" name="submit" id="sm_all">';
    $html .= '</td>';
    $html .= '<td>';
    $html .='<input type="button" value="'.$mod_strings['LBL_CLEAR'].'" name="clear_filter" id="clear_filter" style="margin-top: 4px;">';
    $html .= '</td>';

    $html .= '  </tr>';
    $html .= '</table>';

    $html .= '</form>';

    $html .= '</div>';
    $html .= '</div>';
    $html .= '</div>';

    return $html;
}
