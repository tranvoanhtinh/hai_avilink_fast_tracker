<?php
//bỏ qua session
use Reports\Filter\Conditions\Condition;

if (!defined('sugarEntry')) {
    define('sugarEntry', true);
}
require_once('service/v4_1/SugarWebServiceImplv4_1.php');
require_once('custom/service/v4_1_custom/SugarWebServiceUtilv4_1_custom.php');

class Get_data_summary extends SugarWebServiceImplv4_1
{
    public function __construct()
    {
        self::$helperObject = new SugarWebServiceUtilv4_1_custom();
    }

    function summary_for_dashboard($session, $from_date = '', $to_date = '', $assigned_user_id = [])
    {
        if (!empty($assigned_user_id)) {
            $assigned_user_id = '("' . implode('","', $assigned_user_id) . '")';
        }
        $GLOBALS['log']->info('Begin: SugarWebServiceImplv4_1_custom->summary_for_dashboard');
        global $db, $app_list_strings;



        if (empty($from_date)) {
            $from_date = date('2000-01-01');
        }
        if (empty($to_date)) {
            $to_date = date('Y-m-d');
        }
        // Lấy cấu hình màu nền
        $bgcolor_qr = $db->query('SELECT type_language, key_language, `name`
            FROM lp_language_mobile_app
            WHERE deleted = 0 AND `type` = 2');
        $bgcolor_data = [];
        while ($row = $db->fetchByAssoc($bgcolor_qr)) {
            $bgcolor_data[$row['key_language']] = $row['name'];
        }
        // Lấy cấu hình màu chữ
        $textcolor_qr = $db->query('SELECT type_language, key_language, `name`
            FROM lp_language_mobile_app
            WHERE deleted = 0 AND `type` = 3');
        $textcolor_data = [];
        while ($row = $db->fetchByAssoc($textcolor_qr)) {
            $textcolor_data[$row['key_language']] = $row['name'];
        }
        // Thống kê tiềm năng, đang liên hệ (module liên quan: Leads)
        $owner_where = self::$helperObject->gen_sql_where_permission('Leads');
        $date_range_where = self::$helperObject->gen_sql_where_date_range('leads.date_entered', $from_date, $to_date);
        $cus_where = $owner_where . ' AND ' . $date_range_where;
        $in_process_status = ['In Process'];
        $new_status = ['New'];
        $dead_status = ['Dead'];
        if (!empty($assigned_user_id)) {
            $cus_where .= ' AND leads.assigned_user_id IN ' . $assigned_user_id;
        }
        $leads_amount_qr = $db->query('SELECT status, SUM(opportunity_amount) AS total_opportunity_amount, COUNT(id) AS num_lead 
            FROM leads 
            WHERE deleted = 0 AND ' . $cus_where . ' GROUP BY status');
        $new_amount = $in_process_amount = $dead_amount = 0;
        $new_count = $in_process_count = $dead_count = 0;
        while ($row = $db->fetchByAssoc($leads_amount_qr)) {
            if (in_array($row['status'], $in_process_status)) {
                $in_process_amount += $row['total_opportunity_amount'];
                $in_process_count += $row['num_lead'];
            } elseif (in_array($row['status'], $new_status)) {
                $new_amount += $row['total_opportunity_amount'];
                $new_count += $row['num_lead'];
            } elseif (in_array($row['status'], $dead_status)) {
                $dead_amount += $row['total_opportunity_amount'];
                $dead_count += $row['num_lead'];
            }
        }
        // Thống kê phát sinh nhu cầu và đã mua hàng (module liên quan: Accounts và Opportunities)
        $owner_where = self::$helperObject->gen_sql_where_permission('Accounts');
        $date_range_where = self::$helperObject->gen_sql_where_date_range('accounts.date_entered', $from_date, $to_date);
        $cus_where = $owner_where . ' AND ' . $date_range_where;
        if (!empty($assigned_user_id)) {
            $cus_where .= ' AND accounts.assigned_user_id IN ' . $assigned_user_id;
        }
        $account_with_op_status = ['Converted'];
        $account_with_transaction_status = ['Transaction'];
        $accounts_count_qr = $db->query('SELECT ac.lead_account_status_c, COUNT(accounts.id) AS num_account 
            FROM accounts INNER JOIN accounts_cstm ac ON accounts.id = ac.id_c 
            WHERE accounts.deleted = 0 AND ' . $cus_where . ' GROUP BY ac.lead_account_status_c');
        $account_with_op_count = $account_with_transaction_count = 0;
        while ($row = $db->fetchByAssoc($accounts_count_qr)) {
            if (in_array($row['lead_account_status_c'], $account_with_op_status)) {
                $account_with_op_count += $row['num_account'];
            }
            if (in_array($row['lead_account_status_c'], $account_with_transaction_status)) {
                $account_with_transaction_count += $row['num_account'];
            }
        }
        $acc_op_amount_status = ['Negotiation/Review', 'Proposal/Price Quote', 'Prospecting'];
        $op_won_status = ['Closed Won'];
        $acc_op_amount_qr = $db->query('SELECT op.sales_stage, SUM(op.amount) AS total_amount
            FROM accounts INNER JOIN accounts_opportunities aop ON accounts.id = aop.account_id AND aop.deleted = 0
            INNER JOIN opportunities op ON op.id = aop.opportunity_id
            WHERE accounts.deleted = 0 AND ' . $cus_where . ' AND op.deleted = 0
            GROUP BY op.sales_stage');
        $acc_op_amount = $acc_op_won_amount = 0;
        while ($row = $db->fetchByAssoc($acc_op_amount_qr)) {
            if (in_array($row['sales_stage'], $acc_op_amount_status)) {
                $acc_op_amount += $row['total_amount'];
            }
            if (in_array($row['sales_stage'], $op_won_status)) {
                $acc_op_won_amount += $row['total_amount'];
            }
        }
        // Thống kê cơ hội bán hàng (module liên quan: Opportunities)
        $owner_where = self::$helperObject->gen_sql_where_permission('Opportunities');
        $date_range_where = self::$helperObject->gen_sql_where_date_range('date_closed', $from_date, $to_date);
        $cus_where = $owner_where . ' AND ' . $date_range_where;
        if (!empty($assigned_user_id)) {
            $cus_where .= ' AND opportunities.assigned_user_id IN ' . $assigned_user_id;
        }
        $op_amount_qr = $db->query('SELECT sales_stage, COUNT(id) AS num, SUM(amount) AS total_amount
            FROM opportunities
            WHERE deleted = 0 AND ' . $cus_where . '
            GROUP BY sales_stage');
        $sales_opportunity_summary = [];
        while ($row = $db->fetchByAssoc($op_amount_qr)) {
            if (empty($row['sales_stage'])) {
                continue;
            }
            $sales_opportunity_summary[] = [
                'label' => isset($app_list_strings['sales_stage_dom'][$row['sales_stage']]) ? $app_list_strings['sales_stage_dom'][$row['sales_stage']] : '',
                'value' => $row['sales_stage'],
                'bgcolor' => !empty($bgcolor_data[$row['sales_stage']]) ? $bgcolor_data[$row['sales_stage']] : '#ffffff',
                'txtcolor' => !empty($textcolor_data[$row['sales_stage']]) ? $textcolor_data[$row['sales_stage']] : '#000000',
                'count' => $row['num'],
                'amount' => $row['total_amount'],
            ];
        }
        array_multisort($sales_opportunity_summary);
        // Thống kê đơn hàng
        $owner_where = self::$helperObject->gen_sql_where_permission('AOS_Invoices');
        $date_range_where = self::$helperObject->gen_sql_where_date_range('invoice_date', $from_date, $to_date);
        $cus_where = $owner_where . ' AND ' . $date_range_where;
        if (!empty($assigned_user_id)) {
            $cus_where .= ' AND aos_invoices.assigned_user_id IN ' . $assigned_user_id;
        }
        $order_summary_qr = $db->query('SELECT `status`, COUNT(id) AS num, SUM(total_amount) AS total_amount
            FROM aos_invoices
            WHERE deleted = 0 AND ' . $cus_where . '
            GROUP BY `status`');
        $order_summary = [];
        while ($row = $db->fetchByAssoc($order_summary_qr)) {
            if (empty($row['status'])) {
                continue;
            }
            $order_summary[] = [
                'label' => isset($app_list_strings['invoice_status_dom'][$row['status']]) ? $app_list_strings['invoice_status_dom'][$row['status']] : '',
                'value' => $row['status'],
                'bgcolor' => !empty($bgcolor_data[$row['status']]) ? $bgcolor_data[$row['status']] : '#ffffff',
                'txtcolor' => !empty($textcolor_data[$row['status']]) ? $textcolor_data[$row['status']] : '#000000',
                'count' => $row['num'],
                'amount' => $row['total_amount'],
            ];
        }
        array_multisort($order_summary);
        // Thống kê hợp đồng
        $owner_where = self::$helperObject->gen_sql_where_permission('AOS_Contracts');
        $date_range_where = self::$helperObject->gen_sql_where_date_range('start_date', $from_date, $to_date);
        $cus_where = $owner_where . ' AND ' . $date_range_where;
        if (!empty($assigned_user_id)) {
            $cus_where .= ' AND aos_contracts.assigned_user_id IN ' . $assigned_user_id;
        }
        $contract_summary = [];
        $contract_summary_qr = $db->query('SELECT `status`, COUNT(id) AS num, SUM(total_contract_value) AS total_amount
            FROM aos_contracts
            WHERE deleted = 0 AND ' . $cus_where . '
            GROUP BY `status`');
        while ($row = $db->fetchByAssoc($contract_summary_qr)) {
            if (empty($row['status'])) {
                continue;
            }
            $contract_summary[] = [
                'label' => isset($app_list_strings['contract_status_list'][$row['status']]) ? $app_list_strings['contract_status_list'][$row['status']] : '',
                'value' => $row['status'],
                'bgcolor' => !empty($bgcolor_data[$row['status']]) ? $bgcolor_data[$row['status']] : '#ffffff',
                'txtcolor' => !empty($textcolor_data[$row['status']]) ? $textcolor_data[$row['status']] : '#000000',
                'count' => $row['num'],
                'amount' => $row['total_amount'],
            ];
        }
        array_multisort($contract_summary);
        // Thống kê báo giá
        $owner_where = self::$helperObject->gen_sql_where_permission('AOS_Quotes');
        $date_range_where = self::$helperObject->gen_sql_where_date_range('datequote_c', $from_date, $to_date);
        $cus_where = $owner_where . ' AND ' . $date_range_where;
        if (!empty($assigned_user_id)) {
            $cus_where .= ' AND aos_quotes.assigned_user_id IN ' . $assigned_user_id;
        }
        $quotation_summary = [];
        $quotation_summary_qr = $db->query('SELECT stage, COUNT(id) AS num, SUM(total_amount) AS total_amount
            FROM aos_quotes
            INNER JOIN aos_quotes_cstm ON aos_quotes_cstm.id_c = aos_quotes.id
            WHERE deleted = 0 AND ' . $cus_where . '
            GROUP BY stage');
        while ($row = $db->fetchByAssoc($quotation_summary_qr)) {
            if (empty($row['stage'])) {
                continue;
            }
            $quotation_summary[] = [
                'label' => isset($app_list_strings['quote_stage_dom'][$row['stage']]) ? $app_list_strings['quote_stage_dom'][$row['stage']] : '',
                'value' => $row['stage'],
                'bgcolor' => !empty($bgcolor_data[$row['stage']]) ? $bgcolor_data[$row['stage']] : '#ffffff',
                'txtcolor' => !empty($textcolor_data[$row['stage']]) ? $textcolor_data[$row['stage']] : '#000000',
                'count' => $row['num'],
                'amount' => $row['total_amount'],
            ];
        }
        array_multisort($quotation_summary);
        // Thống kê cuộc gọi
        $owner_where = self::$helperObject->gen_sql_where_permission('Calls');
        $date_range_where = self::$helperObject->gen_sql_where_date_range('date_start', $from_date, $to_date);
        $cus_where = $owner_where . ' AND ' . $date_range_where;
        if (!empty($assigned_user_id)) {
            $cus_where .= ' AND calls.assigned_user_id IN ' . $assigned_user_id;
        }
        $call_summary = [];
        $call_summary_qr = $db->query('SELECT `status`, COUNT(id) AS num
            FROM calls
            WHERE deleted = 0 AND ' . $cus_where . '
            GROUP BY `status`');
        while ($row = $db->fetchByAssoc($call_summary_qr)) {
            if (empty($row['status'])) {
                continue;
            }
            $call_summary[] = [
                'label' => isset($app_list_strings['call_status_dom'][$row['status']]) ? $app_list_strings['call_status_dom'][$row['status']] : '',
                'value' => $row['status'],
                'bgcolor' => !empty($bgcolor_data[$row['status']]) ? $bgcolor_data[$row['status']] : '#ffffff',
                'txtcolor' => !empty($textcolor_data[$row['status']]) ? $textcolor_data[$row['status']] : '#000000',
                'count' => $row['num'],
            ];
        }
        array_multisort($call_summary);
        // Thống kê cuộc gặp
        $owner_where = self::$helperObject->gen_sql_where_permission('Meetings');
        $date_range_where = self::$helperObject->gen_sql_where_date_range('date_start', $from_date, $to_date);
        $cus_where = $owner_where . ' AND ' . $date_range_where;
        if (!empty($assigned_user_id)) {
            $cus_where .= ' AND meetings.assigned_user_id IN ' . $assigned_user_id;
        }
        $meet_summary = [];
        $meet_summary_qr = $db->query('SELECT `status`, COUNT(id) AS num
            FROM meetings
            WHERE deleted = 0 AND ' . $cus_where . '
            GROUP BY `status`');
        while ($row = $db->fetchByAssoc($meet_summary_qr)) {
            if (empty($row['status'])) {
                continue;
            }
            $meet_summary[] = [
                'label' => isset($app_list_strings['meeting_status_dom'][$row['status']]) ? $app_list_strings['meeting_status_dom'][$row['status']] : '',
                'value' => $row['status'],
                'bgcolor' => !empty($bgcolor_data[$row['status']]) ? $bgcolor_data[$row['status']] : '#ffffff',
                'txtcolor' => !empty($textcolor_data[$row['status']]) ? $textcolor_data[$row['status']] : '#000000',
                'count' => $row['num'],
            ];
        }
        array_multisort($meet_summary);
        // Thống kê công việc
        $owner_where = self::$helperObject->gen_sql_where_permission('Tasks');
        $date_range_where = self::$helperObject->gen_sql_where_date_range('date_start', $from_date, $to_date);
        $cus_where = $owner_where . ' AND ' . $date_range_where;
        if (!empty($assigned_user_id)) {
            $cus_where .= ' AND tasks.assigned_user_id IN ' . $assigned_user_id;
        }
        $task_summary = [];
        $task_summary_qr = $db->query('SELECT `status`, COUNT(id) AS num
            FROM tasks
            WHERE deleted = 0 AND ' . $cus_where . '
            GROUP BY `status`');
        while ($row = $db->fetchByAssoc($task_summary_qr)) {
            if (empty($row['status'])) {
                continue;
            }
            $task_summary[] = [
                'label' => isset($app_list_strings['task_status_dom'][$row['status']]) ? $app_list_strings['task_status_dom'][$row['status']] : '',
                'value' => $row['status'],
                'bgcolor' => !empty($bgcolor_data[$row['status']]) ? $bgcolor_data[$row['status']] : '#ffffff',
                'txtcolor' => !empty($textcolor_data[$row['status']]) ? $textcolor_data[$row['status']] : '#000000',
                'count' => $row['num'],
            ];
        }
        array_multisort($task_summary);
        // Thống kê ticket
        $owner_where = self::$helperObject->gen_sql_where_permission('Cases');
        $date_range_where = self::$helperObject->gen_sql_where_date_range('date_entered', $from_date, $to_date);
        $cus_where = $owner_where . ' AND ' . $date_range_where;
        if (!empty($assigned_user_id)) {
            $cus_where .= ' AND cases.assigned_user_id IN ' . $assigned_user_id;
        }
        $ticket_summary = [];
        $ticket_summary_qr = $db->query('SELECT `state`, COUNT(id) AS num
            FROM cases
            WHERE deleted = 0 AND ' . $cus_where . '
            GROUP BY `state`');
        while ($row = $db->fetchByAssoc($ticket_summary_qr)) {
            if (empty($row['state'])) {
                continue;
            }
            $ticket_summary[] = [
                'label' => isset($app_list_strings['case_state_dom'][$row['state']]) ? $app_list_strings['case_state_dom'][$row['state']] : '',
                'value' => $row['state'],
                'bgcolor' => !empty($bgcolor_data[$row['state']]) ? $bgcolor_data[$row['state']] : '#ffffff',
                'txtcolor' => !empty($textcolor_data[$row['state']]) ? $textcolor_data[$row['state']] : '#000000',
                'count' => $row['num'],
            ];
        }
        array_multisort($ticket_summary);
        // Thống kê thanh toán
        $owner_where = self::$helperObject->gen_sql_where_permission('bill_Billing');
        $date_range_where = self::$helperObject->gen_sql_where_date_range('billingdate', $from_date, $to_date);
        $cus_where = $owner_where . ' AND ' . $date_range_where;
        if (!empty($assigned_user_id)) {
            $cus_where .= ' AND bill_billing.assigned_user_id IN ' . $assigned_user_id;
        }
        $billing_3_summary = $billing_payment_summary = $billing_receipts_summary = [];
        $billing_summary_qr = $db->query('SELECT `type`, billing_status, COUNT(id) AS num, SUM(billingamount) AS total_amount
            FROM bill_billing
            WHERE deleted = 0 AND ' . $cus_where . '
            GROUP BY `type`, billing_status');
        while ($row = $db->fetchByAssoc($billing_summary_qr)) {
            if (empty($row['type'])) {
                continue;
            }
            $tmp = [
                'label' => isset($app_list_strings['billing_status_list'][$row['billing_status']]) ? $app_list_strings['billing_status_list'][$row['billing_status']] : '',
                'value' => $row['state'],
                'bgcolor' => !empty($bgcolor_data[$row['billing_status']]) ? $bgcolor_data[$row['billing_status']] : '#ffffff',
                'txtcolor' => !empty($textcolor_data[$row['billing_status']]) ? $textcolor_data[$row['billing_status']] : '#000000',
                'count' => $row['num'],
                'amount' => $row['total_amount'],
            ];
            if ($row['type'] == 'payment') {
                $billing_payment_summary[] = $tmp;
            } elseif ($row['type'] == 'receipts') {
                $billing_receipts_summary[] = $tmp;
            } elseif ($row['type'] == '3') {
                $billing_3_summary[] = $tmp;
            }
        }
        array_multisort($billing_3_summary);
        array_multisort($billing_payment_summary);
        array_multisort($billing_receipts_summary);
        $billing_summary = [
            'billing_3_summary' => $billing_3_summary,
            'billing_payment_summary' => $billing_payment_summary,
            'billing_receipts_summary' => $billing_receipts_summary,
        ];
        $GLOBALS['log']->info('End: SugarWebServiceImplv4_1_custom->summary_for_dashboard - SUCCESS');
        $rt = [
            'num_new_lead' => $new_count,
            'new_lead_amount' => $new_amount,
            'num_in_process_lead' => $in_process_count,
            'in_process_lead_amount' => $in_process_amount,
            'num_dead_lead' => $dead_count,
            'dead_lead_amount' => $dead_amount,
            'num_account' => $account_with_op_count,
            'account_opportunity_amount' => $acc_op_amount,
            'num_account_has_transaction' => $account_with_transaction_count,
            'account_opportunity_won_amount' => $acc_op_won_amount,
            'sales_opportunity_summary' => $sales_opportunity_summary,
            'order_summary' => $order_summary,
            'billing_summary' => $billing_summary,
            'quotation_summary' => $quotation_summary,
            'contract_summary' => $contract_summary,
            'call_summary' => $call_summary,
            'meet_summary' => $meet_summary,
            'task_summary' => $task_summary,
            'ticket_summary' => $ticket_summary,
        ];
        return $rt;
    }

    function check_user_login_app($username, $siteurl)
    {
        $GLOBALS['log']->info('Begin: SugarWebServiceImplv4_1_custom->check_user_login_app');
        global  $db;
        $data = $db->fetchOne('SELECT siteurl
        FROM mba_mobileappcontrol
        WHERE name = "' . $username . '" AND deleted = 0 AND STATUS = 1 AND (siteurl = "http://' . $siteurl . '" OR siteurl = "https://' . $siteurl . '")');
        $rt_siteurl = '';
        $valid = false;
        if (!empty($data['siteurl'])) {
            $rt_siteurl = $data['siteurl'];
            $valid = true;
        }
        $GLOBALS['log']->info('End: SugarWebServiceImplv4_1_custom->check_user_login_app - SUCCESS');
        return [
            'valid' => $valid,
            'siteurl' => $rt_siteurl,
        ];
    }

    function get_data_for_list($session, $list_key)
    {
        $GLOBALS['log']->info('Begin: SugarWebServiceImplv4_1_custom->get_data_for_list');
        global $app_list_strings;
        $GLOBALS['log']->info('End: SugarWebServiceImplv4_1_custom->get_data_for_list - SUCCESS');
        return isset($app_list_strings[$list_key]) ? $app_list_strings[$list_key] : [];
    }

    function get_language_for_app($type_language)
    {
        $GLOBALS['log']->info('Begin: SugarWebServiceImplv4_1_custom->get_language_for_app');
        global  $db;
        $language_qr = $db->query('SELECT type_language, key_language, `name`
            FROM lp_language_mobile_app
            WHERE deleted = 0 AND `type` = 1 AND type_language = "' . $type_language . '"');
        $language_data = [];
        while ($row = $db->fetchByAssoc($language_qr)) {
            $language_data[$row['key_language']] = $row['name'];
        }
        $GLOBALS['log']->info('End: SugarWebServiceImplv4_1_custom->get_language_for_app - SUCCESS');
        return $language_data;
    }

    function check_duplicate_phone_mobile($phone_mobile, $old_phone_mobile = null)
    {
        $GLOBALS['log']->info('Begin: SugarWebServiceImplv4_1_custom->check_duplicate_phone_mobile');
        $phone_mobile = trim($phone_mobile);
        $old_phone_mobile = trim($old_phone_mobile);
        global  $db;
        $more_where_1 = $more_where_2 = '';
        if (!empty($old_phone_mobile)) {
            $more_where_1 .= ' AND phone_mobile <> "' . $old_phone_mobile . '"';
            $more_where_2 .= ' AND phone_alternate <> "' . $old_phone_mobile . '"';
        }
        $duplicate = false;
        $data = $db->fetchOne('SELECT id FROM leads WHERE deleted = 0 AND phone_mobile = "' . $phone_mobile . '"' . $more_where_1);
        if (empty($data['id'])) {
            $data = $db->fetchOne('SELECT id FROM accounts WHERE deleted = 0 AND phone_alternate = "' . $phone_mobile . '"' . $more_where_2);
            if (empty($data['id'])) {
                $data = $db->fetchOne('SELECT id FROM contacts WHERE deleted = 0 AND phone_mobile = "' . $phone_mobile . '"' . $more_where_1);
                if (empty($data['id'])) {
                    $data = $db->fetchOne('SELECT id FROM prospects WHERE deleted = 0 AND phone_mobile = "' . $phone_mobile . '"' . $more_where_1);
                    if (!empty($data['id'])) {
                        $duplicate = true;
                    }
                } else {
                    $duplicate = true;
                }
            } else {
                $duplicate = true;
            }
        } else {
            $duplicate = true;
        }
        $GLOBALS['log']->info('End: SugarWebServiceImplv4_1_custom->check_duplicate_phone_mobile - SUCCESS');
        return [
            'duplicate' => $duplicate,
            'phone_mobile' => $phone_mobile,
        ];
    }

    function check_duplicate_email($email, $old_email)
    {
        $GLOBALS['log']->info('Begin: SugarWebServiceImplv4_1_custom->check_duplicate_email');
        $email = trim($email);
        $old_email = trim($old_email);
        global $db;
        $more_where = '';
        if (!empty($old_email)) {
            $more_where .= ' AND email_addresses.email_address <> "' . $old_email . '"';
        }
        $duplicate = false;
        $module = '';
        $bean_id = '';
        $data = $db->fetchOne('SELECT email_addr_bean_rel.bean_module, email_addr_bean_rel.bean_id, email_addresses.email_address
            FROM email_addresses
            INNER JOIN email_addr_bean_rel ON email_addresses.id=email_addr_bean_rel.email_address_id
            WHERE email_addr_bean_rel.deleted = 0 AND email_addresses.deleted = 0 AND email_addr_bean_rel.bean_module IN ("Leads", "Accounts", "Contacts", "Prospects") AND email_addresses.email_address = "' . $email . '"' . $more_where);
        if (!empty($data['email_address'])) {
            $duplicate = true;
            $module = $data['bean_module'];
            $bean_id = $data['bean_id'];
        }
        $GLOBALS['log']->info('End: SugarWebServiceImplv4_1_custom->check_duplicate_email - SUCCESS');
        return [
            'duplicate' => $duplicate,
            'email' => $email,
            'module' => $module,
            'bean_id' => $bean_id,
        ];
    }
}
