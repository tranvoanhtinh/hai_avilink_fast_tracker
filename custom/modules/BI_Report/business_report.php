<?php
/**
 * Advanced OpenSales, Advanced, robust set of sales modules.
 * @package Advanced OpenSales for SugarCRM
 * @copyright SalesAgility Ltd http://www.salesagility.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU AFFERO GENERAL PUBLIC LICENSE
 * along with this program; if not, see http://www.gnu.org/licenses
 * or write to the Free Software Foundation,Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA 02110-1301  USA
 *
 * @author SalesAgility <info@salesagility.com>
 */
require_once('custom/modules/BI_Report/summary_report_PI.php');
require_once('custom/modules/BI_Report/inventory_report_PI.php');
require_once('custom/modules/BI_Report/booking_report_PI.php');
require_once('custom/modules/BI_Report/booking_report_nv_PI.php');
function business_report ($focus, $field, $value, $view)

{

   $sql = "SELECT * FROM bi_report WHERE id = '" . $focus->id . "' AND deleted = 0";
    $result = $focus->db->query($sql);

    if ($result && $result->num_rows > 0) {

        $row = $result->fetch_assoc();

        $rp = $row['record_rp'];
        $record_rp_list = $GLOBALS['app_list_strings']['record_rp_list'][$rp];

    }
    $html ='';

    if (trim($record_rp_list) == 'summary_report_PI') {
        $html .= summary_report_PI($focus, $field, $value, $view);
        $html .= '<script src="custom/modules/BI_Report/js/summary.js"></script>';
    }
    if (trim($record_rp_list) == 'inventory_report_PI') {
        $html .= inventory_report_PI($focus, $field, $value, $view);
        $html .= '<script src="custom/modules/BI_Report/js/inventory.js"></script>';
    }
    if (trim($record_rp_list) == 'booking_report_PI') {
        $html .= booking_report_PI($focus, $field, $value, $view);
        $html .= '<script src="custom/modules/BI_Report/js/booking.js"></script>';

    }
    if (trim($record_rp_list) == 'booking_report_nv_PI') {
        $html .= booking_report_nv_PI($focus, $field, $value, $view);
        $html .= '<script src="custom/modules/BI_Report/js/booking_nv.js"></script>';

    }
    $html .= '<script src="custom/modules/BI_Report/bi_report.js"></script>';
    $html .= '<script src="https://cdnjs.cloudflare.com/ajax/libs/exceljs/4.3.0/exceljs.min.js"></script>';
    $html .= '<script src="https://cdn.jsdelivr.net/npm/file-saver@2.0.2/dist/FileSaver.min.js"></script>';
    return $html;
}





function choose_time($focus, $field, $value, $view)
{

    if ($view == 'DetailView') {

        global $mod_strings, $db;
        $sql = "SELECT * FROM bi_report WHERE id = '" . $focus->id . "' AND deleted = 0";
        $result = $focus->db->query($sql);

        if ($result && $result->num_rows > 0) {

            $row = $result->fetch_assoc();

            $rp = $row['record_rp'];
            $record_rp_list = $GLOBALS['app_list_strings']['record_rp_list'][$rp];
          
        }
   

        $html ='';
        if (trim($record_rp_list) == 'summary_report_PI') {
            $html .= summary_choostime($focus, $field, $value, $view);
        }
        if (trim($record_rp_list) == 'inventory_report_PI') {
        $html .= inventory_choostime($focus, $field, $value, $view);
        }
        if (trim($record_rp_list) == 'booking_report_PI') {
        $html .= booking_choostime($focus, $field, $value, $view);
        }   
        if (trim($record_rp_list) == 'booking_report_nv_PI') {
        $html .= booking_nv_choostime($focus, $field, $value, $view);
        }        

    }

    return $html;
}



?>