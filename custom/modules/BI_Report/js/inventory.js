// Lấy ngày tháng năm hiện tại
var currentDate = new Date();
var currentYear = currentDate.getFullYear();
var currentMonth = currentDate.getMonth() + 1;
var currentDay = currentDate.getDate();
var lastDayOfMonth = new Date(currentYear, currentMonth, 0);
var firstDayOfMonth = new Date(currentYear, currentMonth - 1, 1);
var dateFromInput = document.getElementById("datefrom");
var dateToInput = document.getElementById("dateto");
var yearInput = document.getElementById("year");
var chooseMonth = document.getElementById("choose_month");
var warehouse = document.getElementById("warehouse");
var submitButton = document.getElementById("statistics");
var date = currentDay + '/' + currentMonth + '/' + currentYear;
yearInput.value = currentYear;
dateFromInput.value =  formatDate(firstDayOfMonth);
dateToInput.value = formatDate(lastDayOfMonth);
submitButton.disabled = true;
document.getElementById("choose_month").value = currentMonth;
document.getElementById("datefrom").disabled = true;
document.getElementById("dateto").disabled = true;
document.getElementById("year").disabled = true;
document.getElementById("choose_month").disabled = true;
document.getElementById("warehouse").disabled = true;

// Tắt các trường input khi selectedValue = -1
function disableInputs() {
    dateToInput.value = '';
    dateFromInput.value = '';
    yearInput.value = currentYear;
    chooseMonth.value = '';
    warehouse.value = '';
    submitButton.disabled = true;
    dateToInput.disabled = true;
    dateFromInput.disabled = true;
    yearInput.disabled = true;
    chooseMonth.disabled = true;
    warehouse.disabled = true;
}

// Kiểm tra giá trị selectedValue và xử lý các trường input tương ứng
function handleChange(selectElement) {
    var selectedValue = selectElement.value;
    
    if (selectedValue == '-1') {
        disableInputs();
    } else {
        submitButton.disabled = false;
        dateToInput.disabled = false;
        dateFromInput.disabled = false;
        yearInput.disabled = false;
        chooseMonth.disabled = false;
        warehouse.disabled = false;
    }

    // Xử lý khi selectedValue = 'month'
    if (selectedValue == 'month') {
        chooseMonth.value = currentMonth;
        dateFromInput.value = formatDate(firstDayOfMonth);
        dateToInput.value = formatDate(lastDayOfMonth);
        yearInput.value = currentYear;
        dateFromInput.readOnly = true;
        dateToInput.readOnly = true;

    }

    // Xử lý khi selectedValue = 'year'
    if (selectedValue == 'year') {
        chooseMonth.value = '';
        dateFromInput.value = '01/01/' + currentYear;
        dateToInput.value = '31/12/' + currentYear;
        yearInput.value = currentYear;
        chooseMonth.disabled = true;
        dateFromInput.readOnly = true;
        dateToInput.readOnly = true;
    }

    // Xử lý khi selectedValue = 'time_range'
    if (selectedValue == 'time_range') {
        chooseMonth.value = '';
        yearInput.value = '';
        chooseMonth.disabled = true;
        yearInput.disabled = true;
        dateFromInput.value = date;
        dateToInput.value = date;
        dateFromInput.readOnly = false;
        dateToInput.readOnly = false;

    }
}

// Hàm định dạng ngày tháng
function formatDate(date) {
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    return (day < 10 ? '0' : '') + day + '/' + (month < 10 ? '0' : '') + month + '/' + year;
}

// Hàm thay đổi năm
function setDate(year) {
    var datefrom = document.getElementById("datefrom").value;
    var dateto = document.getElementById("dateto").value;
    var datefromParts = datefrom.split("/");
    var datetoParts = dateto.split("/");
    datefromParts[2] = year;
    datetoParts[2] = year;
    document.getElementById("datefrom").value = datefromParts.join("/");
    document.getElementById("dateto").value = datetoParts.join("/");
}
function setDate2(month) {
    var datefrom = document.getElementById("datefrom");
    var dateto = document.getElementById("dateto");
    var year = document.getElementById("year").value;

    // Tạo ngày đầu tiên của tháng được chọn
    var firstDayOfMonth = new Date(year, month - 1, 1);
    var formattedFirstDay = formatDate(firstDayOfMonth);

    // Tạo ngày cuối cùng của tháng được chọn
    var lastDayOfMonth = new Date(year, month, 0);
    var formattedLastDay = formatDate(lastDayOfMonth);

    // Gán giá trị cho datefrom và dateto
    datefrom.value = formattedFirstDay;
    dateto.value = formattedLastDay;
    if(month ==''){
            datefrom.value = '';
            dateto.value = '';
            submitButton.disabled = true;

    }else {
        submitButton.disabled = false;
    }
}
function choosedate() {
    // Lấy giá trị được chọn từ dropdown
    var selectedOption = $("#time_filter").val();

    // Nếu chọn 'Khác', mở tab ngày cho người dùng
    if (selectedOption === "time_range") {
        // Gắn DatePicker cho input "từ ngày"
        $("#datefrom").datepicker({
            dateFormat: "dd/mm/yy", // Định dạng ngày tháng
            showButtonPanel: true, // Hiển thị nút chọn ngày tháng
            changeMonth: true, // Cho phép thay đổi tháng
            changeYear: true // Cho phép thay đổi năm
        });

        // Gắn DatePicker cho input "đến ngày"
        $("#dateto").datepicker({
            dateFormat: "dd/mm/yy", // Định dạng ngày tháng
            showButtonPanel: true, // Hiển thị nút chọn ngày tháng
            changeMonth: true, // Cho phép thay đổi tháng
            changeYear: true // Cho phép thay đổi năm
        });
    } else {
        // Tắt DatePicker cho input "từ ngày"
        $("#datefrom").datepicker("destroy");

        // Tắt DatePicker cho input "đến ngày"
        $("#dateto").datepicker("destroy");
    }
}

// Gọi hàm choosedate() khi người dùng thay đổi giá trị trong dropdown
$("#time_filter").change(function() {
    choosedate();
});

function submitForm() {
    var searchForm = document.getElementById('from_statistics');
    event.preventDefault();
    searchForm.submit();
}