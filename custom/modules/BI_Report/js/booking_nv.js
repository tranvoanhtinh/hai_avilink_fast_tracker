 var spanElement = document.getElementById('choose_time_span');
    spanElement.style.width = '100%';


jQuery(document).ready(function () {
  // Preloader
  var preloaderFadeOutTime = 500;

  function hidePreloader() {
    var preloader = jQuery(".spinner-wrapper");
    preloader.fadeOut(preloaderFadeOutTime);
  }

  hidePreloader();
});

document.getElementById("sm_all").addEventListener("click", function () {
  var circle = document.getElementById("circle");
  circle.style.display = "block";
});

document.getElementById("clear_filter").addEventListener("click", function () {
  var circle = document.getElementById("circle");
  circle.style.display = "block"; // Hiển thị vòng quay

  // Thiết lập thời gian trễ trước khi ẩn vòng quay
  setTimeout(function () {
    circle.style.display = "none"; // Ẩn vòng quay sau 0.5 giây
  }, 300); // 500 miligiây = 0.5 giây
});

// Find the <ul> element with both "nav" and "nav-tabs" classes
var navTabsUl = document.querySelector("ul.nav.nav-tabs");

// Check if the element exists
if (navTabsUl) {
  // Remove the <ul> element and its contents
  navTabsUl.parentNode.removeChild(navTabsUl);
}

document.getElementById("period2").addEventListener("change", function () {
  var selectedOption = this.value;
  var today = new Date();
  var firstDay, lastDay;

  switch (selectedOption) {
    case "this_week":
      firstDay = new Date(today);
      firstDay.setDate(today.getDate() - today.getDay() - 1);
      lastDay = new Date(today);
      lastDay.setDate(today.getDate() - today.getDay() + 7);
      break;
    case "last_week":
      firstDay = new Date(today);
      firstDay.setDate(today.getDate() - today.getDay() - 7);
      lastDay = new Date(today);
      lastDay.setDate(today.getDate() - today.getDay() - 1);
      break;
    case "this_month":
      firstDay = new Date(today.getFullYear(), today.getMonth(), 1);
      lastDay = new Date(today.getFullYear(), today.getMonth() + 1, 0);
      break;
    case "last_month":
      firstDay = new Date(today.getFullYear(), today.getMonth() - 1, 1);
      lastDay = new Date(today.getFullYear(), today.getMonth(), 0);
      break;
    case "this_year":
      firstDay = new Date(today.getFullYear(), 0, 1);
      lastDay = new Date(today.getFullYear(), 11, 31);
      break;
    case "last_year":
      firstDay = new Date(today.getFullYear() - 1, 0, 1);
      lastDay = new Date(today.getFullYear() - 1, 11, 31);
      break;
    case "is_between":
      firstDay = new Date(today.getFullYear(), today.getMonth(), 1);
      lastDay = new Date(today.getFullYear(), today.getMonth() + 1, 0);
      break;
  }

  // Format dates as 'dd/mm/yyyy'
  var formattedFirstDay = formatDate(firstDay);
  var formattedLastDay = formatDate(lastDay);

  // Set values to 'date_from' and 'date_to' inputs
  document.getElementById("date_from").value = formattedFirstDay;
  document.getElementById("date_to").value = formattedLastDay;


});

// Hàm chuyển định dạng ngày thành 'dd/mm/yyyy'
function formatDate(date) {
  var dd = date.getDate();
  var mm = date.getMonth() + 1; // Tháng bắt đầu từ 0
  var yyyy = date.getFullYear();

  if (dd < 10) {
    dd = "0" + dd;
  }

  if (mm < 10) {
    mm = "0" + mm;
  }

  return dd + "/" + mm + "/" + yyyy;
}

document.getElementById("period2").addEventListener("change", function () {
  var selectedOption = this.value;

  // Nếu chọn 'Khoảng thời gian', mở tab ngày cho người dùng
  if (selectedOption === "is_between") {
    // Gắn DatePicker cho input "từ ngày"
    $("#date_from").datepicker({
      dateFormat: "dd/mm/yy", // Định dạng ngày tháng
      showButtonPanel: true, // Hiển thị nút chọn ngày tháng
      changeMonth: true, // Cho phép thay đổi tháng
      changeYear: true, // Cho phép thay đổi năm
    });

    // Gắn DatePicker cho input "đến ngày"
    $("#date_to").datepicker({
      dateFormat: "dd/mm/yy", // Định dạng ngày tháng
      showButtonPanel: true, // Hiển thị nút chọn ngày tháng
      changeMonth: true, // Cho phép thay đổi tháng
      changeYear: true, // Cho phép thay đổi năm
    });
  }
});

document.getElementById("clear_filter").addEventListener("click", function () {
  var today = new Date();
  var firstDayOfWeek = new Date(today);
  var day = today.getDay();
  var diff = today.getDate() - day + (day == 0 ? -6 : 1); // Trừ 6 ngày nếu hôm nay là Chủ Nhật

  firstDayOfWeek.setDate(diff);

  // Đặt lại giá trị cho dropdown và input ngày
  document.getElementById("period2").value = "this_week";
  document.getElementById("report_type").value = "1";  
  document.getElementById('search_booking_no').value = '';
  document.getElementById('search_agent_no').value = '';
 var checkbox = document.getElementById("check_exchange_rate");
            if (checkbox) {
                checkbox.checked = false;
            }
  document.getElementById("date_from").value = formatDate(firstDayOfWeek);
  document.getElementById("date_to").value = formatDate(
    new Date(firstDayOfWeek.getTime() + 6 * 24 * 60 * 60 * 1000)
  );

  // Xóa tất cả lựa chọn của dropdown chọn user
  var select = document.querySelector('select[name="users[]"]');
  var options = select.options;
  for (var i = 0; i < options.length; i++) {
    options[i].selected = false;
  }
});



function formatVND(amount) {
    amount = Math.round(amount);
    return new Intl.NumberFormat('en-US', {
        style: 'decimal',
        minimumFractionDigits: 0,
        maximumFractionDigits: 2
    }).format(amount);
}

function formatUSD(amount) {
    amount = Math.round(amount * 100) / 100;
    return new Intl.NumberFormat('en-US', {
        style: 'decimal',
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
    }).format(amount);
}

document.addEventListener('DOMContentLoaded', (event) => {
    // Format VND cells
    const vndCells = document.querySelectorAll('td.vnd');
    vndCells.forEach(cell => {
        let amount = parseFloat(cell.textContent.replace(/,/g, ''));
        if (!isNaN(amount)) {
            cell.textContent = formatVND(amount);
        }
    });

    // Format USD cells
    const usdCells = document.querySelectorAll('td.usd');
    usdCells.forEach(cell => {
        let amount = parseFloat(cell.textContent.replace(/,/g, ''));
        if (!isNaN(amount)) {
            cell.textContent = formatUSD(amount);
        }
    });
});

document.addEventListener('DOMContentLoaded', (event) => {
    document.getElementById('export_excel').addEventListener('click', function () {
        // Create workbook and worksheet
        var workbook = new ExcelJS.Workbook();
        var worksheet = workbook.addWorksheet('Booking Data');

        // Get header row
        var headerRow = document.querySelector('#data-excel thead tr');
        var headerCells = headerRow.querySelectorAll('th');
        var headerData = [];
        headerCells.forEach(function (cell) {
            headerData.push(cell.innerText);
        });

        var excelHeaderRow = worksheet.addRow(headerData);
        excelHeaderRow.eachCell(function (cell) {
            cell.font = { name: 'Arial', bold: true, size: 9};
            cell.border = {
                top: { style: 'thin' },
                left: { style: 'thin' },
                bottom: { style: 'thin' },
                right: { style: 'thin' }
            };
            cell.alignment = { horizontal: 'center', vertical: 'middle' };
        });
        worksheet.getRow(1).height = 30;
        worksheet.getRow(1).eachCell({ includeEmpty: true }, function(cell) {
            cell.alignment.wrapText = true;
        });

        // Get all rows from table body
        var rows = document.querySelectorAll('#data-excel tbody tr');

        // Temporary array to store modified rows
        var modifiedRows = [];

        // Process each row
        var rowspanx = [];
        rows.forEach(function (row, rowIndex) {
            var cells = row.querySelectorAll('td');

            cells.forEach(function (cell, cellIndex) {
                var rowspan = cell.getAttribute('rowspan');

                if (rowspan && parseInt(rowspan) > 1) {;
                    rowspanx.push({
                        'rowspan': rowspan,
                        'row': rowIndex + 1,
                        'col': cellIndex + 1
                    });
                    // Tạo các ô trống cho các hàng bị ảnh hưởng
                    for (var i = 1; i < parseInt(rowspan); i++) {
                        if (!modifiedRows[rowIndex + i]) {
                            modifiedRows[rowIndex + i] = [];
                        }
                        modifiedRows[rowIndex + i][cellIndex] = '';
                    }
                }
            });

            // Copy original cell content to modifiedRows
            if (!modifiedRows[rowIndex]) {
                modifiedRows[rowIndex] = [];
            }
            cells.forEach(function (cell, cellIndex) {
                // Check if the cell is already added due to rowspan handling
                if (modifiedRows[rowIndex][cellIndex] === undefined) {
                    modifiedRows[rowIndex][cellIndex] = cell.innerText;
                }
            });
        });

        // Add rows to worksheet
        modifiedRows.forEach(function (rowData, rowIndex) {
            var excelRow = worksheet.addRow(rowData);
            worksheet.getRow(1).height = 45;

            var check = document.getElementById('Agent');
            worksheet.getColumn(1).width = 25;
            worksheet.getColumn(2).width = 30;
            worksheet.getColumn(3).width = 15;
            worksheet.getColumn(4).width = 15;
            worksheet.getColumn(5).width = 6;
            worksheet.getColumn(6).width = 6;
            worksheet.getColumn(7).width = 20;
            worksheet.getColumn(8).width = 20;
            worksheet.getColumn(9).width = 30;
            worksheet.getColumn(10).width = 5;
            worksheet.getColumn(11).width = 5;
            worksheet.getColumn(12).width = 25;
            worksheet.getColumn(13).width = 25;
            worksheet.getColumn(14).width = 15;
            worksheet.getColumn(15).width = 15;

            excelRow.eachCell(function (cell) {
                cell.font = { name: 'Arial', size: 9};
                cell.border = {

                    top: { style: 'thin' },
                    left: { style: 'thin' },
                    bottom: { style: 'thin' },
                    right: { style: 'thin' }
                };
                cell.alignment = { horizontal: 'center', vertical: 'middle', wrapText: true };
            });
            rows[rowIndex].querySelectorAll('td').forEach(function (cell, cellIndex) {
                if (cell.classList.contains('color-red')) {
                    excelRow.getCell(cellIndex + 1).font = { color: { argb: 'FFFF0000' } };
                }

                if (cell.classList.contains('width-bold')) {
                    excelRow.getCell(cellIndex + 1).font = { bold: true };
                }

                if (cell.classList.contains('yellow')) {
                    excelRow.getCell(cellIndex + 1).fill = {
                        type: 'pattern',
                        pattern: 'solid',
                        fgColor: { argb: 'FFFFFF00' } // Màu vàng
                    };
                }
                if (cellIndex >= 11 && cellIndex <= 27) { // Columns 12 to 28 (index 11 to 27)
                    var cellValue = cell.innerText.trim();
                    let className = cell.classList.value.trim().split(' ')[0];
                    var parsedValue = normalizeNumberStringByClass(cellValue, className);

                    if (!isNaN(parsedValue)) {
                        var excelCell = excelRow.getCell(cellIndex + 1);
                        excelCell.value = parsedValue;
                        excelCell.numFmt = (className === 'usd') ? '0.00' : '#,##0'; // Format number as per currency type
                        excelCell.type = ExcelJS.ValueType.Number; // Ensure Excel recognizes it as a number
                    }
                }
            });
        });
        rowspanx.forEach(function (item) {
            var rowspan = item['rowspan'];
            var x = item['row'] + 1; // Bắt đầu từ hàng thực tế trong bảng + 2
            var y = item['col'];
            var col = getExcelColumnName(y);
            var startCell = `${col}${x}`;
            var endRow = parseInt(x) + parseInt(rowspan) - 1;
            var endCell = `${col}${endRow}`;

            worksheet.mergeCells(startCell + ':' + endCell);

            // Chỉ mergeCells trong cột 'H' nếu cột 'A'
            if (col === 'A') {
                var startCellH = `H${x}`;
                var endCellH = `H${endRow}`;
                worksheet.mergeCells(startCellH + ':' + endCellH);
            }
        });


        workbook.xlsx.writeBuffer().then(function (buffer) {
            var blob = new Blob([buffer], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
            saveAs(blob, 'booking_data.xlsx');
        });
    });
});




// Function to normalize number string based on currency class
function normalizeNumberStringByClass(value, className) {
    if (className === 'vnd') {
        // Remove commas from the number string
        value = value.replace(/,/g, '');

        // Replace any non-standard decimal separator with dot
        value = value.replace(/\./g, '').replace(/,/g, '.');

        // Optionally, format as needed for VND (example: '0,000,000')
        // For example: return parseFloat(value).toFixed(0); // This would round to whole numbers
    } else if (className === 'usd') {
        // Remove commas from the number string
        value = value.replace(/,/g, '');

        // Optionally, format as needed for USD (example: '0.00')
        // For example: return parseFloat(value).toFixed(2); // This would format to two decimal places
    }

    // Convert to number if possible
    return parseFloat(value);
}
function getExcelColumnName(columnNumber) {
    let columnName = '';
    while (columnNumber > 0) {
        let remainder = (columnNumber - 1) % 26;
        columnName = String.fromCharCode(65 + remainder) + columnName;
        columnNumber = Math.floor((columnNumber - 1) / 26);
    }
    return columnName;
}

document.addEventListener('DOMContentLoaded', function() {
    var searchInput = document.getElementById('search_username');
    var selectElement = document.getElementById('username');

    function removeAccents(str) {
        return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase();
    }

    if (searchInput && selectElement) {
        searchInput.addEventListener('input', function(event) {
            var searchValue = removeAccents(event.target.value.trim());
            var options = Array.from(selectElement.getElementsByTagName('option'));

            if (searchValue === "") {
                // Nếu giá trị tìm kiếm rỗng, đặt lại tất cả các tùy chọn về vị trí ban đầu và bỏ màu nền
                options.forEach(function(option) {
                    option.style.backgroundColor = '';
                    selectElement.appendChild(option);
                });
            } else {
                var matchedOptions = [];
                var unmatchedOptions = [];

                options.forEach(function(option) {
                    var optionText = removeAccents(option.textContent.trim());
                    if (optionText.includes(searchValue)) {
                        option.style.backgroundColor = 'yellow';
                        matchedOptions.push(option);
                    } else {
                        option.style.backgroundColor = '';
                        unmatchedOptions.push(option);
                    }
                });

                // Sắp xếp lại các tùy chọn: các tùy chọn hợp lý ở trên cùng
                selectElement.innerHTML = ''; // Xóa tất cả các tùy chọn hiện tại
                matchedOptions.forEach(function(option) {
                    selectElement.appendChild(option);
                });
                unmatchedOptions.forEach(function(option) {
                    selectElement.appendChild(option);
                });
            }
        });
    }
});

document.getElementById('exchange_rate_value').addEventListener('blur', function() {
    // Lấy giá trị của trường nhập liệu
    var exchangeRateValue = this.value;
    var checkbox = document.getElementById('check_exchange_rate');

    // Kiểm tra nếu giá trị của trường nhập liệu có
    if (exchangeRateValue) {
        // Đánh dấu checkbox nếu giá trị tồn tại
        checkbox.checked = true;
    } else {
        // Bỏ chọn checkbox nếu giá trị không tồn tại
        checkbox.checked = false;
    }
});



var businessReportSpan = document.getElementById("business_report_span");
if (businessReportSpan) {
    businessReportSpan.style.width = "100%";
}



//== phân trang  
function addPaginationEventListeners(suffix) {
    var nextButton = document.getElementById('listViewNextButton_' + suffix);
    var prevButton = document.getElementById('listViewPrevButton_' + suffix);
    var startButton = document.getElementById('listViewStartButton_' + suffix);
    var endButton = document.getElementById('listViewEndButton_' + suffix);
    var startProductValueElem = document.getElementById('startProductValue_' + suffix);
    var endProductValueElem = document.getElementById('endProductValue_' + suffix);
    var totalProductValueElem = document.getElementById('totalProductValue_' + suffix);

    if (nextButton) {
        nextButton.addEventListener('click', function() {
            updateCurrentPage(suffix, 'next');
        });
    }

    if (prevButton) {
        prevButton.addEventListener('click', function() {
            updateCurrentPage(suffix, 'prev');
        });
    }

    if (startButton) {
        startButton.addEventListener('click', function() {
            updateCurrentPage(suffix, 'start');
        });
    }

    if (endButton) {
        endButton.addEventListener('click', function() {
            updateCurrentPage(suffix, 'end');
        });
    }

    if (startProductValueElem && startButton && prevButton) {
        var startProductValue = startProductValueElem.innerText;
        var isStartProductOne = startProductValue === '1';
        startButton.disabled = isStartProductOne;
        prevButton.disabled = isStartProductOne;
    }

    if (endProductValueElem && totalProductValueElem && nextButton && endButton) {
        var endProductValue = endProductValueElem.innerText;
        var totalProductValue = totalProductValueElem.innerText;
        var isEndProductSameAsTotal = endProductValue === totalProductValue;
        nextButton.disabled = isEndProductSameAsTotal;
        endButton.disabled = isEndProductSameAsTotal;
    }
}

function updateCurrentPage(suffix, action) {
    var currentPageInput = document.querySelector("input[name='currentpage']");
    var currentPage = parseInt(currentPageInput.value, 10);

    switch (action) {
        case 'next':
            currentPage++;
            break;
        case 'prev':
            currentPage--;
            break;
        case 'start':
            currentPage = 1;
            break;
        case 'end':
            let total_items = parseInt(document.getElementById('totalProductValue_' + suffix).innerText, 10);
            if (!isNaN(total_items) && total_items > 0) {
                currentPage = Math.floor(total_items / 30) + 1; // Chia lấy phần nguyên và cộng thêm 1
            } else {
                currentPage = 1;
            }
            break;
        default:
            break;
    }

    // Đảm bảo currentPage không nhỏ hơn 1
    if (currentPage < 1) {
        currentPage = 1;
    }

    currentPageInput.value = currentPage;
    document.getElementById('spinner').style.display = 'block';
    document.getElementById('paginationForm').submit();
}


document.addEventListener("DOMContentLoaded", function() {
    addPaginationEventListeners('bot');
    addPaginationEventListeners('top');
});


// Hiển thị spinner khi form được submit
document.getElementById('paginationForm').addEventListener('submit', function(event) {
    document.getElementById('spinner').style.display = 'block';
});

// Ẩn spinner khi trang đã tải xong
window.addEventListener('load', function() {
    document.getElementById('spinner').style.display = 'none';
});
const rowsPerPage = 10; // Số mẫu tin trên mỗi trang
let currentPage = 1; // Trang hiện tại
let count_tr = 0; // Số lượng hàng tính toán cho trang hiện tại
let count_tr_Pages = []; // Mảng lưu số lượng hàng cho từng trang
let mautin = 0; // Biến đếm số mẫu tin
let totalRows = 0; // Tổng số hàng (bao gồm cả rowspan)

// Hàm hiển thị trang cụ thể
function showPage(page) {
    const rows = Array.from(document.querySelectorAll('.data-row'));
    let rowIndex = 0; // Chỉ số hàng hiện tại trong mảng

    count_tr = 0; // Đặt lại số lượng hàng tính toán
    count_tr_Pages = []; // Đặt lại mảng số lượng hàng cho từng trang
    mautin = 0; // Đặt lại biến đếm số mẫu tin
    totalRows = 0; // Đặt lại tổng số hàng

    while (rowIndex < rows.length) {
        let currentRow = rows[rowIndex];
        const firstCell = currentRow.querySelector('td');
        const rowspan = parseInt(firstCell.getAttribute('rowspan') || '1', 10);

        if (rowspan > 1) {
            count_tr += rowspan; // Cộng thêm số hàng từ rowspan
            rowIndex += (rowspan - 1); // Bỏ qua các hàng kế tiếp không cần xét
        } else {
            count_tr += 1; // Cộng thêm 1 hàng nếu không có rowspan
        }

        totalRows += 1; // Cập nhật tổng số hàng
        mautin += 1; // Cập nhật số mẫu tin

        // Kiểm tra nếu số mẫu tin đã đủ để tạo một trang mới
        if (mautin >= rowsPerPage) {
            count_tr_Pages.push(count_tr); // Thêm số hàng tính toán vào mảng
            count_tr = 0; // Đặt lại số lượng hàng tính toán
            mautin = 0; // Đặt lại biến đếm số mẫu tin
        }

        rowIndex += 1; // Tiếp tục với hàng kế tiếp
    }

    if (count_tr > 0) {
        count_tr_Pages.push(count_tr); // Thêm số hàng còn lại vào mảng nếu có
    }

    // Cập nhật phân trang
    updatePagination(page);
    displayRows(page);
}

// Hàm hiển thị các hàng của trang hiện tại
function displayRows(page) {
    const rows = Array.from(document.querySelectorAll('.data-row'));
    let startIndex = 0;
    let endIndex = 0;
    let currentPageIndex = 0;

    for (let i = 0; i < count_tr_Pages.length; i++) {
        if (currentPageIndex + 1 === page) {
            startIndex = endIndex;
            endIndex = startIndex + count_tr_Pages[i];
            break;
        }
        currentPageIndex += 1;
        endIndex += count_tr_Pages[i];
    }

    rows.forEach((row, index) => {
        if (index >= startIndex && index < endIndex) {
            row.style.display = ''; // Hiển thị hàng trong khoảng trang hiện tại
        } else {
            row.style.display = 'none'; // Ẩn hàng không thuộc trang hiện tại
        }
    });
}

// Cập nhật phân trang
function updatePagination(currentPage) {
    const paginationTop = document.getElementById('pagination-top');
    const paginationBottom = document.getElementById('pagination-bottom');
    const totalPages = count_tr_Pages.length;

    const createPaginationControls = (pagination) => {
        pagination.innerHTML = '';

        const prevPage = currentPage > 1 ? currentPage - 1 : 1;
        const nextPage = currentPage < totalPages ? currentPage + 1 : totalPages;

        const firstButton = document.createElement('button');
        firstButton.textContent = '<<';
        firstButton.onclick = () => showPage(1);
        if (currentPage === 1) firstButton.disabled = true;

        const prevButton = document.createElement('button');
        prevButton.textContent = '<';
        prevButton.onclick = () => showPage(prevPage);
        if (currentPage === 1) prevButton.disabled = true;

        const nextButton = document.createElement('button');
        nextButton.textContent = '>';
        nextButton.onclick = () => showPage(nextPage);
        if (currentPage === totalPages) nextButton.disabled = true;

        const lastButton = document.createElement('button');
        lastButton.textContent = '>>';
        lastButton.onclick = () => showPage(totalPages);
        if (currentPage === totalPages) lastButton.disabled = true;

        const pageInfo = document.createElement('span');
        pageInfo.textContent = `Page ${currentPage} of ${totalPages}, total ${totalRows} items`;
        pageInfo.style.fontSize = '15px';
        pageInfo.style.fontWeight = 'bold';

        pagination.appendChild(firstButton);
        pagination.appendChild(prevButton);
        pagination.appendChild(pageInfo);
        pagination.appendChild(nextButton);
        pagination.appendChild(lastButton);
    };

    createPaginationControls(paginationTop);
    createPaginationControls(paginationBottom);
}

// Khi trang tải, hiển thị trang hiện tại
document.addEventListener('DOMContentLoaded', () => {
    showPage(currentPage);
});
