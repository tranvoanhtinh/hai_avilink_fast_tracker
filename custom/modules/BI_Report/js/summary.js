

jQuery(document).ready(function () {
  // Preloader
  var preloaderFadeOutTime = 500;

  function hidePreloader() {
    var preloader = jQuery(".spinner-wrapper");
    preloader.fadeOut(preloaderFadeOutTime);
  }

  hidePreloader();
});

document.getElementById("sm_all").addEventListener("click", function () {
  var circle = document.getElementById("circle");
  circle.style.display = "block";
});

document.getElementById("clear_filter").addEventListener("click", function () {
  var circle = document.getElementById("circle");
  circle.style.display = "block"; // Hiển thị vòng quay

  // Thiết lập thời gian trễ trước khi ẩn vòng quay
  setTimeout(function () {
    circle.style.display = "none"; // Ẩn vòng quay sau 0.5 giây
  }, 300); // 500 miligiây = 0.5 giây
});

// Find the <ul> element with both "nav" and "nav-tabs" classes
var navTabsUl = document.querySelector("ul.nav.nav-tabs");

// Check if the element exists
if (navTabsUl) {
  // Remove the <ul> element and its contents
  navTabsUl.parentNode.removeChild(navTabsUl);
}

document.getElementById("period2").addEventListener("change", function () {
  var selectedOption = this.value;
  var today = new Date();
  var firstDay, lastDay;

  switch (selectedOption) {
    case "this_week":
      firstDay = new Date(today);
      firstDay.setDate(today.getDate() - today.getDay() - 1);
      lastDay = new Date(today);
      lastDay.setDate(today.getDate() - today.getDay() + 7);
      break;
    case "last_week":
      firstDay = new Date(today);
      firstDay.setDate(today.getDate() - today.getDay() - 7);
      lastDay = new Date(today);
      lastDay.setDate(today.getDate() - today.getDay() - 1);
      break;
    case "this_month":
      firstDay = new Date(today.getFullYear(), today.getMonth(), 1);
      lastDay = new Date(today.getFullYear(), today.getMonth() + 1, 0);
      break;
    case "last_month":
      firstDay = new Date(today.getFullYear(), today.getMonth() - 1, 1);
      lastDay = new Date(today.getFullYear(), today.getMonth(), 0);
      break;
    case "this_year":
      firstDay = new Date(today.getFullYear(), 0, 1);
      lastDay = new Date(today.getFullYear(), 11, 31);
      break;
    case "last_year":
      firstDay = new Date(today.getFullYear() - 1, 0, 1);
      lastDay = new Date(today.getFullYear() - 1, 11, 31);
      break;
    case "is_between":
      firstDay = new Date(today.getFullYear(), today.getMonth(), 1);
      lastDay = new Date(today.getFullYear(), today.getMonth() + 1, 0);
      break;
  }

  // Format dates as 'dd/mm/yyyy'
  var formattedFirstDay = formatDate(firstDay);
  var formattedLastDay = formatDate(lastDay);

  // Set values to 'date_from' and 'date_to' inputs
  document.getElementById("date_from").value = formattedFirstDay;
  document.getElementById("date_to").value = formattedLastDay;

  // Kiểm tra nếu tùy chọn là 'is_between' thì mở input date_from và date_to cho chỉnh sửa
  if (selectedOption === "is_between") {
    document.getElementById("date_from").readOnly = false;
    document.getElementById("date_to").readOnly = false;
  } else {
    // Nếu không, khóa lại hai input date_from và date_to
    document.getElementById("date_from").readOnly = true;
    document.getElementById("date_to").readOnly = true;
  }
});

// Hàm chuyển định dạng ngày thành 'dd/mm/yyyy'
function formatDate(date) {
  var dd = date.getDate();
  var mm = date.getMonth() + 1; // Tháng bắt đầu từ 0
  var yyyy = date.getFullYear();

  if (dd < 10) {
    dd = "0" + dd;
  }

  if (mm < 10) {
    mm = "0" + mm;
  }

  return dd + "/" + mm + "/" + yyyy;
}

document.getElementById("period2").addEventListener("change", function () {
  var selectedOption = this.value;

  // Nếu chọn 'Khoảng thời gian', mở tab ngày cho người dùng
  if (selectedOption === "is_between") {
    // Gắn DatePicker cho input "từ ngày"
    $("#date_from").datepicker({
      dateFormat: "dd/mm/yy", // Định dạng ngày tháng
      showButtonPanel: true, // Hiển thị nút chọn ngày tháng
      changeMonth: true, // Cho phép thay đổi tháng
      changeYear: true, // Cho phép thay đổi năm
    });

    // Gắn DatePicker cho input "đến ngày"
    $("#date_to").datepicker({
      dateFormat: "dd/mm/yy", // Định dạng ngày tháng
      showButtonPanel: true, // Hiển thị nút chọn ngày tháng
      changeMonth: true, // Cho phép thay đổi tháng
      changeYear: true, // Cho phép thay đổi năm
    });
  }
});

document.getElementById("clear_filter").addEventListener("click", function () {
  var today = new Date();
  var firstDayOfWeek = new Date(today);
  var day = today.getDay();
  var diff = today.getDate() - day + (day == 0 ? -6 : 1); // Trừ 6 ngày nếu hôm nay là Chủ Nhật

  firstDayOfWeek.setDate(diff);

  // Đặt lại giá trị cho dropdown và input ngày
  document.getElementById("period2").value = "this_week";
  document.getElementById("date_from").value = formatDate(firstDayOfWeek);
  document.getElementById("date_to").value = formatDate(
    new Date(firstDayOfWeek.getTime() + 6 * 24 * 60 * 60 * 1000)
  );

  // Xóa tất cả lựa chọn của dropdown chọn user
  var select = document.querySelector('select[name="users[]"]');
  var options = select.options;
  for (var i = 0; i < options.length; i++) {
    options[i].selected = false;
  }
});
