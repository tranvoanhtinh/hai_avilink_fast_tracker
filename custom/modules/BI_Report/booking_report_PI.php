<?php



function booking_report_PI($focus, $field, $value, $view)
{
    global $mod_strings, $sugar_config, $db;
    $html = '';
    $html .= '<link rel="stylesheet" href="custom/modules/BI_Report/css/booking_style.css">';


    require_once 'include/TimeDate.php';
    $time = new TimeDate();
    $typetime = $time->get_time_format();
    $typedate = $time->get_date_format();
    $TimeDate = $typedate . ' ' . $typetime;


    if ($view == 'DetailView') {
        if (isset($_POST['status']) && $_POST['status'] == 'on') {
            $exchange_rate_value = isset($_POST['exchange_rate_value']) ? $_POST['exchange_rate_value'] : null;

            if (is_null($exchange_rate_value) || $exchange_rate_value === '' || $exchange_rate_value < 0 || $exchange_rate_value == 0) {
                $check_status = -1;
            } else {
                $check_status = 1;
            }
        } else {
            $check_status = 0;
        }
        $report_type = isset($_POST["report_type"]) ? $_POST["report_type"] : '1';
        $agent_no = isset($_POST['search_agent_no']) ? $_POST['search_agent_no'] : '';
        $booking_no = isset($_POST['search_booking_no']) ? $_POST['search_booking_no'] : '';
        $booking_status = isset($_POST['booking_status']) ? $_POST['booking_status'] : '';

        $dateFrom = isset($_POST["date_from"]) ? $_POST["date_from"] : '';
        if ($dateFrom == '') {
            $dateFrom = date('d/m/Y', strtotime('monday this week'));
        }

        $dateTo = isset($_POST["date_to"]) ? $_POST["date_to"] : '';
        if ($dateTo == '') {
            $dateTo = date('d/m/Y', strtotime('sunday this week'));
        }
        $dateFromObj = DateTime::createFromFormat('d/m/Y', $dateFrom);
        $dateToObj = DateTime::createFromFormat('d/m/Y', $dateTo);
        $dateFrom2 = $dateFromObj->format('Y-m-d');
        $dateTo2 = $dateToObj->format('Y-m-d');

        $dateTo2 = date('Y-m-d', strtotime($dateTo2 . ' +1 day')); 

        if ($booking_status != '') {
            $whereClause = 'aos_bookings.date_of_service BETWEEN "' . $dateFrom2 . '" AND "' . $dateTo2 . ' 23:59:59" AND aos_bookings.deleted = 0 AND aos_bookings.booking_status = ' . $booking_status;
        } else {
            $whereClause = 'aos_bookings.date_of_service BETWEEN "' . $dateFrom2 . '" AND "' . $dateTo2 . ' 23:59:59" AND aos_bookings.deleted = 0';
        }


        if (isset($_POST['users']) && is_array($_POST['users']) && $_POST['users'][0] !== '') {
            $selected_users = array_map(function ($user_id) {
                return "'" . $user_id . "'";
            }, $_POST['users']);
            $userIdsString = implode(',', $selected_users);
            $whereClause .= ' AND aos_bookings.assigned_user_id IN (' . $userIdsString . ')';
        }
        $booking = BeanFactory::newBean('AOS_Bookings');
        $query = $booking->create_new_list_query(
            'aos_bookings.date_modified DESC',
            $whereClause,
            array()
        );
        $parts = explode(' FROM ', $query, 2);
        $selectPart = $parts[0]; // Phần SELECT
        $rest = isset($parts[1]) ? $parts[1] : ''; // Phần còn lại sau FROM
        // Tách phần FROM và phần WHERE từ phần còn lại
        $parts = explode(' where ', $rest, 2);
        $fromPart = $parts[0]; // Phần FROM
        $wherePart = isset($parts[1]) ? ' where ' . $parts[1] : ''; // Phần WHERE
        // Kiểm tra giá trị của $agent_no và xây dựng câu truy vấn cuối cùng
        if ($agent_no != '') {
            $query = $selectPart . ' FROM ' . $fromPart . ' LEFT JOIN accounts_aos_bookings_1_c ab ON aos_bookings.id = ab.accounts_aos_bookings_1aos_bookings_idb LEFT JOIN accounts acc ON ab.accounts_aos_bookings_1accounts_ida = acc.id ' . $wherePart . ' AND acc.account_code = "' . $agent_no . '"';
        } else {
            $query = $selectPart . ' FROM ' . $fromPart . $wherePart;
        }
        $parts = explode(' FROM ', $query, 2);
        $selectPart = $parts[0]; // Phần SELECT
        $rest = isset($parts[1]) ? $parts[1] : ''; // Phần còn lại sau FROM
        // Tách phần FROM và phần WHERE từ phần còn lại
        $parts = explode(' where ', $rest, 2);
        $fromPart = $parts[0]; // Phần FROM
        $wherePart = isset($parts[1]) ? ' where ' . $parts[1] : ''; // Phần WHERE

        if ($booking_no != '') {
            $query = $selectPart . ' FROM ' . $fromPart . ' ' . $wherePart . ' AND aos_bookings.name LIKE \'%' . $booking_no . '%\'';
        } else {
            $query = $selectPart . ' FROM ' . $fromPart . $wherePart;
        }

        $result = $booking->db->query($query);
        $total_items = $result->num_rows;
        $html .= '<div class="pagination" id="pagination-top"></div>';
        $html .= '<table id="data-excel">
<thead>
    <tr>
        <th>Số BOOKING</th>
        <th>Tên hành khách</th>
        <th>Passport</th>
        <th>Chuyến bay</th>
        <th>Từ</th>
        <th>Đến</th>
        <th>Ngày bay</th>
        <th>Mã đặt chỗ</th>
        <th>Yêu cầu phục vụ</th>
        <th style="display:none">&nbsp</th>
        <th style="display:none">&nbsp</th>
        <th>Tỷ giá</th>';

        if ($report_type == 1) {
            $html .= '<th>Giá vốn<br> (vnđ)<br><span>(1)</span></th>
                      <th>Giá vốn<br> (usd)<br><span>(2)</span></th>';
        }
        $html .= '<th>Phí dịch vụ <br> (vnđ)<br><span>(3)</span></th>
        <th>Phí dịch vụ <br> (usd)<br><span>(4)</span></th>
        <th>Phí dán visa <br> (usd)<br><span>(5)</span></th>
        <th>Phí thu hộ <br> (vnđ)<br><span>(6)</span></th>
        <th>Phí thu hộ <br> (usd)<br><span>(7)</span></th>
        <th>Phí khác <br> (vnđ)<br><span>(8)</span></th>
        <th>Phí khác <br> (usd)<br><span>(9)</span></th>';

        if ($report_type == 1) {
            $html .= '<th>Tổng chi phí <br>(vnd)<br><span>(10)=(1)+(2)+(5)+(8)+(9)</span></th>
                      <th>Tổng thanh toán <br>(vnd)<br><span>(11)=(10)-[(6)+(7)]</span></th>
                      <th>Tổng doanh thu <br>(vnd)<br><span>(12)=(3)+(4)</span></th>
                      <th>Tổng thu <br>(vnd)<br><span>(13)=(12)-[(6)+(7)]</span></th>
                      <th>Lợi nhuận <br>(vnd)<br><span>(14)=[(3)+(4)] - [(1)+(2)]</span></th>
                      <th>Dòng tiền đã thu<br>(vnd)<br><span>(15)</span></th>
                      <th>Công nợ<br>(vnd)<br><span>(16)=(13)-(15)</span></th>';
        }

        if ($report_type != 1) {
            $html .= '<th>Tổng thanh toán (vnd)<br>(3)+(4)+(5)+(6)+(7)+(8)+(9)</th>';
        }
        $html .= '<th>Note 1</th>
                  <th>Note 2</th>';

        if ($report_type == 1) {
            $html .= '<th>Hóa đơn </th>
                      <th>Số hóa đơn </th>';

        }

        $html .= '</tr>
            </thead>
    <tbody>';


        $i = 0;
        $masterData = array();
        while ($row = $db->fetchByAssoc($result)) {
            $booking_id = $row['id'];


            //=================================================
            $accounts = array();
            $qaccounts = "
    SELECT
    a.name,
    a.phone_alternate,
    a.id
    FROM
    accounts_aos_bookings_1_c ab
    JOIN
    accounts a ON ab.accounts_aos_bookings_1accounts_ida = a.id
    JOIN
    aos_bookings b ON ab.accounts_aos_bookings_1aos_bookings_idb = b.id
    WHERE
    b.id = '" . $booking_id . "' AND ab.deleted = 0
    ";
            $result_a = $db->query($qaccounts);
            $accounts = array();
            $account_id;
            while ($row_a = $db->fetchByAssoc($result_a)) {
                $account_id = $row_a['id'];
                $accounts[] = array(
                    'name' => $row_a['name'],
                    'phone_alternate' => $row_a['phone_alternate'],
                );
            }

            //=================================================
            $contacts = array();
            $qcontacts = "
    SELECT
    c.first_name,
    c.last_name,
    c.phone_mobile
    FROM
    contacts_aos_bookings_1_c cb
    JOIN
    contacts c ON cb.contacts_aos_bookings_1contacts_ida = c.id
    JOIN
    aos_bookings b ON cb.contacts_aos_bookings_1aos_bookings_idb = b.id
    WHERE
    b.id = '" . $booking_id . "' AND cb.deleted = 0
    ";
            $result_c = $db->query($qcontacts);
            $contacts = array();
            while ($row_c = $db->fetchByAssoc($result_c)) {
                $contacts[] = array(
                    'name' => $row_c['first_name'] . ' ' . $row_c['last_name'],
                    'phone_mobile' => $row_c['phone_mobile'],
                );
            }
            //=================================================
            $products = array();
            $qproducts = "
    SELECT
    p.name, p.part_number, p.price ,p.type_currency, p.exchange_rate, p.vnd, p.usd , p.type_service
    FROM
    aos_products_aos_bookings_1_c pb
    JOIN
    aos_products p ON pb.aos_products_aos_bookings_1aos_products_ida = p.id
    JOIN
    aos_bookings b ON pb.aos_products_aos_bookings_1aos_bookings_idb = b.id
    WHERE
    b.id = '" . $booking_id . "' AND pb.deleted = 0
    ";


            $result_p = $db->query($qproducts);
            // Lặp qua kết quả trả về và lưu vào mảng $qaos_products
            while ($row_p = $db->fetchByAssoc($result_p)) {
                $products[] = array(
                    'name' => $row_p['name'],
                    'part_number' => $row_p['part_number'],
                    'price' => $row_p['price'],
                    'type_currency' => $row_p['type_currency'],
                    'exchange_rate' => ($check_status == 1) ? $exchange_rate_value : $row_p['exchange_rate'],
                    'vnd' => $row_p['vnd'],
                    'usd' => $row_p['usd'],
                    'type_service' => $row_p['type_service'],
                );
            }
            //=================================================
            $booking_detail = array();
            $qbooking_detail = "
            SELECT bd.*
            FROM
            aos_booking_detail bd
            WHERE
            bd.booking_id = '" . $booking_id . "' AND bd.deleted = 0
            ";

            $result_bd = $db->query($qbooking_detail);
            while ($row_bd = $db->fetchByAssoc($result_bd)) {
                $booking_detail[] = array(
                    'id' => $row_bd['passenger_id'],
                    'name' => $row_bd['name'],
                    'booking_date' => $row_bd['booking_date'],
                    'airport_from' => $row_bd['airport_from'],
                    'airport_to' => $row_bd['airport_to'],
                    'flight_name' => $row_bd['flight_name'],
                    'pnr' => $row_bd['pnr'],
                    'nationality' => $row_bd['nationality'],
                    'service' => $row_bd['service'],
                    'booking_no' => $row_bd['booking_no'],
                    'date_std_sta' => $row_bd['date_std_sta'],
                    'passenger_no' => $row_bd['passenger_no'],
                    'passport' => $row_bd['passport'],
                );
            }
            //=================================================

$q_passenger = "SELECT passenger_id FROM aos_booking_detail WHERE booking_id ='".$booking_id."' AND deleted ='0'";
$result_pa = $db->query($q_passenger);
$action_services = [];

// Vòng lặp qua các hành khách và thực hiện truy vấn chi tiết cho mỗi hành khách
while ($row_pa = $db->fetchByAssoc($result_pa)) {
    $passenger_id = $row_pa['passenger_id'];

    // Truy vấn chi tiết dịch vụ cho từng hành khách
    $qaction_services = "
    SELECT
        s.*,
        b.invoice_issued AS invoice_issued,
        COALESCE(c.id, '') AS c_id,
        COALESCE(c.type_currency, '') AS c_type_currency,
        COALESCE(c.exchange_rate, " . ($check_status == 1 ? $exchange_rate_value : 's.exchange_rate') . ") AS c_exchange_rate,
        c.vnd AS c_vnd,
        c.usd AS c_usd,
        ap.id AS passenger_id,
        ap.name AS passenger_name
    FROM
        aos_bookings_spa_actionservice_1_c AS asr
    JOIN
        spa_actionservice s ON asr.aos_bookings_spa_actionservice_1spa_actionservice_idb = s.id
    JOIN
        aos_bookings b ON asr.aos_bookings_spa_actionservice_1aos_bookings_ida = b.id
    LEFT JOIN
        aos_cost_unitprice c ON s.product_id = c.product_id
        AND c.account_id = '" . $account_id . "'
        AND c.deleted = 0
        AND c.status = '2'
    LEFT JOIN
        aos_passengers_spa_actionservice_1_c AS ap_as ON asr.aos_bookings_spa_actionservice_1spa_actionservice_idb = ap_as.aos_passengers_spa_actionservice_1spa_actionservice_idb
    LEFT JOIN
        aos_passengers AS ap ON ap_as.aos_passengers_spa_actionservice_1aos_passengers_ida = ap.id
    WHERE
        b.id = '" . $booking_id . "' 
        AND asr.deleted = 0 
        AND s.status = 'Completed'
        AND ap.id = '" . $passenger_id . "'";  // Thêm điều kiện để lọc theo passenger_id
    // Thực hiện truy vấn và xử lý kết quả
    $result_s = $db->query($qaction_services);

    // Kiểm tra nếu không có kết quả nào được trả về
    if ($db->getRowCount($result_s) == 0) {
        // Nếu không có kết quả, thêm một mảng rỗng với passenger_id
        $action_services[$passenger_id] = [];
    } else {
        while ($row_s = $db->fetchByAssoc($result_s)) {
            $type_service = $row_s['type_service'];

            // Nếu chưa có dịch vụ này trong mảng, thêm vào với key là type_service
            if (!isset($action_services[$passenger_id][$type_service])) {
                $action_services[$passenger_id][$type_service] = [
                    'type_service' => $row_s['type_service'],
                    'name' => $row_s['name'],
                    'status' => $row_s['status'],
                    'action_date' => $row_s['action_date'],
                    'type_currency' => $row_s['type_currency'],
                    'exchange_rate' => ($check_status == 1) ? $exchange_rate_value : $row_s['exchange_rate'],
                    'vnd' => $row_s['vnd'],
                    'usd' => $row_s['usd'],
                    'vnd_total' => $row_s['vnd_total'],
                    'usd_total' => $row_s['usd_total'],
                    'c_id' => $row_s['c_id'],
                    'c_type_currency' => $row_s['c_type_currency'],
                    'c_exchange_rate' => $row_s['c_exchange_rate'],
                    'c_vnd' => $row_s['c_vnd'],
                    'c_usd' => $row_s['c_usd'],
                    'invoice_issued' => $row_s['invoice_issued'],
                ];
            } else {
                // Nếu đã có, chỉ cần cập nhật các giá trị sum cho vnd, usd, vnd_total, usd_total
                $action_services[$passenger_id][$type_service]['vnd'] += $row_s['vnd'];
                $action_services[$passenger_id][$type_service]['usd'] += $row_s['usd'];
                $action_services[$passenger_id][$type_service]['vnd_total'] += $row_s['vnd_total'];
                $action_services[$passenger_id][$type_service]['usd_total'] += $row_s['usd_total'];
                $action_services[$passenger_id][$type_service]['c_vnd'] += $row_s['c_vnd'];
                $action_services[$passenger_id][$type_service]['c_usd'] += $row_s['c_usd'];
            }
        }
    }
}


// Đảm bảo có đủ các type_service từ 1 đến 4 cho mỗi hành khách
foreach ($action_services as $passenger_id => &$services) {
    for ($i = 1; $i <= 4; $i++) {
        if (!isset($services[$i])) {
            $services[$i] = [
                'type_service' => $i,
                'name' => '',
                'status' => '',
                'action_date' => '',
                'type_currency' => 1,
                'exchange_rate' => 0,
                'vnd' => 0,
                'usd' => 0,
                'vnd_total' => 0,
                'usd_total' => 0,
                'c_id' => '',
                'c_type_currency' => 1,
                'c_exchange_rate' => 0,
                'c_vnd' => 0,
                'c_usd' => 0,
                'invoice_issued' => '',
            ];
        }
    }
}

// Sắp xếp dữ liệu theo type_service cho mỗi hành khách
foreach ($action_services as $passenger_id => &$services) {
    ksort($services);
}

            //===================================================================
            $exchange_rate_clause = ($check_status == 1) ? $exchange_rate_value : 'ab.exchange_rate';

            $qbilling = "
    SELECT
    ab.exchange_rate,
    ab.invoice_issued,
    bb.type_currency,
    bb.billing_status,
    SUM(bb.vnd) AS vnd,
    SUM(bb.usd) AS usd
    FROM
    aos_bookings AS ab
    INNER JOIN
    aos_bookings_bill_billing_1_c AS abbbc
    ON ab.id = abbbc.aos_bookings_bill_billing_1aos_bookings_ida
    INNER JOIN
    bill_billing AS bb
    ON abbbc.aos_bookings_bill_billing_1bill_billing_idb = bb.id
    WHERE
    ab.id = '" . $booking_id . "' AND abbbc.deleted = '0' AND bb.type = 'receipts' AND  bb.billing_status = 'completed'
    GROUP BY
    ab.id, bb.billing_status
    ";
            $result_b = $db->query($qbilling);
            $billing = array();
            while ($row_b = $db->fetchByAssoc($result_b)) {
                $billing[] = array(
                    'billing_status' => $row_b['billing_status'] ?? 0,
                    'type_currency' => $row_b['type_currency'] ?? 0,
                    'vnd' => $row_b['vnd'] ?? 0,
                    'usd' => $row_b['usd'] ?? 0,

                );
            }
            // Build the booking array ================================================= MASTER DATA
            $booking = array(
                'invoice_issued' => $row['invoice_issued'],
                'name' => $row['name'],
                'id' => $row['id'],
                'invoice_no' => $row['invoice_no'],
                'booking_date' => $row['booking_date'],
                'booking_status' => $row['booking_status'],
                'type_service' => $row['type_service'],
                'station' => $row['station'],
                'airport' => $row['airport'],
                'assigned_user_id' => $row['assigned_user_id'],
                'description' => $row['description'],
                'exchange_rate' => ($check_status == 1) ? $exchange_rate_value : $row['exchange_rate'],
                'accounts' => $accounts,
                'contacts' => $contacts,
                'aos_products' => $products,
                'aos_booking_detail' => $booking_detail,
                'spa_action_services' => $action_services,
                'billing' => $billing,

            );

            $masterData[] = $booking;
            $i++;
        }
        //----------- phần tỉnh tổng cho tất cả -----------------
        $all_total_c_vnd = 0;
        $all_total_c_usd = 0;
        $all_service_fee_vnd = 0;
        $all_service_fee_usd = 0;
        $all_visa_fee_usd = 0;
        $all_collection_fee_vnd = 0;
        $all_collection_fee_usd = 0;
        $all_other_fee_vnd = 0;
        $all_other_fee_usd = 0;
        $exchange_rate = 0;
        $all_collect = 0;
        foreach ($masterData as $key => $value) {
            $exchange_rate = $value['exchange_rate'];
            if (isset($value['spa_action_services'])) {
                // Debug output
                // var_dump($value['spa_action_services']);

                $action_services = $value['spa_action_services'];
            $aos_booking_detail = $value['aos_booking_detail'];
            foreach ($aos_booking_detail as $booking_detail) {
               foreach ($action_services[$booking_detail['id']] as $action_service) {
                    if ($action_service['type_service'] == 1) {
                        $all_total_c_vnd += (float)$action_service['c_vnd'];
                        $all_total_c_usd += (float)$action_service['c_usd'];
                        $all_service_fee_vnd += (float)$action_service['vnd'];
                        $all_service_fee_usd += (float)$action_service['usd'];
                    }
                    if ($action_service['type_service'] == 2) {
                        $all_visa_fee_usd += (float)$action_service['usd'];
                    }
                    if ($action_service['type_service'] == 3) {
                        $all_collection_fee_vnd += (float)$action_service['vnd'];
                        $all_collection_fee_usd += (float)$action_service['usd'];
                    }
                    if ($action_service['type_service'] == 4) {
                        $all_other_fee_vnd += (float)$action_service['vnd'];
                        $all_other_fee_usd += (float)$action_service['usd'];
                    }
                }
            }
        }
            if (isset($value['billing'])) {
                $billings = $value['billing'];

                foreach ($billings as $billing) {
                    if ($billing['billing_status'] == 'completed') {
                        $all_collect = ((float)$billing['vnd'] + (float)$billing['usd'] * $exchange_rate) * $num_passenger;
                    }
                }
            }
        }

        $all_total_c_price = ($all_total_c_usd * $exchange_rate) + $all_total_c_vnd;
        $all_service_fee = ($all_service_fee_usd * $exchange_rate) + $all_service_fee_vnd;
        $all_visa_fee = $all_visa_fee_usd * $exchange_rate;
        $all_collection_fee = ($all_collection_fee_usd * $exchange_rate) + $all_collection_fee_vnd;
        $all_other_fee = ($all_other_fee_usd * $exchange_rate) + $all_other_fee_vnd;
        $all_totalcost =   $all_total_c_price + $all_visa_fee + $all_other_fee;
        $all_total_payment = $all_totalcost - $all_collection_fee;
        $all_total_revenue = $all_service_fee;
        $all_total_revenue2 = $all_total_revenue  - $all_collection_fee;
        $all_profit = $all_service_fee - $all_total_c_price;

        $html .= '<tr class="">';
        if ($report_type == 1) {
            $html .= '<td></td>';
        } else {
            $html .= '<td id = "Agent"></td>';
        }

        $html .= '<td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td style="display:none"></td>
        <td style="display:none"></td>';
        if ($report_type == 1) {
            $html .= '<td class="vnd color-red yellow">' . $all_total_c_vnd . '</td>
        <td class="usd color-red yellow">' . $all_total_c_usd . '</td>';
        }
        $html .= '<td style="font-weight: bold"class="vnd color-red yellow">' . $all_service_fee_vnd . '</td>
        <td style="font-weight: bold"class="usd color-red yellow">' . $all_service_fee_usd . '</td>
        <td class="usd color-red yellow">' . $all_visa_fee_usd . '</td>
        <td class="vnd color-red yellow">' . $all_collection_fee_vnd . '</td>
        <td class="usd color-red yellow">' . $all_collection_fee_usd . '</td>
        <td class="vnd color-red yellow">' . $all_other_fee_vnd . '</td>
        <td class="usd color-red yellow">' . $all_other_fee_usd . '</td>';
        if ($report_type == 1) {
            $html .= '<td class="vnd color-red yellow">' . $all_totalcost . '</td>
        <td style="font-weight: bold" class="vnd color-red yellow">' . $all_total_payment . '</td>
        <td class="vnd color-red yellow">' . $all_total_revenue . '</td>
        <td class="vnd color-red yellow">' . $all_total_revenue2 . '</td>
        <td style="font-weight: bold" class="vnd color-red yellow">' . $all_profit . '</td>
        <td class="vnd color-red yellow">' . $all_collect . '</td>
        <td style="font-weight: bold" class="vnd color-red yellow">' . ($all_total_revenue2 - $all_collect) . '</td>
        <td></td>
        <td></td>';
        }
        $total = $all_service_fee_vnd + $all_collection_fee_vnd + $all_other_fee_vnd + ($all_service_fee_usd +  $all_visa_fee_usd + $all_collection_fee_usd + $all_other_fee_usd) * $exchange_rate;
        if ($report_type != 1) {
            $html .= '<td  class="vnd color-red yellow" style="color: red; font-weight: bold">' . $total . '</td>';
        } else {
        }
        $html .= '<td style="width: 5%; "></td><td style="width: 5%; "></td>';
        $html .= '</tr>';
        $l = 1;
        foreach ($masterData as $key => $value) {
            if (isset($value['aos_booking_detail'])) {
                $aos_booking_detail = $value['aos_booking_detail'];

                $k = 1;
                foreach ($aos_booking_detail as $booking_detail) {
                    $count = count($aos_booking_detail);
                    $html .= '<tr class="data-row">';


                    if ($k == 1) {
                        $id = htmlspecialchars($value['id']); // An toàn hóa giá trị của id
                        $html .= '<td class="width-bold link" rowspan="' . $count . '" name="booking_no[' . $l . '][' . $k . ']">'
                            . '<a href="index.php?module=AOS_Bookings&amp;offset=3&amp;stamp=1721376103034076300&amp;return_module=AOS_Bookings&amp;action=DetailView&amp;record=' . $id . '">'
                            . htmlspecialchars($value['name']) // An toàn hóa giá trị của name
                            . '</a>'
                            . '</td>';
                    }else {
                        $html .= '<td style="display: none"></td>';
                    }
                    $html .= '<td>' . $booking_detail['name'] . '</td>';
                    $html .= '<td>' . $booking_detail['passport'] . '</td>';
                    $html .= '<td>' . $booking_detail['flight_name'] . '</td>';
                    $html .= '<td>' . $booking_detail['airport_from'] . '</td>';
                    $html .= '<td>' . $booking_detail['airport_to'] . '</td>';
                    $date = DateTime::createFromFormat('Y-m-d H:i:s', $booking_detail['date_std_sta']);
                    $date->modify('+7 hours');
                    $formatted_date = $date->format('d/m/Y H:i:s');

                    $html .= '<td>' . $formatted_date . '</td>';

                    $html .= '<td>' . $booking_detail['pnr'] . '</td>';

                    if ($k == 1) {
                        $html .= '<td rowspan = "' . $count . '" name="booking_service[' . $l . '][' . $k . ']">' . $booking_detail['service'] . '</td>';

                         $html .= '<td style="display:none"></td>';
                         $html .= '<td style="display: none"></td>';

                    } else {
                        $html .= '<td style="display: none"></td>';
                        $html .= '<td style="display:none"></td>';
                        $html .= '<td style="display: none"></td>';

                    }

                    if (isset($value['spa_action_services'])) {

                        $c_vnd = 0;
                        $c_usd = 0;
                        $vnd_1 = 0;
                        $usd_1 = 0;
                        $usd_2 = 0;
                        $vnd_3 = 0;
                        $usd_3 = 0;
                        $vnd_4 = 0;
                        $usd_4 = 0;
                      // var_dump($booking_detail['id']);

                    foreach ($value['spa_action_services'][$booking_detail['id']] as $action_service) {

                    if ($action_service['type_service'] == 1) {
                        if ($report_type == 1) {

                            $html .= '<td class="vnd">' . $exchange_rate . '</td>';
                            $c_vnd = $action_service['c_vnd'];
                            $c_usd = $action_service['c_usd'];
                            $html .= '<td class="vnd">' . $c_vnd . '</td>';
                            $html .= '<td class="usd">' . $c_usd . '</td>';
                        }
                        if ($report_type == 2) {
                            $html .= '<td class="vnd">' . $exchange_rate . '</td>';
                        }
                        $vnd_1 = $action_service['vnd'];
                        $usd_1 = $action_service['usd'];
                        $html .= '<td style="font-weight: bold" class="vnd">' . $vnd_1 . '</td>';
                        $html .= '<td style="font-weight: bold" class="usd">' . $usd_1 . '</td>';
                    } elseif ($action_service['type_service'] == 2) {
                        $usd_2 = $action_service['usd'];
                        $html .= '<td class="usd">' . $usd_2 . '</td>';
                    } elseif ($action_service['type_service'] == 3) {
                        $vnd_3 = $action_service['vnd'];
                        $usd_3 = $action_service['usd'];
                        $html .= '<td class="vnd">' . $vnd_3 . '</td>';
                        $html .= '<td class="usd">' . $usd_3 . '</td>';
                    } elseif ($action_service['type_service'] == 4) {
                        $vnd_4 = $action_service['vnd'];
                        $usd_4 = $action_service['usd'];
                        $html .= '<td class="vnd">' . $vnd_4 . '</td>';
                        $html .= '<td class="usd">' . $usd_4 . '</td>';
                    }
                }
                 // Tính $Totalcost bỏ qua các giá trị bằng 0
                        $cost_price = ($c_usd * $exchange_rate) + $c_vnd;
                        $service_fee  = ($usd_1 * $exchange_rate) + $vnd_1;
                        $visa_fee =  ($usd_2 * $exchange_rate);
                        $collection_fee =  ($usd_3 * $exchange_rate) + $vnd_3;
                        $other_fee = ($usd_4 * $exchange_rate) + $vnd_4;
                        $totalcost =   $cost_price + $visa_fee + $other_fee; // tổng chi phí
                        $total_payment = $totalcost - $collection_fee;  // tổng thanh toán
                        $total_revenue = $service_fee; // tổng doanh thu
                        $total_revenue2 = $total_revenue  - $collection_fee; //tổng thu
                        $total_chile = $service_fee + $collection_fee + $other_fee + ($usd_2 * $exchange_rate);
                        $profit = $service_fee - $cost_price; // lợi nhuận
                        $billing_vnd = 0;
                        $debt = 0;

                        if ($report_type == 1) {
                            // Hiển thị dưới dạng VND và USD
                            $html .= '<td class="vnd">' . $totalcost . '</td>';
                            $html .= '<td style="font-weight: bold;color: red"class="vnd">' . $total_payment . '</td>';
                            $html .= '<td class="vnd">' . $total_revenue . '</td>';
                            $html .= '<td class="vnd">' . $total_revenue2 . '</td>';
                            $html .= '<td style="font-weight: bold;color: red"class="vnd">' . $profit . '</td>';


                            if (isset($value['billing']) && !empty($value['billing'])) {
                                $billing_vnd = 0;

                                foreach ($value['billing'] as $billing) {
                                    if ($billing['billing_status'] == 'completed') {
                                        $billing_vnd = $billing['vnd'] + ($billing['usd'] * $exchange_rate);
                                    }
                                }

                                // Xử lý khi có billing
                                if ($billing_vnd > 0) {
                                    $html .= '<td class="vnd">' . $billing_vnd . '</td>';
                                    $debt = $total_revenue2 - $billing_vnd;
                                    $html .= '<td  style="color: red; font-weight: bold"lass="vnd">' . $debt . '</td>';
                                } else {
                                    $html .= '<td class="vnd">0</td>';
                                    $debt = $total_revenue2 - 0;
                                    $html .= '<td  style="color: red; font-weight: bold"class="vnd">' . $debt . '</td>';
                                }
                            } else {
                                $html .= '<td class="vnd">0</td>';
                                $debt = $total_revenue2 - 0;
                                $html .= '<td  style="color: red; font-weight: bold"class="vnd">' . $debt . '</td>';
                            }
                            $html .= '<td></td>';
                        }
                        if ($report_type != '1') {
                            $html .= '<td class ="vnd color-red" style="color: red">' . $total_chile . '</td>';
                        }


                        if ($k == 1) {
                            $html .= '<td rowspan="' . $count . '">' . $value['description'] . '</td>';
                        } else {
                            $html .= '<td style="display: none"></td>';
                        }

                        foreach ($value['spa_action_services'][$booking_detail['id']] as $action) {
                                // Kiểm tra nếu loại dịch vụ là 1
                                if ($action['type_service'] == 1 && $report_type == 1) {
                                    // Kiểm tra nếu chỉ mục 'invoice_issued' có tồn tại và hóa đơn đã được phát hành
                                    if (isset($value['invoice_issued'])) {
                                        if ($value['invoice_issued'] == 1) {
                                            $html .= '<td>Có xuất hóa đơn</td>';
                                        }else {
                                            $html .= '<td>Không xuất hóa đơn</td>';
                                        }
                                    } else {
                                        // Nếu chỉ mục 'invoice_issued' không tồn tại
                                        $html .= '<td></td>';
                                    }
                                }
                            }
                        


                        if ($k == 1) {
                            $html .= '<td rowspan="' . $count . '">' . $value['invoice_no'] . '</td>';
                        } else {
                            $html .= '<td style="display: none"></td>';
                        }
                    }
                    $k++;
                }
            }
            $html .= '</tr>';
            $l++;
        }

        $html .= '</tbody></table>';
        $html .= '<div class="pagination" id="pagination-bottom"></div>';

        return $html;
    }
}


function booking_choostime($focus, $field, $value, $view)
{
    global $mod_strings, $db;

    $arr = array();
    $user = BeanFactory::newBean('cs_cusUser');

    $query = $user->create_new_list_query("cs_cususer.date_entered DESC", "cs_cususer.status = 'Active'", array());

    $res = $user->db->query($query);

    $html = "";
    $html .= '<link rel="stylesheet" type="text/css" href="custom/modules/BI_Report/css/booking_style.css">';

    // Lấy giá trị đã chọn từ $_POST
    $selectedOption = isset($_POST['period2']) ? $_POST['period2'] : 'default';
    $selectedOption2 = isset($_POST['report_type']) ? $_POST['report_type'] : '1';
    $statusChecked = isset($_POST['status']) && $_POST['status'] == 'on' ? 'checked' : '';
    $exchangeRateValue = isset($_POST['exchange_rate_value']) ? htmlspecialchars($_POST['exchange_rate_value']) : '';

    $booking_status = isset($_POST['booking_status']) ? $_POST['booking_status'] : '';
    $dateFromValue = isset($_POST['date_from']) ? htmlspecialchars($_POST['date_from']) : '';
    $dateToValue = isset($_POST['date_to']) ? htmlspecialchars($_POST['date_to']) : '';

    $dateFromValue = !empty($_POST['date_from']) ? htmlspecialchars($_POST['date_from']) : date('d/m/Y', strtotime('monday this week'));
    $dateToValue = !empty($_POST['date_to']) ? htmlspecialchars($_POST['date_to']) : date('d/m/Y', strtotime('sunday this week'));

    $html .= '<div id="cde" style="margin-top:20px;">';
    $html .= '<div class="row" style="margin-bottom: 5px">';
    $html .= '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left">';

    $html .= '<form id="paginationForm" method="post" action="">';

    $html .= '<div class="row">';
    $html .= '<div class="col-xs-12 col-sm-6 col-md-2">';

    $html .= '<select style="margin-top: 20px" class="form-control" name="period2" id="period2" onchange="periodChange2()">';
    $html .= '<option value="this_week" ' . ($selectedOption === 'this_week' ? 'selected' : '') . '>' . $mod_strings['LBL_THISWEEK'] . '</option>';
    $html .= '<option value="last_week" ' . ($selectedOption === 'last_week' ? 'selected' : '') . '>' . $mod_strings['LBL_LASTWEEK'] . '</option>';
    $html .= '<option value="this_month" ' . ($selectedOption === 'this_month' ? 'selected' : '') . '>' . $mod_strings['LBL_THISMONTH'] . '</option>';
    $html .= '<option value="last_month" ' . ($selectedOption === 'last_month' ? 'selected' : '') . '>' . $mod_strings['LBL_LASTMONTH'] . '</option>';
    $html .= '<option value="this_year" ' . ($selectedOption === 'this_year' ? 'selected' : '') . '>' . $mod_strings['LBL_THISYEAR'] . '</option>';
    $html .= '<option value="last_year" ' . ($selectedOption === 'last_year' ? 'selected' : '') . '>' . $mod_strings['LBL_LASTYEAR'] . '</option>';
    $html .= '<option value="is_between" ' . ($selectedOption === 'is_between' ? 'selected' : '') . '>' . $mod_strings['LBL_IS_BETWEEN'] . '</option>';
    $html .= '</select>';
    $html .= '<select style="margin-top: 20px" class="form-control" name="report_type" id="report_type">';
    $html .= '<option value="1"' . ($selectedOption2 === '1' ? ' selected' : '') . '>' . 'Tổng hợp' . '</option>';
    $html .= '<option value="2"' . ($selectedOption2 === '2' ? ' selected' : '') . '>' . 'Bảng kê đại lý' . '</option>';
    $html .= '</select>';
    $html .= '</select>';
    $booking_status = isset($_POST['booking_status']) ? $_POST['booking_status'] : '';
    $html .= '<select style="margin-top: 20px" class="form-control" name="booking_status" id="booking_status">';
    $html .= '<option value=""' . ($booking_status === '' ? ' selected' : '') . '>--- Trạng thái ---</option>';
    $html .= '<option value="1"' . ($booking_status === '1' ? ' selected' : '') . '>Mới</option>';
    $html .= '<option value="2"' . ($booking_status === '2' ? ' selected' : '') . '>Đã confirm</option>';
    $html .= '<option value="3"' . ($booking_status === '3' ? ' selected' : '') . '>Đã hoàn thành</option>';
    $html .= '<option value="4"' . ($booking_status === '4' ? ' selected' : '') . '>Hủy</option>';
    $html .= '<option value="5"' . ($booking_status === '5' ? ' selected' : '') . '>Khác</option>';
    $html .= '</select>';
    $html .= '</div>';
    $html .= '<div class="col-xs-12 col-sm-6 col-md-3">';
    $html .= '<label>' . $mod_strings['LBL_FROM'] . '</label>';
    $html .= '<input class="form-control" id="date_from" name="date_from" type="text" maxlength="100" value="' . $dateFromValue . '" title="" tabindex="-1" accesskey="9">';
    $html .= '<div class="form-inline" style="margin-top: 10px;">';
    // Input text
    $html .= '<input class="form-control" id="exchange_rate_value" name="exchange_rate_value" type="number" value="' . $exchangeRateValue . '" placeholder="Nhập tỷ giá"> Gán cho tất cả';
    $html .= '<div class="checkbox" style="margin-left: 10px; transform: scale(1.5);">';
    $html .= '<label>';
    $html .= '<input type="checkbox" id="check_exchange_rate" name="status" ' . $statusChecked . '>';
    $html .= '</label>';
    $html .= '</div>';

    $html .= '</div>'; // Đóng div form-inline
    $html .= '</div>'; // Đóng col-xs-12 col-sm-6 col-md-3

    $html .= '<div class="col-xs-12 col-sm-6 col-md-3">';
    $html .= '<label>' . $mod_strings['LBL_TO'] . '</label>';
    $html .= '<input class="form-control" type="text" id="date_to" name="date_to" maxlength="100" value="' . $dateToValue . '" title="" tabindex="-1" accesskey="9">';
    $html .= '</div>';
    $html .= '<div class="col-xs-12 col-sm-6 col-md-3">';
    $html .= '<label>Người dùng</label>';
    $html .= '<input class="form-control" type="text" id="search_username" name="search_username" maxlength="100" title="" tabindex="-1" accesskey="9">';
    $html .= '</div>';
    $html .= '<div style="margin-top: 10px" class="col-xs-12 col-sm-6 col-md-3">';
    $booking_no = isset($_POST['search_booking_no']) ? $_POST['search_booking_no'] : '';
    $html .= '<input class="form-control" type="text" id="search_booking_no" name="search_booking_no" maxlength="100" title="" tabindex="-1" accesskey="9" placeholder="Nhập số booking" value="' . htmlspecialchars($booking_no) . '">';
    $agent_no = isset($_POST['search_agent_no']) ? $_POST['search_agent_no'] : '';
    $html .= '<br><input class="form-control" type="text" id="search_agent_no" name="search_agent_no" maxlength="100" title="" tabindex="-1" accesskey="9" placeholder="Nhập mã đại lý" value="' . htmlspecialchars($agent_no) . '">';
    $html .= '</div>';
    // Di chuyển phần chọn người dùng vào trong cùng một hàng với ngày tháng
    $html .= '<div style="margin-top: -15px" class="col-xs-12 col-sm-6 col-md-3">';
    $html .= '<label>' . $mod_strings['LBL_USERS'] . '</label>';
    $html .= '<select name="users[]" id ="username" multiple class="form-control" style="height: 150px; font-size: 15px;">';

    while ($row = $user->db->fetchByAssoc($res)) {
        $selected = (isset($_POST['users']) && in_array($row['refuser'], $_POST['users'])) ? 'selected' : '';
        $html .= '<option value="' . $row['refuser'] . '" ' . $selected . '>' . $row['name'] . '</option>';
    }
    $html .= '</select>';

    $html .= '</div>';
    $html .= '</div>'; // Close row

    $html .= '<div class="row" style="width: 70%">';
    $html .= '<div class="col-xs-12 col-sm-4 col-md-2">';
    $html .= '<button id="sm_all" name="sm_all" type="submit" class="button">' . $mod_strings['LBL_STATISTICAL'] . '</button>';
    $html .= '</div>';

    $html .= '<div class="col-xs-12 col-sm-4 col-md-2">';
    $html .= '<button id="clear_filter" name="clear_filter" type="button" class="button">' . $mod_strings['LBL_CLEAR'] . '</button>';
    $html .= '</div>';

    $html .= '<div class="col-xs-12 col-sm-4 col-md-2">';
    $html .= '<button id="export_excel" type="button" style ="background-color: #13b328; color: white" class="btn">' . $mod_strings['LBL_EXPORT_EXCEL'] . '</button>';
    $html .= '</div>';

    $html .= '</div>'; // Close row

    $html .= '</form>';
    $html.='<div id="spinner"></div>';
    $html .= '</div>'; // Close col
    $html .= '</div>'; // Close row
    $html .= '</div>'; // Close cde

    return $html;
}
function compare_type_service($a, $b) {
    return $a['type_service'] <=> $b['type_service'];
}
