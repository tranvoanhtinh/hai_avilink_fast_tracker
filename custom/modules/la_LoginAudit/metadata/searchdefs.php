<?php
$module_name = 'la_LoginAudit';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'modified_user_id' => 
      array (
        'type' => 'assigned_user_name',
        'label' => 'LBL_MODIFIED',
        'width' => '10%',
        'default' => true,
        'name' => 'modified_user_id',
      ),
      'date_entered' => 
      array (
        'type' => 'datetime',
        'label' => 'LBL_DATE_ENTERED',
        'width' => '10%',
        'default' => true,
        'name' => 'date_entered',
      ),
      'result' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_RESULT',
        'width' => '10%',
        'default' => true,
        'name' => 'result',
      ),
    ),
    'advanced_search' => 
    array (
      'modified_user_id' => 
      array (
        'type' => 'assigned_user_name',
        'label' => 'LBL_MODIFIED',
        'width' => '10%',
        'default' => true,
        'name' => 'modified_user_id',
      ),
      'result' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_RESULT',
        'width' => '10%',
        'default' => true,
        'name' => 'result',
      ),
      'typed_name' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_TYPED_NAME',
        'width' => '10%',
        'default' => true,
        'name' => 'typed_name',
      ),
      'is_admin' => 
      array (
        'type' => 'bool',
        'default' => true,
        'label' => 'LBL_IS_ADMIN',
        'width' => '10%',
        'name' => 'is_admin',
      ),
      'ip_address' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_IP_ADDRESS',
        'width' => '10%',
        'default' => true,
        'name' => 'ip_address',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
;
?>
