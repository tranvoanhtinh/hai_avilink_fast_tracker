<?php
// created: 2024-03-22 10:05:15
$mod_strings = array (
  'LBL_DATE_ENTERED' => 'Ngày & giờ đăng nhập',
  'LBL_IP_ADDRESS' => 'Địa chỉ IP',
  'LBL_IS_ADMIN' => 'Là người dùng quản trị?',
  'LBL_RESULT' => 'Tình trạng',
  'LBL_TYPED_NAME' => 'Tên đăng nhập',
  'LBL_MODIFIED_NAME' => 'Tên người dùng',
  'LBL_MODIFIED' => 'Người dùng',
);