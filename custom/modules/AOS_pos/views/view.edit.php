<?php
require_once('include/MVC/View/views/view.edit.php');

class CustomAOS_posViewEdit extends ViewEdit
{
    function display()
    {
        parent::display(); // Gọi phương thức hiển thị từ lớp cha
        ?>
        <script type="text/javascript" src="custom/modules/AOS_pos/js/display_pos.js"></script>
        <script>
             var id2 = '';
            function check_hidden_id() {
                var id = document.getElementById('billing_account_id').value;
                if (id !== id2) {
                    display_aos_pos('billing_account_id', true);
                   id2 = id;
                }
            }
           setInterval(check_hidden_id, 300);
            var phone2 = '';
            function check_hidden_phone() {
                var phone = document.getElementById('mobileref').value;
                if (phone !== phone2) {
                    display_aos_pos('mobileref', true);
                    phone2 = phone;
                }
                
            }
           setInterval(check_hidden_phone, 300);
        </script>

        <?php
    }
}
?>
