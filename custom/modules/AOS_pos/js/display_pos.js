
var check_duplicate = 0;
var duplicate = document.querySelector('input[name="duplicateSave"]');
if(duplicate){
    if(duplicate.value =='true'){
        check_duplicate = 1;  // đang trùng lập
    }
}

var input_name = document.getElementById('name'); // Lấy thẻ input bằng ID
var input_date = document.getElementById('voucherdate'); // Lấy thẻ input bằng ID
var voucherdate_trigger = document.getElementById('voucherdate_trigger'); // Lấy thẻ trigger bằng ID
var intervalId;

function get_voucher_code() {
    var value_date = input_date.value;
    $.ajax({
        type: "POST",
        url: "index.php?entryPoint=get_voucher_code",
        data: {
            module: 'aos_pos',
            voucherdate: value_date,
           
        },
        success: function(data) {
            input_name.value = data;
            input_name.style.color = "blue";
            input_name.style.fontWeight = 'bold';
        },
        error: function(error) {
            // Xử lý lỗi nếu cần
        }
    });
}

// Sự kiện khi click vào phần tử có id là 'voucherdate_trigger'
voucherdate_trigger.addEventListener('click', function() {
    var inputElement = document.getElementById('voucherdate');
    var oldValueDate = inputElement.value;

    // Xóa interval cũ nếu có
    clearInterval(intervalId);

    // Thiết lập một hàm được gọi mỗi giây
    var intervalId = setInterval(function() {
        var newValue = inputElement.value;
        // So sánh giá trị mới với giá trị cũ
        if (newValue !== oldValueDate) {
            // Nếu giá trị đã thay đổi, gọi hàm xử lý thích hợp ở đây
            document.getElementById('remaining').textContent='';
            get_voucher_code();
            // Cập nhật giá trị cũ
            oldValueDate = newValue;
        }
    }, 500);
});


if(input_name.value ==''){
    get_voucher_code();
}
if(check_duplicate == 1){
    get_voucher_code();
}





//-----HÀM kiểm tra MÃ tồn tại
//---check_voucher_code
var fieldcode = 'name';
var oldValue ='';
var editViewFieldDiv = document.querySelector('.edit-view-field[type="'+fieldcode+'"][field="'+fieldcode+'"]');
if (editViewFieldDiv) {
    var pElement = document.createElement('p');
    pElement.textContent = '';
    pElement.id = 'remaining';
    editViewFieldDiv.appendChild(pElement);
}

if(check_duplicate == 1){
        firstvalue = -999;
}else {
    var firstvalue; 
    document.addEventListener("DOMContentLoaded", function() {
        var inputElement = document.getElementById(fieldcode);
        if (inputElement) {
            firstvalue = inputElement.value; 
        }
    });
}

document.getElementById(fieldcode).addEventListener("focus", function() {
    // Lưu trữ giá trị hiện tại của input khi focus vào
    oldValue = this.value;
    this.style.color = "black";
    console.log(firstvalue);
    check_voucher_code(fieldcode,'aos_pos',oldValue,firstvalue);
});

document.getElementById(fieldcode).addEventListener("blur", function() {
      console.log(firstvalue);
    check_voucher_code(fieldcode,'aos_pos',oldValue,firstvalue);

});

// KH + SĐT
var id2 = "";
var intervalId1;
var Account = "billing_account";
var Account_id = 'billing_account_id';
var btn_choose = 'btn_billing_account';
var btn_choose_clear = 'btn_clr_billing_account';
var module_dir = 'Accounts';
var mobile = 'mobileref';

// Hàm lấy tham chiếu đến phần tử HTML
const getElement = (id) => document.getElementById(id);

// Xử lý khi nhấn vào nút
const handleClick = () => {
  clearInterval(intervalId1);
  intervalId1 = setInterval(check_hidden_id, 500);
};

// Xử lý khi phần tử "billing_account" nhận focus
const handleFocus = () => {
  intervalId1 = setInterval(check_hidden_id, 500);
};

// Xử lý khi phần tử "billing_account" mất focus
const handleBlur = () => {
  var id = getElement(Account_id);
  if (id.value !== id2) {
    display_info(Account_id, true, module_dir);
    id2 = id.value;
    clearInterval(intervalId1);
  }
};

// Xử lý khi phần tử "mobileref" mất focus
const handleMobileBlurInput = () => {
  display_info(mobile, true, module_dir);
};

// Xử lý khi phần tử "mobileref" nhập
const handleMobileInput = () => {
  display_info(mobile, true, module_dir);
};

// Xử lý khi nhấn vào nút "Clear"
const clearInputs = () => {
  var elementsToClear = [Account_id, Account, "billing_address_street", "shipping_address_street", mobile];
  elementsToClear.forEach(id => {
    var element = getElement(id);
    if (element) {
      element.value = "";
    }
  });
};

// Hàm kiểm tra hidden id
const check_hidden_id = () => {
  var id = getElement(Account_id);
  if (id.value !== id2) {
    display_info(Account_id, true, module_dir);
    id2 = id.value;
    clearInterval(intervalId1);
  }
};

// Hàm hiển thị hóa đơn
const display_info = (input, i, md) => {
  if (i === true) {
    var record = getElement(input).value;
    var type = input == Account_id ? "id" : "phone";
    $.ajax({
      type: "POST",
      url: "index.php?entryPoint=display_phone",
      data: {
        md: md,
        type: type,
        record: record,
      },
        success: function (data) {
            console.log(data);
            if (data != -1) {
                var datas = JSON.parse(data);

                // Kiểm tra và gán giá trị cho phần tử nếu tồn tại
                if (getElement(Account_id)) {
                    getElement(Account_id).value = datas.id;
                }
                if (getElement(mobile)) {
                    getElement(mobile).value = datas.phone;
                }
                if (getElement(Account)) {
                    getElement(Account).value = datas.billing_account;
                }
                if (document.getElementById("billing_address_street")) {
                    document.getElementById("billing_address_street").value = datas.billing_address_street;
                }
                if (document.getElementById("shipping_address_street")) {
                    document.getElementById("shipping_address_street").value = datas.shipping_address_street;
                }
            } else {
                // Xử lý nếu dữ liệu trả về là -1
                if (document.getElementById("billing_address_street")) {
                    document.getElementById("billing_address_street").value = "";
                }
                if (document.getElementById("shipping_address_street")) {
                    document.getElementById("shipping_address_street").value = "";
                }
            }
        },
      error: function (error) {
        // Xử lý lỗi nếu cần
      },
    });
  }
};

// Xử lý sự kiện khi nhấn vào nút
getElement(btn_choose).addEventListener("click", handleClick);

// Xử lý sự kiện khi phần tử "billing_account" nhận focus
getElement(Account).addEventListener('focus', handleFocus);

// Xử lý sự kiện khi phần tử "billing_account" mất focus
getElement(Account).addEventListener('blur', handleBlur);

// Xử lý sự kiện khi phần tử "mobileref" mất focus
getElement(mobile).addEventListener("blur", handleMobileBlurInput);

// Xử lý sự kiện khi phần tử "mobileref" nhập
getElement(mobile).addEventListener("input", handleMobileInput);

// Xử lý sự kiện khi nhấn vào nút "Clear"
getElement(btn_choose_clear).addEventListener("click", clearInputs);




function removeCommaAndDecimal(input) {
    // Loại bỏ dấu phẩy (,)
    var withoutComma = input.replace(/,/g, '');
    // Loại bỏ .00 nếu có
    var withoutDecimal = withoutComma.replace(/\.00$/, '');
    return withoutDecimal;
}

var discountInput = document.getElementById("total_discount_amount");
if (discountInput) {
    discountInput.addEventListener("blur", function() {
        var inputValue = discountInput.value.trim(); // Loại bỏ khoảng trắng từ đầu và cuối chuỗi
        if (inputValue !== "") {
            var numericValue = parseFloat(removeCommaAndDecimal(inputValue));
            if (numericValue > 0) {
                numericValue *= -1; // Đảo dấu nếu giá trị là dương
            }
            discountInput.value = format2Number(numericValue);
        }
        calculateTotal2(2, '');
    });
}

var advance_payment = document.getElementById("advance_payment");
if (advance_payment) {
    advance_payment.addEventListener("blur", function() {
        advance_payment.value = format2Number(removeCommaAndDecimal(advance_payment.value));
        calculateTotal2(2, ''); 
    });
}

var discountPercentSelect = document.getElementById("discount_percent");
var total_amt = document.getElementById("lp_total_amt");
if (discountPercentSelect && total_amt) {
    discountPercentSelect.addEventListener("change", function() {
        var selectedValue = parseFloat(discountPercentSelect.value);
        var currentValue = parseFloat(total_amt.value.replace(/,/g, '')); // Chuyển đổi giá trị thành số và loại bỏ dấu phẩy
        var calculatedDiscount = (currentValue * selectedValue) / -100;
        discountInput.value = format2Number(calculatedDiscount);

        calculateTotal2(2, '');
    });
}

