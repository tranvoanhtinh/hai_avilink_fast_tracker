<?php
$module_name = 'AOS_pos';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'SAVE',
          1 => 'CANCEL',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'LBL_INVOICE_TO' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_OVERVIEW' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_LINE_ITEMS' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'LBL_INVOICE_TO' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'billing_account',
            'studio' => 'visible',
            'label' => 'LBL_BILLING_ACCOUNT',
          ),
          1 => 
          array (
            'name' => 'mobileref',
            'label' => 'LBL_MOBILE_REF',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'billing_contact',
            'studio' => 'visible',
            'label' => 'LBL_BILLING_CONTACT',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'billing_address_street',
            'comment' => 'The street address used for billing address',
            'label' => 'LBL_BILLING_ADDRESS_STREET',
          ),
          1 => 
          array (
            'name' => 'shipping_address_street',
            'comment' => 'The street address used for for shipping purposes',
            'label' => 'LBL_SHIPPING_ADDRESS_STREET',
          ),
        ),
      ),
      'LBL_PANEL_OVERVIEW' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'name',
            'displayParams' => 
            array (
              'required' => true,
            ),
            'label' => 'LBL_NAME',
          ),
          1 => 
          array (
            'name' => 'customer_ref',
            'label' => 'LBL_CUSTOMER_REF',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'status_inventory',
            'studio' => true,
            'label' => 'LBL_STATUS_INVENTORY',
          ),
          1 => 
          array (
            'name' => 'voucherdate',
            'label' => 'LBL_VOUCHER_DATE',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'invoice_date',
            'label' => 'LBL_INVOICE_DATE',
          ),
          1 => 
          array (
            'name' => 'quote_date',
            'label' => 'LBL_QUOTE_DATE',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'payment_date',
            'label' => 'LBL_PAYMENT_DATE',
          ),
          1 => 
          array (
            'name' => 'pi_effective_date',
            'label' => 'LBL_PI_EFFECTIVE_DATE',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'due_date',
            'label' => 'LBL_DUE_DATE',
          ),
          1 => 
          array (
            'name' => 'cashier',
            'label' => 'LBL_CASHIER',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'delivery_date',
            'label' => 'LBL_DELIVERY_DATE',
          ),
          1 => 
          array (
            'name' => 'estimated_delivery_date',
            'label' => 'LBL_ESTIMATED_DELIVERY_DATE',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'status',
            'label' => 'LBL_STATUS',
          ),
          1 => 
          array (
            'name' => 'statustransport',
            'label' => 'LBL_STATUSTRANSPORT',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'is_invoice',
            'label' => 'LBL_ISINVOICE',
          ),
          1 => 
          array (
            'name' => 'assigned_user_name',
            'label' => 'LBL_ASSIGNED_TO_NAME',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'paymentmethod',
            'label' => 'LBL_PAYMENTMETHOD',
          ),
          1 => 
          array (
            'name' => 'ads',
            'label' => 'LBL_ADS',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'is_commercial',
            'label' => 'LBL_IS_COMMERCIAL',
          ),
          1 => 
          array (
            'name' => 'take_from',
            'label' => 'LBL_TAKE_FROM',
          ),
        ),
        10 => 
        array (
          0 => 
          array (
            'name' => 'is_non_commercial',
            'label' => 'LBL_IS_NON_COMMERCIAL',
          ),
          1 => 
          array (
            'name' => 'type_invoice',
            'studio' => true,
            'label' => 'LBL_TYPE_INVOICE',
          ),
        ),
        11 => 
        array (
          0 => 
          array (
            'name' => 'trade_term',
            'label' => 'LBL_TRADE_TERM',
          ),
          1 => 
          array (
            'name' => 'shipping_method',
            'label' => 'LBL_SHIPPING_METHOD',
          ),
        ),
        12 => 
        array (
          0 => 
          array (
            'name' => 'description',
            'label' => 'LBL_DESCRIPTION',
          ),
          1 => 
          array (
            'name' => 'summary_product',
            'label' => 'LBL_SUMMARY_PRODUCTS',
          ),
        ),
      ),
      'lbl_line_items' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'currency_id',
            'studio' => 'visible',
            'label' => 'LBL_CURRENCY',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'pos_line_items',
            'label' => 'LBL_POS_LINE_ITEMS',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'lp_total_amt',
            'label' => 'LBL_LP_TOTAL_AMT',
          ),
          1 => '',
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'lp_discount_amount',
            'label' => 'LBL_LP_DISCOUNT_AMOUNT',
          ),
          1 => '',
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'discount_percent',
            'label' => 'LBL_DISCOUNT_PERCENT',
          ),
          1 => 
          array (
            'name' => 'total_discount_amount',
            'label' => 'LBL_TOTAL_DISCOUNT_AMOUNT',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'lp_subtotal_amount',
            'label' => 'LBL_LP_SUBTOTAL_AMOUNT',
          ),
          1 => '',
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'lp_shipping_amount',
            'label' => 'LBL_LP_SHIPPING_AMOUNT',
            'displayParams' => 
            array (
              'field' => 
              array (
                'onblur' => 'calculateTotal(\'lineItems\');',
              ),
            ),
          ),
          1 => '',
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'shipping_tax_amt',
            'label' => 'LBL_SHIPPING_TAX_AMT',
          ),
          1 => '',
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'lp_tax_amount',
            'label' => 'LBL_LP_TAX_AMOUNT',
          ),
          1 => '',
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'lp_total_amount',
            'label' => 'LBL_LP_TOTAL_AMOUNT',
          ),
          1 => '',
        ),
        10 => 
        array (
          0 => 
          array (
            'name' => 'advance_payment',
            'label' => 'LBL_ADVANCE_PAYMENT',
          ),
          1 => 
          array (
            'name' => 'remaining_money',
            'label' => 'LBL_REMAINING_MONEY',
          ),
        ),
      ),
    ),
  ),
);
;
?>
