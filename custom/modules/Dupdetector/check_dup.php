<?php
/*
   Created By : Urdhva Tech Pvt. Ltd.
 Created date : 04/23/2018
   Contact at : contact@urdhva-tech.com
          Web : www.urdhva-tech.com
        Skype : urdhvatech
       Module : Dupdetector 1.3
*/
/***
* check for duplicate value
*/

if(!empty($_REQUEST['module_name']) && !empty($_REQUEST['field_name']) && !empty($_REQUEST['field_value'])) {
    require_once("custom/modules/Dupdetector/dupdetector_utils.php");
    global $dictionary;
	global $db;
	if($_REQUEST['field_name'] == "phone_mobile" || $_REQUEST['field_name'] == "phone_alternate" || $_REQUEST['field_name'] == "phone_other" ){
	if($_REQUEST['module_name']=="Leads" || $_REQUEST['module_name']=="Accounts" || $_REQUEST['module_name']=="Prospects"){
	$module_names=array("Leads","Accounts","Prospects");
	}else{
	$module_names=array("Contacts");	
		
	}
    $field_name = $_REQUEST['field_name'];
    $module_name = $_REQUEST['module_name']; 
	$admin=new Administration();
	foreach($module_names as $module_nam){
		$oModule = BeanFactory::getBean($module_nam);
		$arg=array();
		if($module_nam == "Leads" || $module_nam == "Prospects" || $module_nam == "Contacts"){
		$arg["phone_mobile"] = trim($_REQUEST['field_value']);
		$arg["phone_home"] = trim($_REQUEST['field_value']);
		$arg["phone_other"] = trim($_REQUEST['field_value']);
		}
		else{
		$arg["phone_alternate"] = trim($_REQUEST['field_value']);
		}
		$data_array = get_duplicate_string($oModule,$arg,$module_nam);
		$oModule->retrieve_by_string_fields($arg);
		
        $admin->retrieveSettings('checkdup',true);
        $prevent_settings = false;
        if(isset($admin->settings['checkdup_prevent_submit']) && $admin->settings['checkdup_prevent_submit'] == true)
            $prevent_settings = true;
		
		if(!empty($data_array)) {

			foreach($data_array as $key => $aData) {
				if(($_REQUEST['record_id'] != $aData['id'])){
					if(isset($dictionary[$oModule->object_name]['templates']['person'])) {
                    $name = $aData['first_name']." ".$aData['last_name'];
					}
					else if(!empty($aData['name'])) {
						$name = $aData['name'];
					}
				else
                    $name = $aData[$field_name];
                    $query="select user_name from users where id='".$aData["assigned_user_id"]."'";
					$res=$db->query($query);
					$row=$db->fetchByAssoc($res);
					
                $link ="<a href=index.php?module=".$module_nam."&action=DetailView&record=".$aData['id']." target='_blank'>".$name."-".$module_nam."-".$row["user_name"]."</a><br />";
				}	   
			}
		
			if(!empty($link)){			
				$json_en = json_encode(array("status"=>$link,"settings"=>$prevent_settings));
				break;
			} 
			else
			{ // echo json_encode(array("status"=>"",'settings'=>$prevent_settings)); 
				$json_en = json_encode(array("status"=>"","settings"=>$prevent_settings)); 
				break;
			}
		
		
		}
		else 
		{
        $json_en=json_encode(array("status"=>"","settings"=>$prevent_settings));
		}
		
	}
		echo $json_en;
	}
	else if($_REQUEST['field_name'] == "identitycard_c"){
		if($_REQUEST['module_name']=="Leads" || $_REQUEST['module_name']=="Accounts" || $_REQUEST['module_name']=="Prospects"){
	$module_names=array("Leads","Accounts","Prospects");
	}else{
	$module_names=array("Contacts");	
		
	}
    $field_name = $_REQUEST['field_name'];
    $module_name = $_REQUEST['module_name']; 
	$admin=new Administration();
	foreach($module_names as $module_nam){
		$oModule = BeanFactory::getBean($module_nam);
		$arg=array();
		$arg["identitycard_c"] = trim($_REQUEST['field_value']);
		$data_array = get_duplicate_string($oModule,$arg,$module_nam);
		$oModule->retrieve_by_string_fields($arg);
		
        $admin->retrieveSettings('checkdup',true);
        $prevent_settings = false;
        if(isset($admin->settings['checkdup_prevent_submit']) && $admin->settings['checkdup_prevent_submit'] == true)
            $prevent_settings = true;
		
		if(!empty($data_array)) {

			foreach($data_array as $key => $aData) {
				if(($_REQUEST['record_id'] != $aData['id'])){
					if(isset($dictionary[$oModule->object_name]['templates']['person'])) {
                    $name = $aData['first_name']." ".$aData['last_name'];
					}
					else if(!empty($aData['name'])) {
						$name = $aData['name'];
					}
				else
                    $name = $aData[$field_name];
                    $query="select user_name from users where id='".$aData["assigned_user_id"]."'";
					$res=$db->query($query);
					$row=$db->fetchByAssoc($res);
					
                $link ="<a href=index.php?module=".$module_nam."&action=DetailView&record=".$aData['id']." target='_blank'>".$name."-".$module_nam."-".$row["user_name"]."</a><br />";
				}	   
			}
		
			if(!empty($link)){			
				$json_en = json_encode(array("status"=>$link,"settings"=>$prevent_settings));
				break;
			} 
			else
			{ // echo json_encode(array("status"=>"",'settings'=>$prevent_settings)); 
				$json_en = json_encode(array("status"=>"","settings"=>$prevent_settings)); 
				break;
			}
		
		
		}
		else 
		{
        $json_en=json_encode(array("status"=>"","settings"=>$prevent_settings));
		}
		
	}
		echo $json_en;
	}
	else if($_REQUEST['field_name'] == "vatcode_c" || $_REQUEST['field_name'] == "tax_code"){
		if($_REQUEST['module_name']=="Leads" || $_REQUEST['module_name']=="Accounts" || $_REQUEST['module_name']=="Prospects"){
			$module_names=array("Leads","Accounts","Prospects");
		}else{
			$module_names=array("Contacts");
		}
		$field_name = $_REQUEST['field_name'];
		$module_name = $_REQUEST['module_name']; 
		$admin=new Administration();
	foreach($module_names as $module_nam){
		$oModule = BeanFactory::getBean($module_nam);
		$arg=array();
		if($module_nam == "Leads" || $module_nam == "Prospects" || $module_nam == "Contacts"){
			$arg["vatcode_c"] = trim($_REQUEST['field_value']);
		}else{
			$arg["tax_code"] = trim($_REQUEST['field_value']);	
		}
		//if(($_REQUEST['field_name'] == "vatcode_c" && $module_nam == "Leads") || ($_REQUEST['field_name'] == "tax_code" && $module_nam == "Accounts")){
			$data_array = get_duplicate_string($oModule,$arg,$module_nam);
			$oModule->retrieve_by_string_fields($arg);
			
			$admin->retrieveSettings('checkdup',true);
			$prevent_settings = false;
			if(isset($admin->settings['checkdup_prevent_submit']) && $admin->settings['checkdup_prevent_submit'] == true)
				$prevent_settings = true;
			
			if(!empty($data_array)) {

				foreach($data_array as $key => $aData) {
					if(($_REQUEST['record_id'] != $aData['id'])){
						if(isset($dictionary[$oModule->object_name]['templates']['person'])) {
						$name = $aData['first_name']." ".$aData['last_name'];
						}
						else if(!empty($aData['name'])) {
							$name = $aData['name'];
						}
					else
						$name = $aData[$field_name];
						$query="select user_name from users where id='".$aData["assigned_user_id"]."'";
						$res=$db->query($query);
						$row=$db->fetchByAssoc($res);
						
					$link ="<a href=index.php?module=".$module_nam."&action=DetailView&record=".$aData['id']." target='_blank'>".$name."-".$module_nam."-".$row["user_name"]."</a><br />";
					}	   
				}
			
				if(!empty($link)){			
					$json_en = json_encode(array("status"=>$link,"settings"=>$prevent_settings));
					break;
				} 
				else
				{ // echo json_encode(array("status"=>"",'settings'=>$prevent_settings)); 
					$json_en = json_encode(array("status"=>"","settings"=>$prevent_settings)); 
					break;
				}
			
			
			}
			else 
			{
			$json_en=json_encode(array("status"=>"","settings"=>$prevent_settings));
			}
		//}
		
	}
		echo $json_en;
	}
	else
	{
		
		global $db;
		$query="select email_addr_bean_rel.bean_module,email_addr_bean_rel.bean_id from email_addresses inner join email_addr_bean_rel on email_addresses.id=email_addr_bean_rel.email_address_id where email_addresses.email_address='".$_REQUEST['field_value']."' and email_addr_bean_rel.deleted='0'"; 
		
		$res=$db->query($query);
		$row=$db->fetchByAssoc($res);
		if(!empty($row)){
		do{
			
			if($row["bean_module"]!= "Users"){
				if($row["bean_module"]== "Leads"){
					$query2 ="select leads.id,leads.first_name,leads.last_name,leads.status from  leads where leads.id ='".$row['bean_id']."' and leads.deleted='0'";
				
				}
				else if ($row["bean_module"]== "Accounts")
				{
				
					$query2="select accounts.id,accounts.name from accounts where accounts.id ='".$row['bean_id']."' and accounts.deleted='0'";
				
				}
				else if($row["bean_module"]== "Prospects"){
				
				
					//$query2="select contacts.id,contacts.first_name,contacts.last_name from  contacts where contacts.id ='".$row['bean_id']."'";
					$query2 ="select prospects.id,prospects.first_name,prospects.last_name from  prospects where prospects.id ='".$row['bean_id']."' and prospects.deleted='0'";
				}else if($row["bean_module"]== "Contacts"){
					$query3="select contacts.id,contacts.first_name,contacts.last_name from  contacts where contacts.id ='".$row['bean_id']."' and contacts.deleted='0'";
					
				}
			}
			
			if(!empty($query2)){
			
				$res2=$db->query($query2);
				$row2=$db->fetchByAssoc($res2);
		
				if(!empty($row2)){
					
					if($row["bean_module"]== "Leads" || $row["bean_module"] == "Prospects" ){
						$name=$row2["first_name"]." ".$row2["last_name"];
				
					}else{
						$name=$row2["name"];
				
					}
					if($_REQUEST['module_name']!="Contacts"){
					if($_REQUEST['module_name'] == $row["bean_module"]){
						if($_REQUEST['record_id']!=$row2['id'] ){
							$link ="<a href=index.php?module=".$row["bean_module"]."&action=DetailView&record=".$row2['id']." target='_blank'>".$name."-".$row["bean_module"]."</a><br />";
							$json_en=json_encode(array("status"=>$link,"settings"=>""));//trùng
							break;
						}
						else if($_REQUEST['record_id']==$row2['id']){
							
							$json_en=json_encode(array("status"=>"","settings"=>""));//itself
							
						}
					}
					else{
						if($row["bean_module"] == "Leads"){
							if($row2['status'] == "Converted" || $row2['status'] == "Transaction"){
								
							$json_en=json_encode(array("status"=>"","settings"=>""));	
								
							}else{
								
							$link ="<a href=index.php?module=".$row["bean_module"]."&action=DetailView&record=".$row2['id']." target='_blank'>".$name."-".$row["bean_module"]."</a><br />";	
							$json_en=json_encode(array("status"=>$link,"settings"=>""));	
								
								
							}
							
							
						}else{
							if($row["bean_module"] == "Prospects"){
								$json_en=json_encode(array("status"=>"","settings"=>""));	
							}else{
								$link ="<a href=index.php?module=".$row["bean_module"]."&action=DetailView&record=".$row2['id']." target='_blank'>".$name."-".$row["bean_module"]."</a><br />";	
								$json_en=json_encode(array("status"=>$link,"settings"=>""));
								break;
							}
						}
						//$json_en=json_encode(array("status"=>"","settings"=>""));
					}
					
				}else if($_REQUEST['module_name']=="Contacts"){
					
					
				$json_en=json_encode(array("status"=>"","settings"=>""));
				
				
					
					
				}
					
					
				}
				$query2='';
			}
				
			else{
				if(!empty($query3)){
					
					$name=$row2["first_name"]." ".$row2["last_name"];
					if($_REQUEST['module_name'] == $row["bean_module"]){
						if($_REQUEST['record_id']!=$row2['id'] ) {
							$link ="<a href=index.php?module=".$row["bean_module"]."&action=DetailView&record=".$row2['id']." target='_blank'>".$name."-".$row["bean_module"]."</a><br />";
							$json_en=json_encode(array("status"=>$link,"settings"=>""));//trùng
							break;
						}
						else if($_REQUEST['record_id']==$row2['id']){
							if($_REQUEST['module_name'] == $row["bean_module"]){	
							$json_en=json_encode(array("status"=>"","settings"=>""));//itself
							}
						}
					}else{
						
						$json_en=json_encode(array("status"=>"","settings"=>""));
					}
					
				}else{
				$json_en=json_encode(array("status"=>"","settings"=>""));
				
				}
				$query3='';
			}
			
			}while($row=$db->fetchByAssoc($res));
			
			echo $json_en;
		}
		else{
			
			$json_en=json_encode(array("status"=>"","settings"=>""));
			echo $json_en;
			
			
		}
		
		
		
		
		
		
	}
}
?>