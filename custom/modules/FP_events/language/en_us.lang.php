<?php
// created: 2024-06-05 06:17:16
$mod_strings = array (
  'LBL_FP_EVENTS_LEADS_1_FROM_LEADS_TITLE' => 'Leads',
  'LBL_FP_EVENTS_CONTACTS_FROM_CONTACTS_TITLE' => 'Contacts',
  'LBL_FP_EVENTS_PROSPECTS_1_FROM_PROSPECTS_TITLE' => 'Targets',
  'LBL_SHORTNAME' => 'ShortName',
  'LBL_VATCODE' => 'VatCode',
  'LBL_SKYPENAME' => 'SkypeName',
);