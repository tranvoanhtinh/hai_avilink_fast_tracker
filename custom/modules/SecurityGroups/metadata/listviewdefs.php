<?php
$module_name = 'SecurityGroups';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => true,
  ),
  'PROVINCECODE_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_PROVINCECODE',
    'width' => '10%',
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'default' => true,
  ),
  'SECURITYGROUP_PRIMARY_GROUP' => 
  array (
    'type' => 'bool',
    'label' => 'LBL_PRIMARY_GROUP',
    'width' => '10%',
    'default' => false,
  ),
  'ONLY_ASSIGN_RECORD_TO_THIS_GROUP' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'LBL_ONLY_ASSIGN_RECORD_TO_THIS_GROUP_FLAG',
    'width' => '10%',
  ),
  'NONINHERITABLE' => 
  array (
    'width' => '9%',
    'label' => 'LBL_NONINHERITABLE',
    'default' => false,
  ),
  'IS_PARENT_GROUP' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'LBL_IS_PARENT_GROUP',
    'width' => '10%',
  ),
);
;
?>
