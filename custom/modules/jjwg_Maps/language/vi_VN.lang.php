<?php
// created: 2019-05-13 18:13:11
$mod_strings = array (
  'LBL_ACCOUNTS' => 'Accounts',
  'LBL_LEADS' => 'Leads',
  'LBL_OPPORTUNITIES' => 'Opportunities',
  'LBL_PROSPECTS' => 'Prospects',
  'LBL_CONTACTS' => 'Contacts',
  'LBL_CASES' => 'Cases',
  'LBL_PROJECTS' => 'Projects',
  'LBL_MEETINGS' => 'Meetings',
);