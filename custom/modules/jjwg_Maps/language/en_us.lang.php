<?php
// created: 2024-06-05 06:17:19
$mod_strings = array (
  'LBL_ACCOUNTS' => 'Agent',
  'LBL_LEADS' => 'Leads',
  'LBL_CONTACTS' => 'Contacts',
  'LBL_OPPORTUNITIES' => 'Opportunities',
  'LBL_CASES' => 'Ticketss',
  'LBL_MEETINGS' => 'Meetings',
  'LBL_PROJECTS' => 'Projects',
  'LBL_PROSPECTS' => 'Prospects',
);