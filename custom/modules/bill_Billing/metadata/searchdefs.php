<?php
$module_name = 'bill_Billing';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'bill_billing_accounts_name' => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_BILL_BILLING_ACCOUNTS_FROM_ACCOUNTS_TITLE',
        'id' => 'BILL_BILLING_ACCOUNTSACCOUNTS_IDA',
        'width' => '10%',
        'default' => true,
        'name' => 'bill_billing_accounts_name',
      ),
      'current_user_only' => 
      array (
        'name' => 'current_user_only',
        'label' => 'LBL_CURRENT_USER_FILTER',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
      ),
    ),
    'advanced_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'aos_bookings_bill_billing_1_name' => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_AOS_BOOKINGS_BILL_BILLING_1_FROM_AOS_BOOKINGS_TITLE',
        'id' => 'AOS_BOOKINGS_BILL_BILLING_1AOS_BOOKINGS_IDA',
        'width' => '10%',
        'default' => true,
        'name' => 'aos_bookings_bill_billing_1_name',
      ),
      'billing_status' => 
      array (
        'type' => 'enum',
        'studio' => 'visible',
        'label' => 'LBL_BILLING_STATUS',
        'width' => '10%',
        'default' => true,
        'name' => 'billing_status',
      ),
      'bill_billing_accounts_name' => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_BILL_BILLING_ACCOUNTS_FROM_ACCOUNTS_TITLE',
        'id' => 'BILL_BILLING_ACCOUNTSACCOUNTS_IDA',
        'width' => '10%',
        'default' => true,
        'name' => 'bill_billing_accounts_name',
      ),
      'paymentmethod' => 
      array (
        'type' => 'enum',
        'studio' => 'visible',
        'label' => 'LBL_PAYMENTMETHOD',
        'width' => '10%',
        'default' => true,
        'name' => 'paymentmethod',
      ),
      'type' => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_TYPE',
        'width' => '10%',
        'name' => 'type',
      ),
      'billingdate' => 
      array (
        'type' => 'date',
        'label' => 'LBL_BILLINGDATE',
        'width' => '10%',
        'default' => true,
        'name' => 'billingdate',
      ),
      'date_entered' => 
      array (
        'type' => 'datetime',
        'label' => 'LBL_DATE_ENTERED',
        'width' => '10%',
        'default' => true,
        'name' => 'date_entered',
      ),
      'assigned_user_id' => 
      array (
        'name' => 'assigned_user_id',
        'label' => 'LBL_ASSIGNED_TO',
        'type' => 'enum',
        'function' => 
        array (
          'name' => 'get_user_array',
          'params' => 
          array (
            0 => false,
          ),
        ),
        'default' => true,
        'width' => '10%',
      ),
      'current_user_only' => 
      array (
        'label' => 'LBL_CURRENT_USER_FILTER',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
        'name' => 'current_user_only',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
;
?>
