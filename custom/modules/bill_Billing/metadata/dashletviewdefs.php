<?php
$dashletData['bill_BillingDashlet']['searchFields'] = array (
  'billing_status' => 
  array (
    'default' => '',
  ),
  'type' => 
  array (
    'default' => '',
  ),
  'paymentmethod' => 
  array (
    'default' => '',
  ),
  'billingdate' => 
  array (
    'default' => '',
  ),
  'bill_billing_accounts_name' => 
  array (
    'default' => '',
  ),
  'date_entered' => 
  array (
    'default' => '',
  ),
  'assigned_user_name' => 
  array (
    'default' => '',
  ),
);
$dashletData['bill_BillingDashlet']['columns'] = array (
  'bill_billing_accounts_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_BILL_BILLING_ACCOUNTS_FROM_ACCOUNTS_TITLE',
    'id' => 'BILL_BILLING_ACCOUNTSACCOUNTS_IDA',
    'width' => '10%',
    'default' => true,
    'name' => 'bill_billing_accounts_name',
  ),
  'type' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_TYPE',
    'width' => '10%',
    'name' => 'type',
  ),
  'billing_status' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_BILLING_STATUS',
    'width' => '10%',
    'default' => true,
    'name' => 'billing_status',
  ),
  'billingdate' => 
  array (
    'type' => 'date',
    'label' => 'LBL_BILLINGDATE',
    'width' => '10%',
    'default' => true,
    'name' => 'billingdate',
  ),
  'billingamount' => 
  array (
    'type' => 'currency',
    'default' => true,
    'label' => 'LBL_BILLINGAMOUNT',
    'currency_format' => true,
    'width' => '10%',
  ),
  'name' => 
  array (
    'width' => '40%',
    'label' => 'LBL_LIST_NAME',
    'link' => true,
    'default' => false,
    'name' => 'name',
  ),
  'date_modified' => 
  array (
    'width' => '15%',
    'label' => 'LBL_DATE_MODIFIED',
    'name' => 'date_modified',
    'default' => false,
  ),
  'bill_billing_aos_invoices_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_BILL_BILLING_AOS_INVOICES_FROM_AOS_INVOICES_TITLE',
    'id' => 'BILL_BILLING_AOS_INVOICESAOS_INVOICES_IDA',
    'width' => '10%',
    'default' => false,
    'name' => 'bill_billing_aos_invoices_name',
  ),
  'modified_by_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_MODIFIED_NAME',
    'id' => 'MODIFIED_USER_ID',
    'width' => '10%',
    'default' => false,
  ),
  'created_by_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'default' => false,
  ),
  'created_by' => 
  array (
    'width' => '8%',
    'label' => 'LBL_CREATED',
    'name' => 'created_by',
    'default' => false,
  ),
  'date_entered' => 
  array (
    'width' => '15%',
    'label' => 'LBL_DATE_ENTERED',
    'default' => false,
    'name' => 'date_entered',
  ),
  'paymentmethod' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_PAYMENTMETHOD',
    'width' => '10%',
    'default' => false,
    'name' => 'paymentmethod',
  ),
  'invoiceno' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_INVOICENO',
    'width' => '10%',
    'default' => false,
    'name' => 'invoiceno',
  ),
  'assigned_user_name' => 
  array (
    'width' => '8%',
    'label' => 'LBL_LIST_ASSIGNED_USER',
    'name' => 'assigned_user_name',
    'default' => false,
  ),
  'description' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
    'name' => 'description',
  ),
);
