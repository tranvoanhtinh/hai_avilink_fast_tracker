<?php
$module_name = 'bill_Billing';
$viewdefs [$module_name] = 
array (
  'QuickCreate' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'aos_bookings_bill_billing_1_name',
            'label' => 'LBL_AOS_BOOKINGS_BILL_BILLING_1_FROM_AOS_BOOKINGS_TITLE',
          ),
          1 => '',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'type',
            'studio' => 'visible',
            'label' => 'LBL_TYPE',
          ),
          1 => 
          array (
            'name' => 'billing_status',
            'studio' => 'visible',
            'label' => 'LBL_BILLING_STATUS',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'type_currency',
            'studio' => true,
            'label' => 'LBL_TYPE_CURRENCY',
          ),
          1 => 
          array (
            'name' => 'billingamount',
            'label' => 'LBL_BILLINGAMOUNT',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'billingdate',
            'label' => 'LBL_BILLINGDATE',
          ),
          1 => 
          array (
            'name' => 'paymentmethod',
            'studio' => 'visible',
            'label' => 'LBL_PAYMENTMETHOD',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'description',
            'comment' => 'Full text of the note',
            'label' => 'LBL_DESCRIPTION',
          ),
          1 => 'assigned_user_name',
        ),
      ),
    ),
  ),
);
;
?>
