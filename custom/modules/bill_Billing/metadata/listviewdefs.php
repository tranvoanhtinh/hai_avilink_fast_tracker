<?php
$module_name = 'bill_Billing';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'TYPE' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_TYPE',
    'width' => '10%',
  ),
  'BILL_BILLING_ACCOUNTS_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_BILL_BILLING_ACCOUNTS_FROM_ACCOUNTS_TITLE',
    'id' => 'BILL_BILLING_ACCOUNTSACCOUNTS_IDA',
    'width' => '10%',
    'default' => true,
  ),
  'BILLINGDATE' => 
  array (
    'type' => 'date',
    'label' => 'LBL_BILLINGDATE',
    'width' => '10%',
    'default' => true,
  ),
  'BILLING_STATUS' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_BILLING_STATUS',
    'width' => '10%',
    'default' => true,
  ),
  'BILLINGAMOUNT' => 
  array (
    'type' => 'currency',
    'default' => true,
    'label' => 'LBL_BILLINGAMOUNT',
    'currency_format' => true,
    'width' => '10%',
  ),
  'CS_CUSUSER_BILL_BILLING_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CS_CUSUSER_BILL_BILLING_1_FROM_CS_CUSUSER_TITLE',
    'id' => 'CS_CUSUSER_BILL_BILLING_1CS_CUSUSER_IDA',
    'width' => '10%',
    'default' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
  'CURRENCY_ID' => 
  array (
    'type' => 'currency_id',
    'studio' => 'visible',
    'label' => 'LBL_CURRENCY',
    'width' => '10%',
    'default' => false,
  ),
  'MODIFIED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_MODIFIED_NAME',
    'id' => 'MODIFIED_USER_ID',
    'width' => '10%',
    'default' => false,
  ),
  'CREATED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'default' => false,
  ),
  'BILL_BILLING_AOS_INVOICES_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_BILL_BILLING_AOS_INVOICES_FROM_AOS_INVOICES_TITLE',
    'id' => 'BILL_BILLING_AOS_INVOICESAOS_INVOICES_IDA',
    'width' => '10%',
    'default' => false,
  ),
  'PAYMENTMETHOD' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_PAYMENTMETHOD',
    'width' => '10%',
    'default' => false,
  ),
  'DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'BILL_BILLING_CS_CUSUSER_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_BILL_BILLING_CS_CUSUSER_1_FROM_CS_CUSUSER_TITLE',
    'id' => 'BILL_BILLING_CS_CUSUSER_1CS_CUSUSER_IDB',
    'width' => '10%',
    'default' => false,
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => false,
  ),
  'INVOICENO' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_INVOICENO',
    'width' => '10%',
    'default' => false,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => false,
  ),
);
;
?>
