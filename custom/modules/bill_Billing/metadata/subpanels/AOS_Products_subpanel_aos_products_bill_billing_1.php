<?php
// created: 2024-01-09 11:39:42
$subpanel_layout['list_fields'] = array (
  'type' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_TYPE',
    'width' => '10%',
  ),
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '45%',
    'default' => true,
  ),
  'billing_status' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'vname' => 'LBL_BILLING_STATUS',
    'width' => '10%',
    'default' => true,
  ),
  'billingamount' => 
  array (
    'type' => 'currency',
    'default' => true,
    'vname' => 'LBL_BILLINGAMOUNT',
    'currency_format' => true,
    'width' => '10%',
  ),
  'billingdate' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_BILLINGDATE',
    'width' => '10%',
    'default' => true,
  ),
  'assigned_user_name' => 
  array (
    'link' => true,
    'type' => 'relate',
    'vname' => 'LBL_ASSIGNED_TO_NAME',
    'id' => 'ASSIGNED_USER_ID',
    'width' => '10%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Users',
    'target_record_key' => 'assigned_user_id',
  ),
  'edit_button' => 
  array (
    'vname' => 'LBL_EDIT_BUTTON',
    'widget_class' => 'SubPanelEditButton',
    'module' => 'bill_Billing',
    'width' => '4%',
    'default' => true,
  ),
  'remove_button' => 
  array (
    'vname' => 'LBL_REMOVE',
    'widget_class' => 'SubPanelRemoveButton',
    'module' => 'bill_Billing',
    'width' => '5%',
    'default' => true,
  ),
);