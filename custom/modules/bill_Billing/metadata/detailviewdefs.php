<?php
$module_name = 'bill_Billing';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
          4 => 
          array (
            'customCode' => '<input type="button" class="button" onClick="showPopup(\'pdf\');" value="{$MOD.LBL_PRINT_AS_PDF}">',
          ),
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 
          array (
            'name' => 'aos_bookings_bill_billing_1_name',
            'label' => 'LBL_AOS_BOOKINGS_BILL_BILLING_1_FROM_AOS_BOOKINGS_TITLE',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'type',
            'studio' => 'visible',
            'label' => 'LBL_TYPE',
          ),
          1 => 
          array (
            'name' => 'billing_status',
            'studio' => 'visible',
            'label' => 'LBL_BILLING_STATUS',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'type_currency',
            'studio' => true,
            'label' => 'LBL_TYPE_CURRENCY',
          ),
          1 => 
          array (
            'name' => 'billingamount',
            'label' => 'LBL_BILLINGAMOUNT',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'billingdate',
            'label' => 'LBL_BILLINGDATE',
          ),
          1 => 
          array (
            'name' => 'paymentmethod',
            'studio' => 'visible',
            'label' => 'LBL_PAYMENTMETHOD',
          ),
        ),
        4 => 
        array (
          0 => 'description',
          1 => 'assigned_user_name',
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'created_by_name',
            'label' => 'LBL_CREATED',
          ),
          1 => 'date_entered',
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'modified_by_name',
            'label' => 'LBL_MODIFIED_NAME',
          ),
          1 => 'date_modified',
        ),
      ),
    ),
  ),
);
;
?>
