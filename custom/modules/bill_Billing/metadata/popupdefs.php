<?php
$popupMeta = array (
    'moduleMain' => 'bill_Billing',
    'varName' => 'bill_Billing',
    'orderBy' => 'bill_billing.name',
    'whereClauses' => array (
  'name' => 'bill_billing.name',
  'paymentmethod' => 'bill_billing.paymentmethod',
  'type' => 'bill_billing.type',
  'assigned_user_id' => 'bill_billing.assigned_user_id',
  'bill_billing_accounts_name' => 'bill_billing.bill_billing_accounts_name',
  'billing_status' => 'bill_billing.billing_status',
  'billingdate' => 'bill_billing.billingdate',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'paymentmethod',
  5 => 'type',
  9 => 'assigned_user_id',
  10 => 'bill_billing_accounts_name',
  11 => 'billing_status',
  12 => 'billingdate',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => '10%',
  ),
  'type' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_TYPE',
    'width' => '10%',
    'name' => 'type',
  ),
  'paymentmethod' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_PAYMENTMETHOD',
    'width' => '10%',
    'name' => 'paymentmethod',
  ),
  'billing_status' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_BILLING_STATUS',
    'width' => '10%',
    'name' => 'billing_status',
  ),
  'bill_billing_accounts_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_BILL_BILLING_ACCOUNTS_FROM_ACCOUNTS_TITLE',
    'id' => 'BILL_BILLING_ACCOUNTSACCOUNTS_IDA',
    'width' => '10%',
    'name' => 'bill_billing_accounts_name',
  ),
  'billingdate' => 
  array (
    'type' => 'date',
    'label' => 'LBL_BILLINGDATE',
    'width' => '10%',
    'name' => 'billingdate',
  ),
  'assigned_user_id' => 
  array (
    'name' => 'assigned_user_id',
    'label' => 'LBL_ASSIGNED_TO',
    'type' => 'enum',
    'function' => 
    array (
      'name' => 'get_user_array',
      'params' => 
      array (
        0 => false,
      ),
    ),
    'width' => '10%',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'TYPE' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_TYPE',
    'width' => '10%',
    'name' => 'type',
  ),
  'BILL_BILLING_ACCOUNTS_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_BILL_BILLING_ACCOUNTS_FROM_ACCOUNTS_TITLE',
    'id' => 'BILL_BILLING_ACCOUNTSACCOUNTS_IDA',
    'width' => '10%',
    'default' => true,
    'name' => 'bill_billing_accounts_name',
  ),
  'BILLINGDATE' => 
  array (
    'type' => 'date',
    'label' => 'LBL_BILLINGDATE',
    'width' => '10%',
    'default' => true,
    'name' => 'billingdate',
  ),
  'BILLING_STATUS' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_BILLING_STATUS',
    'width' => '10%',
    'default' => true,
    'name' => 'billing_status',
  ),
  'BILLINGAMOUNT' => 
  array (
    'type' => 'currency',
    'default' => true,
    'label' => 'LBL_BILLINGAMOUNT',
    'currency_format' => true,
    'width' => '10%',
    'name' => 'billingamount',
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
    'name' => 'assigned_user_name',
  ),
),
);
