<?php
// created: 2024-05-22 03:41:57
$mod_strings = array (
  'LBL_BILLINGAMOUNT' => 'BillingAmount',
  'LBL_BILLINGDATE' => 'BillingDate',
  'LBL_BILLING_STATUS' => 'Billing Status',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_AOS_PRODUCTS_BILL_BILLING_1_FROM_AOS_PRODUCTS_TITLE' => 'Products',
  'LBL_BILL_BILLING_CALLS_FROM_CALLS_TITLE' => 'Calls',
  'LBL_BILL_BILLING_MEETINGS_FROM_MEETINGS_TITLE' => 'Meetings',
  'LBL_BILL_BILLING_NOTES_FROM_NOTES_TITLE' => 'Notes',
  'LBL_BILL_BILLING_TASKS_FROM_TASKS_TITLE' => 'Tasks',
  'LBL_BILL_BILLING_ACCOUNTS_FROM_ACCOUNTS_TITLE' => 'Đại lý',
  'LBL_BILL_BILLING_AOS_INVOICES_FROM_AOS_INVOICES_TITLE' => 'Invoices',
  'LBL_TYPE' => 'Type',
  'LBL_CS_CUSUSER_BILL_BILLING_1_FROM_CS_CUSUSER_TITLE' => 'Employee',
  'LBL_PRINT_AS_PDF' => 'Print Receipt / Payment',
  'LBL_NAME' => 'Voucher No',
  'LBL_CHARACTER_ERROR' => 'Special character error',
  'LBL_ALREADY-EXISTS' => 'Code already exists',
  'LBL_SAVE' => 'Save',
  'LBL_TYPE_CURRENCY' => 'Currency type',
  'LBL_USD' => 'USD',
  'LBL_VND' => 'VND',
  'LBL_EXCHANGE_RATE' => 'Exchange rate',
);