<?php
// created: 2024-05-30 04:40:48
$mod_strings = array (
  'LBL_BILLINGAMOUNT' => 'Số tiền',
  'LBL_BILLINGDATE' => 'Ngày',
  'LBL_BILLING_STATUS' => 'Tình trạng',
  'LBL_BILL_BILLING_ACCOUNTS_FROM_ACCOUNTS_TITLE' => 'Đại lý',
  'LBL_BILL_BILLING_AOS_INVOICES_FROM_AOS_INVOICES_TITLE' => 'Đơn hàng',
  'LBL_BILL_BILLING_NOTES_FROM_NOTES_TITLE' => 'Ghi chú',
  'LBL_BILL_BILLING_TASKS_FROM_TASKS_TITLE' => 'Quản lý công việc',
  'LBL_INVOICENO' => 'Số hóa đơn',
  'LBL_PAYMENTMETHOD' => 'Phương thức thanh toán',
  'LBL_TYPE' => 'Loại',
  'LNK_NEW_RECORD' => 'Tạo Thu / Chi',
  'LNK_LIST' => 'Xem Thu / Chi',
  'LNK_IMPORT_BILL_BILLING' => 'Nhập Thu Chi',
  'LBL_LIST_FORM_TITLE' => 'Danh sách thu chi',
  'LBL_SEARCH_FORM_TITLE' => 'Tìm kiếm ',
  'LBL_HOMEPAGE_TITLE' => 'Thu / Chi của tôi',
  'LBL_AOS_PRODUCTS_BILL_BILLING_1_FROM_AOS_PRODUCTS_TITLE' => 'Dịch vụ',
  'LBL_BILL_BILLING_CALLS_FROM_CALLS_TITLE' => 'Cuộc gọi',
  'LBL_BILL_BILLING_MEETINGS_FROM_MEETINGS_TITLE' => 'Cuộc gặp',
  'LBL_CS_CUSUSER_BILL_BILLING_1_FROM_CS_CUSUSER_TITLE' => 'Nhân viên',
  'LBL_PRINT_AS_PDF' => 'In phiếu Thu / Chi',
  'LBL_NAME' => 'Số chứng từ',
  'LBL_CHARACTER_ERROR' => 'Ký tự không hợp lệ, chỉ cho phép sử dụng "- . _ khoảng trắng"',
  'LBL_ALREADY-EXISTS' => 'Mã đã tồn tại',
  'LBL_SAVE' => 'Lưu',
  'LBL_VND' => 'VND',
  'LBL_TYPE_CURRENCY' => 'Loại Tiền',
  'LBL_TYPE_SERVICE' => 'Loại dịch vụ',
  'LBL_USD' => 'USD',
  'LBL_EXCHANGE_RATE' => 'Tỷ giá',

);