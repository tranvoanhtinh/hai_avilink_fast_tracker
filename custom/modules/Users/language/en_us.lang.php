<?php
// created: 2024-06-05 06:17:13
$mod_strings = array (
  'LBL_CONTACTS_SYNC' => 'Contacts Sync',
  'LBL_CALLS' => 'Calls',
  'LBL_MEETINGS' => 'Meetings',
  'LBL_PROSPECT_LIST' => 'Prospect List',
  'LBL_PROJECT_USERS_1_FROM_PROJECT_TITLE' => 'Project Users from Project Title',
  'DEPARTMENT' => 'GroupName',
  'LBL_DEPARTMENT' => 'GroupName',
  'LBL_PROVINCECODE' => 'ProvinceCode',
  'LBL_DISTRICTCODE' => 'DistrictCode',
);