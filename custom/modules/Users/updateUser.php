<?php
class Users_UpdateUser
{
	
	function ensure_length(&$string, $length)
	{
		$strlen = strlen($string);
		if ($strlen < $length) {
			$string = str_pad($string, $length, '0');
		} elseif ($strlen > $length) {
			$string = substr($string, 0, $length);
		}
	}
	function create_guid_section($characters)
	{
		$return = '';
		for ($i = 0; $i < $characters; ++$i) {
			$return .= dechex(mt_rand(0, 15));
		}

		return $return;
	}
	function create_guid()
	{
		$microTime = microtime();
		list($a_dec, $a_sec) = explode(' ', $microTime);

		$dec_hex = dechex($a_dec * 1000000);
		$sec_hex = dechex($a_sec);

		$this->ensure_length($dec_hex, 5);
		$this->ensure_length($sec_hex, 6);

		$guid = '';
		$guid .= $dec_hex;
		$guid .= $this->create_guid_section(3);
		$guid .= '-';
		$guid .= $this->create_guid_section(4);
		$guid .= '-';
		$guid .= $this->create_guid_section(4);
		$guid .= '-';
		$guid .= $this->create_guid_section(4);
		$guid .= '-';
		$guid .= $sec_hex;
		$guid .= $this->create_guid_section(6);

		return $guid;
	}
    function updateUser($bean, $event, $arguments)
    {
		global $db;
		global $sugar_config;
		if(!empty($bean->user_name)){
			$query = "update cs_cususer set username = '".$bean->user_name."' where refuser = '".$bean->id."' and deleted = '0'";
			$db->query($query);
		}
	}
}
