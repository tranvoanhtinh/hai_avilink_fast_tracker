<?php
// created: 2023-02-08 04:35:25
$mod_strings = array (
  'LBL_DISSATISFIED_TEXT' => 'Bất mãn',
  'LBL_NEITHER_TEXT' => 'Không hài lòng',
  'LBL_SATISFIED_TEXT' => 'Hài lòng',
  'LBL_SUBMIT_TEXT' => 'Gửi văn bản',
  'LBL_NAME' => 'Tên',
);