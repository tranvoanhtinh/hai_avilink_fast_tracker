<?php
// created: 2024-06-05 06:17:26
$mod_strings = array (
  'LBL_ACCOUNTS' => 'Agent',
  'LBL_ACCOUNTS_SUBPANEL_TITLE' => 'Agent',
  'LBL_CONTACTS' => 'Contacts',
  'LBL_CONTACTS_SUBPANEL_TITLE' => 'Contactss',
  'LBL_TASKS' => 'Tasks',
  'LBL_NOTES' => 'Notess',
  'LBL_MEETINGS' => 'Meetings',
  'LBL_CALLS' => 'Calls',
  'LBL_DOCUMENTS_SUBPANEL_TITLE' => 'Documents',
  'LBL_CASES' => 'Ticketss',
  'LBL_CASES_SUBPANEL_TITLE' => 'Ticketss',
  'LBL_PROJECTS' => 'Projects',
  'LBL_PROJECTS_SUBPANEL_TITLE' => 'Projects',
);