<?php
// created: 2024-06-14 03:32:18
$mod_strings = array (
  'LBL_ACCOUNTS' => 'Accounts',
  'LBL_ACCOUNTS_SUBPANEL_TITLE' => 'Đại lý',
  'LBL_CONTACTS' => 'Contacts',
  'LBL_CONTACTS_SUBPANEL_TITLE' => 'Người liên hệ đại lý',
  'LBL_TASKS' => 'Tasks',
  'LBL_NOTES' => 'Notes',
  'LBL_MEETINGS' => 'Meetings',
  'LBL_CALLS' => 'Calls',
  'LBL_DOCUMENTS_SUBPANEL_TITLE' => 'Tài liệus',
  'LBL_CASES' => 'Cases',
  'LBL_PROJECTS' => 'Projects',
  'LBL_CASES_SUBPANEL_TITLE' => 'Tickets',
  'LBL_PROJECTS_SUBPANEL_TITLE' => 'Dự án',
);