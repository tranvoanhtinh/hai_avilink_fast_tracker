<?php
// created: 2024-06-14 03:32:11
$mod_strings = array (
  'LBL_ACCOUNTS_SUBPANEL_TITLE' => 'Accounts',
  'LBL_LEADS' => 'Leads',
  'LBL_OPPORTUNITIES_SUBPANEL_TITLE' => 'Opportunities',
  'LBL_CONTACTS_SUBPANEL_TITLE' => 'Người liên hệ đại lý',
  'LNK_NEW_DOCUMENT' => 'Tạo Tài liệu',
  'LNK_DOCUMENT_LIST' => 'Xem Tài liệu',
  'LBL_LIST_FORM_TITLE' => 'Danh sách Tài liệu',
  'LBL_SEARCH_FORM_TITLE' => 'Tìm kiếm Tài liệu',
  'LBL_DET_RELATED_DOCUMENT' => 'Tài liệu liên quan',
  'LBL_CASES_SUBPANEL_TITLE' => 'Ticketss',
  'LBL_DOCUMENTTYPE' => 'Loại tài liệu',
  'LBL_SUBTYPEDOCUMENT' => 'Tài liệu con',
  'LBL_DESCRIPTION' => 'Mô tả',
  'LBL_AOS_PRODUCTS_DOCUMENTS_1_FROM_AOS_PRODUCTS_TITLE' => 'Dịch vụ',
);