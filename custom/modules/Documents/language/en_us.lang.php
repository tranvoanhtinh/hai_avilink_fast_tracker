<?php
// created: 2024-06-05 06:17:14
$mod_strings = array (
  'LBL_LEADS' => 'Leads',
  'LBL_ACCOUNTS_SUBPANEL_TITLE' => 'Agent',
  'LBL_CONTACTS_SUBPANEL_TITLE' => 'Liên hệ',
  'LBL_OPPORTUNITIES_SUBPANEL_TITLE' => 'Opportunities',
  'LNK_NEW_DOCUMENT' => 'Create Document',
  'LNK_DOCUMENT_LIST' => 'View Document',
  'LBL_LIST_FORM_TITLE' => 'Document List',
  'LBL_SEARCH_FORM_TITLE' => 'Document Search',
  'LBL_DET_RELATED_DOCUMENT' => 'Related Document:',
  'LBL_CASES_SUBPANEL_TITLE' => 'Ticketss',
  'LBL_HOMEPAGE_TITLE' => 'My Documents',
  'LBL_MODULE_NAME' => 'Document',
  'LBL_QUICKCREATE_PANEL1' => 'More',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_DOCUMENTTYPE' => 'Document Type',
  'LBL_SUBTYPEDOCUMENT' => 'SubtypeDocument',
  'LBL_TEMPLATE_TYPE' => 'Document Template',
);