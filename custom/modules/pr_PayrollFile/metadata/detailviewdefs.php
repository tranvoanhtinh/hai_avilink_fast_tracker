<?php
$module_name = 'pr_PayrollFile';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_DETAILVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'allowance1',
            'label' => 'LBL_ALLOWANCE1',
          ),
          1 => 
          array (
            'name' => 'allowance2',
            'label' => 'LBL_ALLOWANCE2',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'allowance3',
            'label' => 'LBL_ALLOWANCE3',
          ),
          1 => 
          array (
            'name' => 'allowance4',
            'label' => 'LBL_ALLOWANCE4',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'pr_payrollfile_cs_cususer_name',
            'label' => 'LBL_PR_PAYROLLFILE_CS_CUSUSER_FROM_CS_CUSUSER_TITLE',
          ),
          1 => 
          array (
            'name' => 'salary_amount_basic',
            'label' => 'LBL_SALARY_AMOUNT_BASIC',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'workday',
            'label' => 'LBL_WORKDAY',
          ),
          1 => 
          array (
            'name' => 'discountpercent',
            'label' => 'LBL_DISCOUNT_PERCENT',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'workinghours',
            'label' => 'LBL_WORKING_HOURS',
          ),
          1 => 'assigned_user_name',
        ),
        6 => 
        array (
          0 => 'description',
        ),
      ),
      'lbl_detailview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'created_by_name',
            'label' => 'LBL_CREATED',
          ),
          1 => 'date_entered',
        ),
        1 => 
        array (
          0 => 'date_modified',
          1 => 
          array (
            'name' => 'modified_by_name',
            'label' => 'LBL_MODIFIED_NAME',
          ),
        ),
      ),
    ),
  ),
);
;
?>
