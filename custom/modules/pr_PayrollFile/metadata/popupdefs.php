<?php
$popupMeta = array (
    'moduleMain' => 'pr_PayrollFile',
    'varName' => 'pr_PayrollFile',
    'orderBy' => 'pr_payrollfile.name',
    'whereClauses' => array (
  'name' => 'pr_payrollfile.name',
  'assigned_user_id' => 'pr_payrollfile.assigned_user_id',
  'pr_payrollfile_cs_cususer_name' => 'pr_payrollfile.pr_payrollfile_cs_cususer_name',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'assigned_user_id',
  5 => 'pr_payrollfile_cs_cususer_name',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => '10%',
  ),
  'assigned_user_id' => 
  array (
    'name' => 'assigned_user_id',
    'label' => 'LBL_ASSIGNED_TO',
    'type' => 'enum',
    'function' => 
    array (
      'name' => 'get_user_array',
      'params' => 
      array (
        0 => false,
      ),
    ),
    'width' => '10%',
  ),
  'pr_payrollfile_cs_cususer_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_PR_PAYROLLFILE_CS_CUSUSER_FROM_CS_CUSUSER_TITLE',
    'id' => 'PR_PAYROLLFILE_CS_CUSUSERCS_CUSUSER_IDB',
    'width' => '10%',
    'name' => 'pr_payrollfile_cs_cususer_name',
  ),
),
);
