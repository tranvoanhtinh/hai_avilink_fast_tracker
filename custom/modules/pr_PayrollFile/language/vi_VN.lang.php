<?php
// created: 2023-05-05 09:53:30
$mod_strings = array (
  'LNK_NEW_RECORD' => 'Tạo Hồ sơ lương',
  'LNK_LIST' => 'Xem Hồ sơ lương',
  'LNK_IMPORT_PR_PAYROLLFILE' => 'Import Hồ sơ lương',
  'LBL_LIST_FORM_TITLE' => 'Hồ sơ lương Danh sách',
  'LBL_SEARCH_FORM_TITLE' => 'Tìm kiếm Hồ sơ lương',
  'LBL_ALLOWANCE1' => 'Phụ cấp 1',
  'LBL_ALLOWANCE2' => 'Phụ cấp 2',
  'LBL_ALLOWANCE3' => 'Phụ cấp 3',
  'LBL_ALLOWANCE4' => 'Phụ cấp 4',
  'LBL_PR_PAYROLLFILE_CS_CUSUSER_FROM_CS_CUSUSER_TITLE' => 'Nhân viên',
  'LBL_SALARY_AMOUNT_BASIC' => 'Lương cơ bản',
  'LBL_WORKDAY' => 'Số ngày làm việc',
'LBL_DISCOUNT_PERCENT' => '% Hoa hồng',
'LBL_WORKING_HOURS' => 'Công ngày / giờ',
);