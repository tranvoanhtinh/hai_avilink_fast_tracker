<?php
// created: 2024-06-11 03:23:22
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '45%',
    'default' => true,
  ),
  'status' => 
  array (
    'type' => 'enum',
    'vname' => 'LBL_STATUS',
    'width' => '10%',
    'default' => true,
  ),
  'date_modified' => 
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => '45%',
    'default' => true,
  ),
  'type_currency' => 
  array (
    'type' => 'enum',
    'studio' => true,
    'vname' => 'LBL_TYPE_CURRENCY',
    'width' => '10%',
    'default' => true,
  ),
  'exchange_rate' => 
  array (
    'type' => 'currency',
    'vname' => 'LBL_EXCHANGE_RATE',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
  ),
  'price' => 
  array (
    'type' => 'currency',
    'vname' => 'LBL_PRICE',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
  ),
  'edit_button' => 
  array (
    'vname' => 'LBL_EDIT_BUTTON',
    'widget_class' => 'SubPanelEditButton',
    'module' => 'AOS_Data_unitprice',
    'width' => '4%',
    'default' => true,
  ),
  'remove_button' => 
  array (
    'vname' => 'LBL_REMOVE',
    'widget_class' => 'SubPanelRemoveButton',
    'module' => 'AOS_Data_unitprice',
    'width' => '5%',
    'default' => true,
  ),
);