function calculateSalary() {
    SUGAR.ajaxUI.showLoadingPanel();
    $.ajax({
      url: "index.php?entryPoint=calculateSalary",
      type: "POST",
      dataType: "json", 
      data:{
        month: $("#monthPayroll").val(),
        year: $("#yearPayroll").val(),
      },
      
      success: function (body) {
        console.log('hello'+body);
        dataUser = body;
        var table = $("#payrollView").DataTable();
        table.clear().draw();
        if (body != null && body != "") {
          if (body.length > 0) {
            for (let i = 0; i < body.length; i++) {
              table.row
                .add([
                  body[i].user,
                  parseFloat(body[i].basic_salary)
                    ? changeCurrencyFloat(
                        format2Number(body[i].basic_salary, sig_digits)
                      )
                    : "",
                  parseFloat(body[i].workday)
                    ? changeCurrencyFloat(
                        format2Number(body[i].workday, sig_digits)
                      )
                    : "",
                  parseFloat(body[i].dayleave)
                    ? changeCurrencyFloat(
                        format2Number(body[i].dayleave, sig_digits)
                      )
                    : "",
                  parseFloat(body[i].real_workingday)
                    ? changeCurrencyFloat(
                        format2Number(body[i].real_workingday, sig_digits)
                      )
                    : "",
                  parseFloat(body[i].work_salary)
                    ? changeCurrencyFloat(
                        format2Number(body[i].work_salary, sig_digits)
                      )
                    : "",
                  parseFloat(body[i].overtime)
                    ? changeCurrencyFloat(
                        format2Number(body[i].overtime, sig_digits)
                      )
                    : "",
                  parseFloat(body[i].overtime_salary)
                    ? changeCurrencyFloat(
                        format2Number(body[i].overtime_salary, sig_digits)
                      )
                    : "",
                  parseFloat(body[i].allowance)
                    ? changeCurrencyFloat(
                        format2Number(body[i].allowance, sig_digits)
                      )
                    : "",
                  parseFloat(body[i].bonus)
                    ? changeCurrencyFloat(
                        format2Number(body[i].bonus, sig_digits)
                      )
                    : "",
                  parseFloat(body[i].amercement)
                    ? changeCurrencyFloat(
                        format2Number(body[i].amercement, sig_digits)
                      )
                    : "",
                  parseFloat(body[i].advance_salary)
                    ? changeCurrencyFloat(
                        format2Number(body[i].advance_salary, sig_digits)
                      )
                    : "",
                  parseFloat(body[i].commision_service)
                    ? changeCurrencyFloat(
                        format2Number(body[i].commision_service, sig_digits)
                      )
                    : "",
                  parseFloat(body[i].commision_action)
                    ? changeCurrencyFloat(
                        format2Number(body[i].commision_action, sig_digits)
                      )
                    : "",
                  parseFloat(body[i].net_salary)
                    ? changeCurrencyFloat(
                        format2Number(body[i].net_salary, sig_digits)
                      )
                    : "",
                  body[i].note,
                ])
                .draw(false);
            }
          }
        }
        var i = 0;
  
        $("#payrollView tbody tr").find("td:eq(2)").css("fontSize", "16px");
        $("#payrollView tbody tr").find("td:eq(2)").css("fontWeight", "bold");
        $("#payrollView tbody tr").find("td:eq(4)").css("fontSize", "16px");
        $("#payrollView tbody tr").find("td:eq(4)").css("fontWeight", "bold");
        $("#payrollView tbody tr").find("td:eq(11)").css("color", "red");
        $("#payrollView tbody tr").find("td:eq(11)").css("fontSize", "16px");
        $("#payrollView tbody tr").find("td:eq(11)").css("fontWeight", "bold");
        $("#payrollView tbody tr").find("td:eq(1)").css("color", "red");
        $("#payrollView tbody tr").find("td:eq(1)").css("fontSize", "16px");
        $("#payrollView tbody tr").find("td:eq(1)").css("fontWeight", "bold");
        $("#payrollView tbody tr").find("td:eq(3)").css("color", "red");
        $("#payrollView tbody tr").find("td:eq(3)").css("fontSize", "16px");
        $("#payrollView tbody tr").find("td:eq(3)").css("fontWeight", "bold");
        $("#payrollView tbody tr").find("td:eq(5)").css("color", "red");
        $("#payrollView tbody tr").find("td:eq(5)").css("fontSize", "16px");
        $("#payrollView tbody tr").find("td:eq(5)").css("fontWeight", "bold");
        $("#payrollView tbody tr").find("td:eq(6)").css("color", "red");
        $("#payrollView tbody tr").find("td:eq(6)").css("fontSize", "16px");
        $("#payrollView tbody tr").find("td:eq(6)").css("fontWeight", "bold");
        $("#payrollView tbody tr").find("td:eq(7)").css("color", "red");
        $("#payrollView tbody tr").find("td:eq(7)").css("fontSize", "16px");
        $("#payrollView tbody tr").find("td:eq(7)").css("fontWeight", "bold");
        $("#payrollView tbody tr").find("td:eq(8)").css("color", "blue");
        $("#payrollView tbody tr").find("td:eq(8)").css("fontSize", "16px");
        $("#payrollView tbody tr").find("td:eq(8)").css("fontWeight", "bold");
        $("#payrollView tbody tr").find("td:eq(9)").css("color", "blue");
        $("#payrollView tbody tr").find("td:eq(9)").css("fontSize", "16px");
        $("#payrollView tbody tr").find("td:eq(9)").css("fontWeight", "bold");
        $("#payrollView tbody tr").find("td:eq(14)").css("color", "red");
        $("#payrollView tbody tr")
          .find("td:eq(14)")
          .css("backgroundColor", "#FFD800");
        $("#payrollView tbody tr").find("td:eq(14)").css("fontSize", "16px");
        $("#payrollView tbody tr").find("td:eq(14)").css("fontWeight", "bold");
        $("#payrollView tbody tr").find("td:eq(10)").css("color", "red");
        $("#payrollView tbody tr").find("td:eq(10)").css("fontSize", "16px");
        $("#payrollView tbody tr").find("td:eq(10)").css("fontWeight", "bold");
        $("#payrollView tbody tr").find("td:eq(0)").css("fontWeight", "bold");
  
        SUGAR.ajaxUI.hideLoadingPanel();
      },
      error: function (error) {
        alert("không thể xóa sản phẩm");
      },
    });
  }
  