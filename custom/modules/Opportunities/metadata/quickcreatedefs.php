<?php
$viewdefs ['Opportunities'] = 
array (
  'QuickCreate' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'javascript' => '{$PROBABILITY_SCRIPT}',
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_QUICKCREATE_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'collapsed',
        ),
      ),
    ),
    'panels' => 
    array (
      'DEFAULT' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'aos_products_opportunities_1_name',
            'label' => 'LBL_AOS_PRODUCTS_OPPORTUNITIES_1_FROM_AOS_PRODUCTS_TITLE',
          ),
          1 => 
          array (
            'name' => 'name',
            'displayParams' => 
            array (
              'required' => true,
            ),
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'servicetype_c',
            'studio' => 'visible',
            'label' => 'LBL_SERVICETYPE',
          ),
          1 => 
          array (
            'name' => 'opportunity_type',
          ),
        ),
        2 => 
        array (
          0 => 'amount',
          1 => 
          array (
            'name' => 'currency_id',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'aos_product_categories_opportunities_1_name',
            'label' => 'LBL_AOS_PRODUCT_CATEGORIES_OPPORTUNITIES_1_FROM_AOS_PRODUCT_CATEGORIES_TITLE',
          ),
          1 => 'lead_source',
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'depositsamount',
            'label' => 'LBL_Deposits_Amount',
          ),
          1 => 
          array (
            'name' => 'paymentsamount',
            'label' => 'LBL_Payments_Amount',
          ),
        ),
        5 => 
        array (
          0 => 'date_closed',
          1 => 'sales_stage',
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'description',
            'comment' => 'Full text of the note',
            'label' => 'LBL_DESCRIPTION',
          ),
          1 => 
          array (
            'name' => 'campaign_name',
            'label' => 'LBL_CAMPAIGN',
          ),
        ),
      ),
      'lbl_quickcreate_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'assigned_user_name',
          ),
          1 => '',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'account_name',
          ),
          1 => 'next_step',
        ),
      ),
    ),
  ),
);
;
?>
