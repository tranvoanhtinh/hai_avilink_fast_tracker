<?php


class LeadCardOpportunityHook
{
    public function linkOpportunityToAccount(&$bean, $event, $args)
    {
        if(empty($bean->account_id)) {
            if(!empty($bean->customer_id)) {
                $bean->customer_id = trim($bean->customer_id);
                $acc = new Account();
                $acc->retrieve_by_string_fields(array('account_code' => $bean->customer_id));
                // If account does exist
                if(!empty($acc->id)) {
                    $acc->load_relationship('opportunities');
                    $acc->opportunities->add($bean->id);
                }
                $bean->assigned_user_id = $acc->assigned_user_id;
                $bean->processed = true;
                $bean->save();
            }
        }
    }
}