function updateSalesStage(textContent, backgroundColor) {
    var element = document.querySelector('.col-xs-12.col-sm-8.detail-view-field.inlineEdit[field="sales_stage"]');
    if (element) {
        element.textContent = '';
        var pElement = document.createElement("p");
        pElement.textContent = textContent;
        pElement.style.backgroundColor = backgroundColor;
        pElement.style.color = "white";
        pElement.style.padding = "2px";
        pElement.style.borderRadius = "6px";
        pElement.style.display = "inline-block";
        element.appendChild(pElement);
    }
}

var salesStages = {
    '2.Đề xuất báo giá': '#F7D358',
    '3.Thương lượng đàm phán': '#FFBF00',
    '4.Chốt Thắng': '#FF8000',
    '5.Chốt Thua': '#00BFFF'
};
var element = document.querySelector('.col-xs-12.col-sm-8.detail-view-field.inlineEdit[field="sales_stage"]');
var currentSalesStageText = element.textContent.trim();
for (var stage in salesStages) {
    if (currentSalesStageText === stage) {
        updateSalesStage(stage, salesStages[stage]);
        break;
    }
}
