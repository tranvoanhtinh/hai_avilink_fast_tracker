<?php

require_once('include/MVC/View/views/view.edit.php');
class CustomAOS_ContractsViewEdit extends ViewEdit {

    public function display()
    {
        parent::display();
        ?>
        <style type="text/css">
        .yui-ac-content {
            width: 250px !important;
        }

         </style>
         <script src="include/javascript/custom/view.edit.js"></script>
         <script src="custom/modules/AOS_Contracts/js/edit.js"></script>
        <?php
    }
	
	
}

