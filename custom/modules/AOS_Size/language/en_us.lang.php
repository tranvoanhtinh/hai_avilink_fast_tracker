<?php 

$mod_strings = array (
  'LBL_SIZE_L_CM' => 'L(cm)',
  'LBL_SIZE_W_CM' => 'W(cm)',
  'LBL_SIZE_H_CM' => 'H(cm)',
  'LBL_SIZE_L_INCH' => 'L(inch)',
  'LBL_SIZE_W_INCH' => 'W(inch)',
  'LBL_SIZE_H_INCH' => 'H(inch)',
  'LBL_SIZE_W_H_CM' => 'W/H (cm)',
  'LBL_SIZE_W_H_INCH' => 'W/H (inch)',
  'LNK_NEW_RECORD' => 'Create Size Products',
  'LNK_LIST' => 'View Size Products',
  'LNK_IMPORT_AOS_SIZE' => 'Import Size Products',
  'LBL_LIST_FORM_TITLE' => 'Size Products List',
  'LBL_SEARCH_FORM_TITLE' => 'Search Size Products',
  'LBL_HOMEPAGE_TITLE' => 'My Size Product',
  'LBL_EDITVIEW_PANEL2' => 'Panel',
  'LBL_DETAILVIEW_PANEL2' => 'Other',
  'LBL_EDITVIEW_PANEL1' => 'Overview',
  'LBL_HANDLE'=>'Handle',
  'LBL_BOTTOM_LENGTH'=> 'Bottom length',
  'LBL_BOTTOM_WIDTH'=>'Bottom width',
)
?>