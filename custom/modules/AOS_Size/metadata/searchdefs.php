<?php
$module_name = 'AOS_Size';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      0 => 'name',
      1 => 
      array (
        'name' => 'current_user_only',
        'label' => 'LBL_CURRENT_USER_FILTER',
        'type' => 'bool',
      ),
    ),
    'advanced_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'size_l_cm' => 
      array (
        'type' => 'decimal',
        'label' => 'LBL_SIZE_L_CM',
        'width' => '10%',
        'default' => true,
        'name' => 'size_l_cm',
      ),
      'size_w_cm' => 
      array (
        'type' => 'decimal',
        'label' => 'LBL_SIZE_W_CM',
        'width' => '10%',
        'default' => true,
        'name' => 'size_w_cm',
      ),
      'size_h_cm' => 
      array (
        'type' => 'decimal',
        'label' => 'LBL_SIZE_H_CM',
        'width' => '10%',
        'default' => true,
        'name' => 'size_h_cm',
      ),
      'size_w_h_cm' => 
      array (
        'type' => 'decimal',
        'label' => 'LBL_SIZE_W_H_CM',
        'width' => '10%',
        'default' => true,
        'name' => 'size_w_h_cm',
      ),
      'size_l_inch' => 
      array (
        'type' => 'decimal',
        'label' => 'LBL_SIZE_L_INCH',
        'width' => '10%',
        'default' => true,
        'name' => 'size_l_inch',
      ),
      'size_w_inch' => 
      array (
        'type' => 'decimal',
        'label' => 'LBL_SIZE_W_INCH',
        'width' => '10%',
        'default' => true,
        'name' => 'size_w_inch',
      ),
      'size_h_inch' => 
      array (
        'type' => 'decimal',
        'label' => 'LBL_SIZE_H_INCH',
        'width' => '10%',
        'default' => true,
        'name' => 'size_h_inch',
      ),
      'size_w_h_inch' => 
      array (
        'type' => 'decimal',
        'label' => 'LBL_SIZE_W_H_INCH',
        'width' => '10%',
        'default' => true,
        'name' => 'size_w_h_inch',
      ),
      'assigned_user_id' => 
      array (
        'name' => 'assigned_user_id',
        'label' => 'LBL_ASSIGNED_TO',
        'type' => 'enum',
        'function' => 
        array (
          'name' => 'get_user_array',
          'params' => 
          array (
            0 => false,
          ),
        ),
        'default' => true,
        'width' => '10%',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
;
?>
