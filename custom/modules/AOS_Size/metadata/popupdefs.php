<?php
$popupMeta = array (
    'moduleMain' => 'AOS_Size',
    'varName' => 'AOS_Size',
    'orderBy' => 'aos_size.name',
    'whereClauses' => array (
  'name' => 'aos_size.name',
  'size_l_cm' => 'aos_size.size_l_cm',
  'size_w_cm' => 'aos_size.size_w_cm',
  'size_h_cm' => 'aos_size.size_h_cm',
  'size_w_h_cm' => 'aos_size.size_w_h_cm',
  'assigned_user_id' => 'aos_size.assigned_user_id',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'size_l_cm',
  5 => 'size_w_cm',
  6 => 'size_h_cm',
  7 => 'size_w_h_cm',
  8 => 'assigned_user_id',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => '10%',
  ),
  'size_l_cm' => 
  array (
    'type' => 'decimal',
    'label' => 'LBL_SIZE_L_CM',
    'width' => '10%',
    'name' => 'size_l_cm',
  ),
  'size_w_cm' => 
  array (
    'type' => 'decimal',
    'label' => 'LBL_SIZE_W_CM',
    'width' => '10%',
    'name' => 'size_w_cm',
  ),
  'size_h_cm' => 
  array (
    'type' => 'decimal',
    'label' => 'LBL_SIZE_H_CM',
    'width' => '10%',
    'name' => 'size_h_cm',
  ),
  'size_w_h_cm' => 
  array (
    'type' => 'decimal',
    'label' => 'LBL_SIZE_W_H_CM',
    'width' => '10%',
    'name' => 'size_w_h_cm',
  ),
  'assigned_user_id' => 
  array (
    'name' => 'assigned_user_id',
    'label' => 'LBL_ASSIGNED_TO',
    'type' => 'enum',
    'function' => 
    array (
      'name' => 'get_user_array',
      'params' => 
      array (
        0 => false,
      ),
    ),
    'width' => '10%',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'SIZE_L_CM' => 
  array (
    'type' => 'decimal',
    'label' => 'LBL_SIZE_L_CM',
    'width' => '10%',
    'default' => true,
    'name' => 'size_l_cm',
  ),
  'SIZE_W_CM' => 
  array (
    'type' => 'decimal',
    'label' => 'LBL_SIZE_W_CM',
    'width' => '10%',
    'default' => true,
    'name' => 'size_w_cm',
  ),
  'SIZE_H_CM' => 
  array (
    'type' => 'decimal',
    'label' => 'LBL_SIZE_H_CM',
    'width' => '10%',
    'default' => true,
    'name' => 'size_h_cm',
  ),
  'SIZE_W_H_CM' => 
  array (
    'type' => 'decimal',
    'label' => 'LBL_SIZE_W_H_CM',
    'width' => '10%',
    'default' => true,
    'name' => 'size_w_h_cm',
  ),
  'DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => true,
    'name' => 'description',
  ),
),
);
