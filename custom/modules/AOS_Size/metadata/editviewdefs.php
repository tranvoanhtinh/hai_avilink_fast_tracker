<?php
$module_name = 'AOS_Size';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 'name',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'size_l_cm',
            'label' => 'LBL_SIZE_L_CM',
          ),
          1 => 
          array (
            'name' => 'size_l_inch',
            'label' => 'LBL_SIZE_L_INCH',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'size_w_cm',
            'label' => 'LBL_SIZE_W_CM',
          ),
          1 => 
          array (
            'name' => 'size_w_inch',
            'label' => 'LBL_SIZE_W_INCH',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'size_h_cm',
            'label' => 'LBL_SIZE_H_CM',
          ),
          1 => 
          array (
            'name' => 'size_h_inch',
            'label' => 'LBL_SIZE_H_INCH',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'size_w_h_cm',
            'label' => 'LBL_SIZE_W_H_CM',
          ),
          1 => 
          array (
            'name' => 'size_w_h_inch',
            'label' => 'LBL_SIZE_W_H_INCH',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'bottom_length',
            'label' => 'LBL_BOTTOM_LENGTH',
          ),
          1 => 
          array (
            'name' => 'bottom_width',
            'label' => 'LBL_BOTTOM_WIDTH',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'handle',
            'label' => 'LBL_HANDLE',
          ),
          1 => 'description',
        ),
      ),
    ),
  ),
);
;
?>
