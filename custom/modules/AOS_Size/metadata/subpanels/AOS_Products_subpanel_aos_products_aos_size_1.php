<?php
// created: 2023-12-27 08:17:23
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '45%',
    'default' => true,
  ),
  'size_l_cm' => 
  array (
    'type' => 'decimal',
    'vname' => 'LBL_SIZE_L_CM',
    'width' => '10%',
    'default' => true,
  ),
  'size_w_cm' => 
  array (
    'type' => 'decimal',
    'vname' => 'LBL_SIZE_W_CM',
    'width' => '10%',
    'default' => true,
  ),
  'size_h_cm' => 
  array (
    'type' => 'decimal',
    'vname' => 'LBL_SIZE_H_CM',
    'width' => '10%',
    'default' => true,
  ),
  'size_w_h_cm' => 
  array (
    'type' => 'decimal',
    'vname' => 'LBL_SIZE_W_H_CM',
    'width' => '10%',
    'default' => true,
  ),
  'size_l_inch' => 
  array (
    'type' => 'decimal',
    'vname' => 'LBL_SIZE_L_INCH',
    'width' => '10%',
    'default' => true,
  ),
  'size_w_inch' => 
  array (
    'type' => 'decimal',
    'vname' => 'LBL_SIZE_W_INCH',
    'width' => '10%',
    'default' => true,
  ),
  'size_h_inch' => 
  array (
    'type' => 'decimal',
    'vname' => 'LBL_SIZE_H_INCH',
    'width' => '10%',
    'default' => true,
  ),
  'size_w_h_inch' => 
  array (
    'type' => 'decimal',
    'vname' => 'LBL_SIZE_W_H_INCH',
    'width' => '10%',
    'default' => true,
  ),
  'edit_button' => 
  array (
    'vname' => 'LBL_EDIT_BUTTON',
    'widget_class' => 'SubPanelEditButton',
    'module' => 'AOS_Size',
    'width' => '4%',
    'default' => true,
  ),
  'remove_button' => 
  array (
    'vname' => 'LBL_REMOVE',
    'widget_class' => 'SubPanelRemoveButton',
    'module' => 'AOS_Size',
    'width' => '5%',
    'default' => true,
  ),
);