<?php
$module_name = 'AOS_Size';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'SIZE_L_CM' => 
  array (
    'type' => 'decimal',
    'label' => 'LBL_SIZE_L_CM',
    'width' => '10%',
    'default' => true,
  ),
  'SIZE_W_CM' => 
  array (
    'type' => 'decimal',
    'label' => 'LBL_SIZE_W_CM',
    'width' => '10%',
    'default' => true,
  ),
  'SIZE_H_CM' => 
  array (
    'type' => 'decimal',
    'label' => 'LBL_SIZE_H_CM',
    'width' => '10%',
    'default' => true,
  ),
  'SIZE_W_H_CM' => 
  array (
    'type' => 'decimal',
    'label' => 'LBL_SIZE_W_H_CM',
    'width' => '10%',
    'default' => true,
  ),
  'SIZE_L_INCH' => 
  array (
    'type' => 'decimal',
    'label' => 'LBL_SIZE_L_INCH',
    'width' => '10%',
    'default' => true,
  ),
  'SIZE_W_INCH' => 
  array (
    'type' => 'decimal',
    'label' => 'LBL_SIZE_W_INCH',
    'width' => '10%',
    'default' => true,
  ),
  'SIZE_H_INCH' => 
  array (
    'type' => 'decimal',
    'label' => 'LBL_SIZE_H_INCH',
    'width' => '10%',
    'default' => true,
  ),
  'SIZE_W_H_INCH' => 
  array (
    'type' => 'decimal',
    'label' => 'LBL_SIZE_W_H_INCH',
    'width' => '10%',
    'default' => true,
  ),
  'DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
  'MODIFIED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_MODIFIED_NAME',
    'id' => 'MODIFIED_USER_ID',
    'width' => '10%',
    'default' => false,
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => false,
  ),
  'CREATED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'default' => false,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => false,
  ),
);
;
?>
