const global_size_product_cont =2.54;
    var size_cm_elements = [
        document.getElementById("size_l_cm"),
        document.getElementById("size_w_cm"),
        document.getElementById("size_h_cm")
    ];

    var size_inch_elements = [
        document.getElementById("size_l_inch"),
        document.getElementById("size_w_inch"),
        document.getElementById("size_h_inch")
    ];

    function handleSize(cmElements, inchElements) {
        for (var i = 0; i < cmElements.length; i++) {
             inchElements[i].value = (cmElements[i].value / global_size_product_cont).toFixed(2);
        }
    }

    for (var i = 0; i < size_cm_elements.length; i++) {
        size_cm_elements[i].addEventListener("blur", function () {
            // Kiểm tra nếu giá trị không rỗng
            if (this.value.trim() !== "") {
                handleSize(size_cm_elements, size_inch_elements);
            }
        });
    }
