<?php
// created: 2024-06-14 03:32:18
$mod_strings = array (
  'LBL_EMAILS_ACCOUNTS_REL' => 'Emails:Accounts',
  'LBL_ACCOUNTS_SUBPANEL_TITLE' => 'Đại lý',
  'LBL_EMAILS_LEADS_REL' => 'Emails:Leads',
  'LBL_LEADS_SUBPANEL_TITLE' => 'KH Tiềm năng',
  'LBL_EMAILS_OPPORTUNITIES_REL' => 'Emails:Opportunities',
  'LBL_EMAILS_PROSPECT_REL' => 'Emails:Prospect',
  'LBL_OPPORTUNITY_SUBPANEL_TITLE' => 'Cơ hội bán hàng',
  'LBL_EMAILS_CONTACTS_REL' => 'Emails:Contacts',
  'LBL_CONTACTS_SUBPANEL_TITLE' => 'Người liên hệ đại lý',
  'LBL_EMAILS_CASES_REL' => 'Emails:Cases',
  'LBL_EMAILS_PROJECT_REL' => 'Emails:Project',
  'LBL_EMAILS_TASKS_REL' => 'Emails:Tasks',
  'LBL_EMAILS_NOTES_REL' => 'Emails:Notes',
  'LBL_EMAILS_MEETINGS_REL' => 'Emails:Meetings',
  'LBL_NOTES_SUBPANEL_TITLE' => 'Đính kèm',
  'LBL_CASES_SUBPANEL_TITLE' => 'Tickets',
  'LBL_PROJECT_SUBPANEL_TITLE' => 'Dự án',
  'LBL_EMAIL_TYPE' => 'Loại email',
);