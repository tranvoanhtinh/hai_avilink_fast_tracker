<?php
// created: 2024-06-05 06:17:26
$mod_strings = array (
  'LBL_EMAILS_ACCOUNTS_REL' => 'Emails:Agent',
  'LBL_EMAILS_LEADS_REL' => 'Emails:Leads',
  'LBL_ACCOUNTS_SUBPANEL_TITLE' => 'Agent',
  'LBL_LEADS_SUBPANEL_TITLE' => 'Leads',
  'LBL_EMAILS_CONTACTS_REL' => 'Emails:Contacts',
  'LBL_CONTACTS_SUBPANEL_TITLE' => 'Contactss',
  'LBL_EMAILS_OPPORTUNITIES_REL' => 'Emails:Opportunities',
  'LBL_OPPORTUNITY_SUBPANEL_TITLE' => 'Opportunities',
  'LBL_EMAILS_CASES_REL' => 'Emails:Ticketss',
  'LBL_EMAILS_TASKS_REL' => 'Emails:Tasks',
  'LBL_EMAILS_NOTES_REL' => 'Emails:Notess',
  'LBL_EMAILS_MEETINGS_REL' => 'Emails:Meetings',
  'LBL_NOTES_SUBPANEL_TITLE' => 'Attachments',
  'LBL_CASES_SUBPANEL_TITLE' => 'Ticketss',
  'LBL_EMAILS_PROJECT_REL' => 'Emails:Project',
  'LBL_EMAILS_PROSPECT_REL' => 'Emails:Prospect',
  'LBL_PROJECT_SUBPANEL_TITLE' => 'Projects',
  'LBL_EMAIL_TYPE' => 'Email Type',
);