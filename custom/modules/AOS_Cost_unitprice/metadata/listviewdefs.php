<?php
$module_name = 'AOS_Cost_unitprice';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'SERVICE_CODE' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_SERVICE_CODE',
    'width' => '10%',
    'default' => true,
  ),
  'EXCHANGE_RATE' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_EXCHANGE_RATE',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
  ),
  'PRICE' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_PRICE',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
  ),
  'STATUS' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_STATUS',
    'width' => '10%',
    'default' => true,
  ),
  'ACCOUNTS_AOS_COST_UNITPRICE_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_ACCOUNTS_AOS_COST_UNITPRICE_1_FROM_ACCOUNTS_TITLE',
    'id' => 'ACCOUNTS_AOS_COST_UNITPRICE_1ACCOUNTS_IDA',
    'width' => '10%',
    'default' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
  'SERVICE_DATE' => 
  array (
    'type' => 'date',
    'label' => 'LBL_SERVICE_DATE',
    'width' => '10%',
    'default' => false,
  ),
  'DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'CREATED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'default' => false,
  ),
  'MODIFIED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_MODIFIED_NAME',
    'id' => 'MODIFIED_USER_ID',
    'width' => '10%',
    'default' => false,
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => false,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => false,
  ),
  'AOS_PRODUCTS_AOS_COST_UNITPRICE_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_AOS_PRODUCTS_AOS_COST_UNITPRICE_1_FROM_AOS_PRODUCTS_TITLE',
    'id' => 'AOS_PRODUCTS_AOS_COST_UNITPRICE_1AOS_PRODUCTS_IDA',
    'width' => '10%',
    'default' => false,
  ),
);
;
?>
