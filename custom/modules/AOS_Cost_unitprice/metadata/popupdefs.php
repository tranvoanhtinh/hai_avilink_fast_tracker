<?php
$popupMeta = array (
    'moduleMain' => 'AOS_Cost_unitprice',
    'varName' => 'AOS_Cost_unitprice',
    'orderBy' => 'aos_cost_unitprice.name',
    'whereClauses' => array (
  'name' => 'aos_cost_unitprice.name',
  'accounts_aos_cost_unitprice_1_name' => 'aos_cost_unitprice.accounts_aos_cost_unitprice_1_name',
  'assigned_user_id' => 'aos_cost_unitprice.assigned_user_id',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'accounts_aos_cost_unitprice_1_name',
  5 => 'assigned_user_id',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => '10%',
  ),
  'accounts_aos_cost_unitprice_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_ACCOUNTS_AOS_COST_UNITPRICE_1_FROM_ACCOUNTS_TITLE',
    'id' => 'ACCOUNTS_AOS_COST_UNITPRICE_1ACCOUNTS_IDA',
    'width' => '10%',
    'name' => 'accounts_aos_cost_unitprice_1_name',
  ),
  'assigned_user_id' => 
  array (
    'name' => 'assigned_user_id',
    'label' => 'LBL_ASSIGNED_TO',
    'type' => 'enum',
    'function' => 
    array (
      'name' => 'get_user_array',
      'params' => 
      array (
        0 => false,
      ),
    ),
    'width' => '10%',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'SERVICE_CODE' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_SERVICE_CODE',
    'width' => '10%',
    'default' => true,
    'name' => 'service_code',
  ),
  'ACCOUNTS_AOS_COST_UNITPRICE_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_ACCOUNTS_AOS_COST_UNITPRICE_1_FROM_ACCOUNTS_TITLE',
    'id' => 'ACCOUNTS_AOS_COST_UNITPRICE_1ACCOUNTS_IDA',
    'width' => '10%',
    'default' => true,
    'name' => 'accounts_aos_cost_unitprice_1_name',
  ),
  'PRICE' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_PRICE',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
    'name' => 'price',
  ),
  'STATUS' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_STATUS',
    'width' => '10%',
    'default' => true,
    'name' => 'status',
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
    'name' => 'assigned_user_name',
  ),
),
);
