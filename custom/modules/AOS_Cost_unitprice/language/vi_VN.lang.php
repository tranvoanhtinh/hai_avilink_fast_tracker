<?php
// created: 2024-07-18 06:13:07
$mod_strings = array (
  'LBL_SERVICE_CODE' => 'Mã dịch vụ',
  'LBL_SERVICE_NAME' => 'Tên dịch vụ',
  'LBL_PRICE' => 'Giá',
  'LBL_STATUS' => 'Trạng thái',
  'LBL_SERVICE_DATE' => 'Ngày giá dịch vụ',
  'LBL_PRIORITIZE' => 'Ưu tiên',
  'LBL_VND' => 'VND',
  'LBL_TYPE_CURRENCY' => 'Loại Tiền',
  'LBL_TYPE_SERVICE' => 'Loại dịch vụ',
  'LBL_USD' => 'USD',
  'LBL_EXCHANGE_RATE' => 'Tỷ giá',
  'LNK_NEW_RECORD' => 'Tạo Thiết lập giá vốn',
  'LNK_LIST' => 'Xem Thiết lập giá vốn',
  'LNK_IMPORT_AOS_COST_UNITPRICE' => 'Nhập Thiết lập giá vốn',
  'LBL_LIST_FORM_TITLE' => 'Thiết lập giá vốn Danh sách',
  'LBL_SEARCH_FORM_TITLE' => 'Tìm kiếm Thiết lập giá vốn',
  'LBL_HOMEPAGE_TITLE' => 'Của tôi Thiết lập giá vốn',
);