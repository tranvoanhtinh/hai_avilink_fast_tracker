<?php
// created: 2024-05-27 03:43:05
$mod_strings = array (
  'LBL_SERVICE_CODE' => 'Service Code',
  'LBL_SERVICE_NAME' => 'Service Name',
  'LBL_PRICE' => 'Price',
  'LBL_STATUS' => 'Status',
  'LBL_SERVICE_DATE' => 'Service Date',
  'LBL_PRIORITIZE' => 'Prioritize',

  'LBL_VND' => 'VND',
  'LBL_TYPE_CURRENCY' => 'Currency Type',
  'LBL_TYPE_SERVICE' => 'Service Type',
  'LBL_USD' => 'USD',
  'LBL_EXCHANGE_RATE' => 'Exchange Rate',
);
