<?php
$module_name = 'cs_cusUser';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'username',
          ),
          1 => 
          array (
            'name' => 'title',
            'label' => 'LBL_TITLE',
          ),
        ),
        1 => 
        array (
          0 => 'name',
          1 => 
          array (
            'name' => 'status',
            'studio' => 'visible',
            'label' => 'LBL_STATUS',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'email',
            'label' => 'LBL_EMAIL',
          ),
          1 => 
          array (
            'name' => 'mobile',
            'label' => 'LBL_MOBILE',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'provincecode',
            'studio' => 'visible',
            'label' => 'LBL_PROVINCECODE',
          ),
          1 => 
          array (
            'name' => 'districtcode_c',
            'studio' => 'visible',
            'label' => 'LBL_DISTRICTCODE',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'role',
            'studio' => 'visible',
            'label' => 'LBL_ROLE',
          ),
          1 => 
          array (
            'name' => 'DiscountPercent',
            'label' => 'LBL_DISCOUNT_PERCENT',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'groupname',
            'studio' => 'visible',
            'label' => 'LBL_GROUPNAME',
          ),
          1 => 
          array (
            'name' => 'groupname2',
            'studio' => 'visible',
            'label' => 'LBL_GROUPNAME2',
          ),
        ),
        6 => 
        array (
          0 => 'description',
          1 => 'assigned_user_name',
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'groupid',
            'label' => 'LBL_GROUPID',
          ),
        ),
      ),
    ),
  ),
);
;
?>
