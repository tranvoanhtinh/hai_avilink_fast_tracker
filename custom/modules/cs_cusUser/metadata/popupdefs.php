<?php
$popupMeta = array (
    'moduleMain' => 'cs_cusUser',
    'varName' => 'cs_cusUser',
    'orderBy' => 'cs_cususer.name',
    'whereClauses' => array (
  'name' => 'cs_cususer.name',
  'username' => 'cs_cususer.username',
  'status' => 'cs_cususer.status',
  'assigned_user_id' => 'cs_cususer.assigned_user_id',
  'description' => 'cs_cususer.description',
),
    'searchInputs' => array (
  1 => 'name',
  3 => 'status',
  4 => 'username',
  5 => 'assigned_user_id',
  6 => 'description',
),
    'searchdefs' => array (
  'username' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_USERNAME',
    'width' => '10%',
    'name' => 'username',
  ),
  'name' => 
  array (
    'name' => 'name',
    'width' => '10%',
  ),
  'status' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_STATUS',
    'width' => '10%',
    'name' => 'status',
  ),
  'assigned_user_id' => 
  array (
    'name' => 'assigned_user_id',
    'label' => 'LBL_ASSIGNED_TO',
    'type' => 'enum',
    'function' => 
    array (
      'name' => 'get_user_array',
      'params' => 
      array (
        0 => false,
      ),
    ),
    'width' => '10%',
  ),
  'description' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'name' => 'description',
  ),
),
    'listviewdefs' => array (
  'USERNAME' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_USERNAME',
    'width' => '10%',
    'default' => true,
    'name' => 'username',
  ),
  'STATUS' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_STATUS',
    'width' => '10%',
    'default' => true,
    'name' => 'status',
  ),
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'GROUPNAME' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_GROUPNAME',
    'width' => '10%',
    'default' => true,
    'name' => 'groupname',
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
    'name' => 'assigned_user_name',
  ),
),
);
