<?php
class Users_UpdateUser
{
	
	function ensure_length(&$string, $length)
	{
		$strlen = strlen($string);
		if ($strlen < $length) {
			$string = str_pad($string, $length, '0');
		} elseif ($strlen > $length) {
			$string = substr($string, 0, $length);
		}
	}
	function create_guid_section($characters)
	{
		$return = '';
		for ($i = 0; $i < $characters; ++$i) {
			$return .= dechex(mt_rand(0, 15));
		}

		return $return;
	}
	function create_guid()
	{
		$microTime = microtime();
		list($a_dec, $a_sec) = explode(' ', $microTime);

		$dec_hex = dechex($a_dec * 1000000);
		$sec_hex = dechex($a_sec);

		$this->ensure_length($dec_hex, 5);
		$this->ensure_length($sec_hex, 6);

		$guid = '';
		$guid .= $dec_hex;
		$guid .= $this->create_guid_section(3);
		$guid .= '-';
		$guid .= $this->create_guid_section(4);
		$guid .= '-';
		$guid .= $this->create_guid_section(4);
		$guid .= '-';
		$guid .= $this->create_guid_section(4);
		$guid .= '-';
		$guid .= $sec_hex;
		$guid .= $this->create_guid_section(6);

		return $guid;
	}
    function updateUser($bean, $event, $arguments)
    {
		global $db;
		global $sugar_config;
		if(!empty($bean->username) && !empty($bean->status)){
			date_default_timezone_set('Asia/Ho_Chi_Minh');
			$time=date('Y-m-d h:i:s');
			$pass=$sugar_config["lp_psd"];
				if(empty($bean->refuser)){
						$id = $this->create_guid();
						$query1 = "insert into users (id,user_name, last_name, status, title, department, user_hash,date_entered,date_modified,modified_user_id,created_by,deleted,is_group,factor_auth,DiscountPercent,phone_mobile) values
						('".$id."','".$bean->username."','".$bean->name."','".$bean->status."','".html_entity_decode($bean->title)."','". html_entity_decode($bean->groupname)."',
						'".$pass."','".$time."','".$time."','".$bean->assigned_user_id."','".$bean->assigned_user_id."',0,0,0,".$bean->DiscountPercent.",'".$bean->mobile."')";
						$result1 = $db->query($query1);
						if($bean->email != null && $bean->email != "")
						{
							$EUpper=strtoupper($bean->email);
							$eId=create_guid();
							$eRId=create_guid();
							$q="insert into  email_addresses (id,email_address,email_address_caps,date_created,date_modified) values
							('".$eId."','".$bean->email."','".$EUpper."','".$time."','".$time."')";
							$r1=$db->query($q);
							$q="insert into  email_addr_bean_rel (id,email_address_id,bean_id,bean_module,date_created,date_modified,primary_address) values('".$eRId."','".$eId."','".$id."','Users','".$time."','".$time."','1')";
							$r2=$db->query($q);
						}
						//if(!empty($bean->role)){
							$ida = $this->create_guid();
							$query5 = "insert into acl_roles_users (id,role_id,user_id,date_modified,userrefid) values ('".$ida."','".$bean->role."','".$id."','".$time."','".$bean->id."')";
							$db->query($query5);
						//}
						if(!empty($bean->groupid)){
							$idg = $this->create_guid();
							$query6 = "insert into securitygroups_users (id,securitygroup_id,user_id,date_modified) values ('".$idg."','".$bean->groupid."','".$id."','".$time."')";
							$db->query($query6);
						}
							if(!empty($bean->groupname2)){
								$group=explode(',',$bean->groupname2);
								for($j = 0; $j < count($group); $j++){
									$group[$j] = trim($group[$j],'^');
									if($group[$j] != $bean->groupid){
										$idp = $this->create_guid();
										$query7 = "insert into securitygroups_users (id,securitygroup_id,user_id,date_modified) values ('".$idp."','".$group[$j]."','".$id."','".$time."')";
										$db->query($query7);
									}
								}
							}
						
						$query2 = "update cs_cususer set refuser = '".$id."' where username = '".$bean->username."' and deleted='0'";
						$result2 = $db->query($query2);
						
						$query3 = "insert into users_cstm (id_c,provincecode_c,districtcode_c) values ('".$id."','".$bean->provincecode."','".$bean->districtcode_c."')";
						$result3 = $db->query($query3);
				}else{
					//if(!empty($row)){			
							$query1 = "update users set user_name = '".$bean->username."', last_name = '".$bean->name."', status = '".$bean->status."', title = '".html_entity_decode($bean->title)."',
							department = '".html_entity_decode($bean->groupname)."', date_modified = '".$time."',
							modified_user_id = '".$bean->assigned_user_id."',DiscountPercent = '".$bean->DiscountPercent."', created_by = '".$bean->assigned_user_id."' ,phone_mobile = '".$bean->mobile."' where id = '".$bean->refuser."' and deleted = '0'";
							$result1 = $db->query($query1);
							
							$query2 = "update users_cstm inner join users on users_cstm.id_c = users.id set provincecode_c = '".$bean->provincecode."', districtcode_c = '".$bean->districtcode_c."' where 
							users.id = '".$bean->refuser."' and users.deleted = '0'";
							$result2 = $db->query($query2);
							if($bean->email != null && $bean->email != "")
							{ 
								$q = "select * from email_addresses inner join email_addr_bean_rel on email_addresses.id = email_addr_bean_rel.email_address_id where email_addr_bean_rel.bean_id = '".$bean->refuser."' and email_addresses.deleted = '0' and email_addr_bean_rel.primary_address = '1'";
								$r = $db->query($q);
								$row = $db->fetchByAssoc($r);
								if(!empty($row["id"])){
									$EUpper=strtoupper($bean->email);
									$eId=create_guid();
									$eRId=create_guid();
									$q="update  email_addresses inner join email_addr_bean_rel on email_addresses.id = email_addr_bean_rel.email_address_id set email_address = '".$bean->email."',email_address_caps = '".$EUpper."', email_addresses.date_modified = '".$time."'  where email_addr_bean_rel.bean_id = '".$bean->refuser."' and email_addr_bean_rel.primary_address = '1'";
									$db->query($q);
								}else{
									$time=date('Y-m-d h:i:s');
									$EUpper=strtoupper($bean->email);
									$eId=create_guid();
									$eRId=create_guid();
									$q="insert into  email_addresses (id,email_address,email_address_caps,date_created,date_modified) values
									('".$eId."','".$bean->email."','".$EUpper."','".$time."','".$time."')";
									$r1=$db->query($q);
									$q="insert into  email_addr_bean_rel (id,email_address_id,bean_id,bean_module,date_created,date_modified,primary_address) values('".$eRId."','".$eId."','".$bean->refuser."','Users','".$time."','".$time."','1')";
									$r2=$db->query($q);
								}
							}
							//if(!empty($bean->role)){
								$qry = "select id from users where id = '".$bean->refuser."' and deleted = '0'";
								$resu = $db->query($qry);
								$id_row = $db->fetchByAssoc($resu);
								if(!empty($id_row)){
									$query5 = "update acl_roles_users set role_id = '".$bean->role."' where user_id = '".$id_row["id"]."' and deleted = '0' and userrefid = '".$bean->id."'";
									$db->query($query5);
									$query6 = "delete from  securitygroups_users where user_id = '".$id_row["id"]."' and deleted = '0'";
									$db->query($query6);
									if(!empty($bean->groupid)){
										$idg = $this->create_guid();
										$query7 = "insert into securitygroups_users (id,securitygroup_id,user_id,date_modified) values ('".$idg."','".$bean->groupid."','".$id_row["id"]."','".$time."')";
										$db->query($query7);
									}
									if(!empty($bean->groupname2)){
										$group=explode(',',$bean->groupname2);
										for($j = 0; $j < count($group); $j++){
											$group[$j] = trim($group[$j],'^');
											if($group[$j] != $bean->groupid){
											$idp = $this->create_guid();
											$query8 = "insert into securitygroups_users (id,securitygroup_id,user_id,date_modified) values ('".$idp."','".$group[$j]."','".$id_row["id"]."','".$time."')";
											$db->query($query8);
											}
										}
									}
								}
							
				}
		}
    }

}
