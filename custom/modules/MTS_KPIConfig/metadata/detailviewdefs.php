<?php
$module_name = 'MTS_KPIConfig';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_DETAILVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 
          array (
            'name' => 'status',
            'studio' => 'visible',
            'label' => 'LBL_STATUS',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'effect_date',
            'label' => 'LBL_EFFECT_DATE',
          ),
          1 => 
          array (
            'name' => 'expiry_date',
            'label' => 'LBL_EXPIRY_DATE',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'number_call',
            'label' => 'LBL_NUMBER_CALL',
          ),
          1 => 
          array (
            'name' => 'number_meeting',
            'label' => 'LBL_NUMBER_MEETING',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'number_call_center',
            'label' => 'LBL_NUMBER_CALL_CENTER',
          ),
          1 => '',
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'number_email',
            'label' => 'LBL_NUMBER_EMAIL',
          ),
          1 => 
          array (
            'name' => 'number_email_quote',
            'label' => 'LBL_NUMBER_EMAIL_QUOTE',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'number_new_customer',
            'label' => 'LBL_NUMBER_NEW_CUSTOMER',
          ),
          1 => 
          array (
            'name' => 'number_inprocess_customer',
            'label' => 'LBL_NUMBER_INPROCESS_CUSTOMER',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'number_converted_customer',
            'label' => 'LBL_NUMBER_CONVERTED_CUSTOMER',
          ),
          1 => 
          array (
            'name' => 'revenue',
            'label' => 'LBL_REVENUE',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'number_new_customer_buyer',
            'label' => 'LBL_NUMBER_NEW_CUSTOMER_CENTER_BUYER',
          ),
          1 => 
          array (
            'name' => 'number_exit_customer_buyer',
            'label' => 'LBL_NUMBER_EXIT_CUSTOMER_CENTER_BUYER',
          ),
        ),
        8 => 
        array (
          0 => 'description',
          1 => 'assigned_user_name',
        ),
      ),
      'lbl_detailview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'date_entered',
            'comment' => 'Date record created',
            'label' => 'LBL_DATE_ENTERED',
          ),
          1 => 
          array (
            'name' => 'date_modified',
            'comment' => 'Date record last modified',
            'label' => 'LBL_DATE_MODIFIED',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'created_by_name',
            'label' => 'LBL_CREATED',
          ),
          1 => 
          array (
            'name' => 'modified_by_name',
            'label' => 'LBL_MODIFIED_NAME',
          ),
        ),
      ),
    ),
  ),
);
;
?>
