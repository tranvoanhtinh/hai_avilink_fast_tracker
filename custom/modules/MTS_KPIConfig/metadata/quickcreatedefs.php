<?php
$module_name = 'MTS_KPIConfig';
$viewdefs [$module_name] = 
array (
  'QuickCreate' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 
          array (
            'name' => 'status',
            'studio' => 'visible',
            'label' => 'LBL_STATUS',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'effect_date',
            'label' => 'LBL_EFFECT_DATE',
          ),
          1 => 
          array (
            'name' => 'expiry_date',
            'label' => 'LBL_EXPIRY_DATE',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'number_call',
            'label' => 'LBL_NUMBER_CALL',
          ),
          1 => 
          array (
            'name' => 'number_meeting',
            'label' => 'LBL_NUMBER_MEETING',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'number_call_center',
            'label' => 'LBL_NUMBER_CALL_CENTER',
          ),
          1 => '',
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'number_email',
            'label' => 'LBL_NUMBER_EMAIL',
          ),
          1 => 
          array (
            'name' => 'number_email_quote',
            'label' => 'LBL_NUMBER_EMAIL_QUOTE',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'number_new_customer',
            'label' => 'LBL_NUMBER_NEW_CUSTOMER',
          ),
          1 => 
          array (
            'name' => 'number_inprocess_customer',
            'label' => 'LBL_NUMBER_INPROCESS_CUSTOMER',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'number_converted_customer',
            'label' => 'LBL_NUMBER_CONVERTED_CUSTOMER',
          ),
          1 => 
          array (
            'name' => 'revenue',
            'label' => 'LBL_REVENUE',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'description',
            'comment' => 'Full text of the note',
            'label' => 'LBL_DESCRIPTION',
          ),
          1 => 'assigned_user_name',
        ),
      ),
    ),
  ),
);
;
?>
