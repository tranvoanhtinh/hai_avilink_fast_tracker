<?php
$popupMeta = array (
    'moduleMain' => 'MTS_KPIConfig',
    'varName' => 'MTS_KPIConfig',
    'orderBy' => 'mts_kpiconfig.name',
    'whereClauses' => array (
  'name' => 'mts_kpiconfig.name',
  'status' => 'mts_kpiconfig.status',
  'assigned_user_id' => 'mts_kpiconfig.assigned_user_id',
),
    'searchInputs' => array (
  1 => 'name',
  3 => 'status',
  4 => 'assigned_user_id',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => '10%',
  ),
  'status' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_STATUS',
    'width' => '10%',
    'name' => 'status',
  ),
  'assigned_user_id' => 
  array (
    'name' => 'assigned_user_id',
    'label' => 'LBL_ASSIGNED_TO',
    'type' => 'enum',
    'function' => 
    array (
      'name' => 'get_user_array',
      'params' => 
      array (
        0 => false,
      ),
    ),
    'width' => '10%',
  ),
),
);
