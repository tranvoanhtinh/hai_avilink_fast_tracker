<?php
$module_name = 'MTS_KPIConfig';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'EFFECT_DATE' => 
  array (
    'type' => 'date',
    'label' => 'LBL_EFFECT_DATE',
    'width' => '10%',
    'default' => true,
  ),
  'EXPIRY_DATE' => 
  array (
    'type' => 'date',
    'label' => 'LBL_EXPIRY_DATE',
    'width' => '10%',
    'default' => true,
  ),
  'NUMBER_CALL' => 
  array (
    'type' => 'int',
    'default' => true,
    'label' => 'LBL_NUMBER_CALL',
    'width' => '10%',
  ),
  'NUMBER_MEETING' => 
  array (
    'type' => 'int',
    'default' => true,
    'label' => 'LBL_NUMBER_MEETING',
    'width' => '10%',
  ),
  'NUMBER_EMAIL' => 
  array (
    'type' => 'int',
    'default' => true,
    'label' => 'LBL_NUMBER_EMAIL',
    'width' => '10%',
  ),
  'NUMBER_EMAIL_QUOTE' => 
  array (
    'type' => 'int',
    'default' => true,
    'label' => 'LBL_NUMBER_EMAIL_QUOTE',
    'width' => '10%',
  ),
  'NUMBER_NEW_CUSTOMER' => 
  array (
    'type' => 'int',
    'default' => true,
    'label' => 'LBL_NUMBER_NEW_CUSTOMER',
    'width' => '10%',
  ),
  'NUMBER_INPROCESS_CUSTOMER' => 
  array (
    'type' => 'int',
    'default' => true,
    'label' => 'LBL_NUMBER_INPROCESS_CUSTOMER',
    'width' => '10%',
  ),
  'REVENUE' => 
  array (
    'type' => 'currency',
    'default' => true,
    'label' => 'LBL_REVENUE',
    'currency_format' => true,
    'width' => '10%',
  ),
  'STATUS' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_STATUS',
    'width' => '10%',
    'default' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
  'NUMBER_CONVERTED_CUSTOMER' => 
  array (
    'type' => 'int',
    'default' => false,
    'label' => 'LBL_NUMBER_CONVERTED_CUSTOMER',
    'width' => '10%',
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => false,
  ),
  'NUMBER_CALL_CENTER' => 
  array (
    'type' => 'int',
    'default' => false,
    'label' => 'LBL_NUMBER_CALL_CENTER',
    'width' => '10%',
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => false,
  ),
  'DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'MODIFIED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_MODIFIED_NAME',
    'id' => 'MODIFIED_USER_ID',
    'width' => '10%',
    'default' => false,
  ),
  'CREATED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'default' => false,
  ),
);
;
?>
