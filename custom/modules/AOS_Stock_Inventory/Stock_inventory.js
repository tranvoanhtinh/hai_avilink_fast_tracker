document.addEventListener('DOMContentLoaded', function () {
    // Chờ tất cả các phần tử HTML được tải xong

    // Lấy tất cả các thẻ div có class là "label" và có nội dung là "LBL_BUSINESS_REPORT:"
    var labelDivs = document.querySelectorAll('.label');

    // Lặp qua từng thẻ div
    labelDivs.forEach(function (labelDiv) {
        // Kiểm tra xem nội dung của thẻ có phải là "LBL_BUSINESS_REPORT:" không
        if (
            labelDiv.textContent.trim() === 'LBL_STOCK_INVENTORY:' ||
            labelDiv.textContent.trim() === 'LBL_CHOOSE_TIME:'
     
        ) {
            // Ẩn thẻ div
            labelDiv.style.display = 'none';
        }
    });
});

// Lấy đối tượng phần tử HTML
var element = document.querySelector('div.col-xs-12.col-sm-10.detail-view-field[type="function"][field="stock_inventory"][colspan="3"]');

// Kiểm tra xem phần tử có tồn tại không
if (element) {
  // Gán chiều rộng là 130%
  element.style.width = '100%';
  element.style.marginTop = '10px';
} 

var element = document.querySelector('div.col-xs-12.col-sm-10.detail-view-field[type="function"][field="choose_time"][colspan="3"]');

// Kiểm tra xem phần tử có tồn tại không
if (element) {
  // Gán chiều rộng là 130%
  element.style.width = '100%';
} 

// Function để cập nhật ngày tương ứng dựa trên tháng và năm đã chọn
// Function để cập nhật ngày tương ứng dựa trên tháng và năm đã chọn
function updateDate() {
    var month = document.getElementsByName('month')[0].value;
    var year = document.getElementsByName('year')[0].value;

    // Kiểm tra nếu giá trị của ô chọn tháng là rỗng
    if (month === '') {
        // Đặt giá trị của các ô datefrom và dateto về rỗng
        document.getElementsByName('datefrom')[0].value = '';
        document.getElementsByName('dateto')[0].value = '';
    } else {
        // Nếu năm không được chọn, tự động chọn năm hiện tại
        if (year === '') {
            var currentYear = new Date().getFullYear();
            document.getElementsByName('year')[0].value = currentYear;
        }

        // Xác định ngày đầu tiên và ngày cuối cùng của tháng được chọn
        var firstDay = new Date(year, month - 1, 1);
        var lastDay = new Date(year, month, 0);

        // Định dạng ngày từ và ngày đến
        var fromDate = formatDate(firstDay);
        var toDate = formatDate(lastDay);

        // Cập nhật giá trị của các ô input datefrom và dateto
        document.getElementsByName('datefrom')[0].value = fromDate;
        document.getElementsByName('dateto')[0].value = toDate;
    }
}


// Hàm để định dạng ngày theo dd/mm/yyyy
function formatDate(date) {
    var dd = String(date.getDate()).padStart(2, '0');
    var mm = String(date.getMonth() + 1).padStart(2, '0'); // January is 0!
    var yyyy = date.getFullYear();

    return dd + '/' + mm + '/' + yyyy;
}

// Thêm sự kiện onchange cho tháng và năm
document.getElementsByName('month')[0].onchange = updateDate;
document.getElementsByName('year')[0].onchange = updateDate;

// Thêm sự kiện onchange cho input datefrom và dateto
document.getElementsByName('datefrom')[0].onchange = function() {
    document.getElementsByName('month')[0].value = '';
};

document.getElementsByName('dateto')[0].onchange = function() {
    document.getElementsByName('month')[0].value = '';
};

// Thêm sự kiện onchange cho input datefrom và dateto (sửa lại cho việc xóa giá trị ô input "Năm")
document.getElementsByName('datefrom')[0].onchange = function() {
    document.getElementsByName('year')[0].value = '';
    document.getElementsByName('month')[0].value = '';
};

document.getElementsByName('dateto')[0].onchange = function() {
    document.getElementsByName('year')[0].value = '';
    document.getElementsByName('month')[0].value = '';
};


   // Function để xóa tất cả điều kiện lọc
    function clearFilters() {
        // Đặt giá trị của các ô input "Từ", "Đến", và dropdown "Tháng" về rỗng
        document.getElementsByName('datefrom')[0].value = '';
        document.getElementsByName('dateto')[0].value = '';
        document.getElementsByName('month')[0].value = '';

        // Lấy năm hiện tại và đặt giá trị của ô input "Năm" về năm hiện tại
        var currentYear = new Date().getFullYear();
        document.getElementsByName('year')[0].value = currentYear;
    }