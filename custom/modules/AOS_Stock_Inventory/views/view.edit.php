<?php
require_once('include/MVC/View/views/view.edit.php');

class CustomAOS_Stock_inventoryViewEdit extends ViewEdit
{
    function display()
    {
        echo '<style>

.edit-view-row {
    /* Sử dụng flexbox để tự động xếp các cột trên cùng một dòng */
    display: flex;
    flex-wrap: wrap; /* Cho phép các cột tự động xuống dòng khi không đủ không gian */
}

.edit-view-row-item {
    width: 25%; /* Đặt độ rộng của mỗi cột là 25% để hiển thị 4 cột trên mỗi dòng */
    box-sizing: border-box; /* Đảm bảo rằng padding và border không làm thay đổi kích thước của phần tử */
    padding: 0 15px; /* Thêm padding nếu cần */
}

.label {
    font-weight: bold; /* Tạo đậm nhãn */
}

.edit-view-field {
    width: 100%; /* Đặt độ rộng của trường dữ liệu là 100% để điền vào toàn bộ không gian của cột */
}
/* Đảo ngược vị trí của nhãn và trường nhập liệu */
.edit-view-row-item {
    display: flex; /* Sử dụng flexbox */
    align-items: center; /* Căn chỉnh các phần tử theo chiều dọc */
}

/* Chỉnh kích thước và vị trí của nhãn */
.label {
    flex: 1; /* Mở rộng nhãn để chiếm hết không gian còn lại */
    font-weight: bold; /* Tạo đậm nhãn */
}

/* Chỉnh kích thước và vị trí của trường nhập liệu */
.edit-view-field {
    flex: 3; /* Trường nhập liệu chiếm 3 phần trong tổng số 4 phần */
    margin-left: 10px; /* Tạo khoảng cách giữa nhãn và trường nhập liệu */
}

        </style>';
        parent::display(); // Gọi phương thức hiển thị từ lớp cha

    }
}
?>
