<?php
$module_name = 'AOS_Stock_Inventory';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'maincode' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_MAINCODE',
        'width' => '10%',
        'default' => true,
        'name' => 'maincode',
      ),
      'name' => 
      array (
        'type' => 'name',
        'link' => true,
        'label' => 'LBL_NAME',
        'width' => '10%',
        'default' => true,
        'name' => 'name',
      ),
      'month' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_MONTH',
        'width' => '10%',
        'default' => true,
        'name' => 'month',
      ),
      'year' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_YEAR',
        'width' => '10%',
        'default' => true,
        'name' => 'year',
      ),
    ),
    'advanced_search' => 
    array (
      'maincode' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_MAINCODE',
        'width' => '10%',
        'default' => true,
        'name' => 'maincode',
      ),
      'warehouse' => 
      array (
        'type' => 'enum',
        'label' => 'LBL_WAREHOUSE',
        'width' => '10%',
        'default' => true,
        'name' => 'warehouse',
      ),
      'month' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_MONTH',
        'width' => '10%',
        'default' => true,
        'name' => 'month',
      ),
      'year' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_YEAR',
        'width' => '10%',
        'default' => true,
        'name' => 'year',
      ),
      'assigned_user_id' => 
      array (
        'name' => 'assigned_user_id',
        'label' => 'LBL_ASSIGNED_TO',
        'type' => 'enum',
        'function' => 
        array (
          'name' => 'get_user_array',
          'params' => 
          array (
            0 => false,
          ),
        ),
        'default' => true,
        'width' => '10%',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
;
?>
