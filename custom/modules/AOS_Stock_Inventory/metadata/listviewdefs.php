<?php
$module_name = 'AOS_Stock_Inventory';
$listViewDefs [$module_name] = 
array (
  'WAREHOUSE' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_WAREHOUSE',
    'width' => '10%',
    'default' => true,
  ),
  'MONTH' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_MONTH',
    'width' => '10%',
    'default' => true,
  ),
  'YEAR' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_YEAR',
    'width' => '10%',
    'default' => true,
  ),
  'MAINCODE' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_MAINCODE',
    'width' => '10%',
    'default' => true,
  ),
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'UNITCODE' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_UNITCODE',
    'width' => '10%',
    'default' => true,
  ),
  'BEGINNING_BALANCE' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_BEGINNING_BALANCE',
    'width' => '10%',
    'default' => true,
  ),
  'IMPORT' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_IMPORT',
    'width' => '10%',
    'default' => true,
  ),
  'EXPORT' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_EXPORT',
    'width' => '10%',
    'default' => true,
  ),
  'ENDING_BALANCE' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ENDING_BALANCE',
    'width' => '10%',
    'default' => true,
  ),
  'PRICE' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_PRICE',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
  'TOTAL_PRICE' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_TOTAL_PRICE',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
  ),
  'CREATED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'default' => false,
  ),
  'DAY' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_DAY',
    'width' => '10%',
    'default' => false,
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => false,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => false,
  ),
);
;
?>
