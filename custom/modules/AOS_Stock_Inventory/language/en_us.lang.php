<?php

$mod_strings = array (
  'LBL_ORDER_NUMBER' => 'Order Number',
  'LBL_MAINCODE' => 'Product Code',
  'LBL_QUANTITY' => 'Quantity',
  'LBL_BEGINNING_BALANCE' => 'Beginning Balance',
  'LBL_ENDING_BALANCE' => 'Ending Balance',
  'LBL_PURCHASE_PRICE' => 'Purchase Price',
  'LBL_DAY' => 'Day',
  'LBL_MONTH' => 'Month',
  'LBL_YEAR' => 'Year',
  'LBL_PHOTO' => 'Photo',
  'LBL_WAREHOUSE' => 'Warehouse',
  'LBL_QUANTITY_IN' => 'Total Quantity In',
  'LBL_QUANTITY_OUT' => 'Total Quantity Out',
  'LBL_UNITPRICE_IN' => 'Total Price In',
  'LBL_UNITPRICE_OUT' => 'Total Price Out',
  'LBL_PRICE' => 'Average Purchase Price',
  'LBL_IN_OUT' => 'Type',
  'LBL_TOTAL_PRICE' => 'Total Price',

  'LBL_PRODUCT_NAME' => 'Product Name',
  'LBL_EDITVIEW_PANEL1' => 'Edit View Panel 1',
  'LBL_EDITVIEW_PANEL2' => 'Edit View Panel 2',
  'LBL_EDITVIEW_PANEL3' => 'Edit View Panel 3',
  'LBL_NAME' => 'Product Name',
  'LBL_IMPORT' => 'Total Quantity In',
  'LBL_EXPORT' => 'Total Quantity Out',
  'LBL_UNITCODE' => 'Unit Code',


);
