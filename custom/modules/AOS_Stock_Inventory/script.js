var tdElements = document.querySelectorAll('td[field="beginning_balance"]');

tdElements.forEach(function(td, index) {
    td.style.background = index % 2 === 0 ? 'linear-gradient(to right, #93f6a8, #ffffff)' : 'linear-gradient(to right, #ffffff, #93f6a8)';
    td.style.color = 'black';
    td.style.textAlign = 'center';
});

var tdElements2 = document.querySelectorAll('td[field="ending_balance"]');

tdElements2.forEach(function(td, index) {
    td.style.background = index % 2 === 0 ? 'linear-gradient(to right, #ffffff, #eabb0d)' : 'linear-gradient(to right, #eabb0d, #ffffff)';
    td.style.color = 'red';
    td.style.textAlign = 'center';
});

var currentURL = window.location.href;
var index = currentURL.indexOf("/index.php");
var desiredPart = currentURL.slice(0, index);

function check_Inventory() {
    var successFlag; // Khai báo biến successFlag trước khi sử dụng

    $.ajax({
        type: "POST",
        url: "index.php?entryPoint=check_Inventory",
        data: {},
        success: function(data) {
            successFlag = data;
        },
        error: function(error) {
            // Xử lý lỗi nếu cần
        },
        async: false // Đảm bảo AJAX là đồng bộ để hàm có thể trả về kết quả
    });
    return successFlag; // Trả về kết quả từ cờ
}



var tdElements3 = document.querySelectorAll('td[field="import"]');
var tdElements4 = document.querySelectorAll('td[field="export"]');

tdElements3.forEach(function(td) {
    td.style.textAlign = 'center';
    if (td.textContent.trim() !== '0') {
        var trParent = td.closest('tr');
        if (trParent) {
            var tdName = trParent.querySelector('td[field="name"]');
            var warehouse = trParent.querySelector('td[field="warehouse"]').textContent.trim();
            var month = trParent.querySelector('td[field="month"]').textContent.trim();
            var year = trParent.querySelector('td[field="year"]').textContent.trim();
            var maincode = trParent.querySelector('td[field="maincode"]').textContent.trim();
            var parts = warehouse.split('.');

            var warehouse = parts[0].trim();

            var link = tdName.querySelector('a');
            var href = link.getAttribute('href');
            var tdContent = tdName.innerHTML;
            tdName.innerHTML = tdContent;

            // Xóa tham số id
            href = href.replace(/[?&]id=[^&]*&?/, ''); // Xóa id
            href = href.replace(/[?&]id=[^&]*$/, ''); // Xóa id nếu là tham số cuối cùng

            var queryParams = 'type=In&warehouse=' + encodeURIComponent(warehouse) + '&month=' + encodeURIComponent(month) + '&year=' + encodeURIComponent(year) + '&maincode=' + encodeURIComponent(maincode);

            if (href.indexOf('?') !== -1) {
                link.href = desiredPart + "/index.php?module=AOS_Stock_Inventory_Detail&action=index&" + queryParams;
            } else {
                link.href = desiredPart + "/index.php?module=AOS_Stock_Inventory_Detail&action=index&" + queryParams;
            }

            td.textContent = td.textContent.trim();

            link.target = "_blank"; // Mở trong tab mới
            link.style.color = 'darkblue';
            link.style.textDecoration = 'none';

            link.addEventListener('mouseenter', function(event) {
                var div = event.target.querySelector('div');
                if (div) {
                    div.style.backgroundColor = 'rgb(255,119,159)';
                    div.style.transform = 'scale(1.2)';
                }
            });

            link.addEventListener('mouseleave', function(event) {
                var div = event.target.querySelector('div');
                if (div) {
                    div.style.backgroundColor = 'rgb(255,218,230)';
                    div.style.transform = 'scale(1)';
                }
            });

            link.innerHTML = '<div style="display: inline-block; padding: 5px; border-radius: 50%; background-color:rgb(255,218,230) ">' + td.textContent + '</div>';

            td.innerHTML = '';
            td.appendChild(link);
        }
    }
});

tdElements4.forEach(function(td) {
    td.style.textAlign = 'center';

    if (td.textContent.trim() !== '0') {
        var trParent = td.closest('tr');
        if (trParent) {
            var tdName = trParent.querySelector('td[field="name"]');
            var warehouse = trParent.querySelector('td[field="warehouse"]').textContent.trim();
            var month = trParent.querySelector('td[field="month"]').textContent.trim();
            var year = trParent.querySelector('td[field="year"]').textContent.trim();
            var maincode = trParent.querySelector('td[field="maincode"]').textContent.trim();
            var parts = warehouse.split('.');

            // Lấy phần tử đầu tiên của mảng (số trước dấu chấm)
            var warehouse = parts[0].trim();
            
            var link = tdName.querySelector('a'); 
            var href = link.getAttribute('href'); 
            var tdContent = tdName.innerHTML;
            tdName.innerHTML = tdContent;

            // Xóa tham số id
            href = href.replace(/[?&]id=[^&]*&?/, ''); // Xóa id
            href = href.replace(/[?&]id=[^&]*$/, ''); // Xóa id nếu là tham số cuối cùng

            var queryParams = 'type=Out&warehouse=' + encodeURIComponent(warehouse) + '&month=' + encodeURIComponent(month) + '&year=' + encodeURIComponent(year) + '&maincode=' + encodeURIComponent(maincode);

            if (href.indexOf('?') !== -1) {
                link.href = desiredPart + "/index.php?module=AOS_Stock_Inventory_Detail&action=index&" + queryParams;
            } else {
                link.href = desiredPart + "/index.php?module=AOS_Stock_Inventory_Detail&action=index&" + queryParams;
            }

            // Trả lại giá trị cho các ô <td>
            td.textContent = td.textContent.trim();

            link.target = "_blank"; // Mở trong tab mới
            link.style.color = 'darkblue';
            link.style.textDecoration = 'none';

            link.addEventListener('mouseenter', function(event) {
                var div = link.querySelector('div');
                if (div) {
                    div.style.backgroundColor = 'rgb(255,119,159)';
                    div.style.transform = 'scale(1.2)';
                }
            });

            link.addEventListener('mouseleave', function(event) {
                var div = link.querySelector('div');
                if (div) {
                    div.style.backgroundColor = 'rgb(255,218,230)';
                    div.style.transform = 'scale(1)';
                }
            });

            link.innerHTML = '<div style="color:red; display: inline-block; padding: 5px; border-radius: 50%; background-color:rgb(255,218,230) ">' + td.textContent + '</div>';
            td.innerHTML = '';
            td.appendChild(link);
        }
    }
});



var tdElements5 = document.querySelectorAll('td[field="maincode"]');
tdElements5.forEach(function(td) {
    var pino = td.textContent.trim();
    console.log(pino);
    $.ajax({
        type: "POST",
        url: "index.php?entryPoint=pino_name_product",
        data: {
            pino: pino,
        },
        success: function(data) {

            // Tạo một thẻ a mới
            var link = document.createElement('a');
            // Đặt thuộc tính href cho thẻ a
            var desiredPart3 = desiredPart + "/index.php?module=AOS_Products&action=DetailView&record=" + data;
            link.setAttribute('href', desiredPart3);
            // Đặt nội dung cho thẻ a là mã sản phẩm
            link.textContent = pino;

            // Tạo thẻ <b> mới
            var boldElement = document.createElement('b');
            // Chèn thẻ <a> vào trong thẻ <b>
            boldElement.appendChild(link);

            // Xóa nội dung hiện tại của ô td
            td.innerHTML = ''; 
            // Chèn thẻ <b> vào ô td
            td.appendChild(boldElement); 
        },
        error: function(error) {
            // Xử lý lỗi nếu cần
            console.error("Error:", error);
        }
    });
});


var tdElements = document.querySelectorAll('td[type="name"][field="name"].inlineEdit');
tdElements.forEach(function(tdElement) {
    // Lấy tất cả các phần tử a trong td đó
    var aElements = tdElement.querySelectorAll('a');
    
    // Lặp qua từng phần tử a
    aElements.forEach(function(aElement) {
        // Tạo một phần tử p mới
        var pElement = document.createElement('p');
        // Sao chép nội dung của phần tử a vào phần tử p
        pElement.innerHTML = aElement.innerHTML;
        // Thay thế phần tử a bằng phần tử p
        aElement.parentNode.replaceChild(pElement, aElement);
    });
});
