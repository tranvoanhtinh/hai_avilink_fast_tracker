



<div class="row" style="display: flex; align-items: flex-end;">
    <div class="col-sm-2">
        <form id="columnForm" action="" method="post">
            <div class="form-group">
                <label for="columnsSelect">{$mod_strings.LBL_SELECT_COLUMNS}</label>
                <select name="selected_columns" class="form-control" id="columnsSelect">
                    <option value="6" {if $columns_per_row == '6'} selected{/if}>{$mod_strings.LBL_2_COLUMNS}</option>
                    <option value="4" {if $columns_per_row == '4'} selected{/if}>{$mod_strings.LBL_3_COLUMNS}</option>
                    <option value="3" {if $columns_per_row == '3'} selected{/if}>{$mod_strings.LBL_4_COLUMNS}</option>
                </select>
            </div>
        </form>
    </div>
    <div class="col-sm-8">
<form class="form-horizontal" id="searchForm" action="" method="post">
    <div class="row">
        <div class="col-sm-8">
            <div class="form-group">
                <label class="col-sm-2 control-label" for="searchInput">&nbsp</label>
                <div class="col-sm-10">
                    <input type="text" id="searchInput" name="searchInput" class="form-control" value="{$smarty.session.like|escape}" placeholder="Search ...">


                </div>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <input class="btn btn-primary" type="submit" id="search_from" name ="search_from" value="Tìm kiếm">
                </div>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <input class="btn btn-primary" type="submit" id="clear_search" name ="clear_search" value="Xóa tìm kiếm">
                </div>
            </div>
        </div>
    </div>
</form>

    </div>
</div>


<table border="0" cellpadding="0" cellspacing="0" width="100%" class="paginationTable" style="background-color: #bfcad3;">
    <tbody>
    <tr>

    <td nowrap="nowrap" class="paginationActionButtons">
    <ul  class="clickMenu selectmenu SugarActionMenu columnsFilterLink listViewLinkButton listViewLinkButton_top" >
    <li class="sugar_action_button desktopOnly">
        <a  class="glyphicon glyphicon-list-alt clearSearchIcon parent-dropdown-handler" onclick=""></a>

    </li>
    </ul>
    </td>
        <td nowrap="nowrap" class="paginationActionButtons">

        </td>
        <td nowrap="nowrap" align="right" class="paginationChangeButtons" width="1%">
            <button type="button" id="listViewStartButton_top" name="listViewStartButton" title="Bắt đầu" class="list-view-pagination-button">
                <span class="suitepicon suitepicon-action-first"></span>
            </button>
            <button type="button" id="listViewPrevButton_top" name="listViewPrevButton" class="list-view-pagination-button" title="Trước">
                <span class="suitepicon suitepicon-action-left"></span>
            </button>
        </td>
        <td nowrap="nowrap" width="1%" class="paginationActionButtons">
            <div class="pageNumbers">
                (<span id="startProductValue_top"l>{$pages.startproduct}</span> - <span id="endProductValue_top">{$pages.endproduct}</span> of <span id="totalProductValue_top">{$pages.totalproduct}</span>)
            </div>
        </td>

        <td nowrap="nowrap" align="right" class="paginationActionButtons" width="1%">
            <button  id="listViewNextButton_top" name="listViewNextButton" class="list-view-pagination-button" title="Tiếp theo">
                <span class="suitepicon suitepicon-action-right"></span>
            </button>
            <button type="button" id="listViewEndButton_top" name="listViewEndButton" title="Kết thúc" class="list-view-pagination-button">
                <span class="suitepicon suitepicon-action-last"></span>
            </button>
        </td>
        <td nowrap="nowrap" width="4px" class="paginationActionButtons"></td>
    </tr>
    </tbody>
</table>

<form id="paginationForm" action="" method="post">
    <input type="hidden" name="currentpage" value="{$pages.currentpage}">
</form>

<div class="row">
    {foreach from=$products item=product}
    <div class="col-sm-{$columns_per_row} mb-3">
        <div class="panel panel-default">
        <div class="panel-heading" style="height:22px; display: flex; align-items: center; justify-content: left; background: linear-gradient(to right, rgb(96, 102, 156), rgba(88, 191, 175, 0.82)); padding: 0px; border-radius: 15px 15px 0px 0px;">
                      <h3 class="panel-title" style="margin-left: 10px; color: white; white-space: nowrap;">{$product.part_number}</h3>
            </div>
          
            <div class="panel-body">
              <a href="index.php?module=AOS_Products&action=DetailView&record={$product.id}">
                <div class="div-image" style="width: 85%; height: {$height}px; margin: auto; display: flex; align-items: center; justify-content: center;">
                    {if isset($product.image_url) && $product.image_url != ''}
                    <img src="upload/{$product.image_url}" alt="..." style="height: 90%;">
                    {/if}
                </div>
                </a>
               <!-- <p>{$product.description}</p> -->
               <div class="content">
             


                <div style="height: 180px;">
                  <p class="text-bold">{$product.name}</p>


                    {if $product.sizes}
                    <table class="table table-bordered size-product table-striped" style="width: 100%; margin: 0 auto;">
                
                        <tr>
                            <th>L</th>
                            <th>W</th>
                            <th>H</th>
                        </tr>
           
                        {assign var="counter" value=0}
                        {foreach from=$product.sizes item=size}
                        {if $counter < 4}
                        <tr>
                            <td style="padding: 4px;">{$size.size_l_cm}</td>
                            <td style="padding: 4px;">{$size.size_w_cm}</td>
                            <td style="padding: 4px;">{$size.size_h_cm}</td>
                        </tr>
                        {/if}
                        {assign var="counter" value=$counter+1}
                        {/foreach}


                    </table>

                    {else}
            

                        <p class="text-description">{$product.description}</p>
                    {/if}

                </div>
                </div>
                           &nbsp
            </div>

        </div>

    </div>
    {/foreach}

</div>


<table border="0" cellpadding="0" cellspacing="0" width="100%" class="paginationTable" style="background-color: #bfcad3;">
    <tbody>
    <tr>
        <td nowrap="nowrap" class="paginationActionButtons">

        </td>
        <td nowrap="nowrap" align="right" class="paginationChangeButtons" width="1%">
            <button type="button" id="listViewStartButton_bot" name="listViewStartButton" title="Bắt đầu" class="list-view-pagination-button">
                <span class="suitepicon suitepicon-action-first"></span>
            </button>
            <button type="button" id="listViewPrevButton_bot" name="listViewPrevButton" class="list-view-pagination-button" title="Trước">
                <span class="suitepicon suitepicon-action-left"></span>
            </button>
        </td>
        <td nowrap="nowrap" width="1%" class="paginationActionButtons">
            <div class="pageNumbers">
                (<span id="startProductValue_bot"l>{$pages.startproduct}</span> - <span id="endProductValue_bot">{$pages.endproduct}</span> of <span id="totalProductValue_bot">{$pages.totalproduct}</span>)
            </div>
        </td>

        <td nowrap="nowrap" align="right" class="paginationActionButtons" width="1%">
            <button  id="listViewNextButton_bot" name="listViewNextButton" class="list-view-pagination-button" title="Tiếp theo">
                <span class="suitepicon suitepicon-action-right"></span>
            </button>
            <button type="button" id="listViewEndButton_bot" name="listViewEndButton" title="Kết thúc" class="list-view-pagination-button">
                <span class="suitepicon suitepicon-action-last"></span>
            </button>
        </td>
        <td nowrap="nowrap" width="4px" class="paginationActionButtons"></td>
    </tr>
    </tbody>
</table>
