
    document.getElementById('columnsSelect').addEventListener('change', function() {
        // Gửi form khi có thay đổi
        document.getElementById('columnForm').submit();
    });



window.addEventListener('load', function() {
    var images = document.querySelectorAll('.div-image img');

    images.forEach(function(img) {
        img.onload = function() {
            var width = img.width;
            var parentWidth = img.parentElement.offsetWidth;
            if (width > parentWidth) {
                img.style.width = '100%';
                img.style.height = 'auto';
            }
        }
    });
});


// Function to add event listeners to pagination buttons with a specific suffix
function addPaginationEventListeners(suffix) {
    var nextButton = document.getElementById('listViewNextButton_' + suffix);
    var prevButton = document.getElementById('listViewPrevButton_' + suffix);
    var startButton = document.getElementById('listViewStartButton_' + suffix);
    var endButton = document.getElementById('listViewEndButton_' + suffix);

    // Thêm sự kiện 'click' vào nút "Tiếp theo"
    nextButton.addEventListener('click', function() {
        updateCurrentPage(suffix, 'next');
    });

    // Thêm sự kiện 'click' vào nút "Trước"
    prevButton.addEventListener('click', function() {
        updateCurrentPage(suffix, 'prev');
    });

    // Thêm sự kiện 'click' vào nút "Bắt đầu"
    startButton.addEventListener('click', function() {
        updateCurrentPage(suffix, 'start');
    });

    // Thêm sự kiện 'click' vào nút "Kết thúc"
    endButton.addEventListener('click', function() {
        updateCurrentPage(suffix, 'end');
    });

    // Kiểm tra và vô hiệu hóa nút "Bắt đầu" và "Trước" nếu startProductValue là 1
    var startProductValue = document.getElementById('startProductValue_' + suffix).innerText;
    var isStartProductOne = startProductValue === '1';
    startButton.disabled = isStartProductOne;
    prevButton.disabled = isStartProductOne;

    // Kiểm tra và vô hiệu hóa nút "Tiếp theo" và "Kết thúc" nếu endProductValue bằng totalProductValue
    var endProductValue = document.getElementById('endProductValue_' + suffix).innerText;
    var totalProductValue = document.getElementById('totalProductValue_' + suffix).innerText;
    var isEndProductSameAsTotal = endProductValue === totalProductValue;
    nextButton.disabled = isEndProductSameAsTotal;
    endButton.disabled = isEndProductSameAsTotal;
}

// Function to update the current page value and submit the form
function updateCurrentPage(suffix, action) {
    var currentPageInput = document.querySelector("input[name='currentpage']");
    var currentPage = parseInt(currentPageInput.value);
    
    switch (action) {
        case 'next':
            currentPage++;
            break;
        case 'prev':
            currentPage--;
            break;
        case 'start':
            currentPage = 1;
            break;
        case 'end':
            currentPage = 'end';
            break;
        default:
            break;
    }

    currentPageInput.value = currentPage;
    document.getElementById('paginationForm').submit();
}

// Thêm sự kiện cho các nút phân trang dưới với id khác nhau (_bot)
addPaginationEventListeners('bot');
addPaginationEventListeners('top');