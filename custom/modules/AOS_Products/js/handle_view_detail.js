
var labelDivs = document.querySelectorAll('.label');

// Lặp qua từng thẻ div
labelDivs.forEach(function (labelDiv) {
    if (
        labelDiv.textContent.trim() === 'Kích thước:' ||
        labelDiv.textContent.trim() === 'Size:'
    ) {
        // Ẩn thẻ div
        labelDiv.style.display = 'none';
    }
});




//-------------------------------------------------------------------------
var productImage = document.querySelector('div[field="product_image"]');
// Kiểm tra xem phần tử có tồn tại không
if (productImage) {
    productImage.querySelector("img").style.width = "500px";
    productImage.querySelector("img").style.height = "auto";
    // Lấy phần tử cha của hình ảnh
    var parentDiv = productImage.closest('.col-xs-12.col-sm-6.detail-view-row-item');

    // Kiểm tra xem phần tử cha có tồn tại không
    if (parentDiv) {
        // Loại bỏ lớp hiện tại và thêm lớp mới
        parentDiv.classList.remove("col-sm-6");
        parentDiv.classList.add("col-sm-8");
        // Tìm phần tử div có class là "col-1-label" và chỉnh sửa class thành "col-sm-3"
        var labelDiv = parentDiv.querySelector('.col-1-label');
        if (labelDiv) {
            labelDiv.classList.remove("col-sm-4");
            labelDiv.classList.add("col-sm-3");
        }
    }

    var labelDiv2 = productImage.closest('.col-xs-12.col-sm-8.detail-view-field');
    if (labelDiv2) {
        // Loại bỏ lớp hiện tại và thêm lớp mới
        labelDiv2.classList.remove("col-sm-8");
        labelDiv2.classList.add("col-sm-9");
    }
}




var lineItemsDiv = document.querySelector('div[field="line_items"]');
if (lineItemsDiv) {
    // Lấy phần tử cha của hình ảnh
    var parentDiv2 = lineItemsDiv.closest('.col-xs-12.col-sm-6.detail-view-row-item');

    // Kiểm tra xem phần tử cha có tồn tại không
    if (parentDiv2) {
        // Loại bỏ lớp hiện tại và thêm lớp mới
        parentDiv2.classList.remove("col-sm-6");
        parentDiv2.classList.add("col-sm-4");
    }
}


var extraImage = document.querySelector('div[field="extra_image"]');
// Kiểm tra xem phần tử có tồn tại không
if (extraImage) {

    // Lấy phần tử cha của hình ảnh
    var parentDiv = extraImage.closest('.col-xs-12.col-sm-6.detail-view-row-item');

    // Kiểm tra xem phần tử cha có tồn tại không
    if (parentDiv) {
        // Loại bỏ lớp hiện tại và thêm lớp mới
        parentDiv.classList.remove("col-sm-6");
        parentDiv.classList.add("col-sm-8");
        var labelDiv = parentDiv.querySelector('.col-1-label');
        if (labelDiv) {
            labelDiv.classList.remove("col-sm-4");
            labelDiv.classList.add("col-sm-3");
        }
    }

    var labelDiv2 = extraImage.closest('.col-xs-12.col-sm-8.detail-view-field');
    if (labelDiv2) {
        // Loại bỏ lớp hiện tại và thêm lớp mới
        labelDiv2.classList.remove("col-sm-8");
        labelDiv2.classList.add("col-sm-9");
    }
}


//--------------------------------------------------------------------------

var container = document.getElementById('product_image');
var image = container.querySelector('img'); // Lấy phần tử đầu tiên

var extraImages = document.querySelectorAll('.div_extra_img .image'); // Chọn tất cả các thẻ img trong .div_extra_img

extraImages.forEach(function(extraImage) {
    extraImage.addEventListener('click', function() {
        var currentImageUrl = image.src; // Lấy địa chỉ URL của hình ảnh hiện tại

        // Tìm vị trí cuối cùng của dấu /
        var lastSlashIndex = currentImageUrl.lastIndexOf('/');

        // Lấy phần cuối của địa chỉ URL bằng cách cắt chuỗi từ vị trí của dấu /
        var fileName = currentImageUrl.substring(lastSlashIndex + 1);

        // Tạo đường dẫn mới bằng cách kết hợp 'upload/' với phần cuối của địa chỉ URL
        var newImageUrl = 'upload/' + fileName;

        var clickedImageUrl = this.src; // Lấy địa chỉ URL của hình ảnh được nhấp vào

        // Thay đổi địa chỉ ảnh của thẻ img đầu tiên thành địa chỉ của ảnh được nhấp vào
        image.src = clickedImageUrl;

        // Thay đổi địa chỉ ảnh của ảnh được nhấp vào thành địa chỉ của hình ảnh hiện tại
        this.src = newImageUrl;
    });
});



image.addEventListener('click', function() {
    var overlay = document.querySelector('.zoom-overlay');
    if (!overlay) {
        overlay = document.createElement('div');
        overlay.classList.add('zoom-overlay');
        
        var divOutsideImage = document.createElement('div');
        divOutsideImage.classList.add('overlay-container'); // Thêm lớp cho thẻ div bên ngoài hình
        overlay.appendChild(divOutsideImage); // Thêm thẻ div vào overlay
        
        var imgInsideDiv = document.createElement('img');
        imgInsideDiv.classList.add('zoomed-image');
        divOutsideImage.appendChild(imgInsideDiv); // Thêm hình ảnh vào trong thẻ div
        
        var closeButton = document.createElement('span');
        closeButton.classList.add('close-btn');
        closeButton.textContent = 'x';
        divOutsideImage.appendChild(closeButton); // Thêm nút đóng vào trong thẻ div
        
        closeButton.addEventListener('click', function() {
            overlay.style.display = "none";
        });
        
        document.body.appendChild(overlay);
    }

    var zoomedImage = overlay.querySelector('.zoomed-image');
    zoomedImage.src = this.src;
    overlay.style.display = "block";

    // Tính toán kích thước mới cho overlay container
    var imageWidth = this.width;
    var imageHeight = this.height;
    var maxAllowedHeight = 700; // Chiều cao tối đa cho hình ảnh
    var scaleFactor = Math.min(2, maxAllowedHeight / imageHeight); // Tính tỷ lệ scale, giới hạn tối đa là 1.5
    var newHeight = Math.min(maxAllowedHeight, imageHeight * scaleFactor); // Tính toán chiều cao mới, giới hạn tối đa là 700px
    var newWidth = imageWidth * scaleFactor; // Tính toán chiều rộng mới dựa trên tỷ lệ

    // Cập nhật kích thước của overlay container
    var overlayContainer = overlay.querySelector('.overlay-container');
    overlayContainer.style.width = newWidth + 'px';
    overlayContainer.style.height = newHeight + 'px';
});


var elements = document.querySelectorAll('.row.detail-view-row');
elements.forEach(function(element) {
    element.style.backgroundColor = '#f5f5f5';
});

//-------PHẦN chỉnh sửa giao diện View Edit -------------------------
var part_number = document.querySelector('div[field="part_number"]');
// Kiểm tra xem phần tử có tồn tại không
if (part_number) {

    // Lấy phần tử cha của hình ảnh
    var parentDiv = part_number.closest('.col-xs-12.col-sm-6.detail-view-row-item');

    // Kiểm tra xem phần tử cha có tồn tại không
    if (parentDiv) {
        // Loại bỏ lớp hiện tại và thêm lớp mới
        parentDiv.classList.remove("col-sm-6");
        parentDiv.classList.add("col-sm-8");
        var labelDiv = parentDiv.querySelector('.col-1-label');
        if (labelDiv) {
            labelDiv.classList.remove("col-sm-4");
            labelDiv.classList.add("col-sm-3");
        }
    }

    var labelDiv2 = part_number.closest('.col-xs-12.col-sm-8.detail-view-field');
    if (labelDiv2) {
        // Loại bỏ lớp hiện tại và thêm lớp mới
        labelDiv2.classList.remove("col-sm-8");
        labelDiv2.classList.add("col-sm-9");
    }
}
if(part_number) {
    // Thu nhỏ kích thước của phần tử part_number xuống 50%
    part_number.style.width = '30%';

   }


// var targetElement = $('div.col-sm-4[data-label="LBL_PART_NUMBER"]');
// targetElement.removeClass('col-sm-4').addClass('col-sm-3');

var lineItemsDiv = document.querySelector('div[field="aos_product_category_name"]');
if (lineItemsDiv) {
    // Lấy phần tử cha của hình ảnh
    var parentDiv2 = lineItemsDiv.closest('.col-xs-12.col-sm-6.detail-view-row-item');

    // Kiểm tra xem phần tử cha có tồn tại không
    if (parentDiv2) {
        // Loại bỏ lớp hiện tại và thêm lớp mới
        parentDiv2.classList.remove("col-sm-6");
        parentDiv2.classList.add("col-sm-4");
    }
}
// Chọn tất cả các phần tử có class là "col-xs-12 col-sm-4 label col-2-label"
var elements = document.getElementsByClassName('col-xs-12 col-sm-4 label col-2-label');

// Lặp qua từng phần tử và áp dụng thuộc tính padding-left: 5px;
for (var i = 0; i < 1; i++) {
    elements[i].style.paddingLeft = '7px';
}
