
  var tdElements = document.querySelectorAll('td[field="product_image"]');

  // Duyệt qua từng thẻ td và thực hiện thay thế nội dung
  tdElements.forEach(function(tdElement) {
    var tdContent = tdElement.innerText.trim();

    var imgElement = document.createElement('img');
    imgElement.src = 'upload/' + tdContent;
    
    // Thêm thuộc tính width
    imgElement.setAttribute('width', '128px');

    tdElement.innerHTML = '';
    tdElement.appendChild(imgElement);
  });




    var tdElements = document.querySelectorAll('td[field="name"], td[field="line_items"]');
    var id;

    // Iterate over each <td> element
    tdElements.forEach(function(tdElement) {
        // Check if the <td> has the attribute field="name"
        if (tdElement.getAttribute('field') === 'name') {
            var aElement = tdElement.querySelector('a');
            if (aElement) {
                var hrefValue = aElement.getAttribute('href');
                var regex = /record=([a-f\d-]+)/i;
                var match = regex.exec(hrefValue);
                id = match ? match[1] : null;
            }
        } else if (tdElement.getAttribute('field') === 'line_items') {
            var currentURL = window.location.href;
            var index = currentURL.indexOf("/index.php");
            var desiredPart = currentURL.slice(0, index);
            var newURL = desiredPart + "/index.php?entryPoint=Aos_product_listview_sizes";

            var xhr = new XMLHttpRequest();

            // Set up the request
            xhr.open('POST', newURL, true);
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

            // Set the callback function to handle the response
            xhr.onreadystatechange = function() {
                if (xhr.readyState === 4 && xhr.status === 200) {
                    // Handle the response data here
                    var sizes = JSON.parse(xhr.responseText);

                var length = (sizes.length > 3) ? 4 : sizes.length;

                var tableHtml = '<table width="100%" style="border-collapse: collapse; border: 1px solid #ffffff;">';

                // Thêm hàng đầu tiên với thẻ <th>
  
                for (var i = 0; i < length; i++) {
                    var dynamicVarNameL = 'sizeL' + i;
                    var dynamicVarNameW = 'sizeW' + i;
                    var dynamicVarNameH = 'sizeH' + i;

                    var sizeL = sizes[i].size_l;
                    var sizeW = sizes[i].size_w;
                    var sizeH = sizes[i].size_h;

                    tableHtml += `
                        <tr>
                            <td style="border: 1px solid #ffffff; font-size: 12px; background-color: rgba(118, 168, 162, 0.21);">${sizeL}</td>
                            <td style="border: 1px solid #ffffff; font-size: 12px; background-color: rgba(118, 168, 162, 0.21);">${sizeW}</td>
                            <td style="border: 1px solid #ffffff; font-size: 12px; background-color: rgba(118, 168, 162, 0.21);">${sizeH}</td>
                        </tr>
                    `;
                }

                tableHtml += '</table>';

                    tdElement.innerHTML = tableHtml;
                }
            };
            var requestBody = 'id=' + encodeURIComponent(id);
            xhr.send(requestBody);
        }
    });

  var images = document.querySelectorAll('.enlargeable');
  images.forEach(function(img) {
    img.addEventListener('click', function() {
      var enlargedContainer = document.getElementById('enlarged-image-container');
      var enlargedImage = document.getElementById('enlarged-image');
      var src = this.getAttribute('data-src');
      enlargedImage.src = src;
      enlargedContainer.style.display = 'block';
    });


  // Close the enlarged image when clicking on the close button
  document.querySelector('.close').addEventListener('click', function() {
    document.getElementById('enlarged-image-container').style.display = 'none';
  });
});





// Chọn bảng có class là "list view table-responsive"
var table = document.querySelector('table.list.view.table-responsive');

if (table) {
  // Tìm tất cả các phần tử <tr> trong bảng
  var trElements = table.querySelectorAll('tr');

  // Duyệt qua từng phần tử <tr>
  trElements.forEach(function(trElement) {
    // Lấy phần tử <td> đầu tiên trong hàng hiện tại
    var firstTd = trElement.querySelector('td');

    // Kiểm tra xem phần tử <td> đầu tiên có tồn tại không
    if (firstTd) {
      // Lấy các phần tử <td> cụ thể trong hàng hiện tại
      var tdName = trElement.querySelector('td[field="name"]');
      var tdProductImage = trElement.querySelector('td[field="product_image"]');
      var tdLineItems = trElement.querySelector('td[field="line_items"]');

      
      // Nếu tất cả các phần tử <td> cụ thể được tìm thấy
      if (tdName && tdProductImage && tdLineItems) {
        // Lấy phần tử <a> trong phần tử <td> có field="name"
        var aElement = tdName.querySelector('a');

        // Nếu tìm thấy phần tử <a>
        if (aElement) {
          // Lấy giá trị href của phần tử <a>
          var hrefValue = aElement.getAttribute('href');
          
          // Thêm sự kiện click vào các phần tử <td> cụ thể
          [tdName, tdProductImage, tdLineItems].forEach(function(td) {
            td.addEventListener('click', function() {
              // Chuyển hướng trang tới địa chỉ href của phần tử <a>
              window.location.href = hrefValue;
            });
          });
        }
      }
    }
  });
}




function duplicateMenuItem(menuItemId, i) {
    if (!document.getElementById(menuItemId)) {
        var liElements = document.querySelectorAll('li.actionmenulinks');
        
        // Kiểm tra xem có đủ phần tử không
        if (i < liElements.length) {
            var liElement = liElements[i];
            var clonedLiElement = liElement.cloneNode(true);
            clonedLiElement.id = menuItemId;

            var ulElement = document.querySelector('ul.nav.nav-tabs');
            ulElement.appendChild(clonedLiElement);

            // Lấy sideBarIcon và actionMenuLink trong clonedLiElement
            var sideBarIcon = clonedLiElement.querySelector('.side-bar-action-icon');
            var actionMenuLink = clonedLiElement.querySelector('.actionmenulink');

            if (sideBarIcon) {
                sideBarIcon.style.display = 'inline-block';
            }

            if (actionMenuLink) {
                actionMenuLink.style.display = 'inline-block';
            }
        } 
    }
}

// duplicateMenuItem('create_viewcard', 2); // Lấy thẻ thứ hai