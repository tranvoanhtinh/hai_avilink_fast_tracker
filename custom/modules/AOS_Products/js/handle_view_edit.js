//-----------PHẦN TÍNH PACKING ---------------
// Khai báo biến toàn cục
const global_size_product_cont = 2.54;
// Hàm tạo và cập nhật các phần tử
function updateElements() {
    // Lấy lại tất cả các phần tử input có thuộc tính `name` bắt đầu với "cbm_name"
    var bcm_elements = document.querySelectorAll('input[name^="cbm_name"]');
    size_cm_elements = [];
    size_inch_elements = [];

    // Duyệt qua mảng `bcm_elements` và tạo các tập hợp các phần tử CM và Inch tương ứng với mỗi phần tử CBM
    for (var i = 0; i < bcm_elements.length; i++) {
        var rowNumber = bcm_elements[i].name.match(/\d+/)[0];
        var cmElementSet = [
            document.getElementById("packing_l_cm_name" + rowNumber),
            document.getElementById("packing_w_cm_name" + rowNumber),
            document.getElementById("packing_h_cm_name" + rowNumber)
        ];

        var inchElementSet = [
            document.getElementById("packing_l_inch_name" + rowNumber),
            document.getElementById("packing_w_inch_name" + rowNumber),
            document.getElementById("packing_h_inch_name" + rowNumber)
        ];

        size_cm_elements.push(cmElementSet);
        size_inch_elements.push(inchElementSet);
    }
}


// Hàm tính toán lại các phần tử CM, Inch và giá trị CBM
function handleSize(i) {
    var cmElementSet = size_cm_elements[i];
    var inchElementSet = size_inch_elements[i];
    var cbmValue = 1;

    for (var j = 0; j < cmElementSet.length; j++) {
        var cmValue = parseFloat(cmElementSet[j].value);
        if (isNaN(cmValue) || cmElementSet[j].value.trim() === "") {
            cmValue = ''; // Nếu giá trị không phải số hoặc là rỗng, đặt giá trị của CM là rỗng
        } else {
            cmValue = cmValue.toFixed(1); // Làm tròn giá trị CM đến một chữ số thập phân
        }

        cmElementSet[j].value = cmValue;

        // Tính toán và hiển thị giá trị Inch với đúng định dạng số thập phân
        var inchValue = (parseFloat(cmValue) / global_size_product_cont).toFixed(2);
        inchElementSet[j].value = isNaN(inchValue) ? '' : inchValue;

        // Tính toán giá trị CBM
        cbmValue *= parseFloat(cmValue);
    }

    cbmValue /= 1000000;
    // Kiểm tra nếu cbmValue là NaN, thiết lập giá trị là rỗng
    document.getElementById("cbm_name" + i).value = isNaN(cbmValue) ? '' : cbmValue.toFixed(2);
}


// Hàm tính toán lại các phần tử CM và Inch sau khi thêm dòng mới và gắn lại sự kiện blur cho các phần tử CM mới
function recalculateElements() {
    updateElements(); // Cập nhật lại các phần tử sau khi thêm dòng mới
    for (var i = 0; i < size_cm_elements.length; i++) {
        for (var j = 0; j < size_cm_elements[i].length; j++) {
            size_cm_elements[i][j].addEventListener("blur", function () {
                var rowNumber = this.id.match(/\d+/)[0];
                handleSize(rowNumber);
            });
        }
    }
}

// Gắn sự kiện click cho nút "Tạo packing size"
document.getElementById("packing_group0addpackingLine").addEventListener("click", function() {
    updateElements();
    recalculateElements(); // Tính toán lại các phần tử sau khi thêm dòng mới
});
window.addEventListener("load", function() {
    recalculateElements(); // Tính toán lại các phần tử khi trang được tải
});




function disableSaveButton() {
    var x = SUGAR.language.get(module_sugar_grp1, 'LBL_SAVE');
    var saveInputs = document.querySelectorAll('input[value="' + x + '"]');
    
    saveInputs.forEach(function(input) {
        input.disabled = true;
        input.style.setProperty('padding-top', '5px');
        input.style.setProperty('padding-bottom', '26px');
        input.style.setProperty('margin-top', '-10px');
    });
}

// Hàm kích hoạt lại nút "Lưu"
function enableSaveButton() {
    var x = SUGAR.language.get(module_sugar_grp1, 'LBL_SAVE');
    var saveInputs = document.querySelectorAll('input[value="' + x + '"]');
    
    saveInputs.forEach(function(input) {
        input.disabled = false;
        input.style.setProperty('padding-top', '0px');
        input.style.setProperty('padding-bottom', '0px');
        input.style.setProperty('margin-top', '0px');
    });
}

document.getElementById("part_number").addEventListener("focus", function() {
    this.style.color = "black"; // Đặt màu chữ là đen khi con trỏ được đặt vào
   
});

//-----------END PHẦN  Bắt lỗi trùng mã sản phẩm  (part_number)

//-------PHẦN chỉnh sửa giao diện View Edit -------------------------

var part_number = document.querySelector('div[field="part_number"]');
// Kiểm tra xem phần tử có tồn tại không
if (part_number) {

    // Lấy phần tử cha của hình ảnh
    var parentDiv = part_number.closest('.col-xs-12.col-sm-6.edit-view-row-item');

    // Kiểm tra xem phần tử cha có tồn tại không
    if (parentDiv) {
        // Loại bỏ lớp hiện tại và thêm lớp mới
        parentDiv.classList.remove("col-sm-6");
        parentDiv.classList.add("col-sm-8");
    }

    var labelDiv2 = part_number.closest('.col-xs-12.col-sm-8.edit-view-field');
    if (labelDiv2) {
        // Loại bỏ lớp hiện tại và thêm lớp mới
        labelDiv2.classList.remove("col-sm-8");
        labelDiv2.classList.add("col-sm-9");
    }
}
if(part_number) {
    part_number.style.width = '45%';
}


var targetElement = $('div.col-sm-4[data-label="LBL_PART_NUMBER"]');
targetElement.removeClass('col-sm-4').addClass('col-sm-3');

var lineItemsDiv = document.querySelector('div[field="aos_product_category_name"]');
if (lineItemsDiv) {
    // Lấy phần tử cha của hình ảnh
    var parentDiv2 = lineItemsDiv.closest('.col-xs-12.col-sm-6.edit-view-row-item');

    // Kiểm tra xem phần tử cha có tồn tại không
    if (parentDiv2) {
        // Loại bỏ lớp hiện tại và thêm lớp mới
        parentDiv2.classList.remove("col-sm-6");
        parentDiv2.classList.add("col-sm-4");
    }
}


var productImage = document.querySelector('div[field="product_image"]');
// Kiểm tra xem phần tử có tồn tại không
if (productImage) {
    productImage.querySelector("img").style.width = "500px";
    productImage.querySelector("img").style.height = "auto";
    // Lấy phần tử cha của hình ảnh
    var parentDiv = productImage.closest('.col-xs-12.col-sm-6.edit-view-row-item');

    // Kiểm tra xem phần tử cha có tồn tại không
    if (parentDiv) {
        // Loại bỏ lớp hiện tại và thêm lớp mới
        parentDiv.classList.remove("col-sm-6");
        parentDiv.classList.add("col-sm-8");
    }

    var labelDiv2 = productImage.closest('.col-xs-12.col-sm-8.edit-view-field');
    if (labelDiv2) {
        // Loại bỏ lớp hiện tại và thêm lớp mới
        labelDiv2.classList.remove("col-sm-8");
        labelDiv2.classList.add("col-sm-9");
    }
}
var targetElement = $('div.col-sm-4[data-label="LBL_PRODUCT_IMAGE"]');
targetElement.removeClass('col-sm-4').addClass('col-sm-3');


var lineItemsDiv = document.querySelector('div[field="line_items"]');
if (lineItemsDiv) {
    // Lấy phần tử cha của hình ảnh
    var parentDiv2 = lineItemsDiv.closest('.col-xs-12.col-sm-6.edit-view-row-item');

    // Kiểm tra xem phần tử cha có tồn tại không
    if (parentDiv2) {
        // Loại bỏ lớp hiện tại và thêm lớp mới
        parentDiv2.classList.remove("col-sm-6");
        parentDiv2.classList.add("col-sm-4");
    }
}


var extraImage = document.querySelector('div[field="extra_image"]');
// Kiểm tra xem phần tử có tồn tại không
if (extraImage) {

    // Lấy phần tử cha của hình ảnh
    var parentDiv = extraImage.closest('.col-xs-12.col-sm-6.edit-view-row-item');

    // Kiểm tra xem phần tử cha có tồn tại không
    if (parentDiv) {
        // Loại bỏ lớp hiện tại và thêm lớp mới
        parentDiv.classList.remove("col-sm-6");
        parentDiv.classList.add("col-sm-8");
    }

    var labelDiv2 = extraImage.closest('.col-xs-12.col-sm-8.edit-view-field');
    if (labelDiv2) {
        // Loại bỏ lớp hiện tại và thêm lớp mới
        labelDiv2.classList.remove("col-sm-8");
        labelDiv2.classList.add("col-sm-9");
    }
}
var targetElement = $('div.col-sm-4[data-label="LBL_EXTRA_IMAGE"]');
targetElement.removeClass('col-sm-4').addClass('col-sm-3');

// Lặp qua tất cả các phần tử có class là 'edit-view-field'
var elements = document.querySelectorAll('.edit-view-field');
elements.forEach(function(element) {
    // Kiểm tra nếu phần tử có thuộc tính field có giá trị là 'aos_product_category_name'
    if (element.getAttribute('field') === 'aos_product_category_name') {
        // Thực hiện thay đổi lớp CSS của phần tử
        if (element.classList.contains('col-sm-8')) {
            element.classList.remove('col-sm-8');
            element.classList.add('col-sm-9');
        }
    }
});
// Lặp qua tất cả các phần tử có thuộc tính data-label
var elements = document.querySelectorAll('[data-label]');
elements.forEach(function(element) {
    // Lấy giá trị của thuộc tính data-label
    var dataLabelValue = element.getAttribute('data-label');
    // Kiểm tra nếu giá trị của thuộc tính data-label là "LBL_AOS_PRODUCT_CATEGORYS_NAME"
    if (dataLabelValue === 'LBL_AOS_PRODUCT_CATEGORYS_NAME') {
        // Thực hiện thay đổi lớp CSS của phần tử từ col-sm-4 thành col-sm-3
        if (element.classList.contains('col-sm-4')) {
            element.classList.remove('col-sm-4');
            element.classList.add('col-sm-3');
        }
    }
});


//------- END PHẦN chỉnh sửa giao diện View Edit -------------------------

var input_name = document.getElementById('part_number'); // Lấy thẻ input bằng ID

function get_voucher_code() {
    var currentDate = new Date(); // Lấy ngày hiện tại
    var day = currentDate.getDate();
    var month = currentDate.getMonth() + 1; // Tháng bắt đầu từ 0 nên cần cộng thêm 1
    var year = currentDate.getFullYear();
    var formattedDate = (day < 10 ? '0' : '') + day + '/' + (month < 10 ? '0' : '') + month + '/' + year;

    $.ajax({
        type: "POST",
        url: "index.php?entryPoint=get_voucher_code",
        data: {
            module: 'aos_products',
            voucherdate: formattedDate,
        },
        success: function(data) {
            input_name.value = data;
            input_name.style.color = "blue";
            input_name.style.fontWeight = "bold";

        },
        error: function(error) {
            // Xử lý lỗi nếu cần
        }
    });
}
if(input_name.value ==''){
get_voucher_code();
}


//-----HÀM kiểm tra MÃ tồn tại
//---check_voucher_code
var fieldcode = 'part_number';
var oldValue ='';
var editViewFieldDiv = document.querySelector('.edit-view-field[type="varchar"][field="'+fieldcode+'"]');
if (editViewFieldDiv) {
    var pElement = document.createElement('p');
    pElement.textContent = '';
    pElement.id = 'remaining';
    editViewFieldDiv.appendChild(pElement);
}

var firstvalue; 
document.addEventListener("DOMContentLoaded", function() {
    var inputElement = document.getElementById(fieldcode);
    if (inputElement) {
        firstvalue = inputElement.value; 
    }
});

document.getElementById(fieldcode).addEventListener("focus", function() {
    // Lưu trữ giá trị hiện tại của input khi focus vào
    oldValue = this.value;
    this.style.color = "black";
    console.log(firstvalue);
    check_voucher_code(fieldcode,'aos_products',oldValue,firstvalue);
});

document.getElementById(fieldcode).addEventListener("blur", function() {
      console.log(firstvalue);
    check_voucher_code(fieldcode,'aos_products',oldValue,firstvalue);

});

