<?php
// created: 2024-06-14 04:02:17
$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Giao cho',
  'LBL_ASSIGNED_TO_NAME' => 'Giao cho',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Ngày tạo',
  'LBL_DATE_MODIFIED' => 'Ngày sửa sau cùng',
  'LBL_MODIFIED' => 'Sửa bởi',
  'LBL_MODIFIED_NAME' => 'Sửa bởi tên',
  'LBL_CREATED' => 'Tạo bởi',
  'LBL_DESCRIPTION' => 'Giá dịch vụ',
  'LBL_DELETED' => 'Xóa dữ liệu',
  'LBL_NAME' => 'Tên dịch vụ',
  'LBL_CREATED_USER' => 'Người tạo',
  'LBL_MODIFIED_USER' => 'Người sửa sau cùng',
  'LBL_LIST_FORM_TITLE' => 'Danh sách Sản Phẩm DV ',
  'LBL_MODULE_NAME' => 'Sản phẩm',
  'LBL_MODULE_TITLE' => 'Sản phẩm: Trang chủ',
  'LBL_HOMEPAGE_TITLE' => 'Của tôi Sản Phẩm DV',
  'LNK_NEW_RECORD' => 'Tạo dịch vụ',
  'LNK_LIST' => 'Xem dịch vụ',
  'LBL_SEARCH_FORM_TITLE' => 'Tìm kiếm Size Sản Phẩm DV Product',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Xem chi tiết lịch sử làm việc',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Hoạt động',
  'LBL_NEW_FORM_TITLE' => 'Sản phẩm mới',
  'LBL_MAINCODE' => 'Mã sản phẩm_kdung',
  'VALUE' => 'Mã',
  'LBL_PART_NUMBER' => 'Mã dịch vụ',
  'LBL_CATEGORY' => 'Danh mục sản phẩm',
  'LBL_TYPE' => 'Loại sản phẩm',
  'LBL_COST' => 'Giá vốn',
  'LBL_PRICE' => 'Giá',
  'LBL_URL' => 'Link sản phẩm',
  'LBL_CONTACT' => 'Người liện hệ',
  'LBL_PRODUCT_IMAGE' => 'Hình ảnh',
  'LBL_IMAGE_UPLOAD_FAIL' => 'ERROR: uploaded file exceeded the max filesize: max filesize: ',
  'LBL_AOS_PRODUCT_CATEGORYS_NAME' => 'Nhóm SP',
  'LBL_AOS_PRODUCT_CATEGORY' => 'Product Category ID',
  'LBL_AOS_PRODUCT_CATEGORIES' => 'DM nhóm SP',
  'LBL_COST_USDOLLAR' => 'Cost (Default Currency)',
  'LBL_PRICE_USDOLLAR' => 'Price (Default Currency)',
  'LBL_FILE_URL' => 'File URL',
  'LBL_CUSTOMERS_PURCHASED_PRODUCTS_SUBPANEL_TITLE' => 'Báo giá',
  'LBL_PRODUCTS_PURCHASES' => 'Báo giá',
  'LBL_AOS_QUOTE_NAME' => 'Báo giá',
  'LBL_ACCOUNT_NAME' => 'Khách hàng',
  'LBL_AOS_PRODUCTS_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE' => 'Cơ hội bán hàng',
  'LBL_AOS_PRODUCTS_ACCOUNTS_1_FROM_ACCOUNTS_TITLE' => 'Accounts',
  'LBL_AOS_PRODUCTS_LEADS_1_FROM_LEADS_TITLE' => 'KH Tiềm năng',
  'LBL_AOS_PRODUCTS_BILL_BILLING_1_FROM_BILL_BILLING_TITLE' => 'Billing',
  'LBL_UNITCODE' => 'ĐVT',
  'LBL_AOS_PRODUCTS_LEADS_1_FROM_AOS_PRODUCTS_TITLE' => 'Sản phẩm / Dịch vụ',
  'LBL_SPA_ACTIONSERVICE_AOS_PRODUCTS_FROM_SPA_ACTIONSERVICE_TITLE' => 'Chiết tính booking',
  'LBL_LM_LEAVEMANAGERMENT_AOS_PRODUCTS_FROM_LM_LEAVEMANAGERMENT_TITLE' => ' Nghỉ phép',
  'LBL_AOS_PRODUCTS_DOCUMENTS_1_FROM_DOCUMENTS_TITLE' => 'Tài liệu',
  'LBL_PACKING_L_CM' => 'L',
  'LBL_PACKING_W_CM' => 'W',
  'LBL_PACKING_H_CM' => 'H',
  'LBL_PACKING_L_INCH' => 'Dài(inch)',
  'LBL_PACKING_W_INCH' => 'Rộng(inch)',
  'LBL_PACKING_H_INCH' => 'Cao(inch)',
  'LBL_EDITVIEW_PANEL1' => 'Sales',
  'LBL_CBM' => 'CBM',
  'LBL_DESCRIPTION_GOODS_EN' => 'Description EN',
  'LBL_DESCRIPTION_GOODS_VN' => 'Mô tả',
  'LBL_DESCRIBE_IRON_FRAME' => 'Mô tả khung sắt',
  'LBL_WEIGHT_IRON_FRAME' => 'Trọng lượng khung sắt',
  'LBL_KNITTING_PRICE' => 'Giá đan',
  'LBL_KNITTING_DESCRIPTION' => 'Mô tả đan',
  'LBL_EDITVIEW_PANEL2' => 'Tab Sắt',
  'LBL_DETAILVIEW_PANEL2' => 'Khác',
  'LBL_DETAILVIEW_PANEL3' => 'Vật liệu',
  'SETS_CTN' => 'Sets/CTN',
  'LBL_SETS_CTN' => 'Sets/CTN',
  'LBL_AOS_PRODUCTS_AOS_SIZE_1_FROM_AOS_SIZE_TITLE' => 'Size',
  'LBL_EDITVIEW_PANEL3' => 'Mô tả',
  'LBL_SIZE_L_CM' => 'D',
  'LBL_SIZE_W_CM' => 'R',
  'LBL_SIZE_H_CM' => 'C',
  'LBL_SIZE_L_INCH' => 'Dài(inch)',
  'LBL_SIZE_W_INCH' => 'Rộng(inch)',
  'LBL_SIZE_H_INCH' => 'Cao(inch)',
  'LBL_SIZE_W_H_CM' => 'Rộng/Cao (cm)',
  'LBL_SIZE_W_H_INCH' => 'Rộng/Cao (inch)',
  'LNK_IMPORT_AOS_SIZE' => 'Nhập Size Sản Phẩm DV',
  'LBL_HANDLE' => 'Quai',
  'LBL_BOTTOM_LENGTH' => 'DĐ',
  'LBL_BOTTOM_WIDTH' => 'RĐ',
  'LBL_LINE_ITEMS' => 'Kích thước',
  'LBL_EDITVIEW_PANEL4' => 'Đan & Sắt',
  'LBL_EDITVIEW_PANEL5' => 'Khác',
  'LBL_EXTRA_IMAGE' => 'Hình ảnh khác',
  'LBL_EDITVIEW_PANEL6' => 'Mô tả sản phẩm',
  'LBL_CREATE_SZIE' => 'Tạo size',
  'LBL_CREATE_PACKING_SZIE' => 'Tạo packing size',
  'LBL_DETAILVIEW_PANEL6' => 'Bảng điều khiển mới 6',
  'LBL_AOS_PRODUCT_CATEGORY_NAME_AOS_PRODUCT_CATEGORIES_ID' => '&#039;Nhóm SP&#039; (liên quan &#039;DM nhóm sản phẩm DV&#039; ID)',
  'LBL_PACKING_LINE_ITEMS' => 'Kích thước đóng gói',
  'LBL_WEIGHT_OTHER' => 'Trọng lượng khác',
  'LBL_CHARACTER_ERROR' => 'Ký tự không hợp lệ, chỉ cho phép sử dụng "- . _ khoảng trắng"',
  'LBL_ALREADY-EXISTS' => 'Mã đã tồn tại',
  'LBL_SAVE' => 'Lưu',
  'LBL_LOAD_PACKING' => 'Load 1x40HC',
  'LBL_FOB_PACKING' => 'FOB',
  'LBL_NOTE_PACKING' => 'Note',
  'LBL_SEE_DETAILS' => 'Xem chi tiết',
  'LBL_SELECT_COLUMNS' => 'Cột hiển thị',
  'LBL_2_COLUMNS' => '2 Cột',
  'LBL_3_COLUMNS' => '3 Cột',
  'LBL_4_COLUMNS' => '4 Cột',
  'LBL_Of' => 'Của',
  'LBL_VND' => 'VND',
  'LBL_TYPE_CURRENCY' => 'Loại Tiền',
  'LBL_TYPE_SERVICE' => 'Loại dịch vụ',
  'LBL_USD' => 'USD',
  'LBL_EXCHANGE_RATE' => 'Tỷ giá',
  'LBL_AOS_PRODUCTS_SPA_ACTIONSERVICE_1_FROM_SPA_ACTIONSERVICE_TITLE' => 'Chi tiết hoa hồng',
);