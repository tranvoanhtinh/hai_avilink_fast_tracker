<?php
$listViewDefs ['AOS_Products'] = 
array (
  'PART_NUMBER' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_PART_NUMBER',
    'width' => '10%',
    'default' => true,
  ),
  'NAME' => 
  array (
    'width' => '15%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'UPDATE_PRICE' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_UPDATE_PRICE',
    'width' => '10%',
    'default' => true,
  ),
  'TYPE' => 
  array (
    'type' => 'enum',
    'default' => true,
    'label' => 'LBL_TYPE',
    'width' => '10%',
  ),
  'TYPE_CURRENCY' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_TYPE_CURRENCY',
    'width' => '10%',
    'default' => true,
  ),
  'EXCHANGE_RATE' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_EXCHANGE_RATE',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
  ),
  'PRICE' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_PRICE',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'link' => true,
    'type' => 'relate',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'id' => 'ASSIGNED_USER_ID',
    'width' => '10%',
    'default' => true,
  ),
);
;
?>
