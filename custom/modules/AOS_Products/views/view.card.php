<?php
require_once('include/MVC/View/views/view.list.php');

class CustomAOS_ProductsViewCard extends ViewList
{
    function display()
    {
        global $mod_strings;
        ?>
          <link rel="stylesheet" type="text/css" href="custom/modules/AOS_Products/css/cardview.css">
        <?php

        $columns_per_row  = 3;  // mặt định 3 cột
        if(isset($_SESSION['columns_per_row'])){
            $columns_per_row =  $_SESSION['columns_per_row'];
        }
        $products_per_page = 40;
        if(isset($_POST['selected_columns'])){
            $selectedColumns = $_POST['selected_columns']; 
            $columns_per_row = $selectedColumns;
        }
        switch ($columns_per_row) {
            case 6:
                $height = 450;
                $products_per_page = 20;
                break;
            case 4:
                 $height = 300;
                 $products_per_page = 30;
                break;
            case 3:
                 $height = 200;
                 $products_per_page = 40;
                break;
            // Thêm các trường hợp khác nếu cần thiết
            default:
                $height = 200;
        }
        $_SESSION['columns_per_row'] = $columns_per_row;

        $order_by = "date_entered DESC";
        $where = "aos_products.deleted = 0";




        if(isset($_POST['search_from'])){
            $like  = $_POST['searchInput'];

            $where = "aos_products.deleted = 0 AND aos_products.name LIKE '%$like%'";
            $productsBeans = BeanFactory::getBean('AOS_Products')->get_full_list($order_by, $where);
            $_SESSION['search'] = $productsBeans;
            $_SESSION['status_search'] = '1';
            $_SESSION['like'] = $like;
        }elseif($_POST['clear_search']){
             $productsBeans = BeanFactory::getBean('AOS_Products')->get_full_list($order_by, $where);
             unset($_SESSION['like']);
             unset($_SESSION['status_search']);
        }
        elseif($_SESSION['status_search'] ==1){
            $productsBeans = $_SESSION['search'];

        }else{
            $productsBeans = BeanFactory::getBean('AOS_Products')->get_full_list($order_by, $where);
        }

    
        $numProducts = count($productsBeans);
  
        $totalpages = ceil($numProducts / $products_per_page);

        $currentpage = 1;


        if (isset($_POST['currentpage'])) {
            $currentpage = $_POST['currentpage'];

            if($_POST['currentpage'] == 'end'){
                $currentpage = $totalpages;
            }
        }

        $startproduct = ($currentpage - 1) * $products_per_page + 1;
        $endproduct = min($currentpage * $products_per_page, $numProducts);




        $pages = array (
              'currentpage'=>$currentpage,
              'startproduct'=>$startproduct,
              'endproduct'=>$endproduct,
              'totalproduct'=>$numProducts,
        );
        // Khởi tạo mảng để lưu trữ thông tin sản phẩm
        $products = array();

        // Lặp qua danh sách các Bean và lấy thông tin của từng sản phẩm
        for ($i = $startproduct-1; $i < $endproduct; $i++) {
            $productBean = $productsBeans[$i];
            $product = array(
                'description'=>$productBean->description,
                'part_number' => $productBean->part_number,
                'name' => $productBean->name,
                'id' => $productBean->id,
                'image_url' => $productBean->product_image,
                'price' => $productBean->price,
                'description' => $productBean->description,
                // Bổ sung các trường dữ liệu khác nếu cần thiết
            );

            // Lấy thông tin của bảng kích thước (size)
            $sizeBeans = $productBean->get_linked_beans('aos_products_aos_size_1', 'aos_size'); // 'aos_products_aos_size_1' là tên quan hệ, 'aos_size' là tên của module kích thước
            $sizes = array(); 
            foreach ($sizeBeans as $sizeBean) {
                $size = array(
                    'size_l_cm' => customFormat($sizeBean->size_l_cm),
                    'size_w_cm' => customFormat($sizeBean->size_w_cm),
                    'size_h_cm' => customFormat($sizeBean->size_h_cm),
                );
                // Thêm thông tin kích thước vào mảng $sizes
                $sizes[] = $size;
            }

            // Thêm mảng $sizes vào mảng sản phẩm $product
            $product['sizes'] = $sizes;
            $count_sizes = count($product['sizes']);


            // Thêm sản phẩm vào mảng chứa danh sách sản phẩm
            $products[] = $product;
        }


        $this->ss->assign('count_sizes',$count_sizes);
        $this->ss->assign('mod_strings',$mod_strings);
        $this->ss->assign('pages',$pages);
        $this->ss->assign('products', $products);
        $this->ss->assign('height', $height);
        $this->ss->assign('columns_per_row',$columns_per_row);
        // Hiển thị mẫu card sản phẩm
        $this->ss->display('custom/modules/AOS_Products/tpl/card_view.tpl');

        ?>
         <script src="custom/modules/AOS_Products/js/handle_view_card.js"></script>
        <?php
    }
}
function customFormat($number) {

    return number_format((float) $number, 1);
}


?>


