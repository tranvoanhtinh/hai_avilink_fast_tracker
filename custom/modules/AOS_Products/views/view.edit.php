 
<?php

require_once('modules/AOS_Products/views/view.edit.php');

class CustomAOS_ProductsViewEdit extends AOS_ProductsViewEdit
{
    function display()
    {
        ?>
        <link rel="stylesheet" type="text/css" href="custom/modules/AOS_Products/css/style_edit.css">
        <?php
        parent::display(); // Gọi phương thức hiển thị từ lớp cha
         ?>
      <script src="include/javascript/custom/view.edit.js"></script>
      <script src="custom/modules/AOS_Products/js/handle_view_edit.js"></script>
<?php

    }
}

