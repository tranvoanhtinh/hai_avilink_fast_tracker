<?php
$module_name = 'tttt_History_calls';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'current_user_only' => 
      array (
        'name' => 'current_user_only',
        'label' => 'LBL_CURRENT_USER_FILTER',
        'type' => 'bool',
        'default' => true,
      ),
    ),
    'advanced_search' => 
    array (
      'starttime' => 
      array (
        'type' => 'datetime',
        'label' => 'LBL_STARTTIME',
        'width' => '10%',
        'default' => true,
        'name' => 'starttime',
      ),
      'totalduration' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_TOTALDURATION',
        'width' => '10%',
        'default' => true,
        'name' => 'totalduration',
      ),
      'assigned_user_id' => 
      array (
        'name' => 'assigned_user_id',
        'label' => 'LBL_ASSIGNED_TO',
        'type' => 'enum',
        'function' => 
        array (
          'name' => 'get_user_array',
          'params' => 
          array (
            0 => false,
          ),
        ),
        'default' => true,
        'width' => '10%',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
;
?>
