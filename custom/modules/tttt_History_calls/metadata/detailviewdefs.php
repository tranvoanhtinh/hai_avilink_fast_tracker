<?php
$module_name = 'tttt_History_calls';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'collapsed',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'date_entered',
          1 => 
          array (
            'name' => 'customernumberfrom',
            'label' => 'LBL_CUSTOMERNUMBERFROM',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'customernumberto',
            'label' => 'LBL_CUSTOMERNUMBERTO',
          ),
          1 => 
          array (
            'name' => 'userextension',
            'label' => 'LBL_USEREXTENSION',
          ),
        ),
        2 => 
        array (
          0 => 'assigned_user_name',
          1 => '',
        ),
        3 => 
        array (
          0 => 'description',
        ),
      ),
    ),
  ),
);
;
?>
