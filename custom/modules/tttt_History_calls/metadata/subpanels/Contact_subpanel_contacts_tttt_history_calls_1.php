<?php
// created: 2020-12-16 17:48:51
$subpanel_layout['list_fields'] = array (
  'date_entered' => 
  array (
    'type' => 'datetime',
    'vname' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
  'customernumberfrom' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_CUSTOMERNUMBERFROM',
    'width' => '10%',
    'default' => true,
  ),
  'customernumberto' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_CUSTOMERNUMBERTO',
    'width' => '10%',
    'default' => true,
  ),
  'userextension' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_USEREXTENSION',
    'width' => '10%',
    'default' => true,
  ),
  'direction' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_DIRECTION',
    'width' => '10%',
    'default' => true,
  ),
  'assigned_user_name' => 
  array (
    'link' => true,
    'type' => 'relate',
    'vname' => 'LBL_ASSIGNED_TO_NAME',
    'id' => 'ASSIGNED_USER_ID',
    'width' => '10%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Users',
    'target_record_key' => 'assigned_user_id',
  ),
);