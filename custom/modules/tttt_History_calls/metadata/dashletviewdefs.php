<?php
$dashletData['tttt_History_callsDashlet']['searchFields'] = array (
  'date_entered' => 
  array (
    'default' => '',
  ),
  'date_modified' => 
  array (
    'default' => '',
  ),
  'assigned_user_id' => 
  array (
    'type' => 'assigned_user_name',
    'default' => 'Administrator',
  ),
);
$dashletData['tttt_History_callsDashlet']['columns'] = array (
  'name' => 
  array (
    'type' => 'name',
    'link' => true,
    'label' => 'LBL_NAME',
    'width' => '10%',
    'default' => true,
  ),
  'customernumber' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_CUSTOMERNUMBER',
    'width' => '10%',
    'default' => true,
  ),
  'file_ghiam' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_FILE_GHIAM',
    'width' => '10%',
    'default' => true,
  ),
  'callstatus' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_CALLSTATUS',
    'width' => '10%',
    'default' => true,
  ),
  'starttime' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_STARTTIME',
    'width' => '10%',
    'default' => true,
  ),
  'billduration' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_BILLDURATION',
    'width' => '10%',
    'default' => true,
  ),
  'created_by' => 
  array (
    'width' => '8%',
    'label' => 'LBL_CREATED',
    'name' => 'created_by',
    'default' => false,
  ),
  'workstatus' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_WORKSTATUS',
    'width' => '10%',
    'default' => false,
  ),
  'date_entered' => 
  array (
    'width' => '15%',
    'label' => 'LBL_DATE_ENTERED',
    'default' => false,
    'name' => 'date_entered',
  ),
  'date_modified' => 
  array (
    'width' => '15%',
    'label' => 'LBL_DATE_MODIFIED',
    'name' => 'date_modified',
    'default' => false,
  ),
  'assigned_user_name' => 
  array (
    'width' => '8%',
    'label' => 'LBL_LIST_ASSIGNED_USER',
    'name' => 'assigned_user_name',
    'default' => false,
  ),
  'modified_by_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_MODIFIED_NAME',
    'id' => 'MODIFIED_USER_ID',
    'width' => '10%',
    'default' => false,
    'name' => 'modified_by_name',
  ),
  'description' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
    'name' => 'description',
  ),
  'created_by_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'default' => false,
    'name' => 'created_by_name',
  ),
);
