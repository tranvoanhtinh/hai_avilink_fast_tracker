<?php
$module_name = 'tttt_History_calls';
$listViewDefs [$module_name] = 
array (
  'STARTTIME' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_STARTTIME',
    'width' => '10%',
    'default' => true,
  ),
  'CUSTOMERNUMBERFROM' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_CUSTOMERNUMBERFROM',
    'width' => '10%',
    'default' => true,
  ),
  'CUSTOMERNUMBERTO' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_CUSTOMERNUMBERTO',
    'width' => '10%',
    'default' => true,
  ),
  'USEREXTENSION' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_USEREXTENSION',
    'width' => '10%',
    'default' => true,
  ),
  'FILE_GHIAM' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_FILE_GHIAM',
    'width' => '10%',
    'default' => true,
  ),
  'TOTALDURATION' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_TOTALDURATION',
    'width' => '10%',
    'default' => true,
  ),
  'CALLSTATUS' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_CALLSTATUS',
    'width' => '10%',
    'default' => true,
  ),
  'DIRECTION' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_DIRECTION',
    'width' => '10%',
    'default' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'link' => true,
    'type' => 'relate',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'id' => 'ASSIGNED_USER_ID',
    'width' => '10%',
    'default' => true,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => false,
  ),
  'WORKSTATUS' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_WORKSTATUS',
    'width' => '10%',
    'default' => false,
  ),
  'BILLDURATION' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_BILLDURATION',
    'width' => '10%',
    'default' => false,
  ),
  'CALLUUID' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_CALLUUID',
    'width' => '10%',
    'default' => false,
  ),
  'CUSTOMERNUMBER' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_CUSTOMERNUMBER',
    'width' => '10%',
    'default' => false,
  ),
  'ENDTIME' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ENDTIME',
    'width' => '10%',
    'default' => false,
  ),
  'ANSWERTIME' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ANSWERTIME',
    'width' => '10%',
    'default' => false,
  ),
);
;
?>
