var urlParams = new URLSearchParams(window.location.search);
var currentURL = window.location.href;


if (currentURL.includes('maincode=')) {
    var monthInput = document.createElement('input');
monthInput.type = 'hidden';
monthInput.name = 'month_basic';
monthInput.id = 'month_basic';

var yearInput = document.createElement('input');
yearInput.type = 'hidden';
yearInput.name = 'year_basic';
yearInput.id = 'year_basic';

var takeFromInput = document.createElement('input');
takeFromInput.type = 'hidden';
takeFromInput.name = 'take_from_basic';
takeFromInput.id = 'take_from_basic';

var maincodeInput = document.createElement('input');
maincodeInput.type = 'hidden';
maincodeInput.name = 'maincode_basic';
maincodeInput.id = 'maincode_basic';

var inOutInput = document.createElement('input');
inOutInput.type = 'hidden';
inOutInput.name = 'in_out_basic';
inOutInput.id = 'in_out_basic';

// Append inputs to the form
var searchForm = document.getElementById('search_form');
searchForm.appendChild(monthInput);
searchForm.appendChild(yearInput);
searchForm.appendChild(takeFromInput);
searchForm.appendChild(maincodeInput);
searchForm.appendChild(inOutInput);
    function populateInputsFromURL() {
       
        var urlParams = new URLSearchParams(window.location.search);
        if (urlParams.has('month')) {
            monthInput.value = urlParams.get('month');
        }

        if (urlParams.has('year')) {
            yearInput.value = urlParams.get('year');
        }

        if (urlParams.has('warehouse')) {
            takeFromInput.value = urlParams.get('warehouse');
        }

        if (urlParams.has('maincode')) {
            maincodeInput.value = urlParams.get('maincode');
        }

        if (urlParams.has('type')) {
            inOutInput.value = urlParams.get('type');
        }

        // Trigger the form submission after setting the input values
        document.getElementById("search_form_submit").click();
    }
}
if (currentURL.includes('maincode=')) {
    // Call the function to populate inputs from URL
    populateInputsFromURL();
}


var currentURL = window.location.href;
var index = currentURL.indexOf("/index.php");
var desiredPart = currentURL.slice(0, index);


var tdElements2 = document.querySelectorAll('td[field="maincode"]');
tdElements2.forEach(function(td) {
    var pino = td.textContent.trim();
    $.ajax({
        type: "POST",
        url: "index.php?entryPoint=pino_name_product",
        data: {
            pino: pino,
        },
        success: function(data) {

            // Tạo một thẻ a mới
            var link = document.createElement('a');
            // Đặt thuộc tính href cho thẻ a
            var desiredPart3 = desiredPart + "/index.php?module=AOS_Products&action=DetailView&record=" + data;
            link.setAttribute('href', desiredPart3);
            // Đặt nội dung cho thẻ a là mã sản phẩm
            link.textContent = pino;

            // Tạo thẻ <b> mới
            var boldElement = document.createElement('b');
            // Chèn thẻ <a> vào trong thẻ <b>
            boldElement.appendChild(link);

            // Xóa nội dung hiện tại của ô td
            td.innerHTML = ''; 
            // Chèn thẻ <b> vào ô td
            td.appendChild(boldElement); 
        },
        error: function(error) {
            // Xử lý lỗi nếu cần
            console.error("Error:", error);
        }
    });
});




var tdElements = document.querySelectorAll('td[type="name"][field="name"].inlineEdit');
tdElements.forEach(function(tdElement) {
    // Lấy tất cả các phần tử a trong td đó
    var aElements = tdElement.querySelectorAll('a');
    
    // Lặp qua từng phần tử a
    aElements.forEach(function(aElement) {
        // Tạo một phần tử p mới
        var pElement = document.createElement('p');
        // Sao chép nội dung của phần tử a vào phần tử p
        pElement.innerHTML = aElement.innerHTML;
        // Thay thế phần tử a bằng phần tử p
        aElement.parentNode.replaceChild(pElement, aElement);
    });
});

var vouchernoTds = document.querySelectorAll('td[field="voucherno"]');
var vouchernoTds = document.querySelectorAll('td[field="voucherno"]');
vouchernoTds.forEach(function(td) {
    td.style.color = "#f08377";
});

var module_dir; // Đặt biến module_dir ở mức phạm vi cao hơn

vouchernoTds.forEach(function(td) {
    td.addEventListener('click', function() {
        var vouchernoValue = td.innerText.trim(); 
        var typeVoucherValue = td.parentNode.querySelector('td[field="type_voucher"]').innerText.trim();
        var parts = typeVoucherValue.split('.'); // Tách chuỗi bằng dấu chấm

        var numberPart = parts[0];
        switch (numberPart) {
          case '1': // Chú ý, '1' thay vì 1 để khớp với chuỗi
            module_dir = "AOS_Inventoryinput"; // Gán giá trị cho module_dir
            break;
          case '2':
            module_dir = "AOS_Inventoryoutput"; // Gán giá trị cho module_dir
            break;
          case '3':
            module_dir = "AOS_Warehouse_Transfer"; // Gán giá trị cho module_dir
            break;
        }
        console.log(module_dir);
        // Kiểm tra module_dir đã được gán giá trị chưa
        if (module_dir) {
          $.ajax({
            type: "POST",
            url: "index.php?entryPoint=link_inventory_detail",
            data: {
              voucherno: vouchernoValue,
              module_dir: module_dir,
            },
            success: function(data) {
              datas  = JSON.parse(data);
              if(datas.id !== undefined){
                var href = desiredPart + "/index.php?module=" + datas.module_dir + "&return_module="+ datas.module_dir +"&action=DetailView&record=" + datas.id;
                window.open(href, '_blank'); // Mở URL trong tab mới
              }else {
               alert('Mẫu tin ' + vouchernoValue + ' đã bị xóa');
              }
            },
          });
        }
    
    });

});

