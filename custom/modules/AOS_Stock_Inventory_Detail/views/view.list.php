<?php
require_once('include/MVC/View/views/view.list.php');

class CustomAOS_Stock_inventory_DetailViewList extends ViewList
{
    function display()
    {

        parent::display(); // Gọi phương thức hiển thị từ lớp cha
        ?>
            <script src="custom/modules/AOS_Stock_Inventory_Detail/script.js"></script>
        <?php
    }
}

