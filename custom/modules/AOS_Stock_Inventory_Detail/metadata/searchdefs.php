<?php
$module_name = 'AOS_Stock_Inventory_Detail';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'type_voucher' => 
      array (
        'type' => 'enum',
        'label' => 'LBL_TYPE_VOUCHER',
        'width' => '10%',
        'default' => true,
        'name' => 'type_voucher',
      ),
      'month' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_MONTH',
        'width' => '10%',
        'default' => true,
        'name' => 'month',
      ),
      'maincode' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_MAINCODE',
        'width' => '10%',
        'default' => true,
        'name' => 'maincode',
      ),
    ),
    'advanced_search' => 
    array (
      'maincode' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_MAINCODE',
        'width' => '10%',
        'default' => true,
        'name' => 'maincode',
      ),
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'voucherdate' => 
      array (
        'type' => 'date',
        'label' => 'LBL_VOUCHER_DATE',
        'width' => '10%',
        'default' => true,
        'name' => 'voucherdate',
      ),
      'day' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_DAY',
        'width' => '10%',
        'default' => true,
        'name' => 'day',
      ),
      'month' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_MONTH',
        'width' => '10%',
        'default' => true,
        'name' => 'month',
      ),
      'year' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_YEAR',
        'width' => '10%',
        'default' => true,
        'name' => 'year',
      ),
      'take_from' => 
      array (
        'type' => 'enum',
        'label' => 'LBL_TAKE_FROM',
        'width' => '10%',
        'default' => true,
        'name' => 'take_from',
      ),
      'type_voucher' => 
      array (
        'type' => 'enum',
        'label' => 'LBL_TYPE_VOUCHER',
        'width' => '10%',
        'default' => true,
        'name' => 'type_voucher',
      ),
      'assigned_user_id' => 
      array (
        'name' => 'assigned_user_id',
        'label' => 'LBL_ASSIGNED_TO',
        'type' => 'enum',
        'function' => 
        array (
          'name' => 'get_user_array',
          'params' => 
          array (
            0 => false,
          ),
        ),
        'default' => true,
        'width' => '10%',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
;
?>
