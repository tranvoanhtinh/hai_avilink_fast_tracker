<?php
$module_name = 'AOS_Stock_Inventory_Detail';
$listViewDefs [$module_name] = 
array (
  'TYPE_VOUCHER' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_TYPE_VOUCHER',
    'width' => '10%',
    'default' => true,
  ),
  'TAKE_FROM' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_TAKE_FROM',
    'width' => '10%',
    'default' => true,
  ),
  'VOUCHERDATE' => 
  array (
    'type' => 'date',
    'label' => 'LBL_VOUCHER_DATE',
    'width' => '10%',
    'default' => true,
  ),
  'VOUCHERNO' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_VOUCHERNO',
    'width' => '10%',
    'default' => true,
  ),
  'MAINCODE' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_MAINCODE',
    'width' => '10%',
    'default' => true,
  ),
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'QUANTITY' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_QUANTITY',
    'width' => '10%',
    'default' => true,
  ),
  'PRICE' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_PRICE',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
  ),
  'TOTAL_PRICE' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_TOTAL_PRICE',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => false,
  ),
  'PARENT_ID' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_PARENT_ID',
    'width' => '10%',
    'default' => false,
  ),
  'CREATED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'default' => false,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => false,
  ),
  'DAY' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_DAY',
    'width' => '10%',
    'default' => false,
  ),
  'MONTH' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_MONTH',
    'width' => '10%',
    'default' => false,
  ),
  'YEAR' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_YEAR',
    'width' => '10%',
    'default' => false,
  ),
  'IN_OUT' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_IN_OUT',
    'width' => '10%',
    'default' => false,
  ),
);
;
?>
