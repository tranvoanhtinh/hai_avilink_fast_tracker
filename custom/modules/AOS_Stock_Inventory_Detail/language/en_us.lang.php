<?php 
$mod_strings = array (
  'LBL_IN_OUT' => 'In/Out',
  'LBL_MAINCODE' => 'Product Code',
  'LBL_QUANTITY' => 'Quantity',
  'LBL_PRICE' => 'Price',
  'LBL_TOTAL_PRICE' => 'Total Amount',
  'LBL_DAY' => 'Day',
  'LBL_MONTH' => 'Month',
  'LBL_YEAR' => 'Year',
  'LBL_DATE_IN_OUT'=>'dd/mm/yyy',
  'LBL_TAKE_FROM'=>'Warehouse',
  'LBL_VOUCHER_DATE'=> 'Voucher date',
  'LBL_VOUCHERNO'=> 'Voucher No',
  'LBL_PARENT_ID'=> 'Voucher No',
  'LBL_TYPE_VOUCHER'=>'Type voucher',
   'LBL_NAME' => 'Product Name',
);

