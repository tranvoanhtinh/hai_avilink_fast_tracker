<?php
// created: 2024-03-18 10:15:09
$mod_strings = array (
  'LBL_IN_OUT' => 'Nhập/Xuất',
  'LBL_MAINCODE' => 'Mã sản phẩm',
  'LBL_QUANTITY' => 'Số lượng',
  'LBL_PRICE' => 'Giá',
  'LBL_TOTAL_PRICE' => 'Tổng tiền',
  'LBL_DAY' => 'Ngày',
  'LBL_MONTH' => 'Tháng',
  'LBL_YEAR' => 'Năm',
  'LBL_DATE_IN_OUT' => 'dd/mm/yyy',
  'LBL_TAKE_FROM' => 'Từ kho',
  'LBL_VOUCHER_DATE' => 'Ngày chứng từ',
  'LBL_AOS_INVENTORYOUTPUT_AOS_STOCK_INVENTORY_DETAIL_1_FROM_AOS_INVENTORYOUTPUT_TITLE' => 'Xuất Kho',
  'LBL_VOUCHERNO'=> 'Số chứng từ',
  'LBL_PARENT_ID'=> 'Số chứng từ',
  'LBL_TYPE_VOUCHER'=>'Loại chứng từ',
  'LBL_NAME' => 'Tên sản phẩm',
);