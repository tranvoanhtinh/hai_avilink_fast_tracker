<?php
$dashletData['AOS_PassengersDashlet']['searchFields'] = array (
  'date_entered' => 
  array (
    'default' => '',
  ),
  'date_modified' => 
  array (
    'default' => '',
  ),
  'assigned_user_id' => 
  array (
    'type' => 'assigned_user_name',
    'default' => 'Tính Dev CRMOnline',
  ),
);
$dashletData['AOS_PassengersDashlet']['columns'] = array (
  'name' => 
  array (
    'width' => '40%',
    'label' => 'LBL_LIST_NAME',
    'link' => true,
    'default' => true,
    'name' => 'name',
  ),
  'passport' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_PASSPORT',
    'width' => '10%',
    'default' => true,
    'name' => 'passport',
  ),
  'nationality' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_NATIONALITY',
    'width' => '10%',
    'default' => true,
    'name' => 'nationality',
  ),
  'sex' => 
  array (
    'type' => 'enum',
    'default' => true,
    'label' => 'LBL_SEX',
    'width' => '10%',
    'name' => 'sex',
  ),
  'date_entered' => 
  array (
    'width' => '15%',
    'label' => 'LBL_DATE_ENTERED',
    'default' => true,
    'name' => 'date_entered',
  ),
  'date_modified' => 
  array (
    'width' => '15%',
    'label' => 'LBL_DATE_MODIFIED',
    'name' => 'date_modified',
    'default' => false,
  ),
  'phone_mobile' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_PHONE_MOBILE',
    'width' => '10%',
    'default' => false,
    'name' => 'phone_mobile',
  ),
  'description' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
    'name' => 'description',
  ),
  'created_by' => 
  array (
    'width' => '8%',
    'label' => 'LBL_CREATED',
    'name' => 'created_by',
    'default' => false,
  ),
  'assigned_user_name' => 
  array (
    'width' => '8%',
    'label' => 'LBL_LIST_ASSIGNED_USER',
    'name' => 'assigned_user_name',
    'default' => false,
  ),
);
