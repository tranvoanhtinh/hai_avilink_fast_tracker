<?php
$module_name = 'AOS_Passengers';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'PASSPORT' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_PASSPORT',
    'width' => '10%',
    'default' => true,
  ),
  'PHOTO' => 
  array (
    'type' => 'image',
    'width' => '10%',
    'studio' => 
    array (
      'listview' => true,
    ),
    'label' => 'LBL_PHOTO',
    'default' => true,
  ),
  'SEX' => 
  array (
    'type' => 'enum',
    'default' => true,
    'label' => 'LBL_SEX',
    'width' => '10%',
  ),
  'DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => true,
  ),
  'PHONE_MOBILE' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_PHONE_MOBILE',
    'width' => '10%',
    'default' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
  'MODIFIED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_MODIFIED_NAME',
    'id' => 'MODIFIED_USER_ID',
    'width' => '10%',
    'default' => false,
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => false,
  ),
  'CREATED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'default' => false,
  ),
  'PHONE_OTHER' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_PHONE_OTHER',
    'width' => '10%',
    'default' => false,
  ),
);
;
?>
