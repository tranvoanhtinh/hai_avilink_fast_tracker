<?php
$popupMeta = array (
    'moduleMain' => 'AOS_Passengers',
    'varName' => 'AOS_Passengers',
    'orderBy' => 'aos_passengers.name',
    'whereClauses' => array (
  'name' => 'aos_passengers.name',
  'passport' => 'aos_passengers.passport',
  'nationality' => 'aos_passengers.nationality',
  'sex' => 'aos_passengers.sex',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'passport',
  5 => 'nationality',
  6 => 'sex',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => '10%',
  ),
  'passport' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_PASSPORT',
    'width' => '10%',
    'name' => 'passport',
  ),
  'nationality' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_NATIONALITY',
    'width' => '10%',
    'name' => 'nationality',
  ),
  'sex' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_SEX',
    'width' => '10%',
    'name' => 'sex',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'PASSPORT' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_PASSPORT',
    'width' => '10%',
    'default' => true,
    'name' => 'passport',
  ),
  'NATIONALITY' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_NATIONALITY',
    'width' => '10%',
    'default' => true,
    'name' => 'nationality',
  ),
  'SEX' => 
  array (
    'type' => 'enum',
    'default' => true,
    'label' => 'LBL_SEX',
    'width' => '10%',
    'name' => 'sex',
  ),
),
);
