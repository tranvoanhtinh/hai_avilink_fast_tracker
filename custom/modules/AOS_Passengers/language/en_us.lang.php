<?php
// created: 2024-06-05 06:17:09
$mod_strings = array (
  'LBL_NAME' => 'Name',
  'LBL_SEX' => 'Gender',
  'LBL_PASSPORT' => 'Passport',
  'LBL_NATIONALITY' => 'Nationality',
  'LBL_PHONE_MOBILE' => 'Mobile Phone',
  'LBL_PHONE_OTHER' => 'Other Phone',
  'LNK_NEW_RECORD' => 'Create Passengers',
  'LNK_LIST' => 'View Passengers',
  'LNK_IMPORT_AOS_PASSENGERS' => 'Nhập Passengers',
  'LBL_LIST_FORM_TITLE' => 'Passenger List',
  'LBL_SEARCH_FORM_TITLE' => 'Search Passenger',
  'LBL_ALREADY-EXISTS' => 'Passport already exists',
  'LBL_CHARACTER_ERROR' => 'Special character error',
    'LBL_PHOTO'=> 'Photo',

);