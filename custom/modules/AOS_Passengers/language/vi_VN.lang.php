<?php
// created: 2024-06-05 06:18:08
$mod_strings = array (
  'LBL_NAME' => 'Tên',
  'LBL_SEX' => 'Giới tính',
  'LBL_PASSPORT' => 'Passport',
  'LBL_NATIONALITY' => 'Quốc tịch',
  'LBL_PHONE_MOBILE' => 'Di động',
  'LBL_PHONE_OTHER' => 'Điện thoại khác',
  'LNK_NEW_RECORD' => 'Tạo Hành khách',
  'LNK_LIST' => 'Xem Hành khách',
  'LNK_IMPORT_AOS_PASSENGERS' => 'Nhập Hành khách',
  'LBL_LIST_FORM_TITLE' => 'Hành kháchs Danh sách',
  'LBL_SEARCH_FORM_TITLE' => 'Tìm kiếm Hành kháchs',
  'LBL_HOMEPAGE_TITLE' => 'Của tôi Hành khách',
  'LBL_ALREADY-EXISTS' => 'Passport đã tồn tại',
  'LBL_CHARACTER_ERROR' => 'Ký tự không hợp lệ, chỉ cho phép sử dụng "- . _ khoảng trắng"',
    'LBL_PHOTO'=>'Hình ảnh',
);