//-----HÀM kiểm tra MÃ tồn tại
//---check_voucher_code

var check_duplicate = 0;
var duplicate = document.querySelector('input[name="duplicateSave"]');
if(duplicate){
    if(duplicate.value =='true'){
        check_duplicate = 1;  // đang trùng lập
    }
}

var fieldcode = 'passport';
var oldValue ='';

    var divElement = document.querySelector('div.col-xs-12.col-sm-8.edit-view-field[type="varchar"][field="passport"]');
    if (divElement) {
        var pElement = document.createElement('p');
        pElement.id = 'remaining';
        divElement.appendChild(pElement);
    } 
if(check_duplicate == 1){
        firstvalue = -999;
}else {
    var firstvalue; 
    document.addEventListener("DOMContentLoaded", function() {
        var inputElement = document.getElementById(fieldcode);
        if (inputElement) {
            firstvalue = inputElement.value; 
        }
    });
}

document.getElementById(fieldcode).addEventListener("focus", function() {
    // Lưu trữ giá trị hiện tại của input khi focus vào
    oldValue = this.value;
    this.style.color = "black";
    console.log(firstvalue);
    check_voucher_code(fieldcode,'aos_passengers',oldValue,firstvalue);
});

document.getElementById(fieldcode).addEventListener("blur", function() {
      console.log(firstvalue);
    check_voucher_code(fieldcode,'aos_passengers',oldValue,firstvalue);

});

 var s = SUGAR.language.get(module_sugar_grp1, 'LBL_ALREADY-EXISTS');
 console.log(s);


 ///============= hình ảnh 
   document.addEventListener("DOMContentLoaded", function() {
        var photoNewDiv = document.getElementById('photo_new');

        var imgElement = document.createElement('img');
        imgElement.id = 'photo_preview';
        imgElement.src = '';
        imgElement.alt = 'Image Preview';
        imgElement.style.display = 'none';
        imgElement.style.maxWidth = '250px';
        imgElement.style.maxHeight = '250px';

        var deleteButton = document.createElement('button');
        deleteButton.id = 'delete_photo';
        deleteButton.type = 'button';
        deleteButton.style.display = 'none';
        deleteButton.textContent = 'Xóa';

        photoNewDiv.appendChild(imgElement);
        photoNewDiv.appendChild(deleteButton);

        // Hiển thị ảnh đã chọn và nút xóa
        document.getElementById("photo_file").addEventListener("change", function() {
            const fileInput = document.getElementById('photo_file');
            const preview = document.getElementById('photo_preview');
            const deleteButton = document.getElementById('delete_photo');

            const file = fileInput.files[0];
            if (file) {
                const reader = new FileReader();
                reader.onload = function(e) {
                    preview.src = e.target.result;
                    preview.style.display = 'block';
                    deleteButton.style.display = 'inline';
                }
                reader.readAsDataURL(file);
            } else {
                preview.src = '';
                preview.style.display = 'none';
                deleteButton.style.display = 'none';
            }

            $("#photo").val($("#photo_file").val());
        });

        // Xóa ảnh và ẩn nút xóa
        document.getElementById("delete_photo").addEventListener("click", function() {
            const fileInput = document.getElementById('photo_file');
            const preview = document.getElementById('photo_preview');
            const deleteButton = document.getElementById('delete_photo');
            const removeButton = document.getElementById('remove_button');
            fileInput.value = '';
            preview.src = '';
            preview.style.display = 'none';
            deleteButton.style.display = 'none';

            $("#photo").val('');
        });
    });


  document.addEventListener("DOMContentLoaded", function() {
        var photoOldSpan = document.getElementById('photo_old');
        var anchorTag = photoOldSpan.querySelector('a');
        var removeButton = document.getElementById('remove_button');

        if (anchorTag) {
            var imgElement = document.createElement('img');
            imgElement.src = anchorTag.href;
            imgElement.alt = 'Image Preview';
            imgElement.style.maxWidth = '250px';
            imgElement.style.maxHeight = '250px';

            // Thay thế thẻ <a> bằng thẻ <img>
            photoOldSpan.insertBefore(imgElement, anchorTag);
            photoOldSpan.removeChild(anchorTag);
        }

        // Di chuyển nút "Xóa bỏ" xuống dưới hình ảnh
        if (removeButton) {
            removeButton.style.display = 'block';
            photoOldSpan.appendChild(removeButton);
        }
    });


  document.addEventListener("DOMContentLoaded", function() {
    var photoFileInput = document.getElementById('photo_file');
    var photoPreview = document.getElementById('photo_preview');
    var deleteButton = document.getElementById('delete_photo');
    
    // Nếu có hình ảnh hiện tại, hiển thị nó
    var photoCurrentDiv = document.getElementById('photo_current');
    if (photoCurrentDiv) {
        var imageSrc = photoCurrentDiv.getAttribute('data-image-src');
        if (imageSrc) {
            photoPreview.src = imageSrc;
            photoPreview.style.display = 'block';
            deleteButton.style.display = 'inline';
        }
    }

    // Hiển thị ảnh đã chọn và nút xóa
    photoFileInput.addEventListener("change", function() {
        const file = this.files[0];
        if (file) {
            const reader = new FileReader();
            reader.onload = function(e) {
                photoPreview.src = e.target.result;
                photoPreview.style.display = 'block';
                deleteButton.style.display = 'inline';
            }
            reader.readAsDataURL(file);
        } else {
            photoPreview.src = '';
            photoPreview.style.display = 'none';
            deleteButton.style.display = 'none';
        }
    });

    // Xử lý sự kiện khi nhấn nút xóa hình ảnh
    deleteButton.addEventListener("click", function() {
        photoFileInput.value = ''; // Xóa tệp hiện tại
        photoPreview.src = '';
        photoPreview.style.display = 'none';
        deleteButton.style.display = 'none';

        // Cập nhật giá trị trường ẩn với tên hình ảnh cần xóa
        var currentImageSrc = photoCurrentDiv.getAttribute('data-image-src');
        if (currentImageSrc) {
            var currentImageName = currentImageSrc.split('/').pop();
            var deletePhotoInput = document.getElementById('delete_photo_input');
            if (deletePhotoInput) {
                deletePhotoInput.value = currentImageName;
            }
        }
    });
});



 