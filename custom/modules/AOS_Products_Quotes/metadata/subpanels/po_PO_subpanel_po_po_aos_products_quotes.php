<?php
// created: 2021-03-27 04:50:01
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '45%',
    'default' => true,
  ),
  'date_modified' => 
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => '45%',
    'default' => true,
  ),
  'product_discount_amount' => 
  array (
    'type' => 'currency',
    'vname' => 'LBL_PRODUCT_DISCOUNT_AMOUNT',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
  ),
  'product_unit_price_usdollar' => 
  array (
    'type' => 'currency',
    'studio' => 
    array (
      'editview' => false,
      'detailview' => false,
      'quickcreate' => false,
    ),
    'vname' => 'LBL_PRODUCT_UNIT_PRICE_USDOLLAR',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
  ),
  'product_discount_usdollar' => 
  array (
    'type' => 'currency',
    'studio' => 
    array (
      'editview' => false,
      'detailview' => false,
      'quickcreate' => false,
    ),
    'vname' => 'LBL_PRODUCT_DISCOUNT_USDOLLAR',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
  ),
  'vat_amt_usdollar' => 
  array (
    'type' => 'currency',
    'studio' => 
    array (
      'editview' => false,
      'detailview' => false,
      'quickcreate' => false,
    ),
    'vname' => 'LBL_VAT_AMT_USDOLLAR',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
  ),
  'product_list_price_usdollar' => 
  array (
    'type' => 'currency',
    'studio' => 
    array (
      'editview' => false,
      'detailview' => false,
      'quickcreate' => false,
    ),
    'vname' => 'LBL_PRODUCT_LIST_PRICE_USDOLLAR',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
  ),
  'product_total_price' => 
  array (
    'type' => 'currency',
    'vname' => 'LBL_PRODUCT_TOTAL_PRICE',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
  ),
  'vat_amt' => 
  array (
    'type' => 'currency',
    'vname' => 'LBL_VAT_AMT',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
  ),
  'product_total_price_usdollar' => 
  array (
    'type' => 'currency',
    'studio' => 
    array (
      'editview' => false,
      'detailview' => false,
      'quickcreate' => false,
    ),
    'vname' => 'LBL_PRODUCT_TOTAL_PRICE_USDOLLAR',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
  ),
  'number' => 
  array (
    'type' => 'int',
    'vname' => 'LBL_LIST_NUM',
    'width' => '10%',
    'default' => true,
  ),
  'group_name' => 
  array (
    'type' => 'relate',
    'vname' => 'LBL_GROUP_NAME',
    'id' => 'GROUP_ID',
    'link' => true,
    'width' => '10%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'AOS_Line_Item_Groups',
    'target_record_key' => 'group_id',
  ),
  'vat' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_VAT',
    'width' => '10%',
  ),
  'discount' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_DISCOUNT',
    'width' => '10%',
  ),
  'product_unit_price' => 
  array (
    'type' => 'currency',
    'vname' => 'LBL_PRODUCT_UNIT_PRICE',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
  ),
  'product_discount_amount_usdollar' => 
  array (
    'type' => 'currency',
    'studio' => 
    array (
      'editview' => false,
      'detailview' => false,
      'quickcreate' => false,
    ),
    'vname' => 'LBL_PRODUCT_DISCOUNT_AMOUNT_USDOLLAR',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
  ),
  'product_discount' => 
  array (
    'type' => 'currency',
    'vname' => 'LBL_PRODUCT_DISCOUNT',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
  ),
  'product_list_price' => 
  array (
    'type' => 'currency',
    'vname' => 'LBL_PRODUCT_LIST_PRICE',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
  ),
  'product_cost_price' => 
  array (
    'type' => 'currency',
    'vname' => 'LBL_PRODUCT_COST_PRICE',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
  ),
  'product_cost_price_usdollar' => 
  array (
    'type' => 'currency',
    'studio' => 
    array (
      'editview' => false,
      'detailview' => false,
      'quickcreate' => false,
    ),
    'vname' => 'LBL_PRODUCT_COST_PRICE_USDOLLAR',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
  ),
  'product_qty' => 
  array (
    'type' => 'decimal',
    'vname' => 'LBL_PRODUCT_QTY',
    'width' => '10%',
    'default' => true,
  ),
  'edit_button' => 
  array (
    'widget_class' => 'SubPanelEditButton',
    'module' => 'AOS_Products_Quotes',
    'width' => '4%',
    'default' => true,
  ),
  'item_description' => 
  array (
    'type' => 'text',
    'vname' => 'LBL_PRODUCT_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => true,
  ),
  'remove_button' => 
  array (
    'widget_class' => 'SubPanelRemoveButton',
    'module' => 'AOS_Products_Quotes',
    'width' => '5%',
    'default' => true,
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'vname' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
  'modified_by_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_MODIFIED_NAME',
    'id' => 'MODIFIED_USER_ID',
    'width' => '10%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Users',
    'target_record_key' => 'modified_user_id',
  ),
  'created_by_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Users',
    'target_record_key' => 'created_by',
  ),
  'assigned_user_name' => 
  array (
    'link' => true,
    'type' => 'relate',
    'vname' => 'LBL_ASSIGNED_TO_NAME',
    'id' => 'ASSIGNED_USER_ID',
    'width' => '10%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Users',
    'target_record_key' => 'assigned_user_id',
  ),
  'description' => 
  array (
    'type' => 'text',
    'vname' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => true,
  ),
  'part_number' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_PART_NUMBER',
    'width' => '10%',
  ),
  'currency_id' => 
  array (
    'type' => 'id',
    'studio' => 'visible',
    'vname' => 'LBL_CURRENCY',
    'width' => '10%',
    'default' => true,
  ),
);