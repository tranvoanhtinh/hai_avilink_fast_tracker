<?php
// created: 2023-04-13 10:57:23
$mod_strings = array (
  'LBL_AOS_PRODUCTS' => 'Products',
  'LBL_AOS_QUOTES' => 'Quotes',
  'LBL_PO_PO_AOS_PRODUCTS_QUOTES_FROM_PO_PO_TITLE' => 'Purchase Order',
  'LBL_PARK_SERVICE' => 'Parkservice',
  'LBL_COMMISSION_SERVICE' => 'Commission',
);