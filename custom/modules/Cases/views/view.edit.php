<?php
require_once('include/MVC/View/views/view.edit.php');

class CustomCasesViewEdit extends ViewEdit
{
    function display()
    {
        parent::display(); // Gọi phương thức hiển thị từ lớp cha
        ?>
        <script>
            var id2 = '';
            function check_hidden_id() {
                var id = document.getElementById('account_id').value;
                if (id !== id2) {
                    display_phone('account_id', 'phoneaccount', 'accounts', 'phone_alternate', true);
                    id2 = id;
                }
            }
            setInterval(check_hidden_id, 300);
        </script>

        <?php
    }
}
?>
