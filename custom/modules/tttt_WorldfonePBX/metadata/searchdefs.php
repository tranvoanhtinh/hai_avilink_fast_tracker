<?php
$module_name = 'tttt_WorldfonePBX';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'description' => 
      array (
        'type' => 'text',
        'label' => 'LBL_DESCRIPTION',
        'sortable' => false,
        'width' => '10%',
        'default' => true,
        'name' => 'description',
      ),
      'is_status' => 
      array (
        'type' => 'bool',
        'default' => true,
        'label' => 'LBL_IS_STATUSS',
        'width' => '10%',
        'name' => 'is_status',
      ),
      'current_user_only' => 
      array (
        'name' => 'current_user_only',
        'label' => 'LBL_CURRENT_USER_FILTER',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
      ),
    ),
    'advanced_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'description' => 
      array (
        'type' => 'text',
        'label' => 'LBL_DESCRIPTION',
        'sortable' => false,
        'width' => '10%',
        'default' => true,
        'name' => 'description',
      ),
      'is_status' => 
      array (
        'type' => 'bool',
        'default' => true,
        'label' => 'LBL_IS_STATUSS',
        'width' => '10%',
        'name' => 'is_status',
      ),
      'status_c' => 
      array (
        'type' => 'bool',
        'default' => true,
        'label' => 'LBL_STATUS',
        'width' => '10%',
        'name' => 'status_c',
      ),
      'type_sys_app' => 
      array (
        'type' => 'enum',
        'label' => 'LBL_TYPE_SYS_APP',
        'width' => '10%',
        'default' => true,
        'name' => 'type_sys_app',
      ),
      'type_sys_partner' => 
      array (
        'type' => 'enum',
        'label' => 'LBL_TYPE_SYS_PARTNER',
        'width' => '10%',
        'default' => true,
        'name' => 'type_sys_partner',
      ),
      'is_sendzalo' => 
      array (
        'type' => 'bool',
        'default' => true,
        'label' => 'LBL_ISSENDZALO',
        'width' => '10%',
        'name' => 'is_sendzalo',
      ),
      'is_sendsms' => 
      array (
        'type' => 'bool',
        'default' => true,
        'label' => 'LBL_ISSENDSMS',
        'width' => '10%',
        'name' => 'is_sendsms',
      ),
      'campaignlead_c' => 
      array (
        'type' => 'relate',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_CAMPAIGN_LEAD',
        'id' => 'CAMPAIGN_ID_C',
        'link' => true,
        'width' => '10%',
        'name' => 'campaignlead_c',
      ),
      'assigned_user_id' => 
      array (
        'name' => 'assigned_user_id',
        'label' => 'LBL_ASSIGNED_TO',
        'type' => 'enum',
        'function' => 
        array (
          'name' => 'get_user_array',
          'params' => 
          array (
            0 => false,
          ),
        ),
        'default' => true,
        'width' => '10%',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
;
?>
