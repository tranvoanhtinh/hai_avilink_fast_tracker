<?php
$module_name = 'tttt_WorldfonePBX';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 'assigned_user_name',
        ),
        1 => 
        array (
          0 => 'description',
          1 => 
          array (
            'name' => 'is_status',
            'label' => 'LBL_IS_STATUSS',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'is_sendzalo',
            'label' => 'LBL_ISSENDZALO',
          ),
          1 => 
          array (
            'name' => 'is_sendsms',
            'label' => 'LBL_ISSENDSMS',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'type_sys_app',
            'label' => 'LBL_TYPE_SYS_APP',
          ),
          1 => 
          array (
            'name' => 'phone_work',
            'label' => 'LBL_PHONE_WORK',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'psw_callcenter',
            'label' => 'LBL_PASSWORD',
          ),
          1 => 
          array (
            'name' => 'calluuid',
            'label' => 'LBL_CALLUUID',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'campaignlead_c',
            'studio' => 'visible',
            'label' => 'LBL_CAMPAIGN_LEAD',
          ),
          1 => 
          array (
            'name' => 'type_sys_partner',
            'label' => 'LBL_TYPE_SYS_PARTNER',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'companylogo_c',
            'studio' => 'visible',
            'label' => 'LBL_COMPANY_LOGO',
          ),
          1 => 
          array (
            'name' => 'company_name',
            'label' => 'LBL_COMPANY_NAME',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'company_sologun',
            'label' => 'LBL_COMPANY_SOLOGUN',
          ),
          1 => 
          array (
            'name' => 'company_website',
            'label' => 'LBL_COMPANY_WEBSITE',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'company_address',
            'label' => 'LBL_COMPANY_ADDRESS',
          ),
          1 => 
          array (
            'label' => 'LBL_BANKINGINFO',
            'name' => 'bankinginfo',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'label' => 'LBL_BANKACCOUNT',
            'name' => 'bankaccount',
          ),
          1 => 
          array (
            'label' => 'LBL_BANKNAME',
            'name' => 'bankname',
          ),
        ),
        10 => 
        array (
          0 => 
          array (
            'label' => 'LBL_BANKADDRESS',
            'name' => 'bankaddress',
          ),
          1 => 
          array (
            'name' => 'status_c',
            'label' => 'LBL_STATUS',
          ),
        ),
        11 => 
        array (
          0 => 
          array (
            'label' => 'LBL_BANKNO',
            'name' => 'bankno',
          ),
          1 => 
          array (
            'label' => 'LBL_SWIFTCODE',
            'name' => 'swiftcode',
          ),
        ),
        12 => 
        array (
          0 => 
          array (
            'name' => 'api_domain',
            'label' => 'LBL_API_DOMAIN',
          ),
          1 => 
          array (
            'name' => 'outbound_server',
            'label' => 'LBL_OUTBOUND_SERVER',
          ),
        ),
        13 => 
        array (
          0 => 
          array (
            'name' => 'wss_url',
            'label' => 'LBL_WSS_URL',
          ),
          1 => 
          array (
            'name' => 'qrcode_c',
            'studio' => 'visible',
            'label' => 'LBL_QR_CODE',
          ),
        ),
      ),
    ),
  ),
);
;
?>
