<?php
$module_name = 'tttt_WorldfonePBX';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => true,
  ),
  'IS_STATUS' => 
  array (
    'type' => 'bool',
    'default' => true,
    'label' => 'LBL_IS_STATUSS',
    'width' => '10%',
  ),
  'TYPE_SYS_APP' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_TYPE_SYS_APP',
    'width' => '10%',
    'default' => true,
  ),
  'TYPE_SYS_PARTNER' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_TYPE_SYS_PARTNER',
    'width' => '10%',
    'default' => true,
  ),
  'STATUS_C' => 
  array (
    'type' => 'bool',
    'default' => true,
    'label' => 'LBL_STATUS',
    'width' => '10%',
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => false,
  ),
  'QRCODE_C' => 
  array (
    'type' => 'image',
    'default' => false,
    'studio' => 'visible',
    'width' => '10%',
    'label' => 'LBL_QR_CODE',
  ),
  'IS_SENDZALO' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'LBL_ISSENDZALO',
    'width' => '10%',
  ),
  'WSS_URL' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_WSS_URL',
    'width' => '10%',
    'default' => false,
  ),
  'SWIFTCODE' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_SWIFTCODE',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'COMPANY_SOLOGUN' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_COMPANY_SOLOGUN',
    'width' => '10%',
    'default' => false,
  ),
  'IS_SENDSMS' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'LBL_ISSENDSMS',
    'width' => '10%',
  ),
  'COMPANY_WEBSITE' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_COMPANY_WEBSITE',
    'width' => '10%',
    'default' => false,
  ),
  'COMPANY_ADDRESS' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_COMPANY_ADDRESS',
    'width' => '10%',
    'default' => false,
  ),
  'COMPANY_NAME' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_COMPANY_NAME',
    'width' => '10%',
    'default' => false,
  ),
  'COMPANYLOGO_C' => 
  array (
    'type' => 'image',
    'default' => false,
    'studio' => 'visible',
    'width' => '10%',
    'label' => 'LBL_COMPANY_LOGO',
  ),
  'CREATED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'default' => false,
  ),
  'OUTBOUND_SERVER' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_OUTBOUND_SERVER',
    'width' => '10%',
    'default' => false,
  ),
  'CAMPAIGNLEAD_C' => 
  array (
    'type' => 'relate',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_CAMPAIGN_LEAD',
    'id' => 'CAMPAIGN_ID_C',
    'link' => true,
    'width' => '10%',
  ),
  'BANKNO' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_BANKNO',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'BANKNAME' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_BANKNAME',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'BANKINGINFO' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_BANKINGINFO',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'BANKADDRESS' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_BANKADDRESS',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'BANKACCOUNT' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_BANKACCOUNT',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'API_DOMAIN' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_API_DOMAIN',
    'width' => '10%',
    'default' => false,
  ),
  'PHONE_WORK' => 
  array (
    'type' => 'phone',
    'label' => 'LBL_PHONE_WORK',
    'width' => '10%',
    'default' => false,
  ),
  'CALLUUID' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_CALLUUID',
    'width' => '10%',
    'default' => false,
  ),
  'PSW_CALLCENTER' => 
  array (
    'type' => 'name',
    'label' => 'LBL_PASSWORD',
    'width' => '10%',
    'default' => false,
  ),
);
;
?>
