<?php
$listViewDefs ['AOS_Invoices'] = 
array (
  'NAME' => 
  array (
    'width' => '15%',
    'label' => 'LBL_ACCOUNT_NAME',
    'link' => true,
    'default' => true,
  ),
  'INVOICE_DATE' => 
  array (
    'type' => 'date',
    'label' => 'LBL_INVOICE_DATE',
    'width' => '10%',
    'default' => true,
  ),
  'STATUS_INVENTORY' => 
  array (
    'width' => '10%',
    'label' => 'LBL_STATUS_INVENTORY',
    'default' => true,
  ),
  'BILLING_ACCOUNT' => 
  array (
    'width' => '15%',
    'label' => 'LBL_BILLING_ACCOUNT',
    'default' => true,
    'module' => 'Accounts',
    'id' => 'BILLING_ACCOUNT_ID',
    'link' => true,
    'related_fields' => 
    array (
      0 => 'billing_account_id',
    ),
  ),
  'MOBILEREF' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_MOBILE_REF',
    'width' => '10%',
    'default' => true,
  ),
  'TOTAL_AMOUNT' => 
  array (
    'width' => '10%',
    'label' => 'LBL_GRAND_TOTAL',
    'default' => true,
    'currency_format' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '10%',
    'label' => 'LBL_ASSIGNED_USER',
    'default' => true,
    'module' => 'Users',
    'id' => 'ASSIGNED_USER_ID',
    'link' => true,
    'related_fields' => 
    array (
      0 => 'assigned_user_id',
    ),
  ),
  'DATE_ENTERED' => 
  array (
    'width' => '5%',
    'label' => 'LBL_DATE_ENTERED',
    'default' => true,
  ),
  'BILLING_CONTACT' => 
  array (
    'width' => '11%',
    'label' => 'LBL_BILLING_CONTACT',
    'default' => false,
    'module' => 'Contacts',
    'id' => 'BILLING_CONTACT_ID',
    'link' => true,
    'related_fields' => 
    array (
      0 => 'billing_contact_id',
    ),
  ),
  'TOTAL_AMT' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_TOTAL_AMT',
    'currency_format' => true,
    'width' => '10%',
    'default' => false,
  ),
  'SUBTOTAL_AMOUNT' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_SUBTOTAL_AMOUNT',
    'currency_format' => true,
    'width' => '10%',
    'default' => false,
  ),
  'CURRENCY_ID' => 
  array (
    'type' => 'id',
    'studio' => 'visible',
    'label' => 'LBL_CURRENCY',
    'width' => '10%',
    'default' => false,
  ),
  'TAX_AMOUNT' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_TAX_AMOUNT',
    'currency_format' => true,
    'width' => '10%',
    'default' => false,
  ),
  'SHIPPING_AMOUNT_USDOLLAR' => 
  array (
    'type' => 'currency',
    'studio' => 
    array (
      'editview' => false,
      'detailview' => false,
      'quickcreate' => false,
    ),
    'label' => 'LBL_SHIPPING_AMOUNT_USDOLLAR',
    'currency_format' => true,
    'width' => '10%',
    'default' => false,
  ),
  'NUMBER' => 
  array (
    'width' => '5%',
    'label' => 'LBL_LIST_NUM',
    'default' => false,
  ),
  'ADVANCE_PAYMENT' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_ADVANCE_PAYMENT',
    'currency_format' => true,
    'width' => '10%',
    'default' => false,
  ),
  'SHIPPING_AMOUNT' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_SHIPPING_AMOUNT',
    'currency_format' => true,
    'width' => '10%',
    'default' => false,
  ),
  'REMAINING_MONEY' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_REMAINING_MONEY',
    'currency_format' => true,
    'width' => '10%',
    'default' => false,
  ),
  'TOTAL_DISCOUNT_AMOUNT' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_TOTAL_DISCOUNT_AMOUNT',
    'currency_format' => true,
    'width' => '10%',
    'default' => false,
  ),
  'STATUS' => 
  array (
    'width' => '10%',
    'label' => 'LBL_STATUS',
    'default' => false,
  ),
  'STATUSTRANSPORT' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_STATUSTRANSPORT',
    'width' => '10%',
    'default' => false,
  ),
  'CASHIER' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_CASHIER',
    'width' => '10%',
    'default' => false,
  ),
  'DUE_DATE' => 
  array (
    'width' => '10%',
    'label' => 'LBL_DUE_DATE',
    'default' => false,
  ),
  'MODIFIED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_MODIFIED_NAME',
    'id' => 'MODIFIED_USER_ID',
    'width' => '10%',
    'default' => false,
  ),
  'CREATED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'default' => false,
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => false,
  ),
  'IS_NON_COMMERCIAL' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'LBL_IS_NON_COMMERCIAL',
    'width' => '10%',
  ),
  'DISCOUNT_AMOUNT' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_DISCOUNT_AMOUNT',
    'currency_format' => true,
    'width' => '10%',
    'default' => false,
  ),
  'KEY_INVOICEREF' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_KEY_INVOICE_REF',
    'width' => '10%',
    'default' => false,
  ),
  'TYPE_INVOICE' => 
  array (
    'type' => 'enum',
    'studio' => true,
    'label' => 'LBL_TYPE_INVOICE',
    'width' => '10%',
    'default' => false,
  ),
  'SHIPINGFEEAVG' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_SHIPPING_FEE_AVG',
    'currency_format' => true,
    'width' => '10%',
    'default' => false,
  ),
  'STATUS_INVOICE_REF' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_STATUS_INVOICE_REF',
    'width' => '10%',
    'default' => false,
  ),
  'IS_COMMERCIAL' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'LBL_IS_COMMERCIAL',
    'width' => '10%',
  ),
  'RETURN_AMOUNT' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_RETURN_AMOUNT',
    'currency_format' => true,
    'width' => '10%',
    'default' => false,
  ),
  'PI_EFFECTIVE_DATE' => 
  array (
    'type' => 'date',
    'label' => 'LBL_PI_EFFECTIVE_DATE',
    'width' => '10%',
    'default' => false,
  ),
  'DISCOUNT_PERCENT' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_DISCOUNT_PERCENT',
    'width' => '10%',
    'default' => false,
  ),
  'START_DATE' => 
  array (
    'type' => 'datetimecombo',
    'label' => 'LBL_START_DATE',
    'width' => '10%',
    'default' => false,
  ),
  'RECEIVE_AMOUNT' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_RECEIVE_AMOUNT',
    'currency_format' => true,
    'width' => '10%',
    'default' => false,
  ),
  'CUSTOMER_REF' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_CUSTOMER_REF',
    'width' => '10%',
    'default' => false,
  ),
  'INVOICENO' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_INVOICE_NO',
    'width' => '10%',
    'default' => false,
  ),
  'ANNUAL_REVENUE' => 
  array (
    'width' => '10%',
    'label' => 'LBL_ANNUAL_REVENUE',
    'default' => false,
  ),
  'SUMMARY_PRODUCT' => 
  array (
    'type' => 'text',
    'label' => 'LBL_SUMMARY_PRODUCTS',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'DELIVERY_DATE' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DELIVERY_DATE',
    'width' => '10%',
    'default' => false,
  ),
  'ESTIMATED_DELIVERY_DATE' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_ESTIMATED_DELIVERY_DATE',
    'width' => '10%',
    'default' => false,
  ),
  'ADS' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ADS',
    'width' => '10%',
    'default' => false,
  ),
  'TRADE_TERM' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_TRADE_TERM',
    'width' => '10%',
    'default' => false,
  ),
  'TAKE_FROM' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_TAKE_FROM',
    'width' => '10%',
    'default' => false,
  ),
  'BILLING_ADDRESS_STREET' => 
  array (
    'width' => '15%',
    'label' => 'LBL_BILLING_ADDRESS_STREET',
    'default' => false,
  ),
  'SHIPPING_METHOD' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_SHIPPING_METHOD',
    'width' => '10%',
    'default' => false,
  ),
  'PAYMENT_DATE' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_PAYMENT_DATE',
    'width' => '10%',
    'default' => false,
  ),
  'IS_INVOICE' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'LBL_ISINVOICE',
    'width' => '10%',
  ),
  'BILLING_ADDRESS_CITY' => 
  array (
    'width' => '10%',
    'label' => 'LBL_CITY',
    'default' => false,
  ),
  'QUOTE_NUMBER' => 
  array (
    'type' => 'int',
    'label' => 'LBL_QUOTE_NUMBER',
    'width' => '10%',
    'default' => false,
  ),
  'PAYMENTMETHOD' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_PAYMENTMETHOD',
    'width' => '10%',
    'default' => false,
  ),
  'QUOTE_DATE' => 
  array (
    'type' => 'date',
    'label' => 'LBL_QUOTE_DATE',
    'width' => '10%',
    'default' => false,
  ),
  'DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'BILLING_ADDRESS_STATE' => 
  array (
    'width' => '7%',
    'label' => 'LBL_BILLING_ADDRESS_STATE',
    'default' => false,
  ),
  'BILLING_ADDRESS_POSTALCODE' => 
  array (
    'width' => '10%',
    'label' => 'LBL_BILLING_ADDRESS_POSTALCODE',
    'default' => false,
  ),
  'BILLING_ADDRESS_COUNTRY' => 
  array (
    'width' => '10%',
    'label' => 'LBL_BILLING_ADDRESS_COUNTRY',
    'default' => false,
  ),
  'SHIPPING_ADDRESS_STREET' => 
  array (
    'width' => '15%',
    'label' => 'LBL_SHIPPING_ADDRESS_STREET',
    'default' => false,
  ),
  'SHIPPING_ADDRESS_CITY' => 
  array (
    'width' => '10%',
    'label' => 'LBL_SHIPPING_ADDRESS_CITY',
    'default' => false,
  ),
  'SHIPPING_ADDRESS_STATE' => 
  array (
    'width' => '7%',
    'label' => 'LBL_SHIPPING_ADDRESS_STATE',
    'default' => false,
  ),
  'SHIPPING_ADDRESS_POSTALCODE' => 
  array (
    'width' => '10%',
    'label' => 'LBL_SHIPPING_ADDRESS_POSTALCODE',
    'default' => false,
  ),
  'SHIPPING_ADDRESS_COUNTRY' => 
  array (
    'width' => '10%',
    'label' => 'LBL_SHIPPING_ADDRESS_COUNTRY',
    'default' => false,
  ),
  'PHONE_ALTERNATE' => 
  array (
    'width' => '10%',
    'label' => 'LBL_PHONE_ALT',
    'default' => false,
  ),
  'WEBSITE' => 
  array (
    'width' => '10%',
    'label' => 'LBL_WEBSITE',
    'default' => false,
  ),
  'OWNERSHIP' => 
  array (
    'width' => '10%',
    'label' => 'LBL_OWNERSHIP',
    'default' => false,
  ),
  'EMPLOYEES' => 
  array (
    'width' => '10%',
    'label' => 'LBL_EMPLOYEES',
    'default' => false,
  ),
  'TICKER_SYMBOL' => 
  array (
    'width' => '10%',
    'label' => 'LBL_TICKER_SYMBOL',
    'default' => false,
  ),
);
;
?>
