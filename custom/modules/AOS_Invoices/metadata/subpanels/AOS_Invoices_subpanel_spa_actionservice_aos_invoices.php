<?php
// created: 2024-04-12 06:48:39
$subpanel_layout['list_fields'] = array (
  'action_date' => 
  array (
    'type' => 'datetimecombo',
    'vname' => 'LBL_ACTION_DATE',
    'width' => '10%',
    'default' => true,
  ),
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '45%',
    'default' => true,
  ),
  'accounts_spa_actionservice_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_ACCOUNTS_SPA_ACTIONSERVICE_1_FROM_ACCOUNTS_TITLE',
    'id' => 'ACCOUNTS_SPA_ACTIONSERVICE_1ACCOUNTS_IDA',
    'width' => '10%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Accounts',
    'target_record_key' => 'accounts_spa_actionservice_1accounts_ida',
  ),
  'mobileref' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_MOBILE_REF',
    'width' => '10%',
    'default' => true,
  ),
  'status' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'vname' => 'LBL_STATUS',
    'width' => '10%',
    'default' => true,
  ),
  'amountservice' => 
  array (
    'type' => 'currency',
    'default' => true,
    'vname' => 'LBL_AMOUNT_SERVICE',
    'currency_format' => true,
    'width' => '10%',
  ),
  'commission_action' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_COMMISSION_ACTION',
    'width' => '10%',
  ),
  'spa_actionservice_cs_cususer_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_SPA_ACTIONSERVICE_CS_CUSUSER_FROM_CS_CUSUSER_TITLE',
    'id' => 'SPA_ACTIONSERVICE_CS_CUSUSERCS_CUSUSER_IDA',
    'width' => '10%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'cs_cusUser',
    'target_record_key' => 'spa_actionservice_cs_cususercs_cususer_ida',
  ),
  'edit_button' => 
  array (
    'vname' => 'LBL_EDIT_BUTTON',
    'widget_class' => 'SubPanelEditButton',
    'module' => 'spa_ActionService',
    'width' => '4%',
    'default' => true,
  ),
  'remove_button' => 
  array (
    'vname' => 'LBL_REMOVE',
    'widget_class' => 'SubPanelRemoveButton',
    'module' => 'spa_ActionService',
    'width' => '5%',
    'default' => true,
  ),
);