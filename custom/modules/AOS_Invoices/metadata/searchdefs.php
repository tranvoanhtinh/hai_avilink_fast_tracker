<?php
$module_name = 'AOS_Invoices';
$_module_name = 'aos_invoices';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'billing_account' => 
      array (
        'type' => 'relate',
        'studio' => 'visible',
        'default' => true,
        'width' => '10%',
        'label' => 'LBL_BILLING_ACCOUNT',
        'id' => 'BILLING_ACCOUNT_ID',
        'link' => true,
        'name' => 'billing_account',
      ),
      'mobileref' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_MOBILE_REF',
        'width' => '10%',
        'default' => true,
        'name' => 'mobileref',
      ),
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
    ),
    'advanced_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'invoice_date' => 
      array (
        'type' => 'date',
        'label' => 'LBL_INVOICE_DATE',
        'width' => '10%',
        'default' => true,
        'name' => 'invoice_date',
      ),
      'billing_account' => 
      array (
        'name' => 'billing_account',
        'default' => true,
        'width' => '10%',
      ),
      'status' => 
      array (
        'name' => 'status',
        'default' => true,
        'width' => '10%',
      ),
      'trade_term' => 
      array (
        'type' => 'enum',
        'label' => 'LBL_TRADE_TERM',
        'width' => '10%',
        'default' => true,
        'name' => 'trade_term',
      ),
      'customer_ref' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_CUSTOMER_REF',
        'width' => '10%',
        'default' => true,
        'name' => 'customer_ref',
      ),
      'type_invoice' => 
      array (
        'type' => 'enum',
        'label' => 'LBL_TYPE_INVOICE',
        'width' => '10%',
        'default' => true,
        'name' => 'type_invoice',
      ),
      'is_commercial' => 
      array (
        'type' => 'bool',
        'default' => true,
        'label' => 'LBL_IS_COMMERCIAL',
        'width' => '10%',
        'name' => 'is_commercial',
      ),
      'is_non_commercial' => 
      array (
        'type' => 'bool',
        'default' => true,
        'label' => 'LBL_IS_NON_COMMERCIAL',
        'width' => '10%',
        'name' => 'is_non_commercial',
      ),
      'pi_effective_date' => 
      array (
        'type' => 'date',
        'label' => 'LBL_PI_EFFECTIVE_DATE',
        'width' => '10%',
        'default' => true,
        'name' => 'pi_effective_date',
      ),
      'take_from' => 
      array (
        'type' => 'enum',
        'label' => 'LBL_TAKE_FROM',
        'width' => '10%',
        'default' => true,
        'name' => 'take_from',
      ),
      'ads' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_ADS',
        'width' => '10%',
        'default' => true,
        'name' => 'ads',
      ),
      'assigned_user_id' => 
      array (
        'name' => 'assigned_user_id',
        'type' => 'enum',
        'label' => 'LBL_ASSIGNED_TO',
        'function' => 
        array (
          'name' => 'get_user_array',
          'params' => 
          array (
            0 => false,
          ),
        ),
        'default' => true,
        'width' => '10%',
      ),
      'modified_user_id' => 
      array (
        'type' => 'assigned_user_name',
        'label' => 'LBL_MODIFIED',
        'width' => '10%',
        'default' => true,
        'name' => 'modified_user_id',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
;
?>
