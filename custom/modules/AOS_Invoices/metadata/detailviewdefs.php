<?php
$module_name = 'AOS_Invoices';
$_object_name = 'aos_invoices';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
          4 => 
          array (
            'customCode' => '<input type="button" class="button" onClick="showPopup(\'pdf\');" value="{$MOD.LBL_PRINT_AS_PDF}">',
          ),
          5 => 
          array (
            'customCode' => '<input type="button" class="button" onClick="showPopup(\'emailpdf\');" value="{$MOD.LBL_EMAIL_PDF}">',
          ),
          6 => 
          array (
            'customCode' => '<input type="button" class="button" onClick="showPopup(\'email\');" value="{$MOD.LBL_EMAIL_INVOICE}">',
          ),
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => true,
      'tabDefs' => 
      array (
        'LBL_INVOICE_TO' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_LINE_ITEMS' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_OVERVIEW' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'LBL_INVOICE_TO' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'billing_account',
            'label' => 'LBL_BILLING_ACCOUNT',
          ),
          1 => 
          array (
            'name' => 'mobileref',
            'label' => 'LBL_MOBILE_REF',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'billing_contact',
            'label' => 'LBL_BILLING_CONTACT',
          ),
          1 => 
          array (
            'name' => 'take_from',
            'label' => 'LBL_TAKE_FROM',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'invoice_date',
            'label' => 'LBL_INVOICE_DATE',
          ),
          1 => 
          array (
            'name' => 'payment_date',
            'label' => 'LBL_PAYMENT_DATE',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'type_invoice',
            'studio' => true,
            'label' => 'LBL_TYPE_INVOICE',
          ),
          1 => 
          array (
            'name' => 'is_invoice',
            'label' => 'LBL_ISINVOICE',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'start_date',
            'label' => 'LBL_START_DATE',
          ),
          1 => 
          array (
            'name' => 'due_date',
            'label' => 'LBL_DUE_DATE',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'status',
            'label' => 'LBL_STATUS',
          ),
          1 => 
          array (
            'name' => 'paymentmethod',
            'label' => 'LBL_PAYMENTMETHOD',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'status_inventory',
            'studio' => true,
            'label' => 'LBL_STATUS_INVENTORY',
          ),
          1 => '',
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'billing_address_street',
            'label' => 'LBL_BILLING_ADDRESS',
            'type' => 'address',
            'displayParams' => 
            array (
              'key' => 'billing',
            ),
          ),
          1 => 
          array (
            'name' => 'shipping_address_street',
            'label' => 'LBL_SHIPPING_ADDRESS',
            'type' => 'address',
            'displayParams' => 
            array (
              'key' => 'shipping',
            ),
          ),
        ),
      ),
      'lbl_line_items' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'currency_id',
            'studio' => 'visible',
            'label' => 'LBL_CURRENCY',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'line_items',
            'label' => 'LBL_LINE_ITEMS',
          ),
        ),
        2 => 
        array (
          0 => '',
          1 => '',
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'total_amt',
            'label' => 'LBL_TOTAL_AMT',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'discount_amount',
            'label' => 'LBL_DISCOUNT_AMOUNT',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'discount_percent',
            'label' => 'LBL_DISCOUNT_PERCENT',
          ),
          1 => 
          array (
            'name' => 'total_discount_amount',
            'label' => 'LBL_TOTAL_DISCOUNT_AMOUNT',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'subtotal_amount',
            'label' => 'LBL_SUBTOTAL_AMOUNT',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'shipping_amount',
            'label' => 'LBL_SHIPPING_AMOUNT',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'shipping_tax_amt',
            'label' => 'LBL_SHIPPING_TAX_AMT',
          ),
          1 => 
          array (
            'name' => 'shipingfeeavg',
            'label' => 'LBL_SHIPPING_FEE_AVG',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'tax_amount',
            'label' => 'LBL_TAX_AMOUNT',
          ),
        ),
        10 => 
        array (
          0 => 
          array (
            'name' => 'total_amount',
            'label' => 'LBL_GRAND_TOTAL',
          ),
        ),
        11 => 
        array (
          0 => 
          array (
            'name' => 'advance_payment',
            'label' => 'LBL_ADVANCE_PAYMENT',
          ),
          1 => 
          array (
            'name' => 'remaining_money',
            'label' => 'LBL_REMAINING_MONEY',
          ),
        ),
      ),
      'LBL_PANEL_OVERVIEW' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'name',
            'label' => 'LBL_NAME',
          ),
          1 => 
          array (
            'name' => 'pi_effective_date',
            'label' => 'LBL_PI_EFFECTIVE_DATE',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'cashier',
            'label' => 'LBL_CASHIER',
          ),
          1 => 
          array (
            'name' => 'shipping_method',
            'label' => 'LBL_SHIPPING_METHOD',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'delivery_date',
            'label' => 'LBL_DELIVERY_DATE',
          ),
          1 => 
          array (
            'name' => 'estimated_delivery_date',
            'label' => 'LBL_ESTIMATED_DELIVERY_DATE',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'customer_ref',
            'label' => 'LBL_CUSTOMER_REF',
          ),
          1 => 
          array (
            'name' => 'quote_date',
            'label' => 'LBL_QUOTE_DATE',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'statustransport',
            'label' => 'LBL_STATUSTRANSPORT',
          ),
          1 => 
          array (
            'name' => 'assigned_user_name',
            'label' => 'LBL_ASSIGNED_TO_NAME',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'is_commercial',
            'label' => 'LBL_IS_COMMERCIAL',
          ),
          1 => 
          array (
            'name' => 'ads',
            'label' => 'LBL_ADS',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'trade_term',
            'label' => 'LBL_TRADE_TERM',
          ),
          1 => 
          array (
            'name' => 'is_non_commercial',
            'label' => 'LBL_IS_NON_COMMERCIAL',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'description',
            'label' => 'LBL_DESCRIPTION',
          ),
          1 => 
          array (
            'name' => 'summary_product',
            'label' => 'LBL_SUMMARY_PRODUCTS',
          ),
        ),
      ),
    ),
  ),
);
;
?>
