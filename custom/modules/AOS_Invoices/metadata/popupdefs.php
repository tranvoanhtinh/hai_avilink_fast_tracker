<?php
$popupMeta = array (
    'moduleMain' => 'AOS_Invoices',
    'varName' => 'AOS_Invoices',
    'orderBy' => 'aos_invoices.name',
    'whereClauses' => array (
  'name' => 'aos_invoices.name',
  'invoice_date' => 'aos_invoices.invoice_date',
  'billing_account' => 'aos_invoices.billing_account',
  'status' => 'aos_invoices.status',
  'trade_term' => 'aos_invoices.trade_term',
  'customer_ref' => 'aos_invoices.customer_ref',
  'type_invoice' => 'aos_invoices.type_invoice',
  'is_commercial' => 'aos_invoices.is_commercial',
  'is_non_commercial' => 'aos_invoices.is_non_commercial',
  'pi_effective_date' => 'aos_invoices.pi_effective_date',
  'take_from' => 'aos_invoices.take_from',
  'ads' => 'aos_invoices.ads',
  'assigned_user_id' => 'aos_invoices.assigned_user_id',
  'modified_user_id' => 'aos_invoices.modified_user_id',
),
    'searchInputs' => array (
  0 => 'name',
  4 => 'invoice_date',
  5 => 'billing_account',
  6 => 'status',
  7 => 'trade_term',
  8 => 'customer_ref',
  9 => 'type_invoice',
  10 => 'is_commercial',
  11 => 'is_non_commercial',
  12 => 'pi_effective_date',
  13 => 'take_from',
  14 => 'ads',
  15 => 'assigned_user_id',
  16 => 'modified_user_id',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => '10%',
  ),
  'invoice_date' => 
  array (
    'type' => 'date',
    'label' => 'LBL_INVOICE_DATE',
    'width' => '10%',
    'name' => 'invoice_date',
  ),
  'billing_account' => 
  array (
    'name' => 'billing_account',
    'width' => '10%',
  ),
  'status' => 
  array (
    'name' => 'status',
    'width' => '10%',
  ),
  'trade_term' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_TRADE_TERM',
    'width' => '10%',
    'name' => 'trade_term',
  ),
  'customer_ref' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_CUSTOMER_REF',
    'width' => '10%',
    'name' => 'customer_ref',
  ),
  'type_invoice' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_TYPE_INVOICE',
    'width' => '10%',
    'name' => 'type_invoice',
  ),
  'is_commercial' => 
  array (
    'type' => 'bool',
    'label' => 'LBL_IS_COMMERCIAL',
    'width' => '10%',
    'name' => 'is_commercial',
  ),
  'is_non_commercial' => 
  array (
    'type' => 'bool',
    'label' => 'LBL_IS_NON_COMMERCIAL',
    'width' => '10%',
    'name' => 'is_non_commercial',
  ),
  'pi_effective_date' => 
  array (
    'type' => 'date',
    'label' => 'LBL_PI_EFFECTIVE_DATE',
    'width' => '10%',
    'name' => 'pi_effective_date',
  ),
  'take_from' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_TAKE_FROM',
    'width' => '10%',
    'name' => 'take_from',
  ),
  'ads' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ADS',
    'width' => '10%',
    'name' => 'ads',
  ),
  'assigned_user_id' => 
  array (
    'name' => 'assigned_user_id',
    'type' => 'enum',
    'label' => 'LBL_ASSIGNED_TO',
    'function' => 
    array (
      'name' => 'get_user_array',
      'params' => 
      array (
        0 => false,
      ),
    ),
    'width' => '10%',
  ),
  'modified_user_id' => 
  array (
    'type' => 'assigned_user_name',
    'label' => 'LBL_MODIFIED',
    'width' => '10%',
    'name' => 'modified_user_id',
  ),
),
);
