{*

 **
 * Asterisk SugarCRM Integration 
 * (c) KINAMU Business Solutions AG 2009
 * 
 * Parts of this code are (c) 2006. RustyBrick, Inc.  http://www.rustybrick.com/
 * Parts of this code are (c) 2008 vertico software GmbH  
 * Parts of this code are (c) 2009 abcona e. K. Angelo Malaguarnera E-Mail admin@abcona.de
 * http://www.sugarforge.org/projects/yaai/
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact KINAMU Business Solutions AG at office@kinamu.com
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU General Public License version 3.
 * 
 *
*}
<script type='text/javascript' src='include/javascript/overlibmws.js'></script>
<BR>
<form name="ConfigureSettings" enctype='multipart/form-data' method="POST" action="index.php" onSubmit="return (add_checks(document.ConfigureSettings) && check_form('ConfigureSettings'));">
<input type='hidden' name='action' value='asterisk_configurator'/>
<input type='hidden' name='module' value='Configurator'/>
<span class='error'>{$error.main}</span>
<table width="100%" cellpadding="0" cellspacing="0" border="0">
<tr>
			
	<td style="padding-bottom: 2px;">
		<input title="{$APP.LBL_SAVE_BUTTON_TITLE}" accessKey="{$APP.LBL_SAVE_BUTTON_KEY}" class="button"  type="submit"  name="save" value="  {$APP.LBL_SAVE_BUTTON_LABEL}  " >
		&nbsp;<input title="{$MOD.LBL_SAVE_BUTTON_TITLE}"  class="button"  type="submit" name="restore" value="  {$MOD.LBL_RESTORE_BUTTON_LABEL}  " > 
		&nbsp;<input title="{$MOD.LBL_CANCEL_BUTTON_TITLE}"  onclick="document.location.href='index.php?module=Administration&action=index'" class="button"  type="button" name="cancel" value="  {$APP.LBL_CANCEL_BUTTON_LABEL}  " > 
	</td>	
	</tr>
<tr><td>
<br>
<table width="100%" border="0" cellspacing="0" style="margin: 22px;" cellpadding="0" class="tabForm">
	<tr><th align="left" class="dataLabel" colspan="4"><h4 class="dataLabel">{$MOD.LBL_MANAGE_ASTERISK}</h4></th>
	</tr><tr>
<td> 
	<table width="100%" border="0" style="margin: 22px;" cellspacing="0" cellpadding="0">
	
	<tr> 
		<td nowrap width="10%" class="dataLabel">PBX URL: </td>
		<td width="25%" class="dataField">
		{if empty($config.asterisk_host )}
			{assign var='asterisk_host' value=$asterisk_config.asterisk_host}
		{else}
			{assign var='asterisk_host' value=$config.asterisk_host}
		{/if}
			<input type='text' name='asterisk_host' size="45" value='{$asterisk_host}'>
		</td>
		<td nowrap width="10%" class="dataLabel">SECRET KEY: </td>
		<td width="25%" class="dataField">
		{if empty($config.asterisk_port )}
			{assign var='asterisk_port' value=$asterisk_config.asterisk_port}
		{else}
			{assign var='asterisk_port' value=$config.asterisk_port}
		{/if}
			<input type='text' name='asterisk_port' size="45" value='{$asterisk_port}'>
		</td>
	</tr>
	<tr></tr>
	
</table>

	<table width="100%" border="0" style="margin: 22px;" cellspacing="0" cellpadding="0">
	
	<tr> 
		<td nowrap width="10%" class="dataLabel">Default Popup: </td>
		<td width="25%" class="dataField">
		{if empty($config.defaultpopup )}
			{assign var='defaultpopup' value=$asterisk_config.defaultpopup}
		{else}
			{assign var='defaultpopup' value=$config.defaultpopup}
		{/if}
			<!-- <input type='text' name='defaultpopup' size="45" value='{$defaultpopup}'> -->
			{if $defaultpopup == 'Lead' }
			<label class="radio-inline"><input type="radio" name="defaultpopup" value="Lead" checked>Lead</label>
			<label class="radio-inline"><input type="radio" name="defaultpopup" value="Account">Account</label>
			<label class="radio-inline"><input type="radio" name="defaultpopup" value="Target">Target</label>
			{/if}
			{if $defaultpopup == 'Account' }
			<label class="radio-inline"><input type="radio" name="defaultpopup" value="Lead" >Lead</label>
			<label class="radio-inline"><input type="radio" name="defaultpopup" value="Account" checked>Account</label>
			<label class="radio-inline"><input type="radio" name="defaultpopup" value="Target">Target</label>
			{/if}
			{if $defaultpopup == 'Target'}
			<label class="radio-inline"><input type="radio" name="defaultpopup" value="Lead" >Lead</label>
			<label class="radio-inline"><input type="radio" name="defaultpopup" value="Account">Account</label>
			<label class="radio-inline"><input type="radio" name="defaultpopup" value="Target" checked>Target</label>
			{/if}
		</td>

	</tr>
	<tr></tr>
	
</table>
</td></tr>
</table>	
<td>
<br>
</table>
<div style="padding-top: 2px;">
<input title="{$APP.LBL_SAVE_BUTTON_TITLE}" class="button"  type="submit" name="save" value="  {$APP.LBL_SAVE_BUTTON_LABEL}  " />
		&nbsp;<input title="{$MOD.LBL_SAVE_BUTTON_TITLE}"  class="button"  type="submit" name="restore" value="  {$MOD.LBL_RESTORE_BUTTON_LABEL}  " /> 
		&nbsp;<input title="{$MOD.LBL_CANCEL_BUTTON_TITLE}"  onclick="document.location.href='index.php?module=Administration&action=index'" class="button"  type="button" name="cancel" value="  {$APP.LBL_CANCEL_BUTTON_LABEL}  " />
</div>
{$JAVASCRIPT}

