<?php
// created: 2021-06-29 12:43:55
$mod_strings = array (
  'LBL_CONTACTS_SYNC' => 'Contact Sync',
  'LBL_CALLS' => 'Calls',
  'LBL_MEETINGS' => 'Meetings',
  'LBL_PROSPECT_LIST' => 'Prospect List',
  'LBL_PROJECT_USERS_1_FROM_PROJECT_TITLE' => 'Project Users from Project Title',
  'LBL_DISCOUNT_PERCENT' => 'DiscountPercent',
);