<?php
// created: 2024-06-05 06:17:15
$mod_strings = array (
  'LBL_CONTACTS_SYNC' => 'Contact Sync',
  'LBL_CALLS' => 'Calls',
  'LBL_MEETINGS' => 'Meetings',
  'LBL_PROSPECT_LIST' => 'Prospect List',
  'LBL_PROJECT_USERS_1_FROM_PROJECT_TITLE' => 'Project Users from Project Title',
  'LBL_DEPARTMENT' => 'GroupName',
  'LBL_PROVINCECODE' => 'ProvinceCode',
  'LBL_DISTRICTCODE' => 'DistrictCode',
);