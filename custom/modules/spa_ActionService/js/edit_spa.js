//chỉ dành riêng cho module này
function initializeDropdown(input, columns,columns2,entryPoint, module) {

    var inputId = input;
    if(columns2[0] == true){
        var agent = document.getElementById(inputId);
        if (agent) {
            agent.setAttribute('id', 'custom_' + inputId);
            agent.setAttribute('name', 'custom_' + inputId);
            var hiddenInput = document.createElement('input');
            hiddenInput.type = 'hidden';
            hiddenInput.id = inputId; // Sử dụng lại id ban đầu
            hiddenInput.name = inputId; // Sử dụng lại name ban đầu
            hiddenInput.value = document.getElementById('custom_'+inputId).value;
            agent.parentNode.insertBefore(hiddenInput, agent.nextSibling);
        }
        // Tạo ul element chứa danh sách
        var divElement = document.querySelector(`div.col-xs-12.col-sm-8.edit-view-field[type="relate"][field="${inputId}"]`);
        if (divElement) {
            var ulElement = document.createElement('ul');
            ulElement.id = `data_${inputId}_list`;
            ulElement.classList.add('dropdown-list');
            ulElement.style.display = 'none'; // Mặc định ẩn đi
            divElement.appendChild(ulElement);
        }
        booking_id = document.getElementById('aos_bookings_spa_actionservice_1aos_bookings_ida').value;
        console.log(booking_id);
        // Fetch dữ liệu từ server
        function fetchAccountsData() {
            $.ajax({
                type: "POST",
                url: `index.php?entryPoint=${entryPoint}`,
                data: {
                    rows: JSON.stringify(columns),
                    booking_id : booking_id,
                    module: module,
                },
                success: function(data) {
                    var datas = JSON.parse(data);
                    populateResults2(datas, module);
                },
                error: function(error) {
                    console.error('Error fetching data:', error);
                }
            });
        }
        fetchAccountsData();
        function populateResults2(datas, module) {
            const ulElement = document.getElementById(`data_${inputId}_list`);
            if (!ulElement) {
                return;
            }
            ulElement.innerHTML = '';
            datas.forEach((item) => {
                const liElement = document.createElement('li');
                let content = `<span class="name">${item[columns[1]]}</span>`; // Use 'name' from columns array
                if (columns.length > 2 && item[columns[2]]) { // Check for 'tax_code' from columns array if available
                    content += ` - <span class="code">${item[columns[2]]}</span>`;
                }
                if (columns.length > 3 && item[columns[3]]) { // Check for 'account_code' from columns array if available
                    content += ` - <span class="code2">${item[columns[3]]}</span>`;
                }
                liElement.innerHTML = content;
                liElement.setAttribute('data-id', item[columns[0]]); // Use 'id' from columns array
                liElement.setAttribute('data-name', item[columns[1]]); // Use 'name' from columns array
                if (columns.length > 2 && item[columns[2]]) { // Check for 'tax_code' from columns array if available
                    liElement.setAttribute('data-tax-code', item[columns[2]]);
                }
                if (columns.length > 3 && item[columns[3]]) { // Check for 'account_code' from columns array if available
                    liElement.setAttribute('data-account-code', item[columns[3]]);
                }
                liElement.classList.add('dropdown-item');
                liElement.addEventListener('click', () => {
        
                    var main_id = inputId.slice(0, -5);

                    var inputIdField1 = document.getElementById('custom_' + inputId);
                    var hiddeninputIdField1 = document.getElementById(inputId);
                    var inputIdField2 = document.getElementById(columns2[2]);
                    var inputIdField3 =  document.getElementById(columns2[3]);
                    var inputIdField4 =  document.getElementById(columns2[4]);
                    var inputId1 = document.getElementById(main_id + module + '_ida');
                    if  (inputId1){
                        inputId1.value = item[columns[0]];
                    }
                    var inputId2 = document.getElementById(inputId + '_id');
                    if (inputId2){
                        inputId2.value = item[columns[0]];
                    }
                    var inputId3 = document.getElementById(input[1]);
                    if  (inputId3) {
                        inputId3.value = item[columns[0]];
                    }
                    if (inputIdField1 && hiddeninputIdField1) {
                        inputIdField1.value = item[columns[1]];
                        hiddeninputIdField1.value = item[columns[1]];
                    }
                    // Giả sử item[columns[4]] chứa giá trị cần định dạng
                    var value = item[columns[4]];

                    // Định dạng số với dấu phân cách hàng nghìn và 2 chữ số thập phân
                    var formattedValue = new Intl.NumberFormat('en-US', { 
                        style: 'decimal', 
                        minimumFractionDigits: 2, 
                        maximumFractionDigits: 2 
                    }).format(value);
                    if (inputIdField2) {
                        inputIdField2.value = formattedValue;
                    }


                    if(inputIdField3){
                        inputIdField3.value = item[columns[5]];
                    }
                    ulElement.style.display = 'none'; // Hide the list after selection
                });
                ulElement.appendChild(liElement);
            });
        }
        const inputSearch = document.getElementById('custom_' + inputId);
        if (inputSearch) {

            inputSearch.addEventListener('input', function() {
                if (this.value.trim() !== '') { // Kiểm tra nếu input đã có giá trị
                    filterResults(normalizeString(this.value)); // Filter dữ liệu khi focus vào nếu đã có giá trị
                } else {
                    if (ulElement) {

                        ulElement.style.display = 'block';
                    }
                }
            });
            inputSearch.addEventListener('input', function() {
                const searchValue = normalizeString(this.value);
                filterResults(searchValue);
            });
        }
        // Ẩn danh sách khi nhấn ra ngoài
        document.addEventListener('click', function(event) {
            const isClickInside = inputSearch && inputSearch.contains(event.target) || (ulElement && ulElement.contains(event.target));
            if (!isClickInside && ulElement) {
                ulElement.style.display = 'none';
            }
        });
        function filterResults(searchValue) {
            const ulElement = document.getElementById(`data_${inputId}_list`);
            if (!ulElement) {
                return;
            }
            const lis = Array.from(ulElement.getElementsByTagName('li'));
            let hasVisibleItem = false;
            lis.forEach(li => {
                const name = normalizeString(li.getAttribute('data-name'));
                const taxCode = normalizeString(li.getAttribute('data-tax-code'));
                const accountCode = columns.length > 3 ? normalizeString(li.getAttribute('data-account-code')) : '';

                if (name.includes(searchValue) || (taxCode && taxCode.includes(searchValue)) || (accountCode && accountCode.includes(searchValue))) {
                    li.style.display = 'block';
                    hasVisibleItem = true;
                    highlightText(li, li.getAttribute('data-name'), li.getAttribute('data-tax-code'), searchValue, columns.length > 3 ? li.getAttribute('data-account-code') : ''); // Highlight text
                } else {
                    li.style.display = 'none';
                }
            });
            if (!hasVisibleItem) {
                ulElement.style.display = 'none';
            } else {
                ulElement.style.display = 'block';
            }
        }
        function normalizeString(str) {
            return str ? str.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase().trim() : '';
        }
        // Hàm đánh dấu văn bản phù hợp với màu nền
        function highlightText(element, name, taxCode, searchValue, accountCode = '') {
            const normalizedSearchValue = normalizeString(searchValue);
            const nameSpan = element.querySelector('.name');
            const taxCodeSpan = element.querySelector('.code');
            const accountCodeSpan = element.querySelector('.code2');
            if (nameSpan) {
                if (name && normalizeString(name).includes(normalizedSearchValue)) {
                    nameSpan.innerHTML = highlightSubstring(name, searchValue);
                } else {
                    nameSpan.innerHTML = name; // Bỏ đánh dấu nếu không khớp
                }
            }
            if (taxCodeSpan) {
                if (taxCode && normalizeString(taxCode).includes(normalizedSearchValue)) {
                    taxCodeSpan.innerHTML = highlightSubstring(taxCode, searchValue);
                } else {
                    taxCodeSpan.innerHTML = taxCode; // Bỏ đánh dấu nếu không khớp
                }
            }

            if (accountCodeSpan) {
                if (accountCode && normalizeString(accountCode).includes(normalizedSearchValue)) {
                    accountCodeSpan.innerHTML = highlightSubstring(accountCode, searchValue);
                } else {
                    accountCodeSpan.innerHTML = accountCode; // Bỏ đánh dấu nếu không khớp
                }
            }
        }
        function highlightSubstring(text, searchValue) {
            const normalizedText = normalizeString(text);
            const normalizedSearchValue = normalizeString(searchValue);
            const startIndex = normalizedText.indexOf(normalizedSearchValue);

            if (startIndex !== -1) {
                const beforeText = text.substring(0, startIndex);
                const highlightedText = text.substring(startIndex, startIndex + searchValue.length);
                const afterText = text.substring(startIndex + searchValue.length);
                return beforeText + '<span class="highlight">' + highlightedText + '</span>' + afterText;
            }
            return text;
        }
        const customInput = document.getElementById('custom_'+inputId);
        const originalInput = document.getElementById(inputId);

        if (customInput && originalInput) {
            customInput.addEventListener('input', function() {
                originalInput.value = this.value;
            });
        }
        const clearButton = document.getElementById('btn_clr_'+inputId);
        if (clearButton && customInput) {
            clearButton.addEventListener('click', function() {
                customInput.value = ''; // Xóa giá trị của input custom_accounts_aos_bookings_1_name
            });
        }
        var intervalId1;
        var field_id;
        var field_name = 'custom_' + inputId;
        field_id = inputId.slice(0, -5) + module + '_ida';
        var btn_choose = 'btn_' + inputId;
        var btn_choose_clear = 'btn_clr_' + inputId;
        var id2 = document.getElementById(field_id).value;
        var namex = [columns[1]];
        var getElement = (id) => document.getElementById(id);
        var field_idvalue = getElement(field_id).value;
        const handleClick = () => {
            intervalId1 = setInterval(check_hidden_id, 500);
        };
        const check_hidden_id = () => {
            var id = getElement(field_id);
            if (id.value !== id2) {
                id2 = id.value;
                get_fieldname(columns ,id2);
                clearInterval(intervalId1);
            }
        };
        getElement(btn_choose).addEventListener("click", handleClick);
        function get_fieldname(columns,id) {
        $.ajax({
            type: "POST",
            url: `index.php?entryPoint=${entryPoint}`,
            data: {
                rows: JSON.stringify(columns),
                module: module,
                where_id : id,

        },
            success: function(data) {
                datas = JSON.parse(data);

                var price = datas[0]['price'];
                var formattedPrice = new Intl.NumberFormat('en-US', { 
                    style: 'decimal', 
                    minimumFractionDigits: 2, 
                    maximumFractionDigits: 2 
                }).format(price);
                document.getElementById('custom_'+inputId).value = datas[0]['name'];
                document.getElementById('unit_price_input').value = formattedPrice;
                document.getElementById('type_currency').value = datas[0]['type_currency'];
            },
            error: function(error) {
                console.error(error);
            }
        });
    }
    }
}

var form = document.getElementById('EditView');
var module_dir = form.querySelector('input[name="module"]').value;
switch (module_dir) {
    case 'spa_ActionService':
        columns1 = [true,'name','unit_price_input','type_currency'];
         break; 

    default:
        break;
}


var element1 = document.getElementById('aos_products_spa_actionservice_1_name');
if (element1) {
    initializeDropdown('aos_products_spa_actionservice_1_name',['id', 'name', 'part_number','','price','type_currency'], columns1, 'get_info_inputsearch_spa', 'aos_products');
}

