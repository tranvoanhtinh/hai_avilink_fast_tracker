<?php
require_once 'include/MVC/View/views/view.edit.php';
class CustomSpa_ActionServiceViewEdit extends ViewEdit
{
    function display()
    {
        parent::display();

        if (file_exists(getcwd() . '/config.php')) {
            require(getcwd() . '/config.php');
        }
        if (isset($sugar_config['prefix_name_auto_actionservice'])) {
            $name_prefix = $sugar_config['prefix_name_auto_actionservice'];
        }
        echo "<div id='namePrefix' style='display: none;' data-name-prefix='$name_prefix'></div>";
?>
        <script src="custom/modules/spa_ActionService/js/edit_spa.js"> </script>
       
<?php
    }
}
