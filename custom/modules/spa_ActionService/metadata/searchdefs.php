<?php
$module_name = 'spa_ActionService';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'accounts_spa_actionservice_1_name' => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_ACCOUNTS_SPA_ACTIONSERVICE_1_FROM_ACCOUNTS_TITLE',
        'id' => 'ACCOUNTS_SPA_ACTIONSERVICE_1ACCOUNTS_IDA',
        'width' => '10%',
        'default' => true,
        'name' => 'accounts_spa_actionservice_1_name',
      ),
      'aos_bookings_spa_actionservice_1_name' => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_AOS_BOOKINGS_SPA_ACTIONSERVICE_1_FROM_AOS_BOOKINGS_TITLE',
        'id' => 'AOS_BOOKINGS_SPA_ACTIONSERVICE_1AOS_BOOKINGS_IDA',
        'width' => '10%',
        'default' => true,
        'name' => 'aos_bookings_spa_actionservice_1_name',
      ),
      'status' => 
      array (
        'type' => 'enum',
        'studio' => 'visible',
        'label' => 'LBL_STATUS',
        'width' => '10%',
        'default' => true,
        'name' => 'status',
      ),
    ),
    'advanced_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'status' => 
      array (
        'type' => 'enum',
        'studio' => 'visible',
        'label' => 'LBL_STATUS',
        'width' => '10%',
        'default' => true,
        'name' => 'status',
      ),
      'action_date' => 
      array (
        'type' => 'datetimecombo',
        'label' => 'LBL_ACTION_DATE',
        'width' => '10%',
        'default' => true,
        'name' => 'action_date',
      ),
      'accounts_spa_actionservice_1_name' => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_ACCOUNTS_SPA_ACTIONSERVICE_1_FROM_ACCOUNTS_TITLE',
        'width' => '10%',
        'default' => true,
        'id' => 'ACCOUNTS_SPA_ACTIONSERVICE_1ACCOUNTS_IDA',
        'name' => 'accounts_spa_actionservice_1_name',
      ),
      'mobileref' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_MOBILE_REF',
        'width' => '10%',
        'default' => true,
        'name' => 'mobileref',
      ),
      'aos_products_spa_actionservice_1_name' => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_AOS_PRODUCTS_SPA_ACTIONSERVICE_1_FROM_AOS_PRODUCTS_TITLE',
        'id' => 'AOS_PRODUCTS_SPA_ACTIONSERVICE_1AOS_PRODUCTS_IDA',
        'width' => '10%',
        'default' => true,
        'name' => 'aos_products_spa_actionservice_1_name',
      ),
      'aos_bookings_spa_actionservice_1_name' => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_AOS_BOOKINGS_SPA_ACTIONSERVICE_1_FROM_AOS_BOOKINGS_TITLE',
        'width' => '10%',
        'default' => true,
        'id' => 'AOS_BOOKINGS_SPA_ACTIONSERVICE_1AOS_BOOKINGS_IDA',
        'name' => 'aos_bookings_spa_actionservice_1_name',
      ),
      'assigned_user_name' => 
      array (
        'link' => true,
        'type' => 'relate',
        'label' => 'LBL_ASSIGNED_TO_NAME',
        'id' => 'ASSIGNED_USER_ID',
        'width' => '10%',
        'default' => true,
        'name' => 'assigned_user_name',
      ),
      'current_user_only' => 
      array (
        'label' => 'LBL_CURRENT_USER_FILTER',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
        'name' => 'current_user_only',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
;
?>
