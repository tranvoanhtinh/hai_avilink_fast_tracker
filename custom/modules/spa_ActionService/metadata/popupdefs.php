<?php
$popupMeta = array (
    'moduleMain' => 'spa_ActionService',
    'varName' => 'spa_ActionService',
    'orderBy' => 'spa_actionservice.name',
    'whereClauses' => array (
  'status' => 'spa_actionservice.status',
  'action_date' => 'spa_actionservice.action_date',
  'spa_actionservice_cs_cususer_name' => 'spa_actionservice.spa_actionservice_cs_cususer_name',
  'assigned_user_name' => 'spa_actionservice.assigned_user_name',
),
    'searchInputs' => array (
  3 => 'status',
  4 => 'action_date',
  5 => 'spa_actionservice_cs_cususer_name',
  6 => 'assigned_user_name',
),
    'searchdefs' => array (
  'status' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_STATUS',
    'width' => '10%',
    'name' => 'status',
  ),
  'action_date' => 
  array (
    'type' => 'datetimecombo',
    'label' => 'LBL_ACTION_DATE',
    'width' => '10%',
    'name' => 'action_date',
  ),
  'spa_actionservice_cs_cususer_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_SPA_ACTIONSERVICE_CS_CUSUSER_FROM_CS_CUSUSER_TITLE',
    'id' => 'SPA_ACTIONSERVICE_CS_CUSUSERCS_CUSUSER_IDB',
    'width' => '10%',
    'name' => 'spa_actionservice_cs_cususer_name',
  ),
  'assigned_user_name' => 
  array (
    'link' => true,
    'type' => 'relate',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'id' => 'ASSIGNED_USER_ID',
    'width' => '10%',
    'name' => 'assigned_user_name',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'AOS_BOOKINGS_SPA_ACTIONSERVICE_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_AOS_BOOKINGS_SPA_ACTIONSERVICE_1_FROM_AOS_BOOKINGS_TITLE',
    'id' => 'AOS_BOOKINGS_SPA_ACTIONSERVICE_1AOS_BOOKINGS_IDA',
    'width' => '10%',
    'default' => true,
    'name' => 'aos_bookings_spa_actionservice_1_name',
  ),
  'AOS_PASSENGERS_SPA_ACTIONSERVICE_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_AOS_PASSENGERS_SPA_ACTIONSERVICE_1_FROM_AOS_PASSENGERS_TITLE',
    'id' => 'AOS_PASSENGERS_SPA_ACTIONSERVICE_1AOS_PASSENGERS_IDA',
    'width' => '10%',
    'default' => true,
    'name' => 'aos_passengers_spa_actionservice_1_name',
  ),
  'STATUS' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_STATUS',
    'width' => '10%',
    'default' => true,
    'name' => 'status',
  ),
  'AOS_PRODUCTS_SPA_ACTIONSERVICE_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_AOS_PRODUCTS_SPA_ACTIONSERVICE_1_FROM_AOS_PRODUCTS_TITLE',
    'id' => 'AOS_PRODUCTS_SPA_ACTIONSERVICE_1AOS_PRODUCTS_IDA',
    'width' => '10%',
    'default' => true,
    'name' => 'aos_products_spa_actionservice_1_name',
  ),
  'UNIT_PRICE_INPUT' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_UNIT_PRICE_INPUT',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
    'name' => 'unit_price_input',
  ),
  'ACTION_DATE' => 
  array (
    'type' => 'datetimecombo',
    'label' => 'LBL_ACTION_DATE',
    'width' => '10%',
    'default' => true,
    'name' => 'action_date',
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
    'name' => 'date_entered',
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
    'name' => 'assigned_user_name',
  ),
),
);
