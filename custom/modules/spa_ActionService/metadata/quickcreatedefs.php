<?php
$module_name = 'spa_ActionService';
$viewdefs [$module_name] = 
array (
  'QuickCreate' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 
          array (
            'name' => 'aos_bookings_spa_actionservice_1_name',
            'label' => 'LBL_AOS_BOOKINGS_SPA_ACTIONSERVICE_1_FROM_AOS_BOOKINGS_TITLE',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'action_date',
            'label' => 'LBL_ACTION_DATE',
          ),
          1 => 
          array (
            'name' => 'status',
            'studio' => 'visible',
            'label' => 'LBL_STATUS',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'aos_passengers_spa_actionservice_1_name',
            'label' => 'LBL_AOS_PASSENGERS_SPA_ACTIONSERVICE_1_FROM_AOS_PASSENGERS_TITLE',
          ),
          1 => 'assigned_user_name',
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'type_service',
            'studio' => true,
            'label' => 'LBL_TYPE_SERVICE',
          ),
          1 => 
          array (
            'name' => 'aos_products_spa_actionservice_1_name',
            'label' => 'LBL_AOS_PRODUCTS_SPA_ACTIONSERVICE_1_FROM_AOS_PRODUCTS_TITLE',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'unit_price_input',
            'label' => 'LBL_UNIT_PRICE_INPUT',
          ),
          1 => 
          array (
            'name' => 'type_currency',
            'studio' => true,
            'label' => 'LBL_TYPE_CURRENCY',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'description',
            'comment' => 'Full text of the note',
            'label' => 'LBL_DESCRIPTION',
          ),
        ),
      ),
    ),
  ),
);
;
?>
