<?php
$module_name = 'spa_ActionService';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'AOS_BOOKINGS_SPA_ACTIONSERVICE_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_AOS_BOOKINGS_SPA_ACTIONSERVICE_1_FROM_AOS_BOOKINGS_TITLE',
    'id' => 'AOS_BOOKINGS_SPA_ACTIONSERVICE_1AOS_BOOKINGS_IDA',
    'width' => '10%',
    'default' => true,
  ),
  'AOS_PASSENGERS_SPA_ACTIONSERVICE_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_AOS_PASSENGERS_SPA_ACTIONSERVICE_1_FROM_AOS_PASSENGERS_TITLE',
    'id' => 'AOS_PASSENGERS_SPA_ACTIONSERVICE_1AOS_PASSENGERS_IDA',
    'width' => '10%',
    'default' => true,
  ),
  'STATUS' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_STATUS',
    'width' => '10%',
    'default' => true,
  ),
  'AOS_PRODUCTS_SPA_ACTIONSERVICE_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_AOS_PRODUCTS_SPA_ACTIONSERVICE_1_FROM_AOS_PRODUCTS_TITLE',
    'id' => 'AOS_PRODUCTS_SPA_ACTIONSERVICE_1AOS_PRODUCTS_IDA',
    'width' => '10%',
    'default' => true,
  ),
  'UNIT_PRICE_INPUT' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_UNIT_PRICE_INPUT',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
  ),
  'ACTION_DATE' => 
  array (
    'type' => 'datetimecombo',
    'label' => 'LBL_ACTION_DATE',
    'width' => '10%',
    'default' => true,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => false,
  ),
  'MOBILEREF' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_MOBILE_REF',
    'width' => '10%',
    'default' => false,
  ),
  'ACCOUNTS_SPA_ACTIONSERVICE_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_ACCOUNTS_SPA_ACTIONSERVICE_1_FROM_ACCOUNTS_TITLE',
    'id' => 'ACCOUNTS_SPA_ACTIONSERVICE_1ACCOUNTS_IDA',
    'width' => '10%',
    'default' => false,
  ),
  'DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'CREATED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'default' => false,
  ),
  'MODIFIED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_MODIFIED_NAME',
    'id' => 'MODIFIED_USER_ID',
    'width' => '10%',
    'default' => false,
  ),
  'VND_TOTAL' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_VND_TOTAL',
    'currency_format' => true,
    'width' => '10%',
    'default' => false,
  ),
  'AMOUNTSERVICE' => 
  array (
    'type' => 'currency',
    'default' => false,
    'label' => 'LBL_AMOUNT_SERVICE',
    'currency_format' => true,
    'width' => '10%',
  ),
  'DISCOUNTPERCENT' => 
  array (
    'type' => 'decimal',
    'label' => 'LBL_DISCOUNT_PERCENT',
    'width' => '10%',
    'default' => false,
  ),
  'SPA_ACTIONSERVICE_CS_CUSUSER_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_SPA_ACTIONSERVICE_CS_CUSUSER_FROM_CS_CUSUSER_TITLE',
    'id' => 'SPA_ACTIONSERVICE_CS_CUSUSERCS_CUSUSER_IDB',
    'width' => '10%',
    'default' => false,
  ),
  'COMMISSION_ACTION' => 
  array (
    'type' => 'decimal',
    'default' => false,
    'label' => 'LBL_COMMISSION_ACTION',
    'width' => '10%',
  ),
);
;
?>
