<?php
// created: 2024-08-08 04:41:26
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '45%',
    'default' => true,
  ),
  'type_service' => 
  array (
    'type' => 'enum',
    'studio' => true,
    'vname' => 'LBL_TYPE_SERVICE',
    'width' => '10%',
    'default' => true,
  ),
  'aos_products_spa_actionservice_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_AOS_PRODUCTS_SPA_ACTIONSERVICE_1_FROM_AOS_PRODUCTS_TITLE',
    'id' => 'AOS_PRODUCTS_SPA_ACTIONSERVICE_1AOS_PRODUCTS_IDA',
    'width' => '10%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'AOS_Products',
    'target_record_key' => 'aos_products_spa_actionservice_1aos_products_ida',
  ),
  'aos_passengers_spa_actionservice_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_AOS_PASSENGERS_SPA_ACTIONSERVICE_1_FROM_AOS_PASSENGERS_TITLE',
    'id' => 'AOS_PASSENGERS_SPA_ACTIONSERVICE_1AOS_PASSENGERS_IDA',
    'width' => '10%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'AOS_Passengers',
    'target_record_key' => 'aos_passengers_spa_actionservice_1aos_passengers_ida',
  ),
  'status' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'vname' => 'LBL_STATUS',
    'width' => '10%',
    'default' => true,
  ),
  'type_currency' => 
  array (
    'type' => 'enum',
    'studio' => true,
    'vname' => 'LBL_TYPE_CURRENCY',
    'width' => '10%',
    'default' => true,
  ),
  'unit_price_input' => 
  array (
    'type' => 'currency',
    'vname' => 'LBL_UNIT_PRICE_INPUT',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
  ),
  'assigned_user_name' => 
  array (
    'link' => true,
    'type' => 'relate',
    'vname' => 'LBL_ASSIGNED_TO_NAME',
    'id' => 'ASSIGNED_USER_ID',
    'width' => '10%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Users',
    'target_record_key' => 'assigned_user_id',
  ),
  'edit_button' => 
  array (
    'width' => '4%',
    'default' => true,
    'vname' => 'LBL_EDIT_BUTTON',
    'widget_class' => 'SubPanelEditButton',
  ),
);