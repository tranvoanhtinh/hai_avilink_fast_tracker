<?php
// created: 2024-06-14 03:32:15
$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Tên người dùng được chỉ định',
  'LBL_ASSIGNED_TO_NAME' => 'Chỉ định cho',
  'LBL_SECURITYGROUPS' => 'Nhóm bảo mật',
  'LBL_SECURITYGROUPS_SUBPANEL_TITLE' => 'Nhóm bảo mật',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Ngày tạo',
  'LBL_DATE_MODIFIED' => 'Ngày chỉnh sửa',
  'LBL_MODIFIED' => 'Được sửa bởi',
  'LBL_MODIFIED_NAME' => 'Chỉnh sửa bởi Tên',
  'LBL_CREATED' => 'Khởi tạo bởi',
  'LBL_DESCRIPTION' => 'Mô tả',
  'LBL_DELETED' => 'Đã xóa',
  'LBL_NAME' => 'Tên',
  'LBL_CREATED_USER' => 'Tạo bởi người dùng',
  'LBL_MODIFIED_USER' => 'Sửa bởi người dùng',
  'LBL_LIST_NAME' => 'Tên',
  'LBL_EDIT_BUTTON' => 'Sửa',
  'LBL_REMOVE' => 'Xóa bỏ',
  'LBL_ASCENDING' => 'Tăng dần',
  'LBL_DESCENDING' => 'Giảm dần',
  'LBL_OPT_IN' => 'Chọn tham gia',
  'LBL_OPT_IN_PENDING_EMAIL_NOT_SENT' => 'Đang chờ xác nhận chọn tham gia, xác nhận chọn tham gia không được gửi',
  'LBL_OPT_IN_PENDING_EMAIL_SENT' => 'Đang chờ xác nhận chọn tham gia, xác nhận chọn tham gia đã được gửi',
  'LBL_OPT_IN_CONFIRMED' => 'Đã chọn tham gia',
  'LBL_MODULE_NAME' => 'Chi tiết thực hiện ĐH',
  'LBL_MODULE_TITLE' => 'Chi tiết thực hiện ĐH',
  'LNK_NEW_RECORD' => 'Tạo',
  'LNK_LIST' => 'Xem',
  'LNK_IMPORT_SPA_ACTIONSERVICE' => 'Import',
  'LBL_SEARCH_FORM_TITLE' => 'Tìm kiếm',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Xem lịch sử',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Hoạt động',
  'LBL_SPA_ACTIONSERVICE_SUBPANEL_TITLE' => 'Chi tiết thực hiện ĐH',
  'LBL_STATUS' => 'Tình trạng',
  'LBL_COMMISSION_ACTION' => 'Hoa hồng',
  'LBL_ACTION_DATE' => 'Ngày',
  'LBL_DATE_STD_STA' => 'Ngày bay đáp',
  'LBL_SPA_ACTIONSERVICE_AOS_PRODUCTS_FROM_AOS_PRODUCTS_TITLE' => 'Dịch vụ',
  'LBL_SPA_ACTIONSERVICE_AOS_INVOICES_FROM_AOS_INVOICES_TITLE' => 'Hóa đơn',
  'LBL_AMOUNT_SERVICE' => 'Tiền dịch vụ',
  'LBL_DISCOUNT_PERCENT' => '% Hoa hồng',
  'LBL_SPA_ACTIONSERVICE_CS_CUSUSER_FROM_CS_CUSUSER_TITLE' => 'Nhân viên',
  'LBL_LIST_FORM_TITLE' => 'Action Service Danh sách',
  'LBL_HOMEPAGE_TITLE' => 'Của tôi Action Service',
  'LBL_MOBILE_REF' => 'Di động',
  'LBL_ACCOUNTS_SPA_ACTIONSERVICE_1_FROM_ACCOUNTS_TITLE' => 'Đại lý',
  'LBL_UNIT_PRICE_INPUT' => 'Giá bán',
  'LBL_DISCOUNT_AMOUNT_SPA' => '% Chiết khấu',
  'LBL_AOS_PRODUCTS_SPA_ACTIONSERVICE_1_FROM_AOS_PRODUCTS_TITLE' => 'Dịch vụ',
  'LBL_FLIGHT_NAME' => 'Chuyến bay',
  'LBL_AIRPORT_TO' => 'Đến',
  'LBL_AIRPORT_FROM' => 'Từ',
  'LBL_BOOKING_DATE' => 'Ngày Booking',
  'LBL_VND_TOTAL' => 'Tổng tiền(vnd)',
  'LBL_USD_TOTAL' => 'Tổng tiền(usd)',
  'LBL_VND' => 'VND',
  'LBL_TYPE_CURRENCY' => 'Loại tiền',
  'LBL_TYPE_SERVICE' => 'Loại dịch vụ',
  'LBL_USD' => 'USD',
  'LBL_EXCHANGE_RATE' => 'Tỷ Giá',
  'LBL_EDITVIEW_PANEL1' => 'Tiền dịch vụ',
  'LBL_CONTACTS_SPA_ACTIONSERVICE_1_FROM_CONTACTS_TITLE' => 'Hành khách',
);