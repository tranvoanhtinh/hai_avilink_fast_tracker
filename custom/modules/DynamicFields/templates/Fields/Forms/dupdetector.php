<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*
   Created By : Urdhva Tech Pvt. Ltd.
 Created date : 04/23/2018
   Contact at : contact@urdhva-tech.com
          Web : www.urdhva-tech.com
        Skype : urdhvatech
       Module : Dupdetector 1.3
*/
function get_body(&$ss, $vardef) {
    return $ss->fetch('modules/DynamicFields/templates/Fields/Forms/varchar.tpl');
}