<?php
// created: 2022-04-04 05:28:42
$mod_strings = array (
  'LNK_NEW_RECORD' => 'Create Detail Report Activities',
  'LNK_LIST' => 'View Detail Report Activities',
  'LNK_IMPORT_RP_REPORTACTIVITIESDETAIL' => 'Nhập Detail Report Activities',
  'LBL_LIST_FORM_TITLE' => 'Detail Report Activities List',
  'LBL_SEARCH_FORM_TITLE' => 'Search Detail Report Activities',
  'LBL_TIME' => 'Date',
  'LBL_PROVINCE' => 'Province',
  'LBL_DISTR' => 'Group Sales',
  'LBL_TITLE' => 'Title',
  'LBL_USER' => 'User',
   'LBL_NAME'=> 'Name',
  'LBL_DATE'=> 'Date',
  'LBL_MODULE'=> 'Module',
  'LBL_ASSIGN'=> 'Relate to',
  'LBL_CONTACT'=> 'Contact',
  'LBL_STATUS'=> 'Status',
  'LBL_AMOUNT'=> 'Amount',
  'LBL_NOTE'=> 'Notes',
  'LBL_PRODUCT'=> 'Product/Service',
  'LBL_SOURCE'=> 'Source',
  'LBL_EXPORT_EXCEL' => 'Export Excel',
  'LBL_DELETE_BUTTON' => 'Clear Filter',
  'LBL_STATISTICAL_BUTTON' => 'Run Report',

);