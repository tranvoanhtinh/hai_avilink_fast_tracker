<?php
// created: 2024-06-05 06:17:26
$mod_strings = array (
  'LBL_LEADS' => 'Leads',
  'LBL_ACCOUNTS' => 'Agent',
  'LBL_LEADS_SUBPANEL_TITLE' => 'Leads',
  'LBL_ACCOUNTS_SUBPANEL_TITLE' => 'Agent',
  'LBL_CONTACTS' => 'Contacts',
  'LBL_CONTACTS_SUBPANEL_TITLE' => 'Contactss',
  'LBL_CAMPAIGNS' => 'Campaigns',
  'LBL_LIST_FORM_TITLE' => 'Target Lists',
  'LBL_SEARCH_FORM_TITLE' => 'Target Lists Search',
  'LBL_PROSPECTS' => 'Prospects',
  'LBL_PROSPECTS_SUBPANEL_TITLE' => 'Targets',
);