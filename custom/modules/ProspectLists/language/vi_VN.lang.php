<?php
// created: 2024-06-14 03:32:18
$mod_strings = array (
  'LBL_ACCOUNTS' => 'Accounts',
  'LBL_ACCOUNTS_SUBPANEL_TITLE' => 'Đại lý',
  'LBL_LEADS' => 'Leads',
  'LBL_LEADS_SUBPANEL_TITLE' => 'KH Tiềm năng',
  'LBL_PROSPECTS' => 'Prospects',
  'LBL_PROSPECTS_SUBPANEL_TITLE' => 'Đối tượng',
  'LBL_CONTACTS' => 'Contacts',
  'LBL_CONTACTS_SUBPANEL_TITLE' => 'Người liên hệ đại lý',
  'LBL_LIST_FORM_TITLE' => 'Danh sách Đối tượng',
  'LBL_SEARCH_FORM_TITLE' => 'Tìm kiếm',
  'LBL_CAMPAIGNS' => 'Campaigns',
);