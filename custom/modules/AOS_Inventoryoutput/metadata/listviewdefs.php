<?php
$module_name = 'AOS_Inventoryoutput';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'BILLING_ACCOUNT' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'label' => 'LBL_BILLING_ACCOUNT',
    'id' => 'BILLING_ACCOUNT_ID',
    'link' => true,
    'width' => '10%',
    'default' => true,
  ),
  'BILLING_CONTACT' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'label' => 'LBL_BILLING_CONTACT',
    'id' => 'BILLING_CONTACT_ID',
    'link' => true,
    'width' => '10%',
    'default' => true,
  ),
  'MOBILEREF' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_MOBILE_REF',
    'width' => '10%',
    'default' => true,
  ),
  'VOUCHERDATE' => 
  array (
    'type' => 'date',
    'label' => 'LBL_VOUCHER_DATE',
    'width' => '10%',
    'default' => true,
  ),
  'STATUS_INVENTORY' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_STATUS_INVENTORY',
    'width' => '10%',
    'default' => true,
  ),
  'TAKE_FROM' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_TAKE_FROM',
    'width' => '10%',
    'default' => true,
  ),
  'LP_TOTAL_AMOUNT' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_LP_TOTAL_AMOUNT',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
  ),
  'DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
  'MODIFIED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_MODIFIED_NAME',
    'id' => 'MODIFIED_USER_ID',
    'width' => '10%',
    'default' => false,
  ),
  'BILLING_ADDRESS_STREET' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_BILLING_ADDRESS_STREET',
    'width' => '10%',
    'default' => false,
  ),
  'SHIPPING_ADDRESS_STREET' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_SHIPPING_ADDRESS_STREET',
    'width' => '10%',
    'default' => false,
  ),
  'STATUSTRANSPORT' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_STATUSTRANSPORT',
    'width' => '10%',
    'default' => false,
  ),
  'CREATED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'default' => false,
  ),
  'SHIPPING_METHOD' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_SHIPPING_METHOD',
    'width' => '10%',
    'default' => false,
  ),
  'DUE_DATE' => 
  array (
    'type' => 'date',
    'label' => 'LBL_DUE_DATE',
    'width' => '10%',
    'default' => false,
  ),
  'TOTAL_AMT' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_TOTAL_AMT',
    'currency_format' => true,
    'width' => '10%',
    'default' => false,
  ),
  'SUMMARY_PRODUCT' => 
  array (
    'type' => 'text',
    'label' => 'LBL_SUMMARY_PRODUCTS',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'PAYMENTMETHOD' => 
  array (
    'type' => 'enum',
    'studio' => true,
    'default' => false,
    'label' => 'LBL_PAYMENTMETHOD',
    'width' => '10%',
  ),
  'DELIVERY_DATE' => 
  array (
    'type' => 'date',
    'label' => 'LBL_DELIVERY_DATE',
    'width' => '10%',
    'default' => false,
  ),
  'ODN' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ODN',
    'width' => '10%',
    'default' => false,
  ),
  'IS_INVOICE' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'LBL_ISINVOICE',
    'width' => '10%',
  ),
  'ESTIMATED_DELIVERY_DATE' => 
  array (
    'type' => 'date',
    'label' => 'LBL_ESTIMATED_DELIVERY_DATE',
    'width' => '10%',
    'default' => false,
  ),
  'STATUS' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_STATUS',
    'width' => '10%',
    'default' => false,
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => false,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => false,
  ),
);
;
?>
