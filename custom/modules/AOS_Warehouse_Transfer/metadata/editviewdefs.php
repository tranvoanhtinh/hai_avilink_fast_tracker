<?php
$module_name = 'AOS_Warehouse_Transfer';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'SAVE',
          1 => 'CANCEL',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'LBL_INVOICE_TO' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_LINE_ITEMS' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'LBL_INVOICE_TO' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 
          array (
            'name' => 'voucherdate',
            'label' => 'LBL_VOUCHER_DATE',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'status_inventory',
            'studio' => true,
            'label' => 'LBL_STATUS_INVENTORY',
          ),
          1 => 
          array (
            'name' => 'customer_ref',
            'label' => 'LBL_CUSTOMER_REF',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'from_warehouse',
            'label' => 'LBL_FROM_WAREHOUSE',
          ),
          1 => 
          array (
            'name' => 'to_warehouse',
            'label' => 'LBL_TO_WAREHOUSE',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'billing_contact',
            'studio' => 'visible',
            'label' => 'LBL_BILLING_CONTACT',
          ),
          1 => 
          array (
            'name' => 'mobileref',
            'label' => 'LBL_MOBILE_REF',
          ),
        ),
        4 => 
        array (
          0 => 'description',
          1 => 'assigned_user_name',
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'inventory_in',
            'studio' => 'visible',
            'label' => 'LBL_INVENYORY_IN',
          ),
          1 => 
          array (
            'name' => 'inventory_out',
            'studio' => 'visible',
            'label' => 'LBL_INVENYORY_OUT',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'invoice',
            'studio' => 'visible',
            'label' => 'LBL_INVOICE',
          ),
          1 => '',
        ),
      ),
      'lbl_line_items' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'currency_id',
            'studio' => 'visible',
            'label' => 'LBL_CURRENCY',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'pos_line_items',
            'label' => 'LBL_POS_LINE_ITEMS',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'lp_total_amt',
            'label' => 'LBL_LP_TOTAL_AMT',
          ),
          1 => '',
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'lp_discount_amount',
            'label' => 'LBL_LP_DISCOUNT_AMOUNT',
          ),
          1 => '',
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'discount_percent',
            'label' => 'LBL_DISCOUNT_PERCENT',
          ),
          1 => 
          array (
            'name' => 'total_discount_amount',
            'label' => 'LBL_TOTAL_DISCOUNT_AMOUNT',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'lp_subtotal_amount',
            'label' => 'LBL_LP_SUBTOTAL_AMOUNT',
          ),
          1 => '',
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'lp_shipping_amount',
            'label' => 'LBL_LP_SHIPPING_AMOUNT',
            'displayParams' => 
            array (
              'field' => 
              array (
                'onblur' => 'calculateTotal(\'lineItems\');',
              ),
            ),
          ),
          1 => '',
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'shipping_tax_amt',
            'label' => 'LBL_SHIPPING_TAX_AMT',
          ),
          1 => '',
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'lp_tax_amount',
            'label' => 'LBL_LP_TAX_AMOUNT',
          ),
          1 => '',
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'lp_total_amount',
            'label' => 'LBL_LP_TOTAL_AMOUNT',
          ),
          1 => '',
        ),
        10 => 
        array (
          0 => 
          array (
            'name' => 'advance_payment',
            'label' => 'LBL_ADVANCE_PAYMENT',
          ),
          1 => 
          array (
            'name' => 'remaining_money',
            'label' => 'LBL_REMAINING_MONEY',
          ),
        ),
      ),
    ),
  ),
);
;
?>
