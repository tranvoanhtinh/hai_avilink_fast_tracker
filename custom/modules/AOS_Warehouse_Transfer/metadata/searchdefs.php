<?php
$module_name = 'AOS_Warehouse_Transfer';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'billing_contact' => 
      array (
        'type' => 'relate',
        'studio' => 'visible',
        'label' => 'LBL_BILLING_CONTACT',
        'id' => 'BILLING_CONTACT_ID',
        'link' => true,
        'width' => '10%',
        'default' => true,
        'name' => 'billing_contact',
      ),
      'mobileref' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_MOBILE_REF',
        'width' => '10%',
        'default' => true,
        'name' => 'mobileref',
      ),
      'current_user_only' => 
      array (
        'name' => 'current_user_only',
        'label' => 'LBL_CURRENT_USER_FILTER',
        'type' => 'bool',
        'width' => '10%',
        'default' => true,
      ),
    ),
    'advanced_search' => 
    array (
      'voucherdate' => 
      array (
        'type' => 'date',
        'label' => 'LBL_VOUCHER_DATE',
        'width' => '10%',
        'default' => true,
        'name' => 'voucherdate',
      ),
      'from_warehouse' => 
      array (
        'type' => 'enum',
        'label' => 'LBL_FROM_WAREHOUSE',
        'width' => '10%',
        'default' => true,
        'name' => 'from_warehouse',
      ),
      'to_warehouse' => 
      array (
        'type' => 'enum',
        'label' => 'LBL_TO_WAREHOUSE',
        'width' => '10%',
        'default' => true,
        'name' => 'to_warehouse',
      ),
      'customer_ref' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_CUSTOMER_REF',
        'width' => '10%',
        'default' => true,
        'name' => 'customer_ref',
      ),
      'assigned_user_id' => 
      array (
        'name' => 'assigned_user_id',
        'label' => 'LBL_ASSIGNED_TO',
        'type' => 'enum',
        'function' => 
        array (
          'name' => 'get_user_array',
          'params' => 
          array (
            0 => false,
          ),
        ),
        'default' => true,
        'width' => '10%',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
;
?>
