<?php
require_once('include/MVC/View/views/view.edit.php');

class CustomAOS_Warehouse_TransferViewEdit extends ViewEdit
{
    function display()
    {
        parent::display(); // Gọi phương thức hiển thị từ lớp cha
        ?>
        <style type="text/css">
        .yui-ac-content {
            width: 250px !important;
        }

        </style>
        <script src="include/javascript/custom/view.edit.js"></script>
        <script src="custom/modules/AOS_Warehouse_Transfer/js/edit.js"></script>
    
        <?php
    }
}
?>
