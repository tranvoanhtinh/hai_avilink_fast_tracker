<?php
// created: 2024-05-22 03:41:54
$mod_strings = array (
  'LNK_NEW_RECORD' => 'Create Product Categories',
  'LNK_LIST' => 'View Product Categories',
  'LBL_LIST_FORM_TITLE' => 'Product Categories List',
  'LBL_SEARCH_FORM_TITLE' => 'Search Product Categories',
  'LBL_PRODUCT_CATEGORYS_NAME' => 'Parent category',
  'LBL_AOS_PRODUCT_CATEGORIES_AOS_PRODUCTS_FROM_AOS_PRODUCTS_TITLE' => 'Product Categories: Product from Product Title',
  'LBL_SUB_CATEGORIES' => 'Sub categories',
  'LBL_PARENT_CATEGORY' => 'Parent Category',
  'LBL_HOMEPAGE_TITLE' => 'My Product Categories',
  'LBL_AOS_PRODUCT_CATEGORIES_ACCOUNTS_1_FROM_ACCOUNTS_TITLE' => 'Khách hàng',
);