<?php
// created: 2024-05-23 06:13:51
$mod_strings = array (
  'LNK_NEW_RECORD' => 'Tạo danh mục sản phẩm',
  'LNK_LIST' => 'Xem danh mục sản phẩm',
  'LBL_LIST_FORM_TITLE' => 'Danh sách danh mục sản phẩm',
  'LBL_SEARCH_FORM_TITLE' => 'Tìm kiếm Nhóm dịch vụ',
  'LBL_PRODUCT_CATEGORYS_NAME' => 'Parent category',
  'LBL_AOS_PRODUCT_CATEGORIES_AOS_PRODUCTS_FROM_AOS_PRODUCTS_TITLE' => 'Dịch vụ Categories: Dịch vụ from Dịch vụ Title',
  'LBL_SUB_CATEGORIES' => 'Sub categories',
  'LBL_PARENT_CATEGORY' => 'Parent Category',
  'LBL_HOMEPAGE_TITLE' => 'Nhóm dịch vụ của tôi',
  'LBL_ORDER_NUM' => 'STT',
  'LBL_AOS_PRODUCT_CATEGORIES_ACCOUNTS_1_FROM_ACCOUNTS_TITLE' => 'Đại lý',
);