<?php
require_once('service/v4_1/SugarWebServiceUtilv4_1.php');

class SugarWebServiceUtilv4_1_custom extends SugarWebServiceUtilv4_1
{
    function gen_sql_where_permission($module_name)
    {
        global $beanList, $beanFiles, $current_user, $sugar_config;
        $class_name = $beanList[$module_name];
        require_once($beanFiles[$class_name]);
        $seed = new $class_name();
        if ($seed->bean_implements('ACL') && ACLController::requireOwner($seed->module_dir, 'list')) {
            $owner_where = $seed->getOwnerWhere($current_user->id);
            if (empty($where)) {
                $where = $owner_where;
            } else {
                $where .= ' AND ' . $owner_where;
            }
        }
        /* BEGIN - SECURITY GROUPS */
        if (
            $seed->module_dir == 'Users' && !is_admin($current_user)
            && isset($sugar_config['securitysuite_filter_user_list'])
            && $sugar_config['securitysuite_filter_user_list']
        ) {
            require_once('modules/SecurityGroups/SecurityGroup.php');
            global $current_user;
            $group_where = SecurityGroup::getGroupUsersWhere($current_user->id);
            if (empty($where)) {
                $where = " (" . $group_where . ") ";
            } else {
                $where .= " AND (" . $group_where . ") ";
            }
        } elseif ($seed->bean_implements('ACL') && ACLController::requireSecurityGroup($seed->module_dir, 'list')) {
            require_once('modules/SecurityGroups/SecurityGroup.php');
            $owner_where = $seed->getOwnerWhere($current_user->id);
            $group_where = SecurityGroup::getGroupWhere($seed->table_name, $seed->module_dir, $current_user->id);
            if (!empty($owner_where)) {
                if (empty($where)) {
                    $where = " (" . $owner_where . " or " . $group_where . ") ";
                } else {
                    $where .= " AND (" . $owner_where . " or " . $group_where . ") ";
                }
            } else {
                $where .= ' AND ' . $group_where;
            }
        }
        /* END - SECURITY GROUPS */
        if (empty($where)) {
            $where = " 1=1";
        }
        return $where;
    }

    function gen_sql_where_date_range($field, $from_date, $to_date)
    {
        $from_date .= ' 00:00:00';
        $to_date .= ' 23:59:59';
        $where = $field . ' >= "' . $from_date . '" AND ' . $field . ' <= "' . $to_date . '"';
        return $where;
    }
}
