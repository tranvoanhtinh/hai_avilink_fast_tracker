<?php

use Reports\Filter\Conditions\Condition;

if (!defined('sugarEntry')) {
    define('sugarEntry', true);
}
require_once('service/v4_1/SugarWebServiceImplv4_1.php');
require_once('SugarWebServiceUtilv4_1_custom.php');

class SugarWebServiceImplv4_1_custom extends SugarWebServiceImplv4_1
{
    public function __construct()
    {
        self::$helperObject = new SugarWebServiceUtilv4_1_custom();
    }

    function summary_for_dashboard($session, $from_date = '', $to_date = '', $assigned_user_id = [])
    {
        if (!empty($assigned_user_id)) {
            $assigned_user_id = '("' . implode('","', $assigned_user_id) . '")';
        }
        $GLOBALS['log']->info('Begin: SugarWebServiceImplv4_1_custom->summary_for_dashboard');
        global $db, $app_list_strings;

        $error = new SoapError();
        if (!self::$helperObject->checkSessionAndModuleAccess($session, 'invalid_session', '', '', '', $error)) {
            $GLOBALS['log']->error('End: SugarWebServiceImplv4_1_custom->summary_for_dashboard - FAILED on checkSessionAndModuleAccess');
            return;
        } // if

        if (empty($from_date)) {
            $from_date = date('2000-01-01');
        }
        if (empty($to_date)) {
            $to_date = date('Y-m-d');
        }
        // Lấy cấu hình màu nền
        $bgcolor_qr = $db->query('SELECT type_language, key_language, `name`
            FROM lp_language_mobile_app
            WHERE deleted = 0 AND `type` = 2');
        $bgcolor_data = [];
        while ($row = $db->fetchByAssoc($bgcolor_qr)) {
            $bgcolor_data[$row['key_language']] = $row['name'];
        }
        // Lấy cấu hình màu chữ
        $textcolor_qr = $db->query('SELECT type_language, key_language, `name`
            FROM lp_language_mobile_app
            WHERE deleted = 0 AND `type` = 3');
        $textcolor_data = [];
        while ($row = $db->fetchByAssoc($textcolor_qr)) {
            $textcolor_data[$row['key_language']] = $row['name'];
        }
        // Thống kê tiềm năng, đang liên hệ (module liên quan: Leads)
        $owner_where = self::$helperObject->gen_sql_where_permission('Leads');
        $date_range_where = self::$helperObject->gen_sql_where_date_range('leads.date_entered', $from_date, $to_date);
        $cus_where = $owner_where . ' AND ' . $date_range_where;
        $in_process_status = ['In Process'];
        $new_status = ['New'];
        $dead_status = ['Dead'];
        if (!empty($assigned_user_id)) {
            $cus_where .= ' AND leads.assigned_user_id IN ' . $assigned_user_id;
        }
        $leads_amount_qr = $db->query('SELECT status, SUM(opportunity_amount) AS total_opportunity_amount, COUNT(id) AS num_lead 
            FROM leads 
            WHERE deleted = 0 AND ' . $cus_where . ' GROUP BY status');
        $new_amount = $in_process_amount = $dead_amount = 0;
        $new_count = $in_process_count = $dead_count = 0;
        while ($row = $db->fetchByAssoc($leads_amount_qr)) {
            if (in_array($row['status'], $in_process_status)) {
                $in_process_amount += $row['total_opportunity_amount'];
                $in_process_count += $row['num_lead'];
            } elseif (in_array($row['status'], $new_status)) {
                $new_amount += $row['total_opportunity_amount'];
                $new_count += $row['num_lead'];
            } elseif (in_array($row['status'], $dead_status)) {
                $dead_amount += $row['total_opportunity_amount'];
                $dead_count += $row['num_lead'];
            }
        }
        // Thống kê phát sinh nhu cầu và đã mua hàng (module liên quan: Accounts và Opportunities)
        $owner_where = self::$helperObject->gen_sql_where_permission('Accounts');
        $date_range_where = self::$helperObject->gen_sql_where_date_range('accounts.date_entered', $from_date, $to_date);
        $cus_where = $owner_where . ' AND ' . $date_range_where;
        if (!empty($assigned_user_id)) {
            $cus_where .= ' AND accounts.assigned_user_id IN ' . $assigned_user_id;
        }
        $account_with_op_status = ['Converted'];
        $account_with_transaction_status = ['Transaction'];
        $accounts_count_qr = $db->query('SELECT ac.lead_account_status_c, COUNT(accounts.id) AS num_account 
            FROM accounts INNER JOIN accounts_cstm ac ON accounts.id = ac.id_c 
            WHERE accounts.deleted = 0 AND ' . $cus_where . ' GROUP BY ac.lead_account_status_c');
        $account_with_op_count = $account_with_transaction_count = 0;
        while ($row = $db->fetchByAssoc($accounts_count_qr)) {
            if (in_array($row['lead_account_status_c'], $account_with_op_status)) {
                $account_with_op_count += $row['num_account'];
            }
            if (in_array($row['lead_account_status_c'], $account_with_transaction_status)) {
                $account_with_transaction_count += $row['num_account'];
            }
        }
        $acc_op_amount_status = ['Negotiation/Review', 'Proposal/Price Quote', 'Prospecting'];
        $op_won_status = ['Closed Won'];
        $acc_op_amount_qr = $db->query('SELECT op.sales_stage, SUM(op.amount) AS total_amount
            FROM accounts INNER JOIN accounts_opportunities aop ON accounts.id = aop.account_id AND aop.deleted = 0
            INNER JOIN opportunities op ON op.id = aop.opportunity_id
            WHERE accounts.deleted = 0 AND ' . $cus_where . ' AND op.deleted = 0
            GROUP BY op.sales_stage');
        $acc_op_amount = $acc_op_won_amount = 0;
        while ($row = $db->fetchByAssoc($acc_op_amount_qr)) {
            if (in_array($row['sales_stage'], $acc_op_amount_status)) {
                $acc_op_amount += $row['total_amount'];
            }
            if (in_array($row['sales_stage'], $op_won_status)) {
                $acc_op_won_amount += $row['total_amount'];
            }
        }
        // Thống kê cơ hội bán hàng (module liên quan: Opportunities)
        $owner_where = self::$helperObject->gen_sql_where_permission('Opportunities');
        $date_range_where = self::$helperObject->gen_sql_where_date_range('date_closed', $from_date, $to_date);
        $cus_where = $owner_where . ' AND ' . $date_range_where;
        if (!empty($assigned_user_id)) {
            $cus_where .= ' AND opportunities.assigned_user_id IN ' . $assigned_user_id;
        }
        $op_amount_qr = $db->query('SELECT sales_stage, COUNT(id) AS num, SUM(amount) AS total_amount
            FROM opportunities
            WHERE deleted = 0 AND ' . $cus_where . '
            GROUP BY sales_stage');
        $sales_opportunity_summary = [];
        while ($row = $db->fetchByAssoc($op_amount_qr)) {
            if (empty($row['sales_stage'])) {
                continue;
            }
            $sales_opportunity_summary[] = [
                'label' => isset($app_list_strings['sales_stage_dom'][$row['sales_stage']]) ? $app_list_strings['sales_stage_dom'][$row['sales_stage']] : '',
                'value' => $row['sales_stage'],
                'bgcolor' => !empty($bgcolor_data[$row['sales_stage']]) ? $bgcolor_data[$row['sales_stage']] : '#ffffff',
                'txtcolor' => !empty($textcolor_data[$row['sales_stage']]) ? $textcolor_data[$row['sales_stage']] : '#000000',
                'count' => $row['num'],
                'amount' => $row['total_amount'],
            ];
        }
        array_multisort($sales_opportunity_summary);
        // Thống kê đơn hàng
        $owner_where = self::$helperObject->gen_sql_where_permission('AOS_Invoices');
        $date_range_where = self::$helperObject->gen_sql_where_date_range('invoice_date', $from_date, $to_date);
        $cus_where = $owner_where . ' AND ' . $date_range_where;
        if (!empty($assigned_user_id)) {
            $cus_where .= ' AND aos_invoices.assigned_user_id IN ' . $assigned_user_id;
        }
        $order_summary_qr = $db->query('SELECT `status`, COUNT(id) AS num, SUM(total_amount) AS total_amount
            FROM aos_invoices
            WHERE deleted = 0 AND ' . $cus_where . '
            GROUP BY `status`');
        $order_summary = [];
        while ($row = $db->fetchByAssoc($order_summary_qr)) {
            if (empty($row['status'])) {
                continue;
            }
            $order_summary[] = [
                'label' => isset($app_list_strings['invoice_status_dom'][$row['status']]) ? $app_list_strings['invoice_status_dom'][$row['status']] : '',
                'value' => $row['status'],
                'bgcolor' => !empty($bgcolor_data[$row['status']]) ? $bgcolor_data[$row['status']] : '#ffffff',
                'txtcolor' => !empty($textcolor_data[$row['status']]) ? $textcolor_data[$row['status']] : '#000000',
                'count' => $row['num'],
                'amount' => $row['total_amount'],
            ];
        }
        array_multisort($order_summary);
        // Thống kê hợp đồng
        $owner_where = self::$helperObject->gen_sql_where_permission('AOS_Contracts');
        $date_range_where = self::$helperObject->gen_sql_where_date_range('start_date', $from_date, $to_date);
        $cus_where = $owner_where . ' AND ' . $date_range_where;
        if (!empty($assigned_user_id)) {
            $cus_where .= ' AND aos_contracts.assigned_user_id IN ' . $assigned_user_id;
        }
        $contract_summary = [];
        $contract_summary_qr = $db->query('SELECT `status`, COUNT(id) AS num, SUM(total_contract_value) AS total_amount
            FROM aos_contracts
            WHERE deleted = 0 AND ' . $cus_where . '
            GROUP BY `status`');
        while ($row = $db->fetchByAssoc($contract_summary_qr)) {
            if (empty($row['status'])) {
                continue;
            }
            $contract_summary[] = [
                'label' => isset($app_list_strings['contract_status_list'][$row['status']]) ? $app_list_strings['contract_status_list'][$row['status']] : '',
                'value' => $row['status'],
                'bgcolor' => !empty($bgcolor_data[$row['status']]) ? $bgcolor_data[$row['status']] : '#ffffff',
                'txtcolor' => !empty($textcolor_data[$row['status']]) ? $textcolor_data[$row['status']] : '#000000',
                'count' => $row['num'],
                'amount' => $row['total_amount'],
            ];
        }
        array_multisort($contract_summary);
        // Thống kê báo giá
        $owner_where = self::$helperObject->gen_sql_where_permission('AOS_Quotes');
        $date_range_where = self::$helperObject->gen_sql_where_date_range('datequote_c', $from_date, $to_date);
        $cus_where = $owner_where . ' AND ' . $date_range_where;
        if (!empty($assigned_user_id)) {
            $cus_where .= ' AND aos_quotes.assigned_user_id IN ' . $assigned_user_id;
        }
        $quotation_summary = [];
        $quotation_summary_qr = $db->query('SELECT stage, COUNT(id) AS num, SUM(total_amount) AS total_amount
            FROM aos_quotes
            INNER JOIN aos_quotes_cstm ON aos_quotes_cstm.id_c = aos_quotes.id
            WHERE deleted = 0 AND ' . $cus_where . '
            GROUP BY stage');
        while ($row = $db->fetchByAssoc($quotation_summary_qr)) {
            if (empty($row['stage'])) {
                continue;
            }
            $quotation_summary[] = [
                'label' => isset($app_list_strings['quote_stage_dom'][$row['stage']]) ? $app_list_strings['quote_stage_dom'][$row['stage']] : '',
                'value' => $row['stage'],
                'bgcolor' => !empty($bgcolor_data[$row['stage']]) ? $bgcolor_data[$row['stage']] : '#ffffff',
                'txtcolor' => !empty($textcolor_data[$row['stage']]) ? $textcolor_data[$row['stage']] : '#000000',
                'count' => $row['num'],
                'amount' => $row['total_amount'],
            ];
        }
        array_multisort($quotation_summary);
        // Thống kê cuộc gọi
        $owner_where = self::$helperObject->gen_sql_where_permission('Calls');
        $date_range_where = self::$helperObject->gen_sql_where_date_range('date_start', $from_date, $to_date);
        $cus_where = $owner_where . ' AND ' . $date_range_where;
        if (!empty($assigned_user_id)) {
            $cus_where .= ' AND calls.assigned_user_id IN ' . $assigned_user_id;
        }
        $call_summary = [];
        $call_summary_qr = $db->query('SELECT `status`, COUNT(id) AS num
            FROM calls
            WHERE deleted = 0 AND ' . $cus_where . '
            GROUP BY `status`');
        while ($row = $db->fetchByAssoc($call_summary_qr)) {
            if (empty($row['status'])) {
                continue;
            }
            $call_summary[] = [
                'label' => isset($app_list_strings['call_status_dom'][$row['status']]) ? $app_list_strings['call_status_dom'][$row['status']] : '',
                'value' => $row['status'],
                'bgcolor' => !empty($bgcolor_data[$row['status']]) ? $bgcolor_data[$row['status']] : '#ffffff',
                'txtcolor' => !empty($textcolor_data[$row['status']]) ? $textcolor_data[$row['status']] : '#000000',
                'count' => $row['num'],
            ];
        }
        array_multisort($call_summary);
        // Thống kê cuộc gặp
        $owner_where = self::$helperObject->gen_sql_where_permission('Meetings');
        $date_range_where = self::$helperObject->gen_sql_where_date_range('date_start', $from_date, $to_date);
        $cus_where = $owner_where . ' AND ' . $date_range_where;
        if (!empty($assigned_user_id)) {
            $cus_where .= ' AND meetings.assigned_user_id IN ' . $assigned_user_id;
        }
        $meet_summary = [];
        $meet_summary_qr = $db->query('SELECT `status`, COUNT(id) AS num
            FROM meetings
            WHERE deleted = 0 AND ' . $cus_where . '
            GROUP BY `status`');
        while ($row = $db->fetchByAssoc($meet_summary_qr)) {
            if (empty($row['status'])) {
                continue;
            }
            $meet_summary[] = [
                'label' => isset($app_list_strings['meeting_status_dom'][$row['status']]) ? $app_list_strings['meeting_status_dom'][$row['status']] : '',
                'value' => $row['status'],
                'bgcolor' => !empty($bgcolor_data[$row['status']]) ? $bgcolor_data[$row['status']] : '#ffffff',
                'txtcolor' => !empty($textcolor_data[$row['status']]) ? $textcolor_data[$row['status']] : '#000000',
                'count' => $row['num'],
            ];
        }
        array_multisort($meet_summary);
        // Thống kê công việc
        $owner_where = self::$helperObject->gen_sql_where_permission('Tasks');
        $date_range_where = self::$helperObject->gen_sql_where_date_range('date_start', $from_date, $to_date);
        $cus_where = $owner_where . ' AND ' . $date_range_where;
        if (!empty($assigned_user_id)) {
            $cus_where .= ' AND tasks.assigned_user_id IN ' . $assigned_user_id;
        }
        $task_summary = [];
        $task_summary_qr = $db->query('SELECT `status`, COUNT(id) AS num
            FROM tasks
            WHERE deleted = 0 AND ' . $cus_where . '
            GROUP BY `status`');
        while ($row = $db->fetchByAssoc($task_summary_qr)) {
            if (empty($row['status'])) {
                continue;
            }
            $task_summary[] = [
                'label' => isset($app_list_strings['task_status_dom'][$row['status']]) ? $app_list_strings['task_status_dom'][$row['status']] : '',
                'value' => $row['status'],
                'bgcolor' => !empty($bgcolor_data[$row['status']]) ? $bgcolor_data[$row['status']] : '#ffffff',
                'txtcolor' => !empty($textcolor_data[$row['status']]) ? $textcolor_data[$row['status']] : '#000000',
                'count' => $row['num'],
            ];
        }
        array_multisort($task_summary);
        // Thống kê ticket
        $owner_where = self::$helperObject->gen_sql_where_permission('Cases');
        $date_range_where = self::$helperObject->gen_sql_where_date_range('date_entered', $from_date, $to_date);
        $cus_where = $owner_where . ' AND ' . $date_range_where;
        if (!empty($assigned_user_id)) {
            $cus_where .= ' AND cases.assigned_user_id IN ' . $assigned_user_id;
        }
        $ticket_summary = [];
        $ticket_summary_qr = $db->query('SELECT `state`, COUNT(id) AS num
            FROM cases
            WHERE deleted = 0 AND ' . $cus_where . '
            GROUP BY `state`');
        while ($row = $db->fetchByAssoc($ticket_summary_qr)) {
            if (empty($row['state'])) {
                continue;
            }
            $ticket_summary[] = [
                'label' => isset($app_list_strings['case_state_dom'][$row['state']]) ? $app_list_strings['case_state_dom'][$row['state']] : '',
                'value' => $row['state'],
                'bgcolor' => !empty($bgcolor_data[$row['state']]) ? $bgcolor_data[$row['state']] : '#ffffff',
                'txtcolor' => !empty($textcolor_data[$row['state']]) ? $textcolor_data[$row['state']] : '#000000',
                'count' => $row['num'],
            ];
        }
        array_multisort($ticket_summary);
        // Thống kê thanh toán
        $owner_where = self::$helperObject->gen_sql_where_permission('bill_Billing');
        $date_range_where = self::$helperObject->gen_sql_where_date_range('billingdate', $from_date, $to_date);
        $cus_where = $owner_where . ' AND ' . $date_range_where;
        if (!empty($assigned_user_id)) {
            $cus_where .= ' AND bill_billing.assigned_user_id IN ' . $assigned_user_id;
        }
        $billing_3_summary = $billing_payment_summary = $billing_receipts_summary = [];
        $billing_summary_qr = $db->query('SELECT `type`, billing_status, COUNT(id) AS num, SUM(billingamount) AS total_amount
            FROM bill_billing
            WHERE deleted = 0 AND ' . $cus_where . '
            GROUP BY `type`, billing_status');
        while ($row = $db->fetchByAssoc($billing_summary_qr)) {
            if (empty($row['type'])) {
                continue;
            }
            $tmp = [
                'label' => isset($app_list_strings['billing_status_list'][$row['billing_status']]) ? $app_list_strings['billing_status_list'][$row['billing_status']] : '',
                'value' => $row['state'],
                'bgcolor' => !empty($bgcolor_data[$row['billing_status']]) ? $bgcolor_data[$row['billing_status']] : '#ffffff',
                'txtcolor' => !empty($textcolor_data[$row['billing_status']]) ? $textcolor_data[$row['billing_status']] : '#000000',
                'count' => $row['num'],
                'amount' => $row['total_amount'],
            ];
            if ($row['type'] == 'payment') {
                $billing_payment_summary[] = $tmp;
            } elseif ($row['type'] == 'receipts') {
                $billing_receipts_summary[] = $tmp;
            } elseif ($row['type'] == '3') {
                $billing_3_summary[] = $tmp;
            }
        }
        array_multisort($billing_3_summary);
        array_multisort($billing_payment_summary);
        array_multisort($billing_receipts_summary);
        $billing_summary = [
            'billing_3_summary' => $billing_3_summary,
            'billing_payment_summary' => $billing_payment_summary,
            'billing_receipts_summary' => $billing_receipts_summary,
        ];
        $GLOBALS['log']->info('End: SugarWebServiceImplv4_1_custom->summary_for_dashboard - SUCCESS');
        $rt = [
            'num_new_lead' => $new_count,
            'new_lead_amount' => $new_amount,
            'num_in_process_lead' => $in_process_count,
            'in_process_lead_amount' => $in_process_amount,
            'num_dead_lead' => $dead_count,
            'dead_lead_amount' => $dead_amount,
            'num_account' => $account_with_op_count,
            'account_opportunity_amount' => $acc_op_amount,
            'num_account_has_transaction' => $account_with_transaction_count,
            'account_opportunity_won_amount' => $acc_op_won_amount,
            'sales_opportunity_summary' => $sales_opportunity_summary,
            'order_summary' => $order_summary,
            'billing_summary' => $billing_summary,
            'quotation_summary' => $quotation_summary,
            'contract_summary' => $contract_summary,
            'call_summary' => $call_summary,
            'meet_summary' => $meet_summary,
            'task_summary' => $task_summary,
            'ticket_summary' => $ticket_summary,
        ];
        return $rt;
    }

    function check_user_login_app($username, $siteurl)
    {
        $GLOBALS['log']->info('Begin: SugarWebServiceImplv4_1_custom->check_user_login_app');
        global  $db;
        $data = $db->fetchOne('SELECT siteurl
        FROM mba_mobileappcontrol
        WHERE name = "' . $username . '" AND deleted = 0 AND STATUS = 1 AND (siteurl = "http://' . $siteurl . '" OR siteurl = "https://' . $siteurl . '")');
        $rt_siteurl = '';
        $valid = false;
        if (!empty($data['siteurl'])) {
            $rt_siteurl = $data['siteurl'];
            $valid = true;
        }
        $GLOBALS['log']->info('End: SugarWebServiceImplv4_1_custom->check_user_login_app - SUCCESS');
        return [
            'valid' => $valid,
            'siteurl' => $rt_siteurl,
        ];
    }

    function get_data_for_list($session, $list_key)
    {
        $GLOBALS['log']->info('Begin: SugarWebServiceImplv4_1_custom->get_data_for_list');
        global $app_list_strings;
        $error = new SoapError();
        if (!self::$helperObject->checkSessionAndModuleAccess($session, 'invalid_session', '', '', '', $error)) {
            $GLOBALS['log']->error('End: SugarWebServiceImplv4_1_custom->get_data_for_list - FAILED on checkSessionAndModuleAccess');
            return;
        }
        $GLOBALS['log']->info('End: SugarWebServiceImplv4_1_custom->get_data_for_list - SUCCESS');
        return isset($app_list_strings[$list_key]) ? $app_list_strings[$list_key] : [];
    }

    function get_language_for_app($type_language)
    {
        $GLOBALS['log']->info('Begin: SugarWebServiceImplv4_1_custom->get_language_for_app');
        global  $db;
        $language_qr = $db->query('SELECT type_language, key_language, `name`
            FROM lp_language_mobile_app
            WHERE deleted = 0 AND `type` = 1 AND type_language = "' . $type_language . '"');
        $language_data = [];
        while ($row = $db->fetchByAssoc($language_qr)) {
            $language_data[$row['key_language']] = $row['name'];
        }
        $GLOBALS['log']->info('End: SugarWebServiceImplv4_1_custom->get_language_for_app - SUCCESS');
        return $language_data;
    }

    function check_duplicate_phone_mobile($phone_mobile, $old_phone_mobile = null)
    {
        $GLOBALS['log']->info('Begin: SugarWebServiceImplv4_1_custom->check_duplicate_phone_mobile');
        $phone_mobile = trim($phone_mobile);
        $old_phone_mobile = trim($old_phone_mobile);
        global  $db;
        $more_where_1 = $more_where_2 = '';
        if (!empty($old_phone_mobile)) {
            $more_where_1 .= ' AND phone_mobile <> "' . $old_phone_mobile . '"';
            $more_where_2 .= ' AND phone_alternate <> "' . $old_phone_mobile . '"';
        }
        $duplicate = false;
        $data = $db->fetchOne('SELECT id FROM leads WHERE deleted = 0 AND phone_mobile = "' . $phone_mobile . '"' . $more_where_1);
        if (empty($data['id'])) {
            $data = $db->fetchOne('SELECT id FROM accounts WHERE deleted = 0 AND phone_alternate = "' . $phone_mobile . '"' . $more_where_2);
            if (empty($data['id'])) {
                $data = $db->fetchOne('SELECT id FROM contacts WHERE deleted = 0 AND phone_mobile = "' . $phone_mobile . '"' . $more_where_1);
                if (empty($data['id'])) {
                    $data = $db->fetchOne('SELECT id FROM prospects WHERE deleted = 0 AND phone_mobile = "' . $phone_mobile . '"' . $more_where_1);
                    if (!empty($data['id'])) {
                        $duplicate = true;
                    }
                } else {
                    $duplicate = true;
                }
            } else {
                $duplicate = true;
            }
        } else {
            $duplicate = true;
        }
        $GLOBALS['log']->info('End: SugarWebServiceImplv4_1_custom->check_duplicate_phone_mobile - SUCCESS');
        return [
            'duplicate' => $duplicate,
            'phone_mobile' => $phone_mobile,
        ];
    }

    function check_duplicate_email($email, $old_email)
    {
        $GLOBALS['log']->info('Begin: SugarWebServiceImplv4_1_custom->check_duplicate_email');
        $email = trim($email);
        $old_email = trim($old_email);
        global $db;
        $more_where = '';
        if (!empty($old_email)) {
            $more_where .= ' AND email_addresses.email_address <> "' . $old_email . '"';
        }
        $duplicate = false;
        $module = '';
        $bean_id = '';
        $data = $db->fetchOne('SELECT email_addr_bean_rel.bean_module, email_addr_bean_rel.bean_id, email_addresses.email_address
            FROM email_addresses
            INNER JOIN email_addr_bean_rel ON email_addresses.id=email_addr_bean_rel.email_address_id
            WHERE email_addr_bean_rel.deleted = 0 AND email_addresses.deleted = 0 AND email_addr_bean_rel.bean_module IN ("Leads", "Accounts", "Contacts", "Prospects") AND email_addresses.email_address = "' . $email . '"' . $more_where);
        if (!empty($data['email_address'])) {
            $duplicate = true;
            $module = $data['bean_module'];
            $bean_id = $data['bean_id'];
        }
        $GLOBALS['log']->info('End: SugarWebServiceImplv4_1_custom->check_duplicate_email - SUCCESS');
        return [
            'duplicate' => $duplicate,
            'email' => $email,
            'module' => $module,
            'bean_id' => $bean_id,
        ];
    }

    function check_duplicate_identitycard($identitycard, $old_identitycard = null)
    {
        $GLOBALS['log']->info('Begin: SugarWebServiceImplv4_1_custom->check_duplicate_identitycard');
        $identitycard = trim($identitycard);
        $old_identitycard = trim($old_identitycard);
        global $db;
        $more_where_account = $more_where_lead = '';
        if (!empty($old_identitycard)) {
            $more_where_lead .= ' AND leads_cstm.identitycard_c <> "' . $old_identitycard . '"';
            $more_where_account .= ' AND accounts_cstm.identitycard_c <> "' . $old_identitycard . '"';
        }
        $duplicate = false;
        $data = $db->fetchOne('SELECT id FROM leads INNER JOIN leads_cstm ON leads.id = leads_cstm.id_c WHERE leads.deleted = 0 AND leads_cstm.identitycard_c = "' . $identitycard . '"' . $more_where_lead);
        if (empty($data['id'])) {
            $data = $db->fetchOne('SELECT id FROM accounts INNER JOIN accounts_cstm ON accounts.id = accounts_cstm.id_c WHERE accounts.deleted = 0 AND accounts_cstm.identitycard_c = "' . $identitycard . '"' . $more_where_account);
            if (!empty($data['id'])) {
                $duplicate = true;
            }
        } else {
            $duplicate = true;
        }
        $GLOBALS['log']->info('End: SugarWebServiceImplv4_1_custom->check_duplicate_identitycard - SUCCESS');
        return [
            'duplicate' => $duplicate,
            'identitycard' => $identitycard,
        ];
    }

    function check_duplicate_taxcode($taxcode, $old_taxcode = null)
    {
        $GLOBALS['log']->info('Begin: SugarWebServiceImplv4_1_custom->check_duplicate_taxcode');
        $taxcode = trim($taxcode);
        $old_taxcode = trim($old_taxcode);
        global $db;
        $more_where_lead = $more_where_account = '';
        if (!empty($old_taxcode)) {
            $more_where_lead .= ' AND leads_cstm.vatcode_c <> "' . $old_taxcode . '"';
            $more_where_account .= ' AND tax_code <> "' . $old_taxcode . '"';
        }
        $duplicate = false;
        $data = $db->fetchOne('SELECT id FROM leads INNER JOIN leads_cstm ON leads.id = leads_cstm.id_c WHERE leads.deleted = 0 AND leads_cstm.vatcode_c = "' . $taxcode . '"' . $more_where_lead);
        if (empty($data['id'])) {
            $data = $db->fetchOne('SELECT id FROM accounts WHERE deleted = 0 AND tax_code = "' . $taxcode . '"' . $more_where_account);
            if (!empty($data['id'])) {
                $duplicate = true;
            }
        } else {
            $duplicate = true;
        }
        $GLOBALS['log']->info('End: SugarWebServiceImplv4_1_custom->check_duplicate_taxcode - SUCCESS');
        return [
            'duplicate' => $duplicate,
            'taxcode' => $taxcode,
        ];
    }

    function convert_lead($session, $lead_id)
    {
        $GLOBALS['log']->info('Begin: SugarWebServiceImplv4_1_custom->convert_lead');
        $error = new SoapError();
        if (!self::$helperObject->checkSessionAndModuleAccess($session, 'invalid_session', '', '', '', $error)) {
            $GLOBALS['log']->error('End: SugarWebServiceImplv4_1_custom->convert_lead - FAILED on checkSessionAndModuleAccess');
            return;
        } // if
        $convert_status = 'fail';
        $lead = BeanFactory::getBean('Leads', $lead_id);
        if (empty($lead->fetched_row['id'])) {
            return [
                'convert_status' => $convert_status,
                'error' => 'Lead not found',
            ];
        }
        if ($lead->fetched_row['status'] == 'Converted') {
            return [
                'convert_status' => $convert_status,
                'error' => 'Lead was converted',
            ];
        }
        $lead->status = "Converted";
        $lead->converted = '1';
        $lead->in_workflow = true;
        $lead->save();
        $convert_status = 'success';

        global $db;
        $lead_updated = $db->fetchOne('SELECT account_id, contact_id, opportunity_id FROM leads WHERE id = "' . $lead_id . '"');

        $GLOBALS['log']->info('End: SugarWebServiceImplv4_1_custom->convert_lead - SUCCESS');
        return [
            'convert_status' => $convert_status,
            'lead_id' => $lead_id,
            'account_id' => $lead_updated['account_id'],
            'contact_id' => $lead_updated['contact_id'],
            'opportunity_id' => $lead_updated['opportunity_id'],
        ];
    }

    function set_call($session, $call_data = array())
    {
        $GLOBALS['log']->info('Begin: SugarWebServiceImplv4_1_custom->set_call');
        global $timedate;
        $error = new SoapError();
        $module_name = 'Calls';
        if (!self::$helperObject->checkSessionAndModuleAccess(
            $session,
            'invalid_session',
            $module_name,
            'write',
            'no_access',
            $error
        )) {
            $GLOBALS['log']->info('End: SugarWebServiceImplv4_1_custom->set_call');

            return;
        } // if
        if (empty($call_data['name']) || empty($call_data['date_start']) || empty($call_data['time_start'])) {
            return [
                'error' => 'name, date_start, time_start are required',
            ];
        }
        $check_notify = false;
        $call_obj = new Call();
        $call_bean = BeanFactory::newBean('Calls');
        if (!empty($call_data['id'])) {
            $old_record = $call_obj->retrieve($call_data['id']);
            if (empty($old_record->id)) {
                return [
                    'error' => 'Call not found',
                ];
            }
            $old_assigned_user_id = $old_record->assigned_user_id;
            $call_bean = BeanFactory::getBean($module_name, $call_data['id']);
        }
        if ((empty($call_bean->id) && isset($call_data['assigned_user_id']) && !empty($call_data['assigned_user_id']) && $GLOBALS['current_user']->id != $call_data['assigned_user_id']) || (isset($old_assigned_user_id) && !empty($old_assigned_user_id) && isset($call_data['assigned_user_id']) && !empty($call_data['assigned_user_id']) && $old_assigned_user_id != $call_data['assigned_user_id'])) {
            $call_bean->special_notification = true;
            if (!isset($GLOBALS['resavingRelatedBeans']) || $GLOBALS['resavingRelatedBeans'] == false) {
                $check_notify = true;
            }
        }
        $datetime_start = $call_data['date_start'] . 'T' . $call_data['time_start'] . '+07:00';
        $datetime_start = $timedate->fromString($datetime_start);
        $call_bean->name = $call_data['name'];
        $call_bean->description = $call_data['description'];
        $call_bean->assigned_user_id = $call_data['assigned_user_id'];
        $call_bean->date_start = $datetime_start->asDb();
        $call_bean->parent_type = $call_data['parent_type'];
        $call_bean->parent_id = $call_data['parent_id'];
        $call_bean->is_completed = $call_data['is_completed'];
        $call_bean->is_important = $call_data['is_important'];
        $call_bean->status_activites = $call_data['status_activites'];
        if ($call_bean->is_completed) {
            $call_bean->status = 'Held';
        } else {
            $call_bean->status = $call_obj->getDefaultStatus();
        }
        $call_id = $call_bean->save($check_notify);
        if (!empty($call_data['reminders_data'])) {
            $reminders_data = json_encode($call_obj->removeUnInvitedFromReminders($call_data['reminders_data']));
            Reminder::saveRemindersDataJson($module_name, $call_id, $reminders_data);
        }
        $GLOBALS['log']->info('End: SugarWebServiceImplv4_1_custom->set_call - SUCCESS');
        return [
            'call_id' => $call_id,
            'call_data' => $call_data,
        ];
    }

    function set_meeting($session, $meeting_data = array())
    {
        $GLOBALS['log']->info('Begin: SugarWebServiceImplv4_1_custom->set_meeting');
        global $timedate;
        $error = new SoapError();
        $module_name = 'Meetings';
        if (!self::$helperObject->checkSessionAndModuleAccess(
            $session,
            'invalid_session',
            $module_name,
            'write',
            'no_access',
            $error
        )) {
            $GLOBALS['log']->info('End: SugarWebServiceImplv4_1_custom->set_meeting');

            return;
        } // if
        if (empty($meeting_data['name']) || empty($meeting_data['date_start']) || empty($meeting_data['time_start'])) {
            return [
                'error' => 'name, date_start, time_start are required',
            ];
        }
        $check_notify = false;
        $meeting_obj = new Meeting();
        $meeting_bean = BeanFactory::newBean($module_name);
        if (!empty($meeting_data['id'])) {
            $old_record = $meeting_obj->retrieve($meeting_data['id']);
            if (empty($old_record->id)) {
                return [
                    'error' => 'Meeting not found',
                ];
            }
            $old_assigned_user_id = $old_record->assigned_user_id;
            $meeting_bean = BeanFactory::getBean($module_name, $meeting_data['id']);
        }
        if ((empty($meeting_bean->id) && isset($meeting_data['assigned_user_id']) && !empty($meeting_data['assigned_user_id']) && $GLOBALS['current_user']->id != $meeting_data['assigned_user_id']) || (isset($old_assigned_user_id) && !empty($old_assigned_user_id) && isset($meeting_data['assigned_user_id']) && !empty($meeting_data['assigned_user_id']) && $old_assigned_user_id != $meeting_data['assigned_user_id'])) {
            $meeting_bean->special_notification = true;
            if (!isset($GLOBALS['resavingRelatedBeans']) || $GLOBALS['resavingRelatedBeans'] == false) {
                $check_notify = true;
            }
        }
        $datetime_start = $meeting_data['date_start'] . 'T' . $meeting_data['time_start'] . '+07:00';
        $datetime_start = $timedate->fromString($datetime_start);
        $meeting_bean->name = $meeting_data['name'];
        $meeting_bean->description = $meeting_data['description'];
        $meeting_bean->assigned_user_id = $meeting_data['assigned_user_id'];
        $meeting_bean->date_start = $datetime_start->asDb();
        $meeting_bean->parent_type = $meeting_data['parent_type'];
        $meeting_bean->parent_id = $meeting_data['parent_id'];
        $meeting_bean->is_completed = $meeting_data['is_completed'];
        $meeting_bean->is_important = $meeting_data['is_important'];
        $meeting_bean->location = $meeting_data['location'];
        $meeting_bean->status_activites = $meeting_data['status_activites'];
        if ($meeting_bean->is_completed) {
            $meeting_bean->status = 'Held';
        } else {
            $meeting_bean->status = $meeting_obj->getDefaultStatus();
        }
        $meeting_id = $meeting_bean->save($check_notify);
        if (!empty($meeting_data['reminders_data'])) {
            $reminders_data = json_encode($meeting_obj->removeUnInvitedFromReminders($meeting_data['reminders_data']));
            Reminder::saveRemindersDataJson($module_name, $meeting_id, $reminders_data);
        }
        $GLOBALS['log']->info('End: SugarWebServiceImplv4_1_custom->set_meeting - SUCCESS');
        return [
            'meeting_id' => $meeting_id,
            'meeting_data' => $meeting_data,
        ];
    }

    function get_auto_code($session, $info_data = array(), $authenticated_user_id)
    {

        $GLOBALS['log']->info('Begin: SugarWebServiceImplv4_1_custom->get_auto_code');
        global $timedate;
        $error = new SoapError();

        if (!self::$helperObject->checkSessionAndModuleAccess($session, 'invalid_session', '', '', '', $error)) {
            $GLOBALS['log']->info('End: SugarWebServiceImplv4_1_custom->get_auto_code');
            return;
        }

        if (empty($info_data['table_name']) || empty($info_data['voucher_date']) || empty($authenticated_user_id)) {
            return ['error' => 'table_name, authenticated_user_id, voucher_date are required',];
        }
        global $db;

        $type_sys_partner = $GLOBALS['app_list_strings']['type_sys_partner_list'];

        $module = $info_data['table_name'];
        $voucher_date = $info_data['voucher_date'];


        if (isset($type_sys_partner[$module])) {
            $sql_check_user = "select tttt_worldfonepbx.* from tttt_worldfonepbx inner join tttt_worldfonepbx_users_1_c on tttt_worldfonepbx_users_1_c.tttt_worldfonepbx_users_1tttt_worldfonepbx_ida = tttt_worldfonepbx.id and tttt_worldfonepbx_users_1_c.deleted = '0' left join tttt_worldfonepbx_cstm on tttt_worldfonepbx.id = tttt_worldfonepbx_cstm.id_c where tttt_worldfonepbx_users_1_c.tttt_worldfonepbx_users_1users_idb = '" . $authenticated_user_id . "' and tttt_worldfonepbx.is_status = '1' AND tttt_worldfonepbx.type_sys_app = '13' And tttt_worldfonepbx.deleted = '0' AND tttt_worldfonepbx.type_sys_partner ='" . $module . "'";
            if (isset($info_data['type_voucher'])) {
                $type_voucher = $info_data['type_voucher'];
                $sql_check_user .= "AND swiftcode ='" . $type_voucher . "'";
            }
            $result_check_user = $db->query($sql_check_user);
            $num_rows = $result_check_user->num_rows;
        }
        $date_parts = explode('/', $voucher_date);
        $day = $date_parts[0];
        $month = $date_parts[1];
        $year = $date_parts[2];
        $yyyy_mm_dd = $year . '-' . $month . '-' . $day;

        if ($num_rows > 0) {
            $row_worldfonepbx = $result_check_user->fetch_assoc();
            $set_data[$module] = array(
                'code' => $row_worldfonepbx['psw_callcenter'],
                'type_date' => $row_worldfonepbx['calluuid'],
            );
        } else {
            return;
        }
        $col = 'name';
        $where_date = 'voucherdate';
        if ($module == 'accounts') {
            $col = 'account_code';
            $where_date = 'date_entered';
        }
        if ($module == 'aos_invoices') {
            $where_date = 'invoice_date';
        }

        if ($module == 'aos_contracts') {
            $where_date = 'start_date';
        }
        if ($module == 'aos_products') {
            $where_date = 'date_entered';
            $col = 'part_number';
        }
        if ($module == 'bill_billing') {
            $where_date = 'billingdate';
        }
        if ($module == 'aos_quotes') {
            $get_autonum  = "SELECT *
            FROM $module
            JOIN aos_quotes_cstm ON $module.id = aos_quotes_cstm.id_c
            WHERE DATE(datequote_c) = '" . $yyyy_mm_dd . "'AND deleted = 0";
        } else {

            $get_autonum = "SELECT $col FROM $module WHERE DATE(" . $where_date . ") = '" . $yyyy_mm_dd . "'AND deleted = 0 ";
        }
        $result = $db->query($get_autonum);
        $largest_number = null;

        $start_code = $set_data[$module]['code'];

        while ($row = $db->fetchByAssoc($result)) {
            $name = $row[$col];
            $parts = explode('.', $name);
            $number = end($parts);

            if (strpos($name, $start_code) === 0) { // Kiểm tra xem $start_code có là một phần đầu của $name hay không
                if (ctype_digit($number)) {
                    // Nếu $largest_number chưa được gán hoặc $number lớn hơn $largest_number
                    if ($largest_number === null || $number > $largest_number) {
                        $largest_number = $number;
                    }
                }
            }
        }

        $type_date = $set_data[$module]['type_date'];
        // Tăng giá trị số lớn nhất lên một đơn vị và định dạng lại theo "0001"
        $new_number = sprintf('%04d', intval($largest_number) + 1);


        $format_date_code = DateTime::createFromFormat('Y-m-d', $yyyy_mm_dd);
        if ($format_date_code !== false) {
            $format_date_code2 = $format_date_code->format($type_date);
            $date_code = str_replace(array("-", "/"), "", $format_date_code2);
            if ($date_code != '') {
                $date_code .= '.';
            }
            $code = $start_code . $date_code . $new_number;
            $GLOBALS['log']->info('End: SugarWebServiceImplv4_1_custom->get_auto_code - SUCCESS');
            return $code;
        }
    }

    function get_invoice_details($session, $invoice_id)
    {
        global $db;
        $GLOBALS['log']->info('Begin: SugarWebServiceImplv4_1_custom->get_invoice_details');
        $module_name = 'AOS_Invoices';
        $error = new SoapError();
        if (!self::$helperObject->checkSessionAndModuleAccess($session, 'invalid_session', $module_name, '', '', $error)) {
            $GLOBALS['log']->error('End: SugarWebServiceImplv4_1_custom->get_invoice_details - FAILED on checkSessionAndModuleAccess');
            return;
        } // if
        $invoice = BeanFactory::getBean($module_name, $invoice_id);
        if (empty($invoice->fetched_row['id'])) {
            return [
                'error' => 'Invoice not found',
            ];
        }
        $sql = "SELECT pq.product_id, p.name as product_name, p.product_image, pq.product_qty, pq.product_cost_price, pq.product_list_price, pq.product_discount_amount, pq.product_unit_price, pq.vat_amt, pq.vat, pq.product_total_price
            FROM aos_products_quotes pq
            INNER JOIN aos_products p ON pq.product_id = p.id
            WHERE pq.deleted = 0 AND p.deleted = 0 AND parent_type = 'AOS_Invoices' AND parent_id = '" . $invoice_id . "'";
        $result = $db->query($sql);
        $invoice_details = [];
        while ($row = $db->fetchByAssoc($result)) {
            array_push($invoice_details, $row);
        }

        $GLOBALS['log']->info('End: SugarWebServiceImplv4_1_custom->get_invoice_details - SUCCESS');
        return [
            'id' => $invoice->fetched_row['id'],
            'name' => $invoice->fetched_row['name'],
            'invoice_date' => $invoice->fetched_row['invoice_date'],
            'total_amt' => $invoice->fetched_row['total_amt'],
            'discount_amount' => $invoice->fetched_row['discount_amount'],
            'subtotal_amount' => $invoice->fetched_row['subtotal_amount'],
            'tax_amount' => $invoice->fetched_row['tax_amount'],
            'shipping_amount' => $invoice->fetched_row['shipping_amount'],
            'total_amount' => $invoice->fetched_row['total_amount'],
            'details' => $invoice_details,
        ];
    }

    function set_invoice($session, $invoice_data, $product_data)
    {
        $GLOBALS['log']->info('Begin: SugarWebServiceImplv4_1_custom->set_invoice');
        $module_name = 'AOS_Invoices';
        $error = new SoapError();
        if (!self::$helperObject->checkSessionAndModuleAccess($session, 'invalid_session', $module_name, '', '', $error)) {
            $GLOBALS['log']->error('End: SugarWebServiceImplv4_1_custom->set_invoice - FAILED on checkSessionAndModuleAccess');
            return;
        } // if
        $invoice_data['total_amount'] = 0;
        $invoice_data['tax_amount'] = 0;
        $invoice_data['total_amt'] = 0;
        $currency_id = -99;
        $invoice_id =  $group_id = '';
        $product_quote_ids = [];
        if (!empty($product_data)) {
            foreach ($product_data as &$product) {
                $p = BeanFactory::getBean('AOS_Products', $product['product_id']);
                $product['product_cost_price'] = $p->fetched_row['cost'];
                $product['product_list_price'] = $p->fetched_row['price'];
                $product['name'] = $p->fetched_row['name'];
                $product['product_total_price'] = $product['product_qty'] * $product['product_unit_price'];
                $product['vat_amt'] = $product['vat'] / 100 * $product['product_total_price'];
                $invoice_data['total_amt'] += $product['product_total_price'];
                $invoice_data['tax_amount'] += $product['vat_amt'];
            }
        }
        $invoice_data['discount_amount'] = $invoice_data['total_amt'] * $invoice_data['discount_percent'] / 100;
        $invoice_data['total_amount'] = $invoice_data['total_amt'] + $invoice_data['tax_amount'] - $invoice_data['discount_amount'];
        $invoice_bean = BeanFactory::newBean($module_name);
        $invoice_bean->name = $invoice_data['name'];
        $invoice_bean->invoice_date = $invoice_data['invoice_date'];
        $invoice_bean->status = $invoice_data['status'];
        $invoice_bean->status_inventory = $invoice_data['status_inventory'];
        $invoice_bean->cashier = $invoice_data['cashier'];
        $invoice_bean->billing_account_id = $invoice_data['billing_account_id'];
        $invoice_bean->description = $invoice_data['description'];
        $invoice_bean->discount_percent = $invoice_data['discount_percent'];
        $invoice_bean->total_amt = $invoice_data['total_amt'];
        $invoice_bean->total_amount = $invoice_data['total_amount'];
        $invoice_bean->tax_amount = $invoice_data['tax_amount'];
        $invoice_bean->discount_amount = $invoice_data['discount_amount'];
        $invoice_bean->total_discount_amount = $invoice_data['discount_amount'];
        $invoice_bean->remaining_money = $invoice_data['total_amount'];
        $invoice_bean->subtotal_amount = $invoice_data['total_amount'];
        $invoice_bean->assigned_user_id = $invoice_data['assigned_user_id'];
        $invoice_bean->take_from = $invoice_data['take_from'];
        $invoice_bean->mobileref = $invoice_data['mobileref'];
        $invoice_bean->currency_id = $currency_id;
        $invoice_id = $invoice_bean->save();
        if ($invoice_id) {
            $item_group_bean = BeanFactory::newBean('AOS_Line_Item_Groups');
            $item_group_bean->total_amt = $invoice_data['total_amt'];
            $item_group_bean->discount_amount = $invoice_data['discount_amount'];
            $item_group_bean->subtotal_amount = $invoice_data['total_amount'];
            $item_group_bean->tax_amount = $invoice_data['tax_amount'];
            $item_group_bean->total_amount = $invoice_data['total_amount'];
            $item_group_bean->parent_type = $module_name;
            $item_group_bean->parent_id = $invoice_id;
            $item_group_bean->number = 1;
            $item_group_bean->currency_id = $currency_id;
            $group_id = $item_group_bean->save();
            if ($group_id) {
                $number = 1;
                foreach ($product_data as $pro) {
                    $product_quote_bean = BeanFactory::newBean('AOS_Products_Quotes');
                    $product_quote_bean->name = $pro['name'];
                    $product_quote_bean->product_qty = $pro['product_qty'];
                    $product_quote_bean->product_cost_price = $pro['product_cost_price'];
                    $product_quote_bean->product_list_price = $pro['product_list_price'];
                    $product_quote_bean->vat = $pro['vat'];
                    $product_quote_bean->vat_amt = $pro['vat_amt'];
                    $product_quote_bean->product_unit_price = $pro['product_unit_price'];
                    $product_quote_bean->product_total_price = $pro['product_total_price'];
                    $product_quote_bean->parent_type = $module_name;
                    $product_quote_bean->parent_id = $invoice_id;
                    $product_quote_bean->product_id = $pro['product_id'];
                    $product_quote_bean->group_id = $group_id;
                    $product_quote_bean->currency_id = $currency_id;
                    $product_quote_bean->number = $number;
                    $p_q_id = $product_quote_bean->save();
                    if ($p_q_id) {
                        array_push($product_quote_ids, $p_q_id);
                        $number++;
                    } else {
                        $item_group_bean->mark_deleted($group_id);
                        $item_group_bean->save();

                        $invoice_bean->mark_deleted($invoice_id);
                        $invoice_bean->save();
                        break;
                    }
                }
            }
        }

        $GLOBALS['log']->info('End: SugarWebServiceImplv4_1_custom->set_invoice - SUCCESS');
        return [
            'invoice_id' => $invoice_id,
            'group_id' => $group_id,
            'product_quote_ids' => $product_quote_ids,
        ];
    }
}
