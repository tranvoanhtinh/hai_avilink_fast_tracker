{*
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2018 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */
*}
<!DOCTYPE html>
<html {$langHeader}>
<head>
    <link rel="SHORTCUT ICON" href="{$FAVICON_URL}">
    <meta http-equiv="Content-Type" content="text/html; charset={$APP.LBL_CHARSET}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1" />
    <!-- Bootstrap -->
    <link href="themes/SuiteP/css/normalize.css" rel="stylesheet" type="text/css"/>
    <link href='themes/SuiteP/css/fonts.css' rel='stylesheet' type='text/css'>
    <link href="themes/SuiteP/css/grid.css" rel="stylesheet" type="text/css"/>
    <link href="themes/SuiteP/css/footable.core.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <title>{$APP.LBL_BROWSER_TITLE}</title>

    {$SUGAR_JS}
    {literal}
    <script type="text/javascript">
        <!--
        SUGAR.themes.theme_name = '{/literal}{$THEME}{literal}';
        SUGAR.themes.theme_ie6compat = '{/literal}{$THEME_IE6COMPAT}{literal}';
        SUGAR.themes.hide_image = '{/literal}{sugar_getimagepath file="hide.gif"}{literal}';
        SUGAR.themes.show_image = '{/literal}{sugar_getimagepath file="show.gif"}{literal}';
        SUGAR.themes.loading_image = '{/literal}{sugar_getimagepath file="img_loading.gif"}{literal}';
        
        if (YAHOO.env.ua)
            UA = YAHOO.env.ua;
       
    </script>
    {/literal}
    {$SUGAR_CSS}
	
    <link rel="stylesheet" type="text/css" href="themes/SuiteP/css/colourSelector.php">
    <script type="text/javascript" src='{sugar_getjspath file="themes/SuiteP/js/jscolor.js"}'></script>
    <script type="text/javascript" src='{sugar_getjspath file="cache/include/javascript/sugar_field_grp.js"}'></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.debug.js"></script>
	<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap.min.css">
	
    <script src="themes/SuiteP/js/app.js"></script>  
	
</head>
<div id="callCen" style="display:none">
<audio id="player" autoplay></audio>
<audio id="player1" loop="loop" src="./webrtc3/Reng.mp3">
</audio>

    <div class="info">
        <table>
            <tbody>
                <tr>
                    <td colspan="2">
                        <b>Thông tin máy lẻ:</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input id="input-username" type="text" name="username" placeholder="Tài khoản" value="21924" />
                        <input id="input-password" type="text" name="password" placeholder="Mật khẩu" value="46j3H4SMor"/>
						<input id="input-hostname" type="text" name="hostname" placeholder="Host name" value="kam-01.api-connect.io">
						<input id="input-port" type="text" name="port" placeholder="Port" value="7443">
						<input id="input-path" type="text" name="path" placeholder="Path" value="">
                        <input id="button-register" type="button" value="Đăng ký" onclick="handlePageLoad()" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Trạng thái:
                    </td>
                    <td>
                        <span id="ua-status"></span>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="info">
        <table>
            <tbody>
                <tr>
                    <td colspan="2">
                        <b>Thông tin cuộc gọi:</b>
                    </td>
                </tr>
                <tr>
                    <td>
                            Máy lẻ:
                        </td>
                        <td>
                            <span id="extension-username"></span>
                        </td>
                    </tr>
                <tr>
                    <td>
                        Loại cuộc gọi:
                    </td>
                    <td>
                        <span id="call-type"></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        Số điện thoại
                    </td>
                    <td>
                        <span id="call-phone"></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        Trạng thái:
                    </td>
                    <td>
                        <span id="call-status"></span>(<span id="session-status"></span>)
                    </td>
                </tr>
                <tr>
                    <td>
                        Thời lượng:
                    </td>
                    <td>
                        <span id="call-duration"></span>
                    </td>
                </tr>
                <tr>
                    <td>
                            <input id="input-phone" type="text" name="phone" placeholder="Số điện thoại" />
                            <input id="button-call" type="button" value="Gọi" onclick="handleButtonCallClick()" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input id="button-answer" type="button" value="Trả lời" onclick="handleButtonAnswerClick()" style="display:none"/>
                        <input id="button-hangup" type="button" value="Kết thúc" onclick="handleButtonHangupClick()" style="display:none"/>
                    </td>
                </tr>
                <tr>
                        <td colspan="2">
                            <input id="input-phone-transfer" type="text" name="phone" placeholder="Số điện thoại" style="display:none"/>
                            <input id="button-transfer" type="button" value="Chuyển tiếp" onclick="handleButtonTransferClick()" style="display:none"/>
                        </td>
                    </tr>
            </tbody>
        </table>
    </div>
</div>
<div id='quickCreateView'>
   
</div>

<div id='hide_popup'>
      <span class="glyphicon glyphicon-book"></span>
</div>
<div class="form-popup" id="myForm" style="display: none">
    <div style='overflow:  auto'>
        <a onclick="handleButtonHangupClick()" id="endCall" style='color: white; font-size: 14px; float: right; margin:0 5px 0 10px; cursor: pointer;'><span class="glyphicon glyphicon-remove"></span></a>
        <a onclick="togglePopup()" style='color: white; font-size: 14px; float: right; cursor: pointer;'><span class="glyphicon glyphicon-minus"></span></a>
    </div>
  <form action="/action_page.php" class="form-container-popup">
    <div class="head-form row">
        <div class='col-sm-3 cot'>
            <span class="glyphicon glyphicon-earphone"></span>
        </div>
        <div class='col-sm-5 cot'>
            <p style="cursor:pointer" id='tenkh'>
                Không xác định
            </p>
            <p id='phone-mobile-popup'>0987654321</p>
            <p id='userextension-popup'></p>
            <p id='statuscall'></p>
            <!-- <p id='connection'><span id='dot'></span>Đã kết nối</p> -->
        </div>
        <div class='col-sm-4 cot'>
            <div id='direction'>
                Cuộc gọi đến
            </div>
            <p id='time_count'><span id='hrs'>00</span>:<span id='mins'>00</span>:<span id='sec'>00</span></p>
            <p id='time_label'><span>giờ</span><span>phút</span><span>giây</span></p>
        </div>
		<div style="display:none" id="extDropdown2"></div>
    </div>
	
    <input type="hidden" id="timestatus" value="stop">
    <input type="hidden" id="calluuid" value="" >
    <textarea type="text" onkeyup="callnote()" placeholder="Ghi Chú" class="form-control" id="editnote"></textarea>
    <div id='actionButton'>
        
    </div>
<!--     <button type="button" class="btn btn-primary action-btn1"><i class="fa fa-users" aria-hidden="true"></i></button>
    <button type="button" class="btn btn-primary action-btn2"><i class="fas fa-dollar-sign"></i></button> -->
  </form>
</div>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.0.0/dist/chartjs-plugin-datalabels.min.js"></script>
<script src="webrtc3/js/sip-0.5.0.js"></script>
 <script src="webrtc3/ua.js"></script>
 {literal}
    <script type="text/javascript">
	$(document).ready(function(){
		$.ajax({
            type: "POST",
            url: "index.php?entryPoint=getExt",
            success: function(data) {
			var data = JSON.parse(data);
				var html = "";
				html += '<select id="extList2" style="width:250px">';
				html+='<option value="" selected="selected">----</option>';
				for(var i=0;i<data.length;i++){
					html+='<option value="'+data[i].description+'" >'+data[i].description+'-'+data[i].name+'</option>';
					
				}
				html += "</select>";
				$("#extDropdown2").html(html);
				
				},
                error: function (error) {
                    console.log("bbbb");
                }
        });
	});
		 </script>
{/literal}