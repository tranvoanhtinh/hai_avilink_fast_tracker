var OptionData = {},
    r = [],
    type_tel = "tel",
    invalidNodes = ["A", "SCRIPT", "IMG", "WF4XSPAN", "TEXTAREA", "INPUT", "SELECT", "WF4XTEMP", "WF4XPHONE", "PRE", "CODE", "STYLE", "CANVAS", "svg", "rect", "clipPath", "g", void 0],
    invalidContainsPromptAlertConfirm = new RegExp("(^|\\s)([(])?([+]?)[-.()]*[ ]?([0-9]{1,4})([)])?[-. ()]*([0-9]{1,4})[-. ()\\/]*([0-9]{1,4})[-. ]*([0-9]{0,4})[-. ]*([0-9]{0,4})[-. ]*([0-9]{0,4})[-. ]*([0-9]{0,4})($|\\s|[;,])", "g"), //skipped: contains prompt alert confirm
    invalidIP = new RegExp("(^|[^0-9]\\s)((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]|[0-9])\\.){3}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]|[0-9])($|\\s|[;,])", "g"), //skipped: matches IP address regex
    invalidURL = new RegExp("http[s]?:\\/\\/\\S*(\\s|$)", "g"), //skipped: matches URL regex
    invalidDATE = [new RegExp("(^|[^0-9]\\s)([1-9][0-9])([0-9]{2})-(1[0-2]|0[1-9])-(3[0-1]|[1-2][0-9]|0[0-9])($|\\s)", "g"), new RegExp("(^|[^0-9]\\s)(3[0-1]|[0-2][0-9]|[1-9])[\\/.-](1[0-2]|0[1-9]|[1-9])[\\/.-](([1-9][0-9])?[0-9]{2})($|\\s)", "g"), new RegExp("(^|[^0-9]\\s)(1[0-2]|0[1-9]|[1-9])[\\/.-](3[0-1]|[0-2][0-9]|[1-9])[\\/.-](([1-9][0-9])?[0-9]{2})($|\\s)", "g")],//skipped: matches date regex
    E = "",
    y = false,
    mutationObserver = new MutationObserver(function (e) {
        e.forEach(function (e) {
            for (var t = 0; t < e.addedNodes.length; t++){
          
                if (0 <= invalidNodes.indexOf(e.addedNodes[t].tagName)){

                }
                else {
                    console.debug("Dynamic element parsed: " + e.addedNodes[t].tagName);
                    var n = e.addedNodes[t];
                    
                    runRegExp(n)
                    setTimeout(function () {
                        runRegExp(n)
                    }, 1e3)
                }
            }
        })
    });

function beginMutationObserver() {
    mutationObserver.observe(document.body, {
        attributes: true,
        childList: true,
        characterData: true,
        subtree: true
    })
}

function disconnectMutation() {
    mutationObserver.disconnect()
}

function replaceSpace(e) {
    return e.replace(new RegExp(" ", "g"), " ")
}

function u(e, t) {
    var n = e.nodeValue;
    if (n) {
        invalidURL.test(n) && (console.debug(n + " skipped: matches URL regex"), n = n.replace(a, function (e) {
            return "<wf4xtemp>" + btoa(e) + "</wf4xtemp>"
        }));
        invalidIP.test(n) && (console.debug(n + " skipped: matches IP address regex"), n = n.replace(i, function (e) {
            return "<wf4xtemp>" + btoa(e) + "</wf4xtemp>"
        }));
        for (var o = 0; o < invalidDATE.length; ++o) invalidDATE[o].test(n) && (console.debug(n + " skipped: matches date regex"), n = n.replace(invalidDATE[o], function (e) {
            return "<wf4xtemp>" + btoa(e) + "</wf4xtemp>"
        }));
        n = replaceSpace(n);
        for (var r = false; invalidContainsPromptAlertConfirm.test(n);) {
            if (0 < n.indexOf("prompt(") || 0 < n.indexOf("alert(") || 0 < n.indexOf("confirm(")) {
                console.debug(n + " skipped: contains prompt alert confirm");
                break
            }
            var c, l;
            replaceSpace(n).replace(invalidContainsPromptAlertConfirm, function (e) {
                l = replaceSpecialCharacters(c = e)
            });
            // console.debug("Phone detected: " + type_tel + "=>" + l, l.length);
            l.length >= OptionData.minlength && l.length <= OptionData.maxlength ? (r = true, n = n.replace(c, "<wf4xphone>" + btoa(c) + "</wf4xphone>")) : (n = n.replace(c, "<wf4xtemp>" + btoa(c) + "</wf4xtemp>"), console.debug(c + " skipped: does not match length requirements"));
        }
        r && t.push({
            element: e,
            newhtml: n
        })
    }
}

function M(e, t) {
    if (e == document.body || null !== e.offsetParent) {
        for (var n = 0; n < e.childNodes.length; ++n) {
            3 != (o = e.childNodes[n]).nodeType && 8 != o.nodeType && (0 <= invalidNodes.indexOf(o.tagName) || M(o, t))
        }
        for (n = 0; n < e.childNodes.length; ++n) {
            var o;
            3 == (o = e.childNodes[n]).nodeType && u(o, t)
        }
    }
}

function replaceSpecialCharacters(e) {
    return e.replace(/[^0-9+]/gi, "")
}

function replaceTitleCallWith(e) {
    var t = "Call %s with Worldfone4X Click to Call";
    return y && (t = "Call %s with Worldfone4X Web Client"), t.replace("%s", e)
} 

function buildText(e, t) {
    
    var n = replaceSpecialCharacters(t);
        o = type_tel + ":" + n;
    return y && (o = e + n), s = '<wf4xspan onclick="clicktocall(\''+t.trim()+'\')" title="' + replaceTitleCallWith(t) + '">' + t + "</wf4xspan>", s
        
}

function runRegExp(e) {
    if (OptionData.enabled) {
        var t = [];
        M(e, t);
        t.push({
            element: document.createElement("wf4xdiv"),
            html: ""
        });
        
        var n = ["tel://", "callto://", "wf4xcallto://", "tel:", "callto:", "wf4xcallto:"],
        o = e.getElementsByTagName("A");
     
        if (o){
            for (var r = 0, c = o.length; r < c; r++){ 
                try {
                var l = o[r],
                i = decodeURI(l.getAttribute("href"));
                if (i)
                    for (var a = 0; a < n.length; a++) {
                        var s = n[a];
                        if (null != i && i.substr(0, s.length) == s) {
                            disconnectMutation();
                            var d = i.substr(s.length);
                            l.setAttribute("title", replaceTitleCallWith(d)); 
                            l.setAttribute("href", E + replaceSpecialCharacters(d)),
                            l.setAttribute("wf4xhref", E + replaceSpecialCharacters(d)), 
                            l.setAttribute("target", "_blank");
                            l.setAttribute("href", "tel:" + replaceSpecialCharacters(i.substr(s.length)));
                            l.setAttribute("wf4xhref", "tel:" + replaceSpecialCharacters(i.substr(s.length)));
                            l.setAttribute("target", "");
                            beginMutationObserver();
                            break
                        }
                    }
                } catch (e) {
        
                }
            }
            var u = t.length,
            g = false,
            p = "",
            m = document.createElement("div"),
            h = document.createElement("div"),
            b = document.createElement("div");
            disconnectMutation();
            for (a = 0; a < u; ++a){
                if (g != t[a].element.parentNode && (p && g && (g.innerHTML = p), t[a].element.parentNode && (p = t[a].element.parentNode.innerHTML), g = t[a].element.parentNode), t[a].element.data) {
                    var f = t[a].newhtml;
                    f = (f = f.replace(new RegExp("(?:<wf4xtemp>)(.*?)(?:</wf4xtemp>)", "gm"), function (e, t) {
                        return atob(t)
                    })).replace(new RegExp("(?:<wf4xphone>)(.*?)(?:</wf4xphone>)", "gm"), function (e, t) {
                        return replaceSpace(atob(t)).replace(invalidContainsPromptAlertConfirm, function (e) {
                            return buildText(E, e)
                        })
                    });
                    m.innerHTML = f;
                    h.innerHTML = t[a].element.data;
                    for (var x = b.innerHTML = p, w = 0; w < b.childNodes.length; ++w) {
                        var v = b.childNodes[w];
                        3 != v.nodeType && (v.outerHTML && (x = x.replace(v.outerHTML, "*".repeat(v.outerHTML.length))))
                    }
                    r = x.indexOf(h.innerHTML);
                    p = p.substring(0, r) + m.innerHTML + p.substring(r + h.innerHTML.length)
                } 
            }

            beginMutationObserver();
        }
    }
}

function setSettingOption(e) {
    OptionData = e;
    $("<style type='text/css'>wf4xspan{text-decoration: underline;\tcursor: pointer;}</style>").appendTo("head");
    E = OptionData.webclienturl.replace(/\/$/, "") + "/?dial=";
    y = "webclient";

    if (OptionData.enabled) {
		try {
			if(module_sugar_grp1 == "Home" || module_sugar_grp1 == "Accounts" || 
			module_sugar_grp1=="Calls" || module_sugar_grp1 =="Leads" || module_sugar_grp1 =="Prospects") { 
				runRegExp(document.body);
			}
		} catch (err) {
			// TODO
		}
        beginMutationObserver();
    }
}

function init() {
    document.defaultoptions || (document.defaultoptions = {
        enabled: true,
        protocol: "webclient",
        webclienturl: "",
        minlength: 7,
        maxlength: 15,
        exceptions: ""
    });
    chrome && chrome.storage ? chrome.storage.sync.get(document.defaultoptions, function (e) {
        setSettingOption(e)
    }) : setSettingOption(document.defaultoptions);
    return false;
}

document.addEventListener("click", function (e) {
    if (e.target.nodeName =="WF4XSPAN") {
        e.preventDefault();
        
        
        $.ajax({
            url: e.target.getAttribute("wf4xhref"),/*OptionData.webclienturl;*///"https://apps.worldfone.vn/worldfone4x/wfpbx/pbxactions/makeCallchrome",
            type: "GET",
            crossDomain: !0,
            dataType: 'text',
            success: function (response){
                if (response=='200') {
        
                    // $this.html('<i class="fa fa-check"></i> Đã đồng bộ');
                }
                

            },
            error: function (request, status, error) {
              alert('Không thể kết nối với hệ thống, kiểm tra đăng nhập hoặc cấu hình Api!')
            }
        });

    }

    
    // var t = "";
    // "WF4XSPAN" == e.target.nodeName ? t = e.target.getAttribute("wf4xhref") : "A" == e.target.nodeName && (t = e.target.getAttribute("wf4xhref")), t && (y ? window.open(t.replace("+", "%2B"), "_blank") : document.location = t, e.preventDefault())
}, false);
