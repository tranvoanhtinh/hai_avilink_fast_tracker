/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2018 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */
var sig_digits = 2;var enable_groups = 1;var total_tax = 0;
var lineno;
var prodln = 0;
var servln = 0;
var groupn = 0;
var title2;
var mod3;
var groupId2;
var group_ids = {};
var ZeroNum = 0;
var pageButt = 1;
SUGAR.measurements = {
  "breakpoints": {
    "x-small": 750,
    "small": 768,
    "medium": 992,
    "large": 1130,
    "x-large": 1250
  }
};
//
function updateStatus(ale,ale1){
	//var productName = document.getElementById('aos_products_opportunities_1aos_products_ida').innerHTML;
	var record = GetURLParameter("record");
	if(confirm(ale)){
		$.ajax({
						type: "POST",
						url: "index.php?entryPoint=updateProduct",
						data: {
							type:"0",
							//name:productName,
							id:record
						},
						//dataType: "json",
						success: function(data) {
							//$("#opp0").attr("disabled","disabled");
							var statuss = document.getElementById("statusText");
							var depositText = document.getElementById("depositText");
							depositText.style.backgroundColor = "#F7D358";
							statuss.innerText = "Đề xuất báo giá";
							statuss.style.backgroundColor = "#F7D358";
							$("#opp3").attr("value",ale1);
							//$("#opp3").removeAttr("disabled");
							document.getElementById("probability").innerHTML = "30";
							document.getElementById("depositdate").innerHTML = data;
						},
						error: function (error) {
									
						}
					});
	}
}
function showHide(key){
	var users=document.getElementsByClassName(key+"-user");
	for(var i=0;i<users.length;i++){
		if(users[i].style.display == "none"){
			users[i].style.display = "table-row";
			$("#oper-"+key).html("-");
		}else{
			users[i].style.display = "none";
			$("#oper-"+key).html("+");
		}
	}

}
function showHide2(i){
	var groups=document.getElementsByClassName(i+"-group");
	for(var j=0;j<groups.length;j++){
		if(groups[j].style.display == "none"){
			groups[j].style.display = "table-row";
			$("#oper-"+i).html("-");
			
		}else{
			groups[j].style.display = "none";
			$("#oper-"+i).html("+");
			var users=document.getElementsByClassName(i+(groups[j].id).replace(/\s/g, '')+"-user");
			for(var k=0;k<users.length;k++){
				if(users[k].style.display != "none"){
					users[k].style.display = "none";
					$("#oper-"+i+(groups[j].id).replace(/\s/g, '')).html("+");
				}
			}
		}
	}
}
function periodChange() {
    showHideDateFields($('#period').val());
}
function showHideDateFields(type) {
	if(type != "is_between"){
						  $("#date_from").datepicker("destroy");  
						   $("#date_to").datepicker("destroy"); 
						 document.getElementById('date_from').readOnly = true;
						  document.getElementById('date_to').readOnly = true;
					}else{
						$( "#date_to, #date_from").datepicker();
						$('.ui-datepicker-trigger').html('<div class="suitepicon suitepicon-module-calendar"></div>').addClass('button');
						var neww = document.getElementsByClassName('datepicker-trigger');
						var datepicker=document.getElementsByClassName('ui-datepicker-trigger');
						if(datepicker != null && datepicker != undefined){
							neww[0].appendChild(datepicker[0]);
							neww[1].appendChild(datepicker[1]);
						}
					}					
    $.ajax({
                type: "POST",
                url: "index.php?entryPoint=getDate",
                data: {
                    time:type
                },
                dataType: "json",
                success: function(data) {
				$("#date_from").val(data.from);
				$("#date_to").val(data.to);
										
                },
                error: function (error) {
                    
                }
            });
    
}
function openAll(){
	var checkAll=document.getElementById('checkAll'); 
	for(var i=0;i<98;i++){
		var groups=document.getElementsByClassName(i+"-group");
		if(groups !=null && groups != undefined){
			for(var j=0;j<groups.length;j++){
				if(checkAll.checked == true){
					groups[j].style.display = "table-row";
					$("#oper-"+i).html("-");
					var users=document.getElementsByClassName(i+((groups[j].id).replace(/\s/g, ''))+"-user");
					for(var k=0;k<users.length;k++){
						users[k].style.display = "table-row";
						$("#oper-"+i+((groups[j].id).replace(/\s/g, ''))).html("-");
					}
				}
				else
				{
					groups[j].style.display = "none";
					$("#oper-"+i).html("+");
					var users=document.getElementsByClassName(i+((groups[j].id).replace(/\s/g, ''))+"-user");
					for(var k=0;k<users.length;k++){
							users[k].style.display = "none";
							$("#oper-"+i+(groups[j].id).replace(/\s/g, '')).html("+");
					}
				}
			}
		
		}
	
	}
}
function fnExcelReport()
{
    var tab_text="<table border='2px'><tr bgcolor='#87AFC6'>";
    var textRange; var j=0;
    tab = document.getElementById('reportTable'); 

    for(j = 0 ; j < tab.rows.length ; j++) 
    {     
        tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
        //tab_text=tab_text+"</tr>";
    }
    tab_text = '<h2 class="module-title-text" style="font-weight: bold; text-align: center;">Báo Cáo Tổng Hợp Khách Hàng</h2>'+tab_text;
    tab_text=tab_text+"</table>";
    tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
    tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
    tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE "); 

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
    {
        txtArea1.document.open("txt/html","replace");
        txtArea1.document.write(tab_text);
        txtArea1.document.close();
        txtArea1.focus(); 
        sa=txtArea1.document.execCommand("SaveAs",true,"báo cáo tổng hợp khách hàng.xls");
         return (sa);
    }  
    else                 //other browser not tested on IE 11
       {
       	 var element = document.createElement('a');
            element.setAttribute('href', 'data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));
            element.setAttribute('download', "báo cáo tổng hợp khách hàng");
            element.style.display = 'none';
            document.body.appendChild(element);
            element.click();
            document.body.removeChild(element);
       }

   
}
function setCSS(val,a,d,e,f,g,h,w,wr){
	var b = val * (d-1);
	if(e != null &&  e != undefined){
		for(let v = 0 ; v < e.length; v++){
			if(e[v] > b && e[v] <= eval(b + val)){
				//$("#userView").find("tr").eq(e[v] - b).find("td").eq(0).css("backgroundColor","#A9F5A9");
				$("#userView").find("tr").eq(e[v] - b).css("backgroundColor","#A9F5A9");
				for(let v1 = 0 ; v1 < 12; v1++){
					$("#userView").find("tr").eq(e[v] - b).find("td").eq(v1).css("border"," #E6E6FA  1px solid");
					//$("#userView").find("tr").eq(e[v] - b).find("td").eq(v1).css("border-bottom-style","solid");
					//$("#userView").find("tr").eq(e[v] - b).find("td").eq(v1).css("border-bottom-color","black");
				}
			}
			
		}
	}
	if(f != null &&  f != undefined){
		for(let j = 0 ; j < f.length; j++){
			if(f[j] > b && f[j] <= eval(b + val)){
				//$("#userView").find("tr").eq(f[j] - b).find("td").eq(0).css("backgroundColor","#F5D0A9");
				$("#userView").find("tr").eq(f[j] - b).css("backgroundColor","#F5D0A9");
				for(let j1 = 0 ; j1 < 12; j1++){
					$("#userView").find("tr").eq(f[j] - b).find("td").eq(j1).css("border","#E6E6FA  1px solid");
					//$("#userView").find("tr").eq(f[j] - b).find("td").eq(j1).css("border-bottom-style","solid");
					//$("#userView").find("tr").eq(f[j] - b).find("td").eq(j1).css("border-bottom-color","black");
				}
			}
			
		}
	}
	if(g != null &&  g != undefined){
		for(let k = 0 ; k < g.length; k++){
			if(g[k] > b && g[k] <= eval(b + val)){
				$("#userView").find("tr").eq(g[k] - b).find("td").eq(0).css("textAlign","right");
				$("#userView").find("tr").eq(g[k] - b).find("td").eq(0).css("backgroundColor","white");
				$("#userView").find("tr").eq(g[k] - b).find("td").eq(0).css("fontWeight","bold");
				$("#userView").find("tr").eq(g[k] - b).find("td").eq(0).css("color","red");
				if($("#userView").find("tr").eq(g[k] - b).find("td").length > 11){
					for(let z = 0 ; z < wr.length; z++){
						for(var keyUser in wr[z]){
							var tdNum = 1;
							if($("#userView").find("tr").eq(g[k] - b).find("td").eq(0).html() == keyUser){
								for(var modKey in wr[z][keyUser]){
									if(wr[z][keyUser][modKey] != undefined){
										$("#userView").find("tr").eq(g[k] - b).find("td").eq(tdNum).attr("colspan","2");
										$("#userView").find("tr").eq(g[k] - b).find("td").eq(tdNum).css("vertical-align","middle");
										$("#userView").find("tr").eq(g[k] - b).find("td").eq(tdNum).css("text-align","center");
										$("#userView").find("tr").eq(g[k] - b).find("td").eq(tdNum).css("color","red");
										$("#userView").find("tr").eq(g[k] - b).find("td").eq(tdNum).css("fontWeight","bold");
										$("#userView").find("tr").eq(g[k] - b).find("td").eq(tdNum).html(wr[z][keyUser][modKey]);
										$("#userView").find("tr").eq(g[k] - b).find("td").eq(tdNum+1).remove();
										tdNum++;
									}
								}
								
									
								
							}
						}
						
					}
				}
			}
			
		}
	}
	
	if(w != null &&  w != undefined){
		for(let t = 0; t< w.length;t++){
			for(var key in w[t]){
				if(w[t][key] != null && w[t][key] != undefined){
					//if(w[t][key].to - w[t][key].from > 0){
						var from = ((w[t][key].from)>b?(w[t][key].from-b):1);
						var to = ((w[t][key].to>=eval(b + val))?val:(w[t][key].to-b));
						for(let mn = from; mn <= to; mn++){
							$("#userView").find("tr").eq(mn).find("td").eq(0).css("textAlign","right");
							$("#userView").find("tr").eq(mn).find("td").eq(1).css("fontWeight","bold");
							if($("#userView").find("tr").eq(mn).find("td").length == 12){
								
								$("#userView").find("tr").eq(mn).find("td").eq(6).css("fontWeight","bold");
								$("#userView").find("tr").eq(mn).find("td").eq(6).css("color","white");
								if($("#userView").find("tr").eq(mn).find("td").eq(6).html() == "Đã chuyển đổi" || $("#userView").find("tr").eq(mn).find("td").eq(6).html() == "Converted" || $("#userView").find("tr").eq(mn).find("td").eq(6).html() == "Thương lượng đàm phán" || $("#userView").find("tr").eq(mn).find("td").eq(6).html() == "Phát sinh nhu cầu") {
								$("#userView").find("tr").eq(mn).find("td").eq(6).css("backgroundColor","#FFBF00");
								}else if($("#userView").find("tr").eq(mn).find("td").eq(6).html() == "Đã mua hàng" || $("#userView").find("tr").eq(mn).find("td").eq(6).html() == "Transaction" || $("#userView").find("tr").eq(mn).find("td").eq(6).html() == "Chốt Thắng" || $("#userView").find("tr").eq(mn).find("td").eq(6).html() == "Held" || $("#userView").find("tr").eq(mn).find("td").eq(6).html() == "Done" || $("#userView").find("tr").eq(mn).find("td").eq(6).html() == "Đã hoàn thành" || $("#userView").find("tr").eq(mn).find("td").eq(6).html() == "Phát sinh giao dịch" || $("#userView").find("tr").eq(mn).find("td").eq(6).html() == "Closed Won" || $("#userView").find("tr").eq(mn).find("td").eq(6).html() == "Đã mua hàng" || $("#userView").find("tr").eq(mn).find("td").eq(6).html() == "Đã mua"){
									$("#userView").find("tr").eq(mn).find("td").eq(6).css("backgroundColor","#FF8000");
									
								}else if($("#userView").find("tr").eq(mn).find("td").eq(6).html() == "Mới" || $("#userView").find("tr").eq(mn).find("td").eq(6).html() == "New" || $("#userView").find("tr").eq(mn).find("td").eq(6).html() == "Tìm hiểu nhu cầu" || $("#userView").find("tr").eq(mn).find("td").eq(6).html() == "Planned" || $("#userView").find("tr").eq(mn).find("td").eq(6).html() == "Lập kế hoạch" ){
									$("#userView").find("tr").eq(mn).find("td").eq(6).css("backgroundColor","#3ADF00");
									
								}else if($("#userView").find("tr").eq(mn).find("td").eq(6).html() == "In Process" || $("#userView").find("tr").eq(mn).find("td").eq(6).html() == "Đang liên hệ"){
									$("#userView").find("tr").eq(mn).find("td").eq(6).css("backgroundColor","red");
									
								}else if($("#userView").find("tr").eq(mn).find("td").eq(6).html() == "Đề xuất báo giá"){
									$("#userView").find("tr").eq(mn).find("td").eq(6).css("backgroundColor","#F7D358");
									
								}else if($("#userView").find("tr").eq(mn).find("td").eq(6).html() == "Chốt Thua" || $("#userView").find("tr").eq(mn).find("td").eq(6).html() == "Ngừng chăm sóc" || $("#userView").find("tr").eq(mn).find("td").eq(6).html() == "Closed Lost" || $("#userView").find("tr").eq(mn).find("td").eq(6).html() == "Dead"){
									$("#userView").find("tr").eq(mn).find("td").eq(6).css("backgroundColor","#00BFFF");
								}
								$("#userView").find("tr").eq(mn).find("td").eq(6).css("textAlign","center");
								$("#userView").find("tr").eq(mn).find("td").eq(6).css("vertical-align","middle");
							}
							if(w[t][key].to - w[t][key].from > 0){
								if(mn == from){
									$("#userView").find("tr").eq(mn).find("td").eq(3).attr("rowspan",(w[t][key].to)>=eval(b + val)?val:(((w[t][key].from > b) && (w[t][key].to < val * d))?(eval(w[t][key].to - w[t][key].from +1)):(w[t][key].to-b)));
									$("#userView").find("tr").eq(mn).find("td").eq(3).css("color","red");
									$("#userView").find("tr").eq(mn).find("td").eq(3).css("display","table-cell");
									$("#userView").find("tr").eq(mn).find("td").eq(3).css("vertical-align","middle");
									$("#userView").find("tr").eq(mn).find("td").eq(3).css("fontWeight","bold");
									$("#userView").find("tr").eq(mn).find("td").eq(3).css("text-align","center");
								}else{
									if($("#userView").find("tr").eq(mn).find("td").length > 11){
										$("#userView").find("tr").eq(mn).find("td").eq(3).remove();
									}
								}
							}else{
								$("#userView").find("tr").eq(mn).find("td").eq(3).css("fontWeight","bold");
								$("#userView").find("tr").eq(mn).find("td").eq(3).css("color","red");
								$("#userView").find("tr").eq(mn).find("td").eq(3).css("text-align","center");
								$("#userView").find("tr").eq(mn).find("td").eq(3).css("vertical-align","middle");
							}
						}
					//}
				}
			}
		}
	}
	/*
	$('[name="userView_length"]').on("change",function(){
			if($(".pagination > .active").find("a").html() != "Next" && $(".pagination > .active").find("a").html() != "Previous" && $(".pagination > .active").find("a").html() != "Last" && $(".pagination > .active").find("a").html() != "First")
			{
				pageButt = parseInt($(".pagination > .active").find("a").html());
			}else{
				if($(".pagination > .active").find("a").html() == "Next"){
					pageButt ++;
				}else if($(".pagination > .active").find("a").html() == "Previous"){
					pageButt --;
				}else if($(".pagination > .active").find("a").html() == "Last"){
					pageButt = eval(parseInt(h/parseInt(val)) + 1);
				}else if($(".pagination > .active").find("a").html() == "First"){
					pageButt = 1;
				}
			}
			setCSS($(this).val(),$(".paginate_button"),pageButt,e,f,g,h,w,wr);
		
	});
	*/
	a.on("click",function(){
		//setTimeout(function(){
			var pageButt = parseInt($(".pagination > .active").find("a").html());
			setCSS(val,$(".paginate_button"),pageButt,e,f,g,h,w,wr);
		//},300);
	});
}
function actFilter(){
	SUGAR.ajaxUI.showLoadingPanel();
	var province=$("#provincecode_c").val();
	var groupUsers=$("#group-users").val();
	var users=$("#users").val();
	var titleUsers=$("#title-users").val();
	var period=$("#period").val();
	var date_from = $('#date_from').val();
    var date_to = $('#date_to').val();
			$.ajax({
					type: "POST",
					url: "index.php?entryPoint=getAllLanguage",
					dataType:"json",
					success: function(language) {
						$.ajax({
							type: "POST",
							url: "index.php?entryPoint=actFilter",
							data: {
								province:province,
								groupUsers:groupUsers,
								users:users,
								titleUsers:titleUsers,
								period:period,
								date_from:date_from,
								date_to:date_to
							},
						success: function(data){
							if(data != null && data !=''){
								var Obj=JSON.parse(data);
								var table = $('#userView').DataTable();
								table.clear().draw();
								var row = 0;
								var rowArr = [];
								var rowGroup = 0;
								var rowArrGroup = [];
								var rowUser = 0;
								var rowArrUser = [];
								var rowArrMod = [];
								var rowSpan = [];
								var colSpan = [];
								var chartLabel = [];
								var chartContent = [];
								var chartLead = [];
								var chartAcc = [];
								var chartOpp = [];
								var chartCall = [];
								var chartMeeting = [];
								var fromLead;
								var toLead;
								var fromAcc;
								var toAcc;
								var fromOpp;
								var toOpp;
								var fromCall;
								var toCall;
								var fromMeeting;
								var toMeeting;
								//setTimeout(function(){
								for(let i=0;i<Obj.length;i++){
									/*
									html+='<tr onclick=\"showHide2(\''+i+'\')\" style="background-color:#A9F5A9;font-weight:bold">';
									html+='<td rowspan="2">'+eval(i+1)+'</td>';
									html+='<td rowspan="2" style="font-weight:bold"><span id="oper-'+i+'">+</span>'+Obj[i].province+'</td>';
									html+='</tr>';
									html+='<tr onclick=\"showHide2(\''+i+'\')\" style="background-color:#A9F5A9;font-weight:bold">';
									html+='</tr>';
									*/
									//setTimeout(function(){
										table.row.add([
													Obj[i].province,
													"",
													"",
													"",
													"",
													"",
													"",
													"",
													"",
													"",
													"",
													"",
										]).draw( false );
									//},3000);
										row++;
										rowArr.push(row);
										rowGroup++;
										rowUser++;
										
									 
									var data = {};
									Object.keys(Obj[i].data).sort().forEach(function(key){
										data[key] = Obj[i].data[key];
									});
									
									for(var key in data){
										/*
										html+='<tr style="font-weight:bold;display:none;color:red;background-color:#F5D0A9" class="'+i+'-group" onclick=\"showHide(\''+i+key.replace(/\s/g, '')+'\')\" id="'+key.replace(/\s/g, '')+'-2">';
										html+='<td style="border-bottom-style:none"></td>';
										html+='<td style="background-color:#F5D0A9"><span id="oper-'+i+key.replace(/\s/g, '')+'" style="text-align:left">+</span><span style="margin-left:3%">'+key+'</span></td>';
										html+='</tr>';
										*/
										//setTimeout(function(){
											
											table.row.add([
														key,
														"",
														"",
														"",
														"",
														"",
														"",
														"",
														"",
														"",
														"",
														"",
											]).draw( false );
											row++;
											rowGroup++;
											rowArrGroup.push(rowGroup);
											rowUser++;
										//},3000);
										for(let j=0;j<data[key].length;j++){
											//setTimeout(function(){
												table.row.add([
													data[key][j].user,
													"",
													"",
													"",
													"",
													"",
													"",
													"",
													"",
													"",
													"",
													"",
												]).draw( false );
												row++;
												rowGroup++;
												rowUser++;
												rowArrUser.push(rowUser);
											//},3000);
											/*
											html+='<tr style="display:none" class="'+i+key.replace(/\s/g, '')+'-user">';
											html+='<td></td>';
											html+='<td style="text-align:right;margin-right:3px;font-weight:bold"><span style="margin-right:3px;font-weight:bold">'+data[key][j].user+'</span></td>';
											html+='</tr>';
											html+='<tr style="display:none" class="'+i+key.replace(/\s/g, '')+'-user">';
											html+='<td></td>';
											html+='<td style="text-align:center" >Tên</td>';
											html+='<td style="text-align:center">Ngày</td>';
											html+='<td style="text-align:center">Module</td>';
											html+='<td style="text-align:center">Liên quan tới</td>';
											html+='<td style="text-align:center">Thông tin liên hệ</td>';
											html+='<td style="text-align:center">Tình trạng</td>';
											html+='<td style="text-align:center">Doanh số dự kiến</td>';
											html+='<td style="text-align:center">Ghi Chú</td>';
											html+='<td style="text-align:center">Sản phẩm dịch vụ</td>';
											html+='<td style="text-align:center">Nguồn</td>';
											html+='<td style="text-align:center">Người dùng</td>';

											html+='</tr>';
											*/
											if(data[key][j].Lead != null){
												for(let k=0;k<data[key][j].Lead.length;k++){
															table.row.add([
																eval(k+1),
																data[key][j].Lead[k].last_name,
																data[key][j].Lead[k].date_entered,
																language.modNameLead.toUpperCase()+' ('+data[key][j].Lead.length+')',
																data[key][j].Lead[k].account_name,
																data[key][j].Lead[k].phone_mobile+"-"+((data[key][j].Lead[k].email_address != null && data[key][j].Lead[k].email_address != "")?data[key][j].Lead[k].email_address+"-":'')+data[key][j].Lead[k].last_name,
																language.leadStatus[data[key][j].Lead[k].status],
																(data[key][j].Lead[k].opportunity_amount != null && data[key][j].Lead[k].opportunity_amount != "")?changeCurrency(data[key][j].Lead[k].opportunity_amount):"",	
																data[key][j].Lead[k].description,
																data[key][j].Lead[k].proName,
																language.source[data[key][j].Lead[k].lead_source],
																data[key][j].Lead[k].user_name,
																//data[i].userName,
															]).draw( false );
																
															//}
														//}
													//},3000);	
														row++;
														rowGroup++;
														rowUser++;	
														rowArrMod.push(row);
														if( k ==  0 ){
															fromLead = row;
														}
														if(k == data[key][j].Lead.length - 1){
															toLead = row;		
															rowSpan.push({Lead:{from:fromLead,to:toLead}});
															var leadMod =language.modNameLead.toUpperCase()+' ('+data[key][j].Lead.length+')';
														}	
												}
												chartLead.push(data[key][j].Lead.length);
											}else{
												chartLead.push(0);
											}
											if(data[key][j].Account != null){
												for(let f=0;f<data[key][j].Account.length;f++){
													table.row.add([
														eval(f+1),
														data[key][j].Account[f].name,
														data[key][j].Account[f].date_entered,
														language.modNameAcc.toUpperCase()+' ('+data[key][j].Account.length+')',
														data[key][j].Account[f].company_c,
														data[key][j].Account[f].phone_alternate+"-"+((data[key][j].Account[f].email_address != null && data[key][j].Account[f].email_address != "")?data[key][j].Account[f].email_address+"-":'')+"-"+data[key][j].Account[f].name,
														language.leadStatus[data[key][j].Account[f].lead_account_status_c],
														(data[key][j].Account[f].amount != null && data[key][j].Account[f].amount != "")?changeCurrency(data[key][j].Account[f].amount):"",
														data[key][j].Account[f].description,
														data[key][j].Account[f].proName,
														language.source[data[key][j].Account[f].leadsource_c],
														data[key][j].Account[f].user_name,
													]).draw( false );
													row++;
													rowGroup++;
													rowUser++;
													rowArrMod.push(row);
													if( f ==  0 ){
														fromAcc = row;
													}
													if(f == data[key][j].Account.length - 1){
														toAcc = row;		
														rowSpan.push({Account:{from:fromAcc,to:toAcc}});
														var accMod = language.modNameAcc.toUpperCase()+' ('+data[key][j].Account.length +')';				
													}	
														
												}
												chartAcc.push(data[key][j].Account.length);
											}else{
												chartAcc.push(0);
											}
											if(data[key][j].Opp != null){
												for(let u=0;u<data[key][j].Opp.length;u++){
													/*
													html+='<tr style="display:none" class="'+i+key.replace(/\s/g, '')+'-user">';
													html+='<td style="text-align:right"><span style="margin-right:3px;font-weight:bold">'+data[key][j].Opp[u].name+'</span></td>';
													html+='<td style="text-align:right"><span style="margin-right:3px;font-weight:bold">'+data[key][j].Opp[u].date_closed+'</span></td>';
													html+='<td style="text-align:right"><span style="margin-right:3px;font-weight:bold">OPPORTUNITIES('+data[key][j].Opp.length+')</span></td>';
													
													html+='<td style="text-align:right"><span style="margin-right:3px;font-weight:bold">null</span></td>';
													
													html+='<td style="text-align:right"><span style="margin-right:3px;font-weight:bold">null</span></td>';
													
													html+='<td style="text-align:right"><span style="margin-right:3px;font-weight:bold">null</span></td>';
													html+='<td style="text-align:right"><span style="margin-right:3px;font-weight:bold">'+data[key][j].Opp[u].description+'</span></td>';
													html+='<td style="text-align:right"><span style="margin-right:3px;font-weight:bold">null</span></td>';
													html+='<td style="text-align:right"><span style="margin-right:3px;font-weight:bold">null</span></td>';
													html+='<td style="text-align:right"><span style="margin-right:3px;font-weight:bold">'+data[key][j].Opp[u].assigned_user_id+'</span></td>';
													html+='<td style="text-align:right"><span style="margin-right:3px;font-weight:bold">'+data[key][j].Opp[u].sales_stage+'</span></td>';
													html+='</tr>';
													*/
													
													table.row.add([
														eval(u+1),
														data[key][j].Opp[u].name,
														data[key][j].Opp[u].date_closed,
														language.modNameOpp.toUpperCase()+' ('+data[key][j].Opp.length+')',
														data[key][j].Opp[u].accName,
														data[key][j].Opp[u].phone_alternate+"-"+data[key][j].Opp[u].email_address+"-"+data[key][j].Opp[u].name,
														language.oppStatus[data[key][j].Opp[u].sales_stage],
														(data[key][j].Opp[u].amount != null && data[key][j].Opp[u].amount != "")?changeCurrency(data[key][j].Opp[u].amount):"",
														data[key][j].Opp[u].description,
														data[key][j].Opp[u].proName,
														'',
														data[key][j].Opp[u].user_name,
														
													]).draw( false );
													row++;
													rowGroup++;
													rowUser++;
													rowArrMod.push(row);
													if( u ==  0 ){
														fromOpp = row;
													}
													if(u == data[key][j].Opp.length - 1){
														toOpp = row;		
														rowSpan.push({Opp:{from:fromOpp,to:toOpp}});
														var oppMod = language.modNameOpp.toUpperCase()+' ('+data[key][j].Opp.length+ ')';
													}
													
												}
												chartOpp.push(data[key][j].Opp.length);	
											}else{
												chartOpp.push(0);
											}
											if(data[key][j].Call != null){
												for(let l=0;l<data[key][j].Call.length;l++){
													table.row.add([
														eval(l+1),
														data[key][j].Call[l].name,
														data[key][j].Call[l].date_start,
														language.modNameCall.toUpperCase()+' ('+data[key][j].Call.length+')',
														data[key][j].Call[l].parent_type == "Leads"?(data[key][j].Call[l].last_name+" "+data[key][j].Call[l].first_name):(data[key][j].Call[l].accName),
														data[key][j].Call[l].parent_type == "Leads"?data[key][j].Call[l].phone_mobile+"-"+data[key][j].Call[l].email_address+"-"+data[key][j].Call[l].last_name+" "+((data[key][j].Call[l].first_name != null && data[key][j].Call[l].first_name != "")?data[key][j].Call[l].first_name:""):data[key][j].Call[l].phone_alternate+"-"+data[key][j].Call[l].email_address+"-"+data[key][j].Call[l].accName,
														language.callStatus[data[key][j].Call[l].status],
														'',
														data[key][j].Call[l].description,
														'',
														'',
														data[key][j].Call[l].user_name,
														
													]).draw( false );
													row++;
													rowGroup++;
													rowUser++;
													rowArrMod.push(row);
													if( l ==  0 ){
														fromCall = row;
													}
													if(l == data[key][j].Call.length - 1){
														toCall = row;		
														rowSpan.push({Call:{from:fromCall,to:toCall}});
														var callMod = language.modNameCall.toUpperCase()+' ('+data[key][j].Call.length+ ')';
													}	
												}
												chartCall.push(data[key][j].Call.length);	
											}else{
												chartCall.push(0);
											}
											if(data[key][j].Meeting != null){
												for(let n=0;n<data[key][j].Meeting.length;n++){
													table.row.add([
														eval(n+1),
														data[key][j].Meeting[n].name,
														data[key][j].Meeting[n].date_start,
														language.modNameMeeting.toUpperCase()+' ('+data[key][j].Meeting.length+')',
														data[key][j].Meeting[n].parent_type == "Leads"?(data[key][j].Meeting[n].last_name+" "+data[key][j].Meeting[n].first_name):(data[key][j].Meeting[n].accName),
														data[key][j].Meeting[n].parent_type == "Leads"?data[key][j].Meeting[n].phone_mobile+"-"+data[key][j].Meeting[n].email_address+"-"+data[key][j].Meeting[n].last_name+" "+((data[key][j].Meeting[n].first_name != null && data[key][j].Meeting[n].first_name != "")?data[key][j].Meeting[n].first_name:""):data[key][j].Meeting[n].phone_alternate+"-"+data[key][j].Meeting[n].email_address+"-"+data[key][j].Meeting[n].accName,
														language.meetingStatus[data[key][j].Meeting[n].status],
														'',
														data[key][j].Meeting[n].description,
														'',
														'',
														data[key][j].Meeting[n].user_name,
														
													]).draw( false );
													row++;
													rowGroup++;
													rowUser++;
													rowArrMod.push(row);
													if( n ==  0 ){
														fromMeeting = row;
													}
													if(n == data[key][j].Meeting.length - 1){
														toMeeting = row;		
														rowSpan.push({Meeting:{from:fromMeeting,to:toMeeting}});
														var meetingMod = language.modNameMeeting.toUpperCase()+' ('+data[key][j].Meeting.length+ ')';
													}	
													
												}
												chartMeeting.push(data[key][j].Meeting.length);	
											}else{
												chartMeeting.push(0);
											}
											var userMod = {};
											userMod[data[key][j].user] = {Lead:leadMod,Acc:accMod,Opp:oppMod,Call:callMod,Meeting:meetingMod};
											colSpan.push(userMod);
											leadMod = undefined;
											accMod = undefined;
											oppMod = undefined;
											callMod = undefined;
											meetingMod = undefined;
											chartLabel.push(data[key][j].user);
											
										}
									}
								}
								//
								
								/*
								var data =  {
									labels: chartLabel,
									datasets: [
										{
											label: language.modNameLead.toUpperCase(),
											backgroundColor: '#7ea367',
											borderColor: '#46d5f1',
											hoverBackgroundColor: '#7ea367',
											hoverBorderColor: '#42f5ef',
											data: chartLead
										},
										{
											label: language.modNameAcc.toUpperCase(),
											backgroundColor: '#c9876b',
											borderColor: '#46d5f1',
											hoverBackgroundColor: '#c9876b',
											hoverBorderColor: '#42f5ef',
											data: chartAcc
										},
										{
											label: language.modNameOpp.toUpperCase(),
											backgroundColor: '#c289c9',
											borderColor: '#46d5f1',
											hoverBackgroundColor: '#c289c9',
											hoverBorderColor: '#42f5ef',
											data: chartOpp
										},
										{
											label: language.modNameCall.toUpperCase(),
											backgroundColor: '#8992c9',
											borderColor: '#46d5f1',
											hoverBackgroundColor: '#8992c9',
											hoverBorderColor: '#42f5ef',
											data: chartCall
										},
										{
											label: language.modNameMeeting.toUpperCase(),
											backgroundColor: '#c9be87',
											borderColor: '#46d5f1',
											hoverBackgroundColor: '#c9be87',
											hoverBorderColor: '#42f5ef',
											data: chartMeeting
										}
									]
								};
									const config = {
									    height:1000,
									    type: 'bar',
									    data: data,
									    options: {
											maintainAspectRatio: false,
											plugins: {
											  title: {
												display: false,
												text: 'Chart.js Bar Chart - Stacked'
											  },
											},
											responsive: true,
											scales: {
											  x: {
												stacked: true,
											  },
											  y: {
												stacked: true,
												ticks: {
													beginAtZero: false
												}
											  },
											}
										},
									  plugins:[ChartDataLabels]
									};
								const actions = [
									  {
										name: 'Randomize',
										handler(chart) {
										  chart.data.datasets.forEach(dataset => {
											dataset.data = Utils.numbers({count: chart.data.labels.length, min: 0, max: 1200});
										  });
										  chart.update();
										}
									  },
									];
					 $('#chartShow').remove();
					$('#showChart').append('<canvas id="chartShow"></canvas>');
					$('#showChart').css("height","800px");
                    var bar = $("#chartShow"); 
                    var barGraph = new Chart(bar,config,
                        actions);
                    */
								//
									 SUGAR.ajaxUI.hideLoadingPanel();
									$(document).ready(function(){
										$("#userView >tbody > tr").css("backgroundColor","white");
										 setCSS(100,$(".paginate_button"),1,rowArr,rowArrGroup,rowArrUser,row,rowSpan,colSpan);
								});
							}else{
								SUGAR.ajaxUI.hideLoadingPanel();
							}
						}
					});
				},
                error: function (error) {
                    
                }
            });
}
function loadProvince(){
	$.ajax({
                type: "POST",
                url: "index.php?entryPoint=loadProvince",
                
                dataType: "json",
                success: function(data) {
					document.getElementById("provincecode_c").value = data ;
					loadGroup();
					
                },
                error: function (error) {
                    
                }
            });
}
function loadProvince4(){
	$.ajax({
                type: "POST",
                url: "index.php?entryPoint=loadProvince",
                
                dataType: "json",
                success: function(data) {
					$("#provincecode").val(data) ;
					$('#provincecode option[value="'+data+'"]').attr('selected','selected');
					loadDynamicEnum("provincecode","districtcode_c");
					//$("#provincecode").prop('disabled', true);
					$.ajax({
						type: "POST",
						url: "index.php?entryPoint=checkUser",
						data: {
						 // id:recordd
						},
						dataType: "json",
						success: function(dat) {
						if(dat.status != "1"){
							// document.getElementById('username').readOnly = true;
							// var disVal =  document.getElementById('districtcode_c').value;
							var proVal =  document.getElementById('provincecode').value;
							// $('#districtcode_c').on("change",function(){
							//	document.getElementById('districtcode_c').value = disVal;
								 
							// });
							 $('#provincecode').on("change",function(){
								 document.getElementById('provincecode').value = proVal;
								 
							 });
							 loadDynamicEnum("provincecode","districtcode_c");
							// $('#department').prop('disabled',true);
							 
						 }else{
							 $("#provincecode").on("change",function(){
						//$("#provincecode").val(data) ;
						loadDynamicEnum("provincecode","districtcode_c");
						var provinceVal = $("#provincecode").val();
						if(provinceVal != null && provinceVal != ''){
							$.ajax({
								type: "POST",
								url: "index.php?entryPoint=getGroup",
								data: {
									id:$("#provincecode").val()
								},
								dataType: "json",
								success: function(result) {
									var obj=result;
									html="";
									html2="";
									html+='<option selected value=""></option>';
									for(let i=0;i<obj.length;i++){
										html+='<option value="'+obj[i].name+'">'+obj[i].name+'</option>';
										html2+='<option value="'+obj[i].id+'">'+obj[i].name+'</option>';
									}
									
									$('#groupname').html(html);
									$('#groupname2').html(html2);
									$(document).ready(function (){
										$("#groupname").on("change",function(){
											var grp=$('#groupname').val();
											$.ajax({
												type: "POST",
												url: "index.php?entryPoint=getIdGroup",
												data: {
													name:grp
												},
												dataType: "json",
												success: function(res) {
													$("#groupid").val(res.id);
											}
											});
										});
										
									});
								},
								failure: function() {
								   // SUGAR.ajaxUI.hideLoadingPanel();
								}
							});
						}else{
							$('#groupname').html("");
							$('#groupname2').html("");
							
						}
					});
						 }
						}
					});
					
					setTimeout(function(){
					var provinceVal = $("#provincecode").val();
		if(provinceVal != null && provinceVal != ''){
			$.ajax({
							type: "POST",
							url: "index.php?entryPoint=getGroup",
							data: {
								id:$("#provincecode").val()
							},
							dataType: "json",
							success: function(result) {
								var obj=result;
								html="";
								html2="";
								html+='<option selected value=""></option>';
								for(let i=0;i<obj.length;i++){
									html+='<option value="'+obj[i].name+'">'+obj[i].name+'</option>';
									html2+='<option value="'+obj[i].id+'">'+obj[i].name+'</option>';
								}
								
								$('#groupname').html(html);
								$('#groupname2').html(html2);
								$(document).ready(function (){
									$("#groupname").on("change",function(){
										var grp=$('#groupname').val();
										$.ajax({
											type: "POST",
											url: "index.php?entryPoint=getIdGroup",
											data: {
												name:grp
											},
											dataType: "json",
											success: function(res) {
												$("#groupid").val(res.id);
										}
										});
									});
									
								});
							},
							failure: function() {
							   // SUGAR.ajaxUI.hideLoadingPanel();
							}
			});
		}
				},400);	
                },
                error: function (error) {
                    
                }
            });
	     		
}
var timing =[];
function updateStatus1(ale,ale2,ale3){
	//var productName = document.getElementById('aos_products_opportunities_1aos_products_ida').innerHTML;
	var record = GetURLParameter("record");
	if(confirm(ale)){
		$.ajax({
						type: "POST",
						url: "index.php?entryPoint=updateProduct",
						data: {
							type:"1",
							//name:productName,
							id:record
						},
						//dataType: "json",
						success: function(data) {
							if(data != ale2){
								//$("#opp1").attr("disabled","disabled");
								var statuss = document.getElementById("statusText");
								var depositText = document.getElementById("depositText");
								depositText.style.backgroundColor = "#FFBF00";
								statuss.innerText = "Thương lượng đàm phán";
								statuss.style.backgroundColor = "#FFBF00";
								$("#opp3").attr("value",ale3);
								//$("#opp3").removeAttr("disabled");
								document.getElementById("probability").innerHTML = "80";
								document.getElementById("depositdate").innerHTML = data;
							}else{
								alert(data);
							}
							
						},
						error: function (error) {
									
						}
					});
	}
}

function updateStatus2(ale,ale2){
	//var productName = document.getElementById('aos_products_opportunities_1aos_products_ida').innerHTML;
	var record = GetURLParameter("record");
	if(confirm(ale)){
		$.ajax({
						type: "POST",
						url: "index.php?entryPoint=updateProduct",
						data: {
							type:"2",
							//name:productName,
							id:record
						},
						//dataType: "json",
						success: function(data) {
							if(data != ale2){
								//$("#opp2").attr("disabled","disabled");
								//$("#opp3").attr("disabled","disabled");
								var statuss = document.getElementById("statusText");
								var depositText = document.getElementById("depositText");
								depositText.style.backgroundColor = "#FF8000";
								statuss.innerText = "Chốt thắng";
								statuss.style.backgroundColor = "#FF8000";
								document.getElementById("probability").innerHTML = "100";
								document.getElementById("depositdate").innerHTML = data;
							}else{
								alert(data);
							}
						},
						error: function (error) {
									
						}
					});
	}
}
function updateStatus3(a,b,c,d){
	//var productName = document.getElementById('aos_products_opportunities_1aos_products_ida').innerHTML;
	var record = GetURLParameter("record");
	var statuss = $("#opp3").val();
	if(statuss == "Hủy tìm hiểu"){
		var aler = a;
	}else if(statuss == "Hủy đặt cọc"){
		var aler = b;
	}else{
		var aler = c;
	}
	if(confirm(aler)){
		$.ajax({
						type: "POST",
						url: "index.php?entryPoint=updateProduct",
						data: {
							type:"3",
							//name:productName,
							id:record
						},
						//dataType: "json",
						success: function(data) {
							if(data != d){
								//$("#opp3").attr("disabled","disabled");
								//$("#opp2").attr("disabled","disabled");
								var statuss = document.getElementById("statusText");
								var depositText = document.getElementById("depositText");
								statuss.innerText = "Chốt thua";
								
								statuss.style.backgroundColor = "#00BFFF";
								depositText.style.backgroundColor = "#00BFFF";
								document.getElementById("probability").innerHTML = "0";
								document.getElementById("depositdate").innerHTML = data;
							}else{
								alert(data);
							}
						},
						error: function (error) {
									
						}
					});
	}
}
function loadGroup3(){
	SUGAR.ajaxUI.showLoadingPanel();
	var clicked;
	var title = document.getElementsByClassName("module-title-text")[0].innerHTML;
	title2 = title;
	var groupId=document.getElementById("provincecode").value;
	groupId2 = groupId;
	var mod2 = GetURLParameter("module");
	if(mod2 == null || mod2 == undefined){
		mod2 = modul;
	}
	mod3 = mod2;
	
	$.ajax({
                type: "POST",
                url: "index.php?module="+mod2+"&action=index&return_module="+mod2+"&return_action=DetailView&province="+groupId+"&sugar_body_only=true",
                dataType: "html",
				data: {
                    groupName:document.getElementById("myInput").value
                },
                success: function(data) {
					if(data != null && data != undefined && data != '' ){
					var html3 = '';
					html3 += '<div class="moduleTitle">';
					html3 += '<h2 class="module-title-text">'+title+'</h2>';
					html3 += '<span class="utils">';
					html3 += '<a href="#" class="btn btn-success showsearch">';
					html3 += '<span class=" glyphicon glyphicon-search" aria-hidden="true">';
					html3 += '</span>';
					html3 += '</a>&nbsp;';
					html3 += '<a id="create_image" href="index.php?module='+mod2+'&amp;action=EditView&amp;return_module='+mod2+'&amp;return_action=DetailView" class="utilsLink">';
					html3 += '<img src="themes/default/images/create-record.gif?v=golOgr3LHb5uVI36fQV1hQ" alt="Tạo">';
					html3 += '</a>';
					html3 += '<a id="create_link" href="index.php?module='+mod2+'&amp;action=EditView&amp;return_module='+mod2+'&amp;return_action=DetailView" class="utilsLink">';
					html3 += 'Tạo';
					html3 += '</a>';
					html3 += '</span>';
					html3 += '<div class="clear">';
					html3 += '</div>';
					html3 += '</div>';
					
					var html = '';
html += '<div class="row">';
html += '<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left" >';
html += '<select style="width:200px" name="group-users[]" id="provincecode" onchange="loadGroup2()">';
                                                                       html += '<option class="deleteRes" value="" selected="selected">----</option>';                          
                                                                        
                                                                        html += '<option style="display:none;" label=" TP Hà Nội" value="10" id="10"> TP Hà Nội</option>';
                                                                       html += ' <option style="display:none" label=" Tỉnh Hưng Yên" value="16" id="16"> Tỉnh Hưng Yên</option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hải Dương" value="17" id="17"> Tỉnh Hải Dương</option>';
                                                                        html += '<option style="display:none" label=" TP Hải Phòng " value="18" id="18"> TP Hải Phòng </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Ninh " value="20" id="20"> Tỉnh Quảng Ninh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bắc Ninh " value="22" id="22"> Tỉnh Bắc Ninh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bắc Giang " value="23" id="23"> Tỉnh Bắc Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Lạng Sơn " value="24" id="24"> Tỉnh Lạng Sơn </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Thái Nguyên " value="25" id="25"> Tỉnh Thái Nguyên </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bắc Kạn " value="26" id="26"> Tỉnh Bắc Kạn </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Cao Bằng " value="27" id="27"> Tỉnh Cao Bằng </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Vĩnh Phúc " value="28" id="28"> Tỉnh Vĩnh Phúc </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Phú Thọ " value="29" id="29"> Tỉnh Phú Thọ </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Tuyên Quang " value="30" id="30"> Tỉnh Tuyên Quang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hà Giang " value="31" id="31"> Tỉnh Hà Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Yên Bái " value="32" id="32"> Tỉnh Yên Bái </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Lào Cai " value="33" id="33"> Tỉnh Lào Cai </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hoà Bình " value="35" id="35"> Tỉnh Hoà Bình </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Sơn La " value="36" id="36"> Tỉnh Sơn La </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Điện Biên " value="38" id="38"> Tỉnh Điện Biên </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Lai Châu " value="39" id="39"> Tỉnh Lai Châu </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hà Nam " value="40" id="40"> Tỉnh Hà Nam </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Thái Bình " value="41" id="41"> Tỉnh Thái Bình </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Nam Định " value="42" id="42"> Tỉnh Nam Định </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Ninh Bình " value="43" id="43"> Tỉnh Ninh Bình </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Thanh Hoá " value="44" id="44"> Tỉnh Thanh Hoá </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Nghệ An " value="46" id="46"> Tỉnh Nghệ An </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hà Tĩnh " value="48" id="48"> Tỉnh Hà Tĩnh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Bình " value="51" id="51"> Tỉnh Quảng Bình </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Trị " value="52" id="52"> Tỉnh Quảng Trị </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Thừa Thiên Huế " value="53" id="53"> Tỉnh Thừa Thiên Huế </option>';
                                                                        html += '<option style="display:none" label=" TP Đà Nẵng " value="55" id="55"> TP Đà Nẵng </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Nam " value="56" id="56"> Tỉnh Quảng Nam </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Ngãi " value="57" id="57"> Tỉnh Quảng Ngãi </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Kon Tum " value="58" id="58"> Tỉnh Kon Tum </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bình Định " value="59" id="59"> Tỉnh Bình Định </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Gia Lai " value="60" id="60"> Tỉnh Gia Lai </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Phú Yên " value="62" id="62"> Tỉnh Phú Yên </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Đắk Lăk " value="63" id="63"> Tỉnh Đắk Lăk </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Đắk Nông " value="64" id="64"> Tỉnh Đắk Nông </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Khánh Hoà " value="65" id="65"> Tỉnh Khánh Hoà </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Ninh Thuận " value="66" id="66"> Tỉnh Ninh Thuận </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Lâm Đồng " value="67" id="67"> Tỉnh Lâm Đồng </option>';
                                                                        html += '<option style="display:none" label=" TP Hồ Chí Minh " value="70" id="70"> TP Hồ Chí Minh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bà Rịa Vũng Tàu " value="79" id="79"> Tỉnh Bà Rịa Vũng Tàu </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bình Thuận " value="80" id="80"> Tỉnh Bình Thuận </option>';
                                                                        html += '<option style="display:none;" label=" Tỉnh Đồng Nai " value="81" id="81"> Tỉnh Đồng Nai </option>';
                                                                        html += '<option style="display:none;" label=" Tỉnh Bình Dương " value="82" id="82"> Tỉnh Bình Dương </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bình Phước " value="83" id="83"> Tỉnh Bình Phước </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Tây Ninh " value="84" id="84"> Tỉnh Tây Ninh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Long An " value="85" id="85"> Tỉnh Long An </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Tiền Giang " value="86" id="86"> Tỉnh Tiền Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Đồng Tháp " value="87" id="87"> Tỉnh Đồng Tháp </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh An Giang " value="88" id="88"> Tỉnh An Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Vĩnh Long " value="89" id="89"> Tỉnh Vĩnh Long </option>';
                                                                        html += '<option style="display:none" label=" TP Cần Thơ " value="90" id="90"> TP Cần Thơ </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hậu Giang " value="91" id="91"> Tỉnh Hậu Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Kiên Giang " value="92" id="92"> Tỉnh Kiên Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bến Tre " value="93" id="93"> Tỉnh Bến Tre </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Trà Vinh " value="94" id="94"> Tỉnh Trà Vinh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Sóc Trăng " value="95" id="95"> Tỉnh Sóc Trăng </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bạc Liêu " value="96" id="96"> Tỉnh Bạc Liêu </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Cà Mau " value="97" id="97"> Tỉnh Cà Mau </option>';
                                                                        html += '<option style="display:none" label=" Vnpost " value="98" id="98"> Vnpost </option>';
                                                                        html += '<option style="display:none" label=" PPTT " value="99" id="99"> PPTT </option>';
                                                                        html += '<option style="display:none" label=" DVBC " value="100" id="100"> DVBC </option>';
                                                                        html += '<option style="display:none" label=" TCBC " value="101" id="101"> TCBC </option>';
                                                                        html += '<option style="display:none" label=" HCC " value="102" id="102"> HCC </option>';
																		html += '<option style="display:none" label=" Global " value="103" id="103"> Global </option>';	
                                                                    html += '</select>';
                                                                    html += '</div>';
                                                                    html += '<div id="groupUser" style="width:250px;" class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left" tabindex="0">';
                                                                    html += '<input style="width:250px;" placeholder="Search.." id="myInput" onkeyup="filterKeyy()">';
                                                                    html += ' <select style="width:250px;height:100px;display:none" name="group-users[]" id="group-users" multiple="multiple">';
																	html += '</select>';
																	html += '</div>';
																	html += '<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left">';
																	html += '<form id="groupName" method="POST" action="">';
																	html += '<input type=text" style="display:none" id="grouppname" name="groupName" value="">';
																	
																	html += '<span style="margin-left:20px"><input type="button" id="loadgroup" onclick="loadGroup3()" value="Lọc"></span>';
																	html += '<span style="margin-left:20px"><input class="deleteRes"  type="button" value="Xóa đk lọc"></span>';
																	//if(y != null && y != undefined){
																	//html += '<span><a id="clear" href="index.php?module='+y+'&action=index&return_module=Leads&return_action=DetailView&clear=1"><input type="button" value="Xóa ĐK Lọc"></a></span>';
																	//}else{
																	//html += '<span><a id="clear" onclick="SUGAR.savedViews.setChooser()"><input type="button" value="Xóa ĐK Lọc"></a></span>';
																	//}
																	html += '</form>';
																	html += '</div>';
																	html += '<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left">';
																	html += '</div>';																	
                                                                  html += '</div>';
					var data1 = html3 + html + data;
					$("#pagecontent").html(data1);
					$(document).ready(function(){
						var nextButton = document.getElementById("listViewNextButton_top");
						var prevButton = document.getElementById("listViewPrevButton_top");
						var endButton = document.getElementById("listViewEndButton_top");
						var startButton = document.getElementById("listViewStartButton_top");
						if((nextButton != null && nextButton != undefined) && (nextButton != null && nextButton != undefined) && nextButton.getAttribute("disabled") != "disabled"){
							var changeClick = nextButton.getAttribute("onclick").replace("sListView.save_checks", "save_checks2");
							nextButton.setAttribute("onclick",changeClick);
						}
						if((prevButton != null && prevButton != undefined) && prevButton.getAttribute("disabled") != "disabled"){
							var changeClick2 = prevButton.getAttribute("onclick").replace("sListView.save_checks", "save_checks2");
							prevButton.setAttribute("onclick",changeClick2);
						}
						if((endButton != null && endButton != undefined) && endButton.getAttribute("disabled") != "disabled"){
							var changeClick3 = endButton.getAttribute("onclick").replace("sListView.save_checks", "save_checks2");
							endButton.setAttribute("onclick",changeClick3);
						}
						if((startButton != null && startButton != undefined) && startButton.getAttribute("disabled") != "disabled"){
							var changeClick4 = startButton.getAttribute("onclick").replace("sListView.save_checks", "save_checks2");
							startButton.setAttribute("onclick",changeClick4);
						}
		var modulee = GetURLParameter("module");
		if(modulee != null && modulee != undefined){
		setCookie("modulee",modulee);
		}
		/*
		var div = document.createElement("div");
		div.setAttribute("id", "province");
		

    var pagecontent = document.getElementById("pagecontent");
    var listViewBody = document.getElementsByClassName("listViewBody")[0];   
    pagecontent.insertBefore(div,listViewBody);
    document.getElementById("province").innerHTML = html;
	*/
	showProvince();
	
	//var action = GetURLParameter("action");
	//if((action != null && action != undefined) && (modulee != null && modulee != undefined) && (parentTab == null || parentTab == undefined) && (provincee == null || provincee == undefined)){
	document.getElementById("provincecode").value = groupId;
	var groupSaved = getCookie("group");
	if(groupSaved != null && groupSaved != undefined && groupSaved != ''){
			groupSaved = JSON.parse(groupSaved);
				setTimeout(function(){
					var All;
					var checkbxName=[];
					var checkboxName =  document.getElementsByName("checkbxName");
					var length = checkboxName.length; 
					var length2 = groupSaved.length; 
					for(let i=0; i < length; i++)
					{
						for(let j = 0 ; j < length2; j++){	
								if(groupSaved[j] == checkboxName[i].value){
									checkboxName[i].checked = true;
								}
							
						}
					} 
					
					document.getElementById("grouppname").value = checkbxName;
					document.getElementById("myInput").value = groupSaved;
						//setCookie("group",JSON.stringify(checkbxName));
				},1500);
	}
				$.ajax({
							type: "POST",
							url: "index.php?entryPoint=getGroup",
							data: {
								id:groupId
							},
							dataType: "json",
							success: function(data) {
								if(data != null && data != '' && data != undefined){
									var obj=data;
									html="";
									html+='<input style="width:250px;" placeholder="Search.." id="myInput" onkeyup="filterKeyy()">';
									html+='<div tabindex="1" style="background-color:white;width:250px;height:100px;overflow:auto;display:none;position:absolute;z-index:10000" name="group-users[]" id="group-users" >';
									html+='<div onclick="getUser();getTitle()"><input type="checkbox" class="checkbx" name="checkbxName" value="All">All</div>';
									for(let i=0;i<obj.length;i++){
										html+='<div><input type="checkbox" class="checkbx" name="checkbxName" value="'+obj[i].name+'">'+obj[i].name+'</div>';
									}
									html+='</div>';
									$('#groupUser').html(html);
									$('#users').html('');
									$('#myInput').on("focus",function(){
										if(document.getElementById('group-users').style.display == "none"){
										$('#group-users').css("display","block");
										}
										// document.getElementById('group-users').focus();
									});
									//$('.checkbx').on("click",function(){
										 //clicked = true;
									//	 $("#group-users").trigger("focus");
									//});
									//document.getElementById('groupUser').onmouseenter = function(){
									//	 clicked = true;
									//	document.getElementById('group-users').focus();
									//};
									document.getElementById('groupUser').onmouseover = function(){
										 clicked = true;
										document.getElementById('group-users').focus();
									};
									
									document.getElementById('groupUser').onmouseleave = function(){
										 clicked = false;
									};
									$('#myInput').on("blur",function(){
										if(clicked != true){
										$('#group-users').css("display","none");
										}
									});
									$(".checkbx").on("click",function(){
										clicked = true;
										document.getElementById('group-users').focus();
										var All;
										var checkbxName=[];
										var checkboxName =  document.getElementsByName("checkbxName");
										var length = checkboxName.length; 
										for(let i=0; i < length; i++)
										{	
											if(checkboxName[i].checked == true){
												if(checkboxName[i].value == "All"){
													All = true;
													break;
												}else{
														checkbxName.push(checkboxName[i].value);
												}
											}
										} 
										if(All == true){
											checkbxName=[];
											for(let j=1; j < length; j++){
												checkbxName.push(checkboxName[j].value);
											}
										}
										document.getElementById("grouppname").value = checkbxName;
										document.getElementById("myInput").value = checkbxName;
										setCookie("group",JSON.stringify(checkbxName));
									});
									$('#group-users').on("blur",function(){
										if(clicked != true){
										$('#group-users').css("display","none");
										}else{
										document.getElementById('group-users').focus();	
										}
									});
									$("#loadgroup").on("click",function(){
									loadGroup3();
									});
								}
								setTimeout(function(){
									SUGAR.ajaxUI.hideLoadingPanel();
								},700);
							},
							error: function (error) {
								
							}
						});
				$(".deleteRes").on("click",function(){
					SUGAR.ajaxUI.showLoadingPanel();
					$.ajax({
                type: "POST",
                url: "index.php?module="+mod2+"&action=index&searchFormTab=basic_search&query=true&orderBy=DATE_ENTERED&sortOrder=DESC&current_user_only_basic=0&favorites_only_basic=0&button=Tìm kiếm&resetCondition=1&sugar_body_only=true",
                dataType: "html",
                success: function(data) {
					setCookie("group","");
					setCookie("province","");
					var html3 = '';
					html3 += '<div class="moduleTitle">';
					html3 += '<h2 class="module-title-text">'+title+'</h2>';
					html3 += '<span class="utils">';
					html3 += '<a href="#" class="btn btn-success showsearch">';
					html3 += '<span class=" glyphicon glyphicon-search" aria-hidden="true">';
					html3 += '</span>';
					html3 += '</a>&nbsp;';
					html3 += '<a id="create_image" href="index.php?module='+mod2+'&amp;action=EditView&amp;return_module='+mod2+'&amp;return_action=DetailView" class="utilsLink">';
					html3 += '<img src="themes/default/images/create-record.gif?v=golOgr3LHb5uVI36fQV1hQ" alt="Tạo">';
					html3 += '</a>';
					html3 += '<a id="create_link" href="index.php?module='+mod2+'&amp;action=EditView&amp;return_module='+mod2+'&amp;return_action=DetailView" class="utilsLink">';
					html3 += 'Tạo';
					html3 += '</a>';
					html3 += '</span>';
					html3 += '<div class="clear">';
					html3 += '</div>';
					html3 += '</div>';
					
					var html = '';
html += '<div class="row">';
html += '<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left" >';
html += '<select style="width:200px" name="group-users[]" id="provincecode" onchange="loadGroup2()">';
                                                                       html += '<option class="deleteRes" value="" selected="selected">----</option>';                          
                                                                      
                                                                        html += '<option style="display:none;" label=" TP Hà Nội" value="10" id="10"> TP Hà Nội</option>';
                                                                       html += ' <option style="display:none" label=" Tỉnh Hưng Yên" value="16" id="16"> Tỉnh Hưng Yên</option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hải Dương" value="17" id="17"> Tỉnh Hải Dương</option>';
                                                                        html += '<option style="display:none" label=" TP Hải Phòng " value="18" id="18"> TP Hải Phòng </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Ninh " value="20" id="20"> Tỉnh Quảng Ninh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bắc Ninh " value="22" id="22"> Tỉnh Bắc Ninh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bắc Giang " value="23" id="23"> Tỉnh Bắc Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Lạng Sơn " value="24" id="24"> Tỉnh Lạng Sơn </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Thái Nguyên " value="25" id="25"> Tỉnh Thái Nguyên </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bắc Kạn " value="26" id="26"> Tỉnh Bắc Kạn </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Cao Bằng " value="27" id="27"> Tỉnh Cao Bằng </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Vĩnh Phúc " value="28" id="28"> Tỉnh Vĩnh Phúc </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Phú Thọ " value="29" id="29"> Tỉnh Phú Thọ </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Tuyên Quang " value="30" id="30"> Tỉnh Tuyên Quang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hà Giang " value="31" id="31"> Tỉnh Hà Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Yên Bái " value="32" id="32"> Tỉnh Yên Bái </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Lào Cai " value="33" id="33"> Tỉnh Lào Cai </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hoà Bình " value="35" id="35"> Tỉnh Hoà Bình </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Sơn La " value="36" id="36"> Tỉnh Sơn La </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Điện Biên " value="38" id="38"> Tỉnh Điện Biên </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Lai Châu " value="39" id="39"> Tỉnh Lai Châu </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hà Nam " value="40" id="40"> Tỉnh Hà Nam </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Thái Bình " value="41" id="41"> Tỉnh Thái Bình </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Nam Định " value="42" id="42"> Tỉnh Nam Định </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Ninh Bình " value="43" id="43"> Tỉnh Ninh Bình </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Thanh Hoá " value="44" id="44"> Tỉnh Thanh Hoá </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Nghệ An " value="46" id="46"> Tỉnh Nghệ An </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hà Tĩnh " value="48" id="48"> Tỉnh Hà Tĩnh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Bình " value="51" id="51"> Tỉnh Quảng Bình </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Trị " value="52" id="52"> Tỉnh Quảng Trị </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Thừa Thiên Huế " value="53" id="53"> Tỉnh Thừa Thiên Huế </option>';
                                                                        html += '<option style="display:none" label=" TP Đà Nẵng " value="55" id="55"> TP Đà Nẵng </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Nam " value="56" id="56"> Tỉnh Quảng Nam </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Ngãi " value="57" id="57"> Tỉnh Quảng Ngãi </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Kon Tum " value="58" id="58"> Tỉnh Kon Tum </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bình Định " value="59" id="59"> Tỉnh Bình Định </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Gia Lai " value="60" id="60"> Tỉnh Gia Lai </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Phú Yên " value="62" id="62"> Tỉnh Phú Yên </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Đắk Lăk " value="63" id="63"> Tỉnh Đắk Lăk </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Đắk Nông " value="64" id="64"> Tỉnh Đắk Nông </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Khánh Hoà " value="65" id="65"> Tỉnh Khánh Hoà </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Ninh Thuận " value="66" id="66"> Tỉnh Ninh Thuận </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Lâm Đồng " value="67" id="67"> Tỉnh Lâm Đồng </option>';
                                                                        html += '<option style="display:none" label=" TP Hồ Chí Minh " value="70" id="70"> TP Hồ Chí Minh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bà Rịa Vũng Tàu " value="79" id="79"> Tỉnh Bà Rịa Vũng Tàu </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bình Thuận " value="80" id="80"> Tỉnh Bình Thuận </option>';
                                                                        html += '<option style="display:none;" label=" Tỉnh Đồng Nai " value="81" id="81"> Tỉnh Đồng Nai </option>';
                                                                        html += '<option style="display:none;" label=" Tỉnh Bình Dương " value="82" id="82"> Tỉnh Bình Dương </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bình Phước " value="83" id="83"> Tỉnh Bình Phước </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Tây Ninh " value="84" id="84"> Tỉnh Tây Ninh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Long An " value="85" id="85"> Tỉnh Long An </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Tiền Giang " value="86" id="86"> Tỉnh Tiền Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Đồng Tháp " value="87" id="87"> Tỉnh Đồng Tháp </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh An Giang " value="88" id="88"> Tỉnh An Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Vĩnh Long " value="89" id="89"> Tỉnh Vĩnh Long </option>';
                                                                        html += '<option style="display:none" label=" TP Cần Thơ " value="90" id="90"> TP Cần Thơ </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hậu Giang " value="91" id="91"> Tỉnh Hậu Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Kiên Giang " value="92" id="92"> Tỉnh Kiên Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bến Tre " value="93" id="93"> Tỉnh Bến Tre </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Trà Vinh " value="94" id="94"> Tỉnh Trà Vinh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Sóc Trăng " value="95" id="95"> Tỉnh Sóc Trăng </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bạc Liêu " value="96" id="96"> Tỉnh Bạc Liêu </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Cà Mau " value="97" id="97"> Tỉnh Cà Mau </option>';
                                                                        html += '<option style="display:none" label=" Vnpost " value="98" id="98"> Vnpost </option>';
                                                                        html += '<option style="display:none" label=" PPTT " value="99" id="99"> PPTT </option>';
                                                                        html += '<option style="display:none" label=" DVBC " value="100" id="100"> DVBC </option>';
                                                                        html += '<option style="display:none" label=" TCBC " value="101" id="101"> TCBC </option>';
                                                                        html += '<option style="display:none" label=" HCC " value="102" id="102"> HCC </option>';
																			html += '<option style="display:none" label=" Global " value="103" id="103"> Global </option>';
                                                                    html += '</select>';
                                                                    html += '</div>';
                                                                    html += '<div id="groupUser" style="width:250px;" class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left" tabindex="0">';
                                                                    html += '<input style="width:250px;" placeholder="Search.." id="myInput" onkeyup="filterKeyy()">';
                                                                    html += ' <select style="width:250px;height:100px;display:none" name="group-users[]" id="group-users" multiple="multiple">';
																	html += '</select>';
																	html += '</div>';
																	html += '<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left">';
																	html += '<form id="groupName" method="POST" action="">';
																	html += '<input type=text" style="display:none" id="grouppname" name="groupName" value="">';
																	
																	html += '<span style="margin-left:20px"><input type="button" id="loadgroup" onclick="loadGroup3()" value="Lọc"></span>';
																	html += '<span style="margin-left:20px"><input type="button" class="deleteRes" value="Xóa đk lọc"></span>';
																	//if(y != null && y != undefined){
																	//html += '<span><a id="clear" href="index.php?module='+y+'&action=index&return_module=Leads&return_action=DetailView&clear=1"><input type="button" value="Xóa ĐK Lọc"></a></span>';
																	//}else{
																	//html += '<span><a id="clear" onclick="SUGAR.savedViews.setChooser()"><input type="button" value="Xóa ĐK Lọc"></a></span>';
																	//}
																	html += '</form>';
																	html += '</div>';
																	html += '<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left">';
																	html += '</div>';																	
                                                                  html += '</div>';
					var data1 = html3 + html + data;
					
					$("#pagecontent").html(data1);
					$(document).ready(function(){
						var nextButton = document.getElementById("listViewNextButton_top");
						var prevButton = document.getElementById("listViewPrevButton_top");
						var endButton = document.getElementById("listViewEndButton_top");
						var startButton = document.getElementById("listViewStartButton_top");
						if((nextButton != null && nextButton != undefined) && nextButton.getAttribute("disabled") != "disabled"){
							var changeClick = nextButton.getAttribute("onclick").replace("sListView.save_checks", "save_checks2");
							nextButton.setAttribute("onclick",changeClick);
						}
						if((prevButton != null && prevButton != undefined) && prevButton.getAttribute("disabled") != "disabled"){
							var changeClick2 = prevButton.getAttribute("onclick").replace("sListView.save_checks", "save_checks2");
							prevButton.setAttribute("onclick",changeClick2);
						}
						if((endButton != null && endButton != undefined) && endButton.getAttribute("disabled") != "disabled"){
							var changeClick3 = endButton.getAttribute("onclick").replace("sListView.save_checks", "save_checks2");
							endButton.setAttribute("onclick",changeClick3);
						}
						if((startButton != null && startButton != undefined) && startButton.getAttribute("disabled") != "disabled"){
							var changeClick4 = startButton.getAttribute("onclick").replace("sListView.save_checks", "save_checks2");
							startButton.setAttribute("onclick",changeClick4);
						}
		var modulee = GetURLParameter("module");
		if(modulee != null && modulee != undefined){
		setCookie("modulee",modulee);
		}
		
	showProvince();
	
	//var action = GetURLParameter("action");
	//if((action != null && action != undefined) && (modulee != null && modulee != undefined) && (parentTab == null || parentTab == undefined) && (provincee == null || provincee == undefined)){
	document.getElementById("provincecode").value = "";
	var groupSaved = getCookie("group");
	if(groupSaved != null && groupSaved != undefined && groupSaved != ''){
			groupSaved = JSON.parse(groupSaved);
				setTimeout(function(){
					var All;
					var checkbxName=[];
					var checkboxName =  document.getElementsByName("checkbxName");
					var length = checkboxName.length; 
					var length2 = groupSaved.length; 
					for(let i=0; i < length; i++)
					{
						for(let j = 0 ; j < length2; j++){	
								if(groupSaved[j] == checkboxName[i].value){
									checkboxName[i].checked = true;
								}
							
						}
					} 
					
					document.getElementById("grouppname").value = checkbxName;
					document.getElementById("myInput").value = groupSaved;
						//setCookie("group",JSON.stringify(checkbxName));
				},1500);
	}
				$.ajax({
							type: "POST",
							url: "index.php?entryPoint=getGroup",
							data: {
								id:groupId2
							},
							dataType: "json",
							success: function(data) {
								if(data != null && data != '' && data != undefined){
									var obj=data;
									html="";
									html+='<input style="width:250px;" placeholder="Search.." id="myInput" onkeyup="filterKeyy()">';
									html+='<div tabindex="1" style="background-color:white;width:250px;height:100px;overflow:auto;display:none;position:absolute;z-index:10000" name="group-users[]" id="group-users" >';
									html+='<div onclick="getUser();getTitle()"><input type="checkbox" class="checkbx" name="checkbxName" value="All">All</div>';
									for(let i=0;i<obj.length;i++){
										html+='<div><input type="checkbox" class="checkbx" name="checkbxName" value="'+obj[i].name+'">'+obj[i].name+'</div>';
									}
									html+='</div>';
									$('#groupUser').html(html);
									$('#users').html('');
									$('#myInput').on("focus",function(){
										if(document.getElementById('group-users').style.display == "none"){
										$('#group-users').css("display","block");
										}
										// document.getElementById('group-users').focus();
									});
									//$('.checkbx').on("click",function(){
										 //clicked = true;
									//	 $("#group-users").trigger("focus");
									//});
									//document.getElementById('groupUser').onmouseenter = function(){
									//	 clicked = true;
									//	document.getElementById('group-users').focus();
									//};
									document.getElementById('groupUser').onmouseover = function(){
										 clicked = true;
										document.getElementById('group-users').focus();
									};
									
									document.getElementById('groupUser').onmouseleave = function(){
										 clicked = false;
									};
									$('#myInput').on("blur",function(){
										if(clicked != true){
										$('#group-users').css("display","none");
										}
									});
									$(".checkbx").on("click",function(){
										clicked = true;
										document.getElementById('group-users').focus();
										var All;
										var checkbxName=[];
										var checkboxName =  document.getElementsByName("checkbxName");
										var length = checkboxName.length; 
										for(let i=0; i < length; i++)
										{	
											if(checkboxName[i].checked == true){
												if(checkboxName[i].value == "All"){
													All = true;
													break;
												}else{
														checkbxName.push(checkboxName[i].value);
												}
											}
										} 
										if(All == true){
											checkbxName=[];
											for(let j=1; j < length; j++){
												checkbxName.push(checkboxName[j].value);
											}
										}
										document.getElementById("grouppname").value = checkbxName;
										document.getElementById("myInput").value = checkbxName;
										setCookie("group",JSON.stringify(checkbxName));
									});
									$('#group-users').on("blur",function(){
										if(clicked != true){
										$('#group-users').css("display","none");
										}else{
										document.getElementById('group-users').focus();	
										}
									});
									$("#loadgroup").on("click",function(){
									loadGroup3();
									});
								}
								
								setTimeout(function(){
									SUGAR.ajaxUI.hideLoadingPanel();
								},700);
							},
							error: function (error) {
								
							}
						});
				
				
		});
                },
                error: function (error) {
                    
                }
            });
				});
		});
				
	}	
                },
                error: function (error) {
                    
                }
            });
}
function refreshFilter(){
	$("#group-users option:selected").removeAttr("selected");
	$("#users option:selected").removeAttr("selected");
	$('#period option[value="default"]').attr("selected", "selected");
}
function exportInvoices(id,type){
	$("#tab-actions").removeClass("open");
	$("#tab-actions .dropdown-toggle").attr("aria-expanded","false");
	$.ajax({
        type: "POST",
        url: "index.php?entryPoint=getInvLanguage",
		dataType:"json",
        success: function(data) {
			if(type == "1"){
				if(confirm(data[0].language.LBL_QUESTION_EXPORT_VAT)){
					SUGAR.ajaxUI.showLoadingPanel();
					$.ajax({
						type: "POST",
						url: "index.php?entryPoint=exportInvoice",
						//dataType: "json",
						data:{
							id:id,
							type:type
						},
						success: function(data) {
							if(data.indexOf("http") != "-1"){
								window.location = data;
								setTimeout(function(){
									SUGAR.ajaxUI.hideLoadingPanel();
								},8000);
							}else{
								alert(data);
								SUGAR.ajaxUI.hideLoadingPanel();
							}
							
						},
						error: function (error) {
									
						}
					});
				}
			}else{
				SUGAR.ajaxUI.showLoadingPanel();
				$.ajax({
						type: "POST",
						url: "index.php?entryPoint=exportInvoice",
						//dataType: "json",
						data:{
							id:id,
							type:type
						},
						success: function(data) {
							window.location = data;
							setTimeout(function(){
							SUGAR.ajaxUI.hideLoadingPanel();
							},8000);
						},
						error: function (error) {
									
						}
				});
			}
        },
        error: function (error) {
                    
        }
    });
	
}
function changeInvoices(id){
	$("#tab-actions").removeClass("open");
	$("#tab-actions .dropdown-toggle").attr("aria-expanded","false");
	$.ajax({
        type: "POST",
        url: "index.php?entryPoint=changeInvoices",
        //dataType: "json",
		data:{
			id:id
			
		},
        success: function(data) {
			alert(data);
			
        },
        error: function (error) {
                    
        }
    });
	
}
function replaceInvoices(id){
	$("#tab-actions").removeClass("open");
	$("#tab-actions .dropdown-toggle").attr("aria-expanded","false");
	$.ajax({
        type: "POST",
        url: "index.php?entryPoint=replaceInvoices",
        //dataType: "json",
		data:{
			id:id
			
		},
        success: function(data) {
			alert(data);
			
        },
        error: function (error) {
                    
        }
    });
	
}
function showProvince(){
	$.ajax({
                type: "POST",
                url: "index.php?entryPoint=showProvince",
                dataType: "json",
                success: function(data) {
					for(let i=0;i<data.length;i++){
						document.getElementById(data[i]).style.display="block";
					}
                },
                error: function (error) {
                    
                }
            });
}
function moduleFilter(recordNum){
		SUGAR.ajaxUI.showLoadingPanel();
	var module = $("#module").val();
	var province = $("#provincecode_c").val();
	var groupUsers=$("#group-users").val();
	var users=$("#users").val();
	var titleUsers=$("#title-users").val();
	var period=$("#period").val();
	var date_from = $('#date_from').val();
    var date_to = $('#date_to').val();
	$.ajax({
                type: "POST",
                url: "index.php?entryPoint=moduleFilter",
                data: {
					recordNum:recordNum,
					module:module,
                    province:province,
					groupUsers:groupUsers,
					users:users,
					titleUsers:titleUsers,
					period:period,
					date_from:date_from,
					date_to:date_to
                },
                success: function(dat) {
					if(dat != null && dat != '' && dat != undefined){
						var Obj=JSON.parse(dat);
						
						var data = [];
						var userData = [];
						for(let i = 0; i < Obj.length;i++){
							//data.push(Obj[i].data.);
							for (let key in Obj[i].data) {
								for(let j = 0; j<Obj[i].data[key].length; j++){
								data.push(Obj[i].data[key][j]);	
								}
							}
							
						}
						var table = $('#userView').DataTable();
						table.clear().draw();
						setTimeout(function(){
						if(data.length > 0){
						data.sort(function(a, b) {
							//return (a.userName) - (b.userName);
							  return (a.userName < b.userName) ? -1 : (a.userName > b.userName) ? 1 : 0;
						});
						
						
						
						for (let i = 0;i<data.length;i++) {
						
						
						table.row.add([
						eval(i+1),
						data[i].lastName,
						data[i].phoneMobile,
						data[i].accountName,
						data[i].name,
						data[i].description,
						data[i].status,
						data[i].primaryAddressStreet,
						data[i].emailAddress,
						data[i].source,
						data[i].product,
						data[i].userName,
						data[i].date,
						data[i].group,
						]).draw( false );
						}
						
						//
						
						
						//for(let k = 1;k < eval(data.length/100);k++){
						//html+='<div style="border:" id="page'+k+'">'+k+'</div>';
						//}
							//$("#father-group").html(html);
							
						}	 
							SUGAR.ajaxUI.hideLoadingPanel();
							 
							 },3000);
					}
                },
                error: function (error) {
                    
                }
            });
}
function save_checks2(offset,moduleString)
{
	//checks=sugarListView.get_checks();
	if(typeof document.MassUpdate!='undefined'){
		document.MassUpdate.elements[moduleString].value=offset;
if(typeof document.MassUpdate.massupdate!='undefined')
{
	document.MassUpdate.massupdate.value='false';
}
document.MassUpdate.action.value=document.MassUpdate.return_action.value;
document.MassUpdate.return_module.value='';
document.MassUpdate.return_action.value='';
//document.MassUpdate.submit();
$.ajax({
					type: "POST",
					url: "index.php?sugar_body_only=true",
					data: $('#MassUpdate').serialize(),
					dataType: "html",
					success: function(data) {
						
						var html3 = '';
					html3 += '<div class="moduleTitle">';
					html3 += '<h2 class="module-title-text">'+title2+'</h2>';
					html3 += '<span class="utils">';
					html3 += '<a href="#" class="btn btn-success showsearch">';
					html3 += '<span class=" glyphicon glyphicon-search" aria-hidden="true">';
					html3 += '</span>';
					html3 += '</a>&nbsp;';
					html3 += '<a id="create_image" href="index.php?module='+mod3+'&amp;action=EditView&amp;return_module='+mod3+'&amp;return_action=DetailView" class="utilsLink">';
					html3 += '<img src="themes/default/images/create-record.gif?v=golOgr3LHb5uVI36fQV1hQ" alt="Tạo">';
					html3 += '</a>';
					html3 += '<a id="create_link" href="index.php?module='+mod3+'&amp;action=EditView&amp;return_module='+mod3+'&amp;return_action=DetailView" class="utilsLink">';
					html3 += 'Tạo';
					html3 += '</a>';
					html3 += '</span>';
					html3 += '<div class="clear">';
					html3 += '</div>';
					html3 += '</div>';
					
					var html = '';
html += '<div class="row">';
html += '<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left" >';
html += '<select style="width:200px" name="group-users[]" id="provincecode" onchange="loadGroup2()">';
                                                                       html += '<option class="deleteRes" value="" selected="selected">----</option>';                          
                                                                       
                                                                        html += '<option style="display:none;" label=" TP Hà Nội" value="10" id="10"> TP Hà Nội</option>';
                                                                       html += ' <option style="display:none" label=" Tỉnh Hưng Yên" value="16" id="16"> Tỉnh Hưng Yên</option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hải Dương" value="17" id="17"> Tỉnh Hải Dương</option>';
                                                                        html += '<option style="display:none" label=" TP Hải Phòng " value="18" id="18"> TP Hải Phòng </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Ninh " value="20" id="20"> Tỉnh Quảng Ninh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bắc Ninh " value="22" id="22"> Tỉnh Bắc Ninh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bắc Giang " value="23" id="23"> Tỉnh Bắc Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Lạng Sơn " value="24" id="24"> Tỉnh Lạng Sơn </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Thái Nguyên " value="25" id="25"> Tỉnh Thái Nguyên </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bắc Kạn " value="26" id="26"> Tỉnh Bắc Kạn </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Cao Bằng " value="27" id="27"> Tỉnh Cao Bằng </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Vĩnh Phúc " value="28" id="28"> Tỉnh Vĩnh Phúc </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Phú Thọ " value="29" id="29"> Tỉnh Phú Thọ </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Tuyên Quang " value="30" id="30"> Tỉnh Tuyên Quang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hà Giang " value="31" id="31"> Tỉnh Hà Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Yên Bái " value="32" id="32"> Tỉnh Yên Bái </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Lào Cai " value="33" id="33"> Tỉnh Lào Cai </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hoà Bình " value="35" id="35"> Tỉnh Hoà Bình </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Sơn La " value="36" id="36"> Tỉnh Sơn La </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Điện Biên " value="38" id="38"> Tỉnh Điện Biên </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Lai Châu " value="39" id="39"> Tỉnh Lai Châu </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hà Nam " value="40" id="40"> Tỉnh Hà Nam </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Thái Bình " value="41" id="41"> Tỉnh Thái Bình </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Nam Định " value="42" id="42"> Tỉnh Nam Định </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Ninh Bình " value="43" id="43"> Tỉnh Ninh Bình </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Thanh Hoá " value="44" id="44"> Tỉnh Thanh Hoá </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Nghệ An " value="46" id="46"> Tỉnh Nghệ An </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hà Tĩnh " value="48" id="48"> Tỉnh Hà Tĩnh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Bình " value="51" id="51"> Tỉnh Quảng Bình </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Trị " value="52" id="52"> Tỉnh Quảng Trị </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Thừa Thiên Huế " value="53" id="53"> Tỉnh Thừa Thiên Huế </option>';
                                                                        html += '<option style="display:none" label=" TP Đà Nẵng " value="55" id="55"> TP Đà Nẵng </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Nam " value="56" id="56"> Tỉnh Quảng Nam </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Ngãi " value="57" id="57"> Tỉnh Quảng Ngãi </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Kon Tum " value="58" id="58"> Tỉnh Kon Tum </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bình Định " value="59" id="59"> Tỉnh Bình Định </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Gia Lai " value="60" id="60"> Tỉnh Gia Lai </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Phú Yên " value="62" id="62"> Tỉnh Phú Yên </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Đắk Lăk " value="63" id="63"> Tỉnh Đắk Lăk </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Đắk Nông " value="64" id="64"> Tỉnh Đắk Nông </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Khánh Hoà " value="65" id="65"> Tỉnh Khánh Hoà </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Ninh Thuận " value="66" id="66"> Tỉnh Ninh Thuận </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Lâm Đồng " value="67" id="67"> Tỉnh Lâm Đồng </option>';
                                                                        html += '<option style="display:none" label=" TP Hồ Chí Minh " value="70" id="70"> TP Hồ Chí Minh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bà Rịa Vũng Tàu " value="79" id="79"> Tỉnh Bà Rịa Vũng Tàu </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bình Thuận " value="80" id="80"> Tỉnh Bình Thuận </option>';
                                                                        html += '<option style="display:none;" label=" Tỉnh Đồng Nai " value="81" id="81"> Tỉnh Đồng Nai </option>';
                                                                        html += '<option style="display:none;" label=" Tỉnh Bình Dương " value="82" id="82"> Tỉnh Bình Dương </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bình Phước " value="83" id="83"> Tỉnh Bình Phước </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Tây Ninh " value="84" id="84"> Tỉnh Tây Ninh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Long An " value="85" id="85"> Tỉnh Long An </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Tiền Giang " value="86" id="86"> Tỉnh Tiền Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Đồng Tháp " value="87" id="87"> Tỉnh Đồng Tháp </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh An Giang " value="88" id="88"> Tỉnh An Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Vĩnh Long " value="89" id="89"> Tỉnh Vĩnh Long </option>';
                                                                        html += '<option style="display:none" label=" TP Cần Thơ " value="90" id="90"> TP Cần Thơ </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hậu Giang " value="91" id="91"> Tỉnh Hậu Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Kiên Giang " value="92" id="92"> Tỉnh Kiên Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bến Tre " value="93" id="93"> Tỉnh Bến Tre </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Trà Vinh " value="94" id="94"> Tỉnh Trà Vinh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Sóc Trăng " value="95" id="95"> Tỉnh Sóc Trăng </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bạc Liêu " value="96" id="96"> Tỉnh Bạc Liêu </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Cà Mau " value="97" id="97"> Tỉnh Cà Mau </option>';
                                                                        html += '<option style="display:none" label=" Vnpost " value="98" id="98"> Vnpost </option>';
                                                                        html += '<option style="display:none" label=" PPTT " value="99" id="99"> PPTT </option>';
                                                                        html += '<option style="display:none" label=" DVBC " value="100" id="100"> DVBC </option>';
                                                                        html += '<option style="display:none" label=" TCBC " value="101" id="101"> TCBC </option>';
                                                                        html += '<option style="display:none" label=" HCC " value="102" id="102"> HCC </option>';
																		html += '<option style="display:none" label=" Global " value="103" id="103"> Global </option>';
                                                                    html += '</select>';
                                                                    html += '</div>';
                                                                    html += '<div id="groupUser" style="width:250px;" class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left" tabindex="0">';
                                                                    html += '<input style="width:250px;" placeholder="Search.." id="myInput" onkeyup="filterKeyy()">';
                                                                    html += ' <select style="width:250px;height:100px;display:none" name="group-users[]" id="group-users" multiple="multiple">';
																	html += '</select>';
																	html += '</div>';
																	html += '<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left">';
																	html += '<form id="groupName" method="POST" action="">';
																	html += '<input type=text" style="display:none" id="grouppname" name="groupName" value="">';
																	
																	html += '<span style="margin-left:20px"><input type="button" id="loadgroup" onclick="loadGroup3()" value="Lọc"></span>';
																	html += '<span style="margin-left:20px"><input type="button" class="deleteRes" value="Xóa đk lọc"></span>';
																	//if(y != null && y != undefined){
																	//html += '<span><a id="clear" href="index.php?module='+y+'&action=index&return_module=Leads&return_action=DetailView&clear=1"><input type="button" value="Xóa ĐK Lọc"></a></span>';
																	//}else{
																	//html += '<span><a id="clear" onclick="SUGAR.savedViews.setChooser()"><input type="button" value="Xóa ĐK Lọc"></a></span>';
																	//}
																	html += '</form>';
																	html += '</div>';
																	html += '<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left">';
																	html += '</div>';																	
                                                                  html += '</div>';
					var data1 = html3 + html + data;
					
					$("#pagecontent").html(data1);
					$(document).ready(function(){
						var nextButton = document.getElementById("listViewNextButton_top");
						var prevButton = document.getElementById("listViewPrevButton_top");
						var endButton = document.getElementById("listViewEndButton_top");
						var startButton = document.getElementById("listViewStartButton_top");
						if((nextButton != null && nextButton != undefined) && nextButton.getAttribute("disabled") != "disabled"){
							var changeClick = nextButton.getAttribute("onclick").replace("sListView.save_checks", "save_checks2");
							nextButton.setAttribute("onclick",changeClick);
						}
						if((prevButton != null && prevButton != undefined) && prevButton.getAttribute("disabled") != "disabled"){
							var changeClick2 = prevButton.getAttribute("onclick").replace("sListView.save_checks", "save_checks2");
							prevButton.setAttribute("onclick",changeClick2);
						}
						if((endButton != null && endButton != undefined) && endButton.getAttribute("disabled") != "disabled"){
							var changeClick3 = endButton.getAttribute("onclick").replace("sListView.save_checks", "save_checks2");
							endButton.setAttribute("onclick",changeClick3);
						}
						if((startButton != null && startButton != undefined) && startButton.getAttribute("disabled") != "disabled"){
							var changeClick4 = startButton.getAttribute("onclick").replace("sListView.save_checks", "save_checks2");
							startButton.setAttribute("onclick",changeClick4);
						}
		var modulee = GetURLParameter("module");
		if(modulee != null && modulee != undefined){
		setCookie("modulee",modulee);
		}
		
		

    
	showProvince();
	
	//var action = GetURLParameter("action");
	//if((action != null && action != undefined) && (modulee != null && modulee != undefined) && (parentTab == null || parentTab == undefined) && (provincee == null || provincee == undefined)){
	document.getElementById("provincecode").value = groupId2;
	var groupSaved = getCookie("group");
	if(groupSaved != null && groupSaved != undefined && groupSaved != ''){
			groupSaved = JSON.parse(groupSaved);
				setTimeout(function(){
					var All;
					var checkbxName=[];
					var checkboxName =  document.getElementsByName("checkbxName");
					var length = checkboxName.length; 
					var length2 = groupSaved.length; 
					for(let i=0; i < length; i++)
					{
						for(let j = 0 ; j < length2; j++){	
								if(groupSaved[j] == checkboxName[i].value){
									checkboxName[i].checked = true;
								}
							
						}
					} 
					
					document.getElementById("grouppname").value = checkbxName;
					document.getElementById("myInput").value = groupSaved;
						//setCookie("group",JSON.stringify(checkbxName));
				},1500);
	}
				$.ajax({
							type: "POST",
							url: "index.php?entryPoint=getGroup",
							data: {
								id:groupId2
							},
							dataType: "json",
							success: function(data) {
								if(data != null && data != '' && data != undefined){
									var obj=data;
									html="";
									html+='<input style="width:250px;" placeholder="Search.." id="myInput" onkeyup="filterKeyy()">';
									html+='<div tabindex="1" style="background-color:white;width:250px;height:100px;overflow:auto;display:none;position:absolute;z-index:10000" name="group-users[]" id="group-users" >';
									html+='<div onclick="getUser();getTitle()"><input type="checkbox" class="checkbx" name="checkbxName" value="All">All</div>';
									for(let i=0;i<obj.length;i++){
										html+='<div><input type="checkbox" class="checkbx" name="checkbxName" value="'+obj[i].name+'">'+obj[i].name+'</div>';
									}
									html+='</div>';
									$('#groupUser').html(html);
									$('#users').html('');
									$('#myInput').on("focus",function(){
										if(document.getElementById('group-users').style.display == "none"){
										$('#group-users').css("display","block");
										}
										// document.getElementById('group-users').focus();
									});
									//$('.checkbx').on("click",function(){
										 //clicked = true;
									//	 $("#group-users").trigger("focus");
									//});
									//document.getElementById('groupUser').onmouseenter = function(){
									//	 clicked = true;
									//	document.getElementById('group-users').focus();
									//};
									document.getElementById('groupUser').onmouseover = function(){
										 clicked = true;
										document.getElementById('group-users').focus();
									};
									
									document.getElementById('groupUser').onmouseleave = function(){
										 clicked = false;
									};
									$('#myInput').on("blur",function(){
										if(clicked != true){
										$('#group-users').css("display","none");
										}
									});
									$(".checkbx").on("click",function(){
										clicked = true;
										document.getElementById('group-users').focus();
										var All;
										var checkbxName=[];
										var checkboxName =  document.getElementsByName("checkbxName");
										var length = checkboxName.length; 
										for(let i=0; i < length; i++)
										{	
											if(checkboxName[i].checked == true){
												if(checkboxName[i].value == "All"){
													All = true;
													break;
												}else{
														checkbxName.push(checkboxName[i].value);
												}
											}
										} 
										if(All == true){
											checkbxName=[];
											for(let j=1; j < length; j++){
												checkbxName.push(checkboxName[j].value);
											}
										}
										document.getElementById("grouppname").value = checkbxName;
										document.getElementById("myInput").value = checkbxName;
										setCookie("group",JSON.stringify(checkbxName));
									});
									$('#group-users').on("blur",function(){
										if(clicked != true){
										$('#group-users').css("display","none");
										}else{
										document.getElementById('group-users').focus();	
										}
									});
								}
								setTimeout(function(){
									SUGAR.ajaxUI.hideLoadingPanel();
								},700);
							},
							error: function (error) {
								
							}
						});
				$(".deleteRes").on("click",function(){
					SUGAR.ajaxUI.showLoadingPanel();
					$.ajax({
                type: "POST",
                url: "index.php?module="+mod3+"&action=index&searchFormTab=basic_search&query=true&orderBy=DATE_ENTERED&sortOrder=DESC&current_user_only_basic=0&favorites_only_basic=0&button=Tìm kiếm&resetCondition=1&sugar_body_only=true",
                dataType: "html",
                success: function(data) {
					setCookie("group","");
					setCookie("province","");
					var html3 = '';
					html3 += '<div class="moduleTitle">';
					html3 += '<h2 class="module-title-text">'+title2+'</h2>';
					html3 += '<span class="utils">';
					html3 += '<a href="#" class="btn btn-success showsearch">';
					html3 += '<span class=" glyphicon glyphicon-search" aria-hidden="true">';
					html3 += '</span>';
					html3 += '</a>&nbsp;';
					html3 += '<a id="create_image" href="index.php?module='+mod3+'&amp;action=EditView&amp;return_module='+mod3+'&amp;return_action=DetailView" class="utilsLink">';
					html3 += '<img src="themes/default/images/create-record.gif?v=golOgr3LHb5uVI36fQV1hQ" alt="Tạo">';
					html3 += '</a>';
					html3 += '<a id="create_link" href="index.php?module='+mod3+'&amp;action=EditView&amp;return_module='+mod3+'&amp;return_action=DetailView" class="utilsLink">';
					html3 += 'Tạo';
					html3 += '</a>';
					html3 += '</span>';
					html3 += '<div class="clear">';
					html3 += '</div>';
					html3 += '</div>';
					
					var html = '';
html += '<div class="row">';
html += '<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left" >';
html += '<select style="width:200px" name="group-users[]" id="provincecode" onchange="loadGroup2()">';
                                                                       html += '<option class="deleteRes" value="" selected="selected">----</option>';                          
                                                                       
                                                                        html += '<option style="display:none;" label=" TP Hà Nội" value="10" id="10"> TP Hà Nội</option>';
                                                                       html += ' <option style="display:none" label=" Tỉnh Hưng Yên" value="16" id="16"> Tỉnh Hưng Yên</option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hải Dương" value="17" id="17"> Tỉnh Hải Dương</option>';
                                                                        html += '<option style="display:none" label=" TP Hải Phòng " value="18" id="18"> TP Hải Phòng </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Ninh " value="20" id="20"> Tỉnh Quảng Ninh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bắc Ninh " value="22" id="22"> Tỉnh Bắc Ninh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bắc Giang " value="23" id="23"> Tỉnh Bắc Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Lạng Sơn " value="24" id="24"> Tỉnh Lạng Sơn </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Thái Nguyên " value="25" id="25"> Tỉnh Thái Nguyên </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bắc Kạn " value="26" id="26"> Tỉnh Bắc Kạn </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Cao Bằng " value="27" id="27"> Tỉnh Cao Bằng </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Vĩnh Phúc " value="28" id="28"> Tỉnh Vĩnh Phúc </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Phú Thọ " value="29" id="29"> Tỉnh Phú Thọ </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Tuyên Quang " value="30" id="30"> Tỉnh Tuyên Quang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hà Giang " value="31" id="31"> Tỉnh Hà Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Yên Bái " value="32" id="32"> Tỉnh Yên Bái </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Lào Cai " value="33" id="33"> Tỉnh Lào Cai </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hoà Bình " value="35" id="35"> Tỉnh Hoà Bình </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Sơn La " value="36" id="36"> Tỉnh Sơn La </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Điện Biên " value="38" id="38"> Tỉnh Điện Biên </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Lai Châu " value="39" id="39"> Tỉnh Lai Châu </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hà Nam " value="40" id="40"> Tỉnh Hà Nam </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Thái Bình " value="41" id="41"> Tỉnh Thái Bình </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Nam Định " value="42" id="42"> Tỉnh Nam Định </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Ninh Bình " value="43" id="43"> Tỉnh Ninh Bình </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Thanh Hoá " value="44" id="44"> Tỉnh Thanh Hoá </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Nghệ An " value="46" id="46"> Tỉnh Nghệ An </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hà Tĩnh " value="48" id="48"> Tỉnh Hà Tĩnh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Bình " value="51" id="51"> Tỉnh Quảng Bình </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Trị " value="52" id="52"> Tỉnh Quảng Trị </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Thừa Thiên Huế " value="53" id="53"> Tỉnh Thừa Thiên Huế </option>';
                                                                        html += '<option style="display:none" label=" TP Đà Nẵng " value="55" id="55"> TP Đà Nẵng </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Nam " value="56" id="56"> Tỉnh Quảng Nam </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Ngãi " value="57" id="57"> Tỉnh Quảng Ngãi </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Kon Tum " value="58" id="58"> Tỉnh Kon Tum </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bình Định " value="59" id="59"> Tỉnh Bình Định </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Gia Lai " value="60" id="60"> Tỉnh Gia Lai </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Phú Yên " value="62" id="62"> Tỉnh Phú Yên </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Đắk Lăk " value="63" id="63"> Tỉnh Đắk Lăk </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Đắk Nông " value="64" id="64"> Tỉnh Đắk Nông </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Khánh Hoà " value="65" id="65"> Tỉnh Khánh Hoà </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Ninh Thuận " value="66" id="66"> Tỉnh Ninh Thuận </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Lâm Đồng " value="67" id="67"> Tỉnh Lâm Đồng </option>';
                                                                        html += '<option style="display:none" label=" TP Hồ Chí Minh " value="70" id="70"> TP Hồ Chí Minh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bà Rịa Vũng Tàu " value="79" id="79"> Tỉnh Bà Rịa Vũng Tàu </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bình Thuận " value="80" id="80"> Tỉnh Bình Thuận </option>';
                                                                        html += '<option style="display:none;" label=" Tỉnh Đồng Nai " value="81" id="81"> Tỉnh Đồng Nai </option>';
                                                                        html += '<option style="display:none;" label=" Tỉnh Bình Dương " value="82" id="82"> Tỉnh Bình Dương </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bình Phước " value="83" id="83"> Tỉnh Bình Phước </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Tây Ninh " value="84" id="84"> Tỉnh Tây Ninh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Long An " value="85" id="85"> Tỉnh Long An </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Tiền Giang " value="86" id="86"> Tỉnh Tiền Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Đồng Tháp " value="87" id="87"> Tỉnh Đồng Tháp </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh An Giang " value="88" id="88"> Tỉnh An Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Vĩnh Long " value="89" id="89"> Tỉnh Vĩnh Long </option>';
                                                                        html += '<option style="display:none" label=" TP Cần Thơ " value="90" id="90"> TP Cần Thơ </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hậu Giang " value="91" id="91"> Tỉnh Hậu Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Kiên Giang " value="92" id="92"> Tỉnh Kiên Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bến Tre " value="93" id="93"> Tỉnh Bến Tre </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Trà Vinh " value="94" id="94"> Tỉnh Trà Vinh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Sóc Trăng " value="95" id="95"> Tỉnh Sóc Trăng </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bạc Liêu " value="96" id="96"> Tỉnh Bạc Liêu </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Cà Mau " value="97" id="97"> Tỉnh Cà Mau </option>';
                                                                        html += '<option style="display:none" label=" Vnpost " value="98" id="98"> Vnpost </option>';
                                                                        html += '<option style="display:none" label=" PPTT " value="99" id="99"> PPTT </option>';
                                                                        html += '<option style="display:none" label=" DVBC " value="100" id="100"> DVBC </option>';
                                                                        html += '<option style="display:none" label=" TCBC " value="101" id="101"> TCBC </option>';
                                                                        html += '<option style="display:none" label=" HCC " value="102" id="102"> HCC </option>';
																		html += '<option style="display:none" label=" Global " value="103" id="103"> Global </option>';
                                                                    html += '</select>';
                                                                    html += '</div>';
                                                                    html += '<div id="groupUser" style="width:250px;" class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left" tabindex="0">';
                                                                    html += '<input style="width:250px;" placeholder="Search.." id="myInput" onkeyup="filterKeyy()">';
                                                                    html += ' <select style="width:250px;height:100px;display:none" name="group-users[]" id="group-users" multiple="multiple">';
																	html += '</select>';
																	html += '</div>';
																	html += '<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left">';
																	html += '<form id="groupName" method="POST" action="">';
																	html += '<input type=text" style="display:none" id="grouppname" name="groupName" value="">';
																	
																	html += '<span style="margin-left:20px"><input type="button" id="loadgroup" onclick="loadGroup3()" value="Lọc"></span>';
																	html += '<span style="margin-left:20px"><input type="button" class="deleteRes" value="Xóa đk lọc"></span>';
																	//if(y != null && y != undefined){
																	//html += '<span><a id="clear" href="index.php?module='+y+'&action=index&return_module=Leads&return_action=DetailView&clear=1"><input type="button" value="Xóa ĐK Lọc"></a></span>';
																	//}else{
																	//html += '<span><a id="clear" onclick="SUGAR.savedViews.setChooser()"><input type="button" value="Xóa ĐK Lọc"></a></span>';
																	//}
																	html += '</form>';
																	html += '</div>';
																	html += '<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left">';
																	html += '</div>';																	
                                                                  html += '</div>';

					var data1 = html3 + html + data;
					
					$("#pagecontent").html(data1);
					$(document).ready(function(){
						var nextButton = document.getElementById("listViewNextButton_top");
						var prevButton = document.getElementById("listViewPrevButton_top");
						var endButton = document.getElementById("listViewEndButton_top");
						var startButton = document.getElementById("listViewStartButton_top");
						if((nextButton != null && nextButton != undefined) && nextButton.getAttribute("disabled") != "disabled"){
							var changeClick = nextButton.getAttribute("onclick").replace("sListView.save_checks", "save_checks2");
							nextButton.setAttribute("onclick",changeClick);
						}
						if((prevButton != null && prevButton != undefined) && prevButton.getAttribute("disabled") != "disabled"){
							var changeClick2 = prevButton.getAttribute("onclick").replace("sListView.save_checks", "save_checks2");
							prevButton.setAttribute("onclick",changeClick2);
						}
						if((endButton != null && endButton != undefined) && endButton.getAttribute("disabled") != "disabled"){
							var changeClick3 = endButton.getAttribute("onclick").replace("sListView.save_checks", "save_checks2");
							endButton.setAttribute("onclick",changeClick3);
						}
						if((startButton != null && startButton != undefined) && startButton.getAttribute("disabled") != "disabled"){
							var changeClick4 = startButton.getAttribute("onclick").replace("sListView.save_checks", "save_checks2");
							startButton.setAttribute("onclick",changeClick4);
						}
		var modulee = GetURLParameter("module");
		if(modulee != null && modulee != undefined){
		setCookie("modulee",modulee);
		}
		
	showProvince();
	
	//var action = GetURLParameter("action");
	//if((action != null && action != undefined) && (modulee != null && modulee != undefined) && (parentTab == null || parentTab == undefined) && (provincee == null || provincee == undefined)){
	document.getElementById("provincecode").value = "";
	var groupSaved = getCookie("group");
	if(groupSaved != null && groupSaved != undefined && groupSaved != ''){
			groupSaved = JSON.parse(groupSaved);
				setTimeout(function(){
					var All;
					var checkbxName=[];
					var checkboxName =  document.getElementsByName("checkbxName");
					var length = checkboxName.length; 
					var length2 = groupSaved.length; 
					for(let i=0; i < length; i++)
					{
						for(let j = 0 ; j < length2; j++){	
								if(groupSaved[j] == checkboxName[i].value){
									checkboxName[i].checked = true;
								}
							
						}
					} 
					
					document.getElementById("grouppname").value = checkbxName;
					document.getElementById("myInput").value = groupSaved;
						//setCookie("group",JSON.stringify(checkbxName));
				},1500);
	}
				$.ajax({
							type: "POST",
							url: "index.php?entryPoint=getGroup",
							data: {
								id:groupId2
							},
							dataType: "json",
							success: function(data) {
								if(data != null && data != '' && data != undefined){
									var obj=data;
									html="";
									html+='<input style="width:250px;" placeholder="Search.." id="myInput" onkeyup="filterKeyy()">';
									html+='<div tabindex="1" style="background-color:white;width:250px;height:100px;overflow:auto;display:none;position:absolute;z-index:10000" name="group-users[]" id="group-users" >';
									html+='<div onclick="getUser();getTitle()"><input type="checkbox" class="checkbx" name="checkbxName" value="All">All</div>';
									for(let i=0;i<obj.length;i++){
										html+='<div><input type="checkbox" class="checkbx" name="checkbxName" value="'+obj[i].name+'">'+obj[i].name+'</div>';
									}
									html+='</div>';
									$('#groupUser').html(html);
									$('#users').html('');
									$('#myInput').on("focus",function(){
										if(document.getElementById('group-users').style.display == "none"){
										$('#group-users').css("display","block");
										}
										// document.getElementById('group-users').focus();
									});
									//$('.checkbx').on("click",function(){
										 //clicked = true;
									//	 $("#group-users").trigger("focus");
									//});
									//document.getElementById('groupUser').onmouseenter = function(){
									//	 clicked = true;
									//	document.getElementById('group-users').focus();
									//};
									document.getElementById('groupUser').onmouseover = function(){
										 clicked = true;
										document.getElementById('group-users').focus();
									};
									
									document.getElementById('groupUser').onmouseleave = function(){
										 clicked = false;
									};
									$('#myInput').on("blur",function(){
										if(clicked != true){
										$('#group-users').css("display","none");
										}
									});
									$(".checkbx").on("click",function(){
										clicked = true;
										document.getElementById('group-users').focus();
										var All;
										var checkbxName=[];
										var checkboxName =  document.getElementsByName("checkbxName");
										var length = checkboxName.length; 
										for(let i=0; i < length; i++)
										{	
											if(checkboxName[i].checked == true){
												if(checkboxName[i].value == "All"){
													All = true;
													break;
												}else{
														checkbxName.push(checkboxName[i].value);
												}
											}
										} 
										if(All == true){
											checkbxName=[];
											for(let j=1; j < length; j++){
												checkbxName.push(checkboxName[j].value);
											}
										}
										document.getElementById("grouppname").value = checkbxName;
										document.getElementById("myInput").value = checkbxName;
										setCookie("group",JSON.stringify(checkbxName));
									});
									$('#group-users').on("blur",function(){
										if(clicked != true){
										$('#group-users').css("display","none");
										}else{
										document.getElementById('group-users').focus();	
										}
									});
								}
								setTimeout(function(){
									SUGAR.ajaxUI.hideLoadingPanel();
								},700);
							},
							error: function (error) {
								
							}
						});
				
				
		});
                },
                error: function (error) {
                    
                }
            });
				});
				
				
				
		});
					}
					});
return true;
}
else 
	return false;
}	
function loadGroup(){
	var groupId=document.getElementById("provincecode_c").value;
	$.ajax({
                type: "POST",
                url: "index.php?entryPoint=getGroup",
                data: {
                    id:groupId
                },
                dataType: "json",
                success: function(data) {
					var obj=data;
					html="";
					html+='<div>Quận/Huyện</div>';
					html+='<input style="width:250px;" placeholder="Search.." id="myInput" onkeyup="filterKey()">';
					html+='<select style="width:250px;height:100px" name="group-users[]" id="group-users" multiple="multiple" >';
					html+='<option onclick="getUser();getTitle()" value="All">All</option>';
					for(let i=0;i<obj.length;i++){
						html+='<option onclick="getUser();getTitle()" value="'+obj[i].id+'">'+obj[i].name+'</option>';
					}
                    html+='</select>';
					$('#groupUser').html(html);
					$('#users').html('');
					
                },
                error: function (error) {
                    
                }
            });
}
function loadGroup2(){
	if($("#provincecode").val() != ""){
	SUGAR.ajaxUI.showLoadingPanel();
	var clicked;
	var title = document.getElementsByClassName("module-title-text")[0].innerHTML;
	title2 = title;
	var groupId=document.getElementById("provincecode").value;
	groupId2 = groupId;
	var mod2 = GetURLParameter("module");
	if(mod2 == null || mod2 == undefined){
		mod2 = modul;
	}
	mod3 = mod2;
	
	//var href = "index.php?module="+mod2+"&action=index&return_module="+mod2+"&return_action=DetailView&province="+groupId;
	//document.getElementById("groupName").action = href;
	//console.log("index.php?module="+mod2+"&action=index&return_module="+mod2+"&return_action=DetailView&province="+groupId);
	$.ajax({
                type: "POST",
                url: "index.php?module="+mod2+"&action=index&return_module="+mod2+"&return_action=DetailView&province="+groupId+"&sugar_body_only=true",
                dataType: "html",
				data: {
                    groupName:document.getElementById("grouppname").value
                },
                success: function(data) {
					if(data != null && data != undefined && data != '' ){
					setCookie("province",groupId);
					var html3 = '';
					html3 += '<div class="moduleTitle">';
					html3 += '<h2 class="module-title-text">'+title+'</h2>';
					html3 += '<span class="utils">';
					html3 += '<a href="#" class="btn btn-success showsearch">';
					html3 += '<span class=" glyphicon glyphicon-search" aria-hidden="true">';
					html3 += '</span>';
					html3 += '</a>&nbsp;';
					html3 += '<a id="create_image" href="index.php?module='+mod2+'&amp;action=EditView&amp;return_module='+mod2+'&amp;return_action=DetailView" class="utilsLink">';
					html3 += '<img src="themes/default/images/create-record.gif?v=golOgr3LHb5uVI36fQV1hQ" alt="Tạo">';
					html3 += '</a>';
					html3 += '<a id="create_link" href="index.php?module='+mod2+'&amp;action=EditView&amp;return_module='+mod2+'&amp;return_action=DetailView" class="utilsLink">';
					html3 += 'Tạo';
					html3 += '</a>';
					html3 += '</span>';
					html3 += '<div class="clear">';
					html3 += '</div>';
					html3 += '</div>';
					
					var html = '';
html += '<div class="row">';
html += '<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left" >';
html += '<select style="width:200px" name="group-users[]" id="provincecode" onchange="loadGroup2()">';
                                                                       html += '<option class="deleteRes" value="" selected="selected">----</option>';                          
                                                                        
                                                                        html += '<option style="display:none;" label=" TP Hà Nội" value="10" id="10"> TP Hà Nội</option>';
                                                                       html += ' <option style="display:none" label=" Tỉnh Hưng Yên" value="16" id="16"> Tỉnh Hưng Yên</option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hải Dương" value="17" id="17"> Tỉnh Hải Dương</option>';
                                                                        html += '<option style="display:none" label=" TP Hải Phòng " value="18" id="18"> TP Hải Phòng </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Ninh " value="20" id="20"> Tỉnh Quảng Ninh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bắc Ninh " value="22" id="22"> Tỉnh Bắc Ninh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bắc Giang " value="23" id="23"> Tỉnh Bắc Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Lạng Sơn " value="24" id="24"> Tỉnh Lạng Sơn </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Thái Nguyên " value="25" id="25"> Tỉnh Thái Nguyên </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bắc Kạn " value="26" id="26"> Tỉnh Bắc Kạn </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Cao Bằng " value="27" id="27"> Tỉnh Cao Bằng </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Vĩnh Phúc " value="28" id="28"> Tỉnh Vĩnh Phúc </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Phú Thọ " value="29" id="29"> Tỉnh Phú Thọ </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Tuyên Quang " value="30" id="30"> Tỉnh Tuyên Quang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hà Giang " value="31" id="31"> Tỉnh Hà Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Yên Bái " value="32" id="32"> Tỉnh Yên Bái </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Lào Cai " value="33" id="33"> Tỉnh Lào Cai </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hoà Bình " value="35" id="35"> Tỉnh Hoà Bình </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Sơn La " value="36" id="36"> Tỉnh Sơn La </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Điện Biên " value="38" id="38"> Tỉnh Điện Biên </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Lai Châu " value="39" id="39"> Tỉnh Lai Châu </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hà Nam " value="40" id="40"> Tỉnh Hà Nam </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Thái Bình " value="41" id="41"> Tỉnh Thái Bình </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Nam Định " value="42" id="42"> Tỉnh Nam Định </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Ninh Bình " value="43" id="43"> Tỉnh Ninh Bình </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Thanh Hoá " value="44" id="44"> Tỉnh Thanh Hoá </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Nghệ An " value="46" id="46"> Tỉnh Nghệ An </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hà Tĩnh " value="48" id="48"> Tỉnh Hà Tĩnh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Bình " value="51" id="51"> Tỉnh Quảng Bình </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Trị " value="52" id="52"> Tỉnh Quảng Trị </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Thừa Thiên Huế " value="53" id="53"> Tỉnh Thừa Thiên Huế </option>';
                                                                        html += '<option style="display:none" label=" TP Đà Nẵng " value="55" id="55"> TP Đà Nẵng </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Nam " value="56" id="56"> Tỉnh Quảng Nam </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Ngãi " value="57" id="57"> Tỉnh Quảng Ngãi </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Kon Tum " value="58" id="58"> Tỉnh Kon Tum </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bình Định " value="59" id="59"> Tỉnh Bình Định </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Gia Lai " value="60" id="60"> Tỉnh Gia Lai </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Phú Yên " value="62" id="62"> Tỉnh Phú Yên </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Đắk Lăk " value="63" id="63"> Tỉnh Đắk Lăk </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Đắk Nông " value="64" id="64"> Tỉnh Đắk Nông </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Khánh Hoà " value="65" id="65"> Tỉnh Khánh Hoà </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Ninh Thuận " value="66" id="66"> Tỉnh Ninh Thuận </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Lâm Đồng " value="67" id="67"> Tỉnh Lâm Đồng </option>';
                                                                        html += '<option style="display:none" label=" TP Hồ Chí Minh " value="70" id="70"> TP Hồ Chí Minh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bà Rịa Vũng Tàu " value="79" id="79"> Tỉnh Bà Rịa Vũng Tàu </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bình Thuận " value="80" id="80"> Tỉnh Bình Thuận </option>';
                                                                        html += '<option style="display:none;" label=" Tỉnh Đồng Nai " value="81" id="81"> Tỉnh Đồng Nai </option>';
                                                                        html += '<option style="display:none;" label=" Tỉnh Bình Dương " value="82" id="82"> Tỉnh Bình Dương </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bình Phước " value="83" id="83"> Tỉnh Bình Phước </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Tây Ninh " value="84" id="84"> Tỉnh Tây Ninh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Long An " value="85" id="85"> Tỉnh Long An </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Tiền Giang " value="86" id="86"> Tỉnh Tiền Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Đồng Tháp " value="87" id="87"> Tỉnh Đồng Tháp </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh An Giang " value="88" id="88"> Tỉnh An Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Vĩnh Long " value="89" id="89"> Tỉnh Vĩnh Long </option>';
                                                                        html += '<option style="display:none" label=" TP Cần Thơ " value="90" id="90"> TP Cần Thơ </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hậu Giang " value="91" id="91"> Tỉnh Hậu Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Kiên Giang " value="92" id="92"> Tỉnh Kiên Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bến Tre " value="93" id="93"> Tỉnh Bến Tre </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Trà Vinh " value="94" id="94"> Tỉnh Trà Vinh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Sóc Trăng " value="95" id="95"> Tỉnh Sóc Trăng </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bạc Liêu " value="96" id="96"> Tỉnh Bạc Liêu </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Cà Mau " value="97" id="97"> Tỉnh Cà Mau </option>';
                                                                        html += '<option style="display:none" label=" Vnpost " value="98" id="98"> Vnpost </option>';
                                                                        html += '<option style="display:none" label=" PPTT " value="99" id="99"> PPTT </option>';
                                                                        html += '<option style="display:none" label=" DVBC " value="100" id="100"> DVBC </option>';
                                                                        html += '<option style="display:none" label=" TCBC " value="101" id="101"> TCBC </option>';
                                                                        html += '<option style="display:none" label=" HCC " value="102" id="102"> HCC </option>';
																		html += '<option style="display:none" label=" Global " value="103" id="103"> Global </option>';
        
                                                                    html += '</select>';
                                                                    html += '</div>';
                                                                    html += '<div id="groupUser" style="width:250px;" class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left" tabindex="0">';
                                                                    html += '<input style="width:250px;" placeholder="Search.." id="myInput" onkeyup="filterKeyy()">';
                                                                    html += ' <select style="width:250px;height:100px;display:none" name="group-users[]" id="group-users" multiple="multiple">';
																	html += '</select>';
																	html += '</div>';
																	html += '<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left">';
																	html += '<form id="groupName" method="POST" action="">';
																	html += '<input type=text" style="display:none" id="grouppname" name="groupName" value="">';
																	
																	html += '<span style="margin-left:20px"><input type="button" id="loadgroup" onclick="loadGroup3()" value="Lọc"></span>';
																	html += '<span style="margin-left:20px"><input class="deleteRes"  type="button" value="Xóa đk lọc"></span>';
																	//if(y != null && y != undefined){
																	//html += '<span><a id="clear" href="index.php?module='+y+'&action=index&return_module=Leads&return_action=DetailView&clear=1"><input type="button" value="Xóa ĐK Lọc"></a></span>';
																	//}else{
																	//html += '<span><a id="clear" onclick="SUGAR.savedViews.setChooser()"><input type="button" value="Xóa ĐK Lọc"></a></span>';
																	//}
																	html += '</form>';
																	html += '</div>';
																	html += '<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left">';
																	html += '</div>';																	
                                                                  html += '</div>';
					var data1 = html3 + html + data;
					$("#pagecontent").html(data1);
					$(document).ready(function(){
						var nextButton = document.getElementById("listViewNextButton_top");
						var prevButton = document.getElementById("listViewPrevButton_top");
						var endButton = document.getElementById("listViewEndButton_top");
						var startButton = document.getElementById("listViewStartButton_top");
						if((nextButton != null && nextButton != undefined) && (nextButton != null && nextButton != undefined) && nextButton.getAttribute("disabled") != "disabled"){
							var changeClick = nextButton.getAttribute("onclick").replace("sListView.save_checks", "save_checks2");
							nextButton.setAttribute("onclick",changeClick);
						}
						if((prevButton != null && prevButton != undefined) && prevButton.getAttribute("disabled") != "disabled"){
							var changeClick2 = prevButton.getAttribute("onclick").replace("sListView.save_checks", "save_checks2");
							prevButton.setAttribute("onclick",changeClick2);
						}
						if((endButton != null && endButton != undefined) && endButton.getAttribute("disabled") != "disabled"){
							var changeClick3 = endButton.getAttribute("onclick").replace("sListView.save_checks", "save_checks2");
							endButton.setAttribute("onclick",changeClick3);
						}
						if((startButton != null && startButton != undefined) && startButton.getAttribute("disabled") != "disabled"){
							var changeClick4 = startButton.getAttribute("onclick").replace("sListView.save_checks", "save_checks2");
							startButton.setAttribute("onclick",changeClick4);
						}
		var modulee = GetURLParameter("module");
		if(modulee != null && modulee != undefined){
		setCookie("modulee",modulee);
		}
		/*
		var div = document.createElement("div");
		div.setAttribute("id", "province");
		

    var pagecontent = document.getElementById("pagecontent");
    var listViewBody = document.getElementsByClassName("listViewBody")[0];   
    pagecontent.insertBefore(div,listViewBody);
    document.getElementById("province").innerHTML = html;
	*/
	showProvince();
	
	//var action = GetURLParameter("action");
	//if((action != null && action != undefined) && (modulee != null && modulee != undefined) && (parentTab == null || parentTab == undefined) && (provincee == null || provincee == undefined)){
	document.getElementById("provincecode").value = groupId;
	var groupSaved = getCookie("group");
	if(groupSaved != null && groupSaved != undefined && groupSaved != ''){
			groupSaved = JSON.parse(groupSaved);
				setTimeout(function(){
					var All;
					var checkbxName=[];
					var checkboxName =  document.getElementsByName("checkbxName");
					var length = checkboxName.length; 
					var length2 = groupSaved.length; 
					for(let i=0; i < length; i++)
					{
						for(let j = 0 ; j < length2; j++){	
								if(groupSaved[j] == checkboxName[i].value){
									checkboxName[i].checked = true;
								}
							
						}
					} 
					
					document.getElementById("grouppname").value = checkbxName;
					document.getElementById("myInput").value = groupSaved;
						//setCookie("group",JSON.stringify(checkbxName));
				},1500);
	}
				$.ajax({
							type: "POST",
							url: "index.php?entryPoint=getGroup",
							data: {
								id:groupId
							},
							dataType: "json",
							success: function(data) {
								if(data != null && data != '' && data != undefined){
									var obj=data;
									html="";
									html+='<input style="width:250px;" placeholder="Search.." id="myInput" onkeyup="filterKeyy()">';
									html+='<div tabindex="1" style="background-color:white;width:250px;height:100px;overflow:auto;display:none;position:absolute;z-index:10000" name="group-users[]" id="group-users" >';
									html+='<div onclick="getUser();getTitle()"><input type="checkbox" class="checkbx" name="checkbxName" value="All">All</div>';
									for(let i=0;i<obj.length;i++){
										html+='<div><input type="checkbox" class="checkbx" name="checkbxName" value="'+obj[i].name+'">'+obj[i].name+'</div>';
									}
									html+='</div>';
									$('#groupUser').html(html);
									$('#users').html('');
									$('#myInput').on("focus",function(){
										if(document.getElementById('group-users').style.display == "none"){
										$('#group-users').css("display","block");
										}
										// document.getElementById('group-users').focus();
									});
									//$('.checkbx').on("click",function(){
										 //clicked = true;
									//	 $("#group-users").trigger("focus");
									//});
									//document.getElementById('groupUser').onmouseenter = function(){
									//	 clicked = true;
									//	document.getElementById('group-users').focus();
									//};
									document.getElementById('groupUser').onmouseover = function(){
										 clicked = true;
										document.getElementById('group-users').focus();
									};
									
									document.getElementById('groupUser').onmouseleave = function(){
										 clicked = false;
									};
									$('#myInput').on("blur",function(){
										if(clicked != true){
										$('#group-users').css("display","none");
										}
									});
									$(".checkbx").on("click",function(){
										clicked = true;
										document.getElementById('group-users').focus();
										var All;
										var checkbxName=[];
										var checkboxName =  document.getElementsByName("checkbxName");
										var length = checkboxName.length; 
										for(let i=0; i < length; i++)
										{	
											if(checkboxName[i].checked == true){
												if(checkboxName[i].value == "All"){
													All = true;
													break;
												}else{
														checkbxName.push(checkboxName[i].value);
												}
											}
										} 
										if(All == true){
											checkbxName=[];
											for(let j=1; j < length; j++){
												checkbxName.push(checkboxName[j].value);
											}
										}
										document.getElementById("grouppname").value = checkbxName;
										document.getElementById("myInput").value = checkbxName;
										setCookie("group",JSON.stringify(checkbxName));
									});
									$('#group-users').on("blur",function(){
										if(clicked != true){
										$('#group-users').css("display","none");
										}else{
										document.getElementById('group-users').focus();	
										}
									});
								}
								setTimeout(function(){
									SUGAR.ajaxUI.hideLoadingPanel();
								},700);
							},
							error: function (error) {
								
							}
						});
				$(".deleteRes").on("click",function(){
					SUGAR.ajaxUI.showLoadingPanel();
					$.ajax({
                type: "POST",
                url: "index.php?module="+mod2+"&action=index&searchFormTab=basic_search&query=true&orderBy=DATE_ENTERED&sortOrder=DESC&current_user_only_basic=0&favorites_only_basic=0&button=Tìm kiếm&resetCondition=1&sugar_body_only=true",
                dataType: "html",
                success: function(data) {
					setCookie("group","");
					setCookie("province","");
					var html3 = '';
					html3 += '<div class="moduleTitle">';
					html3 += '<h2 class="module-title-text">'+title+'</h2>';
					html3 += '<span class="utils">';
					html3 += '<a href="#" class="btn btn-success showsearch">';
					html3 += '<span class=" glyphicon glyphicon-search" aria-hidden="true">';
					html3 += '</span>';
					html3 += '</a>&nbsp;';
					html3 += '<a id="create_image" href="index.php?module='+mod2+'&amp;action=EditView&amp;return_module='+mod2+'&amp;return_action=DetailView" class="utilsLink">';
					html3 += '<img src="themes/default/images/create-record.gif?v=golOgr3LHb5uVI36fQV1hQ" alt="Tạo">';
					html3 += '</a>';
					html3 += '<a id="create_link" href="index.php?module='+mod2+'&amp;action=EditView&amp;return_module='+mod2+'&amp;return_action=DetailView" class="utilsLink">';
					html3 += 'Tạo';
					html3 += '</a>';
					html3 += '</span>';
					html3 += '<div class="clear">';
					html3 += '</div>';
					html3 += '</div>';
					
					var html = '';
html += '<div class="row">';
html += '<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left" >';
html += '<select style="width:200px" name="group-users[]" id="provincecode" onchange="loadGroup2()">';
                                                                       html += '<option class="deleteRes" value="" selected="selected">----</option>';                          
                                                                      
                                                                        html += '<option style="display:none;" label=" TP Hà Nội" value="10" id="10"> TP Hà Nội</option>';
                                                                       html += ' <option style="display:none" label=" Tỉnh Hưng Yên" value="16" id="16"> Tỉnh Hưng Yên</option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hải Dương" value="17" id="17"> Tỉnh Hải Dương</option>';
                                                                        html += '<option style="display:none" label=" TP Hải Phòng " value="18" id="18"> TP Hải Phòng </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Ninh " value="20" id="20"> Tỉnh Quảng Ninh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bắc Ninh " value="22" id="22"> Tỉnh Bắc Ninh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bắc Giang " value="23" id="23"> Tỉnh Bắc Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Lạng Sơn " value="24" id="24"> Tỉnh Lạng Sơn </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Thái Nguyên " value="25" id="25"> Tỉnh Thái Nguyên </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bắc Kạn " value="26" id="26"> Tỉnh Bắc Kạn </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Cao Bằng " value="27" id="27"> Tỉnh Cao Bằng </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Vĩnh Phúc " value="28" id="28"> Tỉnh Vĩnh Phúc </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Phú Thọ " value="29" id="29"> Tỉnh Phú Thọ </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Tuyên Quang " value="30" id="30"> Tỉnh Tuyên Quang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hà Giang " value="31" id="31"> Tỉnh Hà Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Yên Bái " value="32" id="32"> Tỉnh Yên Bái </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Lào Cai " value="33" id="33"> Tỉnh Lào Cai </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hoà Bình " value="35" id="35"> Tỉnh Hoà Bình </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Sơn La " value="36" id="36"> Tỉnh Sơn La </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Điện Biên " value="38" id="38"> Tỉnh Điện Biên </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Lai Châu " value="39" id="39"> Tỉnh Lai Châu </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hà Nam " value="40" id="40"> Tỉnh Hà Nam </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Thái Bình " value="41" id="41"> Tỉnh Thái Bình </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Nam Định " value="42" id="42"> Tỉnh Nam Định </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Ninh Bình " value="43" id="43"> Tỉnh Ninh Bình </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Thanh Hoá " value="44" id="44"> Tỉnh Thanh Hoá </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Nghệ An " value="46" id="46"> Tỉnh Nghệ An </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hà Tĩnh " value="48" id="48"> Tỉnh Hà Tĩnh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Bình " value="51" id="51"> Tỉnh Quảng Bình </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Trị " value="52" id="52"> Tỉnh Quảng Trị </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Thừa Thiên Huế " value="53" id="53"> Tỉnh Thừa Thiên Huế </option>';
                                                                        html += '<option style="display:none" label=" TP Đà Nẵng " value="55" id="55"> TP Đà Nẵng </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Nam " value="56" id="56"> Tỉnh Quảng Nam </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Ngãi " value="57" id="57"> Tỉnh Quảng Ngãi </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Kon Tum " value="58" id="58"> Tỉnh Kon Tum </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bình Định " value="59" id="59"> Tỉnh Bình Định </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Gia Lai " value="60" id="60"> Tỉnh Gia Lai </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Phú Yên " value="62" id="62"> Tỉnh Phú Yên </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Đắk Lăk " value="63" id="63"> Tỉnh Đắk Lăk </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Đắk Nông " value="64" id="64"> Tỉnh Đắk Nông </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Khánh Hoà " value="65" id="65"> Tỉnh Khánh Hoà </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Ninh Thuận " value="66" id="66"> Tỉnh Ninh Thuận </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Lâm Đồng " value="67" id="67"> Tỉnh Lâm Đồng </option>';
                                                                        html += '<option style="display:none" label=" TP Hồ Chí Minh " value="70" id="70"> TP Hồ Chí Minh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bà Rịa Vũng Tàu " value="79" id="79"> Tỉnh Bà Rịa Vũng Tàu </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bình Thuận " value="80" id="80"> Tỉnh Bình Thuận </option>';
                                                                        html += '<option style="display:none;" label=" Tỉnh Đồng Nai " value="81" id="81"> Tỉnh Đồng Nai </option>';
                                                                        html += '<option style="display:none;" label=" Tỉnh Bình Dương " value="82" id="82"> Tỉnh Bình Dương </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bình Phước " value="83" id="83"> Tỉnh Bình Phước </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Tây Ninh " value="84" id="84"> Tỉnh Tây Ninh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Long An " value="85" id="85"> Tỉnh Long An </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Tiền Giang " value="86" id="86"> Tỉnh Tiền Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Đồng Tháp " value="87" id="87"> Tỉnh Đồng Tháp </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh An Giang " value="88" id="88"> Tỉnh An Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Vĩnh Long " value="89" id="89"> Tỉnh Vĩnh Long </option>';
                                                                        html += '<option style="display:none" label=" TP Cần Thơ " value="90" id="90"> TP Cần Thơ </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hậu Giang " value="91" id="91"> Tỉnh Hậu Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Kiên Giang " value="92" id="92"> Tỉnh Kiên Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bến Tre " value="93" id="93"> Tỉnh Bến Tre </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Trà Vinh " value="94" id="94"> Tỉnh Trà Vinh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Sóc Trăng " value="95" id="95"> Tỉnh Sóc Trăng </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bạc Liêu " value="96" id="96"> Tỉnh Bạc Liêu </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Cà Mau " value="97" id="97"> Tỉnh Cà Mau </option>';
                                                                        html += '<option style="display:none" label=" Vnpost " value="98" id="98"> Vnpost </option>';
                                                                        html += '<option style="display:none" label=" PPTT " value="99" id="99"> PPTT </option>';
                                                                        html += '<option style="display:none" label=" DVBC " value="100" id="100"> DVBC </option>';
                                                                        html += '<option style="display:none" label=" TCBC " value="101" id="101"> TCBC </option>';
                                                                        html += '<option style="display:none" label=" HCC " value="102" id="102"> HCC </option>';
																		html += '<option style="display:none" label=" Global " value="103" id="103"> Global </option>';
                                                                    html += '</select>';
                                                                    html += '</div>';
                                                                    html += '<div id="groupUser" style="width:250px;" class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left" tabindex="0">';
                                                                    html += '<input style="width:250px;" placeholder="Search.." id="myInput" onkeyup="filterKeyy()">';
                                                                    html += ' <select style="width:250px;height:100px;display:none" name="group-users[]" id="group-users" multiple="multiple">';
																	html += '</select>';
																	html += '</div>';
																	html += '<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left">';
																	html += '<form id="groupName" method="POST" action="">';
																	html += '<input type=text" style="display:none" id="grouppname" name="groupName" value="">';
																	
																	html += '<span style="margin-left:20px"><input type="button" id="loadgroup" onclick="loadGroup3()" value="Lọc"></span>';
																	html += '<span style="margin-left:20px"><input type="button" class="deleteRes" value="Xóa đk lọc"></span>';
																	//if(y != null && y != undefined){
																	//html += '<span><a id="clear" href="index.php?module='+y+'&action=index&return_module=Leads&return_action=DetailView&clear=1"><input type="button" value="Xóa ĐK Lọc"></a></span>';
																	//}else{
																	//html += '<span><a id="clear" onclick="SUGAR.savedViews.setChooser()"><input type="button" value="Xóa ĐK Lọc"></a></span>';
																	//}
																	html += '</form>';
																	html += '</div>';
																	html += '<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left">';
																	html += '</div>';																	
                                                                  html += '</div>';
					var data1 = html3 + html + data;
					
					$("#pagecontent").html(data1);
					$(document).ready(function(){
						var nextButton = document.getElementById("listViewNextButton_top");
						var prevButton = document.getElementById("listViewPrevButton_top");
						var endButton = document.getElementById("listViewEndButton_top");
						var startButton = document.getElementById("listViewStartButton_top");
						if((nextButton != null && nextButton != undefined) && nextButton.getAttribute("disabled") != "disabled"){
							var changeClick = nextButton.getAttribute("onclick").replace("sListView.save_checks", "save_checks2");
							nextButton.setAttribute("onclick",changeClick);
						}
						if((prevButton != null && prevButton != undefined) && prevButton.getAttribute("disabled") != "disabled"){
							var changeClick2 = prevButton.getAttribute("onclick").replace("sListView.save_checks", "save_checks2");
							prevButton.setAttribute("onclick",changeClick2);
						}
						if((endButton != null && endButton != undefined) && endButton.getAttribute("disabled") != "disabled"){
							var changeClick3 = endButton.getAttribute("onclick").replace("sListView.save_checks", "save_checks2");
							endButton.setAttribute("onclick",changeClick3);
						}
						if((startButton != null && startButton != undefined) && startButton.getAttribute("disabled") != "disabled"){
							var changeClick4 = startButton.getAttribute("onclick").replace("sListView.save_checks", "save_checks2");
							startButton.setAttribute("onclick",changeClick4);
						}
		var modulee = GetURLParameter("module");
		if(modulee != null && modulee != undefined){
		setCookie("modulee",modulee);
		}
		
	showProvince();
	
	//var action = GetURLParameter("action");
	//if((action != null && action != undefined) && (modulee != null && modulee != undefined) && (parentTab == null || parentTab == undefined) && (provincee == null || provincee == undefined)){
	document.getElementById("provincecode").value = "";
	var groupSaved = getCookie("group");
	if(groupSaved != null && groupSaved != undefined && groupSaved != ''){
			groupSaved = JSON.parse(groupSaved);
				setTimeout(function(){
					var All;
					var checkbxName=[];
					var checkboxName =  document.getElementsByName("checkbxName");
					var length = checkboxName.length; 
					var length2 = groupSaved.length; 
					for(let i=0; i < length; i++)
					{
						for(let j = 0 ; j < length2; j++){	
								if(groupSaved[j] == checkboxName[i].value){
									checkboxName[i].checked = true;
								}
							
						}
					} 
					
					document.getElementById("grouppname").value = checkbxName;
					document.getElementById("myInput").value = groupSaved;
						//setCookie("group",JSON.stringify(checkbxName));
				},1500);
	}
				$.ajax({
							type: "POST",
							url: "index.php?entryPoint=getGroup",
							data: {
								id:groupId2
							},
							dataType: "json",
							success: function(data) {
								if(data != null && data != '' && data != undefined){
									var obj=data;
									html="";
									html+='<input style="width:250px;" placeholder="Search.." id="myInput" onkeyup="filterKeyy()">';
									html+='<div tabindex="1" style="background-color:white;width:250px;height:100px;overflow:auto;display:none;position:absolute;z-index:10000" name="group-users[]" id="group-users" >';
									html+='<div onclick="getUser();getTitle()"><input type="checkbox" class="checkbx" name="checkbxName" value="All">All</div>';
									for(let i=0;i<obj.length;i++){
										html+='<div><input type="checkbox" class="checkbx" name="checkbxName" value="'+obj[i].name+'">'+obj[i].name+'</div>';
									}
									html+='</div>';
									$('#groupUser').html(html);
									$('#users').html('');
									$('#myInput').on("focus",function(){
										if(document.getElementById('group-users').style.display == "none"){
										$('#group-users').css("display","block");
										}
										// document.getElementById('group-users').focus();
									});
									//$('.checkbx').on("click",function(){
										 //clicked = true;
									//	 $("#group-users").trigger("focus");
									//});
									//document.getElementById('groupUser').onmouseenter = function(){
									//	 clicked = true;
									//	document.getElementById('group-users').focus();
									//};
									document.getElementById('groupUser').onmouseover = function(){
										 clicked = true;
										document.getElementById('group-users').focus();
									};
									
									document.getElementById('groupUser').onmouseleave = function(){
										 clicked = false;
									};
									$('#myInput').on("blur",function(){
										if(clicked != true){
										$('#group-users').css("display","none");
										}
									});
									$(".checkbx").on("click",function(){
										clicked = true;
										document.getElementById('group-users').focus();
										var All;
										var checkbxName=[];
										var checkboxName =  document.getElementsByName("checkbxName");
										var length = checkboxName.length; 
										for(let i=0; i < length; i++)
										{	
											if(checkboxName[i].checked == true){
												if(checkboxName[i].value == "All"){
													All = true;
													break;
												}else{
														checkbxName.push(checkboxName[i].value);
												}
											}
										} 
										if(All == true){
											checkbxName=[];
											for(let j=1; j < length; j++){
												checkbxName.push(checkboxName[j].value);
											}
										}
										document.getElementById("grouppname").value = checkbxName;
										document.getElementById("myInput").value = checkbxName;
										setCookie("group",JSON.stringify(checkbxName));
									});
									$('#group-users').on("blur",function(){
										if(clicked != true){
										$('#group-users').css("display","none");
										}else{
										document.getElementById('group-users').focus();	
										}
									});
									$("#loadgroup").on("click",function(){
									loadgroup3();
									});
								}
								
								setTimeout(function(){
									SUGAR.ajaxUI.hideLoadingPanel();
								},700);
							},
							error: function (error) {
								
							}
						});
				
				
		});
                },
                error: function (error) {
                    
                }
            });
				});
		});
				
	}	
                },
                error: function (error) {
                    
                }
            });
	}
}	

function insertLineItems(product,group){

  var type = 'product_';
  var ln = 0;
  var current_group = 'lineItems';
  var gid = product.group_id;

  if(typeof group_ids[gid] === 'undefined'){
    current_group = insertGroup();
    group_ids[gid] = current_group;
    for(var g in group){
      if(document.getElementById('group'+current_group + g) !== null){
        document.getElementById('group'+current_group + g).value = group[g];
      }
    }
  } else {
    current_group = group_ids[gid];
  }

  if(product.product_id != '0' && product.product_id !== ''){
    ln = insertProductLine('product_group'+current_group,current_group);
    type = 'product_';
  } else {
    ln = insertServiceLine('service_group'+current_group,current_group);
    type = 'service_';
  }

  for(var p in product){
    if(document.getElementById(type + p + ln) !== null){
      if(product[p] !== '' && isNumeric(product[p]) && p != 'vat'  && p != 'product_id' && p != 'name' && p != "part_number"){
        document.getElementById(type + p + ln).value = format2Number(product[p]);
      } else {
        document.getElementById(type + p + ln).value = product[p];
      }
    }
  }
	
  calculateLine(ln,type);

}


/**
 * Insert product line
 */
function showMoreCampaign(){
	//alert("aaaA");
    var html=document.createElement('div');
    var more = document.getElementById("showMoreCampaign").innerHTML;
    var count = document.getElementById("showMoreCampaign").childElementCount;
    html.innerHTML='<div id="campaignForm'+eval(count+1)+'"><input type="text" name="campaign_name" onkeyup="filterKeyCampaign('+eval(count+1)+')" class="sqsEnabled yui-ac-input" tabindex="0" id="campaign_name'+eval(count+1)+'" size="" placeholder="thêm Campaign" title="" autocomplete="off" onclick = "showCampaign('+eval(count+1)+')"><span><button type="button" id="removeButton'+eval(count+1)+'" class="btn btn-danger email-address-remove-button" onclick = "removeUserCampaign('+eval(count+1)+')"><span class="suitepicon suitepicon-action-minus"></span></button></span></div><div id="userCampaign'+eval(count+1)+'"></div>';
    
   document.getElementById("showMoreCampaign").append(html);
}
function showCampaign(a){
	$.ajax({
                type: "POST",
                url: "index.php?entryPoint=showUserCampaign",
                dataType: "json",
                success: function(data) {
					var length=data.length;
					var html='<select multiple="multiple" id="userCampaignDrop'+a+'" style="width:180px;height:180px;z-index:1000;position:absolute">';
					for(var i=0;i<length;i++){
					html+='<option value="'+data[i].id+'">'+data[i].name+'</option>';
					}
					html+='</select>';
					document.getElementById("userCampaign"+a).style.display="block";
					document.getElementById("userCampaign"+a).innerHTML=html;
						$("#campaign_name"+a).on("blur",function(){
						setTimeout(function(){
						$("#userCampaign"+a).css('display','none');
						},150);
						});
						
						$("#userCampaignDrop"+a).on("change",function(){
							var vall=$("#userCampaignDrop"+a).find(":selected").text();
							$("#campaign_name"+a).val(vall);
						});
                },
                error: function (error) {
                    
                }
            });
}
function removeUserCampaign(a){
	document.getElementById("campaignForm"+a).remove();
	document.getElementById("userCampaign"+a).remove();
}
function insertProductLine(tableid, groupid) {

  if(!enable_groups){
    tableid = "product_group0";
  }

  if (document.getElementById(tableid + '_head') !== null) {
    document.getElementById(tableid + '_head').style.display = "";
  }

  var vat_hidden = document.getElementById("vathidden").value;
  var discount_hidden = document.getElementById("discounthidden").value;

  sqs_objects["product_name[" + prodln + "]"] = {
    "form": "EditView",
    "method": "query",
    "modules": ["AOS_Products"],
    "group": "or",
    "field_list": ["name", "id", "part_number", "cost", "price", "description", "currency_id"],
    "populate_list": ["product_name[" + prodln + "]", "product_product_id[" + prodln + "]", "product_part_number[" + prodln + "]", "product_product_cost_price[" + prodln + "]", "product_product_list_price[" + prodln + "]", "product_item_description[" + prodln + "]", "product_currency[" + prodln + "]"],
    "required_list": ["product_id[" + prodln + "]"],
    "conditions": [{
      "name": "name",
      "op": "like_custom",
      "end": "%",
      "value": ""
    }],
    "order": "name",
    "limit": "30",
    "post_onblur_function": "formatListPrice(" + prodln + ");",
    "no_match_text": "No Match"
  };
  sqs_objects["product_part_number[" + prodln + "]"] = {
    "form": "EditView",
    "method": "query",
    "modules": ["AOS_Products"],
    "group": "or",
    "field_list": ["part_number", "name", "id","cost", "price","description","currency_id"],
    "populate_list": ["product_part_number[" + prodln + "]", "product_name[" + prodln + "]", "product_product_id[" + prodln + "]",  "product_product_cost_price[" + prodln + "]", "product_product_list_price[" + prodln + "]", "product_item_description[" + prodln + "]", "product_currency[" + prodln + "]"],
    "required_list": ["product_id[" + prodln + "]"],
    "conditions": [{
      "name": "part_number",
      "op": "like_custom",
      "end": "%",
      "value": ""
    }],
    "order": "name",
    "limit": "30",
    "post_onblur_function": "formatListPrice(" + prodln + ");",
    "no_match_text": "No Match"
  };

  tablebody = document.createElement("tbody");
  tablebody.id = "product_body" + prodln;
  document.getElementById(tableid).appendChild(tablebody);


  var x = tablebody.insertRow(-1);
  x.id = 'product_line' + prodln;

  var a = x.insertCell(0);
  a.innerHTML = "<input type='text' name='product_product_qty[" + prodln + "]' id='product_product_qty" + prodln + "'  value='' title='' tabindex='116' onblur='Quantity_format2Number(" + prodln + ");calculateLine(" + prodln + ",\"product_\");' class='product_qty'>";

  var b = x.insertCell(1);
  b.innerHTML = "<input class='sqsEnabled product_name' autocomplete='off' type='text' name='product_name[" + prodln + "]' id='product_name" + prodln + "' maxlength='50' value='' title='' tabindex='116' value=''><input type='hidden' name='product_product_id[" + prodln + "]' id='product_product_id" + prodln + "'  maxlength='50' value=''>";

  var b1 = x.insertCell(2);
  b1.innerHTML = "<input class='sqsEnabled product_part_number' autocomplete='off' type='text' name='product_part_number[" + prodln + "]' id='product_part_number" + prodln + "' maxlength='50' value='' title='' tabindex='116' value=''>";

  var b2 = x.insertCell(3);
  b2.innerHTML = "<button title='" + SUGAR.language.get('app_strings', 'LBL_SELECT_BUTTON_TITLE') + "' accessKey='" + SUGAR.language.get('app_strings', 'LBL_SELECT_BUTTON_KEY') + "' type='button' tabindex='116' class='button product_part_number_button' value='" + SUGAR.language.get('app_strings', 'LBL_SELECT_BUTTON_LABEL') + "' name='btn1' onclick='openProductPopup(" + prodln + ");'><span class=\"suitepicon suitepicon-action-select\"></span></button>";

  var c = x.insertCell(4);
  c.innerHTML = "<input type='text' name='product_product_list_price[" + prodln + "]' id='product_product_list_price" + prodln + "' maxlength='50' value='' title='' tabindex='116' onblur='calculateLine(" + prodln + ",\"product_\");' class='product_list_price'><input type='hidden' name='product_product_cost_price[" + prodln + "]' id='product_product_cost_price" + prodln + "' value=''  />";

  if (typeof currencyFields !== 'undefined'){

    currencyFields.push("product_product_list_price" + prodln);
    currencyFields.push("product_product_cost_price" + prodln);

  }

  var d = x.insertCell(5);
  d.innerHTML = "<input type='text' name='product_product_discount[" + prodln + "]' id='product_product_discount" + prodln + "'  maxlength='50' value='' title='' tabindex='116' onblur='calculateLine(" + prodln + ",\"product_\");' onblur='calculateLine(" + prodln + ",\"product_\");' class='product_discount_text'><input type='hidden' name='product_product_discount_amount[" + prodln + "]' id='product_product_discount_amount" + prodln + "' value=''  />";
  d.innerHTML += "<select tabindex='116' name='product_discount[" + prodln + "]' id='product_discount" + prodln + "' onchange='calculateLine(" + prodln + ",\"product_\");' class='product_discount_amount_select'>" + discount_hidden + "</select>";

  var e = x.insertCell(6);
  e.innerHTML = "<input type='text' name='product_product_unit_price[" + prodln + "]' id='product_product_unit_price" + prodln + "' maxlength='50' value='' title='' tabindex='116' readonly='readonly' onblur='calculateLine(" + prodln + ",\"product_\");' onblur='calculateLine(" + prodln + ",\"product_\");' class='product_unit_price'>";

  if (typeof currencyFields !== 'undefined'){
    currencyFields.push("product_product_unit_price" + prodln);
  }

  var f = x.insertCell(7);
  f.innerHTML = "<input type='text' name='product_vat_amt[" + prodln + "]' id='product_vat_amt" + prodln + "' maxlength='250' value='' title='' tabindex='116' readonly='readonly' class='product_vat_amt_text'>";
  f.innerHTML += "<select tabindex='116' name='product_vat[" + prodln + "]' id='product_vat" + prodln + "' onchange='calculateLine(" + prodln + ",\"product_\");' class='product_vat_amt_select'>" + vat_hidden + "</select>";

  if (typeof currencyFields !== 'undefined'){
    currencyFields.push("product_vat_amt" + prodln);
  }
  var g = x.insertCell(8);
  g.innerHTML = "<input type='text' name='product_product_total_price[" + prodln + "]' id='product_product_total_price" + prodln + "' maxlength='50' value='' title='' tabindex='116' readonly='readonly' class='product_total_price'><input type='hidden' name='product_group_number[" + prodln + "]' id='product_group_number" + prodln + "' value='"+groupid+"'>";

  if (typeof currencyFields !== 'undefined'){
    currencyFields.push("product_product_total_price" + prodln);
  }
  var h = x.insertCell(9);
  h.innerHTML = "<input type='hidden' name='product_currency[" + prodln + "]' id='product_currency" + prodln + "' value=''><input type='hidden' name='product_deleted[" + prodln + "]' id='product_deleted" + prodln + "' value='0'><input type='hidden' name='product_id[" + prodln + "]' id='product_id" + prodln + "' value=''><button type='button' id='product_delete_line" + prodln + "' class='button product_delete_line' value='" + SUGAR.language.get(module_sugar_grp1, 'LBL_REMOVE_PRODUCT_LINE') + "' tabindex='116' onclick='markLineDeleted(" + prodln + ",\"product_\")'><span class=\"suitepicon suitepicon-action-clear\"></span></button><br>";


  enableQS(true);
  //QSFieldsArray["EditView_product_name"+prodln].forceSelection = true;

  var y = tablebody.insertRow(-1);
  y.id = 'product_note_line' + prodln;

  var h1 = y.insertCell(0);
  h1.colSpan = "5";
  h1.style.color = "rgb(68,68,68)";
  h1.innerHTML = "<span style='vertical-align: top;' class='product_item_description_label'>" + SUGAR.language.get(module_sugar_grp1, 'LBL_PRODUCT_DESCRIPTION') + " :&nbsp;&nbsp;</span>";
  h1.innerHTML += "<textarea tabindex='116' name='product_item_description[" + prodln + "]' id='product_item_description" + prodln + "' rows='2' cols='23' class='product_item_description'></textarea>&nbsp;&nbsp;";

  var i = y.insertCell(1);
  i.colSpan = "5";
  i.style.color = "rgb(68,68,68)";
  i.innerHTML = "<span style='vertical-align: top;' class='product_description_label'>"  + SUGAR.language.get(module_sugar_grp1, 'LBL_PRODUCT_NOTE') + " :&nbsp;</span>";
  i.innerHTML += "<textarea tabindex='116' name='product_description[" + prodln + "]' id='product_description" + prodln + "' rows='2' cols='23' class='product_description'></textarea>&nbsp;&nbsp;"

  

  addAlignedLabels(prodln, 'product');

  prodln++;

  return prodln - 1;
}

var addAlignedLabels = function(ln, type) {
  if(typeof type == 'undefined') {
    type = 'product';
  }
  if(type != 'product' && type != 'service') {
    console.error('type could be "product" or "service" only');
  }
  var labels = [];
  $('tr#'+type+'_head td').each(function(i,e){
    if(type=='product' && $(e).attr('colspan')>1) {
      for(var i=0; i<parseInt($(e).attr('colspan')); i++) {
        if(i==0) {
          labels.push($(e).html());
        } else {
          labels.push('');
        }
      }
    } else {
      labels.push($(e).html());
    }
  });
  $('tr#'+type+'_line'+ln+' td').each(function(i,e){
    $(e).prepend('<span class="alignedLabel">'+labels[i]+'</span>');
  });
}


/**
 * Open product popup
 */
function openProductPopup(ln){

  lineno=ln;
  var popupRequestData = {
    "call_back_function" : "setProductReturn",
    "form_name" : "EditView",
    "field_to_name_array" : {
      "id" : "product_product_id" + ln,
      "name" : "product_name" + ln,
      "description" : "product_item_description" + ln,
      "part_number" : "product_part_number" + ln,
      "cost" : "product_product_cost_price" + ln,
      "price" : "product_product_list_price" + ln,
      "currency_id" : "product_currency" + ln
    }
  };

  open_popup('AOS_Products', 800, 850, '', true, true, popupRequestData);

}
function loadProvince3(){
	$.ajax({
                type: "POST",
                url: "index.php?entryPoint=loadProvince",
                
                dataType: "json",
                success: function(data) {
					document.getElementById("provincecode").value = data ;
					loadGroup2();
					
                },
                error: function (error) {
                    
                }
            });
}
function setProductReturn(popupReplyData){
  set_return(popupReplyData);
  formatListPrice(lineno);
}
function changeCur(id){
	
	var textEle=document.getElementById(id);
		var str;
		var i;
		var st;
		var j;
		var text_val;
		var cusText;
		textEle.onkeyup=function(){
		cusText='';
		str='';
		text_val=textEle.value;
		for(var u=0;u<text_val.length;u++)
		 {
		 if(text_val[u]!=",")
		  cusText+=text_val[u];
		 
		 }
		
			
			i=eval(cusText.length-1);
			j=eval(cusText.length-4);
		while(i>-1){
		if(i==j && cusText.length > 3 ){
			j-=3;
		 str=cusText[i]+","+str;
		
		 }else{
		 str=cusText[i]+str;
		 }
		 i--;
		  }
		 
		 textEle.value=str;
		 }
	
	
}
function changeCurrency(number){
	var textEle=number;
	var str='';
	var i=eval(textEle.length-3);
	var st;
	var j;
	var text_val;
	var cusText;
		cusText='';
		str='';
	for(var u=eval(textEle.length-1);u>=0;u--)
	{
		str=textEle[u]+str;
		if(u == i && u > 0){
			str=","+str;	
			i=eval(i-3);
		}
	}
	return str;
		 
}
function calculateLine2(ln,key,numOfZero){
	if(key != "lineItems"){
		var required = 'product_list_price';
		if(document.getElementById(key + required + ln) === null){
			required = 'product_unit_price';
		}
		if (document.getElementById(key + 'name' + ln).value === '' || document.getElementById(key + required + ln).value === ''){
			return;
		}
		if(key === "product_" && document.getElementById(key + 'product_qty' + ln) !== null && document.getElementById(key + 'product_qty' + ln).value === ''){
			document.getElementById(key + 'product_qty' + ln).value =1;
		}
		var productUnitPrice = unformatNumber(document.getElementById(key + 'product_unit_price' + ln).value,',','.');

		if(document.getElementById(key + 'product_list_price' + ln) !== null && document.getElementById(key + 'product_discount' + ln) !== null && document.getElementById(key + 'discount' + ln) !== null){
			
			var listPrice = get_value(key + 'product_list_price' + ln,',','.');
			var discount = get_value(key + 'product_discount' + ln,',','.');
			var dis = document.getElementById(key + 'discount' + ln).value;
			if(dis == 'Amount')
			{
				if(discount > listPrice)
				{
					document.getElementById(key + 'product_discount' + ln).value = listPrice;
					discount = listPrice;
				}
				productUnitPrice = listPrice - discount;
				document.getElementById(key + 'product_unit_price' + ln).value = changeCurrencyWithDotForSum((listPrice - discount),numOfZero);
			}
			else if(dis == 'Percentage')
			{
				if(discount > 100)
				{
					document.getElementById(key + 'product_discount' + ln).value = 100;
					discount = 100;
				}
				discount = (discount/100) * listPrice;
				productUnitPrice = listPrice - discount;
				document.getElementById(key + 'product_unit_price' + ln).value = changeCurrencyWithDotForSum((listPrice - discount),numOfZero);
			}
			else
			{
				document.getElementById(key + 'product_unit_price' + ln).value = document.getElementById(key + 'product_list_price' + ln).value;
				document.getElementById(key + 'product_discount' + ln).value = '';
				discount = 0;
			}
			//document.getElementById(key + 'product_list_price' + ln).value = changeCurrencyWithDotForSum(listPrice,numOfZero);
			//document.getElementById(key + 'product_discount' + ln).value = changeCurrencyWithDotForSum(unformatNumber(document.getElementById(key + 'product_discount' + ln).value,',','.'));
			document.getElementById(key + 'product_discount_amount' + ln).value = changeCurrencyWithDotForSum(-discount,numOfZero);
		}

		var productQty = 1;
		if(document.getElementById(key + 'product_qty' + ln) !== null){
			productQty = unformat2Number(document.getElementById(key + 'product_qty' + ln).value);
			Quantity_format2Number(ln);
		}
		var vat = unformatNumber(document.getElementById(key + 'vat' + ln).value,',','.');
		var productTotalPrice = productQty * productUnitPrice;
		var totalvat=(productTotalPrice * vat) /100;
		if(total_tax){
			productTotalPrice=productTotalPrice + totalvat;
		}
		document.getElementById(key + 'vat_amt' + ln).value = changeCurrencyWithDotForSum(totalvat,numOfZero);
		document.getElementById(key + 'product_unit_price' + ln).value = changeCurrencyWithDotForSum(productUnitPrice,numOfZero);
		document.getElementById(key + 'product_total_price' + ln).value = changeCurrencyWithDotForSum(productTotalPrice,numOfZero);
		var groupid = 0;
		if(enable_groups){
			groupid = document.getElementById(key + 'group_number' + ln).value;
		}
		groupid = 'group' + groupid;
		calculateTotal2(numOfZero,groupid);
		calculateTotal2(numOfZero);
	}else{
		calculateTotal2(numOfZero,key);
	}
}
function calculateLine3(ln,key,numOfZero){
	var required = 'product_list_price';
	if(document.getElementById(key + required + ln) === null){
		required = 'product_unit_price';
	}
	if (document.getElementById(key + 'name' + ln).value === '' || document.getElementById(key + required + ln).value === ''){
		return;
	}
	if(key === "product_" && document.getElementById(key + 'product_qty' + ln) !== null && document.getElementById(key + 'product_qty' + ln).value === ''){
		document.getElementById(key + 'product_qty' + ln).value =1;
	}
	var productUnitPrice = unformatNumber(document.getElementById(key + 'product_unit_price' + ln).value,',','.');

	if(document.getElementById(key + 'product_list_price' + ln) !== null && document.getElementById(key + 'product_discount' + ln) !== null && document.getElementById(key + 'discount' + ln) !== null){
		var listPrice = get_value(key + 'product_list_price' + ln,',','.');
		var discount = get_value(key + 'product_discount' + ln,',','.');
		var dis = document.getElementById(key + 'discount' + ln).value;
		if(dis == 'Amount')
		{
			if(discount > listPrice)
			{
				document.getElementById(key + 'product_discount' + ln).value = listPrice;
				discount = listPrice;
			}
			productUnitPrice = listPrice - discount;
			document.getElementById(key + 'product_unit_price' + ln).value = changeCurrencyWithDotForSum((listPrice - discount),numOfZero);
		}
		else if(dis == 'Percentage')
		{
			if(discount > 100)
			{
				document.getElementById(key + 'product_discount' + ln).value = 100;
				discount = 100;
			}
			discount = (discount/100) * listPrice;
			productUnitPrice = listPrice - discount;
			document.getElementById(key + 'product_unit_price' + ln).value = changeCurrencyWithDotForSum((listPrice - discount),numOfZero);
		}
		else
		{
			document.getElementById(key + 'product_unit_price' + ln).value = document.getElementById(key + 'product_list_price' + ln).value;
			document.getElementById(key + 'product_discount' + ln).value = '';
			discount = 0;
		}
		document.getElementById(key + 'product_list_price' + ln).value = changeCurrencyWithDotForSum(listPrice,numOfZero);
		//document.getElementById(key + 'product_discount' + ln).value = changeCurrencyWithDotForSum(unformatNumber(document.getElementById(key + 'product_discount' + ln).value,',','.'));
		document.getElementById(key + 'product_discount_amount' + ln).value = changeCurrencyWithDotForSum(-discount,numOfZero);
	}

	var productQty = 1;
	if(document.getElementById(key + 'product_qty' + ln) !== null){
		productQty = unformat2Number(document.getElementById(key + 'product_qty' + ln).value);
		Quantity_format2Number(ln);
	}
	var vat = unformatNumber(document.getElementById(key + 'vat' + ln).value,',','.');
	var productTotalPrice = productQty * productUnitPrice;
	var totalvat=(productTotalPrice * vat) /100;
	if(total_tax){
		productTotalPrice=productTotalPrice + totalvat;
	}
	document.getElementById(key + 'vat_amt' + ln).value = changeCurrencyWithDotForSum(totalvat,numOfZero);
	document.getElementById(key + 'product_unit_price' + ln).value = changeCurrencyWithDotForSum(productUnitPrice,numOfZero);
	document.getElementById(key + 'product_total_price' + ln).value = changeCurrencyWithDotForSum(productTotalPrice,numOfZero);
	var groupid = 0;
	if(enable_groups){
		groupid = document.getElementById(key + 'group_number' + ln).value;
	}
	groupid = 'group' + groupid;
	calculateTotal2(numOfZero,groupid);
	calculateTotal2(numOfZero);
	
}
function changeCurrencyWithDotForSum(number,numOfZero){
		var number = String(number); 
		var dotString = "";
		var finalDotString = "";
		if(number.indexOf(".") != "-1"){
			for(let m = eval(number.length-1); m >= 0; m--){
				if(number[m] == "."){
					break;
				}else{
					dotString = number[m] + dotString;
				}
			}
			if(numOfZero != "0"){
				if(dotString.length > 0){
					for(let j = 0; j <= eval(dotString.length-1);j++){
						finalDotString += dotString[j];
					}
				}
			}
		}
		var textEle = String(parseInt(number));
		if(textEle.indexOf("-") != "-1"){
				textEle = textEle.replace(/-/g,'');
				var minus = "-";
		}
		var str='';
		var i=eval(textEle.length-3);
		var st;
		var j;
		var text_val;
		var cusText;
			cusText='';
			str='';
		for(var u=eval(textEle.length-1);u>=0;u--)
		{
			str=textEle[u]+str;
			if(u == i && u > 0){
				str=","+str;	
				i=eval(i-3);
			}
		}
		if(minus != undefined){
				str = minus + str;
		}
		
		var wordCutted = "";
			if(numOfZero > 0){
				for(let j = 1; j <= numOfZero; j++){
					wordCutted+="0";
				}
			}
		
		if(number != ""){
			if(finalDotString != ""){
				return str+"."+finalDotString;
			}else{
				if(wordCutted != ""){
					return str+"."+wordCutted;
				}else{
					return str;
				}
			}
		}
		else{
			if(finalDotString != ""){
				return "0"+"."+finalDotString;
			}else{
				if(wordCutted != ""){
					return "0"+"."+wordCutted;
				}else{
					return "0";
				}
			}
		}
}
function changeCurrencyWithDot(oldNumber,number,numOfZero,ln,key,sum){
	if(oldNumber != ""){
		oldNumber = String(oldNumber).replace(/,/g,'');
	}
	if(number != ""){
		if(number[0] != "."){
			var newNum = '';
			number = String(number).replace(/,/g,'');
			var number2 = String(number);
			var textEle;
			var zeroNum2 = "";
			var zeroNum3 = "";
			if(number2.indexOf(".") != "-1"){
				if(number2[eval(number2.length-1)] != "."){
					textEle = String(parseInt(number));
					if(oldNumber != ""){
						var number3 = String(oldNumber);
						for(let u = eval(number2.length-1); u >= 0; u--){
							if(number2[u] == "."){
								break;
							}else{
								zeroNum2 = number2[u] + zeroNum2;
							}
						}
						for(let q = eval(number3.length-1); q >= 0; q--){
							if(number3[q] == "."){
								break;
							}else{
								zeroNum3 = number3[q] + zeroNum3;
							}
						}
						var oldZeroNum3 = zeroNum3;
						if(zeroNum2.length > zeroNum3.length){
							var distanceTwoNum = eval(zeroNum2.length - zeroNum3.length);
							for(let r = 1;r <= distanceTwoNum; r++){
								zeroNum3+=":";
							}
							for(let r = eval(zeroNum3.length-1); r >= 0; r--){
								if(zeroNum3[r] != zeroNum2[r]){
									if(zeroNum3[r] == ":"){
										newNum = zeroNum2[r] + newNum;
									}
								}else{
									break;
								}
							}
						}else if((zeroNum2.length < zeroNum3.length) || (zeroNum2 = "")){
							var wordCutted = zeroNum2;
						}
					}else{
						var oldZeroNum3 = "" ;
						for(let j = 1; j <= numOfZero; j++){
								oldZeroNum3+="0";
						}
					}
				
					if(numOfZero == 0){
						var numberOfZero = 0;
					}else{
						var numberOfZero = numOfZero;
					}
					if(newNum != ''){
						var distance = eval(zeroNum2.length - numOfZero);
						var newNum2 = "";
						if(distance > 0){
							for(let k = eval(newNum.length-1); k >=0; k--){
								if(k > eval(newNum.length-1-distance)){
									newNum2 = newNum[k] + newNum2;
								}
							}
								textEle += newNum2;
						
						}else{
							wordCutted=zeroNum2;
						}
					}
					if(wordCutted == undefined){
						var wordCutted = "";
						if(numberOfZero == 0){
							for(let i = eval(number2.length -1); i >= 0; i--){
								if(number2[i] == "."){
									break;
								}else{
									wordCutted = number2[i] + wordCutted;
									numberOfZero++;
								}
							}
								ZeroNum = numberOfZero;
						}else{
							if(number2.indexOf(".") == "-1"){
								for(let j = 1; j <= numberOfZero; j++){
									wordCutted+="0";
								}
							}else{
								wordCutted = oldZeroNum3;
							}
						}
					}
				}else{
					var wordCutted = "";
					if(oldNumber != ""){
						var number3 = String(oldNumber);
						if(number3.length > number2.length){
							//number2 = number2.substring(0,eval(number2.length-1));
							//for(let j = 1; j <= numOfZero; j++){
							//	wordCutted+="0";
							//}
							number2 = number2.substring(0,eval(number2.length-1));
							var wordCutted = ".";
						}
						else{
							number2 = number2.substring(0,eval(number2.length-1));
							for(let j = 1; j <= numOfZero; j++){
								wordCutted+="0";
							}
						}
					}
					textEle = String(parseInt(number2));
				}		
			}
			else
			{
				var wordCutted = "";
				if(oldNumber != ""){
					var number3 = String(oldNumber);
				}else{
					var number3 = "";
				}
				if((number3.length < number2.length) || number3 == ""){
					if(number2[eval(number2.length-1)] != "."){
						if(numOfZero != 0){
							for(let j = 1; j <= numOfZero; j++){
								wordCutted+="0";
							}
						}
					}
				}
				else{
					if(number3[eval(number3.length-1)] == "." || ((number3.indexOf(".") != "-1" && number2.indexOf(".") == "-1") && number2 != "")){
						if(numOfZero != 0){
							for(let j = 1; j <= numOfZero; j++){
								wordCutted+="0";
							}
						}
					}
				}
				
				textEle = String(parseInt(number2));
				
			}
			var str='';
			var i=eval(textEle.length-3);
			var st;
			var j;
			var text_val;
			var cusText;
			cusText='';
			str='';
			if(textEle.indexOf("-") != "-1"){
				textEle = textEle.replace(/-/g,'');
				var minus = "-";
			}
			for(var u=eval(textEle.length-1);u>=0;u--)
			{	
					
				str=textEle[u]+str;
				if(u == i && u > 0){
					str=","+str;	
					i=eval(i-3);
				}	
			}
			if(minus != undefined){
				str = minus + str;
			}
		}else{
			var str = "";
			var wordCutted = "";
			for(let j = 1; j <= numOfZero; j++){
				wordCutted+="0";
			}
		}
		setTimeout(function(){
			sum(ln,key,numOfZero);
		},100);
		if(wordCutted != ""){
			if(wordCutted != "."){
				return str+"."+wordCutted;
			}else{
				return str+wordCutted;
			}
		}
		else{
			return str;
		}
	}
	//}else{
	//	return str;
	//} 
}
function formatListPrice(ln){

  if (typeof currencyFields !== 'undefined'){
    var product_currency_id = document.getElementById('product_currency' + ln).value;
    product_currency_id = product_currency_id ? product_currency_id : -99;//Assume base currency if no id
    var product_currency_rate = get_rate(product_currency_id);
    var dollar_product_price = ConvertToDollar(document.getElementById('product_product_list_price' + ln).value, product_currency_rate);
    document.getElementById('product_product_list_price' + ln).value = format2Number(ConvertFromDollar(dollar_product_price, lastRate));
    var dollar_product_cost = ConvertToDollar(document.getElementById('product_product_cost_price' + ln).value, product_currency_rate);
    document.getElementById('product_product_cost_price' + ln).value = format2Number(ConvertFromDollar(dollar_product_cost, lastRate));
  }
  else
  {
    document.getElementById('product_product_list_price' + ln).value = format2Number(document.getElementById('product_product_list_price' + ln).value);
    document.getElementById('product_product_cost_price' + ln).value = format2Number(document.getElementById('product_product_cost_price' + ln).value);
  }

  calculateLine(ln,"product_");
}
function formatListPrice2(ln){

  if (typeof currencyFields !== 'undefined'){
    var product_currency_id = document.getElementById('product_currency' + ln).value;
    product_currency_id = product_currency_id ? product_currency_id : -99;//Assume base currency if no id
    var product_currency_rate = get_rate(product_currency_id);
    var dollar_product_price = ConvertToDollar(document.getElementById('product_product_list_price' + ln).value, product_currency_rate);
    document.getElementById('product_product_list_price' + ln).value = format2Number(ConvertFromDollar(dollar_product_price, lastRate));
    var dollar_product_cost = ConvertToDollar(document.getElementById('product_product_cost_price' + ln).value, product_currency_rate);
    document.getElementById('product_product_cost_price' + ln).value = format2Number(ConvertFromDollar(dollar_product_cost, lastRate));
  }
  else
  {
    document.getElementById('product_product_list_price' + ln).value = changeCurrencyWithDotForSum(document.getElementById('product_product_list_price' + ln).value,sig_digits);
    document.getElementById('product_product_cost_price' + ln).value = changeCurrencyWithDotForSum(document.getElementById('product_product_cost_price' + ln).value,sig_digits);
  }

  calculateLine3(ln,"product_",sig_digits);
}
function showGroupLine(recor,a){
	var content = document.getElementsByClassName("tab-content")[0];
	if(document.getElementById("tab-content-2").innerHTML == null || document.getElementById("tab-content-2").innerHTML == ''){
	//$( '<div class="tab-pane-NOBOOTSTRAPTOGGLER fade in" id="tab-content-2" style="display: block;"></div>' ).insertAfter($("#tab-content-0"));
	var html = "";
	html += '<div class="row detail-view-row">';
	html += '<div class="col-xs-12 col-sm-6 detail-view-row-item">';
	html += '<div class="col-xs-12 col-sm-4 label col-1-label">';
	html += 'Tiền tệ:';
	html += '</div>';
	html += '<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="id" field="currency_id">';
	html += '<span id="currency_id_span">';
	html += 'VNĐ';
	html += '</span>';
	html += '<div class="inlineEditIcon col-xs-hidden">';
	html += '<span class="suitepicon suitepicon-action-edit"></span>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '<div class="col-xs-12 col-sm-6 detail-view-row-item">';
	html += '</div>';
	html += '</div>';
	html += '<div class="row detail-view-row">';
	html += '<div class="col-xs-12 col-sm-12 detail-view-row-item">';
	html += '<div class="col-xs-12 col-sm-2 label col-1-label">';
	html += 'Các hàng mục:';
	html += '</div>';
	html += '<div class="col-xs-12 col-sm-10 detail-view-field " type="function" field="line_items" colspan="3">';
	html += '<span id="line_items_span">';
	html += '<table border="0" width="100%" cellpadding="0" cellspacing="0"><tbody><tr><td class="tabDetailViewDL" style="text-align: left;padding:2px;" scope="row">&nbsp;</td><td class="tabDetailViewDL" style="text-align: left;padding:2px;"  scope="row">Tên nhóm:</td><td class="tabDetailViewDL" colspan="7" id= "psName" style="text-align: left;padding:2px;"></td></tr><tr><td width="5%" class="tabDetailViewDL" style="text-align: left;padding:2px;" scope="row">&nbsp;</td><td width="10%" class="tabDetailViewDL" style="text-align: left;padding:2px;" scope="row">Số lượng</td><td width="12%" class="tabDetailViewDL" style="text-align: left;padding:2px;" scope="row">Sản phẩm</td><td width="12%" class="tabDetailViewDL" style="text-align: right;padding:2px;" scope="row">Danh sách</td><td width="12%" class="tabDetailViewDL" style="text-align: right;padding:2px;" scope="row">Chiết khấu</td><td width="12%" class="tabDetailViewDL" style="text-align: right;padding:2px;" scope="row">Giá bán</td><td width="12%" class="tabDetailViewDL" style="text-align: right;padding:2px;" scope="row">Thuế</td><td width="12%" class="tabDetailViewDL" style="text-align: right;padding:2px;" scope="row">Tổng số tiền thuế</td><td width="12%" class="tabDetailViewDL" style="text-align: right;padding:2px;" scope="row">Tổng cộng</td></tr><tr><td class="tabDetailViewDF" style="text-align: left; padding:2px;">1</td><td class="tabDetailViewDF" style="padding:2px;">1</td><td class="tabDetailViewDF" style="padding:2px;"><a href="index.php?module=AOS_Products&amp;action=DetailView&amp;record=60b458eb-29a7-f3d1-81ef-5e4501407125" class="tabDetailViewDFLink">san pham 4</a><br></td><td class="tabDetailViewDF" style="text-align: right; padding:2px;">đ50,000,000.00</td><td class="tabDetailViewDF" style="text-align: right; padding:2px;">-</td><td class="tabDetailViewDF" style="text-align: right; padding:2px;">đ50,000,000.00</td><td class="tabDetailViewDF" style="text-align: right; padding:2px;">0%</td><td class="tabDetailViewDF" style="text-align: right; padding:2px;">đ0.00</td><td class="tabDetailViewDF" style="text-align: right; padding:2px;">đ50,000,000.00</td></tr><tr><td width="5%" class="tabDetailViewDL" style="text-align: left;padding:2px;" scope="row">&nbsp;</td><td width="46%" class="dataLabel" style="text-align: left;padding:2px;" colspan="2" scope="row">Dịch vụ</td><td width="12%" class="dataLabel" style="text-align: right;padding:2px;" scope="row">Danh sách</td><td width="12%" class="dataLabel" style="text-align: right;padding:2px;" scope="row">Chiết khấu</td><td width="12%" class="dataLabel" style="text-align: right;padding:2px;" scope="row">Giá bán</td><td width="12%" class="dataLabel" style="text-align: right;padding:2px;" scope="row">Thuế</td><td width="12%" class="dataLabel" style="text-align: right;padding:2px;" scope="row">Tổng số tiền thuế</td><td width="12%" class="dataLabel" style="text-align: right;padding:2px;" scope="row">Tổng cộng</td></tr><tr><td class="tabDetailViewDF" style="text-align: left; padding:2px;">1</td><td class="tabDetailViewDF" style="padding:2px;" colspan="2">testservice</td><td class="tabDetailViewDF" style="text-align: right; padding:2px;">đ5,555.00</td><td class="tabDetailViewDF" style="text-align: right; padding:2px;">100%</td><td class="tabDetailViewDF" style="text-align: right; padding:2px;">đ0.00</td><td class="tabDetailViewDF" style="text-align: right; padding:2px;">0%</td><td class="tabDetailViewDF" style="text-align: right; padding:2px;">đ0.00</td><td class="tabDetailViewDF" style="text-align: right; padding:2px;">đ0.00</td></tr><tr><td colspan="9" nowrap="nowrap"><br></td></tr><tr><td class="tabDetailViewDL" colspan="8" style="text-align: right;padding:2px;" scope="row">Tổng cộng:&nbsp;&nbsp;</td><td class="tabDetailViewDL" style="text-align: right;padding:2px;">đ50,005,555.00</td></tr><tr><td class="tabDetailViewDL" colspan="8" style="text-align: right;padding:2px;" scope="row">Chiết khấu:&nbsp;&nbsp;</td><td class="tabDetailViewDL" style="text-align: right;padding:2px;">đ-5,555.00</td></tr><tr><td class="tabDetailViewDL" colspan="8" style="text-align: right;padding:2px;" scope="row">Tổng số phụ:&nbsp;&nbsp;</td><td class="tabDetailViewDL" style="text-align: right;padding:2px;">đ50,000,000.00</td></tr><tr><td class="tabDetailViewDL" colspan="8" style="text-align: right;padding:2px;" scope="row">Thuế:&nbsp;&nbsp;</td><td class="tabDetailViewDL" style="text-align: right;padding:2px;">đ0.00</td></tr><tr><td class="tabDetailViewDL" colspan="8" style="text-align: right;padding:2px;" scope="row">Tổng số:&nbsp;&nbsp;</td><td class="tabDetailViewDL" style="text-align: right;padding:2px;">đ50,000,000.00</td></tr></tbody></table>';
	html += '</span>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '<div class="row detail-view-row">';
	html += '<div class="col-xs-12 col-sm-12 detail-view-row-item">';
	html += '</div>';
	html += '</div>';
	html += '<div class="row detail-view-row">';
	html += '<div class="col-xs-12 col-sm-12 detail-view-row-item">';
	html += '<div class="col-xs-12 col-sm-2 label col-1-label">';
	html += 'Tổng cộng:';
	html += '</div>';
	html += '<div class="col-xs-12 col-sm-10 detail-view-field inlineEdit" type="currency" field="total_amt" colspan="3">';
	html += '<span id="total_amt">';
	html += '50,005,555.00';
	html += '</span>';
	html += '<div class="inlineEditIcon col-xs-hidden">';
	html += '<span class="suitepicon suitepicon-action-edit"></span>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '<div class="row detail-view-row">';
	html += '<div class="col-xs-12 col-sm-12 detail-view-row-item">';
	html += '<div class="col-xs-12 col-sm-2 label col-1-label">';
	html += 'Chiết khấu:';
	html += '</div>';
	html += '<div class="col-xs-12 col-sm-10 detail-view-field inlineEdit" type="currency" field="discount_amount" colspan="3">';
	html += '<span id="discount_amount">';
	html += '-5,555.00';
	html += '</span>';
	html += '<div class="inlineEditIcon col-xs-hidden">';
	html += '<span class="suitepicon suitepicon-action-edit"></span>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '<div class="row detail-view-row">';
	html += '<div class="col-xs-12 col-sm-12 detail-view-row-item">';
	html += '<div class="col-xs-12 col-sm-2 label col-1-label">';
	html += 'Tổng số phụ:';
	html += '</div>';
	html += '<div class="col-xs-12 col-sm-10 detail-view-field inlineEdit" type="currency" field="subtotal_amount" colspan="3">';
	html += '<span id="subtotal_amount">';
	html += '50,000,000.00';
	html += '</span>';
	html += '<div class="inlineEditIcon col-xs-hidden">';
	html += '<span class="suitepicon suitepicon-action-edit"></span>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '<div class="row detail-view-row">';
	html += '<div class="col-xs-12 col-sm-12 detail-view-row-item">';
	html += '<div class="col-xs-12 col-sm-2 label col-1-label">';
	html += 'Mua sắm:';
	html += '</div>';
	html += '<div class="col-xs-12 col-sm-10 detail-view-field inlineEdit" type="currency" field="shipping_amount" colspan="3">';
	html += '<span id="shipping_amount">';
	html += '0.00';
	html += '</span>';
	html += '<div class="inlineEditIcon col-xs-hidden">';
	html += '<span class="suitepicon suitepicon-action-edit"></span>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '<div class="row detail-view-row">';
	html += '<div class="col-xs-12 col-sm-12 detail-view-row-item">';
	html += '<div class="col-xs-12 col-sm-2 label col-1-label">';
	html += 'Thuế vận chuyển:';
	html += '</div>';
	html += '<div class="col-xs-12 col-sm-10 detail-view-field inlineEdit" type="currency" field="shipping_tax_amt" colspan="3">';
	html += '<span id="shipping_tax_amt_span">';
	html += '0.00';
	html += '</span>';
	html += '<div class="inlineEditIcon col-xs-hidden">';
	html += '<span class="suitepicon suitepicon-action-edit"></span>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '<div class="row detail-view-row">';
	html += '<div class="col-xs-12 col-sm-12 detail-view-row-item">';
	html += '<div class="col-xs-12 col-sm-2 label col-1-label">';
	html += 'Thuế:';
	html += '</div>';
	html += '<div class="col-xs-12 col-sm-10 detail-view-field inlineEdit" type="currency" field="tax_amount" colspan="3">';
	html += '<span id="tax_amount">';
	html += '0.00';
	html += '</span>';
	html += '<div class="inlineEditIcon col-xs-hidden">';
	html += '<span class="suitepicon suitepicon-action-edit"></span>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '<div class="row detail-view-row">';
	html += '<div class="col-xs-12 col-sm-12 detail-view-row-item">';
	html += '<div class="col-xs-12 col-sm-2 label col-1-label">';
	html += 'Tổng số:';
	html += '</div>';
	html += '<div class="col-xs-12 col-sm-10 detail-view-field inlineEdit" type="currency" field="total_amount" colspan="3">';
	html += '<span id="total_amount">';
	html += '50,000,000.00';
	html += '</span>';
	html += '<div class="inlineEditIcon col-xs-hidden">';
	html += '<span class="suitepicon suitepicon-action-edit"></span>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	var contentTab = document.getElementById("tab-content-2");
	contentTab.innerHTML = html;
	document.getElementById("tab-content-2").style.display = "block";
	}else{
	document.getElementById("tab-content-2").style.display = "block";
	}
	if(document.getElementById("tab-content-0") != null  &&  document.getElementById("tab-content-0") != undefined){
	document.getElementById("tab-content-0").style.display = "none";
	}
	if(document.getElementById("tab-content-1") != null  &&  document.getElementById("tab-content-1") != undefined){
	document.getElementById("tab-content-1").style.display = "none";
	}
	if(document.getElementById("tab-content-3") != null  &&  document.getElementById("tab-content-3") != undefined){
	document.getElementById("tab-content-3").style.display = "none";
	}
	if(document.getElementById("tab-content-4") != null  &&  document.getElementById("tab-content-4") != undefined){
	document.getElementById("tab-content-4").style.display = "none";
	}
	a.onblur = function(){
		document.getElementById("tab-content-2").style.display = "none";
	}
	
	$.ajax({
					type: "POST",
					url: "index.php?entryPoint=getItem",
					data: {
					  id:recor
					},
					dataType: "json",
					success: function(data) {
						
						var html = '';
						html += '<table border="0" width="100%" cellpadding="0" cellspacing="0">';
						html += '<tbody>';
						let u = 1;
						var unitPrice = 0;
						var discount = 0;
						var vatAmt = 0;
						var totalPrice = 0;
						var subtotal = 0;
						for(let i = 0; i < data.lineItem.length ; i++){
							html += '<tr><td class="tabDetailViewDL" style="text-align: left;padding:2px;" scope="row">&nbsp;</td>';
							html += '<td class="tabDetailViewDL" style="text-align: left;padding:2px;"  scope="row">Tên nhóm:</td>';
							html += '<td class="tabDetailViewDL" colspan="7" class= "psName" style="text-align: left;padding:2px;">'+data.lineItem[i][0].name+'</td></tr>';
							html += '<tr><td class="tabDetailViewDL" style="text-align: left;padding:2px;" scope="row" width="5%">&nbsp;</td>';
							html += '<td class="tabDetailViewDL" style="text-align: left;padding:2px;" scope="row" width="10%">Số lượng</td>';
							html += '<td class="tabDetailViewDL" style="text-align: left;padding:2px;" scope="row" width="12%">Sản phẩm</td>';
							html += '<td class="tabDetailViewDL" style="text-align: right;padding:2px;" scope="row" width="12%">Danh sách</td>';
							html += '<td class="tabDetailViewDL" style="text-align: right;padding:2px;" scope="row" width="12%">Chiết khấu</td>';
							html += '<td class="tabDetailViewDL" style="text-align: right;padding:2px;" scope="row" width="12%">Giá bán</td>';
							html += '<td class="tabDetailViewDL" style="text-align: right;padding:2px;" scope="row" width="12%">Thuế</td>';
							html += '<td class="tabDetailViewDL" style="text-align: right;padding:2px;" scope="row" width="12%">Tổng số tiền thuế</td>';
							html += '<td class="tabDetailViewDL" style="text-align: right;padding:2px;" scope="row" width="12%">Tổng cộng</td></tr>';
							for(let j = 0; j < data.lineItem[i].length ; j++){
								if(j > 0){
									for(let k = 0; k < data.lineItem[i][j].length; k++ ){
										html += '<tr>';
										html += '<td class="tabDetailViewDF" style="text-align: left; padding:2px;">'+u+'</td>';
										html += '<td class="tabDetailViewDF" style="padding:2px;">'+parseInt(data.lineItem[i][j][k].product_qty)+'</td>';
										html += '<td class="tabDetailViewDF" style="padding:2px;">'+data.lineItem[i][j][k].name+'</td>';
										html += '<td class="tabDetailViewDF" style="text-align: right; padding:2px;">'+formatCurrency(data.lineItem[i][j][k].product_unit_price)+'</td>';
										html += '<td class="tabDetailViewDF" style="text-align: right; padding:2px;">'+formatCurrency(data.lineItem[i][j][k].product_discount)+'</td>';
										html += '<td class="tabDetailViewDF" style="text-align: right; padding:2px;">'+formatCurrency(data.lineItem[i][j][k].product_unit_price)+'</td>';
										html += '<td class="tabDetailViewDF" style="text-align: right; padding:2px;">'+parseInt(data.lineItem[i][j][k].vat)+'% </td>';
										html += '<td class="tabDetailViewDF" style="text-align: right; padding:2px;">'+formatCurrency(data.lineItem[i][j][k].vat_amt)+'</td>';
										html += '<td class="tabDetailViewDF" style="text-align: right; padding:2px;">'+formatCurrency(data.lineItem[i][j][k].product_total_price)+'</td>';
										html += '</tr>';
										unitPrice = eval(unitPrice + parseInt(data.lineItem[i][j][k].product_unit_price));
										discount = eval(discount + parseInt(data.lineItem[i][j][k].product_discount));
										vatAmt = eval(vatAmt + parseInt(data.lineItem[i][j][k].vat_amt));
										totalPrice = eval(totalPrice + parseInt(data.lineItem[i][j][k].product_total_price));
										u++;
									}
									}else{
											//document.getElementsByClassName("psName")[i].innerHTML = data.lineItem[i][j].name;
									}
									
								}
								html += '<tr><td class="tabDetailViewDL" colspan="8" style="text-align: right;padding:2px;" scope="row">Tổng cộng:&nbsp;&nbsp;</td>';
								html += '<td class="tabDetailViewDL" style="text-align: right;padding:2px;">đ'+formatCurrency(unitPrice)+'</td></tr>';
								html += '<tr><td class="tabDetailViewDL" colspan="8" style="text-align: right;padding:2px;" scope="row">Chiết khấu:&nbsp;&nbsp;</td>';
								html += '<td class="tabDetailViewDL" style="text-align: right;padding:2px;">đ'+formatCurrency(discount)+'</td></tr>';
								html += '<tr><td class="tabDetailViewDL" colspan="8" style="text-align: right;padding:2px;" scope="row">Tổng số phụ:&nbsp;&nbsp;</td>';
								html += '<td class="tabDetailViewDL" style="text-align: right;padding:2px;">đ'+formatCurrency(subtotal)+'</td></tr>';
								html += '<tr><td class="tabDetailViewDL" colspan="8" style="text-align: right;padding:2px;" scope="row">Thuế:&nbsp;&nbsp;</td>';
								html += '<td class="tabDetailViewDL" style="text-align: right;padding:2px;">đ'+formatCurrency(vatAmt)+'</td></tr>';
								html += '<tr><td class="tabDetailViewDL" colspan="8" style="text-align: right;padding:2px;" scope="row">Tổng số:&nbsp;&nbsp;</td>';
								html += '<td class="tabDetailViewDL" style="text-align: right;padding:2px;">đ'+formatCurrency(eval(totalPrice+vatAmt))+'</td></tr>';
						}
						
						html += '</tbody>';
						html +='<table>';
						document.getElementById("line_items_span").innerHTML = html;
						document.getElementById("total_amt").innerHTML = formatCurrency(data.po.total_amt);
						document.getElementById("discount_amount").innerHTML = formatCurrency(data.po.discount_amount);
						document.getElementById("subtotal_amount").innerHTML = formatCurrency(data.po.subtotal_amount);
						document.getElementById("shipping_amount").innerHTML = formatCurrency(data.po.shipping_amount);
						document.getElementById("tax_amount").innerHTML = formatCurrency(data.po.tax_amount);
						document.getElementById("shipping_tax_amt_span").innerHTML = formatCurrency(data.po.shipping_tax_amt);
						document.getElementById("total_amount").innerHTML = formatCurrency(data.po.total_amount);
					}
					});
}

/**
 * Insert Service Line
 */
function showPoAccount(recor,a){
	var content = document.getElementsByClassName("tab-content")[0];
	if(document.getElementById("tab-content-1").innerHTML == null || document.getElementById("tab-content-1").innerHTML == ''){
	//$( '<div class="tab-pane-NOBOOTSTRAPTOGGLER fade in" id="tab-content-1" style="display: block;"></div>' ).insertAfter($("#tab-content-0"));
	var html = '';
	
	html += '<div class="row detail-view-row">';
	html += '<div class="col-xs-12 col-sm-6 detail-view-row-item">';
	html += '<div class="col-xs-12 col-sm-4 label col-1-label">';
	html += 'Account:';
	html += '</div>';
	html += '<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="billing_account">';
	html += '<span id="billing_account_id" class="sugar_field" data-id-value=""></span>';
	html += '<div class="inlineEditIcon col-xs-hidden">';
	html += '<span class="suitepicon suitepicon-action-edit"></span>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '<div class="col-xs-12 col-sm-6 detail-view-row-item">';
	html += '</div>';
	html += '</div>';
	html += '<div class="row detail-view-row">';
	html += '<div class="col-xs-12 col-sm-6 detail-view-row-item">';
	html += '<div class="col-xs-12 col-sm-4 label col-1-label">';
	html += 'Contact:';
	html += '</div>';
	html += '<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="billing_contact">';
	html += '<span id="billing_contact_id" class="sugar_field" data-id-value=""></span>';
	html += '<div class="inlineEditIcon col-xs-hidden">';
	html += '<span class="suitepicon suitepicon-action-edit"></span>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '<div class="col-xs-12 col-sm-6 detail-view-row-item">';
	html += '</div>';
	html += '</div>';
	html += '<div class="row detail-view-row">';
	html += '<div class="col-xs-12 col-sm-6 detail-view-row-item">';
	html += '<div class="col-xs-12 col-sm-4 label col-1-label">';
	html += 'Địa chỉ:';
	html += '</div>';
	html += '<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="varchar" id="billing_address_street" field="billing_address_street">';
	/*
	html += '<table width="100%" cellspacing="0" cellpadding="0" border="0">';
	html += '<tbody><tr>';
	html += '<td width="99%">';
	html += '<input type="hidden" class="sugar_field" id="billing_address_street" value="">';
	html += '<input type="hidden" class="sugar_field" id="billing_address_city" value="">';
	html += '<input type="hidden" class="sugar_field" id="billing_address_state" value="">';
	html += '<input type="hidden" class="sugar_field" id="billing_address_country" value="">';
	html += '<input type="hidden" class="sugar_field" id="billing_address_postalcode" value="">';
	html += '<br>';
	html += '&nbsp;&nbsp;';
	html += '<br>';
	html += '</td>';
	html += '<td class="dataField" width="1%">';
	html += '</td>';
	html += '</tr>';
	html += '</tbody></table>';
	html += '<div class="inlineEditIcon col-xs-hidden">';
	html += '<span class="suitepicon suitepicon-action-edit"></span>';
	html += '</div>';
	*/
	html += '</div>';
	html += '</div>';
	html += '<div class="col-xs-12 col-sm-6 detail-view-row-item">';
	html += '<div class="col-xs-12 col-sm-4 label col-2-label">';
	html += 'Địa chỉ giao hàng:';
	html += '</div>';
	html += '<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="varchar" id="shipping_address_street" field="shipping_address_street">';
	/*
	html += '<table width="100%" cellspacing="0" cellpadding="0" border="0">';
	html += '<tbody><tr>';
	html += '<td width="99%">';
	html += '<input type="hidden" class="sugar_field" id="shipping_address_street" value="">';
	html += '<input type="hidden" class="sugar_field" id="shipping_address_city" value="">';
	html += '<input type="hidden" class="sugar_field" id="shipping_address_state" value="">';
	html += '<input type="hidden" class="sugar_field" id="shipping_address_country" value="">';
	html += '<input type="hidden" class="sugar_field" id="shipping_address_postalcode" value="">';
	html += '<br>';
	html += '&nbsp;&nbsp;';
	html += '<br>';
	html += '</td>';
	html += '<td class="dataField" width="1%">';
	html += '</td>';
	html += '</tr>';
	html += '</tbody></table>';
	html += '<div class="inlineEditIcon col-xs-hidden">';
	html += '<span class="suitepicon suitepicon-action-edit"></span>';
	html += '</div>';
	*/
	html += '</div>';
	html += '</div>';
	html += '</div>';
	var contentTab = document.getElementById("tab-content-1");
	contentTab.innerHTML = html;
	document.getElementById("tab-content-1").style.display = "block";
	}
	else{
	document.getElementById("tab-content-1").style.display = "block";
	}
	if(document.getElementById("tab-content-0") != null  &&  document.getElementById("tab-content-0") != undefined){
	document.getElementById("tab-content-0").style.display = "none";
	}
	if(document.getElementById("tab-content-2") != null  &&  document.getElementById("tab-content-2") != undefined){
	document.getElementById("tab-content-2").style.display = "none";
	}
	if(document.getElementById("tab-content-3") != null  &&  document.getElementById("tab-content-3") != undefined){
	document.getElementById("tab-content-3").style.display = "none";
	}
	if(document.getElementById("tab-content-4") != null  &&  document.getElementById("tab-content-4") != undefined){
	document.getElementById("tab-content-4").style.display = "none";
	}
	a.onblur = function(){
		document.getElementById("tab-content-1").style.display = "none";
	}
	$.ajax({
					type: "POST",
					url: "index.php?entryPoint=getItem",
					data: {
					  id:recor
					},
					dataType: "json",
					success: function(data) {
						document.getElementById("billing_account_id").innerHTML = data.billing_account_id ;
						document.getElementById("billing_contact_id").innerHTML = data.billing_contact_id;
						document.getElementById("billing_address_street").innerHTML = data.po.billing_address_state+"<br>"+data.po.billing_address_street+"<br>"+data.po.billing_address_city+"<br>"+data.po.billing_address_country;
						document.getElementById("shipping_address_street").innerHTML = data.po.shipping_address_state+"<br>"+data.po.shipping_address_street+"<br>"+data.po.shipping_address_city+"<br>"+data.po.shipping_address_country;
					}
	});
	
}
function insertServiceLine(tableid, groupid) {

  if(!enable_groups){
    tableid = "service_group0";
  }
  if (document.getElementById(tableid + '_head') !== null) {
    document.getElementById(tableid + '_head').style.display = "";
  }

  var vat_hidden = document.getElementById("vathidden").value;
  var discount_hidden = document.getElementById("discounthidden").value;

  tablebody = document.createElement("tbody");
  tablebody.id = "service_body" + servln;
  document.getElementById(tableid).appendChild(tablebody);

  var x = tablebody.insertRow(-1);
  x.id = 'service_line' + servln;

  var a = x.insertCell(0);
  a.colSpan = "4";
  a.innerHTML = "<textarea name='service_name[" + servln + "]' id='service_name" + servln + "'  cols='64' title='' tabindex='116' class='service_name'></textarea><input type='hidden' name='service_product_id[" + servln + "]' id='service_product_id" + servln + "'  maxlength='50' value='0'>";

  var a1 = x.insertCell(1);
  a1.innerHTML = "<input type='text' name='service_product_list_price[" + servln + "]' id='service_product_list_price" + servln + "' maxlength='50' value='' title='' tabindex='116'   onblur='calculateLine(" + servln + ",\"service_\");' class='service_list_price'>";

  if (typeof currencyFields !== 'undefined'){
    currencyFields.push("service_product_list_price" + servln);
  }

  var a2 = x.insertCell(2);
  a2.innerHTML = "<input type='text' name='service_product_discount[" + servln + "]' id='service_product_discount" + servln + "'  maxlength='50' value='' title='' tabindex='116' onblur='calculateLine(" + servln + ",\"service_\");' onblur='calculateLine(" + servln + ",\"service_\");' class='service_discount_text'><input type='hidden' name='service_product_discount_amount[" + servln + "]' id='service_product_discount_amount" + servln + "' value=''/>";
  a2.innerHTML += "<select tabindex='116' name='service_discount[" + servln + "]' id='service_discount" + servln + "' onchange='calculateLine(" + servln + ",\"service_\");' class='service_discount_select'>" + discount_hidden + "</select>";

  var b = x.insertCell(3);
  b.innerHTML = "<input type='text' name='service_product_unit_price[" + servln + "]' id='service_product_unit_price" + servln + "' maxlength='50' value='' title='' tabindex='116'   onblur='calculateLine(" + servln + ",\"service_\");' class='service_unit_price'>";

  if (typeof currencyFields !== 'undefined'){
    currencyFields.push("service_product_unit_price" + servln);
  }
  var c = x.insertCell(4);
  c.innerHTML = "<input type='text' name='service_vat_amt[" + servln + "]' id='service_vat_amt" + servln + "' maxlength='250' value='' title='' tabindex='116' readonly='readonly' class='service_vat_text'>";
  c.innerHTML += "<select tabindex='116' name='service_vat[" + servln + "]' id='service_vat" + servln + "' onchange='calculateLine(" + servln + ",\"service_\");' class='service_vat_select'>" + vat_hidden + "</select>";
  if (typeof currencyFields !== 'undefined'){
    currencyFields.push("service_vat_amt" + servln);
  }

  var e = x.insertCell(5);
  e.innerHTML = "<input type='text' name='service_product_total_price[" + servln + "]' id='service_product_total_price" + servln + "' maxlength='50' value='' title='' tabindex='116' readonly='readonly' class='service_total_price'><input type='hidden' name='service_group_number[" + servln + "]' id='service_group_number" + servln + "' value='"+ groupid +"'>";

  if (typeof currencyFields !== 'undefined'){
    currencyFields.push("service_product_total_price" + servln);
  }
  var f = x.insertCell(6);
  f.innerHTML = "<input type='hidden' name='service_deleted[" + servln + "]' id='service_deleted" + servln + "' value='0'><input type='hidden' name='service_id[" + servln + "]' id='service_id" + servln + "' value=''><button type='button' class='button service_delete_line' id='service_delete_line" + servln + "' value='" + SUGAR.language.get(module_sugar_grp1, 'LBL_REMOVE_PRODUCT_LINE') + "' tabindex='116' onclick='markLineDeleted(" + servln + ",\"service_\")'><span class=\"suitepicon suitepicon-action-clear\"></span></button><br>";

  addAlignedLabels(servln, 'service');

  servln++;

  return servln - 1;
}


/**
 * Insert product Header
 */

function insertProductHeader(tableid){
  tablehead = document.createElement("thead");
  tablehead.id = tableid +"_head";
  tablehead.style.display="none";
  document.getElementById(tableid).appendChild(tablehead);

  var x=tablehead.insertRow(-1);
  x.id='product_head';

  var a=x.insertCell(0);
  a.style.color="rgb(68,68,68)";
  a.innerHTML=SUGAR.language.get(module_sugar_grp1, 'LBL_PRODUCT_QUANITY');

  var b=x.insertCell(1);
  b.style.color="rgb(68,68,68)";
  b.innerHTML=SUGAR.language.get(module_sugar_grp1, 'LBL_PRODUCT_NAME');

  var b1=x.insertCell(2);
  b1.colSpan = "2";
  b1.style.color="rgb(68,68,68)";
  b1.innerHTML=SUGAR.language.get(module_sugar_grp1, 'LBL_PART_NUMBER');

  var c=x.insertCell(3);
  c.style.color="rgb(68,68,68)";
  c.innerHTML=SUGAR.language.get(module_sugar_grp1, 'LBL_LIST_PRICE');

  var d=x.insertCell(4);
  d.style.color="rgb(68,68,68)";
  d.innerHTML=SUGAR.language.get(module_sugar_grp1, 'LBL_DISCOUNT_AMT');

  var e=x.insertCell(5);
  e.style.color="rgb(68,68,68)";
  e.innerHTML=SUGAR.language.get(module_sugar_grp1, 'LBL_UNIT_PRICE');

  var f=x.insertCell(6);
  f.style.color="rgb(68,68,68)";
  f.innerHTML=SUGAR.language.get(module_sugar_grp1, 'LBL_VAT_AMT');

  var g=x.insertCell(7);
  g.style.color="rgb(68,68,68)";
  g.innerHTML=SUGAR.language.get(module_sugar_grp1, 'LBL_TOTAL_PRICE');

  var h=x.insertCell(8);
  h.style.color="rgb(68,68,68)";
  h.innerHTML='&nbsp;';
}


/**
 * Insert service Header
 */
function exportExcel(module)
{
   $.ajax({
        url: 'index.php?entryPoint=exportExcel',
        type: 'POST',
        success: function (get_body) {
         
        }
    });
   
}
function insertServiceHeader(tableid){
  tablehead = document.createElement("thead");
  tablehead.id = tableid +"_head";
  tablehead.style.display="none";
  document.getElementById(tableid).appendChild(tablehead);

  var x=tablehead.insertRow(-1);
  x.id='service_head';

  var a=x.insertCell(0);
  a.colSpan = "4";
  a.style.color="rgb(68,68,68)";
  a.innerHTML=SUGAR.language.get(module_sugar_grp1, 'LBL_SERVICE_NAME');

  var b=x.insertCell(1);
  b.style.color="rgb(68,68,68)";
  b.innerHTML=SUGAR.language.get(module_sugar_grp1, 'LBL_SERVICE_LIST_PRICE');

  var c=x.insertCell(2);
  c.style.color="rgb(68,68,68)";
  c.innerHTML=SUGAR.language.get(module_sugar_grp1, 'LBL_SERVICE_DISCOUNT');

  var d=x.insertCell(3);
  d.style.color="rgb(68,68,68)";
  d.innerHTML=SUGAR.language.get(module_sugar_grp1, 'LBL_SERVICE_PRICE');

  var e=x.insertCell(4);
  e.style.color="rgb(68,68,68)";
  e.innerHTML=SUGAR.language.get(module_sugar_grp1, 'LBL_VAT_AMT');

  var f=x.insertCell(5);
  f.style.color="rgb(68,68,68)";
  f.innerHTML=SUGAR.language.get(module_sugar_grp1, 'LBL_TOTAL_PRICE');

  var g=x.insertCell(6);
  g.style.color="rgb(68,68,68)";
  g.innerHTML='&nbsp;';
}

/**
 * Insert Group
 */

function insertGroup()
{

  if(!enable_groups && groupn > 0){
    return;
  }
  var tableBody = document.createElement("tr");
  tableBody.id = "group_body"+groupn;
  tableBody.className = "group_body";
  document.getElementById('lineItems').appendChild(tableBody);

  var a=tableBody.insertCell(0);
  a.colSpan="100";
  var table = document.createElement("table");
  table.id = "group"+groupn;
  table.className = "group";

  table.style.whiteSpace = 'nowrap';

  a.appendChild(table);



  tableheader = document.createElement("thead");
  table.appendChild(tableheader);
  var header_row=tableheader.insertRow(-1);


  if(enable_groups){
    var header_cell = header_row.insertCell(0);
    header_cell.scope="row";
    header_cell.colSpan="8";
    header_cell.innerHTML=SUGAR.language.get(module_sugar_grp1, 'LBL_GROUP_NAME')+":&nbsp;&nbsp;<input name='group_name[]' id='"+ table.id +"name' maxlength='255'  title='' tabindex='120' type='text' class='group_name'><input type='hidden' name='group_id[]' id='"+ table.id +"id' value=''><input type='hidden' name='group_group_number[]' id='"+ table.id +"group_number' value='"+groupn+"'>";

    var header_cell_del = header_row.insertCell(1);
    header_cell_del.scope="row";
    header_cell_del.colSpan="2";
    header_cell_del.innerHTML="<span title='" + SUGAR.language.get(module_sugar_grp1, 'LBL_DELETE_GROUP') + "' style='float: right;'><a style='cursor: pointer;' id='deleteGroup' tabindex='116' onclick='markGroupDeleted("+groupn+")' class='delete_group'><span class=\"suitepicon suitepicon-action-clear\"></span></a></span><input type='hidden' name='group_deleted[]' id='"+ table.id +"deleted' value='0'>";
  }



  var productTableHeader = document.createElement("thead");
  table.appendChild(productTableHeader);
  var productHeader_row=productTableHeader.insertRow(-1);
  var productHeader_cell = productHeader_row.insertCell(0);
  productHeader_cell.colSpan="100";
  var productTable = document.createElement("table");
  productTable.id = "product_group"+groupn;
  productTable.className = "product_group";
  productHeader_cell.appendChild(productTable);

  insertProductHeader(productTable.id);

  var serviceTableHeader = document.createElement("thead");
  table.appendChild(serviceTableHeader);
  var serviceHeader_row=serviceTableHeader.insertRow(-1);
  var serviceHeader_cell = serviceHeader_row.insertCell(0);
  serviceHeader_cell.colSpan="100";
  var serviceTable = document.createElement("table");
  serviceTable.id = "service_group"+groupn;
  serviceTable.className = "service_group";
  serviceHeader_cell.appendChild(serviceTable);

  insertServiceHeader(serviceTable.id);


  tablefooter = document.createElement("tfoot");
  table.appendChild(tablefooter);
  var footer_row=tablefooter.insertRow(-1);
  var footer_cell = footer_row.insertCell(0);
  footer_cell.scope="row";
  footer_cell.colSpan="20";
  footer_cell.innerHTML="<input type='button' tabindex='116' class='button add_product_line' value='"+SUGAR.language.get(module_sugar_grp1, 'LBL_ADD_PRODUCT_LINE')+"' id='"+productTable.id+"addProductLine' onclick='insertProductLine(\""+productTable.id+"\",\""+groupn+"\")' />";
  footer_cell.innerHTML+=" <input type='button' tabindex='116' class='button add_service_line' value='"+SUGAR.language.get(module_sugar_grp1, 'LBL_ADD_SERVICE_LINE')+"' id='"+serviceTable.id+"addServiceLine' onclick='insertServiceLine(\""+serviceTable.id+"\",\""+groupn+"\")' />";
  if(enable_groups){
    footer_cell.innerHTML+="<span class='totals'><label>"+SUGAR.language.get(module_sugar_grp1, 'LBL_TOTAL_AMT')+":</label><input name='group_total_amt[]' id='"+ table.id +"total_amt' class='group_total_amt' maxlength='26' value='' title='' tabindex='120' type='text' readonly></span>";

    var footer_row2=tablefooter.insertRow(-1);
    var footer_cell2 = footer_row2.insertCell(0);
    footer_cell2.scope="row";
    footer_cell2.colSpan="20";
    footer_cell2.innerHTML="<span class='totals'><label>"+SUGAR.language.get(module_sugar_grp1, 'LBL_DISCOUNT_AMOUNT')+":</label><input name='group_discount_amount[]' id='"+ table.id +"discount_amount' class='group_discount_amount' maxlength='26' value='' title='' tabindex='120' type='text' readonly></label>";

    var footer_row3=tablefooter.insertRow(-1);
    var footer_cell3 = footer_row3.insertCell(0);
    footer_cell3.scope="row";
    footer_cell3.colSpan="20";
    footer_cell3.innerHTML="<span class='totals'><label>"+SUGAR.language.get(module_sugar_grp1, 'LBL_SUBTOTAL_AMOUNT')+":</label><input name='group_subtotal_amount[]' id='"+ table.id +"subtotal_amount' class='group_subtotal_amount'  maxlength='26' value='' title='' tabindex='120' type='text' readonly></span>";

    var footer_row4=tablefooter.insertRow(-1);
    var footer_cell4 = footer_row4.insertCell(0);
    footer_cell4.scope="row";
    footer_cell4.colSpan="20";
    footer_cell4.innerHTML="<span class='totals'><label>"+SUGAR.language.get(module_sugar_grp1, 'LBL_TAX_AMOUNT')+":</label><input name='group_tax_amount[]' id='"+ table.id +"tax_amount' class='group_tax_amount' maxlength='26' value='' title='' tabindex='120' type='text' readonly></span>";

    if(document.getElementById('subtotal_tax_amount') !== null){
      var footer_row5=tablefooter.insertRow(-1);
      var footer_cell5 = footer_row5.insertCell(0);
      footer_cell5.scope="row";
      footer_cell5.colSpan="20";
      footer_cell5.innerHTML="<span class='totals'><label>"+SUGAR.language.get(module_sugar_grp1, 'LBL_SUBTOTAL_TAX_AMOUNT')+":</label><input name='group_subtotal_tax_amount[]' id='"+ table.id +"subtotal_tax_amount' class='group_subtotal_tax_amount' maxlength='26' value='' title='' tabindex='120' type='text' readonly></span>";

      if (typeof currencyFields !== 'undefined'){
        currencyFields.push("" + table.id+ 'subtotal_tax_amount');
      }
    }

    var footer_row6=tablefooter.insertRow(-1);
    var footer_cell6 = footer_row6.insertCell(0);
    footer_cell6.scope="row";
    footer_cell6.colSpan="20";
    footer_cell6.innerHTML="<span class='totals'><label>"+SUGAR.language.get(module_sugar_grp1, 'LBL_GROUP_TOTAL')+":</label><input name='group_total_amount[]' id='"+ table.id +"total_amount' class='group_total_amount'  maxlength='26' value='' title='' tabindex='120' type='text' readonly></span>";

    if (typeof currencyFields !== 'undefined'){
      currencyFields.push("" + table.id+ 'total_amt');
      currencyFields.push("" + table.id+ 'discount_amount');
      currencyFields.push("" + table.id+ 'subtotal_amount');
      currencyFields.push("" + table.id+ 'tax_amount');
      currencyFields.push("" + table.id+ 'total_amount');
    }
  }
  groupn++;
  return groupn -1;
}

/**
 * Mark Group Deleted
 */

function markGroupDeleted(gn)
{
  document.getElementById('group_body' + gn).style.display = 'none';

  var rows = document.getElementById('group_body' + gn).getElementsByTagName('tbody');

  for (x=0; x < rows.length; x++) {
    var input = rows[x].getElementsByTagName('button');
    for (y=0; y < input.length; y++) {
      if (input[y].id.indexOf('delete_line') != -1) {
		
        input[y].click();
      }
    }
  }
document.getElementById('group_body' + gn).remove();
}

/**
 * Mark line deleted
 */

function markLineDeleted(ln, key)
{
  // collapse line; update deleted value
  document.getElementById(key + 'body' + ln).style.display = 'none';
  document.getElementById(key + 'deleted' + ln).value = '1';
  document.getElementById(key + 'delete_line' + ln).onclick = '';
  var groupid = 'group' + document.getElementById(key + 'group_number' + ln).value;

  if(checkValidate('EditView',key+'product_id' +ln)){
    removeFromValidate('EditView',key+'product_id' +ln);
  }

  calculateTotal(groupid);
  calculateTotal();
   document.getElementById(key + 'body' + ln).remove();
}
function markLineDeleted2(ln,key,numOfZero)
{
  // collapse line; update deleted value
  document.getElementById(key + 'body' + ln).style.display = 'none';
  document.getElementById(key + 'deleted' + ln).value = '1';
  document.getElementById(key + 'delete_line' + ln).onclick = '';
  var groupid = 'group' + document.getElementById(key + 'group_number' + ln).value;

  if(checkValidate('EditView',key+'product_id' +ln)){
    removeFromValidate('EditView',key+'product_id' +ln);
  }

  calculateTotal2(numOfZero,groupid);
  calculateTotal2(numOfZero);
   document.getElementById(key + 'body' + ln).remove();
}

/**
 * Calculate Line Values
 */

function calculateLine(ln, key){

  var required = 'product_list_price';
  if(document.getElementById(key + required + ln) === null){
    required = 'product_unit_price';
  }

  if (document.getElementById(key + 'name' + ln).value === '' || document.getElementById(key + required + ln).value === ''){
    return;
  }

  if(key === "product_" && document.getElementById(key + 'product_qty' + ln) !== null && document.getElementById(key + 'product_qty' + ln).value === ''){
    document.getElementById(key + 'product_qty' + ln).value =1;
  }

  var productUnitPrice = unformat2Number(document.getElementById(key + 'product_unit_price' + ln).value);

  if(document.getElementById(key + 'product_list_price' + ln) !== null && document.getElementById(key + 'product_discount' + ln) !== null && document.getElementById(key + 'discount' + ln) !== null){
    var listPrice = get_value(key + 'product_list_price' + ln);
    var discount = get_value(key + 'product_discount' + ln);
    var dis = document.getElementById(key + 'discount' + ln).value;

    if(dis == 'Amount')
    {
      if(discount > listPrice)
      {
        document.getElementById(key + 'product_discount' + ln).value = listPrice;
        discount = listPrice;
      }
      productUnitPrice = listPrice - discount;
      document.getElementById(key + 'product_unit_price' + ln).value = format2Number(listPrice - discount);
    }
    else if(dis == 'Percentage')
    {
      if(discount > 100)
      {
        document.getElementById(key + 'product_discount' + ln).value = 100;
        discount = 100;
      }
      discount = (discount/100) * listPrice;
      productUnitPrice = listPrice - discount;
      document.getElementById(key + 'product_unit_price' + ln).value = format2Number(listPrice - discount);
    }
    else
    {
      document.getElementById(key + 'product_unit_price' + ln).value = document.getElementById(key + 'product_list_price' + ln).value;
      document.getElementById(key + 'product_discount' + ln).value = '';
      discount = 0;
    }
    document.getElementById(key + 'product_list_price' + ln).value = format2Number(listPrice);
    //document.getElementById(key + 'product_discount' + ln).value = format2Number(unformat2Number(document.getElementById(key + 'product_discount' + ln).value));
    document.getElementById(key + 'product_discount_amount' + ln).value = format2Number(-discount, 6);
  }

  var productQty = 1;
  if(document.getElementById(key + 'product_qty' + ln) !== null){
    productQty = unformat2Number(document.getElementById(key + 'product_qty' + ln).value);
    Quantity_format2Number(ln);
  }


  var vat = unformatNumber(document.getElementById(key + 'vat' + ln).value,',','.');

  var productTotalPrice = productQty * productUnitPrice;


  var totalvat=(productTotalPrice * vat) /100;

  if(total_tax){
    productTotalPrice=productTotalPrice + totalvat;
  }

  document.getElementById(key + 'vat_amt' + ln).value = format2Number(totalvat);

  document.getElementById(key + 'product_unit_price' + ln).value = format2Number(productUnitPrice);
  document.getElementById(key + 'product_total_price' + ln).value = format2Number(productTotalPrice);
  var groupid = 0;
  if(enable_groups){
    groupid = document.getElementById(key + 'group_number' + ln).value;
  }
  groupid = 'group' + groupid;

  calculateTotal(groupid);
  calculateTotal();

}

function calculateAllLines() {
  $('.product_group').each(function(productGroupkey, productGroupValue) {
      $(productGroupValue).find('tbody').each(function(productKey, productValue) {
        calculateLine(productKey, "product_");
      });
  });

  $('.service_group').each(function(serviceGroupkey, serviceGroupValue) {
    $(serviceGroupValue).find('tbody').each(function(serviceKey, serviceValue) {
      calculateLine(serviceKey, "service_");
    });
  });
}

/**
 * Calculate totals
 */
function calculateTotal(key)
{
  if (typeof key === 'undefined') {  key = 'lineItems'; }
  var row = document.getElementById(key).getElementsByTagName('tbody');
  if(key == 'lineItems') key = '';
  var length = row.length;
  var head = {};
  var tot_amt = 0;
  var subtotal = 0;
  var dis_tot = 0;
  var tax = 0;

  for (i=0; i < length; i++) {
    var qty = 1;
    var list = null;
    var unit = 0;
    var deleted = 0;
    var dis_amt = 0;
    var product_vat_amt = 0;

    var input = row[i].getElementsByTagName('input');
    for (j=0; j < input.length; j++) {
      if (input[j].id.indexOf('product_qty') != -1) {
        qty = unformat2Number(input[j].value);
      }
      if (input[j].id.indexOf('product_list_price') != -1)
      {
        list = unformat2Number(input[j].value);
      }
      if (input[j].id.indexOf('product_unit_price') != -1)
      {
        unit = unformat2Number(input[j].value);
      }
      if (input[j].id.indexOf('product_discount_amount') != -1)
      {
        dis_amt = unformat2Number(input[j].value);
      }
      if (input[j].id.indexOf('vat_amt') != -1)
      {
        product_vat_amt = unformat2Number(input[j].value);
      }
      if (input[j].id.indexOf('deleted') != -1) {
        deleted = input[j].value;
      }

    }

    if(deleted != 1 && key !== ''){
      head[row[i].parentNode.id] = 1;
    } else if(key !== '' && head[row[i].parentNode.id] != 1){
      head[row[i].parentNode.id] = 0;
    }

    if (qty !== 0 && list !== null && deleted != 1) {
      tot_amt += list * qty;
    } else if (qty !== 0 && unit !== 0 && deleted != 1) {
      tot_amt += unit * qty;
    }

    if (dis_amt !== 0 && deleted != 1) {
      dis_tot += dis_amt * qty;
    }
    if (product_vat_amt !== 0 && deleted != 1) {
      tax += product_vat_amt;
    }
  }

  for(var h in head){
    if (head[h] != 1 && document.getElementById(h + '_head') !== null) {
      document.getElementById(h + '_head').style.display = "none";
    }
  }

  subtotal = tot_amt + dis_tot;

  set_value(key+'total_amt',tot_amt);
  set_value(key+'subtotal_amount',subtotal);
  set_value(key+'discount_amount',dis_tot);

  var shipping = get_value(key+'shipping_amount');

  var shippingtax = get_value(key+'shipping_tax');

  var shippingtax_amt = shipping * (shippingtax/100);

  set_value(key+'shipping_tax_amt',shippingtax_amt);

  tax += shippingtax_amt;

  set_value(key+'tax_amount',tax);

  set_value(key+'subtotal_tax_amount',subtotal + tax);
  set_value(key+'total_amount',subtotal + tax + shipping);
}
function calculateTotal2(numOfZero,key = "")
{
  if (key == '') {  key = 'lineItems'; }
  var row = document.getElementById(key).getElementsByTagName('tbody');
  if(key == 'lineItems') key = '';
  var length = row.length;
  var head = {};
  var tot_amt = 0;
  var subtotal = 0;
  var dis_tot = 0;
  var tax = 0;

  for (i=0; i < length; i++) {
    var qty = 1;
    var list = null;
    var unit = 0;
    var deleted = 0;
    var dis_amt = 0;
    var product_vat_amt = 0;

    var input = row[i].getElementsByTagName('input');
    for (j=0; j < input.length; j++) {
      if (input[j].id.indexOf('product_qty') != -1) {
        qty = unformatNumber(input[j].value,',','.');
      }
      if (input[j].id.indexOf('product_list_price') != -1)
      {
        list = unformatNumber(input[j].value,',','.');
      }
      if (input[j].id.indexOf('product_unit_price') != -1)
      {
        unit = unformatNumber(input[j].value,',','.');
      }
      if (input[j].id.indexOf('product_discount_amount') != -1)
      {
        dis_amt = unformatNumber(input[j].value,',','.');
      }
      if (input[j].id.indexOf('vat_amt') != -1)
      {
        product_vat_amt = unformatNumber(input[j].value,',','.');
      }
      if (input[j].id.indexOf('deleted') != -1) {
        deleted = input[j].value;
      }

    }

    if(deleted != 1 && key !== ''){
      head[row[i].parentNode.id] = 1;
    } else if(key !== '' && head[row[i].parentNode.id] != 1){
      head[row[i].parentNode.id] = 0;
    }

    if (qty !== 0 && list !== null && deleted != 1) {
      tot_amt += list * qty;
    } else if (qty !== 0 && unit !== 0 && deleted != 1) {
      tot_amt += unit * qty;
    }

    if (dis_amt !== 0 && deleted != 1) {
      dis_tot += dis_amt * qty;
    }
    if (product_vat_amt !== 0 && deleted != 1) {
      tax += product_vat_amt;
    }
  }

  for(var h in head){
    if (head[h] != 1 && document.getElementById(h + '_head') !== null) {
      document.getElementById(h + '_head').style.display = "none";
    }
  }

  subtotal = tot_amt + dis_tot;
  set_value2(key+'total_amt',tot_amt,numOfZero);
  set_value2(key+'subtotal_amount',subtotal,numOfZero);
  set_value2(key+'discount_amount',dis_tot,numOfZero);

  var shipping = get_value(key+'shipping_amount',',','.');

  var shippingtax = get_value(key+'shipping_tax',',','.');

  var shippingtax_amt = shipping * (shippingtax/100);

  set_value2(key+'shipping_tax_amt',shippingtax_amt,numOfZero);

  tax += shippingtax_amt;

  set_value2(key+'tax_amount',tax,numOfZero);

  set_value2(key+'subtotal_tax_amount',subtotal + tax,numOfZero);
  set_value2(key+'total_amount',subtotal + tax + shipping,numOfZero);
}
function set_value(id, value){
  if(document.getElementById(id) !== null)
  {
    document.getElementById(id).value = changeCurrencyWithDotForSum(value);
	
  }
}
function set_value2(id, value,numOfZero){
  if(document.getElementById(id) !== null)
  {
    document.getElementById(id).value = changeCurrencyWithDotForSum(value,numOfZero);
  }
}

function get_value(id,a,b){
  if(document.getElementById(id) !== null)
  {
    return unformatNumber(document.getElementById(id).value,a,b);
  }
  return 0;
}


function unformat2Number(num)
{
  return unformatNumber(num, num_grp_sep, dec_sep);
}

function format2Number(str, sig)
{
  if (typeof sig === 'undefined') { sig = sig_digits; }
  num = Number(str);
  if(sig == 2){
    str = formatCurrency(num);
  }
  else{
    str = num.toFixed(sig);
  }

  str = str.split(/,/).join('{,}').split(/\./).join('{.}');
  str = str.split('{,}').join(num_grp_sep).split('{.}').join(dec_sep);

  return str;
}

function formatCurrency(strValue)
{
	if(strValue != null){
	  strValue = strValue.toString().replace(/\$|\,/g,'');
	  dblValue = parseFloat(strValue);

	  blnSign = (dblValue == (dblValue = Math.abs(dblValue)));
	  dblValue = Math.floor(dblValue*100+0.50000000001);
	  intCents = dblValue%100;
	  strCents = intCents.toString();
	  dblValue = Math.floor(dblValue/100).toString();
	  if(intCents<10)
		strCents = "0" + strCents;
	  for (var i = 0; i < Math.floor((dblValue.length-(1+i))/3); i++)
		dblValue = dblValue.substring(0,dblValue.length-(4*i+3))+','+
		  dblValue.substring(dblValue.length-(4*i+3));
	  return (((blnSign)?'':'-') + dblValue + '.' + strCents);
	}
}

function Quantity_format2Number(ln)
{
  var str = '';
  var qty=unformat2Number(document.getElementById('product_product_qty' + ln).value);
  if(qty === null){qty = 1;}

  if(qty === 0){
    str = '0';
  } else {
    str = format2Number(qty);
    if(sig_digits){
      str = str.replace(/0*$/,'');
      str = str.replace(dec_sep,'~');
      str = str.replace(/~$/,'');
      str = str.replace('~',dec_sep);
    }
  }

  document.getElementById('product_product_qty' + ln).value=str;
}

function formatNumber(n, num_grp_sep, dec_sep, round, precision) {
  if (typeof num_grp_sep == "undefined" || typeof dec_sep == "undefined") {
    return n;
  }
  if(n === 0) n = '0';

  n = n ? n.toString() : "";
  if (n.split) {
    n = n.split(".");
  } else {
    return n;
  }
  if (n.length > 2) {
    return n.join(".");
  }
  if (typeof round != "undefined") {
    if (round > 0 && n.length > 1) {
      n[1] = parseFloat("0." + n[1]);
      n[1] = Math.round(n[1] * Math.pow(10, round)) / Math.pow(10, round);
      if(n[1].toString().includes('.')) {
      n[1] = n[1].toString().split(".")[1];
    }
      else {
	  n[0] = (parseInt(n[0]) + n[1]).toString();
	  n[1] = "";
      }
    }
    if (round <= 0) {
      n[0] = Math.round(parseInt(n[0], 10) * Math.pow(10, round)) / Math.pow(10, round);
      n[1] = "";
    }
  }
  if (typeof precision != "undefined" && precision >= 0) {
    if (n.length > 1 && typeof n[1] != "undefined") {
      n[1] = n[1].substring(0, precision);
    } else {
      n[1] = "";
    }
    if (n[1].length < precision) {
      for (var wp = n[1].length; wp < precision; wp++) {
        n[1] += "0";
      }
    }
  }
  regex = /(\d+)(\d{3})/;
  while (num_grp_sep !== "" && regex.test(n[0])) {
    n[0] = n[0].toString().replace(regex, "$1" + num_grp_sep + "$2");
  }
  return n[0] + (n.length > 1 && n[1] !== "" ? dec_sep + n[1] : "");
}

function check_form(formname) {
  calculateAllLines();
  if (typeof(siw) != 'undefined' && siw && typeof(siw.selectingSomething) != 'undefined' && siw.selectingSomething)
    return false;
  return validate_form(formname, '');
}
//
SUGAR.loaded_once = false;

$(document).ready(function () {
  loadSidebar();
  $("ul.clickMenu").each(function (index, node) {
    $(node).sugarActionMenu();
  });
  // Back to top animation
  $('#backtotop').click(function (event) {
    event.preventDefault();
    $('html, body').animate({scrollTop: 0}, 500); // Scroll speed to the top
  });
});
YAHOO.util.Event.onAvailable('sitemapLinkSpan', function () {
  document.getElementById('sitemapLinkSpan').onclick = function () {
    ajaxStatus.showStatus(SUGAR.language.get('app_strings', 'LBL_LOADING_PAGE'));
    var smMarkup = '';
    var callback = {
      success: function (r) {
        ajaxStatus.hideStatus();
        document.getElementById('sm_holder').innerHTML = r.responseText;
        with (document.getElementById('sitemap').style) {
          display = "block";
          position = "absolute";
          right = 0;
          top = 80;
        }
        document.getElementById('sitemapClose').onclick = function () {
          document.getElementById('sitemap').style.display = "none";
        }
      }
    }
    postData = 'module=Home&action=sitemap&GetSiteMap=now&sugar_body_only=true';
    YAHOO.util.Connect.asyncRequest('POST', 'index.php', callback, postData);
  }
});
function checkSendZalo(ele){
	if(ele.checked == true){
		localStorage.setItem("sendAll","true");
		
	}else{
		localStorage.setItem("sendAll","false");
		
	}
	
}


function filterKey() {
  var input, filter, ul, li, a, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  div = document.getElementById("group-users");
  a = div.getElementsByTagName("option");
  for (i = 0; i < a.length; i++) {
    txtValue = a[i].textContent || a[i].innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      a[i].style.display = "";
    } else {
      a[i].style.display = "none";
    }
  }
}
function filterKeyy() {
  var input, filter, ul, li, a, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  div = document.getElementById("group-users");
  a = div.getElementsByTagName("div");
  for (i = 0; i < a.length; i++) {
    txtValue = a[i].textContent || a[i].innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      a[i].style.display = "";
    } else {
      a[i].style.display = "none";
    }
  }
}
function filterKeyAssign(i){
  var input, filter, ul, li, a, i;
  input = document.getElementById("assigned_user_name"+i);
  filter = input.value.toUpperCase();
  div = document.getElementById("userAssignDrop"+i);
  a = div.getElementsByTagName("option");
  for (i = 0; i < a.length; i++) {
    txtValue = a[i].textContent || a[i].innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      a[i].style.display = "";
    } else {
      a[i].style.display = "none";
    }
  }
}
function sendbulkZalo(module){
	 sugarListView.get_checks();
    var sms_ids = document.MassUpdate.uid.value;
	document.getElementsByClassName("modal-title")[5].innerHTML = "<b>Sending Bulk ZALO</b>"
    $('#BULK').modal('toggle');
    $.ajax({
        url: 'index.php?entryPoint=CE_SMS_Options',
        type: 'POST',
        data: {action: 'get_smsbulk_body', modulefrom: module, recid: sms_ids},
        success: function (get_body) {
            //document.getElementById("des").value = errorResponse;
            //alert(get_body);
            $("#bulk_dashboardDialog").empty();
            $("#bulk_dashboardDialog").append(get_body);
			$("#bulksubmit").attr("id","bulksubmit2");
        }
    });
}
function showMoreAssigned(){
	//alert("aaaA");
    var html=document.createElement('div');
    var more = document.getElementById("showMoreAssign").innerHTML;
    var count = document.getElementById("showMoreAssign").childElementCount;
    html.innerHTML='<div id="assignedForm'+eval(count+1)+'"><input type="text" name="assigned_user_name" onkeyup="filterKeyAssign('+eval(count+1)+')" class="sqsEnabled yui-ac-input" tabindex="0" id="assigned_user_name'+eval(count+1)+'" size="" placeholder="thêm user Id" title="" autocomplete="off" onclick = "showUserAssi('+eval(count+1)+')"><span><button type="button" id="removeButton'+eval(count+1)+'" class="btn btn-danger email-address-remove-button" onclick = "removeUserAssi('+eval(count+1)+')"><span class="suitepicon suitepicon-action-minus"></span></button></span></div><div id="userAssign'+eval(count+1)+'"></div>';
    
   document.getElementById("showMoreAssign").append(html);
}

function showUserAssi(a){
	$.ajax({
                type: "POST",
                url: "index.php?entryPoint=showUserAssi",
                dataType: "json",
                success: function(data) {
					var length=data.length;
					var html='<select multiple="multiple" id="userAssignDrop'+a+'" style="width:180px;height:180px;z-index:1000;position:absolute">';
					for(var i=0;i<length;i++){
					html+='<option value="'+data[i].id+'">'+data[i].name+'</option>';
					}
					html+='</select>';
					document.getElementById("userAssign"+a).style.display="block";
					document.getElementById("userAssign"+a).innerHTML=html;
						$("#assigned_user_name"+a).on("blur",function(){
						setTimeout(function(){
						$("#userAssign"+a).css('display','none');
						},150);
						});
						
						$("#userAssignDrop"+a).on("change",function(){
							var vall=$("#userAssignDrop"+a).find(":selected").text();
							$("#assigned_user_name"+a).val(vall);
						});
                },
                error: function (error) {
                    
                }
            });
}
function removeUserAssi(a){
	document.getElementById("assignedForm"+a).remove();
	document.getElementById("userAssign"+a).remove();
}
function setAllowView(){
	var advanced = GetURLParameter("advanced");
	if(advanced != null && advanced != undefined){
		var allowAll = localStorage.getItem("allowAll");
		if(allowAll != null && allowAll != undefined){
		var id = GetURLParameter("saved_search_select");
			$.ajax({
				type: "POST",
				url: "index.php?entryPoint=setAllowView",
				data: {
					id:id,
					allow:allowAll
				},
				success: function(res) {
				localStorage.removeItem("allowAll");
				}
			});
		}
	}
}
function setView(){
	$('#allowAll').on("click",function(){
		if(allowAll.checked == true){
			localStorage.setItem("allowAll","1");
		}else{
			localStorage.setItem("allowAll","0");
		}
	});
}
function loadProvince2(id){
	$.ajax({
                type: "POST",
                url: "index.php?entryPoint=loadProvince2",
                data:{
					id:id
				},
                dataType: "json",
                success: function(data) {
					$("#provincecode").val(data) ;
					$('#provincecode option[value="'+data+'"]').attr('selected','selected');
					loadDynamicEnum("provincecode","districtcode_c");
					//$("#provincecode").prop('disabled', true);
					$("#provincecode").on("change",function(){
						//$("#provincecode").val(data) ;
						loadDynamicEnum("provincecode","districtcode_c");
						setTimeout(function(){
						$.ajax({
							type: "POST",
							url: "index.php?entryPoint=getGroup",
							data: {
								id:$("#provincecode").val()
							},
							dataType: "json",
							success: function(result) {
								var obj=result;
								html="";
								html2="";
								html+='<option selected value=""></option>';
								for(let i=0;i<obj.length;i++){
									html+='<option value="'+obj[i].name+'">'+obj[i].name+'</option>';
									html2+='<option value="'+obj[i].id+'">'+obj[i].name+'</option>';
								}
								
								$('#groupname').html(html);
								$('#groupname2').html(html2);
								$(document).ready(function (){
									$("#groupname").on("change",function(){
										var grp=$('#groupname').val();
										$.ajax({
											type: "POST",
											url: "index.php?entryPoint=getIdGroup",
											data: {
												name:grp
											},
											dataType: "json",
											success: function(res) {
												$("#groupid").val(res.id);
										}
										});
									});
									
								});
							},
							failure: function() {
							   // SUGAR.ajaxUI.hideLoadingPanel();
							}
			});
				},400);	
						
					});
					setTimeout(function(){
					var provinceVal = $("#provincecode").val();
		if(provinceVal != null && provinceVal != ''){
			$.ajax({
							type: "POST",
							url: "index.php?entryPoint=getGroup",
							data: {
								id:$("#provincecode").val()
							},
							dataType: "json",
							success: function(result) {
								var obj=result;
								html="";
								html2="";
								html+='<option selected value=""></option>';
								for(let i=0;i<obj.length;i++){
									html+='<option value="'+obj[i].name+'">'+obj[i].name+'</option>';
									html2+='<option value="'+obj[i].id+'">'+obj[i].name+'</option>';
								}
								
								$('#groupname').html(html);
								$('#groupname2').html(html2);
								$(document).ready(function (){
									$("#groupname").on("change",function(){
										var grp=$('#groupname').val();
										$.ajax({
											type: "POST",
											url: "index.php?entryPoint=getIdGroup",
											data: {
												name:grp
											},
											dataType: "json",
											success: function(res) {
												$("#groupid").val(res.id);
										}
										});
									});
									
								});
							},
							failure: function() {
							   // SUGAR.ajaxUI.hideLoadingPanel();
							}
			});
		}
				},400);	
                },
                error: function (error) {
                    
                }
            });
	     		
}
function loadProvince4(){
	$.ajax({
                type: "POST",
                url: "index.php?entryPoint=loadProvince",
                
                dataType: "json",
                success: function(data) {
					$("#provincecode").val(data) ;
					$('#provincecode option[value="'+data+'"]').attr('selected','selected');
					loadDynamicEnum("provincecode","districtcode_c");
					//$("#provincecode").prop('disabled', true);
					$("#provincecode").on("change",function(){
						//$("#provincecode").val(data) ;
					//	loadDynamicEnum("provincecode","districtcode_c");
						var provinceVal = $("#provincecode").val();
						if(provinceVal != null && provinceVal != ''){
							$.ajax({
								type: "POST",
								url: "index.php?entryPoint=getGroup",
								data: {
									id:$("#provincecode").val()
								},
								dataType: "json",
								success: function(result) {
									var obj=result;
									html="";
									html2="";
									html+='<option selected value=""></option>';
									for(let i=0;i<obj.length;i++){
										html+='<option value="'+obj[i].name+'">'+obj[i].name+'</option>';
										html2+='<option value="'+obj[i].id+'">'+obj[i].name+'</option>';
									}
									
									$('#groupname').html(html);
									$('#groupname2').html(html2);
									$(document).ready(function (){
										$("#groupname").on("change",function(){
											var grp=$('#groupname').val();
											$.ajax({
												type: "POST",
												url: "index.php?entryPoint=getIdGroup",
												data: {
													name:grp
												},
												dataType: "json",
												success: function(res) {
													$("#groupid").val(res.id);
											}
											});
										});
										
									});
								},
								failure: function() {
								   // SUGAR.ajaxUI.hideLoadingPanel();
								}
							});
						}else{
							$('#groupname').html("");
							$('#groupname2').html("");
							
						}
					});
					setTimeout(function(){
					var provinceVal = $("#provincecode").val();
		if(provinceVal != null && provinceVal != ''){
			$.ajax({
							type: "POST",
							url: "index.php?entryPoint=getGroup",
							data: {
								id:$("#provincecode").val()
							},
							dataType: "json",
							success: function(result) {
								var obj=result;
								html="";
								html2="";
								html+='<option selected value=""></option>';
								for(let i=0;i<obj.length;i++){
									html+='<option value="'+obj[i].name+'">'+obj[i].name+'</option>';
									html2+='<option value="'+obj[i].id+'">'+obj[i].name+'</option>';
								}
								
								$('#groupname').html(html);
								$('#groupname2').html(html2);
								$(document).ready(function (){
									$("#groupname").on("change",function(){
										var grp=$('#groupname').val();
										$.ajax({
											type: "POST",
											url: "index.php?entryPoint=getIdGroup",
											data: {
												name:grp
											},
											dataType: "json",
											success: function(res) {
												$("#groupid").val(res.id);
										}
										});
									});
									
								});
							},
							failure: function() {
							   // SUGAR.ajaxUI.hideLoadingPanel();
							}
			});
		}
				},400);	
                },
                error: function (error) {
                    
                }
            });
	     		
}
function filterKey2() {
  var input, filter, ul, li, a, i;
  input = document.getElementById("myInput2");
  filter = input.value.toUpperCase();
  div = document.getElementById("users");
  a = div.getElementsByTagName("option");
  for (i = 0; i < a.length; i++) {
    txtValue = a[i].textContent || a[i].innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      a[i].style.display = "";
    } else {
      a[i].style.display = "none";
    }
  }
}
function filterKey3() {
  var input, filter, ul, li, a, i;
  input = document.getElementById("myInput3");
  filter = input.value.toUpperCase();
  div = document.getElementById("title-users");
  a = div.getElementsByTagName("option");
  for (i = 0; i < a.length; i++) {
    txtValue = a[i].textContent || a[i].innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      a[i].style.display = "";
    } else {
      a[i].style.display = "none";
    }
  }
}
function filterKey4() {
  var input, filter, ul, li, a, i;
  input = document.getElementById("myInput4");
  filter = input.value.toUpperCase();
  div = document.getElementById("lead_source_advanced");
  a = div.getElementsByTagName("option");
  for (i = 0; i < a.length; i++) {
    txtValue = a[i].textContent || a[i].innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      a[i].style.display = "";
    } else {
      a[i].style.display = "none";
    }
  }
}
function filterKey5() {
  var input, filter, ul, li, a, i;
  input = document.getElementById("myInput5");
  filter = input.value.toUpperCase();
  div = document.getElementById("assigned_user_id_advanced");
  a = div.getElementsByTagName("option");
  for (i = 0; i < a.length; i++) {
    txtValue = a[i].textContent || a[i].innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      a[i].style.display = "";
    } else {
      a[i].style.display = "none";
    }
  }
}
function filterKey6() {
  var input, filter, ul, li, a, i;
  input = document.getElementById("myInput6");
  filter = input.value.toUpperCase();
  div = document.getElementsByName("modified_user_id_advanced[]")[0];
  a = div.getElementsByTagName("option");
  for (i = 0; i < a.length; i++) {
    txtValue = a[i].textContent || a[i].innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      a[i].style.display = "";
    } else {
      a[i].style.display = "none";
    }
  }
}
function filterKey7() {
  var input, filter, ul, li, a, i;
  input = document.getElementById("myInput7");
  filter = input.value.toUpperCase();
  div = document.getElementById("industry_advanced");
  a = div.getElementsByTagName("option");
  for (i = 0; i < a.length; i++) {
    txtValue = a[i].textContent || a[i].innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      a[i].style.display = "";
    } else {
      a[i].style.display = "none";
    }
  }
}
function filterKey8() {
  var input, filter, ul, li, a, i;
  input = document.getElementById("myInput8");
  filter = input.value.toUpperCase();
  div = document.getElementById("leadsource_c_advanced");
  a = div.getElementsByTagName("option");
  for (i = 0; i < a.length; i++) {
    txtValue = a[i].textContent || a[i].innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      a[i].style.display = "";
    } else {
      a[i].style.display = "none";
    }
  }
}
function filterKey11() {
  var input, filter, ul, li, a, i;
  input = document.getElementById("myInput11");
  filter = input.value.toUpperCase();
  div = document.getElementsByName("created_by_advanced[]")[0];
  a = div.getElementsByTagName("option");
  for (i = 0; i < a.length; i++) {
    txtValue = a[i].textContent || a[i].innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      a[i].style.display = "";
    } else {
      a[i].style.display = "none";
    }
  }
}
function delAttachment(elemBaseName, docTypeName, elem) {
			var x=GetURLParameter('record');
			if(x == null || x == undefined){
				x=document.getElementsByName("record")[0].value;
			}
            ajaxStatus.showStatus(SUGAR.language.get("app_strings", "LBL_REMOVING_ATTACHMENT"));
            elem.form.deleteAttachment.value = 1;
            elem.form.action.value = "deleteattachment";
            var callback = SUGAR.field.file.deleteAttachmentCallbackGen(elemBaseName, docTypeName);
            var success = function(data) {
                if (data) {
                    callback(data.responseText);
                }
            }
            YAHOO.util.Connect.setForm(elem.form);
            var cObj = YAHOO.util.Connect.asyncRequest("POST", "index.php?entryPoint=deleteAttachment&filename_record_id="+x+"&field=" + elemBaseName, {
                success: success,
                failure: success
            });
            elem.form.deleteAttachment.value = 0;
            elem.form.action.value = "";
}
function IKEADEBUG() {
  var moduleLinks = document.getElementById('moduleList').getElementsByTagName("a");
  moduleLinkMouseOver = function () {
    var matches = /grouptab_([0-9]+)/i.exec(this.id);
    var tabNum = matches[1];
    var moduleGroups = document.getElementById('subModuleList').getElementsByTagName("span");
    for (var i = 0; i < moduleGroups.length; i++) {
      if (i == tabNum) {
        moduleGroups[i].className = 'selected';
      }
      else {
        moduleGroups[i].className = '';
      }
    }
    var groupList = document.getElementById('moduleList').getElementsByTagName("li");
    var currentGroupItem = tabNum;
    for (var i = 0; i < groupList.length; i++) {
      var aElem = groupList[i].getElementsByTagName("a")[0];
      if (aElem == null) {
        continue;
      }
      var classStarter = 'notC';
      if (aElem.id == "grouptab_" + tabNum) {
        classStarter = 'c';
        currentGroupItem = i;
      }
      var spanTags = groupList[i].getElementsByTagName("span");
      for (var ii = 0; ii < spanTags.length; ii++) {
        if (spanTags[ii].className == null) {
          continue;
        }
        var oldClass = spanTags[ii].className.match(/urrentTab.*/);
        spanTags[ii].className = classStarter + oldClass;
      }
    }
    var menuHandle = moduleGroups[tabNum];
    var parentMenu = groupList[currentGroupItem];
    if (menuHandle && parentMenu) {
      updateSubmenuPosition(menuHandle, parentMenu);
    }
  };
  for (var i = 0; i < moduleLinks.length; i++) {
    moduleLinks[i].onmouseover = moduleLinkMouseOver;
  }
};
function updateSubmenuPosition(menuHandle, parentMenu) {
  var left = '';
  if (left == "") {
    p = parentMenu;
    var left = 0;
    while (p && p.tagName.toUpperCase() != 'BODY') {
      left += p.offsetLeft;
      p = p.offsetParent;
    }
  }
  var bw = checkBrowserWidth();
  if (!parentMenu) {
    return;
  }
  var groupTabLeft = left + (parentMenu.offsetWidth / 2);
  var subTabHalfLength = 0;
  var children = menuHandle.getElementsByTagName('li');
  for (var i = 0; i < children.length; i++) {
    if (children[i].className == 'subTabMore' || children[i].parentNode.className == 'cssmenu') {
      continue;
    }
    subTabHalfLength += parseInt(children[i].offsetWidth);
  }
  if (subTabHalfLength != 0) {
    subTabHalfLength = subTabHalfLength / 2;
  }
  var totalLengthInTheory = subTabHalfLength + groupTabLeft;
  if (subTabHalfLength > 0 && groupTabLeft > 0) {
    if (subTabHalfLength >= groupTabLeft) {
      left = 1;
    } else {
      left = groupTabLeft - subTabHalfLength;
    }
  }
  if (totalLengthInTheory > bw) {
    var differ = totalLengthInTheory - bw;
    left = groupTabLeft - subTabHalfLength - differ - 2;
  }
  if (left >= 0) {
    menuHandle.style.marginLeft = left + 'px';
  }
}
YAHOO.util.Event.onDOMReady(function () {
  if (document.getElementById('subModuleList')) {
    var parentMenu = false;
    var moduleListDom = document.getElementById('moduleList');
    if (moduleListDom != null) {
      var parentTabLis = moduleListDom.getElementsByTagName("li");
      var tabNum = 0;
      for (var ii = 0; ii < parentTabLis.length; ii++) {
        var spans = parentTabLis[ii].getElementsByTagName("span");
        for (var jj = 0; jj < spans.length; jj++) {
          if (spans[jj].className.match(/currentTab.*/)) {
            tabNum = ii;
          }
        }
      }
      var parentMenu = parentTabLis[tabNum];
    }
    var moduleGroups = document.getElementById('subModuleList').getElementsByTagName("span");
    for (var i = 0; i < moduleGroups.length; i++) {
      if (moduleGroups[i].className.match(/selected/)) {
        tabNum = i;
      }
    }
    var menuHandle = moduleGroups[tabNum];
    if (menuHandle && parentMenu) {
      updateSubmenuPosition(menuHandle, parentMenu);
    }
  }
});
SUGAR.themes = SUGAR.namespace("themes");
SUGAR.append(SUGAR.themes, {
  allMenuBars: {}, setModuleTabs: function (html) {
    var el = document.getElementById('ajaxHeader');
    if (el) {
      $('#ajaxHeader').html(html);
      loadSidebar();
      if ($(window).width() < 979) {
        $('#bootstrap-container').removeClass('main');
      }
    }
  }, actionMenu: function () {
    $("ul.clickMenu").each(function (index, node) {
      $(node).sugarActionMenu();
    });
  }, loadModuleList: function () {
    var nodes = YAHOO.util.Selector.query('#moduleList>div'), currMenuBar;
    this.allMenuBars = {};
    for (var i = 0; i < nodes.length; i++) {
      currMenuBar = SUGAR.themes.currMenuBar = new YAHOO.widget.MenuBar(nodes[i].id, {
        autosubmenudisplay: true,
        visible: false,
        hidedelay: 750,
        lazyload: true
      });
      currMenuBar.render();
      this.allMenuBars[nodes[i].id.substr(nodes[i].id.indexOf('_') + 1)] = currMenuBar;
      if (typeof YAHOO.util.Dom.getChildren(nodes[i]) == 'object' && YAHOO.util.Dom.getChildren(nodes[i]).shift().style.display != 'none') {
        oMenuBar = currMenuBar;
      }
    }
    YAHOO.util.Event.onAvailable('subModuleList', IKEADEBUG);
  }, setCurrentTab: function () {
  }
});
YAHOO.util.Event.onDOMReady(SUGAR.themes.loadModuleList, SUGAR.themes, true);


// Custom jQuery for theme

// Script to toggle copyright popup
$("button").click(function () {
  $("#sugarcopy").toggle();

});

var initFooterPopups = function () {
  $("#dialog, #dialog2").dialog({
    autoOpen: false,
    show: {
      effect: "blind",
      duration: 100
    },
    hide: {
      effect: "fade",
      duration: 1000
    }
  });
  $("#powered_by").click(function () {
    $("#dialog").dialog("open");
    $("#overlay").show().css({"opacity": "0.5"});
  });
  $("#admin_options").click(function () {
    $("#dialog2").dialog("open");
  });
};

// Custom JavaScript for copyright pop-ups
$(function () {
  initFooterPopups();
});

// Back to top animation
$('#backtotop').click(function (event) {
  event.preventDefault();
  $('html, body').animate({scrollTop: 0}, 500); // Scroll speed to the top
});

// Tabs jQuery for Admin panel
$(function () {
  var tabs = $("#tabs").tabs();
  tabs.find(".ui-tabs-nav").sortable({
    axis: "x",
    stop: function () {
      tabs.tabs("refresh");
    }
  });
});


// JavaScript fix to remove unrequired classes on smaller screens where sidebar is obsolete
$(window).resize(function () {
  if ($(window).width() < 979) {
    $('#bootstrap-container').removeClass('col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 sidebar main');
  }
  if ($(window).width() > 980 && $('.sidebar').is(':visible')) {
    $('#bootstrap-container').addClass('col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main');
  }
});

// jQuery to toggle sidebar
function loadSidebar() {
  if ($('#sidebar_container').length) {
    $('#buttontoggle').click(function () {
      $('.sidebar').toggle();
      if ($('.sidebar').is(':visible')) {
        $.cookie('sidebartoggle', 'expanded');
        $('#buttontoggle').removeClass('button-toggle-collapsed');
        $('#buttontoggle').addClass('button-toggle-expanded');
        $('#bootstrap-container').addClass('col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2');
        $('footer').removeClass('collapsedSidebar');
        $('footer').addClass('expandedSidebar');
        $('#bootstrap-container').removeClass('collapsedSidebar');
        $('#bootstrap-container').addClass('expandedSidebar');
      }
      if ($('.sidebar').is(':hidden')) {
        $.cookie('sidebartoggle', 'collapsed');
        $('#buttontoggle').removeClass('button-toggle-expanded');
        $('#buttontoggle').addClass('button-toggle-collapsed');
        $('#bootstrap-container').removeClass('col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 col-sm-3 col-md-2 sidebar');
        $('footer').removeClass('expandedSidebar');
        $('footer').addClass('collapsedSidebar');
        $('#bootstrap-container').removeClass('expandedSidebar');
        $('#bootstrap-container').addClass('collapsedSidebar');
      }
    });

    var sidebartoggle = $.cookie('sidebartoggle');
    if (sidebartoggle == 'collapsed') {
      $('.sidebar').hide();
      $('#buttontoggle').removeClass('button-toggle-expanded');
      $('#buttontoggle').addClass('button-toggle-collapsed');
      $('#bootstrap-container').removeClass('col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 col-sm-3 col-md-2 sidebar');
      $('footer').removeClass('expandedSidebar');
      $('footer').addClass('collapsedSidebar');
      $('#bootstrap-container').removeClass('expandedSidebar');
      $('#bootstrap-container').addClass('collapsedSidebar');
    }
    else {
      $('#bootstrap-container').addClass('col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2');
      $('#buttontoggle').removeClass('button-toggle-collapsed');
      $('#buttontoggle').addClass('button-toggle-expanded');
      $('footer').removeClass('collapsedSidebar');
      $('footer').addClass('expandedSidebar');
      $('#bootstrap-container').removeClass('collapsedSidebar');
      $('#bootstrap-container').addClass('expandedSidebar');
    }
  }
}

// Alerts Notification
$(document).ready(function () {
  $('#alert-nav').click(function () {
    $('#alert-nav #alerts').css('display', 'inherit');
  });
});

function selectTab(tab) {
  $('#content div.tab-content div.tab-pane-NOBOOTSTRAPTOGGLER').hide();
  $('#content div.tab-content div.tab-pane-NOBOOTSTRAPTOGGLER').eq(tab).show().addClass('active').addClass('in');
};

function changeFirstTab(src) {
  var selected = $(src).attr('id');
  var selectedHtml = $(selected.context).html();
  $('#xstab0').html(selectedHtml);

    var i = $(src).parents('li').index();
    selectTab(parseInt(i));
    return true;
}
// End of custom jQuery


// fix for tab navigation on user profile for SuiteP theme

var getParameterByName = function (name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
    results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, " "));
}
var isUserProfilePage = function () {
  var module = getParameterByName('module');
  if (!module) {
    module = $('#EditView_tabs').closest('form#EditView').find('input[name="module"]').val();
  }
  if (!module) {
    if (typeof module_sugar_grp1 !== "undefined") {
      module = module_sugar_grp1;
    }
  }
  return module == 'Users';
};

var isEditViewPage = function () {
  var action = getParameterByName('action');
  if (!action) {
    action = $('#EditView_tabs').closest('form#EditView').find('input[name="page"]').val();
  }
  return action == 'EditView';
};

var isDetailViewPage = function () {
  var action = getParameterByName('action');
  if (!action) {
    action = action_sugar_grp1;
  }
  return action == 'DetailView';
};

var refreshListViewCheckbox = function (e) {
  $(e).removeClass('glyphicon-check');
  $(e).removeClass('glyphicon-unchecked');
  if ($(e).next().prop('checked')) {
    $(e).addClass('glyphicon-check');
  }
  else {
    $(e).addClass('glyphicon-unchecked');
  }
  $(e).removeClass('disabled')
  if ($(e).next().prop('disabled')) {
    $(e).addClass('disabled')
  }
};




$(function () {
  // Fix for footer position
  if ($('#bootstrap-container footer').length > 0) {
    var clazz = $('#bootstrap-container footer').attr('class');
    $('body').append('<footer class="' + clazz + '">' + $('#bootstrap-container footer').html() + '</footer>');
    $('#bootstrap-container footer').remove();
    initFooterPopups();
  }



  var hideEmptyFormCellsOnTablet = function () {
    if ($(window).width() <= 767) {
      $('div#content div#pagecontent form#EditView div.edit.view table tbody tr td').each(function (i, e) {
        $(e).find('slot').each(function (i, e) {
          if ($(e).html().trim() == '&nbsp;') {
            $(e).html('&nbsp;');
          }
        });
        if ($(e).html().trim() == '<span>&nbsp;</span>') {
          $(e).addClass('hidden');
          $(e).addClass('hiddenOnTablet');
        }
      });
    }
    else {
      $('div#content div#pagecontent form#EditView div.edit.view table tbody tr td.hidden.hiddenOnTablet').each(function (i, e) {
        $(e).removeClass('hidden');
        $(e).removeClass('hiddenOnTablet');
      });
    }
  }

  $(window).click(function () {
    hideEmptyFormCellsOnTablet();
    setTimeout(function () {
      hideEmptyFormCellsOnTablet();
    }, 500);
  });

  $(window).resize(function () {
    hideEmptyFormCellsOnTablet();
  });

  $(window).load(function () {
    hideEmptyFormCellsOnTablet();
  });

function GetURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}


  $(document).ready(function () {
    hideEmptyFormCellsOnTablet();
	var modu = GetURLParameter("module");
	var act = GetURLParameter("action");
	if(modu == "Users" && act == "EditView"){
	$( '#department' ).after( '<div style="display:none;position:absolute;width:280px;height:180px;z-index:10" id="grp"></div>' );
	$("#department").on("click",function(){
		$('#grp').css('display','block');
	});
	$("#department").on("blur",function(){
		setTimeout(function(){ 
			$('#grp').css('display','none');
		}, 170);
	});
	$.ajax({
						type: "POST",
						url: "index.php?entryPoint=getGroup",
						data: {
							id:$("#provincecode_c").val()
						},
						dataType: "json",
						success: function(result) {
							
							var obj=result;
							html="";
							html+='<select style="width:280px;height:180px;z-index:10" id="grp-user" multiple="multiple">';
							for(let i=0;i<obj.length;i++){
								html+='<option value="'+obj[i].name+'">'+obj[i].name+'</option>';
							}
							html+='</select>';
							$('#grp').html(html);
							$(document).ready(function (){
								$("#grp-user").on("change",function(){
									var grp=$('#grp-user').val();
									$('#department').val(grp[0]);
									$('#grp').css('display','none');
								});
							});
						},
						failure: function() {
						   // SUGAR.ajaxUI.hideLoadingPanel();
						}
	});
	$("#provincecode_c").on("change",function(){
					$.ajax({
						type: "POST",
						url: "index.php?entryPoint=getGroup",
						data: {
							id:$("#provincecode_c").val()
						},
						dataType: "json",
						success: function(result) {
							var obj=result;
							html="";
							html+='<select style="width:280px;height:180px;z-index:10" id="grp-user" multiple="multiple">';
							for(let i=0;i<obj.length;i++){
								html+='<option value="'+obj[i].name+'">'+obj[i].name+'</option>';
							}
							html+='</select>';
							$('#grp').html(html);
							$(document).ready(function (){
								$("#grp-user").on("change",function(){
									var grp=$('#grp-user').val();
									$('#department').val(grp[0]);
									$('#grp').css('display','none');
								});
							});
						},
						failure: function() {
						   // SUGAR.ajaxUI.hideLoadingPanel();
						}
					});
	});
	}
	$("#SAVE_FOOTER").on("click",function(){
		if(document.getElementsByName("receive_notifications")[0].checked == false){
			$.ajax({
							type: "POST",
							url: "index.php?entryPoint=fixReceiveNotifications",
							data: {
								id:$("#record").val()
							},
							dataType: "json",
							success: function(result) {
								
							},
							failure: function() {
							   // SUGAR.ajaxUI.hideLoadingPanel();
							}
						
			});
		}
	
	});
	$("#SAVE_HEADER").on("click",function(){
		if(document.getElementsByName("receive_notifications")[0].checked == false){
			$.ajax({
							type: "POST",
							url: "index.php?entryPoint=fixReceiveNotifications",
							data: {
								id:$("#record").val()
							},
							dataType: "json",
							success: function(result) {
								
							},
							failure: function() {
							   // SUGAR.ajaxUI.hideLoadingPanel();
							}
						
			});
		}
	
	});
  $("#subpanel_title_opportunities").on("click",function(){
	  setTimeout(function(){
	$("#formformaccounts_opportunities").on("click", function(){
	setTimeout(function(){
	var textEle=document.getElementById("amount");
	if(textEle != null && textEle != undefined){
		 var str;
		 var i;
		 var st;
		 var j;
		 var text_val;
		 var cusText;
		 textEle.onkeyup=function(){
		 cusText='';
		 str='';
		 text_val=textEle.value;
		 
		 for(var u=0;u<text_val.length;u++)
		 {
		 if(text_val[u]!=",")
		  cusText+=text_val[u];
		 
		 }
		 
			
			i=eval(cusText.length-1);
			j=eval(cusText.length-4);
		while(i>-1){
		if(i==j && cusText.length > 3 ){
			j-=3;
		 str=cusText[i]+","+str;
		
		 }else{
		 str=cusText[i]+str;
		 }
		 i--;
		  }
		
		
		 textEle.value=str;
		 }
	}
	},2000);
	});
	},1500);
	});
	$("#formformaccounts_opportunities").on("click", function(){
	setTimeout(function(){
	var textEle=document.getElementById("amount");
	if(textEle != null && textEle != undefined){
		 var str;
		 var i;
		 var st;
		 var j;
		 var text_val;
		 var cusText;
		 textEle.onkeyup=function(){
		 cusText='';
		 str='';
		 text_val=textEle.value;
		  
		 for(var u=0;u<text_val.length;u++)
		 {
		 if(text_val[u]!=",")
		  cusText+=text_val[u];
		 
		 }
		
			
			i=eval(cusText.length-1);
			j=eval(cusText.length-4);
		while(i>-1){
		if(i==j && cusText.length > 3 ){
			j-=3;
		 str=cusText[i]+","+str;
		
		 }else{
		 str=cusText[i]+str;
		 }
		 i--;
		  }
		
		
		 textEle.value=str;
		 }
	}
	},2000);
	});
	//changeCur("opportunity_amount");
	var mod=document.getElementsByName("return_module");
	var detailView=document.getElementsByName("return_action")[0];
	var returnId = document.getElementsByName("return_id")[0];
	var x = GetURLParameter('action');
	var y=  GetURLParameter('module');
	var m = document.getElementsByName("relate_to")[0];
	if(m != null && m != undefined){
		m = document.getElementsByName("relate_to")[0].value;
	}else{
		m = "";
	}
	var recor =  GetURLParameter('record');
	var modee = GetURLParameter('mode');
	var createe = GetURLParameter('create');
	if(detailView == null || detailView == undefined ){
		var adetailView = null;
	}else{
		if(detailView.value != null && detailView.value != undefined){
			var adetailView=detailView.value;
		}else{
			var adetailView=null;
		}
	}
	if(returnId == null || returnId == undefined ){
		var aReturnId=null;
	}else{
		if(returnId.value != null && returnId.value != undefined){
			var aReturnId=returnId.value;
		}else{
			var aReturnId=null;
		}
	}
	if(mod[0] != null && mod[0] != undefined){
	 var modul = mod[0].value;
	}else{
		var modul = "";
	}
	
	setAllowView();
	if(y!="Home"){
if((y=="Leads" && x=="EditView") || (y=="Accounts" && x=="EditView") || (adetailView == "DetailView" && y != "po_PO" && y != "Opportunities" && y != "cs_cusUser" && y != "EmailTemplates" && y != "AOS_Invoices" && y != "AOS_Quotes" && y != "AOS_Contracts" && y != "bill_Billing" && y != "AOS_Products" && modul != "Tasks" && modul != "EmailTemplates" && modul != "Contacts" && modul != "AOS_Invoices" && modul != "AOS_Quotes" && modul != "AOS_Contracts" && modul != "AOS_Products" && m != "account_aos_invoices" && modul != "po_PO" && modul != "Opportunities" && modul != "bill_Billing" && document.getElementsByName("return_module")[0].value != "cs_cusUser" && aReturnId != null)){
	if((y=="Leads" && x=="EditView")||(adetailView == "DetailView" && modul == "Leads" )){
	var textEle=document.getElementById("opportunity_amount");
	if(textEle != null && textEle != undefined){
		 var str;
		 var i;
		 var st;
		 var j;
		 var text_val;
		 var cusText;
		 textEle.onkeyup=function(){
		 cusText='';
		 str='';
		 text_val=textEle.value;
		 
		 for(var u=0;u<text_val.length;u++)
		 {
		 if(text_val[u]!=",")
		  cusText+=text_val[u];
		 
		 }
		
			
			i=eval(cusText.length-1);
			j=eval(cusText.length-4);
		while(i>-1){
		if(i==j && cusText.length > 3 ){
			j-=3;
		 str=cusText[i]+","+str;
		
		 }else{
		 str=cusText[i]+str;
		 }
		 i--;
		  }
		
		
		 textEle.value=str;
		 }
	}
	}
	
	var account_name=document.getElementsByClassName('edit-view-row-item')[6];
				var account_name_label=account_name.getElementsByClassName("label")[0];
				var requ=account_name_label.getElementsByClassName("required")[0];
				if($('#bussiness_type_c').val() == "B2B"){
					var req=document.getElementsByClassName('required')[0];
					var clone=req.cloneNode(true);
					account_name_label.appendChild(clone);
				}
				$('#bussiness_type_c').on('change',function(){
					account_name=document.getElementsByClassName('edit-view-row-item')[6];
					account_name_label=account_name.getElementsByClassName("label")[0];
					requ=account_name_label.getElementsByClassName("required")[0];
					if($('#bussiness_type_c').val() == "B2B"){
						if(requ == null || requ == undefined){
						var req=document.getElementsByClassName('required')[0];
						var clone=req.cloneNode(true);
						account_name_label.appendChild(clone);
						}
					}
					else{
						
						if(requ != null && requ != undefined){
						
						requ.remove();
						
						}
						
					}
				// <span class="required">*</span>
				});
				//start
				//var emailInput=document.querySelector("[id^=Leads1emailAddress]");
				if(y=="Leads" || modul=="Leads"){
				var pb=document.getElementById('phone_mobile_dup_detector_info');
				}else if(y=="Accounts" || modul=="Accounts"){
				var pb=document.getElementById('phone_alternate_dup_detector_info');
				}
				
				var show_info = pb.cloneNode(true);
				//show_info.setAttribute("id", "show_info1");
				//
				var jj=1;
				var email_address=document.getElementsByClassName("email-address-input-group");
				
				
				for (let i=1;i<email_address.length;i++){
					
					show_info = pb.cloneNode(true);
					show_info.setAttribute("id", "show_info"+i);
					email_address[i].appendChild(show_info);
					
					
					if(y=="Leads" || modul=="Leads"){
				var leads2=email_address[i].querySelector("[id^='Leads']");
				}else if(y=="Accounts" || modul=="Accounts"){
				var leads2=email_address[i].querySelector("[id^='Accounts']");
				}
					
					
					leads2.onblur=function(){
						/*
						if(typeof(total_element) == "undefined")
						var total_element;
				if (typeof (total_element) == 'undefined')
					total_element = 0;
					total_element++;
					
					inside_submit = true;
					*/
					$('#creationModal').find('.modal-body').find('#error').hide();
					if(mod !=null && mod !=undefined){
					var type=modul;	
					}else{
					var type=GetURLParameter("return_module");
					
					}
					var class_info2 = "show_info"+i;// viết thêm class show lỗi
					var form_name2 = 'EditView';//name form
					var module_name2 = type;
					var field_value2 = encodeURIComponent(this.value);
					
					var field_name2 = this.id;
					var record_id2 = $('#' + form_name2 + ' input[name="record"]').val();
					var data2 = "record=" + record_id2 + "&module_name=" + module_name2 + "&field_name=" + field_name2 + "&field_value=" + field_value2;
					/*
					if (form_name2.substring(0, 16) == "form_QuickCreate" || form_name2.substring(0, 24) == "form_SubpanelQuickCreate" || form_name2.substring(0, 18) == "form_DCQuickCreate")
						var curSubmit = $("input[name$='save_button']", $('#' + form_name2));
					else
						var curSubmit = $("input[type=submit]", $('#' + form_name2));
					*/
					//console.log('recordId'+record_id2);
					if (field_value2 != '') {
           // $("#" + class_info).show();
            $("#" + class_info2).fadeIn(400).html('<img src="custom/include/SugarFields/Fields/Dupdetector/image/ajax-loading.gif" />');
					$.ajax({
                type: "POST",
                url: "index.php?module=Dupdetector&action=check_dup&to_pdf=1&sugar_body_only=true&record_id=" + record_id2,
                data: data2,
                cache: false,
                async: true,
                success: function(result) {
                    if (result != '') {
                        res = eval('(' + result + ')');
                        total_element--;
                        if (res.status == '') {
                            $("#" + class_info2).html('');
                            
                        } else {
							inside_submit == true
							//alert(res.status);
                            ele_status[field_name2] = res.status;
                            var more_info_msg2 = '<img border="0"  id ="tooltip_' + field_name2 + '" onclick="return SUGAR.util.showHelpTips(this,ele_status['+field_name2+']);" src="themes/default/images/helpInline.gif" >';
                            $("#" + class_info2).html('<img src="custom/include/SugarFields/Fields/Dupdetector/image/error.gif" /> Khách hàng này đã tồn tại ! ' + more_info_msg2);
                            SUGAR.util.evalScript(more_info_msg2);
                            
                        }
                    }
                    //SUGAR.ajaxUI.hideLoadingPanel();
                   // if (total_element <= 0 && inside_submit == true) {
                    //    inside_submit = false;
                    //    $(curSubmit).trigger("click");
                   // }
                },
                failure: function() {
                    //SUGAR.ajaxUI.hideLoadingPanel();
                }
            });
			
			
					}
					else{
						
						$("#" + class_info2).html('');
						
						
					}
						
					};
					
				}
				
				
					
				
				
				$('.email-address-add-button').on("click",function(){
					var show_inf = pb.cloneNode(true);
					show_inf.setAttribute("id", "show_info"+eval(email_address.length-1));
					email_address[email_address.length-1].appendChild(show_inf);
					//alert("sucessful");
					if(y=="Leads" || modul=="Leads"){
				var leads1=email_address[email_address.length-1].querySelector("[id^='Leads']");
				}else if(y=="Accounts" || modul=="Accounts"){
				var leads1=email_address[email_address.length-1].querySelector("[id^='Accounts']");
				}
					
					//alert(leads.id);
					
					leads1.onblur=function(){
						/*
						if(typeof(total_element) == "undefined")
						var total_element;
				if (typeof (total_element) == 'undefined')
					total_element = 0;
					total_element++;
					
					inside_submit = true;
					*/
					$('#creationModal').find('.modal-body').find('#error').hide();
					
					// viết thêm class show lỗi
					if(mod !=null && mod !=undefined){
					var type=modul;	
					}else{
					var type=GetURLParameter("return_module");
					
					}
					var form_name = 'EditView';//name form
					var module_name1 = type;
					var field_value1 = encodeURIComponent(this.value);
					
					var field_name1 = this.id;
					if(y=="Leads" || modul=="Leads"){
				var e=parseInt(field_name1.slice(18));
				}else if(y=="Accounts" || modul=="Accounts"){
				var e=parseInt(field_name1.slice(21));
				
				}
					
					
					var class_info1 = "show_info"+eval(e+1);
					var record_id1 = $('#' + form_name + ' input[name="record"]').val();
					var data1 = "record=" + record_id1 + "&module_name=" + module_name1 + "&field_name=" + field_name1 + "&field_value=" + field_value1;
					/*
					if (form_name.substring(0, 16) == "form_QuickCreate" || form_name.substring(0, 24) == "form_SubpanelQuickCreate" || form_name.substring(0, 18) == "form_DCQuickCreate")
						var curSubmit = $("input[name$='save_button']", $('#' + form_name));
					else
						var curSubmit = $("input[type=submit]", $('#' + form_name));
					*/
					if (field_value1 != '') {
            //$("#" + class_info).show();
			
            $("#" + class_info1).fadeIn(400).html('<img src="custom/include/SugarFields/Fields/Dupdetector/image/ajax-loading.gif" />');
				$.ajax({
                type: "POST",
                url: "index.php?module=Dupdetector&action=check_dup&to_pdf=1&sugar_body_only=true&record_id=" + record_id1,
                data: data1,
                cache: false,
                async: true,
                success: function(result) {
                    if (result != '') {
                        res = eval('(' + result + ')');
                        //total_element--;
                        if (res.status == '') {
                            $("#" + class_info1).html('');
                            
                        } else {
							
							//alert(res.status);
                            ele_status[field_name1] = res.status;
                            var more_info_msg = '<img border="0"  id ="tooltip_' + field_name1 + '" onclick="return SUGAR.util.showHelpTips(this,ele_status['+field_name1+']);" src="themes/default/images/helpInline.gif" >';
                            $("#" + class_info1).html('<img src="custom/include/SugarFields/Fields/Dupdetector/image/error.gif" /> Khách hàng này đã tồn tại ! ' + more_info_msg);
                            SUGAR.util.evalScript(more_info_msg);
                            
                        }
                    }
                    //SUGAR.ajaxUI.hideLoadingPanel();
                   // if (total_element <= 0 && inside_submit == true) {
                    //    inside_submit = false;
                   //     $(curSubmit).trigger("click");
                   // }
                },
                failure: function() {
                   // SUGAR.ajaxUI.hideLoadingPanel();
                }
            });
			
			
					}
					else{

						$("#" + class_info1).html('');

					}						
					};
					
					//}
				
				});
	
	
	
	
	

	
	
	
	$("#SAVE.button.primary").removeAttr('accesskey');
	$("#SAVE.button.primary").removeAttr('onclick');
	$("#SAVE.button.primary").removeAttr('type');
	$("#SAVE.button.primary").attr('type', 'button');
	

$('#SAVE.button.primary').click(function(event){
	
	var aError=document.getElementById("aError");
	/*
	if(typeof(total_element) == "undefined")
    var total_element;
if (typeof (total_element) == 'undefined')
        total_element = 0;
    total_element++;
	*/
	
	var save1;
	var save2;
	if(mod !=null && mod !=undefined){
		var type=modul;	
	}
	else{
		var type=GetURLParameter("return_module");
					
		}
	//inside_submit = true;
    $('#creationModal').find('.modal-body').find('#error').hide();
	
	var class_info = $("#phone_mobile").attr('id') + "_dup_detector_info";
    var form_name = 'EditView';//name form
	var module_name = type;
	var field_value = encodeURIComponent($("#phone_mobile").val());
	var field_name = $("#phone_mobile").attr('id');
	var record_id = $('#' + form_name + ' input[name="record"]').val();
	var data = "record=" + record_id + "&module_name=" + module_name + "&field_name=" + field_name + "&field_value=" + field_value;
	  if (form_name.substring(0, 16) == "form_QuickCreate" || form_name.substring(0, 24) == "form_SubpanelQuickCreate" || form_name.substring(0, 18) == "form_DCQuickCreate")
        var curSubmit = $("input[name$='save_button']", $('#' + form_name));
    else
        var curSubmit = $("input[type=submit]", $('#' + form_name));
   if((check_form(form_name)) || (!check_form(form_name))) {
	   window.onbeforeunload = null;
	   
		if($("#lastname").val() != '' && ($("#phone_mobile").val() != '' || $("#phone_alternate").val() != '')){
		if(y=="Leads" || modul=="Leads"){
			var save;
        $('#' + form_name).find('input[name=action]').val('MTS_saveLead');
        $('#' + form_name).find('input[name=module]').val('Leads');
		//start
		
		$.ajax({
                type: "POST",
                url: "index.php?module=Dupdetector&action=check_dup&to_pdf=1&sugar_body_only=true&record_id=" + record_id,
                data: data,
                cache: false,
                async: true,
                success: function(result) {
                    if (result != '') {
                        res = eval('(' + result + ')');
                        total_element--;
						
                        if (res.status == '') {
							//fix
							if($("#bussiness_type_c").val() == "B2B"){
								if($("#account_name").val() != ""){
									
							if (aError != undefined){
							aError.style.display = "none";
							}		
							
							//
							save=1;
							
					//$('#creationModal').find('.modal-body').find('#error').hide();
					
							
							//
							
							
							
							
								}//account name != ""
								else
								{
								//$("#" + class_info).html('<img src="custom/include/SugarFields/Fields/Dupdetector/image/error.gif" /> account name không được để trống ! ');
										save=0;
										if (aError == undefined){
											if($('#Leads_dcmenu_save_button').val()=="Save"){
												$('#account_name').after('<p style="color:red" id="aError">Missing required field: Account Name</p>');
											}
											else{
												$('#account_name').after('<p style="color:red" id="aError">	Thiếu trường bắt buộc: Tên công ty</p>');
											
											}	
											
										}
								}
						}else   // type=b2c
						{
							if (aError != undefined){
							aError.style.display = "none";
							}
						save=1;	
							//
							
						//
						


						}							//alert("luu");
                           


						$("#" + class_info).html('');
                            if (res.settings == true) {
                                for (validate_dup_key in validate['form_DCQuickCreate_Leads']) {
                                    if (typeof (validate['form_DCQuickCreate_Leads'][validate_dup_key][0]) != 'undefined' &&
                                            validate['form_DCQuickCreate_Leads'][validate_dup_key][0] == 'phone_mobile' && validate['form_DCQuickCreate_Leads'][validate_dup_key][1] == 'error') {
                                    validate['form_DCQuickCreate_Leads'].splice(validate_dup_key, 1);
                                    }
                                }
							}
						   
                        } 
						else
						{
							
							save=0;
							if (aError != undefined){
								aError.style.display = "none";
							}
							ele_status['phone_mobile'] = res.status;
							var more_info_msg = '<img border="0"  id ="tooltip_' + field_name + '" onclick="return SUGAR.util.showHelpTips(this,ele_status[\'phone_mobile\']);" src="themes/default/images/helpInline.gif" >';
							$("#" + class_info).html('<img src="custom/include/SugarFields/Fields/Dupdetector/image/error.gif" /> Khách hàng này đã tồn tại ! ' + more_info_msg);
							SUGAR.util.evalScript(more_info_msg);
							if (res.settings == true) {
							for (validate_dup_key in validate['form_DCQuickCreate_Leads']) {
								if (typeof (validate['form_DCQuickCreate_Leads'][validate_dup_key][0]) != 'undefined' &&
								validate['form_DCQuickCreate_Leads'][validate_dup_key][0] == 'phone_mobile' && validate['form_DCQuickCreate_Leads'][validate_dup_key][1] == 'error') {
								validate['form_DCQuickCreate_Leads'].splice(validate_dup_key, 1);
								}
							}
								addToValidate('form_DCQuickCreate_Leads', 'phone_mobile', 'error', true, 'Please resolve conflict');
							}
							
                        }
                    }
					/*
                    SUGAR.ajaxUI.hideLoadingPanel();
                    if (total_element <= 0 && inside_submit == true) {
                        inside_submit = false;
                        $(curSubmit).trigger("click");
                    }
					*/
                },
                failure: function() {
                    SUGAR.ajaxUI.hideLoadingPanel();
                }
            });
		
		
	}else if(y=="Accounts" || modul=="Accounts"){
		
		var save=1;
		
	}
		//alert('hehe'+save);
		//end
		var email_address3=document.getElementsByClassName("email-address-input-group");
		var y1=1;
		var x1=1;			
		var ar=[];				
					if(email_address3.length >1){
					var cde= setInterval(function(){
						if(x1==y1){
					if(y=="Leads" || modul=="Leads"){
				var leads3=email_address3[x1].querySelector("[id^='Leads']");
				}else if(y=="Accounts" || modul=="Accounts"){
				var leads3=email_address3[x1].querySelector("[id^='Accounts']");
				}		
					
					x1++;
					var more_info_msg3;
					var form_name3 = 'EditView';//name form
					var module_name3 = type;
					var field_value3 = encodeURIComponent(leads3.value);
					
					var field_name3 = leads3.id;
					if(y=="Leads" || modul=="Leads"){
				var f=parseInt(field_name3.slice(18));
				}else if(y=="Accounts" || modul=="Accounts"){
				var f=parseInt(field_name3.slice(21));
				
				}
					
					var class_inf3 = "show_info"+eval(f+1);
					var record_id3 = $('#' + form_name3 + ' input[name="record"]').val();
					var data3 = "record=" + record_id3 + "&module_name=" + module_name3 + "&field_name=" + field_name3 + "&field_value=" + field_value3;
					
					
					if (field_value3 != '') {
            //$("#" + class_info).show();
			
            $("#" + class_inf3).fadeIn(400).html('<img src="custom/include/SugarFields/Fields/Dupdetector/image/ajax-loading.gif" />');
				$.ajax({
                type: "POST",
                url: "index.php?module=Dupdetector&action=check_dup&to_pdf=1&sugar_body_only=true&record_id=" + record_id3,
                data: data3,
                cache: false,
                async: true,
                success: function(result1) {
                    if (result1 != '') {
                        res1 = eval('(' + result1 + ')');
                        total_element--;
                        if (res1.status == '') {
                            $("#" + class_inf3).html('');
                            ar[y1]=1;
							
							
                        } else {
							ar[y1]=0;
							//alert(res.status);
                            ele_status[field_name3] = res1.status;
                             more_info_msg3 = '<img border="0"  id ="tooltip_' + field_name3 + '" onclick="return SUGAR.util.showHelpTips(this,ele_status['+field_name3+']);" src="themes/default/images/helpInline.gif" >';
                            $("#" + class_inf3).html('<img src="custom/include/SugarFields/Fields/Dupdetector/image/error.gif" /> Khách hàng này đã tồn tại ! ' + more_info_msg3);
                            SUGAR.util.evalScript(more_info_msg3);
                            
                        }
						
                    }
					y1++;
					
                    //SUGAR.ajaxUI.hideLoadingPanel();
                    //if (total_element <= 0 && inside_submit == true) {
                    //    inside_submit = false;
                  //      $(curSubmit).trigger("click");
                   // }
				   if(y1==email_address3.length){
				   for(var o=0;o<ar.length;o++){
						
						if(ar[o]==0){
						save1=ar[o];
						break;
						}
						if(o==eval(ar.length-1)){
							if(ar[o]==1){
								save1=1;
							}
							
						}
					}
					
				}
                },
                failure: function() {
                   // SUGAR.ajaxUI.hideLoadingPanel();
                }
            });
				
						}
						else{
							 $("#" + class_inf3).html('');
							y1++;
							ar[0]=1;
							 if(y1==email_address3.length){
					for(var o=0;o<ar.length;o++){
						
						if(ar[o]==0){
						save1=ar[o];
						break;
						}
						if(o==eval(ar.length-1)){
							if(ar[o]==1){
								save1=1;
							}
							
						}
					}
					
				}
						}
						
					
						
					}
				if(x1==email_address3.length){
						
						 clearInterval(cde);
					
						
				}	
					
				}, 50);
			//}
			}
			else{
					ar[0]=1;
					if(y1==email_address3.length){
						for(var o=0;o<ar.length;o++){
						
							if(ar[o]==0){
								save1=ar[o];
								break;
							}
							if(o==eval(ar.length-1)){
								if(ar[o]==1){
								save1=1;
								}
							
							}
						}
					
					}
				
				
			}
					
				
				
					
				
				//while(save==undefined || save1==undefined ){
					/*
					var abc= setInterval(function(){
						
					}, 50);
					*/
					
		var abc= setInterval(function(){
			
		if(save!=undefined && save1 != undefined  && y1==email_address3.length){
			
				if(save==1 && save1==1 ){
							
						var _form = document.getElementById('EditView');
						_form.action.value = 'Save';
						var count = document.getElementById("showMoreAssign").childElementCount;
						var assignedArray = [];
						if(count > 0){
							//save other assigned
							for (let j = 1; j <= count; j++){
								if($("#assigned_user_name"+j).val() != null && $("#assigned_user_name"+j).val() != '' && $("#assigned_user_name"+j).val() != undefined ){
								assignedArray.push($("#userAssignDrop"+j).val()[0]);
								}
							}
							if(assignedArray.length > 0){
								localStorage.setItem("Assigned",JSON.stringify(assignedArray));
							}else{
								localStorage.setItem("Assigned",'');
							}
						}
						var count2 = document.getElementById("showMoreCampaign").childElementCount;
						var campaignArray = [];
						if(count2 > 0){
							//save other campaign
							for (let vv = 1; vv <= count2; vv++){
								if($("#campaign_name"+vv).val() != null && $("#campaign_name"+vv).val() != '' && $("#campaign_name"+vv).val() != undefined ){
								campaignArray.push($("#userCampaignDrop"+vv).val()[0]);
								}
							}
							if(campaignArray.length > 0){
								localStorage.setItem("Campaign",JSON.stringify(campaignArray));
							}else{
								localStorage.setItem("Campaign",'');
							}
						}
						//if (check_form('EditView')){
						//	setTimeout(function(){
						SUGAR.ajaxUI.submitForm(_form); // this will submit the form
						//}}
				//},300);
							
				}
			
		 clearInterval(abc);
		}
		}, 50);
				}			
			}				
		
	//}
	

      
     

});
	
	//
	}
	var module2=document.getElementsByName("return_module")[0];
	if(module2 == null || module2 == undefined ){
		var amodule=null;
	}else{
		if(module2.value != null && module2.value != undefined){
			var amodule=module2.value;
		}else{
			var amodule=null;
		}
	}
	if(mod[0] != null && mod[0] != undefined){
	 var modul = mod[0].value;
	}else{
		var modul = "";
	}
	if((y == "cs_cusUser" && x == "EditView") || (amodule == "cs_cusUser" && x != "index")){
		
		if(cusRecord != null && cusRecord != undefined){
			var cusUserRecord = cusRecord;
		}else{
			cusRecord = document.getElementsByName("record")[0].value; 
			if(cusRecord != null && cusRecord != undefined && cusRecord != ''){
				var cusUserRecord = cusRecord;
			}else{
				var cusUserRecord = '';	
			}
		}
		
		$("#SAVE.button.primary").removeAttr('accesskey');
		$("#SAVE.button.primary").removeAttr('onclick');
		$("#SAVE.button.primary").removeAttr('type');
		$("#SAVE.button.primary").attr('type', 'button');
		$('#username').after('<div style="visibility:hidden;width:280px;height:10px;z-index:10;color:red" id="usernameError">username này đã tồn tại</div>');
		var parentGroupId = document.getElementById("groupid").parentNode;
		parentGroupId.parentNode.style.display = 'none';
		var provinceVal = $("#provincecode").val();
		$( '#groupname' ).after( '<div style="display:none;position:absolute;width:280px;height:180px;z-index:10" id="grp"></div>' );
		$("#groupname").on("click",function(){
			$('#grp').css('display','block');
		});
		$("#groupname").on("blur",function(){
			setTimeout(function(){ 
				$('#grp').css('display','none');
			}, 170);
		});
	
		var cusRecord = GetURLParameter("record");
		//if(cusRecord != null && cusRecord != undefined){
		//	var cusUserRecord = cusRecord;
		//}else{
		//	var cusUserRecord = '';
		//}
		
		$("#username").on("blur",function(){
			$.ajax({
				type: "POST",
				url: "index.php?entryPoint=checkUsername",
				data: {
						username:$("#username").val(),
						id:cusUserRecord
					},
				success: function(res){
					if(res == "1"){
						$("#usernameError").css("visibility","hidden");
					}else{
						$("#usernameError").css("visibility","visible");
					}
				}
			});
		});
		
		$('#SAVE.button.primary').click(function(){
			if(check_form("EditView")) {
				$.ajax({
					type: "POST",
					url: "index.php?entryPoint=checkUsername",
					data: {
							username:$("#username").val(),
							id:cusUserRecord
						},
					success: function(res){
						if(res == "1"){
							var _form = document.getElementById('EditView');
							_form.action.value = 'Save';
							SUGAR.ajaxUI.submitForm(_form);
						}else{
							$("#usernameError").css("visibility","visible");
							alert("username này đã tồn tại");
						}
					}
				});
			}
			
		});
	}
	if((y == "cs_cusUser" && x == "EditView")|| (amodule == "cs_cusUser" && x != "index")){
		var cuserIdd = GetURLParameter('record');
		if(cuserIdd != null && cuserIdd != undefined){
			var CCuserId = cuserIdd;
		}else{
			cuserIddd = document.getElementsByName("record")[0];
			if(cuserIddd != null && cuserIddd != undefined && cuserIddd != ''){
				cuserIdd = document.getElementsByName("record")[0].value; 
				if(cuserIdd != null && cuserIdd != undefined && cuserIdd != ''){
				var CCuserId = cuserIdd;
				}else{
				var CCuserId = '';	
				}
			}else{
				var CCuserId = '';	
				
			}
		}
		if(recor != null && recor != undefined){
		$('<input type="button" id="resetPass" value="reset Password">').appendTo('.edit-view-row');
		}
		$(document).ready(function(){
			if(CCuserId != ''){
			loadProvince2(CCuserId);
			$("#resetPass").on("click", function(){
				if(confirm("Are you sure want to reset this password") == true){
					$.ajax({
				type: "POST",
				url: "index.php?entryPoint=resetPass",
				data: {
						id:CCuserId
					},
				success: function(res){
					alert(res);
				}
			});
				}
					
			});
			}else{
				loadProvince4();
			}
		});
	}
	if(y == "AOS_Invoices" && x == "DetailView" && (aReturnId != null && aReturnId != undefined)){
		document.getElementsByClassName("detail-view-row-item")[7].style.display="block";
	}
	if(y == "AOS_Invoices" && x == "DetailView" && (recor != null && recor != undefined && recor != "" )){
		$.ajax({
					type: "POST",
					url: "index.php?entryPoint=getInvLanguage",
					data:{
						id:recor
					},
					dataType:"json",
					success: function(data) {
						var html = '';
						html += '<li>';
						html += '<input type="button" class="button" value="'+data[0].language.LBL_EXPORT_INVOICE_VAT+'" onclick="exportInvoices(\''+recor+'\',\'1\')">';
						html+='</li>';
						//nếu tình trang là chưa kí, không disable
						if(data[0].status != "2"){
							html += '<li>';
							html += '<input type="button" class="button" value="'+data[0].language.LBL_BUTTON_EXPORT_INVOICE_DRAFT+'" onclick="exportInvoices(\''+recor+'\',\'2\')">';
							html+='</li>';
						}else{
							html += '<li>';
							html += '<input type="button" class="button" value="'+data[0].language.LBL_BUTTON_EXPORT_INVOICE_DRAFT+'" onclick="alert(\''+data[0].language.LBL_EXPORT_INVOICE_PERMISSION+'\')">';
							html+='</li>';
						}
						//html += '<li>';
						//html += '<input type="button" class="button" value="Điều chỉnh HĐ VAT" //onclick="changeInvoices(\''+recor+'\')">';
						//html+='</li>';
						//html += '<li>';
						//html += '<input type="button" class="button" value="Thay Thế HĐ VAT" //onclick="replaceInvoices(\''+recor+'\')">';
						//html+='</li>';
						//html += '<input type="button" class="button" value="Thay thế HĐ VAT" //onclick="exportInvoices(\''+recor+'\',\'4\')">';
						//html+='</li>';
						var tabAct = document.getElementById("tab-actions");
						tabAct.getElementsByClassName("dropdown-menu")[0].innerHTML += html;
					},
					error: function (error) {
								
					}
		});
		
	}
	if(((y == "AOS_Invoices" || y == "AOS_Quotes" || y == "AOS_Contracts") && x == "EditView") || ((modul == "AOS_Invoices" || modul == "AOS_Quotes" || modul == "AOS_Contracts" || m == "account_aos_invoices" || m == "account_aos_quotes" || m == "account_aos_contracts") && adetailView == "DetailView" && (aReturnId != null && aReturnId != undefined))){
		var k=-1;
		var p=-1;
		var ab = []; 
		var bc = [];
		var cd = []; 
		var de = []; 
		var ef = []; 
		var fg = []; 
		var gh = [];
		var hi = []; 
		var ik = []; 
		var kl = []; 
		var lm = [];
		var mn = "";
		var totalAmt = "";
		var discountAmount = "";
		var subtotalAmount = "";
		var shippingAmount = "";
		var shippingTaxAmt = "";
		var taxAmount = "";
		var totalAmount = "";
		if((aReturnId != undefined && aReturnId != null && aReturnId != "") || (recor != undefined && recor != null && recor != "")){
			var productListPric = document.getElementsByClassName("product_part_number");
			var serviceNam = document.getElementsByClassName("service_name");
			var group = document.getElementsByClassName("group");
			if(productListPric != null && productListPric != undefined){
				for(let i = 0; i < productListPric.length; i++){
					k++;
					$("#EditView_product_part_number"+k+"_results").on("click",function(){
								var index = this.id;
								index = String(index);
								index = index.replace('_results', '');
								index = index.slice("28");
								var index2 = this.parentNode.parentNode.parentNode.parentNode.id;
								index2 = String(index2);
								index2 = index2.slice("13");
									setTimeout(function(){
										if($("#product_part_number"+index).val() != ""){
											document.getElementById('product_product_list_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_list_price' + index).value,sig_digits);
											document.getElementById('product_product_cost_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_cost_price' + index).value,sig_digits);
											document.getElementById('product_product_total_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_total_price' + index).value,sig_digits);
											document.getElementById('product_product_unit_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_unit_price' + index).value,sig_digits);
											
											document.getElementById('group'+index2+'total_amt').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'total_amt').value,sig_digits);
											document.getElementById('group'+index2+'discount_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'discount_amount').value,sig_digits);
											document.getElementById('group'+index2+'subtotal_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'subtotal_amount').value,sig_digits);
											document.getElementById('group'+index2+'tax_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'tax_amount').value,sig_digits);
											document.getElementById('group'+index2+'total_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'total_amount').value,sig_digits);
											
											document.getElementById('total_amt').value = changeCurrencyWithDotForSum(document.getElementById('total_amt').value,sig_digits);
											document.getElementById('discount_amount').value = changeCurrencyWithDotForSum(document.getElementById('discount_amount').value,sig_digits);
											document.getElementById('subtotal_amount').value = changeCurrencyWithDotForSum(document.getElementById('subtotal_amount').value,sig_digits);
											document.getElementById('shipping_tax_amt').value = changeCurrencyWithDotForSum(document.getElementById('shipping_tax_amt').value,sig_digits);
											document.getElementById('tax_amount').value = changeCurrencyWithDotForSum(document.getElementById('tax_amount').value,sig_digits);
											document.getElementById('total_amount').value = changeCurrencyWithDotForSum(document.getElementById('total_amount').value,sig_digits);
																															
										}
									},800);
								});
								
								$("#product_part_number"+k).on("keyup",function(event){
									if(event.keyCode == 13){
										var index = this.id;
										index = String(index);
										index = index.slice("19");
										var index2 = this.parentNode.parentNode.parentNode.parentNode.id;
										index2 = String(index2);
										index2 = index2.slice("13");
										setTimeout(function(){
											if($("#product_part_number"+index).val() != ""){
												document.getElementById('product_product_list_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_list_price' + index).value,sig_digits);
												document.getElementById('product_product_cost_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_cost_price' + index).value,sig_digits);
												document.getElementById('product_product_total_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_total_price' + index).value,sig_digits);
												document.getElementById('product_product_unit_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_unit_price' + index).value,sig_digits);
											
												document.getElementById('group'+index2+'total_amt').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'total_amt').value,sig_digits);
												document.getElementById('group'+index2+'discount_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'discount_amount').value,sig_digits);
												document.getElementById('group'+index2+'subtotal_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'subtotal_amount').value,sig_digits);
												document.getElementById('group'+index2+'tax_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'tax_amount').value,sig_digits);
												document.getElementById('group'+index2+'total_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'total_amount').value,sig_digits);
												
												document.getElementById('total_amt').value = changeCurrencyWithDotForSum(document.getElementById('total_amt').value,sig_digits);
												document.getElementById('discount_amount').value = changeCurrencyWithDotForSum(document.getElementById('discount_amount').value,sig_digits);
												document.getElementById('subtotal_amount').value = changeCurrencyWithDotForSum(document.getElementById('subtotal_amount').value,sig_digits);
												document.getElementById('shipping_tax_amt').value = changeCurrencyWithDotForSum(document.getElementById('shipping_tax_amt').value,sig_digits);
												document.getElementById('tax_amount').value = changeCurrencyWithDotForSum(document.getElementById('tax_amount').value,sig_digits);
												document.getElementById('total_amount').value = changeCurrencyWithDotForSum(document.getElementById('total_amount').value,sig_digits);
											}
										},800);
									}
								});
								$("#EditView_product_name"+k+"_results").on("click",function(){
											var index = this.id;
											index = String(index);
											index = index.replace('_results', '');
											index = index.slice("21");
											var index2 = this.parentNode.parentNode.parentNode.parentNode.id;
											index2 = String(index2);
											index2 = index2.slice("13");
											setTimeout(function(){
												if($("#product_name"+index).val() != ""){
													document.getElementById('product_product_list_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_list_price' + index).value,sig_digits);
													document.getElementById('product_product_cost_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_cost_price' + index).value,sig_digits);
													document.getElementById('product_product_total_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_total_price' + index).value,sig_digits);
													document.getElementById('product_product_unit_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_unit_price' + index).value,sig_digits);
													
													document.getElementById('group'+index2+'total_amt').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'total_amt').value,sig_digits);
													document.getElementById('group'+index2+'discount_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'discount_amount').value,sig_digits);
													document.getElementById('group'+index2+'subtotal_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'subtotal_amount').value,sig_digits);
													document.getElementById('group'+index2+'tax_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'tax_amount').value,sig_digits);
													document.getElementById('group'+index2+'total_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'total_amount').value,sig_digits);
													
													document.getElementById('total_amt').value = changeCurrencyWithDotForSum(document.getElementById('total_amt').value,sig_digits);
													document.getElementById('discount_amount').value = changeCurrencyWithDotForSum(document.getElementById('discount_amount').value,sig_digits);
													document.getElementById('subtotal_amount').value = changeCurrencyWithDotForSum(document.getElementById('subtotal_amount').value,sig_digits);
													document.getElementById('shipping_tax_amt').value = changeCurrencyWithDotForSum(document.getElementById('shipping_tax_amt').value,sig_digits);
													document.getElementById('tax_amount').value = changeCurrencyWithDotForSum(document.getElementById('tax_amount').value,sig_digits);
													document.getElementById('total_amount').value = changeCurrencyWithDotForSum(document.getElementById('total_amount').value,sig_digits);
												
												
												}
											},800);
								});
								
								$("#product_name"+k).on("keyup",function(event){
									if(event.keyCode == 13){
											var index = this.id;
												index = String(index);
												index = index.slice("12");
												var index2 = this.parentNode.parentNode.parentNode.parentNode.id;
												index2 = String(index2);
												index2 = index2.slice("13");
											setTimeout(function(){
												if($("#product_name"+index).val() != ""){
													document.getElementById('product_product_list_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_list_price' + index).value,sig_digits);
													document.getElementById('product_product_cost_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_cost_price' + index).value,sig_digits);
													document.getElementById('product_product_total_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_total_price' + index).value,sig_digits);
													document.getElementById('product_product_unit_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_unit_price' + index).value,sig_digits);
													
													document.getElementById('group'+index2+'total_amt').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'total_amt').value,sig_digits);
													document.getElementById('group'+index2+'discount_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'discount_amount').value,sig_digits);
													document.getElementById('group'+index2+'subtotal_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'subtotal_amount').value,sig_digits);
													document.getElementById('group'+index2+'tax_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'tax_amount').value,sig_digits);
													document.getElementById('group'+index2+'total_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'total_amount').value,sig_digits);
													
													document.getElementById('total_amt').value = changeCurrencyWithDotForSum(document.getElementById('total_amt').value,sig_digits);
													document.getElementById('discount_amount').value = changeCurrencyWithDotForSum(document.getElementById('discount_amount').value,sig_digits);
													document.getElementById('subtotal_amount').value = changeCurrencyWithDotForSum(document.getElementById('subtotal_amount').value,sig_digits);
													document.getElementById('shipping_tax_amt').value = changeCurrencyWithDotForSum(document.getElementById('shipping_tax_amt').value,sig_digits);
													document.getElementById('tax_amount').value = changeCurrencyWithDotForSum(document.getElementById('tax_amount').value,sig_digits);
													document.getElementById('total_amount').value = changeCurrencyWithDotForSum(document.getElementById('total_amount').value,sig_digits);
												}
											},800);
									}
								});
						if($("#product_product_list_price"+i).val() != ""){
							$("#product_product_list_price"+i).val(changeCurrencyWithDotForSum($("#product_product_list_price"+i).val().replace(/,/g,''),sig_digits));
						}
						if($("#product_product_discount"+i).val() != ""){
							$("#product_product_discount"+i).val(changeCurrencyWithDotForSum($("#product_product_discount"+i).val().replace(/,/g,''),sig_digits));
						}
						if($("#product_vat_amt"+i).val() != ""){
							$("#product_vat_amt"+i).val(changeCurrencyWithDotForSum($("#product_vat_amt"+i).val().replace(/,/g,''),sig_digits));
						}
						if($("#product_product_total_price"+i).val() != ""){
							$("#product_product_total_price"+i).val(changeCurrencyWithDotForSum($("#product_product_total_price"+i).val().replace(/,/g,''),sig_digits));
						}
						if($("#product_product_unit_price"+i).val() != ""){
							$("#product_product_unit_price"+i).val(changeCurrencyWithDotForSum($("#product_product_unit_price"+i).val().replace(/,/g,''),sig_digits));
						}
								ab[i] = "";
								bc[i] = "";
								cd[i] = "";
								de[i] = "";
								ef[i] = "";
								$("#product_product_list_price"+i).on("focus",function(){
									if(ab[i] == ""){
										ab[i] = $("#product_product_list_price"+i).val();
									}
								});
								$("#product_product_discount"+i).on("focus",function(){
									if(ef[i] == ""){
										ef[i] = $("#product_product_discount"+i).val();
									}
								});
								
								$("#product_product_list_price"+i).on("input",function(){
									if($("#product_product_list_price"+i).val() == ""){
										$("#product_product_unit_price"+i).val("");
									}
									if(ab[i] != $("#product_product_list_price"+i).val()){
										if($("#product_product_list_price"+i).val() != ""){
											//$("#product_product_list_price"+i).val(getLetter(this.value,ZeroNum));
											$("#product_product_list_price"+i).val(changeCurrencyWithDot(ab[i],$("#product_product_list_price"+i).val().replace(/,/g,''),sig_digits,i,"product_",calculateLine2));
										}
										ab[i] = $("#product_product_list_price"+i).val();
									}
								});
								$("#product_product_discount"+i).on("input",function(){
								if(ef[i] != $("#product_product_discount"+i).val()){
									if($("#product_product_discount"+i).val() != ""){
										//$("#product_product_unit_price"+i).val(getLetter(this.value,ZeroNum));
										$("#product_product_discount"+i).val(changeCurrencyWithDot(ef[i],$("#product_product_discount"+i).val().replace(/,/g,''),sig_digits,i,"product_",calculateLine2));
									}
									ef[i] = $("#product_product_discount"+i).val();
								}
								});
								$("#product_vat_amt"+i).on("input",function(){
								if(cd[i] != $("#product_vat_amt"+i).val()){
									if($("#product_vat_amt"+i).val() != ""){
										//$("#product_vat_amt"+i).val(getLetter(this.value,ZeroNum));
										$("#product_vat_amt"+i).val(changeCurrencyWithDot(cd[i],$("#product_vat_amt"+i).val().replace(/,/g,''),sig_digits,i,"product_",calculateLine2));
									}
									cd[i] = $("#product_vat_amt"+i).val();
								}
								});
								$("#product_product_total_price"+i).on("input",function(){
								if(de[i] != $("#product_product_total_price"+i).val()){
									if($("#product_product_total_price"+i).val() != ""){
										//$("#product_product_total_price"+i).val(getLetter(this.value,ZeroNum));
										$("#product_product_total_price"+i).val(changeCurrencyWithDot(de[i],$("#product_product_total_price"+i).val().replace(/,/g,''),sig_digits,i,"product_",calculateLine2));
									}
									de[i] = $("#product_product_total_price"+i).val();
								}
								});
								
								$("#product_vat"+i).on("change", function(){
									calculateLine2(i,"product_",sig_digits);
								});
								$("#product_product_qty"+i).attr("onblur","");
								$("#product_product_qty"+i).on("input",function(){
									if($("#product_product_qty"+i).val() != ""){
										calculateLine2(i,"product_",sig_digits);
									}
								});
								$("#product_discount"+i).on("change", function(){
									calculateLine2(i,"product_",sig_digits);
								});
								$("#product_delete_line"+i).attr("onclick","");
								$("#product_delete_line"+i).attr("onclick","markLineDeleted2("+i+",'product_',sig_digits)");
						
				}
			}
			if(serviceNam != null && serviceNam != undefined){
				for(let r = 0; r < serviceNam.length; r++){
					p++;
					if($("#service_product_list_price"+r).val() != ""){
						$("#service_product_list_price"+r).val(changeCurrencyWithDotForSum($("#service_product_list_price"+r).val().replace(/,/g,''),sig_digits));
					}
					if($("#service_product_unit_price"+r).val() != ""){
						$("#service_product_unit_price"+r).val(changeCurrencyWithDotForSum($("#service_product_unit_price"+r).val().replace(/,/g,''),sig_digits));
					}
					if($("#service_vat_amt"+r).val() != ""){
						$("#service_vat_amt"+r).val(changeCurrencyWithDotForSum($("#service_vat_amt"+r).val().replace(/,/g,''),sig_digits));
					}
					if($("#service_product_total_price"+r).val() != ""){
						$("#service_product_total_price"+r).val(changeCurrencyWithDotForSum($("#service_product_total_price"+r).val().replace(/,/g,''),sig_digits));
					}	
					if($("#service_product_discount"+r).val() != ""){
						$("#service_product_discount"+r).val(changeCurrencyWithDotForSum($("#service_product_discount"+r).val().replace(/,/g,''),sig_digits));
					}
					
					fg[r] = ""; 
								gh[r] = ""; 
								hi[r] = ""; 
								ik[r] = ""; 
								kl[r] = ""; 
								lm[r] = ""; 
								$("#service_name"+r).on("input",function(){
									if($("#service_name"+r).val() != ""){
										calculateLine2(r,"service_",sig_digits);
									}
								});
								$("#service_product_list_price"+r).on("input",function(){
								if(fg[r] != $("#service_product_list_price"+r).val()){
									if($("#service_product_list_price"+r).val() != ""){
										//$("#product_product_list_price"+i).val(getLetter(this.value,ZeroNum));
										$("#service_product_list_price"+r).val(changeCurrencyWithDot(fg[r],$("#service_product_list_price"+r).val().replace(/,/g,''),sig_digits,r,"service_",calculateLine2));
									}
									fg[r] = $("#service_product_list_price"+r).val();
								}
								});
								/*
								$("#service_discount"+i).on("input",function(){
								if(gh[i] != $("#service_discount"+i).val()){
									if($("#service_discount"+i).val() != ""){
										//$("#product_product_unit_price"+i).val(getLetter(this.value,ZeroNum));
										$("#service_discount"+i).val(changeCurrencyWithDot(gh[i],$("#service_discount"+i).val().replace(/,/g,''),sig_digits,i,"service_",calculateLine2));
									}
									gh[i] = $("#service_discount"+i).val();
								}
								});
								*/
								$("#service_product_unit_price"+r).on("input",function(){
								if(hi[r] != $("#service_product_unit_price"+r).val()){
									if($("#service_product_unit_price"+r).val() != ""){
										//$("#product_vat_amt"+i).val(getLetter(this.value,ZeroNum));
										$("#service_product_unit_price"+r).val(changeCurrencyWithDot(hi[r],$("#service_product_unit_price"+r).val().replace(/,/g,''),sig_digits,r,"service_",calculateLine2));
									}
									hi[r] = $("#service_product_unit_price"+r).val();
								}
								});
								$("#service_vat_amt"+r).on("input",function(){
								if(ik[r] != $("#service_vat_amt"+r).val()){
									if($("#service_vat_amt"+r).val() != ""){
										//$("#product_product_total_price"+i).val(getLetter(this.value,ZeroNum));
										$("#service_vat_amt"+r).val(changeCurrencyWithDot(ik[r],$("#service_vat_amt"+r).val().replace(/,/g,''),sig_digits,r,"service_",calculateLine2));
									}
									ik[r] = $("#service_vat_amt"+r).val();
								}
								});
								$("#service_vat"+r).on("change", function(){
									calculateLine2(r,"service_",sig_digits);
								});
								$("#service_product_total_price"+r).on("input",function(){
								if(kl[r] != $("#service_product_total_price"+r).val()){
									if($("#service_product_total_price"+r).val() != ""){
										//$("#product_product_total_price"+i).val(getLetter(this.value,ZeroNum));
										$("#service_product_total_price"+r).val(changeCurrencyWithDot(kl[r],$("#service_product_total_price"+r).val().replace(/,/g,''),sig_digits,r,"service_",calculateLine2));
									}
									kl[r] = $("#service_product_total_price"+r).val();
								}
								});
								
								$("#service_product_discount"+r).on("input",function(){
									if(lm[r] != $("#service_product_discount"+r).val()){
										if($("#service_product_discount"+r).val() != ""){
											//$("#product_product_unit_price"+i).val(getLetter(this.value,ZeroNum));
											$("#service_product_discount"+r).val(changeCurrencyWithDot(lm[r],$("#service_product_discount"+r).val().replace(/,/g,''),sig_digits,r,"service_",calculateLine2));
										}
										lm[r] = $("#service_product_discount"+r).val();
									}
								});
								$("#service_delete_line"+r).attr("onclick","");
								$("#service_delete_line"+r).attr("onclick","markLineDeleted2("+r+",'service_',sig_digits)");
				}
			}
			// thay định dạng tổng tiền 1 nhóm khi edit
			if(group != null && group != undefined){
				for(let i = 0; i< group.length; i++){
					if($("#group"+i+"total_amt").val() != ""){
						$("#group"+i+"total_amt").val(changeCurrencyWithDotForSum($("#group"+i+"total_amt").val().replace(/,/g,''),sig_digits));
					}
					if($("#group"+i+"discount_amount").val() != ""){
						$("#group"+i+"discount_amount").val(changeCurrencyWithDotForSum($("#group"+i+"discount_amount").val().replace(/,/g,''),sig_digits));
					}
					if($("#group"+i+"subtotal_amount").val() != ""){
						$("#group"+i+"subtotal_amount").val(changeCurrencyWithDotForSum($("#group"+i+"subtotal_amount").val().replace(/,/g,''),sig_digits));
					}
					if($("#group"+i+"tax_amount").val() != ""){
						$("#group"+i+"tax_amount").val(changeCurrencyWithDotForSum($("#group"+i+"tax_amount").val().replace(/,/g,''),sig_digits));
					}
					if($("#group"+i+"total_amount").val() != ""){
						$("#group"+i+"total_amount").val(changeCurrencyWithDotForSum($("#group"+i+"total_amount").val().replace(/,/g,''),sig_digits));
					}
				}
			}
			var group_tax_amount = document.getElementsByClassName("group_tax_amount");
			for(let j = 0; j< group_tax_amount.length; j++){
				group_tax_amount[j].previousElementSibling.innerHTML = "Tiền thuế (1)";
				
			}
			
			//
			$(".add_product_line").on("click",function(){
				k++;
				setTimeout(function(){
				var groupBody1 = document.getElementsByClassName("group_body");
				if(groupBody1 != null && groupBody1 != undefined){
						var productListPrice1 = document.getElementsByClassName("product_part_number");
						if(productListPrice1 != null && productListPrice1 != undefined){
							$("#EditView_product_part_number"+k+"_results").on("click",function(){
								var index = this.id;
								index = String(index);
								index = index.replace('_results', '');
								index = index.slice("28");
								var index2 = this.parentNode.parentNode.parentNode.parentNode.id;
								index2 = String(index2);
								index2 = index2.slice("13");
									setTimeout(function(){
										if($("#product_part_number"+index).val() != ""){
											document.getElementById('product_product_list_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_list_price' + index).value,sig_digits);
											document.getElementById('product_product_cost_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_cost_price' + index).value,sig_digits);
											document.getElementById('product_product_total_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_total_price' + index).value,sig_digits);
											document.getElementById('product_product_unit_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_unit_price' + index).value,sig_digits);
											
											document.getElementById('group'+index2+'total_amt').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'total_amt').value,sig_digits);
											document.getElementById('group'+index2+'discount_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'discount_amount').value,sig_digits);
											document.getElementById('group'+index2+'subtotal_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'subtotal_amount').value,sig_digits);
											document.getElementById('group'+index2+'tax_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'tax_amount').value,sig_digits);
											document.getElementById('group'+index2+'total_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'total_amount').value,sig_digits);
											
											document.getElementById('total_amt').value = changeCurrencyWithDotForSum(document.getElementById('total_amt').value,sig_digits);
											document.getElementById('discount_amount').value = changeCurrencyWithDotForSum(document.getElementById('discount_amount').value,sig_digits);
											document.getElementById('subtotal_amount').value = changeCurrencyWithDotForSum(document.getElementById('subtotal_amount').value,sig_digits);
											document.getElementById('shipping_tax_amt').value = changeCurrencyWithDotForSum(document.getElementById('shipping_tax_amt').value,sig_digits);
											document.getElementById('tax_amount').value = changeCurrencyWithDotForSum(document.getElementById('tax_amount').value,sig_digits);
											document.getElementById('total_amount').value = changeCurrencyWithDotForSum(document.getElementById('total_amount').value,sig_digits);
																															
										}
									},800);
								});
								
								$("#product_part_number"+k).on("keyup",function(event){
									if(event.keyCode == 13){
										var index = this.id;
										index = String(index);
										index = index.slice("19");
										var index2 = this.parentNode.parentNode.parentNode.parentNode.id;
										index2 = String(index2);
										index2 = index2.slice("13");
										setTimeout(function(){
											if($("#product_part_number"+index).val() != ""){
												document.getElementById('product_product_list_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_list_price' + index).value,sig_digits);
												document.getElementById('product_product_cost_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_cost_price' + index).value,sig_digits);
												document.getElementById('product_product_total_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_total_price' + index).value,sig_digits);
												document.getElementById('product_product_unit_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_unit_price' + index).value,sig_digits);
											
												document.getElementById('group'+index2+'total_amt').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'total_amt').value,sig_digits);
												document.getElementById('group'+index2+'discount_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'discount_amount').value,sig_digits);
												document.getElementById('group'+index2+'subtotal_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'subtotal_amount').value,sig_digits);
												document.getElementById('group'+index2+'tax_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'tax_amount').value,sig_digits);
												document.getElementById('group'+index2+'total_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'total_amount').value,sig_digits);
												
												document.getElementById('total_amt').value = changeCurrencyWithDotForSum(document.getElementById('total_amt').value,sig_digits);
												document.getElementById('discount_amount').value = changeCurrencyWithDotForSum(document.getElementById('discount_amount').value,sig_digits);
												document.getElementById('subtotal_amount').value = changeCurrencyWithDotForSum(document.getElementById('subtotal_amount').value,sig_digits);
												document.getElementById('shipping_tax_amt').value = changeCurrencyWithDotForSum(document.getElementById('shipping_tax_amt').value,sig_digits);
												document.getElementById('tax_amount').value = changeCurrencyWithDotForSum(document.getElementById('tax_amount').value,sig_digits);
												document.getElementById('total_amount').value = changeCurrencyWithDotForSum(document.getElementById('total_amount').value,sig_digits);
											}
										},800);
									}
								});
								$("#EditView_product_name"+k+"_results").on("click",function(){
											var index = this.id;
											index = String(index);
											index = index.replace('_results', '');
											index = index.slice("21");
											var index2 = this.parentNode.parentNode.parentNode.parentNode.id;
											index2 = String(index2);
											index2 = index2.slice("13");
											setTimeout(function(){
												if($("#product_name"+index).val() != ""){
													document.getElementById('product_product_list_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_list_price' + index).value,sig_digits);
													document.getElementById('product_product_cost_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_cost_price' + index).value,sig_digits);
													document.getElementById('product_product_total_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_total_price' + index).value,sig_digits);
													document.getElementById('product_product_unit_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_unit_price' + index).value,sig_digits);
													
													document.getElementById('group'+index2+'total_amt').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'total_amt').value,sig_digits);
													document.getElementById('group'+index2+'discount_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'discount_amount').value,sig_digits);
													document.getElementById('group'+index2+'subtotal_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'subtotal_amount').value,sig_digits);
													document.getElementById('group'+index2+'tax_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'tax_amount').value,sig_digits);
													document.getElementById('group'+index2+'total_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'total_amount').value,sig_digits);
													
													document.getElementById('total_amt').value = changeCurrencyWithDotForSum(document.getElementById('total_amt').value,sig_digits);
													document.getElementById('discount_amount').value = changeCurrencyWithDotForSum(document.getElementById('discount_amount').value,sig_digits);
													document.getElementById('subtotal_amount').value = changeCurrencyWithDotForSum(document.getElementById('subtotal_amount').value,sig_digits);
													document.getElementById('shipping_tax_amt').value = changeCurrencyWithDotForSum(document.getElementById('shipping_tax_amt').value,sig_digits);
													document.getElementById('tax_amount').value = changeCurrencyWithDotForSum(document.getElementById('tax_amount').value,sig_digits);
													document.getElementById('total_amount').value = changeCurrencyWithDotForSum(document.getElementById('total_amount').value,sig_digits);
												
												
												}
											},800);
								});
								
								$("#product_name"+k).on("keyup",function(event){
									if(event.keyCode == 13){
											var index = this.id;
												index = String(index);
												index = index.slice("12");
												var index2 = this.parentNode.parentNode.parentNode.parentNode.id;
												index2 = String(index2);
												index2 = index2.slice("13");
											setTimeout(function(){
												if($("#product_name"+index).val() != ""){
													document.getElementById('product_product_list_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_list_price' + index).value,sig_digits);
													document.getElementById('product_product_cost_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_cost_price' + index).value,sig_digits);
													document.getElementById('product_product_total_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_total_price' + index).value,sig_digits);
													document.getElementById('product_product_unit_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_unit_price' + index).value,sig_digits);
													
													document.getElementById('group'+index2+'total_amt').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'total_amt').value,sig_digits);
													document.getElementById('group'+index2+'discount_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'discount_amount').value,sig_digits);
													document.getElementById('group'+index2+'subtotal_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'subtotal_amount').value,sig_digits);
													document.getElementById('group'+index2+'tax_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'tax_amount').value,sig_digits);
													document.getElementById('group'+index2+'total_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'total_amount').value,sig_digits);
													
													document.getElementById('total_amt').value = changeCurrencyWithDotForSum(document.getElementById('total_amt').value,sig_digits);
													document.getElementById('discount_amount').value = changeCurrencyWithDotForSum(document.getElementById('discount_amount').value,sig_digits);
													document.getElementById('subtotal_amount').value = changeCurrencyWithDotForSum(document.getElementById('subtotal_amount').value,sig_digits);
													document.getElementById('shipping_tax_amt').value = changeCurrencyWithDotForSum(document.getElementById('shipping_tax_amt').value,sig_digits);
													document.getElementById('tax_amount').value = changeCurrencyWithDotForSum(document.getElementById('tax_amount').value,sig_digits);
													document.getElementById('total_amount').value = changeCurrencyWithDotForSum(document.getElementById('total_amount').value,sig_digits);
												}
											},800);
									}
								});
								
							for(let i = 0; i < productListPrice1.length; i++){
								k++;
								$("#product_product_list_price"+i).on("focus",function(){
									if(ab[i] == ""){
										ab[i] = $("#product_product_list_price"+i).val();
									}
								});
								$("#product_product_discount"+i).on("focus",function(){
									if(ef[i] == ""){
										ef[i] = $("#product_product_discount"+i).val();
									}
								});
								
								$("#product_product_list_price"+i).on("input",function(){
									if($("#product_product_list_price"+i).val() == ""){
										$("#product_product_unit_price"+i).val("");
									}
									if(ab[i] != $("#product_product_list_price"+i).val()){
										if($("#product_product_list_price"+i).val() != ""){
											//$("#product_product_list_price"+i).val(getLetter(this.value,ZeroNum));
											$("#product_product_list_price"+i).val(changeCurrencyWithDot(ab[i],$("#product_product_list_price"+i).val().replace(/,/g,''),sig_digits,i,"product_",calculateLine2));
										}
										ab[i] = $("#product_product_list_price"+i).val();
									}
								});
								/*
								$("#product_product_unit_price"+i).on("input",function(){
								if(bc[i] != $("#product_product_unit_price"+i).val()){
									if($("#product_product_unit_price"+i).val() != ""){
										//$("#product_product_unit_price"+i).val(getLetter(this.value,ZeroNum));
										$("#product_product_unit_price"+i).val(changeCurrencyWithDot(bc[i],$("#product_product_unit_price"+i).val().replace(/,/g,''),sig_digits,i,"product_",calculateLine2));
									}
									bc[i] = $("#product_product_unit_price"+i).val();
								}
								});
								*/
								$("#product_product_discount"+i).on("input",function(){
								if(ef[i] != $("#product_product_discount"+i).val()){
									if($("#product_product_discount"+i).val() != ""){
										//$("#product_product_unit_price"+i).val(getLetter(this.value,ZeroNum));
										$("#product_product_discount"+i).val(changeCurrencyWithDot(ef[i],$("#product_product_discount"+i).val().replace(/,/g,''),sig_digits,i,"product_",calculateLine2));
									}
									ef[i] = $("#product_product_discount"+i).val();
								}
								});
								$("#product_vat_amt"+i).on("input",function(){
								if(cd[i] != $("#product_vat_amt"+i).val()){
									if($("#product_vat_amt"+i).val() != ""){
										//$("#product_vat_amt"+i).val(getLetter(this.value,ZeroNum));
										$("#product_vat_amt"+i).val(changeCurrencyWithDot(cd[i],$("#product_vat_amt"+i).val().replace(/,/g,''),sig_digits,i,"product_",calculateLine2));
									}
									cd[i] = $("#product_vat_amt"+i).val();
								}
								});
								$("#product_product_total_price"+i).on("input",function(){
								if(de[i] != $("#product_product_total_price"+i).val()){
									if($("#product_product_total_price"+i).val() != ""){
										//$("#product_product_total_price"+i).val(getLetter(this.value,ZeroNum));
										$("#product_product_total_price"+i).val(changeCurrencyWithDot(de[i],$("#product_product_total_price"+i).val().replace(/,/g,''),sig_digits,i,"product_",calculateLine2));
									}
									de[i] = $("#product_product_total_price"+i).val();
								}
								});
								
								$("#product_vat"+i).on("change", function(){
									calculateLine2(i,"product_",sig_digits);
								});
								$("#product_product_qty"+i).attr("onblur","");
								$("#product_product_qty"+i).on("input",function(){
									if($("#product_product_qty"+i).val() != ""){
										calculateLine2(i,"product_",sig_digits);
									}
								});
								$("#product_discount"+i).on("change", function(){
									calculateLine2(i,"product_",sig_digits);
								});
								$("#product_delete_line"+i).attr("onclick","");
								$("#product_delete_line"+i).attr("onclick","markLineDeleted2("+i+",'product_',sig_digits)")
							}
							
						}
					//}
					
				}
				
				
				},500);
			});
			$(".add_service_line").on("click",function(){
				p++;
				setTimeout(function(){
					var groupBody2 = document.getElementsByClassName("group_body");
					if(groupBody2 != null && groupBody2 != undefined){
						var serviceName2 = document.getElementsByClassName("service_name");
						if(serviceName2 != null && serviceName2 != undefined){
							for(let r = 0; r < serviceName2.length; r++){
								$("#service_name"+r).on("input",function(){
									if($("#service_name"+r).val() != ""){
										calculateLine2(r,"service_",sig_digits);
									}
								});
								$("#service_product_list_price"+r).on("input",function(){
								if(fg[r] != $("#service_product_list_price"+r).val()){
									if($("#service_product_list_price"+r).val() != ""){
										//$("#product_product_list_price"+i).val(getLetter(this.value,ZeroNum));
										$("#service_product_list_price"+r).val(changeCurrencyWithDot(fg[r],$("#service_product_list_price"+r).val().replace(/,/g,''),sig_digits,r,"service_",calculateLine2));
									}
									fg[r] = $("#service_product_list_price"+r).val();
								}
								});
								/*
								$("#service_discount"+i).on("input",function(){
								if(gh[i] != $("#service_discount"+i).val()){
									if($("#service_discount"+i).val() != ""){
										//$("#product_product_unit_price"+i).val(getLetter(this.value,ZeroNum));
										$("#service_discount"+i).val(changeCurrencyWithDot(gh[i],$("#service_discount"+i).val().replace(/,/g,''),sig_digits,i,"service_",calculateLine2));
									}
									gh[i] = $("#service_discount"+i).val();
								}
								});
								*/
								$("#service_vat"+r).on("change", function(){
									calculateLine2(r,"service_",sig_digits);
								});
								$("#service_product_unit_price"+r).on("input",function(){
								if(hi[r] != $("#service_product_unit_price"+r).val()){
									if($("#service_product_unit_price"+r).val() != ""){
										//$("#product_vat_amt"+i).val(getLetter(this.value,ZeroNum));
										$("#service_product_unit_price"+r).val(changeCurrencyWithDot(hi[r],$("#service_product_unit_price"+r).val().replace(/,/g,''),sig_digits,r,"service_",calculateLine2));
									}
									hi[r] = $("#service_product_unit_price"+r).val();
								}
								});
								$("#service_vat_amt"+r).on("input",function(){
								if(ik[r] != $("#service_vat_amt"+r).val()){
									if($("#service_vat_amt"+r).val() != ""){
										//$("#product_product_total_price"+i).val(getLetter(this.value,ZeroNum));
										$("#service_vat_amt"+r).val(changeCurrencyWithDot(ik[r],$("#service_vat_amt"+r).val().replace(/,/g,''),sig_digits,r,"service_",calculateLine2));
									}
									ik[r] = $("#service_vat_amt"+r).val();
								}
								});
								$("#service_product_total_price"+r).on("input",function(){
								if(kl[r] != $("#service_product_total_price"+r).val()){
									if($("#service_product_total_price"+r).val() != ""){
										//$("#product_product_total_price"+i).val(getLetter(this.value,ZeroNum));
										$("#service_product_total_price"+r).val(changeCurrencyWithDot(kl[r],$("#service_product_total_price"+r).val().replace(/,/g,''),sig_digits,r,"service_",calculateLine2));
									}
									kl[r] = $("#service_product_total_price"+r).val();
								}
								});
								
								$("#service_product_discount"+r).on("input",function(){
									if(lm[r] != $("#service_product_discount"+r).val()){
										if($("#service_product_discount"+r).val() != ""){
											//$("#product_product_unit_price"+i).val(getLetter(this.value,ZeroNum));
											$("#service_product_discount"+r).val(changeCurrencyWithDot(lm[r],$("#service_product_discount"+r).val().replace(/,/g,''),sig_digits,r,"service_",calculateLine2));
										}
										lm[r] = $("#service_product_discount"+r).val();
									}
								});
								$("#service_delete_line"+r).attr("onclick","");
								$("#service_delete_line"+r).attr("onclick","markLineDeleted2("+r+",'service_',sig_digits)");
							}
						}
						
					}
				},500);
				
			});
			//
		}
		
		$("#addGroup").on("click",function(){
			setTimeout(function(){
			$(".add_product_line").on("click",function(){
				k++;
				setTimeout(function(){
				var groupBody = document.getElementsByClassName("group_body");
				if(groupBody != null && groupBody != undefined){
						var productListPrice = document.getElementsByClassName("product_part_number");
						if(productListPrice != null && productListPrice != undefined){
							$("#EditView_product_part_number"+k+"_results").on("click",function(){
								var index = this.id;
								index = String(index);
								index = index.replace('_results', '');
								index = index.slice("28");
								var index2 = this.parentNode.parentNode.parentNode.parentNode.id;
								index2 = String(index2);
								index2 = index2.slice("13");
									setTimeout(function(){
										if($("#product_part_number"+index).val() != ""){
											document.getElementById('product_product_list_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_list_price' + index).value,sig_digits);
											document.getElementById('product_product_cost_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_cost_price' + index).value,sig_digits);
											document.getElementById('product_product_total_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_total_price' + index).value,sig_digits);
											document.getElementById('product_product_unit_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_unit_price' + index).value,sig_digits);
											
											document.getElementById('group'+index2+'total_amt').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'total_amt').value,sig_digits);
											document.getElementById('group'+index2+'discount_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'discount_amount').value,sig_digits);
											document.getElementById('group'+index2+'subtotal_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'subtotal_amount').value,sig_digits);
											document.getElementById('group'+index2+'tax_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'tax_amount').value,sig_digits);
											document.getElementById('group'+index2+'total_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'total_amount').value,sig_digits);
											
											document.getElementById('total_amt').value = changeCurrencyWithDotForSum(document.getElementById('total_amt').value,sig_digits);
											document.getElementById('discount_amount').value = changeCurrencyWithDotForSum(document.getElementById('discount_amount').value,sig_digits);
											document.getElementById('subtotal_amount').value = changeCurrencyWithDotForSum(document.getElementById('subtotal_amount').value,sig_digits);
											document.getElementById('shipping_tax_amt').value = changeCurrencyWithDotForSum(document.getElementById('shipping_tax_amt').value,sig_digits);
											document.getElementById('tax_amount').value = changeCurrencyWithDotForSum(document.getElementById('tax_amount').value,sig_digits);
											document.getElementById('total_amount').value = changeCurrencyWithDotForSum(document.getElementById('total_amount').value,sig_digits);
																															
										}
									},800);
								});
								
								$("#product_part_number"+k).on("keyup",function(event){
									if(event.keyCode == 13){
										var index = this.id;
										index = String(index);
										index = index.slice("19");
										var index2 = this.parentNode.parentNode.parentNode.parentNode.id;
										index2 = String(index2);
										index2 = index2.slice("13");
										setTimeout(function(){
											if($("#product_part_number"+index).val() != ""){
												document.getElementById('product_product_list_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_list_price' + index).value,sig_digits);
												document.getElementById('product_product_cost_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_cost_price' + index).value,sig_digits);
												document.getElementById('product_product_total_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_total_price' + index).value,sig_digits);
												document.getElementById('product_product_unit_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_unit_price' + index).value,sig_digits);
											
												document.getElementById('group'+index2+'total_amt').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'total_amt').value,sig_digits);
												document.getElementById('group'+index2+'discount_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'discount_amount').value,sig_digits);
												document.getElementById('group'+index2+'subtotal_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'subtotal_amount').value,sig_digits);
												document.getElementById('group'+index2+'tax_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'tax_amount').value,sig_digits);
												document.getElementById('group'+index2+'total_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'total_amount').value,sig_digits);
												
												document.getElementById('total_amt').value = changeCurrencyWithDotForSum(document.getElementById('total_amt').value,sig_digits);
												document.getElementById('discount_amount').value = changeCurrencyWithDotForSum(document.getElementById('discount_amount').value,sig_digits);
												document.getElementById('subtotal_amount').value = changeCurrencyWithDotForSum(document.getElementById('subtotal_amount').value,sig_digits);
												document.getElementById('shipping_tax_amt').value = changeCurrencyWithDotForSum(document.getElementById('shipping_tax_amt').value,sig_digits);
												document.getElementById('tax_amount').value = changeCurrencyWithDotForSum(document.getElementById('tax_amount').value,sig_digits);
												document.getElementById('total_amount').value = changeCurrencyWithDotForSum(document.getElementById('total_amount').value,sig_digits);
											}
										},800);
									}
								});
								$("#EditView_product_name"+k+"_results").on("click",function(){
											var index = this.id;
											index = String(index);
											index = index.replace('_results', '');
											index = index.slice("21");
											var index2 = this.parentNode.parentNode.parentNode.parentNode.id;
											index2 = String(index2);
											index2 = index2.slice("13");
											setTimeout(function(){
												if($("#product_name"+index).val() != ""){
													document.getElementById('product_product_list_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_list_price' + index).value,sig_digits);
													document.getElementById('product_product_cost_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_cost_price' + index).value,sig_digits);
													document.getElementById('product_product_total_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_total_price' + index).value,sig_digits);
													document.getElementById('product_product_unit_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_unit_price' + index).value,sig_digits);
													
													document.getElementById('group'+index2+'total_amt').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'total_amt').value,sig_digits);
													document.getElementById('group'+index2+'discount_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'discount_amount').value,sig_digits);
													document.getElementById('group'+index2+'subtotal_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'subtotal_amount').value,sig_digits);
													document.getElementById('group'+index2+'tax_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'tax_amount').value,sig_digits);
													document.getElementById('group'+index2+'total_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'total_amount').value,sig_digits);
													
													document.getElementById('total_amt').value = changeCurrencyWithDotForSum(document.getElementById('total_amt').value,sig_digits);
													document.getElementById('discount_amount').value = changeCurrencyWithDotForSum(document.getElementById('discount_amount').value,sig_digits);
													document.getElementById('subtotal_amount').value = changeCurrencyWithDotForSum(document.getElementById('subtotal_amount').value,sig_digits);
													document.getElementById('shipping_tax_amt').value = changeCurrencyWithDotForSum(document.getElementById('shipping_tax_amt').value,sig_digits);
													document.getElementById('tax_amount').value = changeCurrencyWithDotForSum(document.getElementById('tax_amount').value,sig_digits);
													document.getElementById('total_amount').value = changeCurrencyWithDotForSum(document.getElementById('total_amount').value,sig_digits);
												
												
												}
											},800);
								});
								
								$("#product_name"+k).on("keyup",function(event){
									if(event.keyCode == 13){
											var index = this.id;
												index = String(index);
												index = index.slice("12");
												var index2 = this.parentNode.parentNode.parentNode.parentNode.id;
												index2 = String(index2);
												index2 = index2.slice("13");
											setTimeout(function(){
												if($("#product_name"+index).val() != ""){
													document.getElementById('product_product_list_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_list_price' + index).value,sig_digits);
													document.getElementById('product_product_cost_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_cost_price' + index).value,sig_digits);
													document.getElementById('product_product_total_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_total_price' + index).value,sig_digits);
													document.getElementById('product_product_unit_price' + index).value = changeCurrencyWithDotForSum(document.getElementById('product_product_unit_price' + index).value,sig_digits);
													
													document.getElementById('group'+index2+'total_amt').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'total_amt').value,sig_digits);
													document.getElementById('group'+index2+'discount_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'discount_amount').value,sig_digits);
													document.getElementById('group'+index2+'subtotal_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'subtotal_amount').value,sig_digits);
													document.getElementById('group'+index2+'tax_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'tax_amount').value,sig_digits);
													document.getElementById('group'+index2+'total_amount').value = changeCurrencyWithDotForSum(document.getElementById('group'+index2+'total_amount').value,sig_digits);
													
													document.getElementById('total_amt').value = changeCurrencyWithDotForSum(document.getElementById('total_amt').value,sig_digits);
													document.getElementById('discount_amount').value = changeCurrencyWithDotForSum(document.getElementById('discount_amount').value,sig_digits);
													document.getElementById('subtotal_amount').value = changeCurrencyWithDotForSum(document.getElementById('subtotal_amount').value,sig_digits);
													document.getElementById('shipping_tax_amt').value = changeCurrencyWithDotForSum(document.getElementById('shipping_tax_amt').value,sig_digits);
													document.getElementById('tax_amount').value = changeCurrencyWithDotForSum(document.getElementById('tax_amount').value,sig_digits);
													document.getElementById('total_amount').value = changeCurrencyWithDotForSum(document.getElementById('total_amount').value,sig_digits);
												}
											},800);
									}
								});
								
							for(let i = k; i < eval(productListPrice.length +k); i++){
								ab[i] = "";
								bc[i] = "";
								cd[i] = "";
								de[i] = "";
								ef[i] = "";
								if(document.getElementById('product_product_list_price'+i) != null && document.getElementById('product_product_list_price'+i) != undefined){
									$("#product_product_list_price"+i).on("focus",function(){
										if(ab[i] == ""){
											ab[i] = $("#product_product_list_price"+i).val();
										}
									});
									$("#product_product_discount"+i).on("focus",function(){
										if(ef[i] == ""){
											ef[i] = $("#product_product_discount"+i).val();
										}
									});
									
									$("#product_product_list_price"+i).on("input",function(){
										if($("#product_product_list_price"+i).val() == ""){
											$("#product_product_unit_price"+i).val("");
										}
										if(ab[i] != $("#product_product_list_price"+i).val()){
											if($("#product_product_list_price"+i).val() != ""){
												//$("#product_product_list_price"+i).val(getLetter(this.value,ZeroNum));
												$("#product_product_list_price"+i).val(changeCurrencyWithDot(ab[i],$("#product_product_list_price"+i).val().replace(/,/g,''),sig_digits,i,"product_",calculateLine2));
											}
											ab[i] = $("#product_product_list_price"+i).val();
										}
									});
									$("#product_product_discount"+i).on("input",function(){
									if(ef[i] != $("#product_product_discount"+i).val()){
										if($("#product_product_discount"+i).val() != ""){
											//$("#product_product_unit_price"+i).val(getLetter(this.value,ZeroNum));
											$("#product_product_discount"+i).val(changeCurrencyWithDot(ef[i],$("#product_product_discount"+i).val().replace(/,/g,''),sig_digits,i,"product_",calculateLine2));
										}
										ef[i] = $("#product_product_discount"+i).val();
									}
									});
									$("#product_vat_amt"+i).on("input",function(){
									if(cd[i] != $("#product_vat_amt"+i).val()){
										if($("#product_vat_amt"+i).val() != ""){
											//$("#product_vat_amt"+i).val(getLetter(this.value,ZeroNum));
											$("#product_vat_amt"+i).val(changeCurrencyWithDot(cd[i],$("#product_vat_amt"+i).val().replace(/,/g,''),sig_digits,i,"product_",calculateLine2));
										}
										cd[i] = $("#product_vat_amt"+i).val();
									}
									});
									$("#product_product_total_price"+i).on("input",function(){
									if(de[i] != $("#product_product_total_price"+i).val()){
										if($("#product_product_total_price"+i).val() != ""){
											//$("#product_product_total_price"+i).val(getLetter(this.value,ZeroNum));
											$("#product_product_total_price"+i).val(changeCurrencyWithDot(de[i],$("#product_product_total_price"+i).val().replace(/,/g,''),sig_digits,i,"product_",calculateLine2));
										}
										de[i] = $("#product_product_total_price"+i).val();
									}
									});
									
									$("#product_vat"+i).on("change", function(){
										calculateLine2(i,"product_",sig_digits);
									});
									$("#product_product_qty"+i).attr("onblur","");
									$("#product_product_qty"+i).on("input",function(){
										if($("#product_product_qty"+i).val() != ""){
											calculateLine2(i,"product_",sig_digits);
										}
									});
									$("#product_discount"+i).on("change", function(){
										calculateLine2(i,"product_",sig_digits);
									});
									$("#product_delete_line"+i).attr("onclick","");
									$("#product_delete_line"+i).attr("onclick","markLineDeleted2("+i+",'product_',sig_digits)");
								}
							}
							
						}
					//}
					
				}
				
				
				},500);
			});
			$(".add_service_line").on("click",function(){
				p++;
				setTimeout(function(){
					var groupBody = document.getElementsByClassName("group_body");
					if(groupBody != null && groupBody != undefined){
						var serviceName = document.getElementsByClassName("service_name");
						if(serviceName != null && serviceName != undefined){
							for(let r = p; r < eval(serviceName.length + p); r++){
								fg[r] = ""; 
								gh[r] = ""; 
								hi[r] = ""; 
								ik[r] = ""; 
								kl[r] = ""; 
								lm[r] = ""; 
								$("#service_name"+r).on("input",function(){
									if($("#service_name"+r).val() != ""){
										calculateLine2(r,"service_",sig_digits);
									}
								});
								$("#service_product_list_price"+r).on("input",function(){
								if(fg[r] != $("#service_product_list_price"+r).val()){
									if($("#service_product_list_price"+r).val() != ""){
										//$("#product_product_list_price"+i).val(getLetter(this.value,ZeroNum));
										$("#service_product_list_price"+r).val(changeCurrencyWithDot(fg[r],$("#service_product_list_price"+r).val().replace(/,/g,''),sig_digits,r,"service_",calculateLine2));
									}
									fg[r] = $("#service_product_list_price"+r).val();
								}
								});
								$("#service_vat"+r).on("change", function(){
									calculateLine2(r,"service_",sig_digits);
								});
								/*
								$("#service_discount"+i).on("input",function(){
								if(gh[i] != $("#service_discount"+i).val()){
									if($("#service_discount"+i).val() != ""){
										//$("#product_product_unit_price"+i).val(getLetter(this.value,ZeroNum));
										$("#service_discount"+i).val(changeCurrencyWithDot(gh[i],$("#service_discount"+i).val().replace(/,/g,''),sig_digits,i,"service_",calculateLine2));
									}
									gh[i] = $("#service_discount"+i).val();
								}
								});
								*/
								$("#service_product_unit_price"+r).on("input",function(){
								if(hi[r] != $("#service_product_unit_price"+r).val()){
									if($("#service_product_unit_price"+r).val() != ""){
										//$("#product_vat_amt"+i).val(getLetter(this.value,ZeroNum));
										$("#service_product_unit_price"+r).val(changeCurrencyWithDot(hi[r],$("#service_product_unit_price"+r).val().replace(/,/g,''),sig_digits,r,"service_",calculateLine2));
									}
									hi[r] = $("#service_product_unit_price"+r).val();
								}
								});
								$("#service_vat_amt"+r).on("input",function(){
								if(ik[r] != $("#service_vat_amt"+r).val()){
									if($("#service_vat_amt"+r).val() != ""){
										//$("#product_product_total_price"+i).val(getLetter(this.value,ZeroNum));
										$("#service_vat_amt"+r).val(changeCurrencyWithDot(ik[r],$("#service_vat_amt"+r).val().replace(/,/g,''),sig_digits,r,"service_",calculateLine2));
									}
									ik[r] = $("#service_vat_amt"+r).val();
								}
								});
								$("#service_product_total_price"+r).on("input",function(){
								if(kl[r] != $("#service_product_total_price"+r).val()){
									if($("#service_product_total_price"+r).val() != ""){
										//$("#product_product_total_price"+i).val(getLetter(this.value,ZeroNum));
										$("#service_product_total_price"+r).val(changeCurrencyWithDot(kl[r],$("#service_product_total_price"+r).val().replace(/,/g,''),sig_digits,r,"service_",calculateLine2));
									}
									kl[r] = $("#service_product_total_price"+r).val();
								}
								});
								
								$("#service_product_discount"+r).on("input",function(){
									if(lm[r] != $("#service_product_discount"+r).val()){
										if($("#service_product_discount"+r).val() != ""){
											//$("#product_product_unit_price"+i).val(getLetter(this.value,ZeroNum));
											$("#service_product_discount"+r).val(changeCurrencyWithDot(lm[r],$("#service_product_discount"+r).val().replace(/,/g,''),sig_digits,r,"service_",calculateLine2));
										}
										lm[r] = $("#service_product_discount"+r).val();
									}
								});
								$("#service_delete_line"+r).attr("onclick","");
								$("#service_delete_line"+r).attr("onclick","markLineDeleted2("+r+",'service_',sig_digits)");
							}
						}
						
					}
				},500);
				
			});
			
			var group_tax_amount = document.getElementsByClassName("group_tax_amount");
			for(let j = 0; j< group_tax_amount.length; j++){
				group_tax_amount[j].previousElementSibling.innerHTML = "Tiền thuế (1)";
				
			}
			},500);
			
		
		});
		$("#shipping_amount").attr("onblur","");
		$("#shipping_amount").on("input",function(){
			if(mn != $("#shipping_amount").val()){
				if($("#shipping_amount").val() != ""){
					$("#shipping_amount").val(changeCurrencyWithDot(mn,$("#shipping_amount").val().replace(/,/g,''),sig_digits,"","lineItems",calculateLine2));
				}
				mn = $("#shipping_amount").val();
			}
		});
		$("#shipping_tax").attr("onchange","");
		$("#shipping_tax").on("change",function(){
			calculateTotal2(sig_digits,"lineItems");
			
		});
		$("#total_amt").attr("onblur","");
		$("#discount_amount").attr("onblur","");
		$("#subtotal_amount").attr("onblur","");
		$("#shipping_tax_amt").attr("onblur","");
		$("#tax_amount").attr("onblur","");
		$("#total_amount").attr("onblur","");
		document.getElementById('total_amt').readOnly = true;
		document.getElementById('discount_amount').readOnly = true;
		document.getElementById('subtotal_amount').readOnly = true;
		document.getElementById('shipping_tax_amt').readOnly = true;
		document.getElementById('tax_amount').readOnly = true;
		document.getElementById('total_amount').readOnly = true;
		//
	}
	if(y == "cs_cusUser" && adetailView == "DetailView"){
		var userId = GetURLParameter('record');
		if(userId != null && userId != undefined){
			setCookie("userId",userId);
			$("#cs_cususer_securitygroups_1_select_button").on("click", function(){
				setCookie("module","cs_cusUser");
			});
		}
		 //var userId = GetURLParameter('record');
		$("#delete_button").removeAttr('onclick');
		 $("#delete_button").on("click",function(){
			 var _form = document.getElementById('formDetailView');
			 _form.return_module.value='cs_cusUser';
			 _form.return_action.value='ListView';
			 _form.action.value='Delete';
			 
			 if(confirm('Bạn chắc muốn xoá hoàn toàn bản ghi?')){
			$.ajax({
				type: "POST",
				url: "index.php?entryPoint=deleteUser",
				data: {
						id:userId,
						module:"cs_cusUser"
					},
				success: function(res){
					 SUGAR.ajaxUI.submitForm(_form); 
				}
			});
			 }
			 
			 // return false;
		});
		
	}
	
	
	
}
	if((y == "Opportunities" && x == "EditView") || (modul == "Opportunities" && adetailView == "DetailView" && (aReturnId != null && aReturnId != undefined))){
			changeCur("amount");
			changeCur("depositsamount");
			changeCur("paymentsamount");
			$("#amount").css("background-color", "yellow");
			$("#depositsamount").css("background-color", "yellow");
			$("#paymentsamount").css("background-color", "yellow");
			$("#amount").css("color", "red");
			$("#depositsamount").css("color", "red");
			$("#paymentsamount").css("color", "red");
			$("#amount").css("font-weight", "bold");
			$("#depositsamount").css("font-weight", "bold");
			$("#paymentsamount").css("font-weight", "bold");
			var field = document.getElementsByClassName("edit-view-row-item");
			
			$(document).ready(function(){
				var prodVal = $("#aos_products_opportunities_1_name").val();
				var oppStatus = document.getElementsByClassName("edit-view-row-item");
				

			});
	}
	var dup = document.getElementsByName("duplicateSave");
	
	if(dup[0] != null && dup[0] != undefined){
	 var duplicateSave = dup[0].value;
	}else{
		var duplicateSave = "";
	}
	if(modul  == "Opportunities" && adetailView == "DetailView" && (aReturnId != null && aReturnId != undefined)){
		//var saleVal = $("#sales_stage").val();
		//if(saleVal != "Prospecting"){
		//$("#btn_aos_products_opportunities_1_name").removeAttr('onclick');
		//$("#btn_clr_aos_products_opportunities_1_name").removeAttr('onclick');
		//}
		
	}
	if(y == "Leads" && x == "DetailView" && (recor != null && recor != undefined)){
		var rowItem = document.getElementsByClassName("detail-view-row-item");
		rowItem[10].style.display = "block";
	}
	if(y=="Opportunities" && x=="DetailView"){
		$(document).ready(function(){
			var statusText = document.getElementsByClassName("detail-view-field")[8];
			var depositText = document.getElementsByClassName("detail-view-field")[10];
			var depo = document.getElementById("depositdate");	
			if(statusText.innerText == "Prospecting" || statusText.innerText == "Tìm hiểu nhu cầu"){
				statusText.innerHTML = '<div id="statusText" style="background-color:#3ADF00;border-radius:10px;text-align:center;font-weight:bold;color:white;width:40%">'+statusText.innerHTML+'</div>';
				if(depo.innerText != null && depo.innerText != '' && depo.innerText != undefined){
					depositText.innerHTML = '<div id="depositText" style="background-color:#3ADF00;border-radius:10px;text-align:center;font-weight:bold;color:white;width:40%">'+depositText.innerHTML+'</div>';
				}else{
					depositText.innerHTML = '<div id="depositText" style="border-radius:10px;text-align:center;font-weight:bold;color:white;width:40%">'+depositText.innerHTML+'</div>';
				}
			}else if(statusText.innerText == "Closed Lost" || statusText.innerText == "Hủy cọc" || statusText.innerText == "Hủy giữ chỗ" || statusText.innerText == "Hủy tìm hiểu" || statusText.innerText == "Chốt Thua"){
				statusText.innerHTML = '<div id="statusText" style="background-color:#00BFFF;border-radius:10px;text-align:center;font-weight:bold;color:white;width:40%" >'+statusText.innerHTML+'</div>';
				if(depo.innerText != null && depo.innerText != '' && depo.innerText != undefined){
					depositText.innerHTML = '<div id="depositText" style="background-color:#00BFFF;border-radius:10px;text-align:center;font-weight:bold;color:white;width:40%">'+depositText.innerHTML+'</div>';
				}else{
					depositText.innerHTML = '<div id="depositText" style="border-radius:10px;text-align:center;font-weight:bold;color:white;width:40%">'+depositText.innerHTML+'</div>';
				}
			}else if(statusText.innerText == "Negotiation/Review" || statusText.innerText == "Đặt cọc" || statusText.innerText == "Thương lượng đàm phán"){
				statusText.innerHTML = '<div id="statusText" style="background-color:#FFBF00;border-radius:10px;text-align:center;font-weight:bold;color:white;width:40%" >'+statusText.innerHTML+'</div>';	
				if(depo.innerText != null && depo.innerText != '' && depo.innerText != undefined){
					depositText.innerHTML = '<div id="depositText" style="background-color:#FFBF00;border-radius:10px;text-align:center;font-weight:bold;color:white;width:40%">'+depositText.innerHTML+'</div>';
				}else{
					depositText.innerHTML = '<div id="depositText" style="border-radius:10px;text-align:center;font-weight:bold;color:white;width:40%">'+depositText.innerHTML+'</div>';
				}
			}else if(statusText.innerText == "Closed Won" || statusText.innerText == "Chốt mua" || statusText.innerText == "Chốt Thắng"){
				statusText.innerHTML = '<div id="statusText" style="background-color:#FF8000;border-radius:10px;text-align:center;font-weight:bold;color:white;width:40%" >'+statusText.innerHTML+'</div>';
				if(depo.innerText != null && depo.innerText != '' && depo.innerText != undefined){
					depositText.innerHTML = '<div id="depositText" style="background-color:#FF8000;border-radius:10px;text-align:center;font-weight:bold;color:white;width:40%">'+depositText.innerHTML+'</div>';
				}else{
					depositText.innerHTML = '<div id="depositText" style="border-radius:10px;text-align:center;font-weight:bold;color:white;width:40%">'+depositText.innerHTML+'</div>';
				}
			}else if(statusText.innerText == "Proposal/Price Quote" || statusText.innerText == "Giữ chỗ" || statusText.innerText == "Đề xuất giải pháp / Báo giá"){
				statusText.innerHTML = '<div id="statusText" style="background-color: #F7D358;border-radius:10px;text-align:center;font-weight:bold;color:white;width:40%" >'+statusText.innerHTML+'</div>';	
				if(depo.innerText != null && depo.innerText != '' && depo.innerText != undefined){
					depositText.innerHTML = '<div id="depositText" style="background-color: #F7D358;border-radius:10px;text-align:center;font-weight:bold;color:white;width:40%">'+depositText.innerHTML+'</div>';
				}else{
					depositText.innerHTML = '<div id="depositText" style="border-radius:10px;text-align:center;font-weight:bold;color:white;width:40%">'+depositText.innerHTML+'</div>';
				}
			}
			
			/*
			var amount = document.getElementById("amount").innerText;
			setInterval(function(){
				if(((document.getElementById("amount").val() != null || document.getElementById("amount").val() != "") && $("#aos_products_opportunities_1_name").val() != amount)){
					$.ajax({
						type: "POST",
						url: "index.php?entryPoint=getDeposite",
						success: function(pData) {
							var amount = document.getElementById("amount").innerText;
							var depositsAmount = parseInt(parseInt(amount) * (pData / 100));
							document.getElementById("depositsamount").innerText = (changeCurrency(String(parseInt(depositsAmount))));
							var paymentsAmount = amount - parseInt(depositsAmount);
							document.getElementById("paymentsamount").innerText = (changeCurrency(String(paymentsAmount)));
						}
					});
				}
			},1000)
			*/
			$.ajax({
				type: "POST",
				url: "index.php?entryPoint=getOppLanguage",
				dataType:"json",
				success: function(data) {
					var statuss = document.getElementsByClassName("inlineEdit")[2];
					statuss.innerHTML = '<div id="oppStatus">'+statuss.innerHTML+'</div>';
					$('<li><input type="button" class="btn btn-danger email-address-add-button"  style="background-color:#00BFFF" id="opp3" value="'+data.LBL_Closed_Lost_button+'" onclick="updateStatus3(\''+data.LBL_Closed_Lost_Alert+'\',\''+data.LBL_Closed_Lost_Alert+'\',\''+data.LBL_Closed_Lost_Alert+'\',\''+data.LBL_Closed_Lost+'\');"></li>' ).insertAfter($("#tab-actions"));
					$('<li><input type="button" class="btn btn-danger email-address-add-button"  style="background-color:#FF8000" id="opp2" value="'+data.LBL_Closed_Won_button+'" onclick="updateStatus2(\''+data.LBL_Closed_Won_Alert+'\',\''+data.LBL_Closed_Won+'\');"></li>' ).insertAfter($("#tab-actions"));
					$('<li><input type="button" class="btn btn-danger email-address-add-button"  style="background-color:#FFBF00" id="opp1" value="'+data.LBL_Negotiation_Review_button+'" onclick="updateStatus1(\''+data.LBL_Negotiation_Review_Alert+'\',\''+data.LBL_Negotiation_Review+'\',\''+data.LBL_Closed_Lost_button+'\');"></li>' ).insertAfter($("#tab-actions"));
					$('<li><input type="button" class="btn btn-danger email-address-add-button"  style="background-color:#F7D358" id="opp0" value="'+data.LBL_Proposal_Price_Quote_button+'" onclick="updateStatus(\''+data.LBL_Proposal_Price_Quote_Alert+'\',\''+data.LBL_Closed_Lost_button+'\');"></li>' ).insertAfter($("#tab-actions"));
				}
			});
			
			
			
		});
		
	}
	if((y=="AOS_Products" && x == "EditView") || (amodule == "AOS_Products" && adetailView == "DetailView")){
		changeCur("price");
	}
	if(y=="rls_Reports" && x=="DetailView"){
		var val = $(".provinceDistrict [id*='provincecode_c']").val();
		$(".provinceDistrict [id*='districtcode_c'] option").css('display','none');
		if(val != null && val != '' && val != undefined){
			setTimeout(function(){
			$(".provinceDistrict [id*='districtcode_c'] > [value*="+val+"_]").css('display','block');
			$(".provinceDistrict [id*='districtcode_c']").val(val+"_");
			},200);
		}
		$(".provinceDistrict [id*='provincecode_c']").on('change',function(){
			 val = $(this).val();
			$(".provinceDistrict [id*='districtcode_c'] option").css('display','none');
			setTimeout(function(){
			$(".provinceDistrict [id*='districtcode_c'] > [value*="+val+"_]").css('display','block');
			$(".provinceDistrict [id*='districtcode_c']").val(val+"_");
			},200);
		});
	}
	
	if(((y == "Leads" || y == "Accounts" || y == "Contacts" || y  == "Prospects" || y == "Calls" || y == "Meetings" || y == "Opportunities" ||  y == "Cases" || y == "Notes" || y == "Campaigns" || y == "SecurityGroups" || y == "bill_Billing") && (x=="index" || x == "ListView")) || ((modul == "Leads" || modul  == "Accounts" || modul  == "Contacts" || modul  == "Prospects" || modul  == "Opportunities" || modul  == "Calls" || modul  == "Meetings" || modul == "Cases" || modul == "Notes" || modul == "Campaigns" || modul == "SecurityGroups" || modul == "bill_Billing") && adetailView == "index" && x != "EditView")){
	
	//if(((y == "Leads" || y == "Accounts" || y == "Contacts" || y == "Calls" || y == "Meetings" || y == "Opportunities" ) && x=="index")){	
	var listRound = document.getElementsByClassName("list-view-rounded-corners");
	if(listRound[0] != null && listRound[0] != undefined){
		listRound[0].style.marginTop = "30px";
		
	}
		
		$(document).ready(function(){
		var modulee = GetURLParameter("module");
		if(modulee != null && modulee != undefined){
		setCookie("modulee",modulee);
		}
		var div = document.createElement("div");
		div.setAttribute("id", "province");
		var html = '';
html += '<div class="row">';
html += '<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left" >';
html += '<select style="width:200px" name="group-users[]" id="provincecode" onchange="loadGroup2()">';
                                                                       html += '<option class="deleteRes" value="" selected="selected">----</option>';                          
                                                                        
                                                                        html += '<option style="display:none;" label=" TP Hà Nội" value="10" id="10"> TP Hà Nội</option>';
                                                                       html += ' <option style="display:none" label=" Tỉnh Hưng Yên" value="16" id="16"> Tỉnh Hưng Yên</option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hải Dương" value="17" id="17"> Tỉnh Hải Dương</option>';
                                                                        html += '<option style="display:none" label=" TP Hải Phòng " value="18" id="18"> TP Hải Phòng </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Ninh " value="20" id="20"> Tỉnh Quảng Ninh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bắc Ninh " value="22" id="22"> Tỉnh Bắc Ninh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bắc Giang " value="23" id="23"> Tỉnh Bắc Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Lạng Sơn " value="24" id="24"> Tỉnh Lạng Sơn </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Thái Nguyên " value="25" id="25"> Tỉnh Thái Nguyên </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bắc Kạn " value="26" id="26"> Tỉnh Bắc Kạn </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Cao Bằng " value="27" id="27"> Tỉnh Cao Bằng </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Vĩnh Phúc " value="28" id="28"> Tỉnh Vĩnh Phúc </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Phú Thọ " value="29" id="29"> Tỉnh Phú Thọ </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Tuyên Quang " value="30" id="30"> Tỉnh Tuyên Quang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hà Giang " value="31" id="31"> Tỉnh Hà Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Yên Bái " value="32" id="32"> Tỉnh Yên Bái </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Lào Cai " value="33" id="33"> Tỉnh Lào Cai </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hoà Bình " value="35" id="35"> Tỉnh Hoà Bình </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Sơn La " value="36" id="36"> Tỉnh Sơn La </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Điện Biên " value="38" id="38"> Tỉnh Điện Biên </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Lai Châu " value="39" id="39"> Tỉnh Lai Châu </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hà Nam " value="40" id="40"> Tỉnh Hà Nam </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Thái Bình " value="41" id="41"> Tỉnh Thái Bình </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Nam Định " value="42" id="42"> Tỉnh Nam Định </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Ninh Bình " value="43" id="43"> Tỉnh Ninh Bình </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Thanh Hoá " value="44" id="44"> Tỉnh Thanh Hoá </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Nghệ An " value="46" id="46"> Tỉnh Nghệ An </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hà Tĩnh " value="48" id="48"> Tỉnh Hà Tĩnh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Bình " value="51" id="51"> Tỉnh Quảng Bình </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Trị " value="52" id="52"> Tỉnh Quảng Trị </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Thừa Thiên Huế " value="53" id="53"> Tỉnh Thừa Thiên Huế </option>';
                                                                        html += '<option style="display:none" label=" TP Đà Nẵng " value="55" id="55"> TP Đà Nẵng </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Nam " value="56" id="56"> Tỉnh Quảng Nam </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Ngãi " value="57" id="57"> Tỉnh Quảng Ngãi </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Kon Tum " value="58" id="58"> Tỉnh Kon Tum </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bình Định " value="59" id="59"> Tỉnh Bình Định </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Gia Lai " value="60" id="60"> Tỉnh Gia Lai </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Phú Yên " value="62" id="62"> Tỉnh Phú Yên </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Đắk Lăk " value="63" id="63"> Tỉnh Đắk Lăk </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Đắk Nông " value="64" id="64"> Tỉnh Đắk Nông </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Khánh Hoà " value="65" id="65"> Tỉnh Khánh Hoà </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Ninh Thuận " value="66" id="66"> Tỉnh Ninh Thuận </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Lâm Đồng " value="67" id="67"> Tỉnh Lâm Đồng </option>';
                                                                        html += '<option style="display:none" label=" TP Hồ Chí Minh " value="70" id="70"> TP Hồ Chí Minh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bà Rịa Vũng Tàu " value="79" id="79"> Tỉnh Bà Rịa Vũng Tàu </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bình Thuận " value="80" id="80"> Tỉnh Bình Thuận </option>';
                                                                        html += '<option style="display:none;" label=" Tỉnh Đồng Nai " value="81" id="81"> Tỉnh Đồng Nai </option>';
                                                                        html += '<option style="display:none;" label=" Tỉnh Bình Dương " value="82" id="82"> Tỉnh Bình Dương </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bình Phước " value="83" id="83"> Tỉnh Bình Phước </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Tây Ninh " value="84" id="84"> Tỉnh Tây Ninh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Long An " value="85" id="85"> Tỉnh Long An </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Tiền Giang " value="86" id="86"> Tỉnh Tiền Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Đồng Tháp " value="87" id="87"> Tỉnh Đồng Tháp </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh An Giang " value="88" id="88"> Tỉnh An Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Vĩnh Long " value="89" id="89"> Tỉnh Vĩnh Long </option>';
                                                                        html += '<option style="display:none" label=" TP Cần Thơ " value="90" id="90"> TP Cần Thơ </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hậu Giang " value="91" id="91"> Tỉnh Hậu Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Kiên Giang " value="92" id="92"> Tỉnh Kiên Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bến Tre " value="93" id="93"> Tỉnh Bến Tre </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Trà Vinh " value="94" id="94"> Tỉnh Trà Vinh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Sóc Trăng " value="95" id="95"> Tỉnh Sóc Trăng </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bạc Liêu " value="96" id="96"> Tỉnh Bạc Liêu </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Cà Mau " value="97" id="97"> Tỉnh Cà Mau </option>';
                                                                        html += '<option style="display:none" label=" Vnpost " value="98" id="98"> Vnpost </option>';
                                                                        html += '<option style="display:none" label=" PPTT " value="99" id="99"> PPTT </option>';
                                                                        html += '<option style="display:none" label=" DVBC " value="100" id="100"> DVBC </option>';
                                                                        html += '<option style="display:none" label=" TCBC " value="101" id="101"> TCBC </option>';
                                                                        html += '<option style="display:none" label=" HCC " value="102" id="102"> HCC </option>';
																		html += '<option style="display:none" label=" Global " value="103" id="103"> Global </option>';
                                                                    html += '</select>';
                                                                    html += '</div>';
                                                                    html += '<div id="groupUser" style="width:250px;" class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left" tabindex="0">';
                                                                    html += '<input style="width:250px;" placeholder="Search.." id="myInput" onkeyup="filterKeyy()">';
                                                                    html += ' <select style="width:250px;height:100px;display:none" name="group-users[]" id="group-users" multiple="multiple">';
																	html += '</select>';
																	html += '</div>';
																	html += '<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left">';
																	html += '<form id="groupName" method="POST" action="">';
																	html += '<input type=text" style="display:none" id="grouppname" name="groupName" value="">';
																	
																	html += '<span style="margin-left:20px"><input type="button" id="loadgroup" onclick="loadGroup3()" value="Lọc"></span>';
																	html += '<span style="margin-left:20px"><input class="deleteRes" type="button" value="Xóa đk lọc"></span>';
																	//if(y != null && y != undefined){
																	//html += '<span><a id="clear" href="index.php?module='+y+'&action=index&return_module=Leads&return_action=DetailView&clear=1"><input type="button" value="Xóa ĐK Lọc"></a></span>';
																	//}else{
																	//html += '<span><a id="clear" onclick="SUGAR.savedViews.setChooser()"><input type="button" value="Xóa ĐK Lọc"></a></span>';
																	//}
																	html += '</form>';
																	html += '</div>';
																	html += '<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left">';
																	html += '</div>';																	
                                                                  html += '</div>';

    var pagecontent = document.getElementById("pagecontent");
    var listViewBody = document.getElementsByClassName("listViewBody")[0];   
    pagecontent.insertBefore(div,listViewBody);
    document.getElementById("province").innerHTML = html;
	var action = GetURLParameter("action");
	var resetCondition = GetURLParameter("resetCondition");
	var parentTab = GetURLParameter("parentTab");
	var provincee = GetURLParameter("province");
	showProvince();
	//if((action != null && action != undefined) && (modulee != null && modulee != undefined) && (parentTab == null || parentTab == undefined) && (provincee == null || provincee == undefined)){
	
		var prov = getCookie("province");
		if(prov != null && prov != undefined && prov != '' ){
			   document.getElementById("provincecode").value = prov;
			   var title = document.getElementsByClassName("module-title-text")[0].innerHTML;
			  $(".deleteRes").on("click",function(){
					SUGAR.ajaxUI.showLoadingPanel();
					$.ajax({
                type: "POST",
                url: "index.php?module="+y+"&action=index&searchFormTab=basic_search&query=true&orderBy=DATE_ENTERED&sortOrder=DESC&current_user_only_basic=0&favorites_only_basic=0&button=Tìm kiếm&resetCondition=1&sugar_body_only=true",
                dataType: "html",
                success: function(data) {
					setCookie("group","");
					setCookie("province","");
					var html3 = '';
					html3 += '<div class="moduleTitle">';
					html3 += '<h2 class="module-title-text">'+title+'</h2>';
					html3 += '<span class="utils">';
					html3 += '<a href="#" class="btn btn-success showsearch">';
					html3 += '<span class=" glyphicon glyphicon-search" aria-hidden="true">';
					html3 += '</span>';
					html3 += '</a>&nbsp;';
					html3 += '<a id="create_image" href="index.php?module='+y+'&amp;action=EditView&amp;return_module='+y+'&amp;return_action=DetailView" class="utilsLink">';
					html3 += '<img src="themes/default/images/create-record.gif?v=golOgr3LHb5uVI36fQV1hQ" alt="Tạo">';
					html3 += '</a>';
					html3 += '<a id="create_link" href="index.php?module='+y+'&amp;action=EditView&amp;return_module='+y+'&amp;return_action=DetailView" class="utilsLink">';
					html3 += 'Tạo';
					html3 += '</a>';
					html3 += '</span>';
					html3 += '<div class="clear">';
					html3 += '</div>';
					html3 += '</div>';
					
					var html = '';
html += '<div class="row">';
html += '<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left" >';
html += '<select style="width:200px" name="group-users[]" id="provincecode" onchange="loadGroup2()">';
                                                                       html += '<option class="deleteRes" value="" selected="selected">----</option>';                          
                                                                      
                                                                        html += '<option style="display:none;" label=" TP Hà Nội" value="10" id="10"> TP Hà Nội</option>';
                                                                       html += ' <option style="display:none" label=" Tỉnh Hưng Yên" value="16" id="16"> Tỉnh Hưng Yên</option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hải Dương" value="17" id="17"> Tỉnh Hải Dương</option>';
                                                                        html += '<option style="display:none" label=" TP Hải Phòng " value="18" id="18"> TP Hải Phòng </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Ninh " value="20" id="20"> Tỉnh Quảng Ninh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bắc Ninh " value="22" id="22"> Tỉnh Bắc Ninh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bắc Giang " value="23" id="23"> Tỉnh Bắc Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Lạng Sơn " value="24" id="24"> Tỉnh Lạng Sơn </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Thái Nguyên " value="25" id="25"> Tỉnh Thái Nguyên </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bắc Kạn " value="26" id="26"> Tỉnh Bắc Kạn </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Cao Bằng " value="27" id="27"> Tỉnh Cao Bằng </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Vĩnh Phúc " value="28" id="28"> Tỉnh Vĩnh Phúc </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Phú Thọ " value="29" id="29"> Tỉnh Phú Thọ </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Tuyên Quang " value="30" id="30"> Tỉnh Tuyên Quang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hà Giang " value="31" id="31"> Tỉnh Hà Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Yên Bái " value="32" id="32"> Tỉnh Yên Bái </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Lào Cai " value="33" id="33"> Tỉnh Lào Cai </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hoà Bình " value="35" id="35"> Tỉnh Hoà Bình </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Sơn La " value="36" id="36"> Tỉnh Sơn La </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Điện Biên " value="38" id="38"> Tỉnh Điện Biên </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Lai Châu " value="39" id="39"> Tỉnh Lai Châu </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hà Nam " value="40" id="40"> Tỉnh Hà Nam </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Thái Bình " value="41" id="41"> Tỉnh Thái Bình </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Nam Định " value="42" id="42"> Tỉnh Nam Định </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Ninh Bình " value="43" id="43"> Tỉnh Ninh Bình </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Thanh Hoá " value="44" id="44"> Tỉnh Thanh Hoá </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Nghệ An " value="46" id="46"> Tỉnh Nghệ An </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hà Tĩnh " value="48" id="48"> Tỉnh Hà Tĩnh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Bình " value="51" id="51"> Tỉnh Quảng Bình </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Trị " value="52" id="52"> Tỉnh Quảng Trị </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Thừa Thiên Huế " value="53" id="53"> Tỉnh Thừa Thiên Huế </option>';
                                                                        html += '<option style="display:none" label=" TP Đà Nẵng " value="55" id="55"> TP Đà Nẵng </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Nam " value="56" id="56"> Tỉnh Quảng Nam </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Quảng Ngãi " value="57" id="57"> Tỉnh Quảng Ngãi </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Kon Tum " value="58" id="58"> Tỉnh Kon Tum </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bình Định " value="59" id="59"> Tỉnh Bình Định </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Gia Lai " value="60" id="60"> Tỉnh Gia Lai </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Phú Yên " value="62" id="62"> Tỉnh Phú Yên </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Đắk Lăk " value="63" id="63"> Tỉnh Đắk Lăk </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Đắk Nông " value="64" id="64"> Tỉnh Đắk Nông </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Khánh Hoà " value="65" id="65"> Tỉnh Khánh Hoà </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Ninh Thuận " value="66" id="66"> Tỉnh Ninh Thuận </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Lâm Đồng " value="67" id="67"> Tỉnh Lâm Đồng </option>';
                                                                        html += '<option style="display:none" label=" TP Hồ Chí Minh " value="70" id="70"> TP Hồ Chí Minh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bà Rịa Vũng Tàu " value="79" id="79"> Tỉnh Bà Rịa Vũng Tàu </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bình Thuận " value="80" id="80"> Tỉnh Bình Thuận </option>';
                                                                        html += '<option style="display:none;" label=" Tỉnh Đồng Nai " value="81" id="81"> Tỉnh Đồng Nai </option>';
                                                                        html += '<option style="display:none;" label=" Tỉnh Bình Dương " value="82" id="82"> Tỉnh Bình Dương </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bình Phước " value="83" id="83"> Tỉnh Bình Phước </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Tây Ninh " value="84" id="84"> Tỉnh Tây Ninh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Long An " value="85" id="85"> Tỉnh Long An </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Tiền Giang " value="86" id="86"> Tỉnh Tiền Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Đồng Tháp " value="87" id="87"> Tỉnh Đồng Tháp </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh An Giang " value="88" id="88"> Tỉnh An Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Vĩnh Long " value="89" id="89"> Tỉnh Vĩnh Long </option>';
                                                                        html += '<option style="display:none" label=" TP Cần Thơ " value="90" id="90"> TP Cần Thơ </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Hậu Giang " value="91" id="91"> Tỉnh Hậu Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Kiên Giang " value="92" id="92"> Tỉnh Kiên Giang </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bến Tre " value="93" id="93"> Tỉnh Bến Tre </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Trà Vinh " value="94" id="94"> Tỉnh Trà Vinh </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Sóc Trăng " value="95" id="95"> Tỉnh Sóc Trăng </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Bạc Liêu " value="96" id="96"> Tỉnh Bạc Liêu </option>';
                                                                        html += '<option style="display:none" label=" Tỉnh Cà Mau " value="97" id="97"> Tỉnh Cà Mau </option>';
                                                                        html += '<option style="display:none" label=" Vnpost " value="98" id="98"> Vnpost </option>';
                                                                        html += '<option style="display:none" label=" PPTT " value="99" id="99"> PPTT </option>';
                                                                        html += '<option style="display:none" label=" DVBC " value="100" id="100"> DVBC </option>';
                                                                        html += '<option style="display:none" label=" TCBC " value="101" id="101"> TCBC </option>';
                                                                        html += '<option style="display:none" label=" HCC " value="102" id="102"> HCC </option>';
																		html += '<option style="display:none" label=" Global " value="103" id="103"> Global </option>';
                                                                    html += '</select>';
                                                                    html += '</div>';
                                                                    html += '<div id="groupUser" style="width:250px;" class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left" tabindex="0">';
                                                                    html += '<input style="width:250px;" placeholder="Search.." id="myInput" onkeyup="filterKeyy()">';
                                                                    html += ' <select style="width:250px;height:100px;display:none" name="group-users[]" id="group-users" multiple="multiple">';
																	html += '</select>';
																	html += '</div>';
																	html += '<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left">';
																	html += '<form id="groupName" method="POST" action="">';
																	html += '<input type=text" style="display:none" id="grouppname" name="groupName" value="">';
																	
																	html += '<span style="margin-left:20px"><input type="button" id="loadgroup" onclick="loadGroup3()" value="Lọc"></span>';
																	html += '<span style="margin-left:20px"><input type="button" class="deleteRes" value="Xóa đk lọc"></span>';
																	//if(y != null && y != undefined){
																	//html += '<span><a id="clear" href="index.php?module='+y+'&action=index&return_module=Leads&return_action=DetailView&clear=1"><input type="button" value="Xóa ĐK Lọc"></a></span>';
																	//}else{
																	//html += '<span><a id="clear" onclick="SUGAR.savedViews.setChooser()"><input type="button" value="Xóa ĐK Lọc"></a></span>';
																	//}
																	html += '</form>';
																	html += '</div>';
																	html += '<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left">';
																	html += '</div>';																	
                                                                  html += '</div>';
					var data1 = html3 + html + data;
					
					$("#pagecontent").html(data1);
					$(document).ready(function(){
						var nextButton = document.getElementById("listViewNextButton_top");
						var prevButton = document.getElementById("listViewPrevButton_top");
						var endButton = document.getElementById("listViewEndButton_top");
						var startButton = document.getElementById("listViewStartButton_top");
						if((nextButton != null && nextButton != undefined) && nextButton.getAttribute("disabled") != "disabled"){
							var changeClick = nextButton.getAttribute("onclick").replace("sListView.save_checks", "save_checks2");
							nextButton.setAttribute("onclick",changeClick);
						}
						if((prevButton != null && prevButton != undefined) && prevButton.getAttribute("disabled") != "disabled"){
							var changeClick2 = prevButton.getAttribute("onclick").replace("sListView.save_checks", "save_checks2");
							prevButton.setAttribute("onclick",changeClick2);
						}
						if((endButton != null && endButton != undefined) && endButton.getAttribute("disabled") != "disabled"){
							var changeClick3 = endButton.getAttribute("onclick").replace("sListView.save_checks", "save_checks2");
							endButton.setAttribute("onclick",changeClick3);
						}
						if((startButton != null && startButton != undefined) && startButton.getAttribute("disabled") != "disabled"){
							var changeClick4 = startButton.getAttribute("onclick").replace("sListView.save_checks", "save_checks2");
							startButton.setAttribute("onclick",changeClick4);
						}
		var modulee = GetURLParameter("module");
		if(modulee != null && modulee != undefined){
		setCookie("modulee",modulee);
		}
		
	showProvince();
	
	//var action = GetURLParameter("action");
	//if((action != null && action != undefined) && (modulee != null && modulee != undefined) && (parentTab == null || parentTab == undefined) && (provincee == null || provincee == undefined)){
	document.getElementById("provincecode").value = "";
	var groupSaved = getCookie("group");
	if(groupSaved != null && groupSaved != undefined && groupSaved != ''){
			groupSaved = JSON.parse(groupSaved);
				setTimeout(function(){
					var All;
					var checkbxName=[];
					var checkboxName =  document.getElementsByName("checkbxName");
					var length = checkboxName.length; 
					var length2 = groupSaved.length; 
					for(let i=0; i < length; i++)
					{
						for(let j = 0 ; j < length2; j++){	
								if(groupSaved[j] == checkboxName[i].value){
									checkboxName[i].checked = true;
								}
							
						}
					} 
					
					document.getElementById("grouppname").value = checkbxName;
					document.getElementById("myInput").value = groupSaved;
						//setCookie("group",JSON.stringify(checkbxName));
				},1500);
	}
				$.ajax({
							type: "POST",
							url: "index.php?entryPoint=getGroup",
							data: {
								id:groupId2
							},
							dataType: "json",
							success: function(data) {
								if(data != null && data != '' && data != undefined){
									var obj=data;
									html="";
									html+='<input style="width:250px;" placeholder="Search.." id="myInput" onkeyup="filterKeyy()">';
									html+='<div tabindex="1" style="background-color:white;width:250px;height:100px;overflow:auto;display:none;position:absolute;z-index:10000" name="group-users[]" id="group-users" >';
									html+='<div onclick="getUser();getTitle()"><input type="checkbox" class="checkbx" name="checkbxName" value="All">All</div>';
									for(let i=0;i<obj.length;i++){
										html+='<div><input type="checkbox" class="checkbx" name="checkbxName" value="'+obj[i].name+'">'+obj[i].name+'</div>';
									}
									html+='</div>';
									$('#groupUser').html(html);
									$('#users').html('');
									$('#myInput').on("focus",function(){
										if(document.getElementById('group-users').style.display == "none"){
										$('#group-users').css("display","block");
										}
										// document.getElementById('group-users').focus();
									});
									//$('.checkbx').on("click",function(){
										 //clicked = true;
									//	 $("#group-users").trigger("focus");
									//});
									//document.getElementById('groupUser').onmouseenter = function(){
									//	 clicked = true;
									//	document.getElementById('group-users').focus();
									//};
									document.getElementById('groupUser').onmouseover = function(){
										 clicked = true;
										document.getElementById('group-users').focus();
									};
									
									document.getElementById('groupUser').onmouseleave = function(){
										 clicked = false;
									};
									$('#myInput').on("blur",function(){
										if(clicked != true){
										$('#group-users').css("display","none");
										}
									});
									$(".checkbx").on("click",function(){
										clicked = true;
										document.getElementById('group-users').focus();
										var All;
										var checkbxName=[];
										var checkboxName =  document.getElementsByName("checkbxName");
										var length = checkboxName.length; 
										for(let i=0; i < length; i++)
										{	
											if(checkboxName[i].checked == true){
												if(checkboxName[i].value == "All"){
													All = true;
													break;
												}else{
														checkbxName.push(checkboxName[i].value);
												}
											}
										} 
										if(All == true){
											checkbxName=[];
											for(let j=1; j < length; j++){
												checkbxName.push(checkboxName[j].value);
											}
										}
										document.getElementById("grouppname").value = checkbxName;
										document.getElementById("myInput").value = checkbxName;
										setCookie("group",JSON.stringify(checkbxName));
									});
									$('#group-users').on("blur",function(){
										if(clicked != true){
										$('#group-users').css("display","none");
										}else{
										document.getElementById('group-users').focus();	
										}
									});
									$("#loadgroup").on("click",function(){
									loadGroup3();
									});
								}
								
								setTimeout(function(){
									SUGAR.ajaxUI.hideLoadingPanel();
								},700);
							},
							error: function (error) {
								
							}
						});
				
				
		});
                },
                error: function (error) {
                    
                }
            });
				});
		}
	var groupSaved = getCookie("group");
	if(groupSaved != null && groupSaved != undefined && groupSaved != ''){
			groupSaved = JSON.parse(groupSaved);
				setTimeout(function(){
					var All;
					var checkbxName=[];
					var checkboxName =  document.getElementsByName("checkbxName");
					var length = checkboxName.length; 
					var length2 = groupSaved.length; 
					for(let i=0; i < length; i++)
					{
						for(let j = 0 ; j < length2; j++){	
								if(groupSaved[j] == checkboxName[i].value){
									checkboxName[i].checked = true;
								}
							
						}
					} 
					
					document.getElementById("grouppname").value = checkbxName;
					document.getElementById("myInput").value = groupSaved;
						//setCookie("group",JSON.stringify(checkbxName));
				},1500);
	}
		});
		
		
	}
	
		if((y == "Leads" || y == "Accounts" || y == "Contacts" || y == "Tasks" || y == "Opportunities" ) && x=="DetailView"){
		var record = GetURLParameter("record");
		if(record == null || record == ''){
		var record = document.getElementsByName("record")[0].value;
		}
		var assigneArr  = localStorage.getItem("Assigned");
		var campaignArr = localStorage.getItem("Campaign");
		if(y == "Leads"){
			var table = "leads";
		}else if(y == "Accounts"){
			var table = "accounts";
		}
		else if(y == "Tasks"){
			var table = "tasks";
		}
		else if(y == "Opportunities"){
			var table = "opportunities";
		}else{
			var table = "contacts";
		}
		if(assigneArr != null && assigneArr != '' && assigneArr != undefined){
		$.ajax({
							type: "POST",
							url: "index.php?entryPoint=saveAssign",
							data:{
								record: record,
								assigned: JSON.parse(assigneArr),
								table: table,
								module: y
							},
							success: function(data) {
								//alert(data);
								localStorage.removeItem("Assigned");
							
						},
						error: function (error) {
                    
						}
            });
		}else{
			if(assigneArr == ''){
				$.ajax({
							type: "POST",
							url: "index.php?entryPoint=saveAssign",
							data:{
								record: record,
								assigned: "null",
								table: table,
								module: y
							},
							success: function(data) {
								//alert(data);
								localStorage.removeItem("Assigned");
							
						},
						error: function (error) {
                    
						}
				});
			}
			
		}
		if(campaignArr != null && campaignArr != '' && campaignArr != undefined){
		$.ajax({
							type: "POST",
							url: "index.php?entryPoint=saveCampaign",
							data:{
								record: record,
								campaign: JSON.parse(campaignArr),
								table: table,
								module: y
							},
							success: function(data) {
								//alert(data);
								localStorage.removeItem("Campaign");
							
						},
						error: function (error) {
                    
						}
            });
		}else{
			if(campaignArr == ''){
				$.ajax({
							type: "POST",
							url: "index.php?entryPoint=saveCampaign",
							data:{
								record: record,
								campaign: "null",
								table: table,
								module: y
							},
							success: function(data) {
								//alert(data);
								localStorage.removeItem("Campaign");
							
						},
						error: function (error) {
                    
						}
				});
			}
			
		}
		
	}
	
	if(((y == "Leads" || y == "Accounts" || y == "Contacts" || y == "Tasks" || y == "Opportunities") && x == "EditView") || (adetailView == "DetailView" && (modul == "Leads" || modul == "Accounts" || modul == "Contacts" || modul == "Tasks" || modul == "Opportunities") && m != "account_aos_invoices" && aReturnId != null)){
	if(y == "Opportunities" || modul == "Opportunities"){
		var field = document.getElementsByClassName("edit-view-row-item");
	}else{
		var tab0 = document.getElementById("tab-content-0");
		var field = tab0.getElementsByClassName("edit-view-row-item");
	}
	var html2 = '';
	
	html2 +='<button type="button" class="btn btn-danger email-address-add-button" title="Thêm assign" id="assignedAdd" onclick="showMoreAssigned();">';
	html2 +='<span class="suitepicon suitepicon-action-plus"></span>';
	html2 +='<span></span>';
	html2 +='</button>';
	html2 +='<div id="showMoreAssign"></div>';
	
	document.getElementsByClassName("edit-view-row-item")[eval(field.length-2)].innerHTML=document.getElementsByClassName("edit-view-row-item")[eval(field.length-2)].innerHTML+html2;	
	
	
	var recordId = GetURLParameter("record");
	if(recordId == null || recordId == ''){
		var recordId = document.getElementsByName("record")[0].value;
	}
	if(y == "Leads" || modul == "Leads"){
		var table = "leads";
	}else if(y == "Accounts" || modul == "Accounts"){
		var table = "accounts";
	}
	else if(y == "Tasks" || modul == "Tasks"){
		var table = "tasks";
	}
	else if(y == "Opportunities" || modul == "Opportunities"){
		var table = "opportunities";
	}
	else{
		var table = "contacts";
	}
		if(recordId != null && recordId != '' && recordId != undefined){
			var html;
			var more;
			var count;
			$.ajax({
						type: "POST",
						url: "index.php?entryPoint=getOtherAssign",
						data: {
						   id: recordId,
						   table: table
						},
						dataType: "json",
						success: function(data) {
							if(data != null && data != undefined && data != ""){
							var assigArr = data;
								for(let i=0;i < assigArr.length;i++){
									html=document.createElement('div');
									//more = document.getElementById("showMoreAssign").innerHTML;
									//count = document.getElementById("showMoreAssign").childElementCount;
									html.innerHTML='<div id="assignedForm'+eval(i+1)+'"><input type="text" name="assigned_user_name" onkeyup="filterKeyAssign('+eval(i+1)+')" value="'+assigArr[i]+'"class="sqsEnabled yui-ac-input" tabindex="0" id="assigned_user_name'+eval(i+1)+'" size="" placeholder="thêm user Id" title="" autocomplete="off" onclick = "showUserAssi('+eval(i+1)+')"><span><button type="button" id="removeButton'+eval(i+1)+'" class="btn btn-danger email-address-remove-button" onclick = "removeUserAssi('+eval(i+1)+')"><span class="suitepicon suitepicon-action-minus"></span></button></span></div><div id="userAssign'+eval(i+1)+'"></div>';
									
									document.getElementById("showMoreAssign").append(html);
									$.ajax({
										type: "POST",
										url: "index.php?entryPoint=showUserAssi",
										dataType: "json",
										success: function(data) {
											var assignVal;
											var length=data.length;
											var html='<select multiple="multiple" id="userAssignDrop'+eval(i+1)+'" style="width:180px;height:180px;z-index:1000;position:absolute">';
											for(var k=0;k<length;k++){
											html+='<option value="'+data[k].id+'">'+data[k].name+'</option>';
											}
											html+='</select>';
											document.getElementById("userAssign"+eval(i+1)).style.display="none";
											document.getElementById("userAssign"+eval(i+1)).innerHTML=html;
											assignVal = $("#assigned_user_name"+eval(i+1)).val();
											$("#userAssignDrop"+eval(i+1)+" option:contains("+assignVal+")").attr('selected', 'selected');
												$("#assigned_user_name"+eval(i+1)).on("blur",function(){
												setTimeout(function(){
												$("#userAssign"+eval(i+1)).css('display','none');
												},150);
												});
												
												$("#userAssignDrop"+eval(i+1)).on("change",function(){
													var vall=$("#userAssignDrop"+eval(i+1)).find(":selected").text();
													$("#assigned_user_name"+eval(i+1)).val(vall);
												});
												
										},
										error: function (error) {
											
										}
									});
									html='';					
								}
							}
						},
						error: function (error) {
							
						}
					});
		}
		var html3 = '';
	html3 +='<button type="button" class="btn btn-danger email-address-add-button" title="Thêm campaign" id="campaignAdd" onclick="showMoreCampaign();">';
	html3 +='<span class="suitepicon suitepicon-action-plus"></span>';
	html3 +='<span></span>';
	html3 +='</button>';
	html3 +='<div id="showMoreCampaign"></div>';
	
	document.getElementsByClassName("edit-view-row-item")[eval(field.length-1)].innerHTML=document.getElementsByClassName("edit-view-row-item")[eval(field.length-1)].innerHTML+html3;	
	
	
	var recordId = GetURLParameter("record");
	if(recordId == null || recordId == ''){
		var recordId = document.getElementsByName("record")[0].value;
	}
	if(y == "Leads" || modul == "Leads"){
		var table = "leads";
	}else if(y == "Accounts" || modul == "Accounts"){
		var table = "accounts";
	}
	else if(y == "Tasks" || modul == "Tasks"){
		var table = "tasks";
	}
	else if(y == "Opportunities" || modul == "Opportunities"){
		var table = "opportunities";
	}
	else{
		var table = "contacts";
	}
		if(recordId != null && recordId != '' && recordId != undefined){
			var html;
			var more;
			var count;
			$.ajax({
						type: "POST",
						url: "index.php?entryPoint=getOtherCampaign",
						data: {
						   id: recordId,
						   table: table
						},
						dataType: "json",
						success: function(data) {
							if(data != null && data != undefined && data != ""){
							var campaignArr = data;
								for(let i=0;i < campaignArr.length;i++){
									html=document.createElement('div');
									//more = document.getElementById("showMoreAssign").innerHTML;
									//count = document.getElementById("showMoreAssign").childElementCount;
									html.innerHTML='<div id="campaignForm'+eval(i+1)+'"><input type="text" name="campaign_name" onkeyup="filterKeyCampaign('+eval(i+1)+')" value="'+campaignArr[i]+'"class="sqsEnabled yui-ac-input" tabindex="0" id="campaign_name'+eval(i+1)+'" size="" placeholder="thêm campaign" title="" autocomplete="off" onclick = "showUserCampaign('+eval(i+1)+')"><span><button type="button" id="removeButton'+eval(i+1)+'" class="btn btn-danger email-address-remove-button" onclick = "removeUserCampaign('+eval(i+1)+')"><span class="suitepicon suitepicon-action-minus"></span></button></span></div><div id="userCampaign'+eval(i+1)+'"></div>';
									
									document.getElementById("showMoreCampaign").append(html);
									$.ajax({
										type: "POST",
										url: "index.php?entryPoint=showUserCampaign",
										dataType: "json",
										success: function(data) {
											var campaignVal;
											var length=data.length;
											var html='<select multiple="multiple" id="userCampaignDrop'+eval(i+1)+'" style="width:180px;height:180px;z-index:1000;position:absolute">';
											for(var k=0;k<length;k++){
											html+='<option value="'+data[k].id+'">'+data[k].name+'</option>';
											}
											html+='</select>';
											document.getElementById("userCampaign"+eval(i+1)).style.display="none";
											document.getElementById("userCampaign"+eval(i+1)).innerHTML=html;
											campaignVal = $("#campaign_name"+eval(i+1)).val();
											$("#userCampaignDrop"+eval(i+1)+" option:contains("+campaignVal+")").attr('selected', 'selected');
												$("#campaign_name"+eval(i+1)).on("blur",function(){
												setTimeout(function(){
												$("#userCampaign"+eval(i+1)).css('display','none');
												},150);
												});
												
												$("#userCampaignDrop"+eval(i+1)).on("change",function(){
													var vall=$("#userCampaignDrop"+eval(i+1)).find(":selected").text();
													$("#campaign_name"+eval(i+1)).val(vall);
												});
												
										},
										error: function (error) {
											
										}
									});
									html='';					
								}
							}
						},
						error: function (error) {
							
						}
					});
		}
		$(document).ready(function(){
			if(y == "Leads" || modul == "Leads"){
				changeCur("opportunity_amount");
				changeCur("depositsamount");
				changeCur("paymentsamount");
				$('<span  style="display:inline-block;margin-left:5px"><input id="getNameCheck" type="checkbox" checked="true"></span>').insertAfter($("#opportunity_name"));
				$("#opportunity_amount").css("background-color", "yellow");
				$("#depositsamount").css("background-color", "yellow");
				$("#paymentsamount").css("background-color", "yellow");
				$("#opportunity_amount").css("color", "red");
				$("#depositsamount").css("color", "red");
				$("#paymentsamount").css("color", "red");
				$("#opportunity_amount").css("font-weight", "bold");
				$("#depositsamount").css("font-weight", "bold");
				$("#paymentsamount").css("font-weight", "bold");
				$("#phone_mobile").on("keyup", function(){
					if($("#getNameCheck").is(":checked")){
						if($("#aos_products_leads_1_name").val() != ""){
						$("#opportunity_name").val($("#aos_products_leads_1_name").val()+"_"+$("#phone_mobile").val());
						}else{
						$("#opportunity_name").val($("#phone_mobile").val());	
						}
					}
				});
				var deposite;
				$.ajax({
					type: "POST",
					url: "index.php?entryPoint=getDeposite",
					success: function(pData) {
						deposite = pData;
						var proVal = $("#aos_products_leads_1_name").val();
						var proAmo = $("#opportunity_amount").val();
						var depoAmount = $("#depositsamount").val();
						setInterval(function(){
							if($("#aos_products_leads_1_name").val() != null){
								if($("#aos_products_leads_1_name").val() != proVal){
									if($("#aos_products_leads_1_name").val() != ""){
										if($("#getNameCheck").is(":checked")){
											if($("#phone_mobile").val() != ""){
											$("#opportunity_name").val($("#aos_products_leads_1_name").val()+"_"+$("#phone_mobile").val());
											}else{
											$("#opportunity_name").val($("#aos_products_leads_1_name").val());	
											}
										}
										
										$.ajax({
														type: "POST",
														url: "index.php?entryPoint=loadPrice",
														dataType:"json",
														data:{
															name:$("#aos_products_leads_1_name").val()
														},
														success: function(data) {
															if(data != null && data != '' && data != undefined){
																var amount = parseInt(data);
																$("#opportunity_amount").val(changeCurrency(String(parseInt(data))));
																proVal = $("#aos_products_leads_1_name").val();
																proAmo = $("#opportunity_amount").val();
																var depositsAmount = amount * (deposite / 100);
																$("#depositsamount").val(changeCurrency(String(parseInt(depositsAmount))));
																var paymentsAmount = amount - parseInt(depositsAmount);
																$("#paymentsamount").val(changeCurrency(String(paymentsAmount)));
															}else{
																$("#opportunity_amount").val(0);
																proVal = 0;
																proAmo = 0;
																$("#depositsamount").val(0);
																$("#paymentsamount").val(0);
															}
														},
														error: function (error) {
												
														}
										});
									}else{
										if($("#opportunity_amount").val() != proAmo){
											proVal = $("#aos_products_leads_1_name").val();
											proAmo = $("#opportunity_amount").val();
											var proAmo2 = proAmo.replace(/,/g,'');
											var depositsAmount = proAmo2 * (deposite / 100);
											$("#depositsamount").val(changeCurrency(String(parseInt(depositsAmount))));
											var paymentsAmount = proAmo2 - parseInt(depositsAmount);
											$("#paymentsamount").val(changeCurrency(String(paymentsAmount)));
										}
									}
								}else{
									if($("#opportunity_amount").val() != proAmo){
										proVal = $("#aos_products_leads_1_name").val();
										proAmo = $("#opportunity_amount").val();
										var proAmo2 = proAmo.replace(/,/g,'');
										var depositsAmount = proAmo2 * (deposite / 100);
										$("#depositsamount").val(changeCurrency(String(parseInt(depositsAmount))));
										var paymentsAmount = proAmo2 - parseInt(depositsAmount);
										$("#paymentsamount").val(changeCurrency(String(paymentsAmount)));
									}
								}
							}
							if($("#depositsamount").val() != depoAmount){
								depoAmount = $("#depositsamount").val();
								proAmo = $("#opportunity_amount").val();
								var proAmo2 = proAmo.replace(/,/g,'');
								var paymentsAmount = proAmo2 - parseInt(depoAmount.replace(/,/g,''));
								$("#paymentsamount").val(changeCurrency(String(paymentsAmount)));
							}
							
						},1000);
					}
				});
				 document.getElementById('paymentsamount').readOnly = true;
			}
			
			
		});
	}
	if((y == "Opportunities" && x == "EditView") || (modul == "Opportunities" && adetailView == "DetailView")){
		$.ajax({
					type: "POST",
					url: "index.php?entryPoint=getOppLanguage",
					dataType: "json",
					success: function(dat) {
						var form_name = 'EditView';
						$("#SAVE.button.primary").removeAttr('onclick');
						$("#SAVE.button.primary").removeAttr('type');
						$("#SAVE.button.primary").attr('type', 'button');
						$("#SAVE.button.primary").on("click",function(){
						//if(!check_form(form_name) || check_form(form_name)){
						check_form(form_name);
						if(($("#account_name").val() != null && $("#account_name").val() != "") && ($("#name").val() != null && $("#name").val() != "") && ($("#amount").val() != null && $("#amount").val() != "") && ($("#date_closed") != null && $("#date_closed").val() != "")){
							
								var _form = document.getElementById('EditView');
								var count = document.getElementById("showMoreAssign").childElementCount;
								var assignedArray = [];
								if(count > 0){
									for (let j = 1; j <= count; j++){
										if($("#assigned_user_name"+j).val() != null && $("#assigned_user_name"+j).val() != '' && $("#assigned_user_name"+j).val() != undefined ){
													assignedArray.push($("#userAssignDrop"+j).val()[0]);
													
										}	
									}
										if(assignedArray.length > 0){
											localStorage.setItem("Assigned",JSON.stringify(assignedArray));
										}else{
											localStorage.setItem("Assigned",'');
										}
								}
									var count2 = document.getElementById("showMoreCampaign").childElementCount;
									var campaignArray = [];
									if(count2 > 0){
										for (let vv = 1; vv <= count2; vv++){
											if($("#campaign_name"+vv).val() != null && $("#campaign_name"+vv).val() != '' && $("#campaign_name"+vv).val() != undefined ){
												campaignArray.push($("#userCampaignDrop"+vv).val()[0]);
													
											}
												
										}
										if(campaignArray.length > 0){
											localStorage.setItem("Campaign",JSON.stringify(campaignArray));
										}else{
											localStorage.setItem("Campaign",'');
										}
									}
									_form.action.value = 'Save';
									SUGAR.ajaxUI.submitForm(_form);
							
				}
			//}
			
		});
						
					}
		});
		
		
		$(document).ready(function(){
			var deposite;
			$('<span  style="display:inline-block;margin-left:5px"><input id="getNameCheck" type="checkbox" checked="true"></span>').insertAfter($("#name"));
			$.ajax({
				type: "POST",
				url: "index.php?entryPoint=getDeposite",
				success: function(pData) {
					deposite = pData;
					var proVal = $("#aos_products_opportunities_1_name").val();
					var proAmo = $("#amount").val();
					var depoAmount = $("#depositsamount").val();
					setInterval(function(){
						if($("#date_closed").val() != null && $("#date_closed").val() != "" ){
							if($("#getNameCheck").is(":checked")){
								if($("#aos_products_opportunities_1_name").val() != ""){
								$("#name").val($("#aos_products_opportunities_1_name").val()+"_"+$("#date_closed").val().replace(/\//g,""));
								}else{
								$("#name").val($("#date_closed").val().replace(/\//g,""));	
								}
							}
						}
						if($("#aos_products_opportunities_1_name").val() != null){
							if($("#aos_products_opportunities_1_name").val() != proVal){
								if($("#aos_products_opportunities_1_name").val() != ""){
									if($("#getNameCheck").is(":checked")){
										if($("#date_closed").val() != ""){
											$("#name").val($("#aos_products_opportunities_1_name").val()+"_"+$("#date_closed").val().replace(/\//g,""));
										}else{
											$("#name").val($("#aos_products_opportunities_1_name").val());	
										}
									}
									$.ajax({
													type: "POST",
													url: "index.php?entryPoint=loadPrice",
													dataType:"json",
													data:{
														name:$("#aos_products_opportunities_1_name").val()
													},
													success: function(data) {
														if(data != null && data != '' && data != undefined){
															var amount = parseInt(data);
															$("#amount").val(changeCurrency(String(parseInt(data))));
															proVal = $("#aos_products_opportunities_1_name").val();
															proAmo = $("#amount").val();
															var depositsAmount = amount * (deposite / 100);
															$("#depositsamount").val(changeCurrency(String(parseInt(depositsAmount))));
															var paymentsAmount = amount - parseInt(depositsAmount);
															$("#paymentsamount").val(changeCurrency(String(paymentsAmount)));
														}else{
															$("#amount").val(0);
															proVal = 0;
															proAmo = 0;
															$("#depositsamount").val(0);
															$("#paymentsamount").val(0);
														}
													},
													error: function (error) {
											
													}
									});
								}else{
									if($("#amount").val() != proAmo){
										proVal = $("#aos_products_opportunities_1_name").val();
										proAmo = $("#amount").val();
										var proAmo2 = proAmo.replace(/,/g,'');
										var depositsAmount = proAmo2 * (deposite / 100);
										$("#depositsamount").val(changeCurrency(String(parseInt(depositsAmount))));
										var paymentsAmount = proAmo2 - parseInt(depositsAmount);
										$("#paymentsamount").val(changeCurrency(String(paymentsAmount)));
									}
								}
							}else{
								if($("#amount").val() != proAmo){
									proVal = $("#aos_products_opportunities_1_name").val();
									proAmo = $("#amount").val();
									var proAmo2 = proAmo.replace(/,/g,'');
									var depositsAmount = proAmo2 * (deposite / 100);
									$("#depositsamount").val(changeCurrency(String(parseInt(depositsAmount))));
									var paymentsAmount = proAmo2 - parseInt(depositsAmount);
									$("#paymentsamount").val(changeCurrency(String(paymentsAmount)));
								}
							}
						}
						if($("#depositsamount").val() != depoAmount){
							depoAmount = $("#depositsamount").val();
							proAmo = $("#amount").val();
							var proAmo2 = proAmo.replace(/,/g,'');
							var paymentsAmount = proAmo2 - parseInt(depoAmount.replace(/,/g,''));
							$("#paymentsamount").val(changeCurrency(String(paymentsAmount)));
						}
						
					},1000);
				}
			});
			 document.getElementById('paymentsamount').readOnly = true;
		});
	}
	if((y == "po_PO" && x == "EditView") || (adetailView == "DetailView" && modul == "po_PO")){
		module_sugar_grp1 = "po_PO";
		var html = '';
		html += '<div class="panel panel-default">';
html += '<div class="panel-heading ">';
html += '<a class="" role="button" data-toggle="collapse-edit" aria-expanded="false">';
html += '<div class="col-xs-10 col-sm-11 col-md-11">';
html += 'Account infomation';
html += '</div>';
html += '</a>';
html += '</div>';
html += '<div class="panel-body panel-collapse collapse in panelContainer" id="detailpanel_0" >';
html += '<div class="tab-content">';

html += '<div class="row edit-view-row">';
html += '<div class="col-xs-12 col-sm-6 edit-view-row-item">';
html += '<div class="col-xs-12 col-sm-4 label" data-label="LBL_BILLING_ACCOUNT">';
html += 'Account:</div>';
html += '<div class="col-xs-12 col-sm-8 edit-view-field  yui-ac" type="relate" field="billing_account">';
html += '<input type="text" name="billing_account" class="sqsEnabled yui-ac-input" tabindex="0" id="billing_account" size="" value="" title="" autocomplete="off"><div id="EditView_billing_account_results" class="yui-ac-container"><div class="yui-ac-content" style="display: none;"><div class="yui-ac-hd" style="display: none;"></div><div class="yui-ac-bd"><ul><li style="display: none;"></li><li style="display: none;"></li><li style="display: none;"></li><li style="display: none;"></li><li style="display: none;"></li><li style="display: none;"></li><li style="display: none;"></li><li style="display: none;"></li><li style="display: none;"></li><li style="display: none;"></li></ul></div><div class="yui-ac-ft" style="display: none;"></div></div></div>';
html += '<input type="hidden" name="billing_account_id" id="billing_account_id" value="">';
html += '<span class="id-ff multiple">';
html += '<button type="button" name="btn_billing_account" id="btn_billing_account" tabindex="0" title="Select Account" class="button firstChild" value="Select Account" onclick="open_popup(';
html += '&quot;Accounts&quot;,';
html += '600,';
html += '400,'; 
html += '&quot;&quot;,'; 
html += 'true,'; 
html += 'false,'; 
html += '{&quot;call_back_function&quot;:&quot;set_return&quot;,&quot;form_name&quot;:&quot;EditView&quot;,&quot;field_to_name_array&quot;:{&quot;id&quot;:&quot;billing_account_id&quot;,&quot;name&quot;:&quot;billing_account&quot;,&quot;billing_address_street&quot;:&quot;billing_address_street&quot;,&quot;billing_address_city&quot;:&quot;billing_address_city&quot;,&quot;billing_address_state&quot;:&quot;billing_address_state&quot;,&quot;billing_address_postalcode&quot;:&quot;billing_address_postalcode&quot;,&quot;billing_address_country&quot;:&quot;billing_address_country&quot;,&quot;shipping_address_street&quot;:&quot;shipping_address_street&quot;,&quot;shipping_address_city&quot;:&quot;shipping_address_city&quot;,&quot;shipping_address_state&quot;:&quot;shipping_address_state&quot;,&quot;shipping_address_postalcode&quot;:&quot;shipping_address_postalcode&quot;,&quot;shipping_address_country&quot;:&quot;shipping_address_country&quot;,&quot;phone_office&quot;:&quot;phone_work&quot;}},'; 
html += '&quot;single&quot;,'; 
html += 'true';
html += ');"><span class="suitepicon suitepicon-action-select"></span></button><button type="button" name="btn_clr_billing_account" id="btn_clr_billing_account" tabindex="0" title="Clear Account" class="button lastChild" onclick="SUGAR.clearRelateField(this.form, "billing_account", "billing_account_id");" value="Clear Account"><span class="suitepicon suitepicon-action-clear"></span></button>';
html += '</span>';
html += '<script type="text/javascript">';
html += 'SUGAR.util.doWhen(';
html += '"typeof(sqs_objects) != \'undefined\' && typeof(sqs_objects[\'EditView_billing_account\']) != \'undefined\'",';
html += 'enableQS';
html += ');';
html += '</script>';
html += '</div>';

html += '</div>';
html += '<div class="col-xs-12 col-sm-6 edit-view-row-item">';
html += '</div>';
html += '<div class="clear"></div>';
html += '<div class="clear"></div>';
html += '<div class="col-xs-12 col-sm-6 edit-view-row-item">';
html += '<div class="col-xs-12 col-sm-4 label" data-label="LBL_BILLING_CONTACT">';
html += 'Contact:</div>';
html += '<div class="col-xs-12 col-sm-8 edit-view-field  yui-ac" type="relate" field="billing_contact">';
html += '<input type="text" name="billing_contact" class="sqsEnabled yui-ac-input" tabindex="0" id="billing_contact" size="" value="" title="" autocomplete="off"><div id="EditView_billing_contact_results" class="yui-ac-container"><div class="yui-ac-content" style="display: none;"><div class="yui-ac-hd" style="display: none;"></div><div class="yui-ac-bd"><ul><li style="display: none;"></li><li style="display: none;"></li><li style="display: none;"></li><li style="display: none;"></li><li style="display: none;"></li><li style="display: none;"></li><li style="display: none;"></li><li style="display: none;"></li><li style="display: none;"></li><li style="display: none;"></li></ul></div><div class="yui-ac-ft" style="display: none;"></div></div></div>';
html += '<input type="hidden" name="billing_contact_id" id="billing_contact_id" value="">';
html += '<span class="id-ff multiple">';
html += '<button type="button" name="btn_billing_contact" id="btn_billing_contact" tabindex="0" title="Select Contact" class="button firstChild" value="Select Contact" onclick="open_popup(';
html += '&quot;Contacts&quot;,'; 
html += '600,';
html += '400,'; 
html += '&quot;&amp;account_name=&quot;+this.form.billing_account.value+&quot;&quot;,'; 
html += 'true,'; 
html += 'false,'; 
html += '{&quot;call_back_function&quot;:&quot;set_return&quot;,&quot;form_name&quot;:&quot;EditView&quot;,&quot;field_to_name_array&quot;:{&quot;id&quot;:&quot;billing_contact_id&quot;,&quot;name&quot;:&quot;billing_contact&quot;}},'; 
html += '&quot;single&quot;,'; 
html += 'true';
html += ');"><span class="suitepicon suitepicon-action-select"></span></button><button type="button" name="btn_clr_billing_contact" id="btn_clr_billing_contact" tabindex="0" title="Clear Contact" class="button lastChild" onclick="SUGAR.clearRelateField(this.form, \'billing_contact\', \'billing_contact_id\');" value="Clear Contact"><span class="suitepicon suitepicon-action-clear"></span></button>';
html += '</span>';
html += '<script type="text/javascript">';
html += 'SUGAR.util.doWhen(';
html +=	'"typeof(sqs_objects) != \'undefined\' && typeof(sqs_objects[\'EditView_billing_contact\']) != \'undefined\'",';
html +=	'enableQS';
html += ');';
html += '</script>';
html += '</div>';

html += '</div>';
html += '<div class="col-xs-12 col-sm-6 edit-view-row-item">';
html += '</div>';
html += '<div class="clear"></div>';
html += '<div class="clear"></div>';
html += '<div class="col-xs-12 col-sm-6 edit-view-row-item">';
html += '<div class="col-xs-12 col-sm-12 edit-view-field " type="varchar" field="billing_address_street" colspan="2">';
html += '<script src="include/SugarFields/Fields/Address/SugarFieldAddress.js?v=golOgr3LHb5uVI36fQV1hQ"></script>';
html += '<fieldset id="BILLING_address_fieldset">';
html += '<legend>Billing Address</legend>';
html += '<table border="0" cellspacing="1" cellpadding="0" class="edit" width="100%">';
html += '<tbody><tr>';
html += '<td valign="top" id="billing_address_street_label" width="25%" scope="row">';
html += '<label for="billing_address_street">Street:</label>';
html += '</td>';
html += '<td width="*">';
html += '<textarea id="billing_address_street" name="billing_address_street" title="" maxlength="150" rows="2" cols="30" tabindex="0"></textarea>';
html += '</td>';
html += '</tr>';
html += '<tr>';
html += '<td id="billing_address_city_label" width="%" scope="row">';
html += '<label for="billing_address_city">City:';
html += '</label></td>';
html += '<td>';
html += '<input type="text" name="billing_address_city" id="billing_address_city" title="" size="30" maxlength="150" value="" tabindex="0">';
html += '</td>';
html += '</tr>';
html += '<tr>';
html += '<td id="billing_address_state_label" width="%" scope="row">';
html += '<label for="billing_address_state">State:</label>';
html += '</td>';
html += '<td>';
html += '<input type="text" name="billing_address_state" id="billing_address_state" title="" size="30" maxlength="150" value="" tabindex="0">';
html += '</td>';
html += '</tr>';
html += '<tr>';
html += '<td id="billing_address_postalcode_label" width="%" scope="row">';
html += '<label for="billing_address_postalcode">Postal Code:</label>';
html += '</td>';
html += '<td>';
html += '<input type="text" name="billing_address_postalcode" id="billing_address_postalcode" title="" size="30" maxlength="150" value="" tabindex="0">';
html += '</td>';
html += '</tr>';
html += '<tr>';
html += '<td id="billing_address_country_label" width="%" scope="row">';
html += '<label for="billing_address_country">Country:</label>';
html += '</td>';
html += '<td>';
html += '<input type="text" name="billing_address_country" id="billing_address_country" title="" size="30" maxlength="150" value="" tabindex="0">';
html += '</td>';
html += '</tr>';
html += '<tr>';
html += '<td colspan="2" nowrap="">&nbsp;</td>';
html += '</tr>';
html += '</tbody></table>';
html += '</fieldset>';
html += '<script type="text/javascript">';
html += 'SUGAR.util.doWhen("typeof(SUGAR.AddressField) != \'undefined\'", function () {';
html += 'billing_address = new SUGAR.AddressField("billing_checkbox", \'\', \'billing\');';
html += '});';
html += '</script>';
html += '</div>';

html += '</div>';
html += '<div class="col-xs-12 col-sm-6 edit-view-row-item">';
html += '<div class="col-xs-12 col-sm-12 edit-view-field " type="varchar" field="shipping_address_street" colspan="2">';
html += '<script src="include/SugarFields/Fields/Address/SugarFieldAddress.js?v=golOgr3LHb5uVI36fQV1hQ"></script>';
html += '<fieldset id="SHIPPING_address_fieldset">';
html += '<legend>Shipping Address</legend>';
html += '<table border="0" cellspacing="1" cellpadding="0" class="edit" width="100%">';
html += '<tbody><tr>';
html += '<td valign="top" id="shipping_address_street_label" width="25%" scope="row">';
html += '<label for="shipping_address_street">Street:</label>';
html += '</td>';
html += '<td width="*">';
html += '<textarea id="shipping_address_street" name="shipping_address_street" title="" maxlength="150" rows="2" cols="30" tabindex="0"></textarea>';
html += '</td>';
html += '</tr>';
html += '<tr>';
html += '<td id="shipping_address_city_label" width="%" scope="row">';
html += '<label for="shipping_address_city">City:';
html += '</label></td>';
html += '<td>';
html += '<input type="text" name="shipping_address_city" id="shipping_address_city" title="" size="30" maxlength="150" value="" tabindex="0">';
html += '</td>';
html += '</tr>';
html += '<tr>';
html += '<td id="shipping_address_state_label" width="%" scope="row">';
html += '<label for="shipping_address_state">State:</label>';
html += '</td>';
html += '<td>';
html += '<input type="text" name="shipping_address_state" id="shipping_address_state" title="" size="30" maxlength="150" value="" tabindex="0">';
html += '</td>';
html += '</tr>';
html += '<tr>';
html += '<td id="shipping_address_postalcode_label" width="%" scope="row">';
html += '<label for="shipping_address_postalcode">Postal Code:</label>';
html += '</td>';
html += '<td>';
html += '<input type="text" name="shipping_address_postalcode" id="shipping_address_postalcode" title="" size="30" maxlength="150" value="" tabindex="0">';
html += '</td>';
html += '</tr>';
html += '<tr>';
html += '<td id="shipping_address_country_label" width="%" scope="row">';
html += '<label for="shipping_address_country">Country:</label>';
html += '</td>';
html += '<td>';
html += '<input type="text" name="shipping_address_country" id="shipping_address_country" title="" size="30" maxlength="150" value="" tabindex="0">';
html += '</td>';
html += '</tr>';
html += '<tr>';
html += '<td scope="row" nowrap="">';
html += 'Copy address from left:';
html += '</td>';
html += '<td>';
html += '<input id="shipping_checkbox" name="shipping_checkbox" type="checkbox" onclick="shipping_address.syncFields();">';
html += '</td>';
html += '</tr>';
html += '</tbody></table>';
html += '</fieldset>';
html += '<script type="text/javascript">';
html += 'SUGAR.util.doWhen("typeof(SUGAR.AddressField) != \'undefined\'", function () {';
html += 'shipping_address = new SUGAR.AddressField("shipping_checkbox", \'billing\', \'shipping\');';
html += '});';
html += '</script>';
html += '</div>';

html += '</div>';
html += '<div class="clear"></div>';
html += '<div class="clear"></div>';
html += '</div>';
 html += '</div>';
html += '</div>';
html += '</div>';
html += '<div class="panel panel-default">';
html += '<div class="panel-heading ">';
html += '<a class="" role="button" data-toggle="collapse-edit" aria-expanded="false">';
html += '<div class="col-xs-10 col-sm-11 col-md-11">';
html += 'Line Items';
html += '</div>';
html += '</a>';
html += '</div>';
html += '<div class="panel-body panel-collapse collapse in panelContainer" id="detailpanel_1" data-id="LBL_LINE_ITEMS">';
html += '<div class="tab-content">';

html += '<div class="row edit-view-row">';
html += '<div class="col-xs-12 col-sm-12 edit-view-row-item">';
html += '<div class="col-xs-12 col-sm-2 label" data-label="LBL_CURRENCY">';
html += 'Currency:</div>';
html += '<div class="col-xs-12 col-sm-8 edit-view-field " type="id" field="currency_id" colspan="3">';
html += '<span id="currency_id_span">';
html += '<select name="currency_id" id="currency_id_select" onchange="CurrencyConvertAll(this.form);"><option value="-99" selected="">VNĐ : đ</option></select><script>var ConversionRates = new Array();'; 
html += 'var CurrencySymbols = new Array(); ';
html += 'var lastRate = "1"; ConversionRates[\'-99\'] = \'1\';';
 html += 'CurrencySymbols[\'-99\'] = \'đ\';';
html += 'var currencyFields = ["total_amt","total_amt_usdollar","subtotal_amount","subtotal_amount_usdollar","discount_amount","discount_amount_usdollar","tax_amount","tax_amount_usdollar","shipping_amount","shipping_amount_usdollar","shipping_tax_amt","shipping_tax_amt_usdollar","total_amount","total_amount_usdollar","subtotal_tax_amount","subtotal_tax_amount_usdollar"];';
html += '					function get_rate(id){';
html += '						return ConversionRates[id];';
html += '					}';
html += '					function ConvertToDollar(amount, rate){';
html += '						return amount / rate;';
html += '					}';
html += '					function ConvertFromDollar(amount, rate){';
html += '						return amount * rate;';
html += '					}';
html += '					function ConvertRate(id,fields){';
html += '							for(var i = 0; i < fields.length; i++){';
html += '								fields[i].value = toDecimal(ConvertFromDollar(toDecimal(ConvertToDollar(toDecimal(fields[i].value), lastRate)), ConversionRates[id]));';
html += '							}';
html += '							lastRate = ConversionRates[id];';
html += '						}';
html += '					function ConvertRateSingle(id,field){';
html += '						var temp = field.innerHTML.substring(1, field.innerHTML.length);';
html += '						unformattedNumber = unformatNumber(temp, num_grp_sep, dec_sep);';
						
html += '						field.innerHTML = CurrencySymbols[id] + formatNumber(toDecimal(ConvertFromDollar(ConvertToDollar(unformattedNumber, lastRate), ConversionRates[id])), num_grp_sep, dec_sep, 2, 2);';
html += '						lastRate = ConversionRates[id];';
html += '					}';
html += '					function CurrencyConvertAll(form){';
html += '                       try {';
html += '                        var id = form.currency_id.options[form.currency_id.selectedIndex].value;';
html += '						var fields = new Array();';
						
html += '						for(i in currencyFields){';
html += '							var field = currencyFields[i];';
html += '							if(typeof(form[field]) != \'undefined\'){';
html += '								form[field].value = unformatNumber(form[field].value, num_grp_sep, dec_sep);';
html += '								fields.push(form[field]);';
html += '							}';
							
html += '						}';
							
html += '							ConvertRate(id, fields);';
html += '						for(i in fields){';
html += '							fields[i].value = formatNumber(fields[i].value, num_grp_sep, dec_sep);';

html += '						}';
							
html += '						} catch (err) {';
                            
html += '                        }';
						
html += '					}';
html += '				</script></span>';
html += '</div>';

html += '</div>';
html += '<div class="clear"></div>';
html += '<div class="col-xs-12 col-sm-12 edit-view-row-item">';
html += '<div class="col-xs-12 col-sm-2 label" data-label="LBL_LINE_ITEMS">';
html += 'Line Items:</div>';
html += '<div class="col-xs-12 col-sm-8 edit-view-field " type="function" field="line_items" colspan="3">';
html += '<span id="line_items_span">';
html += '<script language="javascript">var sig_digits = 2;var module_sugar_grp1 = "AOS_Invoices";var enable_groups = 1;var total_tax = 0;</script><table border="0" cellspacing="4" id="lineItems"></table><div style="padding-top: 10px; padding-bottom:10px;"><input type="button" tabindex="116" class="button" value="Add Group" id="addGroup" onclick="insertGroup(0)"></div><input type="hidden" name="vathidden" id="vathidden" value="';
html += '<OPTION value=\'0.0\'>0%</OPTION>';
html += '<OPTION value=\'5.0\'>5%</OPTION>';
html += '<OPTION value=\'7.5\'>7.5%</OPTION>';
html += '<OPTION value=\'17.5\'>17.5%</OPTION>';
html += '<OPTION value=\'20.0\'>20%</OPTION>">';
html += '<input type="hidden" name="discounthidden" id="discounthidden" value="';
html += '<OPTION value=\'Percentage\'>Pct</OPTION>';
html += '<OPTION value=\'Amount\'>Amt</OPTION>"></span>';
html += '</div>';

html += '</div>';
html += '<div class="clear"></div>';
html += '<div class="col-xs-12 col-sm-12 edit-view-row-item">';
html += '</div>';
html += '<div class="clear"></div>';
html += '<div class="col-xs-12 col-sm-12 edit-view-row-item">';
html += '<div class="col-xs-12 col-sm-2 label" data-label="LBL_TOTAL_AMT">';
html += 'Total:</div>';
html += '<div class="col-xs-12 col-sm-8 edit-view-field " type="currency" field="total_amt" colspan="3">';
html += '<input type="text" name="total_amt" id="total_amt" size="30" maxlength="26,6" value="" title="" tabindex="0">';
html += '</div>';

html += '</div>';
html += '<div class="clear"></div>';
html += '<div class="col-xs-12 col-sm-12 edit-view-row-item">';
html += '<div class="col-xs-12 col-sm-2 label" data-label="LBL_DISCOUNT_AMOUNT">';
html += 'Discount:</div>';
html += '<div class="col-xs-12 col-sm-8 edit-view-field " type="currency" field="discount_amount" colspan="3">';
html += '<input type="text" name="discount_amount" id="discount_amount" size="30" maxlength="26,6" value="" title="" tabindex="0">';
html += '</div>';

html += '</div>';
html += '<div class="clear"></div>';
html += '<div class="col-xs-12 col-sm-12 edit-view-row-item">';
html += '<div class="col-xs-12 col-sm-2 label" data-label="LBL_SUBTOTAL_AMOUNT">';
html += 'Subtotal:</div>';
html += '<div class="col-xs-12 col-sm-8 edit-view-field " type="currency" field="subtotal_amount" colspan="3">';
html += '<input type="text" name="subtotal_amount" id="subtotal_amount" size="30" maxlength="26,6" value="" title="" tabindex="0">';
html += '</div>';

html += '</div>';
html += '<div class="clear"></div>';
html += '<div class="col-xs-12 col-sm-12 edit-view-row-item">';
html += '<div class="col-xs-12 col-sm-2 label" data-label="LBL_SHIPPING_AMOUNT">';
html += 'Shipping:</div>';
html += '<div class="col-xs-12 col-sm-8 edit-view-field " type="currency" field="shipping_amount" colspan="3">';
html += '<input type="text" name="shipping_amount" id="shipping_amount" size="30" maxlength="26,6" value="" title="" tabindex="0" onblur="calculateTotal(\'lineItems\');">';
html += '</div>';

html += '</div>';
html += '<div class="clear"></div>';
html += '<div class="col-xs-12 col-sm-12 edit-view-row-item">';
html += '<div class="col-xs-12 col-sm-2 label" data-label="LBL_SHIPPING_TAX_AMT">';
html += 'Shipping Tax:</div>';
html += '<div class="col-xs-12 col-sm-8 edit-view-field " type="currency" field="shipping_tax_amt" colspan="3">';
html += '<span id="shipping_tax_amt_span">';
html += '<input id="shipping_tax_amt" type="text" tabindex="0" title="" value="" maxlength="26,6" size="22" name="shipping_tax_amt" onblur="calculateTotal(&quot;lineItems&quot;);"><select name="shipping_tax" id="shipping_tax" onchange="calculateTotal(&quot;lineItems&quot;);">';
html += '<option value="0.0">0%</option>';
html += '<option value="5.0">5%</option>';
html += '<option value="7.5">7.5%</option>';
html += '<option value="17.5">17.5%</option>';
html += '<option value="20.0">20%</option></select></span>';
html += '</div>';

html += '</div>';
html += '<div class="clear"></div>';
html += '<div class="col-xs-12 col-sm-12 edit-view-row-item">';
html += '<div class="col-xs-12 col-sm-2 label" data-label="LBL_TAX_AMOUNT">';
html += 'Tax:</div>';
html += '<div class="col-xs-12 col-sm-8 edit-view-field " type="currency" field="tax_amount" colspan="3">';
html += '<input type="text" name="tax_amount" id="tax_amount" size="30" maxlength="26,6" value="" title="" tabindex="0">';
html += '</div>';

html += '</div>';
html += '<div class="clear"></div>';
html += '<div class="col-xs-12 col-sm-12 edit-view-row-item">';
html += '<div class="col-xs-12 col-sm-2 label" data-label="LBL_GRAND_TOTAL">';
html += 'Grand Total:</div>';
html += '<div class="col-xs-12 col-sm-8 edit-view-field " type="currency" field="total_amount" colspan="3">';
html += '<input type="text" name="total_amount" id="total_amount" size="30" maxlength="26,6" value="" title="" tabindex="0">';
html += '</div>';

html += '</div>';
html += '<div class="col-xs-12 col-sm-6 edit-view-row-item">';
html += '</div>';
html += '<div class="clear"></div>';
html += '<div class="clear"></div>';
html += '</div>';
 html += '                   </div>';
html += '</div>';
html += '</div>';

document.getElementById("EditView_tabs").innerHTML += html;	

	$("#addGroup").on("click", function(){
		setTimeout(function(){
		$(".add_product_line").on("click", function(){
			setTimeout(function(){
				$(".product_part_number_button").on("click", function(){
				localStorage.setItem("product","1");
				});
			},500);
		});
		},500);
		setTimeout(function(){
		var groupname = document.getElementsByClassName("group_name");
		for(var i=0; i < groupname.length; i++){
			groupname[i].onblur=function(){
				var groupnameVal = '';
				for(var i=0; i < groupname.length; i++){
					groupnameVal  += groupname[i].value+", ";
					localStorage.setItem("name",groupnameVal);
				}
			}
			
		}
			
		},800);	
	});
	
	
	window.onfocus = function(){
		/*
					setTimeout(function(){
						var product = localStorage.getItem("product");
						
						
							if(product == "1"){
							var group_total_amt = document.getElementsByClassName("group_total_amt");
							var group_discount_amount = document.getElementsByClassName("group_discount_amount");
							var group_subtotal_amount = document.getElementsByClassName("group_subtotal_amount");
							var group_tax_amount = document.getElementsByClassName("group_tax_amount");
							var group_total_amount = document.getElementsByClassName("group_total_amount");
							var sumAmt = 0;
							var sumDiscountAmount = 0;
							var sumSubtotalAmount = 0;
							var sumTaxAmount = 0;
							var sumTotalAmount = 0;
							for(var i = 0; i < group_total_amt.length ; i++ ){
								sumAmt = eval(sumAmt + parseInt((group_total_amt[i].value).replace(/,/g,"")));
								
								if(i == eval(group_total_amt.length - 1)){
									document.getElementById("total_amt").value = changeCurrency(String(sumAmt));
									
								}
							}
							
							for(var i2 = 0; i2 < group_discount_amount.length ; i2++){
								sumDiscountAmount = eval(sumDiscountAmount + parseInt((group_discount_amount[i2].value).replace(/,/g,"")));
								if(i2 == group_discount_amount.length - 1){
									document.getElementById("discount_amount").value = changeCurrency(String(sumDiscountAmount));
								}
							}
							for(var i3 = 0; i3 < group_subtotal_amount.length ; i3++){
								sumSubtotalAmount = eval(sumSubtotalAmount + parseInt((group_subtotal_amount[i3].value).replace(/,/g,"")));
								if(i3 == group_subtotal_amount.length - 1){
									document.getElementById("subtotal_amount").value = changeCurrency(String(sumSubtotalAmount));
								}
							}
							for(var i4 = 0; i4 < group_tax_amount.length ; i4++){
								sumTaxAmount = eval(sumTaxAmount + parseInt((group_tax_amount[i4].value).replace(/,/g,"")));
								if(i4 == group_tax_amount.length - 1){
									document.getElementById("tax_amount").value = changeCurrency(String(sumTaxAmount));
								}
							}
							for(var i5 = 0; i5 < group_total_amount.length ; i5++){
								sumTotalAmount = eval(sumTotalAmount + parseInt((group_total_amount[i5].value).replace(/,/g,"")));
								if(i5 == group_total_amount.length - 1){
									document.getElementById("total_amount").value = changeCurrency(String(sumTotalAmount));
								}
							}
							
							localStorage.setItem("product","0");
							
						}	
					},1500);
					*/
			
	}
	var items = [];
	//var items1 = ;
	$("#SAVE.button.primary").removeAttr('type');
	$("#SAVE.button.primary").attr('type', 'button');
	$("#SAVE.button.primary").on("click",function(){
		var group_body  = document.getElementsByClassName("group_body");
		var group_name;
		var group_total_amt;
		var group_discount_amount; 
		var group_subtotal_amount;
		var group_tax_amount;
		var group_total_amount;
		var product_qty;
		var product_name;
		var product_part_number;
		var product_list_price;
		var product_discount_text;
		var product_discount_amount_select;
		var product_unit_price;
		var product_vat_amt_text;
		var product_vat_amt_select;
		var product_total_price;
		var product_item_description;
		var product_description;
		
		var service_name;
		var service_list_price;
		var service_discount_text;
		var service_discount_select;
		var service_unit_price;
		var service_vat_text;
		var service_vat_select;
		var service_total_price;
		for (var j = 0; j < group_body.length ; j++){
			//for (var j = 0; j < group_total_amt.length ; j++)
			//{
				var items1 = [];
				var items2 = [];
				localStorage.setItem("group_body.length",group_body.length);
				group_name = group_body[j].getElementsByClassName("group_name");
				group_total_amt = group_body[j].getElementsByClassName("group_total_amt");
				group_discount_amount = group_body[j].getElementsByClassName("group_discount_amount");
				group_subtotal_amount = group_body[j].getElementsByClassName("group_subtotal_amount");
				group_tax_amount = group_body[j].getElementsByClassName("group_tax_amount");
				group_total_amount = group_body[j].getElementsByClassName("group_total_amount");
				
				
				item = {};
				item.group_name = group_name[0].value;
				item.group_total_amt = group_total_amt[0].value.replace(/,/g,"");
				item.group_discount_amount = group_discount_amount[0].value.replace(/,/g,"");
				item.group_subtotal_amount = group_subtotal_amount[0].value.replace(/,/g,"");
				item.group_tax_amount = group_tax_amount[0].value.replace(/,/g,"");
				item.group_total_amount = group_total_amount[0].value.replace(/,/g,"");
				
				
				product_qty = group_body[j].getElementsByClassName("product_qty");
				product_name = group_body[j].getElementsByClassName("product_name");
				product_part_number = group_body[j].getElementsByClassName("product_part_number");
				product_list_price = group_body[j].getElementsByClassName("product_list_price");
				product_discount_text = group_body[j].getElementsByClassName("product_discount_text");
				product_discount_amount_select = group_body[j].getElementsByClassName("product_discount_amount_select");
				product_unit_price = group_body[j].getElementsByClassName("product_unit_price");
				product_vat_amt_text = group_body[j].getElementsByClassName("product_vat_amt_text");
				product_vat_amt_select = group_body[j].getElementsByClassName("product_vat_amt_select");
				product_total_price = group_body[j].getElementsByClassName("product_total_price");
				product_item_description = group_body[j].getElementsByClassName("product_item_description");
				product_description = group_body[j].getElementsByClassName("product_description");
				//var item1 = {};
				service_name = group_body[j].getElementsByClassName("service_name");
				service_list_price = group_body[j].getElementsByClassName("service_list_price");
				service_discount_text = group_body[j].getElementsByClassName("service_discount_text");
				service_discount_select = group_body[j].getElementsByClassName("service_discount_select");
				service_unit_price = group_body[j].getElementsByClassName("service_unit_price");
				service_vat_text = group_body[j].getElementsByClassName("service_vat_text");
				service_vat_select = group_body[j].getElementsByClassName("service_vat_select");
				service_total_price = group_body[j].getElementsByClassName("service_total_price");
				
				for (var o = 0; o < product_name.length ; o++)
				{
					var item1 = {};
					item1.product_qty = product_qty[o].value;
					item1.product_name = product_name[o].value;
					item1.product_part_number = product_part_number[o].value;
					item1.product_list_price = product_list_price[o].value.replace(/,/g,"");
					item1.product_discount_text = product_discount_text[o].value.replace(/,/g,"");
					item1.product_discount_amount_select = product_discount_amount_select[o].value;
					item1.product_unit_price = product_unit_price[o].value.replace(/,/g,"");
					item1.product_vat_amt_text = product_vat_amt_text[o].value.replace(/,/g,"");
					item1.product_vat_amt_select = product_vat_amt_select[o].value;
					item1.product_total_price = product_total_price[o].value.replace(/,/g,"");
					item1.product_item_description = product_item_description[o].value;
					item1.product_description = product_description[o].value;
					items1.push(item1);
					
					if(o == eval(product_name.length - 1)){
						//var itemss1 = JSON.stringify(items1);
						//localStorage.setItem("items1",itemss);
						item.product = items1;
						//items.push(item);
					}
					
											
				}
				
				for (var o1 = 0; o1 < service_name.length ; o1++)
				{
					var item2 = {};
					item2.service_name = service_name[o1].value;
					item2.service_list_price = service_list_price[o1].value.replace(/,/g,"");
					item2.service_discount_text = service_discount_text[o1].value.replace(/,/g,"");
					item2.service_discount_select = service_discount_select[o1].value;
					item2.service_unit_price = service_unit_price[o1].value.replace(/,/g,"");
					item2.service_vat_text = service_vat_text[o1].value.replace(/,/g,"");
					item2.service_vat_select = service_vat_select[o1].value;
					item2.service_total_price = service_total_price[o1].value.replace(/,/g,"");
					items2.push(item2);
					if(o1 == eval(service_name.length - 1)){
						//var itemss1 = JSON.stringify(items1);
						//localStorage.setItem("items1",itemss);
						item.service = items2;
						
					}
					
				}
				items.push(item);
				if(j == eval(group_body.length - 1)){
					var itemss = JSON.stringify(items);
					localStorage.setItem("items",itemss);
				}						
			//}
		}
	});
	}	
		
	if(y == "po_PO" && x == "DetailView" && recor != null && recor != undefined){
			var items = localStorage.getItem("items");
			//var items1 = localStorage.getItem("items1");
		//	console.log(items);
			
			if(items != null && items != undefined){
				items = JSON.parse(items);
				//console.log(items);
				$.ajax({
					type: "POST",
					url: "index.php?entryPoint=saveItem",
					data: {
					  items:items,
					  //items1:items1
					  id:recor
					},
					//dataType: "json",
					success: function(data) {
						localStorage.removeItem("items");
					},
					error: function (error) {
						
					}
				});
			}
			$(document).ready(function(){
					var hiddenn  = document.getElementsByClassName("hidden-xs")[1];
					var aa = hiddenn.getElementsByTagName("a")[0];
					//var otherTab = document.getElementById("tab1");
					//$("#tab1").removeAttr("id");
					aa.setAttribute("id", "tab3");
					var contentTab1 =  document.getElementById("tab-content-1");
					contentTab1.setAttribute("id", "tab-content-3");
					var ul = document.getElementsByClassName("nav-tabs")[0];
					var li = document.createElement("li");
					var aC = document.createElement("a");
					aC.innerHTML = "Các hàng mục";
					aC.setAttribute("class", "dropdown-toggle");
					li.appendChild(aC);
					li.setAttribute("onclick", "showGroupLine('"+recor+"')");
					//ul.appendChild(li);
					$( "<li class =\"dropdown-toggle\" onclick=\"showGroupLine('"+recor+"',this)\" ><a id=\"tab2\">Các hàng mục</a></li>" ).insertAfter($(".nav-tabs li:eq(0)"));
					
					
					var ul2 = document.getElementsByClassName("nav-tabs")[0];
					var li2 = document.createElement("li");
					var aC2 = document.createElement("a");
					aC2.innerHTML = "Hóa đơn cho";
					aC2.setAttribute("class", "dropdown-toggle");
					li2.appendChild(aC2);
					li2.setAttribute("onclick", "showPoAccount('"+recor+"')");
					$( "<li class =\"dropdown-toggle\" onclick=\"showPoAccount('"+recor+"',this)\" ><a id=\"tab1\">Hóa đơn cho</a></li>" ).insertAfter($(".nav-tabs li:eq(0)"));
					if(document.getElementById("tab-content-2") == null || document.getElementById("tab-content-2") == undefined){
						$( '<div class="tab-pane-NOBOOTSTRAPTOGGLER fade in" id="tab-content-2" style="display: none;"></div>' ).insertAfter($("#tab-content-0"));
					}
					if(document.getElementById("tab-content-1") == null || document.getElementById("tab-content-1") == undefined){
						$( '<div class="tab-pane-NOBOOTSTRAPTOGGLER fade in" id="tab-content-1" style="display: none;"></div>' ).insertAfter($("#tab-content-0"));
					}
					
					aa.onclick = function(){
					document.getElementById("tab-content-3").style.display = "block";
					}
					aa.onblur = function(){
					document.getElementById("tab-content-3").style.display = "none";
					}
			});
					
					
					
					
					
	}
	if((y == "po_PO" && x == "EditView" && recor != null && recor != undefined) || (adetailView == "DetailView" && modul == "po_PO")){
		var recordd = recor;
			if(recordd == null || recordd == undefined){
				 recordd = document.getElementsByName("record")[0];
				 if(recordd != null || recordd != undefined){
					recordd = document.getElementsByName("record")[0].value;
				 }else{
					recordd = '';
				 }
			}
			if(recordd != null && recordd != undefined && recordd != ""){
				$.ajax({
					type: "POST",
					url: "index.php?entryPoint=getItem",
					data: {
					  id:recordd
					},
					dataType: "json",
					success: function(data) {
						//console.log(data);
						if(data.billing_account_id !=null && data.billing_account_id != undefined){
						$("#billing_account").val(data.billing_account_id);
						}
						if(data.billing_contact_id !=null && data.billing_contact_id != undefined){
						$("#billing_contact").val(data.billing_contact_id);
						}
						$("#billing_address_street").val(data.po.billing_address_street);
						$("#billing_address_city").val(data.po.billing_address_city);
						$("#billing_address_state").val(data.po.billing_address_state);
						$("#billing_address_postalcode").val(data.po.billing_address_postalcode);
						$("#billing_address_country").val(data.po.billing_address_country);
						$("#shipping_address_street").val(data.po.shipping_address_street);
						$("#shipping_address_city").val(data.po.shipping_address_city);
						$("#shipping_address_state").val(data.po.shipping_address_state);
						$("#shipping_address_postalcode").val(data.po.shipping_address_postalcode);
						$("#shipping_address_country").val(data.po.shipping_address_country);
						for(let i = 0; i<data.lineItem.length;i++){
							insertGroup(0);
							for(let j = 0; j<data.lineItem[i][1].length; j++){
								setTimeout(function(){
									insertProductLine("product_group"+i,i);
								},300);
								
							}
							for(let j1 = 0; j1 < data.lineItem[i][2].length; j1++){
								setTimeout(function(){
									insertServiceLine("service_group"+i,i)
								},300);
								
							}
						}
						
						setTimeout(function(){
							var c = 0;
							var d = 0;
							var group_body  = document.getElementsByClassName("group_body");
							for (let k = 0; k < group_body.length ; k++){
								group_body[k].getElementsByClassName("group_name")[0].value = data.lineItem[k][0].name;
								var len = group_body[k].getElementsByClassName("product_qty");
								var len2 = group_body[k].getElementsByClassName("service_name");
								if(len != null && len != undefined){
									for(let o = 0; o < len.length; o++){
									group_body[k].getElementsByClassName("product_qty")[o].value = data.lineItem[k][1][o].product_qty;
									group_body[k].getElementsByClassName("product_name")[o].value = data.lineItem[k][1][o].name;
									group_body[k].getElementsByClassName("product_part_number")[o].value = data.lineItem[k][1][o].part_number;
									group_body[k].getElementsByClassName("product_list_price")[o].value = data.lineItem[k][1][o].product_list_price;
									group_body[k].getElementsByClassName("product_discount_text")[o].value = data.lineItem[k][1][o].product_discount_amount;
									group_body[k].getElementsByClassName("product_discount_amount_select")[o].value = data.lineItem[k][1][o].discount;
									group_body[k].getElementsByClassName("product_unit_price")[o].value = data.lineItem[k][1][o].product_unit_price;
									group_body[k].getElementsByClassName("product_vat_amt_text")[o].value = data.lineItem[k][1][o].vat_amt;
									group_body[k].getElementsByClassName("product_vat_amt_select")[o].value = data.lineItem[k][1][o].vat;
									group_body[k].getElementsByClassName("product_total_price")[o].value = data.lineItem[k][1][o].product_total_price;
									group_body[k].getElementsByClassName("product_item_description")[o].value = data.lineItem[k][1][o].item_description;
									group_body[k].getElementsByClassName("product_description")[o].value = data.lineItem[k][1][o].description;
									calculateLine(c,"product_");
									c++;
									}
								}
								if(len2 != null && len2 != undefined){
									for(let o1 = 0; o1 < len2.length; o1++){
										group_body[k].getElementsByClassName("service_name")[o1].value = data.lineItem[k][2][o1].name;
										group_body[k].getElementsByClassName("service_list_price")[o1].value = data.lineItem[k][2][o1].product_list_price;
										group_body[k].getElementsByClassName("service_discount_text")[o1].value = data.lineItem[k][2][o1].product_discount_amount;
										group_body[k].getElementsByClassName("service_discount_select")[o1].value = data.lineItem[k][2][o1].discount;
										group_body[k].getElementsByClassName("service_unit_price")[o1].value = data.lineItem[k][2][o1].product_unit_price;
										group_body[k].getElementsByClassName("service_vat_text")[o1].value = data.lineItem[k][2][o1].vat_amt;
										group_body[k].getElementsByClassName("service_vat_select")[o1].value = data.lineItem[k][2][o1].vat;
										group_body[k].getElementsByClassName("service_total_price")[o1].value = data.lineItem[k][2][o1].product_total_price;
										calculateLine(d,"service_");
										d++;
									}
								}
								/*
							if(k == eval(group_body.length - 1)){
					setTimeout(function(){
							var group_total_amt = document.getElementsByClassName("group_total_amt");
							var group_discount_amount = document.getElementsByClassName("group_discount_amount");
							var group_subtotal_amount = document.getElementsByClassName("group_subtotal_amount");
							var group_tax_amount = document.getElementsByClassName("group_tax_amount");
							var group_total_amount = document.getElementsByClassName("group_total_amount");
							var sumAmt = 0;
							var sumDiscountAmount = 0;
							var sumSubtotalAmount = 0;
							var sumTaxAmount = 0;
							var sumTotalAmount = 0;
							for(var i = 0; i < group_total_amt.length ; i++ ){
								sumAmt = eval(sumAmt + parseInt((group_total_amt[i].value).replace(/,/g,"")));
								
								if(i == eval(group_total_amt.length - 1)){
									document.getElementById("total_amt").value = changeCurrency(String(sumAmt));
									
								}
							}
							
							for(var i2 = 0; i2 < group_discount_amount.length ; i2++){
								sumDiscountAmount = eval(sumDiscountAmount + parseInt((group_discount_amount[i2].value).replace(/,/g,"")));
								if(i2 == group_discount_amount.length - 1){
									document.getElementById("discount_amount").value = changeCurrency(String(sumDiscountAmount));
								}
							}
							for(var i3 = 0; i3 < group_subtotal_amount.length ; i3++){
								sumSubtotalAmount = eval(sumSubtotalAmount + parseInt((group_subtotal_amount[i3].value).replace(/,/g,"")));
								if(i3 == group_subtotal_amount.length - 1){
									document.getElementById("subtotal_amount").value = changeCurrency(String(sumSubtotalAmount));
								}
							}
							for(var i4 = 0; i4 < group_tax_amount.length ; i4++){
								sumTaxAmount = eval(sumTaxAmount + parseInt((group_tax_amount[i4].value).replace(/,/g,"")));
								if(i4 == group_tax_amount.length - 1){
									document.getElementById("tax_amount").value = changeCurrency(String(sumTaxAmount));
								}
							}
							for(var i5 = 0; i5 < group_total_amount.length ; i5++){
								sumTotalAmount = eval(sumTotalAmount + parseInt((group_total_amount[i5].value).replace(/,/g,"")));
								if(i5 == group_total_amount.length - 1){
									document.getElementById("total_amount").value = changeCurrency(String(sumTotalAmount));
								}
							}
					},500);
				
								}	
								*/
							}
							
						},1500);
					},
					error: function (error) {
						
					}
				});
			}
		$(document).ready(function(){
			var ExpDate = document.getElementById("expdatepayment_date").value;
			var Dliverydate = document.getElementById("expected_deliverydate_date").value;
			var OrderDate = document.getElementById("orderdate_date").value;
			document.getElementById("expdatepayment_date").value = ExpDate.split(' ')[0];
			document.getElementById("expected_deliverydate_date").value = Dliverydate.split(' ')[0];
			document.getElementById("orderdate_date").value = OrderDate.split(' ')[0];
		});
			
	}
	/*
	if(y == "AOS_Products" && x == "Popup"){
		localStorage.setItem("product","1");
	}
	*/
	if((y == "Users" && x == "EditView" && recor != null && recor != undefined) || (adetailView == "EditView" && modul == "Users")){
		
		var recordd = recor;
			if(recordd == null || recordd == undefined){
				 recordd = document.getElementsByName("record")[0].value;
			}
			$.ajax({
					type: "POST",
					url: "index.php?entryPoint=checkUser",
					data: {
					 // id:recordd
					},
					dataType: "json",
					success: function(data) {
						
					 if(data.status != "1"){
						if(data != "1"){
						 document.getElementById('user_name').readOnly = true;
						 var disVal =  document.getElementById('districtcode_c').value;
						var proVal =  document.getElementById('provincecode_c').value;
						 $('#districtcode_c').on("change",function(){
							document.getElementById('districtcode_c').value = disVal;
							 
						 });
						 $('#provincecode_c').on("change",function(){
							 document.getElementById('provincecode_c').value = proVal;
							 
						 });
						 $('#department').prop('disabled',true);
						 
					 }
					 }
					}
					});
	 }
	 var duplicateId = document.getElementsByName("old_id");
	 if(duplicateId.length == 0){
		 duplicateId = null;
	 }
	 if((y == "EmailTemplates" && x == "EditView") || ((adetailView == "DetailView" || adetailView == "index")  && modul == "EmailTemplates" && ((recor != null && recor != undefined && recor != "") || (aReturnId != null && aReturnId != undefined && aReturnId != "") || (duplicateId != null && duplicateId != undefined && duplicateId != "")))){
		
		$( "<div id=\"temId\">Template Zalo ZNS  </div>").insertAfter($("#btn_clr"));
		$( "#temId" ).parent().parent().css( "width", "40%" );
		var html = ''; 
		
		html += "&nbsp<span><input id=\"template_id_c\" name=\"template_id_c\" ></span>";
		document.getElementById("temId").innerHTML = document.getElementById("temId").innerHTML + html;
		var dropdown = document.getElementsByName('type')[0];
		var opt = document.createElement('option');
			opt.value = "zalo";
			opt.innerHTML = "Zalo OA Template";
		dropdown.appendChild(opt);
		if((recor != null && recor != undefined && recor != "") || (aReturnId != null && aReturnId != undefined && aReturnId != "")){
			if(recor != null && recor != undefined){
				var idTem = recor;
			}else if(aReturnId != null && aReturnId != undefined){
				var idTem = aReturnId;
			}
			
		}else{
				var idTem = document.getElementsByName("old_id")[0];
				if(idTem != null && idTem != undefined){
					idTem = document.getElementsByName("old_id")[0].value;
				}else{
					idTem = null;
				}
				
		}
		if(idTem != null){
				$.ajax({
						type: "POST",
						url: "index.php?entryPoint=getTemplateId",
						data: {
						  id:idTem
						},
						dataType:"json",
						success: function(dat) {
							if(dat != null && dat != undefined && dat != "" ){
								$("#template_id").val(dat.id);
								document.getElementsByName('type')[0].value = dat.type;
							}
						}
				});
		}
		 
	}
	 if((y == "cs_cusUser" && x == "EditView") || (adetailView == "DetailView" && modul == "cs_cusUser")){
		var recordd = recor;
			if(recordd == null || recordd == undefined){
				 recordd = document.getElementsByName("record")[0];
				 if(recordd != null || recordd != undefined){
					recordd = document.getElementsByName("record")[0].value;
				 }else{
					recordd = ""; 
				 }
			}
			$.ajax({
					type: "POST",
					url: "index.php?entryPoint=checkUser",
					data: {
					 // id:recordd
					},
					dataType: "json",
					success: function(dat){
						if(dat.status != "1"){
							// document.getElementById('username').readOnly = true;
							if(recordd != ""){
								var disVal =  document.getElementById('districtcode_c').value;
								var proVal =  document.getElementById('provincecode').value;
								//$('#districtcode_c').on("change",function(){
								//	document.getElementById('districtcode_c').value = disVal;
								//});
								$('#provincecode').on("change",function(){
									document.getElementById('provincecode').value = proVal;
								});
							}else{
								document.getElementById('provincecode').value = dat.province ;
								var proVal =  document.getElementById('provincecode').value;
								$('#provincecode').on("change",function(){
									document.getElementById('provincecode').value = proVal;
								});
							}
								setTimeout(function(){
									$("#role option").each(function()
									{
										if($(this).text() == "TCT_ROLE"){
											$(this).remove();
											$("#role").val(""); 
											
										}
									});
								},1300);
							 
						}
					}
			});
	 }
	 
	
	 if(y == "Repor_ReportSummarDetail" && x == "index"){
		 $.ajax({
					type: "POST",
					url: "index.php?entryPoint=getLanguage",
					dataType: "json",
					success: function(dat) {
		var html = '';
		html+='<div class="row" style="margin-bottom: 10px">';
		html+='<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">';
		html+='<div>Module</div>';
        html+='<select style="width:160px" id="module">';
		html+='<option value="default">------</option>';
		html+='<option selected="" value="Leads">Leads</option>';
		html+='<option value="Accounts">Accounts</option>';
		html+='<option value="Contacts">Contacts</option>';
		html+='<option value="Users">Users</option>';
        html+='</select>';
		html+='</div>';
		html+='<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">';
		html+='<div>Thời gian</div>';
        html+=' <select style="width:200px" name="period" id="period" onchange="periodChange()">';
		html+='<option value="default">------</option>';
		html+='<option selected="" value="this_week">Tuần này</option>';
		html+='<option value="last_week">Tuần trước</option>';
		html+='<option value="this_month">Tháng này</option>';
		html+='<option value="last_month">Tháng trước</option>';
		html+='<option value="this_year">Năm này</option>';
		html+='<option value="last_year">Năm trước</option>';
		html+='<option value="is_between">Khoảng thời gian</option>';
        html+='</select>';
		html+='<div id="date_range" class="text-center" style="margin-top:20px">';
        html+='<div class="row" style="margin-bottom: 5px">';
        html+='<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-left">';
        html+='<label>Từ</label>';
        html+='</div>';
        html+='<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 text-left">';
        html+='<input id="date_from"  name="date_from" type="text" style="width:170px;" maxlength="100" value="" title="" tabindex="-1" accesskey="9">';
        html+='<br><span class="error" style="display: none;">Please select a date</span>';
        html+='</div>';
		html+='<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 datepicker-trigger">';
		html+='</div>';
		html+='</div>';
        html+='<div class="row" style="margin-bottom: 5px">';
        html+='<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-left">';
        html+='<label>Đến</label>';
        html+='</div>';
        html+='<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 text-left">';
        html+='<input type="text" style="width:170px;"  id="date_to" name="date_to" maxlength="100" value="" title="" tabindex="-1" accesskey="9">';
        html+='<br><span class="error" style="display: none;">Please select a date</span>';
        html+='</div>';
		html+='<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 datepicker-trigger">';
		html+='</div>';
        html+='</div>';
        html+='</div>';
        html+='</div>';
        html+='<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 ">';
		html+='<div>Tỉnh/TP</div>';
        html+='<select style="width:200px" name="group-users[]" id="provincecode_c" onchange="loadGroup()">';
		html+='<option  value="" selected="selected">----</option>';							
		html+='<option  label="" value="All">Chọn tất cả</option>';
		html+='<option style="display:none" label=" TP Hà Nội" value="10" id="10"> TP Hà Nội</option>';
		html+='<option style="display:none" label=" Tỉnh Hưng Yên" value="16" id="16"> Tỉnh Hưng Yên</option>';
		html+='<option style="display:none" label=" Tỉnh Hải Dương" value="17" id="17"> Tỉnh Hải Dương</option>';
		html+='<option style="display:none" label=" TP Hải Phòng " value="18" id="18"> TP Hải Phòng </option>';
		html+='<option style="display:none" label=" Tỉnh Quảng Ninh " value="20" id="20"> Tỉnh Quảng Ninh </option>';
		html+='<option style="display:none" label=" Tỉnh Bắc Ninh " value="22" id="22"> Tỉnh Bắc Ninh </option>';
		html+='<option style="display:none" label=" Tỉnh Bắc Giang " value="23" id="23"> Tỉnh Bắc Giang </option>';
		html+='<option style="display:none" label=" Tỉnh Lạng Sơn " value="24" id="24"> Tỉnh Lạng Sơn </option>';
		html+='<option style="display:none" label=" Tỉnh Thái Nguyên " value="25" id="25"> Tỉnh Thái Nguyên </option>';
		html+='<option style="display:none" label=" Tỉnh Bắc Kạn " value="26" id="26"> Tỉnh Bắc Kạn </option>';
		html+='<option style="display:none" label=" Tỉnh Cao Bằng " value="27" id="27"> Tỉnh Cao Bằng </option>';
		html+='<option style="display:none" label=" Tỉnh Vĩnh Phúc " value="28" id="28"> Tỉnh Vĩnh Phúc </option>';
		html+='<option style="display:none" label=" Tỉnh Phú Thọ " value="29" id="29"> Tỉnh Phú Thọ </option>';
		html+='<option style="display:none" label=" Tỉnh Tuyên Quang " value="30" id="30"> Tỉnh Tuyên Quang </option>';
		html+='<option style="display:none" label=" Tỉnh Hà Giang " value="31" id="31"> Tỉnh Hà Giang </option>';
		html+='<option style="display:none" label=" Tỉnh Yên Bái " value="32" id="32"> Tỉnh Yên Bái </option>';
		html+='<option style="display:none" label=" Tỉnh Lào Cai " value="33" id="33"> Tỉnh Lào Cai </option>';
		html+='<option style="display:none" label=" Tỉnh Hoà Bình " value="35" id="35"> Tỉnh Hoà Bình </option>';
		html+='<option style="display:none" label=" Tỉnh Sơn La " value="36" id="36"> Tỉnh Sơn La </option>';
		html+='<option style="display:none" label=" Tỉnh Điện Biên " value="38" id="38"> Tỉnh Điện Biên </option>';
		html+='<option style="display:none" label=" Tỉnh Lai Châu " value="39" id="39"> Tỉnh Lai Châu </option>';
		html+='<option style="display:none" label=" Tỉnh Hà Nam " value="40" id="40"> Tỉnh Hà Nam </option>';
		html+='<option style="display:none" label=" Tỉnh Thái Bình " value="41" id="41"> Tỉnh Thái Bình </option>';
		html+='<option style="display:none" label=" Tỉnh Nam Định " value="42" id="42"> Tỉnh Nam Định </option>';
		html+='<option style="display:none" label=" Tỉnh Ninh Bình " value="43" id="43"> Tỉnh Ninh Bình </option>';
		html+='<option style="display:none" label=" Tỉnh Thanh Hoá " value="44" id="44"> Tỉnh Thanh Hoá </option>';
		html+='<option style="display:none" label=" Tỉnh Nghệ An " value="46" id="46"> Tỉnh Nghệ An </option>';
		html+='<option style="display:none" label=" Tỉnh Hà Tĩnh " value="48" id="48"> Tỉnh Hà Tĩnh </option>';
		html+='<option style="display:none" label=" Tỉnh Quảng Bình " value="51" id="51"> Tỉnh Quảng Bình </option>';
		html+='<option style="display:none" label=" Tỉnh Quảng Trị " value="52" id="52"> Tỉnh Quảng Trị </option>';
		html+='<option style="display:none" label=" Tỉnh Thừa Thiên Huế " value="53" id="53"> Tỉnh Thừa Thiên Huế </option>';
		html+='<option style="display:none" label=" TP Đà Nẵng " value="55" id="55"> TP Đà Nẵng </option>';
		html+='<option style="display:none" label=" Tỉnh Quảng Nam " value="56" id="56"> Tỉnh Quảng Nam </option>';
		html+='<option style="display:none" label=" Tỉnh Quảng Ngãi " value="57" id="57"> Tỉnh Quảng Ngãi </option>';
		html+='<option style="display:none" label=" Tỉnh Kon Tum " value="58" id="58"> Tỉnh Kon Tum </option>';
		html+='<option style="display:none" label=" Tỉnh Bình Định " value="59" id="59"> Tỉnh Bình Định </option>';
		html+='<option style="display:none" label=" Tỉnh Gia Lai " value="60" id="60"> Tỉnh Gia Lai </option>';
		html+='<option style="display:none" label=" Tỉnh Phú Yên " value="62" id="62"> Tỉnh Phú Yên </option>';
		html+='<option style="display:none" label=" Tỉnh Đắk Lăk " value="63" id="63"> Tỉnh Đắk Lăk </option>';
		html+='<option style="display:none" label=" Tỉnh Đắk Nông " value="64" id="64"> Tỉnh Đắk Nông </option>';
		html+='<option style="display:none" label=" Tỉnh Khánh Hoà " value="65" id="65"> Tỉnh Khánh Hoà </option>';
		html+='<option style="display:none" label=" Tỉnh Ninh Thuận " value="66" id="66"> Tỉnh Ninh Thuận </option>';
		html+='<option style="display:none" label=" Tỉnh Lâm Đồng " value="67" id="67"> Tỉnh Lâm Đồng </option>';
		html+='<option style="display:none" label=" TP Hồ Chí Minh " value="70" id="70"> TP Hồ Chí Minh </option>';
		html+='<option style="display:none" label=" Tỉnh Bà Rịa Vũng Tàu " value="79" id="79"> Tỉnh Bà Rịa Vũng Tàu </option>';
		html+='<option style="display:none" label=" Tỉnh Bình Thuận " value="80" id="80"> Tỉnh Bình Thuận </option>';
		html+='<option style="display:none" label=" Tỉnh Đồng Nai " value="81" id="81"> Tỉnh Đồng Nai </option>';
		html+='<option style="display:none" label=" Tỉnh Bình Dương " value="82" id="82"> Tỉnh Bình Dương </option>';
		html+='<option style="display:none" label=" Tỉnh Bình Phước " value="83" id="83"> Tỉnh Bình Phước </option>';
		html+='<option style="display:none" label=" Tỉnh Tây Ninh " value="84" id="84"> Tỉnh Tây Ninh </option>';
		html+='<option style="display:none" label=" Tỉnh Long An " value="85" id="85"> Tỉnh Long An </option>';
		html+='<option style="display:none" label=" Tỉnh Tiền Giang " value="86" id="86"> Tỉnh Tiền Giang </option>';
		html+='<option style="display:none" label=" Tỉnh Đồng Tháp " value="87" id="87"> Tỉnh Đồng Tháp </option>';
		html+='<option style="display:none" label=" Tỉnh An Giang " value="88" id="88"> Tỉnh An Giang </option>';
		html+='<option style="display:none" label=" Tỉnh Vĩnh Long " value="89" id="89"> Tỉnh Vĩnh Long </option>';
		html+='<option style="display:none" label=" TP Cần Thơ " value="90" id="90"> TP Cần Thơ </option>';
		html+='<option style="display:none" label=" Tỉnh Hậu Giang " value="91" id="91"> Tỉnh Hậu Giang </option>';
		html+='<option style="display:none" label=" Tỉnh Kiên Giang " value="92" id="92"> Tỉnh Kiên Giang </option>';
		html+='<option style="display:none" label=" Tỉnh Bến Tre " value="93" id="93"> Tỉnh Bến Tre </option>';
		html+='<option style="display:none" label=" Tỉnh Trà Vinh " value="94" id="94"> Tỉnh Trà Vinh </option>';
		html+='<option style="display:none" label=" Tỉnh Sóc Trăng " value="95" id="95"> Tỉnh Sóc Trăng </option>';
		html+='<option style="display:none" label=" Tỉnh Bạc Liêu " value="96" id="96"> Tỉnh Bạc Liêu </option>';
		html+='<option style="display:none" label=" Tỉnh Cà Mau " value="97" id="97"> Tỉnh Cà Mau </option>';
		html+='<option style="display:none" label=" Vnpost " value="98" id="98"> Vnpost </option>';
		html+='<option style="display:none" label=" PPTT " value="99" id="99"> PPTT </option>';
		html+='<option style="display:none" label=" DVBC " value="100" id="100"> DVBC </option>';
		html+='<option style="display:none" label=" TCBC " value="101" id="101"> TCBC </option>';
		html+='<option style="display:none" label=" HCC " value="102" id="102"> HCC </option>';
		html+='</select>';
        html+='</div>';
		html+='<div id="groupUser" class="col-xs-12 col-sm-12 col-md-2 col-lg-2">';
		html+='<div>Quận/Huyện</div>';
		html+='<input style="width:200px;" type="text" placeholder="Search.." id="myInput" onkeyup="filterKey()">';
        html+='<select style="width:200px;height:100px" name="group-users[]" id="group-users" multiple="multiple">';
		html+='</select>';
        html+='</div>';
		html+='<div id="userTitle" class="col-xs-12 col-sm-12 col-md-2 col-lg-2">';
		html+='<div>Chức vụ</div>';
		html+='<input style="width:200px;" type="text" placeholder="Search.." id="myInput3" onkeyup="filterKey3()">';
        html+='<select style="width:200px;height:100px;" name="title-users[]" onclick="clearUser()" id="title-users" multiple="multiple">';
		html+='</select>';
        html+='</div>';
		html+='<div id="listUser" class="col-xs-12 col-sm-12 col-md-2 col-lg-2 ">';
		html+='<div>Người dùng</div>';
		html+='<input style="width:200px;" type="text" placeholder="Search.." id="myInput2" onkeyup="filterKey2()">';
        html+='<select style="width:200px;height:100px;" name="users[]" id="users" multiple="multiple">';
		html+='</select>';
        html+='</div>';
        html+='</div>';
		html+='<div class="row">';
        html+='<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center" style="margin-left:-5px">';
        html+='<button id="filter" class="button" onclick="moduleFilter(\'0\')" style="width:130px;">Thống kê</button>';
        html+='</div>';
		html+='<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center" style="margin-left:5px">';
        html+='<input type="button" onclick="refreshFilter()" class="button" value="Xóa ĐK Lọc">';
        html+='</div>';
		html+='<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center">';
		html+='<input type="button" onclick="fnExcelReport()" class="button" value="Xuất excel">';
        html+='</div>';
        html+='</div>';
        html+='<div id="father-group" style="margin-top:5px;padding-top:10px;">';
		html+='<table id="userView" class="table table-striped table-bordered cell-border dataTable" style="width:100%;">';
		html+='<thead>';
		html+='<tr style="background-color:#4b97c4;color:white;font-weight:bold;text-align:center;font-size:16px;">';
		html+='<td style="padding-right:3px;padding-left:3px;vertical-align:middle">STT</td>';
		html+='<td style="vertical-align:middle">'+dat.LBL_LAST_NAME+'</td>';
		html+='<td style="vertical-align:middle">'+dat.LBL_MOBILE_PHONE+'</td>';
		html+='<td style="vertical-align:middle">'+dat.LBL_ACCOUNT_NAME+'</td>';
		html+='<td style="vertical-align:middle">'+dat.LBL_CAMPAIGN_NAME+'</td>';
		html+='<td style="vertical-align:middle">'+dat.LBL_DESCRIPTION+'</td>';
		html+='<td style="vertical-align:middle">'+dat.LBL_LIST_STATUS+'</td>';
		html+='<td style="vertical-align:middle">'+dat.LBL_ADDRESS_INFORMATION+'</td>';
		html+='<td style="vertical-align:middle">'+dat.LBL_EMAIL_ADDRESS+'</td>';
		html+='<td style="vertical-align:middle">'+dat.LBL_LEAD_SOURCE+'</td>';
		html+='<td style="vertical-align:middle">'+dat.LBL_PRODUCT_NAME+'</td>';
		html+='<td style="vertical-align:middle">'+dat.LBL_ASSIGNED_TO_NAME+'</td>';
		html+='<td style="vertical-align:middle">'+dat.LBL_LIST_DATE_ENTERED+'</td>';
		html+='<td style="vertical-align:middle">'+dat.LBL_GROUP_NAME+'</td>';
		html+='</tr>';
		html+='</thead>';
		html+='</table>';
		html+='</div>';
		
		
		$(document).ready(function(){
			document.getElementsByClassName("listViewBody")[0].innerHTML = html;
			$.datepicker.setDefaults({
				showOn: "both",
				dateFormat: convertSugarDateFormatToDatePickerFormat(),
			});
			
			showProvince();
			loadProvince();
			showHideDateFields($('#period').val());
			
			$('#userView').DataTable({
				"pagingType": "full_numbers",
				"bDestroy" : true,
				"bSort":false,
				"bPaginate":true,
				"sPaginationType":"full_numbers",
				"iDisplayLength": 10,
				'stateSave': true,
				"pageLength":100,
				"lengthMenu": [ 25, 50, 100 ]
		
			});
		});
		}
		}); 
	 }
	 if(y == "Users" && x == "DetailView"){
		  
		 var userId = GetURLParameter('record');
		 $("#delete_button").removeAttr('onclick');
		 $("#delete_button").on("click",function(){
			 confirmDelete();
			 if(confirm('Bạn chắc muốn xoá hoàn toàn bản ghi?')){
			$.ajax({
				type: "POST",
				url: "index.php?entryPoint=deleteUser",
				data: {
						id:userId,
						module:y
					},
				success: function(res){
					
				}
			});
			 }
		});
		 
	 }
	 if(y == "bill_Billing" && x == "EditView"){
		 var textEle=document.getElementById("billingamount");
	if(textEle != null && textEle != undefined){
		 var str;
		 var i;
		 var st;
		 var j;
		 var text_val;
		 var cusText;
		 textEle.onkeyup=function(){
		 cusText='';
		 str='';
		 text_val=textEle.value;
		  
		 for(var u=0;u<text_val.length;u++)
		 {
		 if(text_val[u]!=",")
		  cusText+=text_val[u];
		 
		 }
		
			
			i=eval(cusText.length-1);
			j=eval(cusText.length-4);
		while(i>-1){
		if(i==j && cusText.length > 3 ){
			j-=3;
		 str=cusText[i]+","+str;
		
		 }else{
		 str=cusText[i]+str;
		 }
		 i--;
		  }
		
		
		 textEle.value=str;
		 }
	}
		 
	 }
	 if(((y == "Leads" || y == "Accounts" || y == "Contacts" || y == "Opportunities" ) && x=="index") || (modul == "Leads" || modul == "Accounts" || modul == "Contacts" || modul == "Opportunities" ) && adetailView == "index"){
		
		 $("#massupdate_listview_top").on("click",function(){
			 var massUp = document.getElementById("massUp");;
			 if(massUp == null || massUp == undefined){
			 setTimeout(function(){
			// var div = document.createElement("div");
			html = '';
			html += '<td scope="row" width="15%">Chiến dịch:</td>';
			html += '<td id = "massUp">';
			
			html += '<span><input class="sqsEnabled yui-ac-input" autocomplete="off" id="mass_campaign_name" name="campaign_name" type="text" value=""></span>';
			html += '<div id="MassUpdate_mass_campaign_name_results" class="yui-ac-container">';
			html += '<div class="yui-ac-content" style="display: none;">';
			html += '<div class="yui-ac-hd" style="display: none;">';
			html += '</div>';
			html += '<div class="yui-ac-bd">';
			html += '<ul>';
			html += '<li style="display: none;">';
			html += '</li>';
			html += '<li style="display: none;">';
			html += '</li>';
			html += '<li style="display: none;">';
			html += '</li>';
			html += '<li style="display: none;">';
			html += '</li>';
			html += '<li style="display: none;">';
			html += '</li>';
			html += '<li style="display: none;">';
			html += '</li>';
			html += '<li style="display: none;">';
			html += '</li>';
			html += '<li style="display: none;">';
			html += '</li>';
			html += '<li style="display: none;">';
			html += '</li>';
			html += '<li style="display: none;">';
			html += '</li>';
			html += '</ul>';
			html += '</div>';
			html += '<div class="yui-ac-ft" style="display: none;">';
			html += '</div>';
			html += '</div>';
			html += '</div>';
			html += '<input id="mass_campaign_id" name="campaign_id" type="hidden" value="">';
			html += '<span class="id-ff multiple">';
			html += '<button id="mass_campaign_name_btn" title="Chọn" type="button" class="button" value="Chọn" name="btn1" onclick="open_popup(\'Campaigns\', 600, 400, \'\', true, false, {\'call_back_function\':\'set_return\',\'form_name\':\'MassUpdate\',\'field_to_name_array\':{\'id\':\'campaign_id\',\'name\':\'campaign_name\'}});">';
			html += '<span class="suitepicon suitepicon-action-select">';
			html += '</span>';
			html += '</button>';
			html += '</span>';
			html += '</td>';
			var massUpdate = document.getElementById("mass_update_div");
			var tBody = massUpdate.getElementsByTagName('tbody')[0];
			tBody.innerHTML = html + tBody.innerHTML;
			 },800);
			 }
		 })
	 }
		
	if((y == "rp_ReportActivitiesDetail" && x == "index") || (adetailView == "DetailView" && modul == "rp_ReportActivitiesDetail")){
		var title = document.getElementsByClassName("moduleTitle")[0];
		document.getElementById("content").innerHTML = "";
		document.getElementById("content").appendChild(title);
		
		html = "";
		html += '<div id="cde" style="margin-top:20px;">';
		html += '<div class="row" style="margin-bottom: 10px">';
							html += '<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 text-left" >';
							html += '<div>Thời gian</div>';
                            html += '<select style="width:200px" name="period" id="period" onchange="periodChange()">';
							html += '<option value="default">------</option>';
							html += '<option selected="" value="this_week">Tuần này</option>';
							html += '<option value="last_week">Tuần trước</option>';
							html += '<option value="this_month">Tháng này</option>';
							html += '<option value="last_month">Tháng trước</option>';
							html += '<option value="this_year">Năm này</option>';
							html += '<option value="last_year">Năm trước</option>';
							html += '<option value="is_between">Khoảng thời gian</option>';
                            html += '</select>';
							html += '<div id="date_range" class="text-center" style="margin-top:20px">';
                    html += '<div class="row" style="margin-bottom: 5px">';
                    html += ' <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-left">';
                    html += ' <label>Từ</label>';
                    html += '</div>';
                    html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 text-left">';
                    html += '<input id="date_from"  name="date_from" type="text" style="width:170px;" maxlength="100" value="" title="" tabindex="-1" accesskey="9">';
                    html += '<br><span class="error" style="display: none;">Please select a date</span>';
                    html += '</div>';
					html += '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 datepicker-trigger">';
					html += '</div>';
					html += '</div>';
                   html += ' <div class="row" style="margin-bottom: 5px">';
                    html += '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-left">';
                    html += ' <label>Đến</label>';
                    html += '</div>';
                    html += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 text-left">';
                   html += '<input type="text" style="width:170px;"  id="date_to" name="date_to" maxlength="100" value="" title="" tabindex="-1" accesskey="9">';
                    html += ' <br><span class="error" style="display: none;">Please select a date</span>';
                   html += '</div>';
					html += '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 datepicker-trigger">';
						
					html += '</div>';
                    html += '</div>';
                html += '</div>';
                 html += '</div>';
                html += ' <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 text-left ">';
				html += '<div>Tỉnh/TP</div>';
                html += '<select style="width:200px" name="group-users[]" id="provincecode_c" onchange="loadGroup()">';
										
																		html += '<option  label="" value="All" selected="selected">Chọn tất cả</option>';
																		html += '<option style="display:none" label=" TP Hà Nội" value="10" id="10"> TP Hà Nội</option>';
																		html += '<option style="display:none" label=" Tỉnh Hưng Yên" value="16" id="16"> Tỉnh Hưng Yên</option>';
																		html += '<option style="display:none" label=" Tỉnh Hải Dương" value="17" id="17"> Tỉnh Hải Dương</option>';
																		html += '<option style="display:none" label=" TP Hải Phòng " value="18" id="18"> TP Hải Phòng </option>';
																		html += '<option style="display:none" label=" Tỉnh Quảng Ninh " value="20" id="20"> Tỉnh Quảng Ninh </option>';
																		html += '<option style="display:none" label=" Tỉnh Bắc Ninh " value="22" id="22"> Tỉnh Bắc Ninh </option>';
																		html += '<option style="display:none" label=" Tỉnh Bắc Giang " value="23" id="23"> Tỉnh Bắc Giang </option>';
																		html += '<option style="display:none" label=" Tỉnh Lạng Sơn " value="24" id="24"> Tỉnh Lạng Sơn </option>';
																		html += '<option style="display:none" label=" Tỉnh Thái Nguyên " value="25" id="25"> Tỉnh Thái Nguyên </option>';
																		html += '<option style="display:none" label=" Tỉnh Bắc Kạn " value="26" id="26"> Tỉnh Bắc Kạn </option>';
																		html += '<option style="display:none" label=" Tỉnh Cao Bằng " value="27" id="27"> Tỉnh Cao Bằng </option>';
																		html += '<option style="display:none" label=" Tỉnh Vĩnh Phúc " value="28" id="28"> Tỉnh Vĩnh Phúc </option>';
																		html += '<option style="display:none" label=" Tỉnh Phú Thọ " value="29" id="29"> Tỉnh Phú Thọ </option>';
																		html += '<option style="display:none" label=" Tỉnh Tuyên Quang " value="30" id="30"> Tỉnh Tuyên Quang </option>';
																		html += '<option style="display:none" label=" Tỉnh Hà Giang " value="31" id="31"> Tỉnh Hà Giang </option>';
																		html += '<option style="display:none" label=" Tỉnh Yên Bái " value="32" id="32"> Tỉnh Yên Bái </option>';
																		html += '<option style="display:none" label=" Tỉnh Lào Cai " value="33" id="33"> Tỉnh Lào Cai </option>';
																		html += '<option style="display:none" label=" Tỉnh Hoà Bình " value="35" id="35"> Tỉnh Hoà Bình </option>';
																		html += '<option style="display:none" label=" Tỉnh Sơn La " value="36" id="36"> Tỉnh Sơn La </option>';
																		html += '<option style="display:none" label=" Tỉnh Điện Biên " value="38" id="38"> Tỉnh Điện Biên </option>';
																		html += '<option style="display:none" label=" Tỉnh Lai Châu " value="39" id="39"> Tỉnh Lai Châu </option>';
																		html += '<option style="display:none" label=" Tỉnh Hà Nam " value="40" id="40"> Tỉnh Hà Nam </option>';
																		html += '<option style="display:none" label=" Tỉnh Thái Bình " value="41" id="41"> Tỉnh Thái Bình </option>';
																		html += '<option style="display:none" label=" Tỉnh Nam Định " value="42" id="42"> Tỉnh Nam Định </option>';
																		html += '<option style="display:none" label=" Tỉnh Ninh Bình " value="43" id="43"> Tỉnh Ninh Bình </option>';
																		html += '<option style="display:none" label=" Tỉnh Thanh Hoá " value="44" id="44"> Tỉnh Thanh Hoá </option>';
																		html += '<option style="display:none" label=" Tỉnh Nghệ An " value="46" id="46"> Tỉnh Nghệ An </option>';
																		html += '<option style="display:none" label=" Tỉnh Hà Tĩnh " value="48" id="48"> Tỉnh Hà Tĩnh </option>';
																		html += '<option style="display:none" label=" Tỉnh Quảng Bình " value="51" id="51"> Tỉnh Quảng Bình </option>';
																		html += '<option style="display:none" label=" Tỉnh Quảng Trị " value="52" id="52"> Tỉnh Quảng Trị </option>';
																		html += '<option style="display:none" label=" Tỉnh Thừa Thiên Huế " value="53" id="53"> Tỉnh Thừa Thiên Huế </option>';
																		html += '<option style="display:none" label=" TP Đà Nẵng " value="55" id="55"> TP Đà Nẵng </option>';
																		html += '<option style="display:none" label=" Tỉnh Quảng Nam " value="56" id="56"> Tỉnh Quảng Nam </option>';
																		html += '<option style="display:none" label=" Tỉnh Quảng Ngãi " value="57" id="57"> Tỉnh Quảng Ngãi </option>';
																		html += '<option style="display:none" label=" Tỉnh Kon Tum " value="58" id="58"> Tỉnh Kon Tum </option>';
																		html += '<option style="display:none" label=" Tỉnh Bình Định " value="59" id="59"> Tỉnh Bình Định </option>';
																		html += '<option style="display:none" label=" Tỉnh Gia Lai " value="60" id="60"> Tỉnh Gia Lai </option>';
																		html += '<option style="display:none" label=" Tỉnh Phú Yên " value="62" id="62"> Tỉnh Phú Yên </option>';
																		html += '<option style="display:none" label=" Tỉnh Đắk Lăk " value="63" id="63"> Tỉnh Đắk Lăk </option>';
																		html += '<option style="display:none" label=" Tỉnh Đắk Nông " value="64" id="64"> Tỉnh Đắk Nông </option>';
																		html += '<option style="display:none" label=" Tỉnh Khánh Hoà " value="65" id="65"> Tỉnh Khánh Hoà </option>';
																		html += '<option style="display:none" label=" Tỉnh Ninh Thuận " value="66" id="66"> Tỉnh Ninh Thuận </option>';
																		html += '<option style="display:none" label=" Tỉnh Lâm Đồng " value="67" id="67"> Tỉnh Lâm Đồng </option>';
																		html += '<option style="display:none" label=" TP Hồ Chí Minh " value="70" id="70"> TP Hồ Chí Minh </option>';
																		html += '<option style="display:none" label=" Tỉnh Bà Rịa Vũng Tàu " value="79" id="79"> Tỉnh Bà Rịa Vũng Tàu </option>';
																		html += '<option style="display:none" label=" Tỉnh Bình Thuận " value="80" id="80"> Tỉnh Bình Thuận </option>';
																		html += '<option style="display:none" label=" Tỉnh Đồng Nai " value="81" id="81"> Tỉnh Đồng Nai </option>';
																		html += '<option style="display:none" label=" Tỉnh Bình Dương " value="82" id="82"> Tỉnh Bình Dương </option>';
																		html += '<option style="display:none" label=" Tỉnh Bình Phước " value="83" id="83"> Tỉnh Bình Phước </option>';
																		html += '<option style="display:none" label=" Tỉnh Tây Ninh " value="84" id="84"> Tỉnh Tây Ninh </option>';
																		html += '<option style="display:none" label=" Tỉnh Long An " value="85" id="85"> Tỉnh Long An </option>';
																		html += '<option style="display:none" label=" Tỉnh Tiền Giang " value="86" id="86"> Tỉnh Tiền Giang </option>';
																		html += '<option style="display:none" label=" Tỉnh Đồng Tháp " value="87" id="87"> Tỉnh Đồng Tháp </option>';
																		html += '<option style="display:none" label=" Tỉnh An Giang " value="88" id="88"> Tỉnh An Giang </option>';
																		html += '<option style="display:none" label=" Tỉnh Vĩnh Long " value="89" id="89"> Tỉnh Vĩnh Long </option>';
																		html += '<option style="display:none" label=" TP Cần Thơ " value="90" id="90"> TP Cần Thơ </option>';
																		html += '<option style="display:none" label=" Tỉnh Hậu Giang " value="91" id="91"> Tỉnh Hậu Giang </option>';
																		html += '<option style="display:none" label=" Tỉnh Kiên Giang " value="92" id="92"> Tỉnh Kiên Giang </option>';
																		html += '<option style="display:none" label=" Tỉnh Bến Tre " value="93" id="93"> Tỉnh Bến Tre </option>';
																		html += '<option style="display:none" label=" Tỉnh Trà Vinh " value="94" id="94"> Tỉnh Trà Vinh </option>';
																		html += '<option style="display:none" label=" Tỉnh Sóc Trăng " value="95" id="95"> Tỉnh Sóc Trăng </option>';
																		html += '<option style="display:none" label=" Tỉnh Bạc Liêu " value="96" id="96"> Tỉnh Bạc Liêu </option>';
																		html += '<option style="display:none" label=" Tỉnh Cà Mau " value="97" id="97"> Tỉnh Cà Mau </option>';
																		html += '<option style="display:none" label=" Vnpost " value="98" id="98"> Vnpost </option>';
																		html += '<option style="display:none" label=" PPTT " value="99" id="99"> PPTT </option>';
																		html += '<option style="display:none" label=" DVBC " value="100" id="100"> DVBC </option>';
																		html += '<option style="display:none" label=" TCBC " value="101" id="101"> TCBC </option>';
																		html += '<option style="display:none" label=" DVKH " value="102" id="102"> DVKH </option>';
																		html += '<option style="display:none" label=" Global " value="103" id="103"> Global </option>';
        
									                                html += '</select>';
                            html += '</div>';
							html += '<div id="groupUser" class="col-xs-12 col-sm-12 col-md-3 col-lg-3">';
							html += '<div>Quận/Huyện</div>';
							html += '<input style="width:250px;" type="text" placeholder="Search.." id="myInput" onkeyup="filterKey()">';
                               html += ' <select style="width:250px;height:100px" name="group-users[]" id="group-users" multiple="multiple">';
                                
									                                html += '</select>';
                           html += '</div>';
							 html += '<div id="userTitle" class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-left ">';
							 html += '<div>Chức vụ</div>';
							html += '<input style="width:250px;" type="text" placeholder="Search.." id="myInput3" onkeyup="filterKey3()">';
                             html += '<select style="width:250px;height:100px;" onclick="clearUser()" name="title-users[]" id="title-users" multiple="multiple">';
                                
									                               html += ' </select>';
                            html += '</div>';
							html += '<div id="listUser" class="col-xs-12 col-sm-12 col-md-2 col-lg-2 ">';
							html += '<div>Người dùng</div>';
							html += '<input style="width:250px;" type="text" placeholder="Search.." id="myInput2" onkeyup="filterKey2()">';
                            html += '<select style="width:250px;height:100px;" name="users[]" id="users" multiple="multiple">';
																		
        
									                                html += '</select>';
                            html += '</div>';
							
                html += '</div>';
               
				
			  html += '<div class="row">';
                     html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center" style="margin-left:-5px">';
                       html += ' <button id="filter" class="button" onclick="actFilter()" style="width:130px;">Thống kê</button>';
                    html += '</div>';
					html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center" style="margin-left:5px">';
                    html += '<input type="button" onclick="refreshFilter()" class="button" value="Xóa ĐK Lọc">';
                    html += '</div>';
					html += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center">';
					html += '<input type="button" onclick="fnExcelReport()" class="button" value="Xuất excel">';
                    html += '</div>';
                html += '</div>';
				 html += '<div id="showChart">';
				html += '<canvas id="chartShow" style="margin-top:5px" style="width:100%;"> ';
				html += '</canvas> ';
				 html += '</div>';
          html += '<div id="father-group" style="margin-top:5px" style="width:100%;"> ';
		html += '<table id="userView" class="table table-striped table-bordered cell-border dataTable"  style="width:100%;">';
		html+='<thead>';
		html+='<tr style="background-color:#4b97c4;color:white;font-weight:bold;text-align:center;font-size:16px;">';
		html+='<td style="vertical-align:middle">Tỉnh thành</td>';
		html+='<td style="vertical-align:middle;column-width: 130px">Tên</td>';
		html+='<td style="vertical-align:middle">Ngày</td>';
		html+='<td style="vertical-align:middle">Module</td>';
		html+='<td style="vertical-align:middle;column-width: 130px">Liên quan tới</td>';
		html+='<td style="vertical-align:middle;column-width: 130px">Thông tin liên hệ</td>';
		html+='<td style="vertical-align:middle">Tình trạng</td>';
		html+='<td style="vertical-align:middle;column-width: 90px">Doanh số dự kiến</td>';
		html+='<td style="vertical-align:middle">Ghi chú</td>';
		html+='<td style="vertical-align:middle">Sản phẩm dịch vụ</td>';
		html+='<td style="vertical-align:middle">Nguồn</td>';
		html+='<td style="vertical-align:middle">Người dùng</td>';
		html+='</tr>';
		html+='</thead>';
		html+='</table>';
		html += '</div>';
		
		html += '</div>';
		$(document).ready(function(){
			document.getElementById("content").innerHTML += html;
				$.datepicker.setDefaults({
					showOn: "both",
					dateFormat: convertSugarDateFormatToDatePickerFormat(),
				});
			showProvince();
			loadProvince();
			showHideDateFields($('#period').val());
			$('#userView').DataTable({
				"columnDefs": [{
					"defaultContent": "-",
					"targets": "_all"
				}],
				"pagingType": "full_numbers",
				"bDestroy" : true,
				"bSort":false,
				"bPaginate":true,
				"sPaginationType":"full_numbers",
				"iDisplayLength": 10,
				'stateSave': true,
				"pageLength":100,
				"lengthMenu": [ 25, 50, 100 ]
		
			});
			 document.getElementById('userView_filter').remove();
			 document.getElementById('userView_length').remove();
		});
		//
		
			
			
			
		
		//
    
		
	}
	
  });

  setTimeout(function () {
    hideEmptyFormCellsOnTablet();
  }, 1500);

  var listViewCheckboxInit = function () {
    var checkboxesInitialized = false;
    var checkboxesInitializeInterval = false;
    var checkboxesCountdown = 100;
    var initializeBootstrapCheckboxes = function () {
      if (!checkboxesInitialized) {
        if ($('.glyphicon.bootstrap-checkbox').length == 0) {
          if (!checkboxesInitializeInterval) {
            checkboxesInitializeInterval = setInterval(function () {
              checkboxesCountdown--;
              if (checkboxesCountdown <= 0) {
                clearInterval(checkboxesInitializeInterval);
                return;
              }
              initializeBootstrapCheckboxes();
            }, 100);
          }
        } else {
          $('.glyphicon.bootstrap-checkbox').each(function (i, e) {
            $(e).removeClass('hidden');
            $(e).next().hide();
            refreshListViewCheckbox(e);
            if (!$(e).hasClass('initialized-checkbox')) {
              $(e).click(function () {
                $(this).next().click();
                refreshListViewCheckbox($(this));
              });
              $(e).addClass('initialized-checkbox');
            }
          });

          $('#selectLink > li > ul > li > a, #selectLinkTop > li > ul > li > a, #selectLinkBottom > li > ul > li > a').click(function (e) {
            e.preventDefault();
            $('.glyphicon.bootstrap-checkbox').each(function (i, e) {
              refreshListViewCheckbox(e);
            });
          });

          checkboxesInitialized = true;
          clearInterval(checkboxesInitializeInterval);
          checkboxesInitializeInterval = false;
        }
      }
    };
    initializeBootstrapCheckboxes();
  };
  setInterval(function () {
    listViewCheckboxInit();
  }, 100);

});

$(document).ready(function(){
	var role = document.getElementById("role");
	if(role != null && role != undefined){
		var CuserModule = GetURLParameter('record');
		if(CuserModule != null && CuserModule != undefined){
			var CuserId = CuserModule;
		}else{
			CuserModule = document.getElementsByName("record")[0].value; 
			if(CuserModule != null && CuserModule != undefined && CuserModule != ''){
			var CuserId = CuserModule;
			}else{
			var CuserId = '';	
			}
		}
		$.ajax({
                type: "POST",
                url: "index.php?entryPoint=getRole",
                data: {
                   id:CuserId
                },
                dataType: "json",
                success: function(data) {
					var roleId;
					var length=data.length;
					var html='';
					for(var i=0;i<length;i++){
					html+='<option value="'+data[i].id+'" selected="selected">'+data[i].name+'</option>';
						roleId = data[i].roleId;
					}
					html+='<option value="" selected="selected"></option>';
					role.innerHTML=html;
					if(roleId != ''){
						$("#role").val(roleId).change();
					}
                },
                error: function (error) {
                    
                }
            });
			setTimeout(function(){
				$.ajax({
					type: "POST",
					url: "index.php?entryPoint=getGroupName",
					data: {
						id:CuserId							
					},
					dataType: "json",
					success: function(res) {
							$("#groupname").val(res.groupname);
							$("#groupname2").val(res.groupname2);
							
					}
				});
			},2000);
			
	}
	
});
	
	
	
