 var flag_popup     = false;
 var userext     = false;
 var customernumber;
 var parent_type;
 var parent_name;
 var fullname;
 var account_id;
 var play=0;
 var phoneNum;
 var permission;
 var sec=0;
 var hr=0;
 var min=0;
 var stop=0;
 var branch;
 var Cookie_name;
 var us_id;
 var check_cookie=getCookie("token");
 var fromNumber;
 var fromNumber1;
 var type2;
 var access_token;
 var account_name;
 var callComing;
 var is_opening = false;
 var  lead_mods = {"LBL_ID":"ID","LBL_DATE_ENTERED":"Date Created","LBL_DATE_MODIFIED":"Date Modified","LBL_MODIFIED":"Modified By","LBL_MODIFIED_NAME":"Modified By Name","LBL_CREATED":"Created By","LBL_DESCRIPTION":"Description:","LBL_DELETED":"Deleted","LBL_NAME":"Name:","LBL_CREATED_USER":"Created User","LBL_MODIFIED_USER":"Modified User","LBL_LIST_NAME":"Name","LBL_EDIT_BUTTON":"Edit","LBL_REMOVE":"Remove","LBL_ASCENDING":"Ascending","LBL_DESCENDING":"Descending","LBL_OPT_IN":"Opt In","LBL_OPT_IN_PENDING_EMAIL_NOT_SENT":"Pending Confirm opt in, Confirm opt in not sent","LBL_OPT_IN_PENDING_EMAIL_SENT":"Pending Confirm opt in, Confirm opt in sent","LBL_OPT_IN_CONFIRMED":"Opted in","LBL_ASSIGNED_TO_ID":"Assigned User:","LBL_ASSIGNED_TO_NAME":"Assigned to","LBL_SECURITYGROUPS":"Security Groups","LBL_SECURITYGROUPS_SUBPANEL_TITLE":"LBL_SECURITYGROUPS_SUBPANEL_TITLE","LBL_SALUTATION":"Salutation","LBL_FIRST_NAME":"First Name:","LBL_LAST_NAME":"Last Name:","LBL_TITLE":"Title:","LBL_DEPARTMENT":"Department:","LBL_DO_NOT_CALL":"Do Not Call:","LBL_HOME_PHONE":"Home Phone:","LBL_MOBILE_PHONE":"Mobile:","LBL_OFFICE_PHONE":"Office Phone:","LBL_OTHER_PHONE":"Other Phone:","LBL_FAX_PHONE":"Fax:","LBL_EMAIL_ADDRESS":"Email Address:","LBL_PRIMARY_ADDRESS":"Primary Address:","LBL_PRIMARY_ADDRESS_STREET":"Primary Address Street","LBL_PRIMARY_ADDRESS_STREET_2":"Primary Address Street 2","LBL_PRIMARY_ADDRESS_STREET_3":"Primary Address Street 3","LBL_PRIMARY_ADDRESS_CITY":"City","LBL_PRIMARY_ADDRESS_STATE":"District","LBL_PRIMARY_ADDRESS_POSTALCODE":"Ward","LBL_PRIMARY_ADDRESS_COUNTRY":"Country","LBL_ALT_ADDRESS":"Other Address:","LBL_ALT_ADDRESS_STREET":"Alt Address Street","LBL_ALT_ADDRESS_STREET_2":"Alt Address Street 2","LBL_ALT_ADDRESS_STREET_3":"Alt Address Street 3","LBL_ALT_ADDRESS_CITY":"City","LBL_ALT_ADDRESS_STATE":"District","LBL_ALT_ADDRESS_POSTALCODE":"Ward","LBL_ALT_ADDRESS_COUNTRY":" Country","LBL_PRIMARY_STREET":"Address","LBL_ALT_STREET":"Other Address","LBL_STREET":"Street","LBL_CITY":"City:","LBL_STATE":"District:","LBL_POSTAL_CODE":"Ward:","LBL_COUNTRY":"Country:","LBL_CONTACT_INFORMATION":"OVERVIEW","LBL_ADDRESS_INFORMATION":"Address Information","LBL_OTHER_EMAIL_ADDRESS":"Other Email:","LBL_ASSISTANT":"Assistant","LBL_ASSISTANT_PHONE":"Assistant Phone","LBL_WORK_PHONE":"Work Phone","LNK_IMPORT_VCARD":"Create Lead From vCard","LBL_ANY_EMAIL":"Any Email:","LBL_EMAIL_NON_PRIMARY":"Non Primary E-mails","LBL_PHOTO":"Photo","LBL_LAWFUL_BASIS":"Lawful Basis","LBL_DATE_REVIEWED":"Lawful Basis Date Reviewed","LBL_LAWFUL_BASIS_SOURCE":"Lawful Basis Source","LBL_CONSENT":"Consent","db_last_name":"LBL_LIST_LAST_NAME","db_first_name":"LBL_LIST_FIRST_NAME","db_title":"LBL_LIST_TITLE","db_email1":"LBL_LIST_EMAIL_ADDRESS","db_account_name":"LBL_LIST_ACCOUNT_NAME","db_email2":"LBL_LIST_EMAIL_ADDRESS","ERR_DELETE_RECORD":"A record number must be specified to delete the lead.","LBL_ACCOUNT_DESCRIPTION":"Account Description","LBL_ACCOUNT_ID":"Account ID","LBL_ACCOUNT_NAME":"Account Name:","LBL_ACTIVITIES_SUBPANEL_TITLE":"Activities","LBL_ADD_BUSINESSCARD":"Add Business Card","LBL_ALTERNATE_ADDRESS":"Other Address:","LBL_ANY_ADDRESS":"Any Address:","LBL_ANY_PHONE":"Any Phone:","LBL_BUSINESSCARD":"Convert Lead","LBL_CONTACT_ID":"Contact ID","LBL_CONTACT_NAME":"Lead Name:","LBL_CONTACT_OPP_FORM_TITLE":"Lead-Opportunity:","LBL_CONTACT_ROLE":"Role:","LBL_CONTACT":"Lead:","LBL_CONVERTED_ACCOUNT":"Converted Account:","LBL_CONVERTED_CONTACT":"Converted Contact:","LBL_CONVERTED_OPP":"Converted Opportunity:","LBL_CONVERTED":"Converted","LBL_CONVERTLEAD_BUTTON_KEY":"V","LBL_CONVERTLEAD_TITLE":"Convert Lead","LBL_CONVERTLEAD":"Convert Lead","LBL_CONVERTLEAD_WARNING":"Warning: The status of the Lead you are about to convert is ","LBL_CONVERTLEAD_WARNING_INTO_RECORD":" Possible Contact: ","LBL_CREATED_NEW":"Created a new","LBL_CREATED_ACCOUNT":"Created a new account","LBL_CREATED_CALL":"Created a new call","LBL_CREATED_CONTACT":"Created a new contact","LBL_CREATED_MEETING":"Created a new meeting","LBL_CREATED_OPPORTUNITY":"Created a new opportunity","LBL_DEFAULT_SUBPANEL_TITLE":"Leads","LBL_DUPLICATE":"Similar Leads","LBL_EMAIL_OPT_OUT":"Email Opt Out:","LBL_EXISTING_ACCOUNT":"Used an existing account","LBL_EXISTING_CONTACT":"Used an existing contact","LBL_EXISTING_OPPORTUNITY":"Used an existing opportunity","LBL_HISTORY_SUBPANEL_TITLE":"History","LBL_IMPORT_VCARD":"Import vCard","LBL_VCARD":"vCard","LBL_IMPORT_VCARDTEXT":"Automatically create a new lead by importing a vCard from your file system.","LBL_INVALID_EMAIL":"Invalid Email:","LBL_INVITEE":"Direct Reports","LBL_LEAD_SOURCE_DESCRIPTION":"Lead Source Description:","LBL_LEAD_SOURCE":"Lead Source:","LBL_LIST_ACCEPT_STATUS":"Accept Status","LBL_LIST_ACCOUNT_NAME":"Account Name","LBL_LIST_CONTACT_NAME":"Lead Name","LBL_LIST_CONTACT_ROLE":"Role","LBL_LIST_DATE_ENTERED":"Date Created","LBL_LIST_EMAIL_ADDRESS":"Email","LBL_LIST_FIRST_NAME":"First Name","LBL_LIST_FORM_TITLE":"Lead List","LBL_LIST_LAST_NAME":"Last Name","LBL_LIST_LEAD_SOURCE_DESCRIPTION":"Lead Source Description","LBL_LIST_LEAD_SOURCE":"Lead Source","LBL_LIST_MY_LEADS":"My Leads","LBL_LIST_PHONE":"Office Phone","LBL_LIST_REFERED_BY":"Referred By","LBL_LIST_STATUS":"Status","LBL_LIST_TITLE":"Title","LBL_MODULE_NAME":"Leads","LBL_MODULE_TITLE":"Leads: Home","LBL_NEW_FORM_TITLE":"New Lead","LBL_OPP_NAME":"Opportunity Name:","LBL_OPPORTUNITY_AMOUNT":"Opportunity Amount:","LBL_OPPORTUNITY_ID":"Opportunity ID","LBL_OPPORTUNITY_NAME":"Opportunity Name:","LBL_PHONE":"Phone:","LBL_PORTAL_APP":"Portal Application","LBL_PORTAL_INFORMATION":"Portal Information","LBL_PORTAL_NAME":"Portal Name:","LBL_REFERED_BY":"Referred By:","LBL_REPORTS_TO_ID":"Reports To ID","LBL_REPORTS_TO":"Reports To:","LBL_SEARCH_FORM_TITLE":"Lead Search","LBL_SELECT_CHECKED_BUTTON_LABEL":"Select Checked Leads","LBL_SELECT_CHECKED_BUTTON_TITLE":"Select Checked Leads","LBL_STATUS_DESCRIPTION":"Status Description:","LBL_STATUS":"Status:","LNK_LEAD_LIST":"View Leads","LNK_NEW_ACCOUNT":"Create Account","LNK_NEW_APPOINTMENT":"Create Appointment","LNK_NEW_CONTACT":"Create Contact","LNK_NEW_LEAD":"Create Lead","LNK_NEW_NOTE":"Create Note","LNK_NEW_TASK":"Create Task","LNK_NEW_CASE":"Create Case","LNK_NEW_CALL":"Log Call","LNK_NEW_MEETING":"Schedule Meeting","LNK_NEW_OPPORTUNITY":"Create Opportunity","LNK_SELECT_ACCOUNTS":" OR Select Account","LNK_SELECT_CONTACTS":" OR Select Contact","NTC_DELETE_CONFIRMATION":"Are you sure you want to delete this record?","NTC_REMOVE_CONFIRMATION":"Are you sure you want to remove this lead from this case?","LBL_CAMPAIGN_LIST_SUBPANEL_TITLE":"Campaigns","LBL_CAMPAIGN":"Campaign:","LBL_LIST_ASSIGNED_TO_NAME":"Assigned User","LBL_PROSPECT_LIST":"Prospect List","LBL_CAMPAIGN_LEAD":"Campaigns","LBL_BIRTHDATE":"Birthdate:","LBL_CAMPAIGNS":"Campaigns","LBL_CONVERT_MODULE_NAME":"Module","LBL_CONVERT_REQUIRED":"Required","LBL_CONVERT_SELECT":"Allow Selection","LBL_CONVERT_COPY":"Copy Data","LBL_CONVERT_EDIT":"Edit","LBL_CONVERT_DELETE":"Delete","LBL_CONVERT_ADD_MODULE":"Add Module","LBL_CREATE":"Create","LBL_SELECT":" OR Select","LBL_WEBSITE":"Website","LNK_IMPORT_LEADS":"Import Leads","LBL_MODULE_TIP":"The module to create a new record in.","LBL_REQUIRED_TIP":"Required modules must be created or selected before the lead can be converted.","LBL_COPY_TIP":"If checked, fields from the lead will be copied to fields with the same name in the newly created records.","LBL_SELECTION_TIP":"Modules with a relate field in Contacts can be selected rather than created during the convert lead process.","LBL_EDIT_TIP":"Modify the convert layout for this module.","LBL_DELETE_TIP":"Remove this module from the convert layout.","LBL_ACTIVITIES_MOVE":"Move Activities to","LBL_ACTIVITIES_COPY":"Copy Activities to","LBL_ACTIVITIES_MOVE_HELP":"Select the record to which to move the Lead&#039;s activities. Tasks, Calls, Meetings, Notes and Emails will be moved to the selected record(s).","LBL_ACTIVITIES_COPY_HELP":"Select the record(s) for which to create copies of the Lead&#039;s activities. New Tasks, Calls, Meetings and Notes will be created for each of the selected record(s). Emails will be related to the selected record(s).","LBL_CAMPAIGN_ID":"Campaign ID","LBL_EDITLAYOUT":"Edit Layout","LBL_ENTERDATE":"Enter Date","LBL_LOADING":"Loading","LBL_EDIT_INLINE":"Edit","LBL_FP_EVENTS_LEADS_1_FROM_FP_EVENTS_TITLE":"Events","LBL_WWW":"WWW","LBL_NEW_COLUMN_TITLE":"New","LBL_IN_PROCESS_COLUMN_TITLE":"In Process","LBL_CONVERTED_COLUMN_TITLE":"Converted","LBL_INVOICED_COLUMN_TITLE":"Transaction","LBL_ETS_AMOUNT_TITLE":"Revenue","LBL_COMMISSION_AMOUNT_TITLE":"Total Revenue","LBL_SEARCH_TITLE":"Search","LBL_DETAIL_LABEL":"Detail","LBL_FILTER_LABEL":"Filter activites & revenue","LBL_PLEASE_SELECT_A_DATE":"Please select a date","LBL_BTN_FILTER":"Filter","LBL_DATE_FROM":"From","LBL_DATE_TO":"To","LBL_PERIOD":"Period","LBL_ADD_CALL_MODAL_TITLE":"New call","LBL_ADD_MEETING_MODAL_TITLE":"New meeting","LBL_FULL_VIEW":"Full form","LBL_NO_DATA":"No date","LBL_ADD_NEW_CARD":"New","LBL_CHOOSE_A_RECORD":"Select a record to preview","LBL_PROBABILITY":"Probability(%)","LBL_INDUSTRY":"L\u0129nh v\u1ef1c ho\u1ea1t \u0111\u1ed9ng","LBL_EDITVIEW_PANEL1":"New Panel 1","LBL_DATE_PROCESSED":"Date Processed","LBL_DATE_CONVERTED":"Date Converted","LBL_DATE_TRANSACTION":"Date Transaction","LBL_DATE_NEW":"Date New","LBL_NEW_OPPORTUNITY":"Create Opportunity","LBL_DETAILVIEW_PANEL1":"ADD INFORMATION","LBL_DETAILVIEW_PANEL2":"New Panel 2","LBL_ACCOUNT":"Account","LBL_EDITVIEW_PANEL2":"ADDRESS INFORMATION","LBL_QUICKCREATE_PANEL1":"MORE INFORMATION","LBL_QUICKCREATE_PANEL2":"ADD INFORMATION","LBL_CONTACTS":"Contacts","LBL_LEADS":"Leads","LBL_OPPORTUNITIES":"Opportunities","LBL_TASKS":"Tasks","LBL_NOTES":"Notes","LBL_MEETINGS":"Meetings","LBL_CALLS":"Calls","LBL_BUSSINESS_TYPE":"Bussiness Type"};

 window.onload = function(e) {
  //init();
  setInterval(
    function () {
      if(!userext){
        //getuser()
      }
      if(!flag_popup){
        //getOnCall();
      } else {
        checkCallComplete();
            //if(check_outbound){
               // checkCallAnswer();
            //}
            setTime();
            if($('#phone_mobile').val() == '')
              $('#phone_mobile').val($('#phone-mobile-popup').text());
          }
       // getCallRing();  

     }, 1000
     );

};
function refreshLeadCard(cb)
{


   var pageNum = parseInt($('ul.nav.nav-tabs.nav-dashboard li.active a').first().attr('id').substring(3));
    retrievePage(pageNum, function (res) {
        if(cb) {
            cb(res);
        }
    });
}

function closeModal(modal_id) {
    $('#' + modal_id).find('.modal-body').html('');
    $('#' + modal_id).modal('hide');
}
function closedetailModal2()
{
    closeModal('detailModal');
    clearInterval(process_subpanel);
	
}
function showCreationModal2(type, status) {
    showSpinner('creationModal');
	var fromNum=getCookie("fromNum");
    setTimeout(function() {
        var url = "index.php?module=Leads&action=QuickCreate&sugar_body_only=1";
        if(type == 'Leads') {
            url = "index.php?module=Leads&action=QuickCreate&sugar_body_only=1";
        }
        else {
            url = "index.php?module=Accounts&action=QuickCreate&sugar_body_only=1";
        }
        $.get(url, function (res) {
            $('#creationModal').find('.modal-body').html(res);
            $('#creationModal').find('.modal-body').find('#save_and_continue, #Leads_subpanel_full_form_button, #Leads_dcmenu_cancel_button, #Accounts_subpanel_full_form_button, #Accounts_dcmenu_cancel_button').remove();
            $('#creationModal').find('.modal-body').find('#status').val(status).prop('disabled', true);
            $('#creationModal').find('.modal-body').find('#' + type + '_dcmenu_save_button').attr('onclick', "handleSaveLead('" + type + "','" + status + "')").attr('type', 'button');
            $('#creationModal').find('.modal-body').append('<script type="text/javascript" src="custom/modules/Leads/js/Validate.js"></script>');
			$(document).ready(function(){
				$('#phone_mobile').val(fromNum);
				changeCur("opportunity_amount");
				var account_name=document.getElementsByClassName('edit-view-row-item')[6];
				var account_name_label=account_name.getElementsByClassName("label")[0];
				var requ=account_name_label.getElementsByClassName("required")[0];
				if($('#bussiness_type_c').val() == "B2B"){
					var req=document.getElementsByClassName('required')[0];
					var clone=req.cloneNode(true);
					account_name_label.appendChild(clone);
				}
				$('#bussiness_type_c').on('change',function(){
					account_name=document.getElementsByClassName('edit-view-row-item')[6];
					account_name_label=account_name.getElementsByClassName("label")[0];
					requ=account_name_label.getElementsByClassName("required")[0];
					if($('#bussiness_type_c').val() == "B2B"){
						if(requ == null || requ == undefined){
						var req=document.getElementsByClassName('required')[0];
						var clone=req.cloneNode(true);
						account_name_label.appendChild(clone);
						}
					}
					else{
						
						if(requ != null && requ != undefined){
						
						requ.remove();
						
						}
						
					}
				// <span class="required">*</span>
				});
				//start
				//var emailInput=document.querySelector("[id^=Leads1emailAddress]");
				var pb=document.getElementById('phone_mobile_dup_detector_info');
				var show_info = pb.cloneNode(true);
				show_info.setAttribute("id", "show_info1");
				//
				
				var email_address=document.getElementsByClassName("email-address-input-group");
				for (var i=1;i<email_address.length;i++){
					
					email_address[i].appendChild(show_info);
				
					
					
					var leads2=email_address[i].querySelector("[id^='Leads']");
					
					leads2.onblur=function(){
						/*
						if(typeof(total_element) == "undefined")
						var total_element;
				if (typeof (total_element) == 'undefined')
					total_element = 0;
					total_element++;
					
					inside_submit = true;
					*/
					$('#creationModal').find('.modal-body').find('#error').hide();
					
					var class_info2 = "show_info1";// viết thêm class show lỗi
					var form_name2 = 'form_DCQuickCreate_' + type;//name form
					var module_name2 = type;
					var field_value2 = encodeURIComponent(leads2.value);
					
					var field_name2 = leads2.id;
					var record_id2 = $('#' + form_name2 + ' input[name="record"]').val();
					var data2 = "record=" + record_id2 + "&module_name=" + module_name2 + "&field_name=" + field_name2 + "&field_value=" + field_value2;
					/*
					if (form_name2.substring(0, 16) == "form_QuickCreate" || form_name2.substring(0, 24) == "form_SubpanelQuickCreate" || form_name2.substring(0, 18) == "form_DCQuickCreate")
						var curSubmit = $("input[name$='save_button']", $('#' + form_name2));
					else
						var curSubmit = $("input[type=submit]", $('#' + form_name2));
					*/
					if (field_value2 != '') {
           // $("#" + class_info).show();
            $("#" + class_info2).fadeIn(400).html('<img src="custom/include/SugarFields/Fields/Dupdetector/image/ajax-loading.gif" />');
					$.ajax({
                type: "POST",
                url: "index.php?module=Dupdetector&action=check_dup&to_pdf=1&sugar_body_only=true&record_id=" + record_id2,
                data: data2,
                cache: false,
                async: true,
                success: function(result) {
                    if (result != '') {
                        res = eval('(' + result + ')');
                        total_element--;
                        if (res.status == '') {
                            $("#" + class_info2).html('');
                            
                        } else {
							inside_submit == true
							//alert(res.status);
                            ele_status[field_name2] = res.status;
                            var more_info_msg2 = '<img border="0"  id ="tooltip_' + field_name2 + '" onclick="return SUGAR.util.showHelpTips(this,ele_status['+field_name2+']);" src="themes/default/images/helpInline.gif" >';
                            $("#" + class_info2).html('<img src="custom/include/SugarFields/Fields/Dupdetector/image/error.gif" /> Khách hàng này đã tồn tại ! ' + more_info_msg2);
                            SUGAR.util.evalScript(more_info_msg2);
                            
                        }
                    }
                    //SUGAR.ajaxUI.hideLoadingPanel();
                   // if (total_element <= 0 && inside_submit == true) {
                    //    inside_submit = false;
                    //    $(curSubmit).trigger("click");
                   // }
                },
                failure: function() {
                    //SUGAR.ajaxUI.hideLoadingPanel();
                }
            });
			
			
					}
					else{
						
						$("#" + class_info2).html('');
						
						
					}
						
					};
					
				}
				
				
					
				
				
				$('.email-address-add-button').on("click",function(){
					
					//while(email_address.length<5){
					//email_address=document.getElementsByClassName("email-address-input-group");
					
					
					var show_inf = pb.cloneNode(true);
					show_inf.setAttribute("id", "show_info"+eval(email_address.length-1));
					email_address[email_address.length-1].appendChild(show_inf);
					//alert("sucessful");
					var leads1=email_address[email_address.length-1].querySelector("[id^='Leads']");
					//alert(leads.id);
					
					leads1.onblur=function(){
						/*
						if(typeof(total_element) == "undefined")
						var total_element;
				if (typeof (total_element) == 'undefined')
					total_element = 0;
					total_element++;
					
					inside_submit = true;
					*/
					$('#creationModal').find('.modal-body').find('#error').hide();
					
					// viết thêm class show lỗi
					var form_name = 'form_DCQuickCreate_' + type;//name form
					var module_name1 = type;
					var field_value1 = encodeURIComponent(this.value);
					
					var field_name1 = this.id;
					var e=parseInt(field_name1.slice(18));
					var class_info1 = "show_info"+eval(e+1);
					var record_id1 = $('#' + form_name + ' input[name="record"]').val();
					var data1 = "record=" + record_id1 + "&module_name=" + module_name1 + "&field_name=" + field_name1 + "&field_value=" + field_value1;
					/*
					if (form_name.substring(0, 16) == "form_QuickCreate" || form_name.substring(0, 24) == "form_SubpanelQuickCreate" || form_name.substring(0, 18) == "form_DCQuickCreate")
						var curSubmit = $("input[name$='save_button']", $('#' + form_name));
					else
						var curSubmit = $("input[type=submit]", $('#' + form_name));
					*/
					if (field_value1 != '') {
            //$("#" + class_info).show();
			
            $("#" + class_info1).fadeIn(400).html('<img src="custom/include/SugarFields/Fields/Dupdetector/image/ajax-loading.gif" />');
				$.ajax({
                type: "POST",
                url: "index.php?module=Dupdetector&action=check_dup&to_pdf=1&sugar_body_only=true&record_id=" + record_id1,
                data: data1,
                cache: false,
                async: true,
                success: function(result) {
                    if (result != '') {
                        res = eval('(' + result + ')');
                        //total_element--;
                        if (res.status == '') {
                            $("#" + class_info1).html('');
                            
                        } else {
							
							//alert(res.status);
                            ele_status[field_name1] = res.status;
                            var more_info_msg = '<img border="0"  id ="tooltip_' + field_name1 + '" onclick="return SUGAR.util.showHelpTips(this,ele_status['+field_name1+']);" src="themes/default/images/helpInline.gif" >';
                            $("#" + class_info1).html('<img src="custom/include/SugarFields/Fields/Dupdetector/image/error.gif" /> Khách hàng này đã tồn tại ! ' + more_info_msg);
                            SUGAR.util.evalScript(more_info_msg);
                            
                        }
                    }
                    //SUGAR.ajaxUI.hideLoadingPanel();
                   // if (total_element <= 0 && inside_submit == true) {
                    //    inside_submit = false;
                   //     $(curSubmit).trigger("click");
                   // }
                },
                failure: function() {
                   // SUGAR.ajaxUI.hideLoadingPanel();
                }
            });
			
			
					}
					else{

						$("#" + class_info1).html('');

					}						
					};
					
					//}
				
				});
				
				
			});
			// $('#creationModal').modal('show');
        });
    }, 300);
}
function GetURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}
/*
function getuser(){
  var user = $('#userextension').val();
  $.ajax({
    type:'post',
    url:"Api/getuser.php",
    data:{
     user : user ,
   },
   dataType :"json",
   success:function(response) {

    if(response !="" && response !="null" && response !=null ){
      document.getElementById("userextension").value =response.description;
      userext = true;
    }
    
  },error: function (xhr, ajaxOptions, thrownError) {

  }
});


}
function getOnCall(){
 // var user = document.getElementById("userextension").value;
  $.ajax({
    type:'post',
    url:"Api/GetOncall.php",
    data:{
     user : user 
   },
   dataType:"JSON",
   success:function(response) {
    if(response !="" && response !="null" && response !=null ){
     $("#myForm").css("display", "block");
     $("#tenkh").html(response.fullname);
     fullname = response.fullname;
     $("#phone-mobile-popup").html(response.customernumber);
     customernumber = response.customernumber;
     $("#userextension-popup").html(' <span class="glyphicon glyphicon-user"></span> '+response.userextension);
     $("#direction").html((response.direction == 'inbound') ? 'Cuộc gọi đến' : 'Cuộc gọi ra');
     $("#calluuid").val(response.calluuid);
     $("#editnote").val("");
     $("#titlenote").val((response.fullname == 'Không xác định') ? '' : 'Gọi điện cho '+ response.fullname);
     $("#actionButton").html(response.buton);
     $('#timestatus').val('run');
     flag_popup     = true;
     parent_type = response.datafrom;
     parent_id= response.idd;
     account_name = (response.account_name) ? response.account_name : '';
     account_id = (response.account_id) ? response.account_id : '';

     if(response.fullname == 'Không xác định'){
      switch (response.defaultpopup) {
        case 'Lead':
        getViewQuickCreateLeads(response.customernumber);
        break;
        case 'Account':
        getViewQuickCreateAccounts(response.customernumber);
        break;
        case 'Target':
        getViewQuickCreateTarget(response.customernumber);
        break;
      }
    }
    else if(response.href != undefined){
      var check_reload = window.localStorage.getItem("check_reload");
      if(check_reload != null){
        if(check_reload == response.idd){
          localStorage.clear();
        }else{
          window.location.href = response.href;
          localStorage.setItem("check_reload", response.idd);
        }
      }else{
       window.location.href = response.href;
       localStorage.setItem("check_reload", response.idd);
     }
   }


 }
}
});

}
*/

function clicktocall(srtphone,name,id,accountName,module='',statuss='',account_id=''){
	if(getCookie("username") != null && getCookie("username") != "" && getCookie("username") != undefined ){
		$.ajax({
		type:'post',
		url:"index.php?entryPoint=checkSession",
	    success:function(response) {
		   if(response.length < 50 && response != null && response != undefined && response != ''){
			sec=0;
			hr=0;
			min=0;
			$("#endCall").attr('onclick','handleButtonHangupClick()');
			 document.getElementById('input-phone').value=srtphone;
			//if(user != null && user != undefined && user != ''){
			//permission=getCookie("permission");
			//setCookie("hangup","No");
			//if(permission == 1 || permission == null || permission == undefined){
				//var top1 = window.innerHeight-height;
				//var left1 = window.innerWidth-width;
				//setCookie("permission",2);
				//window.open("https://demo.crmonline.vn/webrtc2",'abc', 'location=no,height=10,width=10,scrollbars=no,toolbar=no,menubar=no,status=no,top='+window.innerHeight+',left='+window.innerWidth);
				handleButtonCallClick();
				setCookie('phone',srtphone);
				setCookie("alreadyCall","");
				var html='';
					html+='<a type="button" id="quick_create_meeting" title="Quick Create Meeting" class="btn btn-primary action-btn1" ><i class="fa fa-users" aria-hidden="true"></i></a>';
					html+='<a type="button" id="quick_create_opportunities" title ="Quick Create Opportunities" class="btn btn-primary action-btn2"><i class="fa fa-usd" ></i></a>';
					html+='<a type="button" id="quick_create_case" title ="Quick Create Case" class="btn btn-primary action-btn2"><i class="fa fa-folder" aria-hidden="true"></i></a>';
					html+='<a type="button" style="width: auto" id="save_popup" title ="Lưu" class="btn btn-primary">Lưu thông tin</a>';
					html+='<a type="button" id="eCall" onclick="handleButtonHangupClick2()" style="width: auto;margin-right:3px;" id="Decline" title ="Lưu" class="btn btn-primary">Kết thúc</a>'
					html+='<a type="button" id="callTransfer" onclick="handleButtonTransferClick()" style="width: auto;display:none"  title ="chuyển tiếp" class="btn btn-primary">Chuyển tiếp</a>';
				account_name=accountName;
				
				var type1=document.getElementsByName("parent_type")[0];
				if(type1 !=null && type1 != undefined){
					type2=type1.value;
				}else{
					type2=GetURLParameter("module");
				}
				$("a").attr("target","_blank");
				$(".panel-body a").attr("target","_self");
				$(".panel-body span").attr("target","_self");
				$("span").attr("target","_blank");
				$('#detailModal').find('.modal-body').html('');
				$('#detailModal').modal('hide');
				stop=0;
				us_id=id;
			document.getElementById("time_count").innerHTML='<span id="hrs">00</span>:' + '<span id="mins">00</span>:' + '<span id="sec">00</span>';
			$("#direction").html("Cuộc gọi đi");
			$("#statuscall").html("");
			var label=document.getElementById("with-label");
			var accountLabel=label.getAttribute('title');
			var user = document.getElementById("userextension").value;
			setCookie("phoneNum",srtphone);
			$("#actionButton").html('');	
			$("#show").html("Đang gọi "+srtphone);
			$("#phoneForm").css('display', 'block');	
			$("#myForm").css("display", "block");
			$("#actionButton").html(html);
			$("#actionButton").css("display", "block");
				$("#phone-mobile-popup").html(srtphone);
				$("#userextension-popup").html('<span class="glyphicon glyphicon-user"></span>'+accountLabel);
				if(name!=null && name!=''){
				$("#tenkh").html(name);	
				}else{
				$("#tenkh").html(accountName);	
				}
	}
	}
	});	
   }
}

function callnote(_switch ='callnote'){
  var value;
  var type = _switch;
  if(_switch == 'callnote'){
    value = document.getElementById("editnote").value;
  }else{
   value = document.getElementById("titlenote").value;
 }
 var calluuid = $('#calluuid').val();
 $.ajax({
  type:'post',
  url:"Api/editnote.php",
  data:{
    value : value,
    calluuid : calluuid,
    type: type
  },
  success:function(response) {
  }
});
}

var totalSeconds = 0;
var totaMins = 0;

function setTime(){
  if($('#timestatus').val()=='run'){
    ++totalSeconds;
    if(totalSeconds > 60){
      totalSeconds = 0;
      totaMins++;
    }

    document.getElementById("sec").innerHTML =(totalSeconds<10) ? "0"+totalSeconds : totalSeconds;
    document.getElementById("mins").innerHTML =(totaMins<10) ? "0"+totaMins : totaMins;
  }
}


function secondsTimeSpanToHMS(s) {
  var h = Math.floor(s/3600); 
  s -= h*3600;
  var m = Math.floor(s/60); 
  s -= m*60;
  return h+":"+(m < 10 ? '0'+m : m)+":"+(s < 10 ? '0'+s : s); 
}


function checkCallComplete(){
  var user = document.getElementById("userextension").value;
  if($('#timestatus').val()=='run'){
    var calluuid = $('#calluuid').val();
    $.ajax({
      type: 'POST',
      url: "Api/checkCallComplete.php",
      dataType: "json",
      data:{
        calluuid : calluuid,
        user: user
      },
      success: function (data) { 

        if (data !="" && data !="null" && data !=null) {
          if(data.workstatus == 'Complete'){
            $('#timestatus').val('stop');
          }
          document.getElementById("statuscall").innerHTML = "("+data.workstatus+")";
        }
      }
    });
  } 
}
function testHangupCall() {
	module=GetURLParameter("module");
	if(module ==null || module == undefined){
		module=document.getElementsByClassName("recent-links-detail")[0].getAttribute("title");
	}
	if(($("#editnote").val()!= null && $("#editnote").val()!= '')){
		$.ajax({
		type: "POST",
		url: "index.php?entryPoint=saveUser",
		data: {
		id : us_id,
		//title: $("#titlenote").val(),
		content:$("#editnote").val(),
		module: module
		},
						
		success: function (response) {
		},
						
		});
	}
//	setTimeout(function(){
	//	getInfo();
//	}, 240000);
								
	$("a").attr("target","_self");
	$("span").attr("target","_self");
	//$("#titlenote").val('');
	$("#editnote").val('');
}
function PUonClose() {
	setCookie("hangup","Yes");
	setCookie("permission",1);
	setCookie("callcoming","No");
	setCookie("acceptC","No");
	 $("#myForm").css("display", "none");
	 var y=GetURLParameter("module");
	var z=GetURLParameter("action");
	if(y == "Home" && z == "index"){
	 SUGAR.ajaxUI.showLoadingPanel();
	setTimeout(function () {
        refreshLeadCard(function (res) {
            SUGAR.ajaxUI.hideLoadingPanel();
      });
   }, 300);
	}
	 testHangupCall();
}
function Close3(){
	 $("#myForm").css("display", "none");
	 var y=GetURLParameter("module");
	var z=GetURLParameter("action");
	if(y == "Home" && z == "index"){
	 SUGAR.ajaxUI.showLoadingPanel();
	setTimeout(function () {
        refreshLeadCard(function (res) {
            SUGAR.ajaxUI.hideLoadingPanel();
      });
   }, 300);
	}
}
function PUonClose2() {
	//setCookie("hangup","Yes");
	//setCookie("permission",1);
	//setCookie("acceptC","No");
	//setCookie("callcoming","No");
	testHangupCall();
}


function togglePopup(){
  $("#myForm").toggle();
  $('#hide_popup').toggle();
}
$(document).ready(function(){
  $('#hide_popup').click(function(){
    $("#myForm").toggle();
    $('#hide_popup').toggle();
  });

  $(document).on('click','#close_quick_create_view',function(){
   $('#quickCreateView').hide();
 });

  $(document).on('click','#call_quick_create_lead',function(){
    getViewQuickCreateLeads();
    $('#quickCreateView').show();
  });
  $(document).on('click','#call_quick_create_contact',function(){
    getViewQuickCreateAccounts(customernumber);
    $('#quickCreateView').show();
  });

  $(document).on('click','#call_quick_create_target',function(){
    getViewQuickCreateTarget();
    $('#quickCreateView').show();
  });

  $(document).on('click','#quick_create_meeting',function(){
    getViewQuickCreateOpportunitiesOrMeeting_Case('Meetings');
    $('#quickCreateView').show();
  });
  $(document).on('click','#quick_create_opportunities',function(){
    getViewQuickCreateOpportunities();
    $('#quickCreateView').show();
  });
  $(document).on('click','#quick_create_case',function(){
    getViewQuickCreateCase();
    $('#quickCreateView').show();
  });
  $(document).on('click','#save_popup', function(){
    PUonClose2();
  });
})

function getViewQuickCreateLeads(){ 
  $('#quickCreateView').show();
  $.ajax({
    type: 'POST',
    url: "index.php",
    async: false,
    dataType: "text",
    data:{
      target_module: "Leads",
      account_id: "",
      account_name: "",
      account_leads_name: "",
      to_pdf: true,
      tpl: "QuickCreate.tpl",
      return_module: "",
      return_action: "",
      return_id: "",
      return_relationship: "",
      record: "",
      action: "SubpanelCreates",
      module: "Home",
      target_action: "QuickCreate",
      primary_address_street: "",
      primary_address_city: "",
      primary_address_state: "",
      primary_address_country: "",
      primary_address_postalcode: "",
      phone_work: customernumber,
      account_id: "",
      return_name: "",
      parent_type: "",
      parent_name: "",
      parent_id: "",
      account_leads_create_button_button: "Create"
    },
    success: function (data) {  
      data= data.replace('var selectTab','var selectTab2');
      data= data.replace('var selectTabOnError','var selectTabOnError2');
      data= data.replace('var selectTabOnErrorInputHandle','var selectTabOnErrorInputHandle2');
      data = "<a id='close_quick_create_view' style='color: #7f7272; font-size: 14px; position: absolute; right: 13px; top: 5px; cursor: pointer;'><span class='glyphicon glyphicon-remove'></span></a>" + data;

      data = data + `<script> 
      SUGAR.subpanelUtils = function() {
        var originalLayout = null,
        subpanelContents = {},
        subpanelLocked = {},
        currentPanelDiv;
        return {

          inlineSave: function(theForm, buttonName) {
            createAction('#form_SubpanelQuickCreate_Leads');
            return false;
          },


        };
      }(); </script> `;
      $('#quickCreateView').html(data);
    },
    error: function(e){
      console.log(e);
    }
  });
}
function createAction(idForm){
  $.ajax({
    type: 'POST',
    url: "index.php",
    async: false,
    dataType: "text",
    data: $(idForm).serialize(),
    success: function (data) {
     $('#quickCreateView').hide();
     location.reload();
   }
 });
}
function saveOpportunityFromModal1(theForm)
{
	var us_id = localStorage.getItem("us_id");
	$("#account_id").val(us_id);
	if(theForm != "form_SubpanelQuickCreate_Opportunities")
	{
		var Call = document.getElementById('formformCalls');
		var _form = document.getElementById(theForm);
		disableOnUnloadEditView();
		_form.action.value='Save';
		if(check_form(theForm)) {
			$('#activityCreationModal').find('.spinner').show();
			setTimeout(function () {
				$.ajax({
					type: "POST",
					url: "index.php",
					data: $('#' + theForm).serialize(),
					success: function(res) {
						$('#activityCreationModal').modal('hide');
						if(Call != null || Call != undefined){
						  showSubPanel('history', null, true);
						  showSubPanel('activities', null, true);
						  showSubPanel_re('cases', null, true);
						  showSubPanel_re('opportunities', null, true);
						  }else{
						 
							SUGAR.ajaxUI.showLoadingPanel();
							setTimeout(function () {
							refreshLeadCard(function (res) {
							SUGAR.ajaxUI.hideLoadingPanel();
							});
							}, 300);

						}  
						
					},
					error: function(error) {
						alert(error.message);
					}
				});
			}, 300);
		}
	}else{
		if($('#sales_stage').val() == "Closed Won"){
			var Call = document.getElementById('formformCalls');
		var _form = document.getElementById(theForm);
		disableOnUnloadEditView();
		_form.action.value='Save';
		if(check_form(theForm)) {
				var aId=getCookie("aId");
				var type=$('#servicetype_c').val();
					$.ajax({
					type: "POST",
					url: "index.php?entryPoint=CheckAccount",
					data: {
						Id:aId,
						serviceType:type
					},
					success: function(res) {
						if(res != '' && res != null && res != undefined){
						alert(res);
						}else{
						$('#activityCreationModal').find('.spinner').show();
						setTimeout(function () {
						$.ajax({
							type: "POST",
							url: "index.php",
							data: $('#' + theForm).serialize(),
							success: function(res) {
									//
								$.ajax({
									type: "POST",
									url: "index.php?entryPoint=UpdateAccountCode",
									data: {
									Id:aId,
									serviceType:type
									}
								});
									//
							var u=GetURLParameter("module");
							$('#activityCreationModal').modal('hide');
							$('#quickCreateView').hide();
							if(u == "Home"){
							SUGAR.ajaxUI.showLoadingPanel();
							setTimeout(function () {
							refreshLeadCard(function (res) {
							SUGAR.ajaxUI.hideLoadingPanel();
							});
							}, 300);
							}
								
					},
					error: function(error) {
						alert(error.message);
					}
				});
			}, 300);	
							
						}
					},
					error: function(error) {
						alert(error.message);
					}
				});	
			
		}
			
		}else{
			
			var Call = document.getElementById('formformCalls');
		var _form = document.getElementById(theForm);
		disableOnUnloadEditView();
		_form.action.value='Save';
		if(check_form(theForm)) {
			$('#activityCreationModal').find('.spinner').show();
			setTimeout(function () {
				$.ajax({
					type: "POST",
					url: "index.php",
					data: $('#' + theForm).serialize(),
					success: function(res) {
						var u=GetURLParameter("module");
						$('#activityCreationModal').modal('hide');
						$('#quickCreateView').hide();
						  if(u == "Home"){
						SUGAR.ajaxUI.showLoadingPanel();
						setTimeout(function () {
						refreshLeadCard(function (res) {
						SUGAR.ajaxUI.hideLoadingPanel();
						});
						}, 300);
						  }
					},
					error: function(error) {
						alert(error.message);
					}
				});
			}, 300);
		}
			
			
			
			
		}
		
		
	}
	
}
function getViewQuickCreateAccounts(customernumber){
  $('#quickCreateView').show();
  $.ajax({
    type: 'POST',
    url: "index.php",
    async: false,
    dataType: "text",
    data:{
      target_module: "Accounts",
      account_id: "",
      account_name: "",
      account_leads_name: "",
      to_pdf: true,
      tpl: "QuickCreate.tpl",
      return_module: "",
      return_action: "",
      return_id: "",
      return_relationship: "",
      record: "",
      action: "SubpanelCreates",
      module: "Home",
      target_action: "QuickCreate",
      primary_address_street: "",
      primary_address_city: "",
      primary_address_state: "",
      primary_address_country: "",
      primary_address_postalcode: "",
      phone_work: customernumber,
      account_id: "",
      return_name: "",
      parent_type: "",
      parent_name: "",
      parent_id: "",
      account_leads_create_button_button: "Create"
    },
    success: function (data) {
      data= data.replace('var selectTab','var selectTab2');
      data= data.replace('var selectTabOnError','var selectTabOnError2');
      data= data.replace('var selectTabOnErrorInputHandle','var selectTabOnErrorInputHandle2');
      data = "<a id='close_quick_create_view' style='color: #7f7272; font-size: 14px; position: absolute; right: 13px; top: 5px; cursor: pointer;'><span class='glyphicon glyphicon-remove'></span></a>" + data;

      data = data + `<script> 
      SUGAR.subpanelUtils = function() {
        var originalLayout = null,
        subpanelContents = {},
        subpanelLocked = {},
        currentPanelDiv;
        return {
          inlineSave: function(theForm, buttonName) {
            createAction('#form_SubpanelQuickCreate_Accounts');
            return false;

          },

        };
      }(); </script> `;
      $('#quickCreateView').html(data);
      $("#phone_office").val(customernumber);
    },
    error: function(e){
      console.log(e);
    }
  });
}

function getViewQuickCreateTarget(customernumber){
  $('#quickCreateView').show();
  $.ajax({
    type: 'POST',
    url: "index.php",
    async: false,
    dataType: "text",
    data:{
      target_module: "Prospects",
      account_id: "",
      account_name: "",
      account_leads_name: "",
      to_pdf: true,
      tpl: "QuickCreate.tpl",
      return_module: "",
      return_action: "",
      return_id: "",
      return_relationship: "",
      record: "",
      action: "SubpanelCreates",
      module: "Home",
      target_action: "QuickCreate",
      primary_address_street: "",
      primary_address_city: "",
      primary_address_state: "",
      primary_address_country: "",
      primary_address_postalcode: "",
      phone_work: customernumber,
      account_id: "",
      return_name: "",
      parent_type: "",
      parent_name: "",
      parent_id: "",
      account_leads_create_button_button: "Create"
    },
    success: function (data) { 
      data= data.replace('var selectTab','var selectTab2');
      data= data.replace('var selectTabOnError','var selectTabOnError2');
      data= data.replace('var selectTabOnErrorInputHandle','var selectTabOnErrorInputHandle2');
      data = "<a id='close_quick_create_view' style='color: #7f7272; font-size: 14px; position: absolute; right: 13px; top: 5px; cursor: pointer;'><span class='glyphicon glyphicon-remove'></span></a>" + data;

      data = data + `<script> 
      SUGAR.subpanelUtils = function() {
        var originalLayout = null,
        subpanelContents = {},
        subpanelLocked = {},
        currentPanelDiv;
        return {
          inlineSave: function(theForm, buttonName) {
            createAction('#form_SubpanelQuickCreate_Prospects');
            return false;
          },
        };
      }(); </script> `;

      $('#quickCreateView').html(data);
    },
    error: function(e){
      console.log(e);
    }
  });
}
function createMeeting(){
  $.ajax({
    type: 'POST',
    url: "index.php",
    async: false,
    dataType: "text",
    data: $('#form_SubpanelQuickCreate_Meetings').serialize(),
    success: function (data) {
     $('#quickCreateView').hide();
     location.reload();
   }
 });
}
function createOpportunities(){
  $.ajax({
    type: 'POST',
    url: "index.php",
    async: false,
    dataType: "text",
    data: $('#form_SubpanelQuickCreate_Opportunities').serialize(),
    success: function (data) {
     $('#quickCreateView').hide();
     // location.reload();
   }
 });
}
function getViewQuickCreateOpportunities(){
	//var formOpp=document.getElementById("formformaccounts_opportunities");
	//var returnId=formOpp.querySelector("input[name='return_id']");
	//returnId.value="c0211d7b-7361-33f0-1ba9-5f506bb79970";
  $.ajax({
    type: 'POST',
    url: "index.php",
    async: false,
    dataType: "text",
    data:{
		target_module:"Opportunities",
		account_id:us_id,
		account_name:account_name,
		accounts_opportunities_name:account_name,
		to_pdf:"true",
		tpl:"QuickCreate.tpl",
		return_module:"Accounts",
		return_action:"SubPanelViewer",
		return_id:us_id,
		return_relationship:"accounts_opportunities",
		record:"",
		action:"SubpanelCreates",
		module:"Home",
		target_action:"QuickCreate",
		return_name:account_name,
		parent_type:"Accounts",
		parent_name:account_name,
		parent_id:us_id
	},
    success: function (data) {
      data= data.replace('var selectTab','var selectTab2');
      data= data.replace('var selectTabOnError','var selectTabOnError2');
      data= data.replace('var selectTabOnErrorInputHandle','var selectTabOnErrorInputHandle2');
      data = "<a id='close_quick_create_view' style='color: #7f7272; font-size: 14px; position: absolute; right: 13px; top: 5px; cursor: pointer;'><span class='glyphicon glyphicon-remove'></span></a>" + data;
     
	 data = data + `<script> 
      var oppSub=document.getElementsByName('Opportunities_subpanel_save_button');
		oppSub[1].getAttribute("onclick");
		oppSub[1].removeAttribute('onclick');
		oppSub[1].setAttribute('type', 'button');
		oppSub[1].setAttribute('onclick', 'saveOpportunityFromModal1("form_SubpanelQuickCreate_Opportunities")');
		changeCur('amount');
		</script> `;
	 
        $('#quickCreateView').html(data);
        $('#quickCreateView').show();
		/*
		$(document).ready(function(){
		var oppSub=document.getElementsByName('Opportunities_subpanel_save_button');
		oppSub[1].getAttribute("onclick");
		oppSub[1].removeAttribute('onclick');
		oppSub[1].setAttribute('type', 'button');
		oppSub[1].setAttribute('onclick', 'saveOpportunityFromModal1("form_SubpanelQuickCreate_Opportunities")');
		});
		*/
    }
  });
}
function getViewQuickCreateCase(){
  $.ajax({
    type: 'POST',
    url: "index.php",
    async: false,
    dataType: "text",
    data:{
      target_module:"Cases",
	  account_id:us_id,
	  account_name:account_name,
	  to_pdf:true,
	  tpl:'QuickCreate.tpl',
	  return_module:'Accounts',
	  return_action:'SubPanelViewer',
	  return_id:us_id,
	  record:"",
	  action:'SubpanelCreates',
	  module:'Home',
	  target_action:'QuickCreate',
	  parent_type:"Accounts",
	  parent_name:account_name,
	  parent_id:us_id
    },
    success: function (data) {
      data= data.replace('var selectTab','var selectTab2');
      data= data.replace('var selectTabOnError','var selectTabOnError2');
      data= data.replace('var selectTabOnErrorInputHandle','var selectTabOnErrorInputHandle2');
      data = "<a id='close_quick_create_view' style='color: #7f7272; font-size: 14px; position: absolute; right: 13px; top: 5px; cursor: pointer;'><span class='glyphicon glyphicon-remove'></span></a>" + data;
      data = data + `<script> 
		var caseSub=document.getElementsByName('Cases_subpanel_save_button');
		caseSub[1].getAttribute("onclick");
		caseSub[1].removeAttribute('onclick');
		caseSub[1].setAttribute('type', 'button');
		caseSub[1].setAttribute('onclick', 'saveActivityFromModal1("form_SubpanelQuickCreate_Cases")');
       </script> `;
      $('#quickCreateView').html(data);
      $('#quickCreateView').show();
    }
  })
}
function URLToArray(url) {
    var request = {};
    var pairs = url.substring(url.indexOf('?') + 1).split('&');
    for (var i = 0; i < pairs.length; i++) {
        if(!pairs[i])
            continue;
        var pair = pairs[i].split('=');
        request[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
     }
     return request;
}
function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}
function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
function eraseCookie(name) {   
    document.cookie = 'name='+name +'; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}
function clear(){
	setCookie("token",'');
	setCookie("branch",'');
	
}

function saveActivityFromModal1(theForm)
{
	if(theForm == "form_SubpanelQuickCreate_Cases"){
		$("#account_id").val(us_id);
	}
	var Call = document.getElementById('formformCalls');
    var _form = document.getElementById(theForm);
     disableOnUnloadEditView();
	 console.log($('#' + theForm).serialize());
      _form.action.value='Save';
      if(check_form(theForm)) {
          $('#activityCreationModal').find('.spinner').show();
          setTimeout(function () {
              $.ajax({
                  type: "POST",
                  url: "index.php",
                  data: $('#' + theForm).serialize(),
                  success: function(res) {
                      $('#activityCreationModal').modal('hide');
					  if(Call != null || Call != undefined){
                      showSubPanel('history', null, true);
                      showSubPanel('activities', null, true);
					  showSubPanel_re('cases', null, true);
					  showSubPanel_re('opportunities', null, true);
					  showSubPanel_re('contacts', null, true);
					  }else{
						  var u=GetURLParameter("module");
						  $('#quickCreateView').hide();
						  if(u == "Home"){
						SUGAR.ajaxUI.showLoadingPanel();
						setTimeout(function () {
						refreshLeadCard(function (res) {
						SUGAR.ajaxUI.hideLoadingPanel();
						});
						}, 300);
						  }
					}  
		  
                  },
                  error: function(error) {
                      alert(error.message);
                  }
              });
          }, 300);
		  
      }
}
function accept(){
	setCookie("acceptC","Yes");
	setCookie("callcoming","No");
	document.getElementById("Accept").style.display="none";
}

function getViewQuickCreateOpportunitiesOrMeeting_Case(param){
	var us_id = localStorage.getItem("us_id");
  $.ajax({
    type: 'POST',
    url: "index.php",
    async: false,
    dataType: "text",
    data:{
      target_module: param,
      account_id: us_id,
      account_name: account_name,
      account_leads_name: "",
      to_pdf: true,
      tpl: "QuickCreate.tpl",
      return_module: type2,
      return_action: "SubPanelViewer",
      return_id: us_id,
      return_relationship: "",
      record: null,
      action: "SubpanelCreates",
      module: "Home",
      target_action: "QuickCreate",
      primary_address_street: "",
      primary_address_city: "",
      primary_address_state: "",
      primary_address_country: "",
      primary_address_postalcode: "",
      phone_work: customernumber,
      account_id: "",
      return_name: "",
      parent_type: type2,
      parent_name: account_name,
      parent_id: "",
      account_leads_create_button_button: "Create"
    },
    success: function (data) {
      data= data.replace('var selectTab','var selectTab2');
      data= data.replace('var selectTabOnError','var selectTabOnError2');
      data= data.replace('var selectTabOnErrorInputHandle','var selectTabOnErrorInputHandle2');
      data = "<a id='close_quick_create_view' style='color: #7f7272; font-size: 14px; position: absolute; right: 13px; top: 5px; cursor: pointer;'><span class='glyphicon glyphicon-remove'></span></a>" + data;
      if(param == "Meetings"){
        data = data;
      }else if(param == "Opportunities"){
        data = data + `<script> 
        SUGAR.subpanelUtils = function() {
          return {

            inlineSave: function(theForm, buttonName) {
              createOpportunities();
              return false;
              $('#quickCreateView').hide();
            },


          };
        }(); </script> `;
      }else{
        data = data + `<script> 
        SUGAR.subpanelUtils = function() {
          return {

            inlineSave: function(theForm, buttonName) {

            },
          };
        }(); </script> `;
      }

      $('#quickCreateView').html(data);
      if(param == "Meetings"){
        $("#parent_type").val(type2);
        $("#parent_id").val('');
		$("#parent_name").val(account_name);
		 //if(param == "Meetings"){
	var meetSub=document.getElementsByName('Meetings_subpanel_save_button');
		MeetSubOn=meetSub[1].getAttribute("onclick");
		meetSub[1].removeAttribute('onclick');
		meetSub[1].removeAttribute('type');
		meetSub[1].setAttribute('type', 'button');
		meetSub[1].setAttribute('onclick', 'saveActivityFromModal1("form_SubpanelQuickCreate_Meetings")');
		
		/*
	$("[name='Meetings_subpanel_save_button']").removeAttr('onclick');
	$("[name='Meetings_subpanel_save_button']").removeAttr('type');
	$("[name='Meetings_subpanel_save_button']").attr('type', 'button');
	$("[name='Meetings_subpanel_save_button']").attr('onclick', 'saveActivityFromModal("form_SubpanelQuickCreate_Meetings")');
	*/
	// }
      }
	  /*
	  else if{console.log(account_name);
		oppSub=document.getElementById('quickCreateView');
		oppSubOn=oppSub.getElementById("Opportunities_subpanel_save_button");
		oppSubOnButt.removeAttribute('onclick');
		oppSubOnButt.removeAttribute('type');
		oppSubOnButt.setAttribute('type', 'button');
		oppSubOnButt.setAttribute('onclick', 'saveOpportunityFromModal("form_SubpanelQuickCreate_Opportunities")');
       $("#account_name").val(account_name);
       $("#account_id").val(account_id);
     }
	 */
     $('#quickCreateView').show();
	 $(document).ready(function(){
		 
			//setCookie("permission",1);
	 });
   },
   error: function(e){
    console.log(e);
  }
});
}

function getViewQuickCreateOpportunitiesOrMeeting_Case2(param,module,id,name){
  $.ajax({
    type: 'POST',
    url: "index.php",
    async: false,
    dataType: "text",
    data:{
      target_module: param,
      account_id: id,
      account_name: name,
      account_leads_name: "",
      to_pdf: true,
      tpl: "QuickCreate.tpl",
      return_module: module,
      return_action: "SubPanelViewer",
      return_id: id,
      return_relationship: "",
      record: null,
      action: "SubpanelCreates",
      module: "Home",
      target_action: "QuickCreate",
      primary_address_street: "",
      primary_address_city: "",
      primary_address_state: "",
      primary_address_country: "",
      primary_address_postalcode: "",
      phone_work: customernumber,
      account_id: "",
      return_name: "",
      parent_type: module,
      parent_name: name,
      parent_id: "",
      account_leads_create_button_button: "Create"
    },
    success: function (data) {
      data= data.replace('var selectTab','var selectTab2');
      data= data.replace('var selectTabOnError','var selectTabOnError2');
      data= data.replace('var selectTabOnErrorInputHandle','var selectTabOnErrorInputHandle2');
      data = "<a id='close_quick_create_view' style='color: #7f7272; font-size: 14px; position: absolute; right: 13px; top: 5px; cursor: pointer;'><span class='glyphicon glyphicon-remove'></span></a>" + data;
      if(param == "Meetings"){
        data = data;
      }else if(param == "Opportunities"){
        data = data + `<script> 
        SUGAR.subpanelUtils = function() {
          return {

            inlineSave: function(theForm, buttonName) {
              createOpportunities();
              return false;
              $('#quickCreateView').hide();
            },


          };
        }(); </script> `;
      }else{
        data = data + `<script> 
        SUGAR.subpanelUtils = function() {
          return {

            inlineSave: function(theForm, buttonName) {

            },
          };
        }(); </script> `;
      }

      $('#quickCreateView').html(data);
      if(param == "Meetings"){
        $("#parent_type").val(type2);
        $("#parent_id").val('');
		$("#parent_name").val(account_name);
		 //if(param == "Meetings"){
	var meetSub=document.getElementsByName('Meetings_subpanel_save_button');
		MeetSubOn=meetSub[1].getAttribute("onclick");
		meetSub[1].removeAttribute('onclick');
		meetSub[1].removeAttribute('type');
		meetSub[1].setAttribute('type', 'button');
		meetSub[1].setAttribute('onclick', 'saveActivityFromModal1("form_SubpanelQuickCreate_Meetings")');
		
		/*
	$("[name='Meetings_subpanel_save_button']").removeAttr('onclick');
	$("[name='Meetings_subpanel_save_button']").removeAttr('type');
	$("[name='Meetings_subpanel_save_button']").attr('type', 'button');
	$("[name='Meetings_subpanel_save_button']").attr('onclick', 'saveActivityFromModal("form_SubpanelQuickCreate_Meetings")');
	*/
	// }
      }else{console.log(account_name);
       $("#account_name").val(account_name);
       $("#account_id").val(account_id);
     }
     $('#quickCreateView').show();
	 $(document).ready(function(){
		 
			//setCookie("permission",1);
	 });
   },
   error: function(e){
    console.log(e);
  }
});
}

var x = GetURLParameter("action");
if(x=="Login"){
	clear();
	setCookie("clear","Yes");
	setCookie("alreadyGetToken","0");
	setCookie("username",'');
	setCookie("pass",'');
}
var alreadyGetToken=getCookie("alreadyGetToken");
if(x != "Login"){
	if(alreadyGetToken == "0"){
		$.ajax({
			type: "POST",
			url: "index.php?entryPoint=getToken",
			success: function(result) {
				setCookie("alreadyGetToken","1");
				if(result != null && result != undefined && result != ''){
					var inf=JSON.parse(result);
					setCookie("username",inf.username);
					setCookie("pass",inf.pass);
				}else{
					setCookie("username","");
					setCookie("pass","");	
				}
			},
			failure: function() {
			}
		});
	}
}	
			
			

			

			
			
			
			
			
			
	
	