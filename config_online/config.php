<?php
// created: 2024-04-04 07:03:55
$sugar_config = array (
  'addAjaxBannedModules' => 
  array (
    0 => 'login_LoginAudit',
    1 => 'la_LoginAudit',
    2 => 'AOS_Warehouse_Transfer',
    3 => 'BI_Report',
    4 => 'login_la_LoginAudit',
    5 => 'AOS_Inventoryinput',
    6 => 'AOS_Inventoryoutput',
    7 => 'AOS_pos',
    8 => 'AOS_Stock_Inventory',
    9 => 'AOS_Products_Pos',
    10 => 'AOS_Stock_Inventory_Detail',
    11 => 'lp_Language_Mobile_App',
    12 => 'rp_Customer_Not_Transaction',
    13 => 'AOS_Pos_Line_Items_Detail',
    14 => 'AOS_Size',
    15 => 'rp_Profitsdebtbyopportunities',
    16 => 'rp_Profitsdebtbyinvoice',
    17 => 'rp_Profitsdebtbycustomer',
    18 => 'rp_Profitsdebtbycustomer_',
    19 => 'rp_filterRevenueByInvoice',
    20 => 'rp_filterCustomerRevenue',
    21 => 'rp_CustomReport',
    22 => 'rp_ReportActivitiesDetail',
    23 => 'mba_MobileAppControl',
    24 => 'lm_LeaveManagerment',
    25 => 'spa_ActionService',
    26 => 'pr_payroll',
    27 => 'pr_PayrollParam',
    28 => 'pr_PayrollFile',
    29 => 'rp_ReportKPIDetail',
    30 => 'cs_cusUser',
    31 => 'bill_Billing',
    32 => 'Repor_ReportSummarDetail',
    33 => 'RD_ReportDetail',
    34 => 'SecurityGroups',
    35 => 'jckl_DashboardTemplates',
    36 => 'po_PO',
    37 => 'tttt_WorldfonePBX',
    38 => 'MTS_Country',
    39 => 'tttt_History_calls',
    40 => 'Meetings',
    41 => 'MTS_State',
    42 => 'AOR_Reports',
    43 => 'MTS_City',
    44 => 'Tasks',
    45 => 'MTS_KPIConfig',
    46 => 'Prospects',
    47 => 'MTS_Industry',
    48 => 'jjwg_Markers',
    49 => 'jjwg_Maps',
    50 => 'jjwg_Areas',
    51 => 'AOS_Quotes',
    52 => 'jjwg_Address_Cache',
    53 => 'AOW_WorkFlow',
    54 => 'AOS_Contracts',
    55 => 'AOS_Products',
    56 => 'AOS_Invoices',
    57 => 'AOS_PDF_Templates',
    58 => 'AOS_Product_Categories',
    59 => 'AOR_Scheduled_Reports',
    60 => 'FP_Event_Locations',
    61 => 'AOBH_BusinessHours',
    62 => 'FP_events',
    63 => 'ResourceCalendar',
    64 => 'AM_ProjectTemplates',
    65 => 'Spots',
    66 => 'AOK_KnowledgeBase',
    67 => 'AOK_Knowledge_Base_Categories',
    68 => 'Accounts',
    69 => 'Leads',
    70 => 'Bugs',
    71 => 'ProspectLists',
    72 => 'Cases',
    73 => 'Calls',
    74 => 'Opportunities',
    75 => 'Contacts',
    76 => 'Notes',
    77 => 'C_SLLTool',
    78 => 'Home',
    79 => 'rls_Dashboards',
    80 => 'rls_Reports',
    81 => 'RLS_Scheduling_Reports',
  ),
  'admin_access_control' => false,
  'admin_export_only' => false,
  'aod' => 
  array (
    'enable_aod' => false,
  ),
  'aop' => 
  array (
    'distribution_method' => 'roundRobin',
    'case_closure_email_template_id' => 'a7cd1a3d-c06f-792f-61ea-5cc688e2d675',
    'joomla_account_creation_email_template_id' => 'b0db0912-aa7c-92f8-97ce-5cc688efa9a5',
    'case_creation_email_template_id' => 'c17da93c-1656-e2a2-cd01-5cc688856876',
    'contact_email_template_id' => 'ce8563d2-57d3-d9b5-3f0a-5cc688ed32c8',
    'user_email_template_id' => 'd809f605-02e8-e725-c648-5cc6881ddbc3',
  ),
  'aos' => 
  array (
    'version' => '5.3.3',
    'contracts' => 
    array (
      'renewalReminderPeriod' => '14',
    ),
    'lineItems' => 
    array (
      'totalTax' => false,
      'enableGroups' => true,
    ),
    'invoices' => 
    array (
      'initialNumber' => '1',
    ),
    'quotes' => 
    array (
      'initialNumber' => '1',
    ),
  ),
  'asterisk_host' => '',
  'asterisk_port' => '',
  'cache_dir' => 'cache/',
  'calculate_response_time' => true,
  'calendar' => 
  array (
    'default_view' => 'week',
    'show_calls_by_default' => true,
    'show_tasks_by_default' => true,
    'show_completed_by_default' => true,
    'editview_width' => 990,
    'editview_height' => 485,
    'day_timestep' => 15,
    'week_timestep' => 30,
    'items_draggable' => true,
    'items_resizable' => true,
    'enable_repeat' => true,
    'max_repeat_count' => 1000,
  ),
  'ce_sms_type' => 'Plivo',
  'chartEngine' => 'Jit',
  'com_percent' => 100,
  0 => 
  array (
    'wsdl' => 'https://api-02.worldsms.vn/webapi/sendSMS',
    'user_name' => 'longphatdemo',
    'authentication_key' => '',
    'brand_name' => 'VienThongMN',
  ),
  'common_ml_dir' => '',
  'create_default_user' => false,
  'cron' => 
  array (
    'max_cron_jobs' => 10,
    'max_cron_runtime' => 30,
    'min_cron_interval' => 30,
    'allowed_cron_users' => 
    array (
      0 => 'root',
      1 => 'admin',
    ),
  ),
  'currency' => '',
  'dashlet_auto_refresh_min' => '30',
  'dashlet_display_row_options' => 
  array (
    0 => '1',
    1 => '3',
    2 => '5',
    3 => '10',
  ),
  'date_formats' => 
  array (
    'Y-m-d' => '2010-12-23',
    'm-d-Y' => '12-23-2010',
    'd-m-Y' => '23-12-2010',
    'Y/m/d' => '2010/12/23',
    'm/d/Y' => '12/23/2010',
    'd/m/Y' => '23/12/2010',
    'Y.m.d' => '2010.12.23',
    'd.m.Y' => '23.12.2010',
    'm.d.Y' => '12.23.2010',
  ),
  'datef' => 'd/m/Y',
  'dbconfig' => 
  array (
    'db_host_name' => '103.98.152.78',
    'db_host_instance' => 'SQLEXPRESS',
    'db_user_name' => 'admin_dev_lp',
    'db_password' => 'Ah0lSwRNzT',
    'db_name' => 'admin_dev_lp',
    'db_type' => 'mysql',
    'db_port' => '',
    'db_manager' => 'MysqliManager',
  ),
  'dbconfigoption' => 
  array (
    'persistent' => false,
    'autofree' => true,
    'debug' => 0,
    'ssl' => false,
    'collation' => 'utf8_general_ci',
  ),
  'default_action' => 'index',
  'default_charset' => 'UTF-8',
  'default_currencies' => 
  array (
    'AUD' => 
    array (
      'name' => 'Australian Dollars',
      'iso4217' => 'AUD',
      'symbol' => '$',
    ),
    'BRL' => 
    array (
      'name' => 'Brazilian Reais',
      'iso4217' => 'BRL',
      'symbol' => 'R$',
    ),
    'GBP' => 
    array (
      'name' => 'British Pounds',
      'iso4217' => 'GBP',
      'symbol' => '£',
    ),
    'CAD' => 
    array (
      'name' => 'Canadian Dollars',
      'iso4217' => 'CAD',
      'symbol' => '$',
    ),
    'CNY' => 
    array (
      'name' => 'Chinese Yuan',
      'iso4217' => 'CNY',
      'symbol' => '￥',
    ),
    'EUR' => 
    array (
      'name' => 'Euro',
      'iso4217' => 'EUR',
      'symbol' => '€',
    ),
    'HKD' => 
    array (
      'name' => 'Hong Kong Dollars',
      'iso4217' => 'HKD',
      'symbol' => '$',
    ),
    'INR' => 
    array (
      'name' => 'Indian Rupees',
      'iso4217' => 'INR',
      'symbol' => '₨',
    ),
    'KRW' => 
    array (
      'name' => 'Korean Won',
      'iso4217' => 'KRW',
      'symbol' => '₩',
    ),
    'YEN' => 
    array (
      'name' => 'Japanese Yen',
      'iso4217' => 'JPY',
      'symbol' => '¥',
    ),
    'MXN' => 
    array (
      'name' => 'Mexican Pesos',
      'iso4217' => 'MXN',
      'symbol' => '$',
    ),
    'SGD' => 
    array (
      'name' => 'Singaporean Dollars',
      'iso4217' => 'SGD',
      'symbol' => '$',
    ),
    'CHF' => 
    array (
      'name' => 'Swiss Franc',
      'iso4217' => 'CHF',
      'symbol' => 'SFr.',
    ),
    'THB' => 
    array (
      'name' => 'Thai Baht',
      'iso4217' => 'THB',
      'symbol' => '฿',
    ),
    'USD' => 
    array (
      'name' => 'US Dollars',
      'iso4217' => 'USD',
      'symbol' => '$',
    ),
  ),
  'default_currency_iso4217' => 'VND',
  'default_currency_name' => 'VNĐ',
  'default_currency_significant_digits' => 2,
  'default_currency_symbol' => 'đ',
  'default_date_format' => 'd/m/Y',
  'default_decimal_seperator' => '.',
  'default_email_charset' => 'UTF-8',
  'default_email_client' => 'sugar',
  'default_email_editor' => 'html',
  'default_export_charset' => 'UTF-8',
  'default_language' => 'Vietnamese',
  'default_locale_name_format' => 's f l',
  'default_max_tabs' => 8,
  'default_module' => 'Home',
  'default_module_favicon' => true,
  'default_navigation_paradigm' => 'gm',
  'default_number_grouping_seperator' => ',',
  'default_password' => '',
  'default_permissions' => 
  array (
    'dir_mode' => 1528,
    'file_mode' => 493,
    'user' => '',
    'group' => '',
  ),
  'default_subpanel_links' => false,
  'default_subpanel_tabs' => true,
  'default_swap_last_viewed' => false,
  'default_swap_shortcuts' => false,
  'default_theme' => 'SuiteP',
  'default_time_format' => 'H:i',
  'default_user_is_admin' => false,
  'default_user_name' => '',
  'defaultpopup' => 'Lead',
  'demoData' => 'no',
  'developerMode' => false,
  'disable_convert_lead' => true,
  'disable_export' => false,
  'disable_persistent_connections' => false,
  'display_email_template_variable_chooser' => false,
  'display_inbound_email_buttons' => false,
  'dump_slow_queries' => false,
  'email_address_separator' => ',',
  'email_allow_send_as_user' => true,
  'email_confirm_opt_in_email_template_id' => '36b435eb-2882-0bf3-8147-5cc6889e8a98',
  'email_default_client' => 'sugar',
  'email_default_delete_attachments' => false,
  'email_default_editor' => 'html',
  'email_enable_auto_send_opt_in' => false,
  'email_enable_confirm_opt_in' => 'not-opt-in',
  'email_xss' => 'YToxMzp7czo2OiJhcHBsZXQiO3M6NjoiYXBwbGV0IjtzOjQ6ImJhc2UiO3M6NDoiYmFzZSI7czo1OiJlbWJlZCI7czo1OiJlbWJlZCI7czo0OiJmb3JtIjtzOjQ6ImZvcm0iO3M6NToiZnJhbWUiO3M6NToiZnJhbWUiO3M6ODoiZnJhbWVzZXQiO3M6ODoiZnJhbWVzZXQiO3M6NjoiaWZyYW1lIjtzOjY6ImlmcmFtZSI7czo2OiJpbXBvcnQiO3M6ODoiXD9pbXBvcnQiO3M6NToibGF5ZXIiO3M6NToibGF5ZXIiO3M6NDoibGluayI7czo0OiJsaW5rIjtzOjY6Im9iamVjdCI7czo2OiJvYmplY3QiO3M6MzoieG1wIjtzOjM6InhtcCI7czo2OiJzY3JpcHQiO3M6Njoic2NyaXB0Ijt9',
  'enable_action_menu' => true,
  'enable_line_editing_detail' => true,
  'enable_line_editing_list' => false,
  'export_delimiter' => ',',
  'export_excel_compatible' => false,
  'filter_module_fields' => 
  array (
    'Users' => 
    array (
      0 => 'show_on_employees',
      1 => 'portal_only',
      2 => 'is_group',
      3 => 'system_generated_password',
      4 => 'external_auth_only',
      5 => 'sugar_login',
      6 => 'authenticate_id',
      7 => 'pwd_last_changed',
      8 => 'is_admin',
      9 => 'user_name',
      10 => 'user_hash',
      11 => 'password',
      12 => 'last_login',
      13 => 'oauth_tokens',
    ),
    'Employees' => 
    array (
      0 => 'show_on_employees',
      1 => 'portal_only',
      2 => 'is_group',
      3 => 'system_generated_password',
      4 => 'external_auth_only',
      5 => 'sugar_login',
      6 => 'authenticate_id',
      7 => 'pwd_last_changed',
      8 => 'is_admin',
      9 => 'user_name',
      10 => 'user_hash',
      11 => 'password',
      12 => 'last_login',
      13 => 'oauth_tokens',
    ),
  ),
  'google_auth_json' => '',
  'hide_subpanels' => true,
  'history_max_viewed' => 50,
  'host_name' => '103.98.152.78',
  'imap_test' => false,
  'import_max_execution_time' => 3600,
  'import_max_records_per_file' => 100,
  'import_max_records_total_limit' => '',
  'installer_locked' => true,
  'jobs' => 
  array (
    'min_retry_interval' => 30,
    'max_retries' => 5,
    'timeout' => 86400,
  ),
  'js_custom_version' => 1,
  'js_lang_version' => 263,
  'languages' => 
  array (
    'vi_VN' => 'Vietnamese',
    'en_us' => 'English (US)',
  ),
  'large_scale_test' => false,
  'lead_conv_activity_opt' => 'move',
  'list_max_entries_per_page' => '20',
  'list_max_entries_per_subpanel' => 10,
  'lock_default_user_name' => false,
  'lock_homepage' => false,
  'lock_subpanels' => false,
  'log_dir' => '.',
  'log_file' => 'suitecrm.log',
  'log_memory_usage' => false,
  'logger' => 
  array (
    'level' => 'fatal',
    'file' => 
    array (
      'ext' => '.log',
      'name' => 'suitecrm',
      'dateFormat' => '%c',
      'maxSize' => '10MB',
      'maxLogs' => 10,
      'suffix' => '',
    ),
  ),
  'max_dashlets_homepage' => '15',
  'name_formats' => 
  array (
    's f l' => 's f l',
    'f l' => 'f l',
    's l' => 's l',
    'l, s f' => 'l, s f',
    'l, f' => 'l, f',
    's l, f' => 's l, f',
    'l s f' => 'l s f',
    'l f s' => 'l f s',
  ),
  'passwordsetting' => 
  array (
    'SystemGeneratedPasswordON' => '',
    'generatepasswordtmpl' => 'e0617f59-4c2b-196f-0102-5cc688e447c9',
    'lostpasswordtmpl' => 'e97ff3b8-d584-a19c-09eb-5cc688cd2895',
    'factoremailtmpl' => 'f28676ce-3b32-9ad7-a0db-5cc6886d0c52',
    'forgotpasswordON' => false,
    'linkexpiration' => '1',
    'linkexpirationtime' => '30',
    'linkexpirationtype' => '1',
    'systexpiration' => '1',
    'systexpirationtime' => '7',
    'systexpirationtype' => '1',
    'systexpirationlogin' => '',
  ),
  'portal_view' => 'single_user',
  'require_accounts' => true,
  'resource_management' => 
  array (
    'special_query_limit' => 50000000,
    'special_query_modules' => 
    array (
      0 => 'Reports',
      1 => 'Export',
      2 => 'Import',
      3 => 'Administration',
      4 => 'Sync',
    ),
    'default_limit' => 1000000,
  ),
  'rss_cache_time' => '10800',
  'save_query' => 'all',
  'search' => 
  array (
    'controller' => 'UnifiedSearch',
    'defaultEngine' => 'BasicSearchEngine',
    'pagination' => 
    array (
      'min' => 10,
      'max' => 50,
      'step' => 10,
    ),
    'ElasticSearch' => 
    array (
      'enabled' => false,
      'host' => 'localhost',
      'user' => '',
      'pass' => '',
    ),
  ),
  'search_wildcard_char' => '%',
  'search_wildcard_infront' => false,
  'securitysuite_additive' => true,
  'securitysuite_filter_user_list' => true,
  'securitysuite_inbound_email' => false,
  'securitysuite_inherit_assigned' => true,
  'securitysuite_inherit_creator' => false,
  'securitysuite_inherit_parent' => false,
  'securitysuite_popup_select' => false,
  'securitysuite_strict_rights' => false,
  'securitysuite_user_popup' => true,
  'securitysuite_user_role_precedence' => true,
  'securitysuite_version' => '6.5.17',
  'session_dir' => '',
  'showDetailData' => true,
  'showThemePicker' => true,
  'site_url' => 'localhost/CRM/dev/dev_longphat',
  'slow_query_time_msec' => '100',
  'sms_brandname' => 
  array (
    'wsdl' => 'https://api-02.worldsms.vn/webapi/sendSMS',
    'user_name' => 'longphatdemo',
    'authentication_key' => '',
    'brand_name' => 'VienThongMN',
  ),
  'stack_trace_errors' => false,
  'sugar_version' => '6.5.25',
  'sugarbeet' => false,
  'suitecrm_version' => '7.11.3',
  'system_email_templates' => 
  array (
    'confirm_opt_in_template_id' => '36b435eb-2882-0bf3-8147-5cc6889e8a98',
  ),
  'time_formats' => 
  array (
    'H:i' => '23:00',
    'h:ia' => '11:00pm',
    'h:iA' => '11:00PM',
    'h:i a' => '11:00 pm',
    'h:i A' => '11:00 PM',
    'H.i' => '23.00',
    'h.ia' => '11.00pm',
    'h.iA' => '11.00PM',
    'h.i a' => '11.00 pm',
    'h.i A' => '11.00 PM',
  ),
  'timef' => 'H:i',
  'tmp_dir' => 'cache/xml/',
  'tracker_max_display_length' => 15,
  'translation_string_prefix' => false,
  'unique_key' => 'b3847f6a496a1966c98c78139793cc6f',
  'upload_badext' => 
  array (
    0 => 'php',
    1 => 'php3',
    2 => 'php4',
    3 => 'php5',
    4 => 'pl',
    5 => 'cgi',
    6 => 'py',
    7 => 'asp',
    8 => 'cfm',
    9 => 'js',
    10 => 'vbs',
    11 => 'html',
    12 => 'htm',
    13 => 'phtml',
  ),
  'upload_dir' => 'upload/',
  'upload_maxsize' => 30000000,
  'use_common_ml_dir' => false,
  'use_real_names' => false,
  'vcal_time' => '2',
  'verify_client_ip' => false,
  'authenKey' => 'eEENNGZOCHMfwPTT0Dyu7ANbaLK7oMbzm_ELRcQMHIc5zvmeNCr0ARgTYHzvsYC-gkdl8K3J9mQ3ei87HvaEGxZ4e7XBbNLwby-XIJ-FQ46R-Q9hKvTGJFkbfWbQamjFeTRNJd-U6agTxhLCP8rcSwVPg6S6WK5FrTorPn6s65hkWS95ICeF0g64qXLu_WmCaSg-336AHX7JnAfM1_DqVCQ1gZiVudagmB2m8o7JQWBVXw0H1znDKg-cY4rUnKLwll6RG7-UF43Q-EPV68uRLltzr5GwXdv9a0AVS0FFEXy',
  'lp_psd' => '$1$x7ecl99b$Mvdzefb0xXrGf.1R2cjaY.',
  'deposit' => 0,
  'accessToken' => '',
  'TYPE_OA' => 'LONGPHATCRM',
  'default_currency_significant_digits_payroll' => 2,
  'inventory_date_start' => '01/12/2023',
  'inventory_date_end' => '01/03/2024',
  'get_voucher_code' => 
  array (
    'aos_invoices' => 
    array (
      'code' => 'DH_',
      'type_date' => 'd-m-Y',
    ),
    'aos_quotes' => 
    array (
      'code' => 'BG_',
      'type_date' => 'd-m-Y',
    ),
    'aos_inventoryinput' => 
    array (
      'code' => 'PNK_',
      'type_date' => 'd-m-Y',
    ),
    'aos_inventoryoutput' => 
    array (
      'code' => 'PXK_',
      'type_date' => 'd-m-Y',
    ),
    'aos_warehouse_transfer' => 
    array (
      'code' => 'PCK_',
      'type_date' => 'd-m-Y',
    ),
    'accounts' => 
    array (
      'code' => 'KH_',
      'type_date' => 'd-m-Y',
    ),
    'aos_contracts' => 
    array (
      'code' => 'HD_',
      'type_date' => 'd-m-Y',
    ),

  ),
);