<?php
$module_name = 'la_LoginAudit';
$listViewDefs [$module_name] = 
array (
  'TYPED_NAME' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_TYPED_NAME',
    'width' => '10%',
    'default' => true,
  ),
  'IS_ADMIN' => 
  array (
    'type' => 'bool',
    'default' => true,
    'label' => 'LBL_IS_ADMIN',
    'width' => '10%',
  ),
  'IP_ADDRESS' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_IP_ADDRESS',
    'width' => '10%',
    'default' => true,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
  'RESULT' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_RESULT',
    'width' => '10%',
    'default' => true,
  ),
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => false,
    'link' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => false,
  ),
);
;
?>
