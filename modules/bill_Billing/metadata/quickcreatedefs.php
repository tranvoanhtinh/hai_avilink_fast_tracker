<?php
$module_name = 'bill_Billing';
$viewdefs [$module_name] = 
array (
  'QuickCreate' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 
          array (
            'name' => 'type',
            'studio' => 'visible',
            'label' => 'LBL_TYPE',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'billingdate',
            'label' => 'LBL_BILLINGDATE',
          ),
          1 => 
          array (
            'name' => 'billingamount',
            'label' => 'LBL_BILLINGAMOUNT',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'billing_status',
            'studio' => 'visible',
            'label' => 'LBL_BILLING_STATUS',
          ),
          1 => 
          array (
            'name' => 'paymentmethod',
            'studio' => 'visible',
            'label' => 'LBL_PAYMENTMETHOD',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'invoiceno',
            'label' => 'LBL_INVOICENO',
          ),
          1 => 
          array (
            'name' => 'bill_billing_aos_invoices_name',
            'label' => 'LBL_BILL_BILLING_AOS_INVOICES_FROM_AOS_INVOICES_TITLE',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'bill_billing_accounts_name',
            'label' => 'LBL_BILL_BILLING_ACCOUNTS_FROM_ACCOUNTS_TITLE',
          ),
          1 => 'assigned_user_name',
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'description',
            'comment' => 'Full text of the note',
            'label' => 'LBL_DESCRIPTION',
          ),
        ),
      ),
    ),
  ),
);
;
?>
