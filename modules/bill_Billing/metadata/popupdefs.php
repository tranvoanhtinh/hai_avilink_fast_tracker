<?php
$popupMeta = array (
    'moduleMain' => 'bill_Billing',
    'varName' => 'bill_Billing',
    'orderBy' => 'bill_billing.name',
    'whereClauses' => array (
  'name' => 'bill_billing.name',
  'paymentmethod' => 'bill_billing.paymentmethod',
  'type' => 'bill_billing.type',
  'accounts' => 'bill_billing.accounts',
  'invoiceno' => 'bill_billing.invoiceno',
  'assigned_user_name' => 'bill_billing.assigned_user_name',
  'assigned_user_id' => 'bill_billing.assigned_user_id',
  'bill_billing_accounts_name' => 'bill_billing.bill_billing_accounts_name',
  'billing_status' => 'bill_billing.billing_status',
  'billingdate' => 'bill_billing.billingdate',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'paymentmethod',
  5 => 'type',
  6 => 'accounts',
  7 => 'invoiceno',
  8 => 'assigned_user_name',
  9 => 'assigned_user_id',
  10 => 'bill_billing_accounts_name',
  11 => 'billing_status',
  12 => 'billingdate',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => '10%',
  ),
  'bill_billing_accounts_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_BILL_BILLING_ACCOUNTS_FROM_ACCOUNTS_TITLE',
    'id' => 'BILL_BILLING_ACCOUNTSACCOUNTS_IDA',
    'width' => '10%',
    'name' => 'bill_billing_accounts_name',
  ),
  'paymentmethod' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_PAYMENTMETHOD',
    'width' => '10%',
    'name' => 'paymentmethod',
  ),
  'type' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_TYPE',
    'width' => '10%',
    'name' => 'type',
  ),
  'billingdate' => 
  array (
    'type' => 'date',
    'label' => 'LBL_BILLINGDATE',
    'width' => '10%',
    'name' => 'billingdate',
  ),
  'accounts' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'label' => 'LBL_ACCOUNTS',
    'id' => 'ACCOUNT_ID_C',
    'link' => true,
    'width' => '10%',
    'name' => 'accounts',
  ),
  'billing_status' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_BILLING_STATUS',
    'width' => '10%',
    'name' => 'billing_status',
  ),
  'invoiceno' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_INVOICENO',
    'width' => '10%',
    'name' => 'invoiceno',
  ),
  'assigned_user_name' => 
  array (
    'link' => true,
    'type' => 'relate',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'id' => 'ASSIGNED_USER_ID',
    'width' => '10%',
    'name' => 'assigned_user_name',
  ),
  'assigned_user_id' => 
  array (
    'name' => 'assigned_user_id',
    'label' => 'LBL_ASSIGNED_TO',
    'type' => 'enum',
    'function' => 
    array (
      'name' => 'get_user_array',
      'params' => 
      array (
        0 => false,
      ),
    ),
    'width' => '10%',
  ),
),
);
