<?php
$module_name = 'bill_Billing';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'type' => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_TYPE',
        'width' => '10%',
        'name' => 'type',
      ),
      'billing_status' => 
      array (
        'type' => 'enum',
        'studio' => 'visible',
        'label' => 'LBL_BILLING_STATUS',
        'width' => '10%',
        'default' => true,
        'name' => 'billing_status',
      ),
      'current_user_only' => 
      array (
        'name' => 'current_user_only',
        'label' => 'LBL_CURRENT_USER_FILTER',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
      ),
    ),
    'advanced_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'billing_status' => 
      array (
        'type' => 'enum',
        'studio' => 'visible',
        'label' => 'LBL_BILLING_STATUS',
        'width' => '10%',
        'default' => true,
        'name' => 'billing_status',
      ),
      'paymentmethod' => 
      array (
        'type' => 'enum',
        'studio' => 'visible',
        'label' => 'LBL_PAYMENTMETHOD',
        'width' => '10%',
        'default' => true,
        'name' => 'paymentmethod',
      ),
      'bill_billing_accounts_name' => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_BILL_BILLING_ACCOUNTS_FROM_ACCOUNTS_TITLE',
        'id' => 'BILL_BILLING_ACCOUNTSACCOUNTS_IDA',
        'width' => '10%',
        'default' => true,
        'name' => 'bill_billing_accounts_name',
      ),
      'bill_billing_aos_invoices_name' => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_BILL_BILLING_AOS_INVOICES_FROM_AOS_INVOICES_TITLE',
        'id' => 'BILL_BILLING_AOS_INVOICESAOS_INVOICES_IDA',
        'width' => '10%',
        'default' => true,
        'name' => 'bill_billing_aos_invoices_name',
      ),
      'invoiceno' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_INVOICENO',
        'width' => '10%',
        'default' => true,
        'name' => 'invoiceno',
      ),
      'type' => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_TYPE',
        'width' => '10%',
        'name' => 'type',
      ),
      'billingdate' => 
      array (
        'type' => 'date',
        'label' => 'LBL_BILLINGDATE',
        'width' => '10%',
        'default' => true,
        'name' => 'billingdate',
      ),
      'date_entered' => 
      array (
        'type' => 'datetime',
        'label' => 'LBL_DATE_ENTERED',
        'width' => '10%',
        'default' => true,
        'name' => 'date_entered',
      ),
      'assigned_user_id' => 
      array (
        'name' => 'assigned_user_id',
        'label' => 'LBL_ASSIGNED_TO',
        'type' => 'enum',
        'function' => 
        array (
          'name' => 'get_user_array',
          'params' => 
          array (
            0 => false,
          ),
        ),
        'default' => true,
        'width' => '10%',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
;
?>
