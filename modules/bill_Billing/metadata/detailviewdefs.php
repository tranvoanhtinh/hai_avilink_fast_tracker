<?php
$module_name = 'bill_Billing';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_DETAILVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 
          array (
            'name' => 'type',
            'studio' => 'visible',
            'label' => 'LBL_TYPE',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'billingdate',
            'label' => 'LBL_BILLINGDATE',
          ),
          1 => 
          array (
            'name' => 'billingamount',
            'label' => 'LBL_BILLINGAMOUNT',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'billing_status',
            'studio' => 'visible',
            'label' => 'LBL_BILLING_STATUS',
          ),
          1 => 
          array (
            'name' => 'paymentmethod',
            'studio' => 'visible',
            'label' => 'LBL_PAYMENTMETHOD',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'invoiceno',
            'label' => 'LBL_INVOICENO',
          ),
          1 => 
          array (
            'name' => 'bill_billing_aos_invoices_name',
            'label' => 'LBL_BILL_BILLING_AOS_INVOICES_FROM_AOS_INVOICES_TITLE',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'bill_billing_accounts_name',
            'label' => 'LBL_BILL_BILLING_ACCOUNTS_FROM_ACCOUNTS_TITLE',
          ),
          1 => 'assigned_user_name',
        ),
        5 => 
        array (
          0 => 'description',
        ),
      ),
      'lbl_detailview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'created_by_name',
            'label' => 'LBL_CREATED',
          ),
          1 => 
          array (
            'name' => 'modified_by_name',
            'label' => 'LBL_MODIFIED_NAME',
          ),
        ),
        1 => 
        array (
          0 => 'date_entered',
          1 => 'date_modified',
        ),
      ),
    ),
  ),
);
;
?>
