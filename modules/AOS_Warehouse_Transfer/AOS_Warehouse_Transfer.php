<?php
/**
 * Advanced OpenSales, Advanced, robust set of sales modules.
 * @package Advanced OpenSales for SugarCRM
 * @copyright SalesAgility Ltd http://www.salesagility.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHin ANY WARRANTY; within even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU AFFERO GENERAL PUBLIC LICENSE
 * along with this program; if not, see http://www.gnu.org/licenses
 * or write to the Free Software Foundation,Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA 02110-1301  USA
 *
 * @author SalesAgility <info@salesagility.com>
 */

require_once('modules/AOS_Warehouse_Transfer/AOS_Warehouse_Transfer_sugar.php');
class AOS_Warehouse_Transfer extends AOS_Warehouse_Transfer_sugar
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @deprecated deprecated since version 7.6, PHP4 Style Constructors are deprecated and will be remove in 7.8, please update your code, use __construct instead
     */
    public function AOS_Warehouse_Transfer()
    {
        $deprecatedMessage = 'PHP4 Style Constructors are deprecated and will be remove in 7.8, please update your code';
        if (isset($GLOBALS['log'])) {
            $GLOBALS['log']->deprecated($deprecatedMessage);
        } else {
            trigger_error($deprecatedMessage, E_USER_DEPRECATED);
        }
        self::__construct();
    }


    public function save($check_notify = false)
    {
        global $sugar_config,  $db;

        if (empty($this->id) || $this->new_with_id
            || (isset($_POST['duplicateSave']) && $_POST['duplicateSave'] == 'true')) {
            if (isset($_POST['group_id'])) {
                unset($_POST['group_id']);
            }
            if (isset($_POST['product_id'])) {
                unset($_POST['product_id']);
            }

        }
        require_once 'include/utils/get_voucher_code.php';

        if($this->name ==''){
            $this->name = get_voucher_code($this->table_name, $this->voucherdate);
        }

        require_once('modules/AOS_Products_Quotes/AOS_Utils.php');
        //reset-------------------------------
        $import_id = $this->import_id;
        $export_id = $this->export_id;

        $q = "DELETE FROM aos_stock_inventory_detail
              WHERE parent_id = '$this->import_id'";
        $db->query($q);

        $q2 = "DELETE FROM aos_stock_inventory_detail
              WHERE parent_id = '$this->export_id'";
        $db->query($q2);

        $q3 = "DELETE  FROM aos_inventoryinput
        WHERE id = '".$import_id."'";
        $db->query($q3);

        $q4 = "DELETE  FROM aos_inventoryoutput
        WHERE id = '".$export_id."'";
        $db->query($q4);
        //end----------------------------


        perform_aos_save($this);
        $return_id = parent::save($check_notify);
        //save x3
        require_once('modules/AOS_Pos_Line_Items_Detail/AOS_Pos_Line_Items_Detail.php'); //class save chuyển kho 
        $warehouse_Transfer2 = new AOS_Pos_Line_Items_Detail();
        $warehouse_Transfer2->save_groups($_POST, $this, 'group_');


        require_once('modules/AOS_Pos_Line_Items_Detail/AOS_Pos_Warehouse_Transfer.php'); //class save chuyển kho 
        $warehouse_Transfer = new AOS_Pos_Warehouse_Transfer();
        $warehouse_Transfer->save_groups($_POST,$this, 'group_','AOS_Inventoryinput');  //++  nhận
        $warehouse_Transfer->save_groups($_POST,$this, 'group_','AOS_Inventoryoutput'); //--  chuyển 

        $transfer_id = $this->id;
        $sql = "SELECT * FROM aos_inventoryinput WHERE transfer_id ='".$transfer_id."'";
        $result = $db->query($sql);
        $row = $result->fetch_assoc();

        $sql2 = "SELECT * FROM aos_inventoryoutput WHERE transfer_id ='".$transfer_id."'";
        $result2 = $db->query($sql2);
        $row2 = $result2->fetch_assoc();

        if ($row && $row2) {
            $update = "UPDATE aos_warehouse_transfer SET import_id = '".$row['id']."', export_id = '".$row2['id']."' WHERE id = '".$transfer_id."'";
            $db->query($update);
        }

        return $return_id;
    }

    public function mark_deleted($id)
    {
        global $sugar_config,  $db;
        $q3 = "DELETE  FROM aos_inventoryinput
        WHERE id = '".$import_id."'";
        $db->query($q3);

        $q4 = "DELETE  FROM aos_inventoryoutput
        WHERE id = '".$export_id."'";
        $db->query($q4);

        require_once('modules/AOS_Products_Pos/AOS_Products_Pos.php');
        $productQuote = new AOS_Products_pos();
        $productQuote->mark_lines_deleted($this);

        require_once('modules/AOS_Inventoryinput/AOS_Inventoryinput.php');
        $inventoryinput = new AOS_Inventoryinput();
        $inventoryinput->make_transfer_deleted($this);

        require_once('modules/AOS_Inventoryoutput/AOS_Inventoryoutput.php');
        $inventoryout = new AOS_Inventoryoutput();
        $inventoryout->make_transfer_deleted($this);
        
        parent::mark_deleted($id);
    }



}

