<?php
$module_name = 'cs_cusUser';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 'assigned_user_name',
        ),
        1 => 
        array (
          0 => 'description',
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'username',
            'label' => 'LBL_USERNAME',
          ),
          1 => 
          array (
            'name' => 'title',
            'label' => 'LBL_TITLE',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'last_name',
            'label' => 'LBL_LAST_NAME',
          ),
          1 => 
          array (
            'name' => 'groupname',
            'studio' => 'visible',
            'label' => 'LBL_GROUPNAME',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'groupname2',
            'studio' => 'visible',
            'label' => 'LBL_GROUPNAME2',
          ),
          1 => 
          array (
            'name' => 'status',
            'studio' => 'visible',
            'label' => 'LBL_STATUS',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'role',
            'studio' => 'visible',
            'label' => 'LBL_ROLE',
          ),
          1 => 
          array (
            'name' => 'provincecode',
            'studio' => 'visible',
            'label' => 'LBL_PROVINCECODE',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'groupid',
            'label' => 'LBL_GROUPID',
          ),
        ),
      ),
    ),
  ),
);
;
?>
