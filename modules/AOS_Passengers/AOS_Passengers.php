<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2018 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */


class AOS_Passengers extends Basic
{
    public $new_schema = true;
    public $module_dir = 'AOS_Passengers';
    public $object_name = 'AOS_Passengers';
    public $table_name = 'aos_passengers';
    public $importable = false;

    public $id;
    public $name;
    public $date_entered;
    public $date_modified;
    public $modified_user_id;
    public $modified_by_name;
    public $created_by;
    public $created_by_name;
    public $description;
    public $deleted;
    public $created_by_link;
    public $modified_user_link;
    public $assigned_user_id;
    public $assigned_user_name;
    public $assigned_user_link;
    public $SecurityGroups;
	
    public function bean_implements($interface)
    {
        switch($interface)
        {
            case 'ACL':
                return true;
        }

        return false;
    }

   public function save($check_notify = false)
    {
        global $sugar_config, $db;

        // Gọi phương thức lưu cha và lấy ID trả về
        if($_POST['delete_photo'] !=''){
            $this->photo2 = '';

            $currentImageFile = $sugar_config['upload_dir'] . $_POST['delete_photo'];
            if (file_exists($currentImageFile)) {
                unlink($currentImageFile);
            }
        }
        $return_id = parent::save($check_notify);

        // Kiểm tra nếu có hình ảnh được tải lên
        if (isset($_FILES['photo']) && $_FILES['photo']['error'] === UPLOAD_ERR_OK) {

            $file = $_FILES['photo'];
            $file_name = $file['name'];

          if($this->photo2 !=  basename($file_name)){
            $file_tmp_name = $file['tmp_name'];

            // Tạo tên file mới với định dạng $this->id . '_' . $tên_hình
            $new_file_name = $this->id . '_' . basename($file_name);

            // Định nghĩa thư mục lưu trữ hình ảnh
            $upload_dir = $sugar_config['upload_dir'];
            
            // Đảm bảo thư mục upload tồn tại
            if (!is_dir($upload_dir)) {
                mkdir($upload_dir, 0777, true);
            }

            // Định nghĩa đường dẫn đầy đủ cho file upload
            $upload_file_path = $upload_dir . $new_file_name;

            // Di chuyển file từ thư mục tạm sang thư mục đích
            if (move_uploaded_file($file_tmp_name, $upload_file_path)) {
                // Giảm kích thước hình ảnh nếu cần
                $this->resizeImageIfNeeded($upload_file_path);

                // Cập nhật thuộc tính liên quan đến ảnh nếu cần
                $this->photo2 = basename($file_name);
                $return_id = parent::save($check_notify);

            }
        }
    }
        return $return_id;
    }

    private function resizeImageIfNeeded($upload_file_path)
    {
        $maxFileSize = 1024 * 1024; // 1MB

        // Kiểm tra kích thước file ảnh
        $fileSize = filesize($upload_file_path);

        if ($fileSize > $maxFileSize) {
            // Xác định loại hình ảnh dựa trên phần mở rộng
            $imageInfo = getimagesize($upload_file_path);
            $mimeType = $imageInfo['mime'];
            
            switch ($mimeType) {
                case 'image/jpeg':
                    $sourceImage = imagecreatefromjpeg($upload_file_path);
                    break;
                case 'image/png':
                    $sourceImage = imagecreatefrompng($upload_file_path);
                    break;
                case 'image/gif':
                    $sourceImage = imagecreatefromgif($upload_file_path);
                    break;
                default:
                    return; // Không hỗ trợ định dạng hình ảnh
            }

            $sourceWidth = imagesx($sourceImage);
            $sourceHeight = imagesy($sourceImage);

            // Thiết lập kích thước mới cho ảnh (giảm kích thước)
            $newWidth = $sourceWidth * 0.5;
            $newHeight = $sourceHeight * 0.5;

            // Tạo một ảnh mới với kích thước mới
            $newImage = imagecreatetruecolor($newWidth, $newHeight);

            // Thay đổi kích thước của ảnh gốc thành ảnh mới
            imagecopyresampled($newImage, $sourceImage, 0, 0, 0, 0, $newWidth, $newHeight, $sourceWidth, $sourceHeight);

            // Lưu ảnh mới với chất lượng tốt hơn và ghi đè lên tệp ảnh gốc
            switch ($mimeType) {
                case 'image/jpeg':
                    imagejpeg($newImage, $upload_file_path, 100); // 90 là chất lượng hình ảnh (giữa 0 và 100)
                    break;
                case 'image/png':
                    imagepng($newImage, $upload_file_path, 9); // 9 là chất lượng hình ảnh (giữa 0 và 9)
                    break;
                case 'image/gif':
                    imagegif($newImage, $upload_file_path);
                    break;
            }

            // Giải phóng bộ nhớ được sử dụng
            imagedestroy($sourceImage);
            imagedestroy($newImage);
        }
    }


	
}
