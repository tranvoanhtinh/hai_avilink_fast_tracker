<?php

function display_photo($focus, $field, $value, $view)
{
    global $sugar_config, $locale, $app_list_strings, $mod_strings;
    $html = '';

    if ($view == 'EditView') {
        // Tạo thẻ chứa hình ảnh mới
        $html .= '<div id="photo_new">';
        $html .= '<input type="file" id="photo_file" name="photo">'; // Tạo input dạng file với id và name là photo_file
        $html .= '</div>';

        // Kiểm tra nếu có hình ảnh hiện tại
        if (!empty($focus->photo2)) {
            // Xây dựng đường dẫn đến hình ảnh hiện tại
            $imageSrc = $sugar_config['upload_dir'] . $focus->id . '_' . $focus->photo2;
            
            // Thêm đường dẫn hình ảnh hiện tại vào thuộc tính data
            $html .= '<div id="photo_current" data-image-src="' . htmlspecialchars($imageSrc, ENT_QUOTES, 'UTF-8') . '">';
            $html .= '<img id="photo_preview" src="" alt="Current Photo" style="max-width: 250px; max-height: 250px; display: none;">';
            $html .= '<button type="button" id="delete_photo" style="display: none;">Xóa</button>'; // Thêm nút xóa nếu cần
            $html .= '</div>';
        }

        // Thẻ chứa hình ảnh cũ (có thể để trống hoặc thêm thông tin khác nếu cần)
        $html .= '<span id="photo_old"></span>';

        // Thêm trường ẩn để lưu tên hình ảnh cần xóa
        if (!empty($focus->photo2)) {
            $html .= '<input type="hidden" id="delete_photo_input" name="delete_photo" value="' . htmlspecialchars($focus->photo2, ENT_QUOTES, 'UTF-8') . '">';
        }
        
        return $html;
    } elseif ($view == 'DetailView') {
    	if($focus->photo2 !=''){
        // Xây dựng tên file hình ảnh dựa trên $id và $photo2
        $imageSrc = $sugar_config['upload_dir'] . $focus->id . '_' . $focus->photo2;
        
        // Tạo thẻ <img> để hiển thị hình ảnh
        $html .= '<img src="' . htmlspecialchars($imageSrc, ENT_QUOTES, 'UTF-8') . '" alt="Photo" style="max-width: 250px; max-height: 250px;">';
        
        return $html;
    	}

    }
}
?>
