<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2018 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */
require_once("modules/AOS_Pos_Line_Items_Detail/AOS_Pos_Line_Items_Detail_sugar.php");

class AOS_Pos_Line_Items_Detail extends AOS_Pos_Line_Items_Detail_sugar
{
     public function __construct()
    {
        parent::__construct();
    }

    /**
     * @deprecated deprecated since version 7.6, PHP4 Style Constructors are deprecated and will be remove in 7.8, please update your code, use __construct instead
     */
    public function AOS_Pos_Line_Items_Detail()
    {
        $deprecatedMessage = 'PHP4 Style Constructors are deprecated and will be remove in 7.8, please update your code';
        if (isset($GLOBALS['log'])) {
            $GLOBALS['log']->deprecated($deprecatedMessage);
        } else {
            trigger_error($deprecatedMessage, E_USER_DEPRECATED);
        }
        self::__construct();
    }


    public function save_groups($post_data, $parent, $key = '')
    {

        $groups = array();
        $group_count = isset($post_data[$key . 'group_number']) ? count($post_data[$key . 'group_number']) : 0;
        $j = 0;
        for ($i = 0; $i < $group_count; ++$i) {
            $postData = null;
            if (isset($post_data[$key . 'deleted'][$i])) {
                $postData = $post_data[$key . 'deleted'][$i];
            } else {
                LoggerManager::getLogger()->warn('AOS Line Item Group deleted field is not set in requested POST data at key: ' . $key . '['. $i .']');
            }

            if ($postData == 1) {
                $this->mark_deleted($post_data[$key . 'id'][$i]);
            } else {
                $pos_groups = new AOS_Pos_Line_Items_Detail();
                foreach ($this->field_defs as $field_def) {
                    $field_name = $field_def['name'];
                    if (isset($post_data[$key . $field_name][$i])) {
                        $pos_groups->$field_name = $post_data[$key . $field_name][$i];
                    }
                    if (isset($post_data[$key .'lp_'. $field_name][$i])) {
                        $pos_groups->$field_name = $post_data[$key .'lp_'. $field_name][$i];
                    }
                }
                $pos_groups->number = ++$j;
                $pos_groups->assigned_user_id = $parent->assigned_user_id;

                $parentCurrencyId = null;
                if (isset($parent->currency_id)) {
                    $parentCurrencyId = $parent->currency_id;
                } else {
                    LoggerManager::getLogger()->warn('AOS Line Item Group trying to save group nut Parent currency ID is not set');
                }

                $pos_groups->currency_id = $parentCurrencyId;
                $pos_groups->parent_id = $parent->id;
                $pos_groups->parent_type = $parent->object_name;

                $pos_groups->save();
                $post_data[$key . 'id'][$i] = $pos_groups->id;


                if (isset($post_data[$key . 'group_number'][$i])) {
                    $groups[$post_data[$key . 'group_number'][$i]] = $pos_groups->id;
                }
            }
        }
        require_once('modules/AOS_Products_Pos/AOS_Products_Pos.php');
        $productPos = new AOS_Products_Pos();
        $productPos->save_lines($post_data, $parent, $groups, 'product_');
    }

    public function save($check_notify = false)
    {
        require_once('modules/AOS_Products_Quotes/AOS_Utils.php');
        perform_aos_save($this);
        return parent::save($check_notify);
    }
    
}
