<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2018 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */
require_once("modules/AOS_Pos_Line_Items_Detail/AOS_Pos_Line_Items_Detail_sugar.php");

class AOS_Pos_Warehouse_Transfer extends AOS_Pos_Line_Items_Detail_sugar
{
     public function __construct()
    {
        parent::__construct();
    }

    /**
     * @deprecated deprecated since version 7.6, PHP4 Style Constructors are deprecated and will be remove in 7.8, please update your code, use __construct instead
     */
    public function AOS_Pos_Warehouse_Transfer()
    {
        $deprecatedMessage = 'PHP4 Style Constructors are deprecated and will be remove in 7.8, please update your code';
        if (isset($GLOBALS['log'])) {
            $GLOBALS['log']->deprecated($deprecatedMessage);
        } else {
            trigger_error($deprecatedMessage, E_USER_DEPRECATED);
        }
        self::__construct();
    }


    public function save_groups($post_data, $parent, $key = '',$module)
    {
        if(isset($module) && $module !=''&&$module !=null){
            if($parent->status_inventory==2){
            $module_save = BeanFactory::getBean($module, $post_data[$key . 'id'][$i]);
            if (!$module_save) {
                $module_save = BeanFactory::newBean($module);
            }
            $module_save->transfer_id = $parent->id;
            $module_save->status_inventory = 2;
            $table_module = strtolower($module);
            require_once 'include/utils/get_voucher_code.php';

            $dateString = $parent->voucherdate;
            $date = DateTime::createFromFormat('Y-m-d', $dateString);
            $newDateString = $date->format('d/m/Y');
            $module_save->name = get_voucher_code($table_module,$newDateString);
            $module_save->lp_total_amt = $parent->lp_total_amt;
            $module_save->lp_discount_amount = $parent->lp_discount_amount;
            $module_save->lp_subtotal_amount = $parent->lp_subtotal_amount;
            $module_save->lp_shipping_amount = $parent->lp_shipping_amount;
            $module_save->shipping_tax = $parent->shipping_tax;
            $module_save->lp_tax_amount = $parent->lp_tax_amount;
            $module_save->lp_total_amount = $parent->lp_total_amount;
            $module_save->shipping_tax_amt = $parent->shipping_tax_amt;
            $module_save->total_discount_amount = $parent->total_discount_amount;
            $module_save->discount_percent = $parent->discount_percent;
            $module_save->advance_payment = $parent->advance_payment;
            $module_save->remaining_money = $parent->remaining_money;
            $module_save->transfer = 1;
            $module_save->voucherdate = $parent->voucherdate;
            if($module  == 'AOS_Inventoryinput'){
                $module_save->transfer_from = $parent->from_warehouse;
                $module_save->take_from = $parent->to_warehouse;

            }else {
               $module_save->transfer_to = $parent->to_warehouse;
               $module_save->take_from = $parent->from_warehouse;
            }
            $module_save->save();
         }
       }
    }
    

    public function save($check_notify = false)
    {
        require_once('modules/AOS_Products_Quotes/AOS_Utils.php');
        perform_aos_save($this);
        return parent::save($check_notify);
    }
    
}
