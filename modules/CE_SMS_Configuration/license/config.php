<?php

$outfitters_config = array(
    'name' => 'CE_SMS_Configuration', 
    'shortname' => '',
    'public_key' => '',
    'api_url' => '',
    'validate_users' => false,
    'manage_licensed_users' => false,
    'validation_frequency' => 'daily',
    'continue_url' => '', 
);

