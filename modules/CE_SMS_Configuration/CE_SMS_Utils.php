<?php

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

class ce_sms_utils {
	
	
	private $wsdl = null;
    private $user_name = null;
    private $authentication_key = null;
    private $brand_name = null;
public function __construct()
    {
        $configuration = new Configurator();
        $configuration->loadConfig();
        $this->wsdl = isset($configuration->config['sms_brandname']['wsdl']) ? $configuration->config['sms_brandname']['wsdl'] : "";
        $this->user_name = isset($configuration->config['sms_brandname']['user_name']) ? $configuration->config['sms_brandname']['user_name'] : "";
        $this->authentication_key = isset($configuration->config['sms_brandname']['authentication_key']) ? $configuration->config['sms_brandname']['authentication_key'] : "";
        $this->brand_name = isset($configuration->config['sms_brandname']['brand_name']) ? $configuration->config['sms_brandname']['brand_name'] : "";
    }

    function getmobilenumbers($modulefrom, $records) {

        $sending_rec_id = explode(",", $records);

        $listwithnumber = array();
        $mobile_list = "";
        for ($ismsc = 0; $ismsc < count($sending_rec_id); $ismsc++) {
            $phone_mobile = $this->getnumberfromid($modulefrom, $sending_rec_id[$ismsc]);
            $listwithnumber[$phone_mobile] = $sending_rec_id[$ismsc];
            if((count($sending_rec_id) -1 ) == $ismsc)
            {
            $mobile_list.=$phone_mobile;    
            }else
            {
            $mobile_list.=$phone_mobile . ",";
            }
        }
        $listwithnumber['sms_rec_list'] = $mobile_list;
        return $listwithnumber;
    }

    function save_gateway($REQUEST) {
        
        // Http Gateway Variables
        $gateway = trim($_REQUEST['gateway']);
        $username = trim($_REQUEST['username']);
        $password = trim($_REQUEST['password']);
        $from = trim($_REQUEST['from']);
        $parameter1 = trim($_REQUEST['parameter1']);
        $parameter2 = trim($_REQUEST['parameter2']);
        $parameter3 = trim($_REQUEST['parameter3']);
        $parameter4 = trim($_REQUEST['parameter4']);
        $parameter5 = trim($_REQUEST['parameter5']);
        $parameter6 = trim($_REQUEST['parameter6']);
        $parameter7 = trim($_REQUEST['parameter7']);
        $http_to = trim($_REQUEST['http_to']);
        $api_url = trim($_REQUEST['api_url']);

        //Twilio Variable
        $account_sid = trim($_REQUEST['account_sid']);
        $auth_token = trim($_REQUEST['auth_token']);
        $from_number = trim($_REQUEST['from_number']);
        $twilio_to = trim($_REQUEST['twilio_to']);

        //Plivo Variable

        $plivo_auth_id = trim($_REQUEST['plivo_auth_id']);
        $plivo_src = trim($_REQUEST['plivo_src']);
        $plivo_text = trim($_REQUEST['plivo_text']);
        $plivo_auth_token = trim($_REQUEST['plivo_auth_token']);
        $pl_destination = trim($_REQUEST['pl_destination']);
        $pl_api_url = trim($_REQUEST['pl_api_url']);
		
			
       $checking_rec = $this->record_avail($gateway);
        
         if (empty($checking_rec)) {
            //Insesrt gateway
            if ($gateway === "Http") {
                $sql_ht = "INSERT INTO ce_sms_configuration (id, username, password, from_number, parameter_one, parameter_two, parameter_three, parameter_four, parameter_five, parameter_six, parameter_seven, to_number, api_url, sms_type)"
                        . " VALUES (uuid(), '$username', '$password', '$from', '$parameter1', '$parameter2', '$parameter3', '$parameter4', '$parameter5', '$parameter6', '$parameter7', '$http_to', '$api_url', 'Http')";
                $result_ht = $GLOBALS['db']->query($sql_ht);
            }
            if ($gateway === "Twilio") {
                $sql_tw = "INSERT INTO ce_sms_configuration (id, tw_account_sid, tw_auth_token, tw_from_number, tw_to_number, sms_type) "
                        . "VALUES (uuid(), '$account_sid', '$auth_token', '$from_number', '$twilio_to', 'Twilio')";
                $result_tw = $GLOBALS['db']->query($sql_tw);
            }
            if ($gateway == "Plivo") {
                $sql_pl = "INSERT INTO ce_sms_configuration (id, pl_auth_id, pl_auth_token, pl_src_number, pl_text, pl_destination, pl_api_url, sms_type)"
                        . " VALUES (uuid(),'$plivo_auth_id', '$plivo_auth_token', '$plivo_src', '$plivo_text', '$pl_destination', '$pl_api_url', 'Plivo')";
                $result_pl = $GLOBALS['db']->query($sql_pl);
            }
        } else {
            //Update gateway
            if ($gateway === "Http") {
                $sql_ht_u = "Update ce_sms_configuration Set username='$username',password='$password', from_number='$from',"
                        . " parameter_one='$parameter1', parameter_two='$parameter2', parameter_three='$parameter3', parameter_four='$parameter4', "
                        . "parameter_five='$parameter5', parameter_six='$parameter6', parameter_seven='$parameter7', to_number='$http_to', api_url='$api_url' where sms_type = 'Http'";
                $result_ht_u = $GLOBALS['db']->query($sql_ht_u);
            }

            if ($gateway === "Twilio") {
                $sql_tw_u = "Update ce_sms_configuration Set tw_account_sid='$account_sid', tw_auth_token='$auth_token', tw_from_number='$from_number', tw_to_number='$twilio_to' where sms_type='Twilio'";
                $result_tw_u = $GLOBALS['db']->query($sql_tw_u);
            }

            if ($gateway === "Plivo") {
                $sql_pl = "Update ce_sms_configuration Set pl_auth_id='$plivo_auth_id', pl_auth_token='$plivo_auth_token', pl_src_number='$plivo_src', pl_text='$plivo_text', pl_destination='$pl_destination', pl_api_url='$pl_api_url' where sms_type='Plivo'";
                $result_pl = $GLOBALS['db']->query($sql_pl);
            }
        }
        //Update the gateway
        $this->save_type($gateway);
    }

    public function getnumberfromid($module, $rec_id) {

        $Bean = BeanFactory::newBean($module);
        $SearchSMS = $Bean->retrieve_by_string_fields(array('id' => $rec_id));
        $foundmobilenumb = "";
        if ($module == "Accounts") {
            $acc_val = array("phone_office", "phone_alternate", "phone_fax");
            foreach ($acc_val as $fieldssearch_id) {
                $fieldssearch = trim($fieldssearch_id);
                $foundmob = $SearchSMS->$fieldssearch;
                if (!empty($foundmob)) {
                    $foundmobilenumb = $foundmob;
                    break;
                }
            }
        }

        if ($module == "Contacts") {
            $getarr_val = array("phone_mobile", "phone_other", "phone_work", "phone_fax", "phone_home");
            foreach ($getarr_val as $fieldssearch_id) {
                $fieldssearch = trim($fieldssearch_id);
                $foundmob = $SearchSMS->$fieldssearch;
                if (!empty($foundmob)) {
                    $foundmobilenumb = $foundmob;
                    break;
                }
            }
        }

        if ($module == "Leads") {
            $getarr_val = array("phone_mobile", "phone_other", "phone_work", "phone_fax", "phone_home");
            foreach ($getarr_val as $fieldssearch_id) {
                $fieldssearch = trim($fieldssearch_id);
                $foundmob = $SearchSMS->$fieldssearch;
                if (!empty($foundmob)) {
                    $foundmobilenumb = $foundmob;
                    break;
                }
            }
        }

        return $foundmobilenumb;
    }

    function record_avail($type) {
        $sql_et = "SELECT id FROM ce_sms_configuration where sms_type='$type'";
        $result_et = $GLOBALS['db']->query($sql_et);
        $result_row = $GLOBALS['db']->fetchByAssoc($result_et);
        return $result_row['id'];
    }

    function save_type($ce_sms_type) {
        require_once('modules/Configurator/Configurator.php');
        $confi_sms = new Configurator();

        $confi_sms->loadConfig();
        $confi_sms->config['ce_sms_type'] = $ce_sms_type;
        $confi_sms->saveConfig();
    }

    function httpurl($mobtosend, $msgtosend) {
       /*  $sql_et = "Select username, password, from_number, parameter_one, parameter_two, parameter_three, parameter_four, parameter_five, parameter_six, parameter_seven, to_number, api_url from ce_sms_configuration where sms_type = 'Http'";
        $result_et = $GLOBALS['db']->query($sql_et);
        $row_et = $GLOBALS['db']->fetchByAssoc($result_et);

        $smsparam = array();
        if (!empty($row_et['username'])) {
            array_push($smsparam, trim($row_et['username']));
        }
        if (!empty($row_et['password'])) {
            array_push($smsparam, trim($row_et['password']));
        }
        if (!empty($row_et['from_number'])) {
            array_push($smsparam, trim($row_et['from_number']));
        }
        if (!empty($row_et['parameter_one'])) {
            array_push($smsparam, trim($row_et['parameter_one']));
        }
        if (!empty($row_et['parameter_two'])) {
            array_push($smsparam, trim($row_et['parameter_two']));
        }
        if (!empty($row_et['parameter_three'])) {
            array_push($smsparam, trim($row_et['parameter_three']));
        }
        if (!empty($row_et['parameter_four'])) {
            array_push($smsparam, trim($row_et['parameter_four']));
        }

        if (!empty($row_et['parameter_five'])) {
            array_push($smsparam, trim($row_et['parameter_five']));
        }
        if (!empty($row_et['parameter_six'])) {
            array_push($smsparam, trim($row_et['parameter_six']));
        }

        //parameter7 = text
        if (!empty($row_et['parameter_seven'])) {
            $split_text = explode("=", trim($row_et['parameter_seven']));
            $to_text = $split_text[0] . "=" . $msgtosend;
            array_push($smsparam, $to_text);
        }
        //Number
        if (!empty($row_et['to_number'])) {
            $split_number = explode("=", trim($row_et['to_number']));
            $to_number = $split_number[0] . "=" . $mobtosend;
            array_push($smsparam, $to_number);
        }

        $api_url = trim($row_et['api_url']);

        for ($api = 0; $api < count($smsparam); $api++) {
            if (!empty($smsparam[$api])) {
                if ($api == 0) {
                    $api_url .= trim($smsparam[$api]);
                } else {
                    $api_url .= "&" . trim($smsparam[$api]);
                }
            }
        } */
      
        return $api_url;
    }

    function sendbytwilio($mobile_numbers, $template_name, $message, $sl_mod, $sl_mod_id) 
	{
       /*  $sql_tw = "Select tw_account_sid, tw_auth_token, tw_from_number, tw_to_number from ce_sms_configuration where sms_type = 'Twilio'";
        $result_tw = $GLOBALS['db']->query($sql_tw);
        $row_tw = $GLOBALS['db']->fetchByAssoc($result_tw); */
		
		 $configuration = new Configurator();
        $configuration->loadConfig();
        $this->wsdl = isset($configuration->config['sms_brandname']['wsdl']) ? $configuration->config['sms_brandname']['wsdl'] : "";
        $this->user_name = isset($configuration->config['sms_brandname']['user_name']) ? $configuration->config['sms_brandname']['user_name'] : "";
        $this->authentication_key = isset($configuration->config['sms_brandname']['authentication_key']) ? $configuration->config['sms_brandname']['authentication_key'] : "";
        $this->brand_name = isset($configuration->config['sms_brandname']['brand_name']) ? $configuration->config['sms_brandname']['brand_name'] : "";

        $mobile_numbers_array = explode(",", $mobile_numbers);
		 $headers = array(
            "Content-type: application/json;charset=utf-8",
            "Accept: application/json",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "Authorization: Basic " . $this->authentication_key
        );
		
        for ($ce = 0; $ce < count($mobile_numbers_array); $ce++) {

            $message = htmlspecialchars_decode($message, ENT_QUOTES);
            $post_data = array(
                "From" => $this->brand_name,
                "To" => "+" . trim($mobile_numbers_array[$ce]),
                "Body" => $message
            );
            $ch = curl_init();
          
        curl_setopt($ch, CURLOPT_URL, $this->wsdl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        if ($post_data) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_USERPWD, "{$this->user_name}");

            if (is_string($post_data)) {
                $post_data = json_decode($post_data, true);
            }
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		
            $result = curl_exec($ch);
            curl_close($ch);

            $result_sms = json_decode($result, true);
            //$GLOBALS['log']->fatal(print_r($result_sms,true));
		
         if (!empty($result_sms['sid'])) {
                $call_status = "Sent";
            } else {
                $call_status = "failed";
            }
			   /*
            //Create the sms
            return $this->create_sms($template_name, $message, $call_status, $sl_mod, $sl_mod_id);
			*/
        }
    }


		
    function sendbyhttp($receiver_id_list = array(), $template = '',$message, $sl_mod, $sl_mod_id) {

        if(empty($receiver_id_list) || empty($template) || empty($module)) {
            $GLOBALS['log']->fatal('Receiver or Content or Module is Empty');
            return false;
        }

        if(!is_array($receiver_id_list)) {
            $receiver_id_list = array($receiver_id_list);
        }
        $response_list = array();
        foreach($receiver_id_list as $receiver_id) {
            $data = array();
            $receiver = BeanFactory::getBean($module);
            $receiver->retrieve($receiver_id);
            $sent = 0;
            if(!empty($receiver->id) && !empty($receiver->mobile)) {
                $mobile = ltrim($receiver->mobile, "0");
                $mobile = "84" . $mobile;
                $content = $this->translateSMSTemplate($template, $receiver);
                $post_data = array(
                    "from" => $this->brand_name,
                    "to" => $mobile,
                    "text" => $content
                );
                $response = $this->sendRequest($post_data);
                $sent = 1;
                $msg = "The message has been sent";
            }
            else {
                $msg = "Can not send message";
                $response = null;
            }
            $data['sent'] = $sent;
            $data['response'] = $response;
            $data['msg'] = $msg;
            $data['mobile'] = $receiver->mobile;
            $data['name'] = $receiver->name;
            $response_list[] = $data;
        }
        return $response_list;
    }

    function create_sms($template_name, $message, $call_status, $sl_mod, $sl_mod_id) {
        global $timedate;
        global $current_user;
        $create_bean = BeanFactory::newBean("Calls");
        $create_bean->date_start = $timedate->getInstance()->nowDb();
        $create_bean->date_end = $timedate->getInstance()->nowDb();
        if (!empty($template_name) && trim($template_name) != "No SMS Template Found") {
            $create_bean->name = "OutboundSMS-" . $template_name;
        } else {
            $create_bean->name = "OutboundSMS";
        }
        $create_bean->description = $message;
        $create_bean->status = $call_status;
        $create_bean->direction = "Outbound";

        $create_bean->set_created_by = false;
        $create_bean->update_date_entered = false;
        $create_bean->update_date_modified = false;
        $create_bean->update_modified_by = false;

        $create_bean->assigned_user_id = $current_user->id;
        $create_bean->modified_user_id = $current_user->id;
        $create_bean->created_by = $current_user->id;

        $create_bean->parent_type = $sl_mod;
        $create_bean->parent_id = $sl_mod_id;
        $create_id = $create_bean->save();

        $RelCall = BeanFactory::getBean('Calls', $create_id);
        if ($sl_mod == "Contacts") {
            $RelCall->load_relationship('contacts');
            $RelCall->contacts->add($sl_mod_id);
        }
        if ($sl_mod == "Leads") {
            $RelCall->load_relationship('leads');
            $RelCall->leads->add($sl_mod_id);
        }

        //Creating Relationships
        return $create_id;
    }
//duy

public function sendRequest($post_data)
    {
		
		
        $headers = array(
            "Content-type: application/json;charset=utf-8",
            "Accept: application/json",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "Authorization: Basic " . $this->authentication_key
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->wsdl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        if ($post_data) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_USERPWD, "{$this->user_name}");

            if (is_string($post_data)) {
                $post_data = json_decode($post_data, true);
            }
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);
        $GLOBALS['log']->debug($response);
        $code = null;
        if (curl_errno($ch)) {
            $GLOBALS['log']->fatal('curl error: ' . curl_error($ch));
            $code = 500;
            $response = json_decode($response, true);
        } else {
            $return_code = (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $code = $return_code;
            $response = json_decode($response, true);
        }
        curl_close($ch);
        return array(
            'code' => $code,
            'response' => $response
        );
  }
    
	
	function sendbyplivo($mobile_numbers, $template_name, $message, $sl_mod, $sl_mod_id) {
		

       /*  $sql_pl = "Select pl_auth_id, pl_auth_token, pl_src_number, pl_text, pl_destination, pl_api_url from ce_sms_configuration where sms_type = 'Plivo'";
        $result_pl = $GLOBALS['db']->query($sql_pl);
        $row_pl = $GLOBALS ['db']->fetchByAssoc($result_pl);*/
		
		$headers = array(
            "Content-type: application/json;charset=utf-8",
            "Accept: application/json",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "Authorization: Basic ".$this->authentication_key
        );
        $mobile_numbers_array = explode(",", $mobile_numbers);
        for ($ce = 0; $ce < count($mobile_numbers_array); $ce++) {
            
           $message = htmlspecialchars_decode($message, ENT_QUOTES);
            $post_data = array(
                "from" => $this->brand_name,
                "to" => trim($mobile_numbers_array[$ce]),
                "text" => $message
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $this->wsdl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($ch, CURLOPT_TIMEOUT, 30);
						 
        if ($post_data) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_USERPWD, "{$this->user_name}");

            if (is_string($post_data)) {
                $post_data = json_decode($post_data, true);
            }
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
        }
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);	
        

            $result = curl_exec($ch);
			$GLOBALS['log']->debug($result);
            curl_close($ch);
            $result_sms = json_decode($result,true);
         
			
			 
			if($result_sms['sent'] == 0) {
                $call_status = "Sent";
            }
			else
			{
				$call_status = "Failure";
			}
			return result_sms;
			  /*
            //Create the sms
            return $this->create_sms($template_name, $message, $call_status, $sl_mod, $sl_mod_id);
			*/
        }
    }

}
