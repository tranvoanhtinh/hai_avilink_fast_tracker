

var prodl = 0;
var groupn = 0;



/**
 * Load Line Items
 */



/**
 * Insert packing line
 */
function generateUUID() {
    var uuid = '';
    var hexDigits = '0123456789abcdef';

    for (var i = 0; i < 36; i++) {
        if (i === 8 || i === 13 || i === 18 || i === 23) {
            uuid += '-';
        } else if (i === 14) {
            uuid += '4';
        } else if (i === 19) {
            uuid += hexDigits[(Math.random() * 4) | 8];
        } else {
            uuid += hexDigits[Math.floor(Math.random() * 16)];
        }
    }

    return uuid;
}

function packing_InsertpackingLine(tableid, groupid) {


    tablebody = document.createElement("tbody");
    tablebody.id = "packing_body" + prodl;
    document.getElementById(tableid).appendChild(tablebody);


    var x = tablebody.insertRow(-1);
    x.id = 'packing_line' + prodl;

var f1 = x.insertCell(0);
f1.innerHTML = "<input type='text' name='sets_ctn_name[" + prodl + "]' id='sets_ctn_name" + prodl + "' maxlength='50' value='' title='' tabindex='116' value=''>";

var a = x.insertCell(1);
a.innerHTML = "<input type='text' name='packing_l_cm_name[" + prodl + "]' id='packing_l_cm_name" + prodl + "' maxlength='50'  value='' title='' tabindex='116' value=''>";


var b = x.insertCell(2);
b.innerHTML = "<input type='text' name='packing_w_cm_name[" + prodl + "]' id='packing_w_cm_name" + prodl + "' maxlength='50' value='' title='' tabindex='116' value=''>";

var c = x.insertCell(3);
c.innerHTML = "<input type='text' name='packing_h_cm_name[" + prodl + "]' id='packing_h_cm_name" + prodl + "' maxlength='50' value='' title='' tabindex='116' value=''>";

var e = x.insertCell(4);
e.innerHTML = "<input type='text' name='cbm_name[" + prodl + "]' id='cbm_name" + prodl + "' maxlength='50' value='' title='' tabindex='116' value=''>";

var f2 = x.insertCell(5);
f2.innerHTML = "<input type='text' name='load_packing[" + prodl + "]' id='load_packing" + prodl + "' maxlength='50' value='' title='' tabindex='116' value=''>";

var f3 = x.insertCell(6);
f3.innerHTML = "<input type='text' name='fob_packing[" + prodl + "]' id='fob_packing" + prodl + "' maxlength='50' value='' title='' tabindex='116' style='width: 100px; margin-right: 5px;' onchange='format_currency(this)' value=''>";

var f4 = x.insertCell(7);
f4.setAttribute('valign', 'top');
f4.innerHTML = "<textarea name='note_packing[" + prodl + "]' id='note_packing" + prodl + "' title='' tabindex='116' style='width: 250px; height: 40px;'></textarea>";

var a2 = x.insertCell(8);
a2.innerHTML = "<input type='hidden' name='packing_l_inch_name[" + prodl + "]' id='packing_l_inch_name" + prodl + "' maxlength='50'  value='' title='' tabindex='116' value=''>";

var b2 = x.insertCell(9);
b2.innerHTML = "<input type='hidden' name='packing_w_inch_name[" + prodl + "]' id='packing_w_inch_name" + prodl + "' maxlength='50' value='' title='' tabindex='116' value=''>";

var c2 = x.insertCell(10);
c2.innerHTML = "<input type='hidden' name='packing_h_inch_name[" + prodl + "]' id='packing_h_inch_name" + prodl + "' maxlength='50' value='' title='' tabindex='116' value=''>";

var h = x.insertCell(11);
h.innerHTML = "<input type='hidden' name='packing_id[" + prodl + "]' id='packing_id" + prodl + "' value='" + generateUUID() + "'><input type='hidden' name='packing_delete[" + prodl + "]' id='packing_delete" + prodl + "' value='0'><button type='button' id='packing_delete_line" + prodl + "' class='button packing_delete_line' value='" + SUGAR.language.get(module_sugar_grp1, 'LBL_REMOVE_packing_LINE') + "' tabindex='116' onclick='packing_MarkLineDeleted(" + prodl + ",\"packing_\")' style='font-packing: 10px; padding: 10px;'><span class=\"suitepicon suitepicon-action-clear\"></span></button>";


    //QSFieldsArray["EditView_packing_name"+prodl].forceSelection = true;

    var y = tablebody.insertRow(-1);
    y.id = 'packing_note_line' + prodl;

    var h1 = y.insertCell(0);
    h1.colSpan = "5";
    h1.style.color = "rgb(68,68,68)";

    packing_AddAlignedLabels(prodl, 'packing');

    prodl++;

    return prodl - 1;
}








function packing_InsertpackingHeader(tableid){
    tablehead = document.createElement("thead");
    tablehead.id = tableid +"_head";
    tablehead.style.display="none";
    document.getElementById(tableid).appendChild(tablehead);

    var x=tablehead.insertRow(-1);
    x.id='packing_head';

    var f = x.insertCell(0);
    f.style.color = "rgb(68, 68, 68)";
    f.innerHTML = '<p>' + SUGAR.language.get(module_sugar_grp1, 'LBL_SETS_CTN') + '</p>';

    var a = x.insertCell(1);
    a.style.color = "rgb(68, 68, 68)";
    a.innerHTML = '<p>' + SUGAR.language.get(module_sugar_grp1, 'LBL_PACKING_L_CM') + '</p>';

    var b = x.insertCell(2);
    b.style.color = "rgb(68, 68, 68)";
    b.innerHTML = '<p>' + SUGAR.language.get(module_sugar_grp1, 'LBL_PACKING_W_CM') + '</p>';

    var c = x.insertCell(3);
    c.style.color = "rgb(68, 68, 68)";
    c.innerHTML = '<p>' + SUGAR.language.get(module_sugar_grp1, 'LBL_PACKING_H_CM') + '</p>';

    var e = x.insertCell(4);
    e.style.color = "rgb(68, 68, 68)";
    e.innerHTML = '<p>' + SUGAR.language.get(module_sugar_grp1, 'LBL_CBM') + '</p>';

    var f = x.insertCell(5);
    f.style.color = "rgb(68, 68, 68)";
    f.innerHTML = '<p>' + SUGAR.language.get(module_sugar_grp1, 'LBL_LOAD_PACKING') + '</p>';

    var f2 = x.insertCell(6);
    f2.style.color = "rgb(68, 68, 68)";
    f2.innerHTML = '<p>' + SUGAR.language.get(module_sugar_grp1, 'LBL_FOB_PACKING') + '</p>';

    var f3 = x.insertCell(7);
    f3.style.color = "rgb(68, 68, 68)";
    f3.innerHTML = '<p>' + SUGAR.language.get(module_sugar_grp1, 'LBL_NOTE_PACKING') + '</p>';

    var h=x.insertCell(8);
    h.style.color="rgb(68,68,68)";
    h.innerHTML='&nbsp';
    var h=x.insertCell(9);
    h.style.color="rgb(68,68,68)";
    h.innerHTML='&nbsp';
    var h=x.insertCell(10);
    h.style.color="rgb(68,68,68)";
    h.innerHTML='&nbsp';
    var h=x.insertCell(11);
    h.style.color="rgb(68,68,68)";
    h.innerHTML='&nbsp';


    var h=x.insertCell(12);
    h.style.color="rgb(68,68,68)";
    h.innerHTML='&nbsp';


}


/**
 * Insert service Header
 */

function packing_InsertServiceHeader(tableid){

}
/*
 * Insert Group
 */
function insertpacking(count) {
    // Kiểm tra xem nhóm có bị tắt và một nhóm đã tồn tại chưa
    if (!enable_groups && groupn > 0) {
        return;
    }

    // Tạo một hàng mới cho phần đầu bảng nhóm
    var tableheader = document.createElement("thead");
    tableheader.id = "group_head" + groupn;
    document.getElementById('packings').appendChild(tableheader);

    var x = tableheader.insertRow(-1);
    x.id = 'group_head_row' + groupn;

    // Chèn các ô vào hàng đầu của bảng nhóm
    var a = x.insertCell(0);
    a.style.color = "rgb(68,68,68)";
    a.innerHTML = '<hr>';

    // Tạo một bảng cho các mục sản phẩm trong nhóm
    var packingTable = document.createElement("table");
    packingTable.id = "packing_group" + groupn;
    packingTable.className = "packing_group";

    document.getElementById('packings').appendChild(packingTable);

    // Chèn tiêu đề sản phẩm vào bảng sản phẩm
    packing_InsertpackingHeader(packingTable.id);

    // Tạo phần chân cho bảng nhóm
    var tablefooter = document.createElement("tfoot");
    document.getElementById('packings').appendChild(tablefooter);

    // Chèn hàng chân vào bảng nhóm
    var footer_row = tablefooter.insertRow(-1);
    var footer_cell = footer_row.insertCell(0);
    footer_cell.colSpan = "100";

    // Thêm một nút để thêm một dòng sản phẩm mới trong nhóm
    footer_cell.innerHTML = "<input type='button' tabindex='116' value='"+SUGAR.language.get(module_sugar_grp1, 'LBL_CREATE_PACKING_SZIE')+"' class='button add_packing_line'  id='" + packingTable.id + "addpackingLine' onclick='insertpackingLines(\"" + packingTable.id + "\",\"" + groupn + "\",\"" + count + "\")' />";

    // Tăng số nhóm lên
    groupn++;
    return groupn - 1;
}

function insertpackingLines(tableid, groupid, count) {
    for (var i = 0; i < count; i++) {
        packing_InsertpackingLine(tableid, groupid);
    }
}






/**
 * Mark Group Deleted
 */

function packing_MarkGroupDeleted(gn)
{
    document.getElementById('group_body' + gn).style.display = 'none';

    var rows = document.getElementById('group_body' + gn).getElementsByTagName('tbody');

    for (x=0; x < rows.length; x++) {
        var input = rows[x].getElementsByTagName('button');
        for (y=0; y < input.length; y++) {
            if (input[y].id.indexOf('delete_line') != -1) {
                input[y].click();
            }
        }
    }

}

/**
 * Mark line deleted
 */

function packing_MarkLineDeleted(ln, key) {
    // Remove the AJAX code here
    document.getElementById(key + 'body' + ln).style.display = 'none';
    document.getElementById('packing_delete' + ln).value = '1';
    document.getElementById('packing_delete_line' + ln).onclick = '';

    if (checkValidate('EditView', key + 'packing_id' + ln)) {
        removeFromValidate('EditView', key + 'packing_id' + ln);
    }
}

var packing_AddAlignedLabels = function(ln, type) {
    if(typeof type == 'undefined') {
        type = 'packing';
    }
    if(type != 'packing' && type != 'service') {
        console.error('type could be "packing" or "service" only');
    }
    var labels = [];
    $('tr#'+type+'_head td').each(function(i,e){
        if(type=='packing' && $(e).attr('colspan')>1) {
            for(var i=0; i<parseInt($(e).attr('colspan')); i++) {
                if(i==0) {
                    labels.push($(e).html());
                } else {
                    labels.push('');
                }
            }
        } else {
            labels.push($(e).html());
        }
    });
    $('tr#'+type+'_line'+ln+' td').each(function(i,e){
        $(e).prepend('<span class="alignedLabel">'+labels[i]+'</span>');
    });
}


function format_currency(input) {
    // Lấy giá trị nhập vào từ input
    var value = input.value;

    // Kiểm tra xem giá trị nhập vào có chứa phần thập phân không
    if (!isNaN(value) && value.indexOf('.') !== -1) {
        // Nếu có phần thập phân, chuyển đổi giá trị nhập vào thành số và định dạng theo tiêu chuẩn tiền tệ
        var formattedValue = parseFloat(value).toLocaleString('en-US', { style: 'decimal', maximumFractionDigits: 2 });
        // Cập nhật giá trị của input với giá trị đã được định dạng
        input.value = formattedValue;
    } else if (!isNaN(value) && value.indexOf('.') === -1) {
        // Nếu không có phần thập phân, định dạng giá trị là số nguyên
        var formattedValue = parseFloat(value).toLocaleString('en-US', { style: 'decimal' });
        // Cập nhật giá trị của input với giá trị đã được định dạng
        input.value = formattedValue+'.00';
    }
}



