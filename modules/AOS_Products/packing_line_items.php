<?php
/**
 * Advanced OpenSales, Advanced, robust set of sales modules.
 * @package Advanced OpenSales for SugarCRM
 * @copyright SalesAgility Ltd http://www.salesagility.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU AFFERO GENERAL PUBLIC LICENSE
 * along with this program; if not, see http://www.gnu.org/licenses
 * or write to the Free Software Foundation,Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA 02110-1301  USA
 *
 * @author SalesAgility <info@salesagility.com>
 */

function display_packing_lines($focus, $field, $value, $view)
{
    global $sugar_config, $locale, $app_list_strings, $mod_strings;

$sql = "SELECT aos_packing.*
        FROM aos_packing
        JOIN aos_products_aos_packing_1_c ON aos_packing.id = aos_products_aos_packing_1_c.aos_products_aos_packing_1aos_packing_idb 
        JOIN aos_products ON aos_products_aos_packing_1_c.aos_products_aos_packing_1aos_products_ida = aos_products.id 
        WHERE aos_products.id = '".$focus->id."' AND aos_packing.deleted = 0
        ORDER BY aos_packing.cbm DESC";




    $result = $focus->db->query($sql);
    if ($result){
        $count = mysqli_num_rows($result);
    }
    if ($count ==0) {
        $count = 1;
    }

    $html = '';

    if ($view == 'EditView') {

        $html .= '<script src="modules/AOS_Products/packing_line_items.js"></script>';
        if (file_exists('custom/modules/AOS_Products/packing_line_items.js')) {
            $html .= '<script src="custom/modules/AOS_Products/packing_line_items.js"></script>';
        }
        $html .= '<script language="javascript">var sig_digits = '.$locale->getPrecision().';';
        $html .= 'var module_sugar_grp1 = "'.$focus->module_dir.'";';

        $html .= '</script>';

        $html .= "<table border='0' cellspacing='4' id='packings' width='120%'></table>";


        $html .= "<script>insertpacking(1)</script>";
        if($focus->id == null){
            $html .= "<script>";
            $html .= "    insertpackingLines('packing_group0', '0', '5');"; // Chạy insertpackingLines trước
            $html .= "</script>";
        }
        $i = 0;

        while ($row = $result->fetch_assoc()) {
            if ($row['sets_ctn'] != '' || 
                customFormat($row['packing_l_cm']) != '0.0' || 
                customFormat($row['packing_w_cm']) != '0.0' || 
                customFormat($row['packing_h_cm']) != '0.0' || 
                customFormat($row['cbm']) != '0.00' || 
                customFormat($row['load_packing']) != '0.0' || 
                $row['note_packing'] != '') {

            $html .= "<script>";
            $html .= "    insertpackingLines('packing_group0', '0', '1');"; // Chạy insertpackingLines trước
            $html .= "document.addEventListener('DOMContentLoaded', function() {";
            $html .= "    document.getElementById('packing_id' + {$i}).value = '{$row['id']}';";
            $html .= "    document.getElementById('packing_l_cm_name' + {$i}).value = '" . customFormat($row['packing_l_cm']) . "';";
            $html .= "    document.getElementById('packing_w_cm_name' + {$i}).value = '" . customFormat($row['packing_w_cm']) . "';";
            $html .= "    document.getElementById('packing_h_cm_name' + {$i}).value = '" . customFormat($row['packing_h_cm']) . "';";

            $html .= "    document.getElementById('cbm_name' + {$i}).value = '" . customFormat2($row['cbm']) . "';";

            $html .= "    document.getElementById('sets_ctn_name' + {$i}).value = '{$row['sets_ctn']}';";
            $html .= "    document.getElementById('load_packing' + {$i}).value = '{$row['load_packing']}';";
            $html .= "    document.getElementById('fob_packing' + {$i}).value = '" . customFormat2($row['fob_packing']) . "';";

            $html .= "    document.getElementById('note_packing' + {$i}).value = '{$row['note_packing']}';";
            $html .= "});";
            $html .= "</script>";
            $i++;
        }
        }
            if($focus->id != null){
            $y  =  5 - $i;
            $html .= "<script>";
            $html .= "    insertpackingLines('packing_group0', '0', '".$y."');"; // Chạy insertSizeLines trước
            $html .= "</script>";
            }

    }


    else {
        if ($view =='DetailView') {



            $html .= '<style>';

            $html .= '#packing_table_detail th {';
            $html .= '    text-align: center; width:7%';

            $html .= '}';

            $html .= 'th, td { border: 1px solid #dddddd; text-align: left; padding: 8px; }';
            $html .= 'tr:nth-child(even) { background-color: #f2f2f2; }';
            $html .= '</style>';

            $html .= '<table id="packing_table_detail" style="width: 100%; background-color: #f5f5f5;">';
            $html .= '<tr>';
            $html .= '<th>'.$mod_strings['LBL_SETS_CTN'].'</th>';
            $html .= '<th>'.$mod_strings['LBL_PACKING_L_CM'].'</th>';
            $html .= '<th>'.$mod_strings['LBL_PACKING_W_CM'].'</th>';
            $html .= '<th>'.$mod_strings['LBL_PACKING_H_CM'].'</th>';
            $html .= '<th>'.$mod_strings['LBL_CBM'].'</th>';

            $html .= '<th>'.$mod_strings['LBL_LOAD_PACKING'].'</th>';
            $html .= '<th>'.$mod_strings['LBL_FOB_PACKING'].'</th>';
            $html .= '<th style ="width:250px">'.$mod_strings['LBL_NOTE_PACKING'].'</th>';



            $html .= '</tr>';

            $stt = 1;

            if ($result){
                while ($row = $result->fetch_assoc()) {
                    $html .= '<tr>';


                    if ($row['sets_ctn'] != '' || 
                    customFormat($row['packing_l_cm']) != '0.0' || 
                    customFormat($row['packing_w_cm']) != '0.0' || 
                    customFormat($row['packing_h_cm']) != '0.0' || 
                    customFormat($row['cbm']) != '0.00' || 
                    customFormat($row['load_packing']) != '0.0' || 
                    $row['note_packing'] != ''){

                    $html .= '<td>' . $row['sets_ctn'] . '</td>';
                    $html .= '<td>' . customFormat($row['packing_l_cm']) . '</td>';
                    $html .= '<td>' . customFormat($row['packing_w_cm']) . '</td>';
                    $html .= '<td>' . customFormat($row['packing_h_cm']) . '</td>';
                    $html .= '<td>' . customFormat2($row['cbm']) . '</td>';

                    $html .= '<td>' . $row['load_packing'] . '</td>';
                    $html .= '<td>' . currency_format_number($row['fob_packing']) . '</td>';
                    $html .= '<td>' . $row['note_packing'] . '</td>';
                    }else {
            
                    }

       
                    $html .= '</tr>';
                    $stt++;
                }
            }

            $html .= '</table>';

        }

    }
    return $html;
}


function customFormat2($number) {
    // Định dạng số với một số thập phân
    return number_format((float) $number, 2);
}