

var number = 0;


var c = 1; // Khởi tạo biến đếm ở giá trị 1

function deleteImage(id, name) {
    // Tìm container của hình ảnh dựa trên id
    var containerElement = document.getElementById('container_' + id);

    if (containerElement) {
        // Tìm parent có class là 'col-md-3' và xóa nó
        var colElement = findAncestor(containerElement, 'col-md-3');
        if (colElement) {
            colElement.parentNode.removeChild(colElement);
        }

        // Tạo input hidden mới
        var hiddenInput = document.createElement('input');
        hiddenInput.type = 'hidden';
        hiddenInput.name = 'deleted_' + c;
        hiddenInput.value = id;
        hiddenInput.setAttribute('deleted', '1');

        // Thêm input hidden mới vào một nơi cụ thể trong tài liệu (ví dụ: <div>)
        var formElement = document.getElementById('images_deleted');
        if (formElement) {
            formElement.appendChild(hiddenInput);

            // Tăng giá trị của biến đếm để đảm bảo tên sẽ theo thứ tự
            c++;
        }
    }
}

// Hàm tìm parent có class là className
function findAncestor(element, className) {
    while ((element = element.parentElement) && !element.classList.contains(className));
    return element;
}

// Sử dụng closure để bảo vệ giá trị của i
(function () {
    var originalDeleteImage = deleteImage;
    deleteImage = function (id, name) {
        originalDeleteImage.apply(this, arguments);
    };
})();

function handleFileSelect() {
    var container = document.getElementById("fileInputsContainer");
    var input = document.getElementById("extra_image");
    var files = input.files;

    // Clear previous inputs
    container.innerHTML = "";

    for (var i = 0; i < files.length; i++) {
        // Create a new input of type "hidden"
        var newInput = document.createElement("input");
        newInput.type = "hidden";
        newInput.name = "extra_images_" + (i);
        newInput.value = files[i].name;

        // Create a new image element to display the selected image
        var newImg = document.createElement("img");
        
        // Create a closure to ensure newImg reference is correct for each iteration
        (function(img) {
            var reader = new FileReader();
            reader.onload = function(event) {
                img.src = event.target.result;
            };
            reader.readAsDataURL(files[i]);
        })(newImg);
        
        newImg.className = "selected-image";
        newImg.style.width = "90px"; // Set width to 90px
        newImg.style.height = "67px"; // Set height to 67px
        newImg.style.margin = "5px"; // Add 5px margin

        // Create a new paragraph to display the file name
        var newParagraph = document.createElement("p");
        if (files[i].name.length > 14) {
            newParagraph.textContent = files[i].name.substring(0, 14);
        } else {
            newParagraph.textContent = files[i].name;
        }

        // Create a new delete button
        var deleteButton = document.createElement("button");
        deleteButton.textContent = "x";
        deleteButton.type = "button";
        deleteButton.className = "button delete-button";
        deleteButton.onclick = function () {
            // Remove the parent div when delete button is clicked
            this.parentNode.remove();
            updateInputNames(); // Call a function to update input names
        };

        // Create a new div to contain the input, image, paragraph, and delete button
        var newDiv = document.createElement("div");
        newDiv.className = "col-md-3";

        // Append the input, image, paragraph, delete button to the div
        newDiv.appendChild(newInput);
        newDiv.appendChild(newImg);
        newDiv.appendChild(newParagraph);
        newDiv.appendChild(deleteButton);

        // Append the div to the container
        container.appendChild(newDiv);
    }
}



function showImagePreview(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        document.getElementById('old_attachment').style.display = 'block';
        reader.onload = function(e) {
            var imagePreview = document.getElementById("imagePreview");
            imagePreview.src = e.target.result;
        };

        reader.readAsDataURL(input.files[0]);
    }
}



function setup_image(){
   var extraImagesInput = document.querySelector('input[name="extra_images_0"]');
    if (!extraImagesInput) {
    var input = document.getElementById("extra_image");
    input.value = "";
    }
}
