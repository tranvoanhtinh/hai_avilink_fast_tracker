

var lineno;
var prodln = 0;
var servln = 0;
var groupn = 0;
var group_ids = {};


/**
 * Load Line Items
 */



/**
 * Insert size line
 */
function generateUUID() {
    var uuid = '';
    var hexDigits = '0123456789abcdef';

    for (var i = 0; i < 36; i++) {
        if (i === 8 || i === 13 || i === 18 || i === 23) {
            uuid += '-';
        } else if (i === 14) {
            uuid += '4';
        } else if (i === 19) {
            uuid += hexDigits[(Math.random() * 4) | 8];
        } else {
            uuid += hexDigits[Math.floor(Math.random() * 16)];
        }
    }

    return uuid;
}

function size_InsertsizeLine(tableid, groupid) {


    tablebody = document.createElement("tbody");
    tablebody.id = "size_body" + prodln;
    document.getElementById(tableid).appendChild(tablebody);


    var x = tablebody.insertRow(-1);
    x.id = 'size_line' + prodln;


var a = x.insertCell(0);
a.innerHTML = "<input type='text' name='size_l_cm_name[" + prodln + "]' id='size_l_cm_name" + prodln + "' maxlength='50' value='' title='' tabindex='116' style='width: 94%' value=''>";
a.colSpan = '2';

var b = x.insertCell(1);
b.innerHTML = "<input type='text' name='size_w_cm_name[" + prodln + "]' id='size_w_cm_name" + prodln + "' maxlength='50' value='' title='' tabindex='116' style='width: 94%' value=''>";

var c = x.insertCell(2);
c.innerHTML = "<input type='text' name='size_h_cm_name[" + prodln + "]' id='size_h_cm_name" + prodln + "' maxlength='50' value='' title='' tabindex='116' style='width: 94%' value=''>";

var e = x.insertCell(3);
e.innerHTML = "<input type='text' name='handle_name[" + prodln + "]' id='handle_name" + prodln + "' maxlength='50' value='' title='' tabindex='116' style='width: 94%' value=''>";

var f = x.insertCell(4);
f.innerHTML = "<input type='text' name='bottom_length_name[" + prodln + "]' id='bottom_length_name" + prodln + "' maxlength='50' value='' title='' tabindex='116' style='width: 94%' value=''>";

var g = x.insertCell(5);
g.innerHTML = "<input type='text' name='bottom_width_name[" + prodln + "]' id='bottom_width_name" + prodln + "' maxlength='50' value='' title='' tabindex='116' style='width: 94%' value=''>";

var d = x.insertCell(6);
d.innerHTML = "<input type='hidden' name='size_w_h_cm_name[" + prodln + "]' id='size_w_h_cm_name" + prodln + "' maxlength='50' value='' title='' tabindex='116' style='width: 94%' value=''>";

var h = x.insertCell(7);
h.innerHTML = "<input type='hidden' name='size_id[" + prodln + "]' id='size_id" + prodln + "' value='" + generateUUID() + "'><input type='hidden' name='size_delete[" + prodln + "]' id='size_delete" + prodln + "' value='0'><button type='button' id='size_delete_line" + prodln + "' class='button size_delete_line' value='" + SUGAR.language.get(module_sugar_grp1, 'LBL_REMOVE_size_LINE') + "' tabindex='116' onclick='Size_MarkLineDeleted(" + prodln + ",\"size_\")' style='font-size: 10px; padding: 10px; width: 94%'><span class=\"suitepicon suitepicon-action-clear\"></span></button>";


    //QSFieldsArray["EditView_size_name"+prodln].forceSelection = true;

    var y = tablebody.insertRow(-1);
    y.id = 'size_note_line' + prodln;

    var h1 = y.insertCell(0);
    h1.colSpan = "5";
    h1.style.color = "rgb(68,68,68)";

    size_AddAlignedLabels(prodln, 'size');

    prodln++;

    return prodln - 1;
}








function size_InsertsizeHeader(tableid){
    tablehead = document.createElement("thead");
    tablehead.id = tableid +"_head";
    tablehead.style.display="none";
    document.getElementById(tableid).appendChild(tablehead);

    var x=tablehead.insertRow(-1);
    x.id='size_head';



var a = x.insertCell(0);
a.style.color = "rgb(68, 68, 68)";
a.innerHTML = '<p>' + SUGAR.language.get(module_sugar_grp1, 'LBL_SIZE_L_CM') + '</p>';

var b = x.insertCell(1);
b.style.color = "rgb(68, 68, 68)";
b.innerHTML = '<p>' + SUGAR.language.get(module_sugar_grp1, 'LBL_SIZE_W_CM') + '</p>';

var c = x.insertCell(2);
c.style.color = "rgb(68, 68, 68)";
c.innerHTML = '<p>' + SUGAR.language.get(module_sugar_grp1, 'LBL_SIZE_H_CM') + '</p>';

var e = x.insertCell(3);
e.style.color = "rgb(68, 68, 68)";
e.innerHTML = '<p>' + SUGAR.language.get(module_sugar_grp1, 'LBL_HANDLE') + '</p>';

var f = x.insertCell(4);
f.style.color = "rgb(68, 68, 68)";
f.innerHTML = '<p>' + SUGAR.language.get(module_sugar_grp1, 'LBL_BOTTOM_LENGTH') + '</p>';

var g = x.insertCell(5);
g.style.color = "rgb(68, 68, 68)";
g.innerHTML = '<p>' + SUGAR.language.get(module_sugar_grp1, 'LBL_BOTTOM_WIDTH') + '</p>';




    var h=x.insertCell(6);
    h.style.color="rgb(68,68,68)";
    h.innerHTML='&nbsp';


    var h=x.insertCell(7);
    h.style.color="rgb(68,68,68)";
    h.innerHTML='&nbsp';


}


/**
 * Insert service Header
 */

function size_InsertServiceHeader(tableid){

}
/*
 * Insert Group
 */
function insertSize(count) {
    // Kiểm tra xem nhóm có bị tắt và một nhóm đã tồn tại chưa
    if (!enable_groups && groupn > 0) {
        return;
    }

    // Tạo một hàng mới cho phần đầu bảng nhóm
    var tableheader = document.createElement("thead");
    tableheader.id = "group_head" + groupn;
    document.getElementById('sizes').appendChild(tableheader);

    var x = tableheader.insertRow(-1);
    x.id = 'group_head_row' + groupn;

    // Chèn các ô vào hàng đầu của bảng nhóm
    var a = x.insertCell(0);
    a.style.color = "rgb(68,68,68)";
    a.innerHTML = '<hr>';

    // Tạo một bảng cho các mục sản phẩm trong nhóm
    var sizeTable = document.createElement("table");
    sizeTable.id = "size_group" + groupn;
    sizeTable.className = "size_group";

    document.getElementById('sizes').appendChild(sizeTable);

    // Chèn tiêu đề sản phẩm vào bảng sản phẩm
    size_InsertsizeHeader(sizeTable.id);

    // Tạo phần chân cho bảng nhóm
    var tablefooter = document.createElement("tfoot");
    document.getElementById('sizes').appendChild(tablefooter);

    // Chèn hàng chân vào bảng nhóm
    var footer_row = tablefooter.insertRow(-1);
    var footer_cell = footer_row.insertCell(0);
    footer_cell.colSpan = "100";

    // Thêm một nút để thêm một dòng sản phẩm mới trong nhóm
    footer_cell.innerHTML = "<input type='button' tabindex='116' value='"+SUGAR.language.get(module_sugar_grp1, 'LBL_CREATE_SZIE')+"' class='button add_size_line'  id='" + sizeTable.id + "addsizeLine' onclick='insertSizeLines(\"" + sizeTable.id + "\",\"" + groupn + "\",\"" + count + "\")' />";

    // Tăng số nhóm lên
    groupn++;
    return groupn - 1;
}

    function insertSizeLines(tableid, groupid, count) {
        for (var i = 0; i < count; i++) {
            size_InsertsizeLine(tableid, groupid);
        }
    }






/**
 * Mark Group Deleted
 */

function size_MarkGroupDeleted(gn)
{
    document.getElementById('group_body' + gn).style.display = 'none';

    var rows = document.getElementById('group_body' + gn).getElementsByTagName('tbody');

    for (x=0; x < rows.length; x++) {
        var input = rows[x].getElementsByTagName('button');
        for (y=0; y < input.length; y++) {
            if (input[y].id.indexOf('delete_line') != -1) {
                input[y].click();
            }
        }
    }

}

/**
 * Mark line deleted
 */

function Size_MarkLineDeleted(ln, key) {
    // Remove the AJAX code here
    document.getElementById(key + 'body' + ln).style.display = 'none';
    document.getElementById('size_delete' + ln).value = '1';
    document.getElementById('size_delete_line' + ln).onclick = '';

    if (checkValidate('EditView', key + 'size_id' + ln)) {
        removeFromValidate('EditView', key + 'size_id' + ln);
    }
}


var size_AddAlignedLabels = function(ln, type) {
    if(typeof type == 'undefined') {
        type = 'size';
    }
    if(type != 'size' && type != 'service') {
        console.error('type could be "size" or "service" only');
    }
    var labels = [];
    $('tr#'+type+'_head td').each(function(i,e){
        if(type=='size' && $(e).attr('colspan')>1) {
            for(var i=0; i<parseInt($(e).attr('colspan')); i++) {
                if(i==0) {
                    labels.push($(e).html());
                } else {
                    labels.push('');
                }
            }
        } else {
            labels.push($(e).html());
        }
    });
    $('tr#'+type+'_line'+ln+' td').each(function(i,e){
        $(e).prepend('<span class="alignedLabel">'+labels[i]+'</span>');
    });
}


