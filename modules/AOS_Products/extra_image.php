<?php
/**
 * Advanced OpenSales, Advanced, robust set of sales modules.
 * @package Advanced OpenSales for SugarCRM
 * @copyright SalesAgility Ltd http://www.salesagility.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU AFFERO GENERAL PUBLIC LICENSE
 * along with this program; if not, see http://www.gnu.org/licenses
 * or write to the Free Software Foundation,Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA 02110-1301  USA
 *
 * @author SalesAgility <info@salesagility.com>
 */

function display_extra_image($focus, $field, $value, $view)
{
    global $sugar_config, $locale, $app_list_strings, $mod_strings;
    $html = '';

    if ($view == 'EditView') {
        $html .= '<link rel="stylesheet" type="text/css" href="modules/AOS_Products/extra_image.css">';
        $html .= '<script src="modules/AOS_Products/extra_image.js"></script>';

        $sql = "SELECT documents.id, documents.document_name
            FROM documents
            JOIN aos_products_documents_1_c ON  aos_products_documents_1_c.aos_products_documents_1documents_idb = documents.id
            WHERE  aos_products_documents_1_c.aos_products_documents_1aos_products_ida  = '" . $focus->id . "'
            AND documents.deleted = 0";

        $result = $focus->db->query($sql);

if ($result && $result->num_rows > 0) {
    $i = 0;

    while ($row = $result->fetch_assoc()) {
        if ($i % 4 == 0) {
            $html .= '<div class="row">';
        }

        $imageId = $row['id'];
        $name = $row['document_name'];

        $html .= '<style>';
        $html .= '.m-5 {';
        $html .= '    margin: 5px;'; // Thêm margin 5px cho hình ảnh
        $html .= '}';
        $html .= '</style>';

        $html .= '<div class="col-md-3">'; // Each image will take 3 columns on medium devices
        $html .= '<div class="image-container" id="container_' . $imageId . '">';
        $html .= '<img id="' . $imageId . '" name="' . $name . '" src="upload/' . $imageId . '_' . $name . '" class="d-block m-5" width="90px" height="67px" alt="Ảnh ' . $imageId . '">';
        $html .= '</div>';
        $html .= '<input type="button" id="inputx_' . $imageId . '" class="delete-button" value="x" onclick="deleteImage(\'' . $imageId . '\', \'' . $name . '\')">';
        $html .= '</div>';

        $i++;

        if ($i % 4 == 0) {
            $html .= '</div>'; // Đóng hàng sau mỗi 4 hình ảnh
        }
    }

    // Đóng hàng cuối cùng nếu không đủ 4 hình ảnh để đóng hàng
    if ($i % 4 != 0) {
        $html .= '</div>';
    }
}





        $html .= '<div id = "images_deleted">';
        $html .= '</div>';
       
$html .= '<input type="file" id="extra_image" name="extra_images[]" multiple accept="image/*" onchange="handleFileSelect()" onclick="setup_image()">';
        $html .= '<div id="fileInputsContainer"></div>';
    }

    if ($view == 'DetailView') {
        $html .= '<link rel="stylesheet" type="text/css" href="modules/AOS_Products/extra_image.css">';
        $html .= '<script src="modules/AOS_Products/extra_image.js"></script>';
        $sql = "SELECT documents.id, documents.document_name
            FROM documents
            JOIN  aos_products_documents_1_c ON  aos_products_documents_1_c.aos_products_documents_1documents_idb = documents.id
            WHERE  aos_products_documents_1_c.aos_products_documents_1aos_products_ida  = '" . $focus->id . "'
            AND documents.deleted = 0";

        $result = $focus->db->query($sql);
            $html .= '<style>
            .image {
                margin-bottom: 10px;
                margin-left: 5px;
                float: left; /* Canh trái các hình ảnh */
            }
            .div_extra_img {
                width: 500px;

            }
            #imageContainer {
                display: flex; /* Hiển thị các phần tử con trên cùng một hàng */
                flex-wrap: nowrap; /* Không cho các phần tử con xuống dòng */
                overflow-x: auto; /* Cho phép cuộn ngang khi các hình ảnh tràn ra khỏi container */
            }

            </style>';

            $html .= '<div class="div_extra_img" id="imageContainer">';

            if ($result && $result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    $imageId = $row['id'];
                    $name= $row['document_name'];
                    $html .= '<img class="image" src="upload/' . $imageId . '_' . $name . '" width="90px" height="67px" alt="Ảnh ' . $imageId . '">';
                }
            } else {
                // Xử lý trường hợp không có ảnh
                $html .= '<p>No images found.</p>';
            }

            $html .= '</div>'; // Close container



    }
    return $html;
}
?>
