<?php
/**
 * Products, Quotations & Invoices modules.
 * Extensions to SugarCRM
 * @package Advanced OpenSales for SugarCRM
 * @subpackage Products
 * @copyright SalesAgility Ltd http://www.salesagility.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU AFFERO GENERAL PUBLIC LICENSE
 * along with this program; if not, see http://www.gnu.org/licenses
 * or write to the Free Software Foundation,Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA 02110-1301  USA
 *
 * @author SalesAgility Ltd <support@salesagility.com>
 */

/**
 * THIS CLASS IS FOR DEVELOPERS TO MAKE CUSTOMIZATIONS IN
 */
require_once('modules/AOS_Products/AOS_Products_sugar.php');
class AOS_Products extends AOS_Products_sugar
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @deprecated deprecated since version 7.6, PHP4 Style Constructors are deprecated and will be remove in 7.8, please update your code, use __construct instead
     */
    public function AOS_Products()
    {
        $deprecatedMessage = 'PHP4 Style Constructors are deprecated and will be remove in 7.8, please update your code';
        if (isset($GLOBALS['log'])) {
            $GLOBALS['log']->deprecated($deprecatedMessage);
        } else {
            trigger_error($deprecatedMessage, E_USER_DEPRECATED);
        }
        self::__construct();
    }

    public function getGUID()
    {
        if (function_exists('com_create_guid')) {
            return com_create_guid();
        } else {
            mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid = substr($charid, 0, 8).$hyphen
                .substr($charid, 8, 4).$hyphen
                .substr($charid, 12, 4).$hyphen
                .substr($charid, 16, 4).$hyphen
                .substr($charid, 20, 12);
            return $uuid;
        }
    }


    public function getCustomersPurchasedProductsQuery()
    {
        $query = "
 			SELECT * FROM (
 				SELECT
					aos_quotes.*,
					accounts.id AS account_id,
					accounts.name AS billing_account,

					opportunity_id AS opportunity,
					billing_contact_id AS billing_contact,
					'' AS created_by_name,
					'' AS modified_by_name,
					'' AS assigned_user_name
				FROM
					aos_products

				JOIN aos_products_quotes ON aos_products_quotes.product_id = aos_products.id AND aos_products.id = '{$this->id}' AND aos_products_quotes.deleted = 0 AND aos_products.deleted = 0
				JOIN aos_quotes ON aos_quotes.id = aos_products_quotes.parent_id AND aos_quotes.stage = 'Closed Accepted' AND aos_quotes.deleted = 0
				JOIN accounts ON accounts.id = aos_quotes.billing_account_id -- AND accounts.deleted = 0

				GROUP BY accounts.id
			) AS aos_quotes

		";
        return $query;
    }

    public function generateUUIDv4() {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0x0fff) | 0x4000,
            mt_rand(0, 0x3fff) | 0x8000,
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }
    public function save($check_notify=false)
    {
        global $sugar_config,$mod_strings;

        if (isset($_POST['deleteAttachment']) && $_POST['deleteAttachment']=='1') {
         

            $img_deleted = $this->product_image;
            $image_path = "upload/$img_deleted";
                if (file_exists($image_path)) {
                    unlink($image_path);
                }
           $this->product_image = '';
            
        }

        require_once('include/upload_file.php');
        $GLOBALS['log']->debug('UPLOADING PRODUCT IMAGE');
        $upload_file = new UploadFile('uploadfile');

        // hình ảnh chính 
        if (isset($_FILES['uploadimage']['tmp_name'])&&$_FILES['uploadimage']['tmp_name']!="") {
            if ($_FILES['uploadimage']['size'] > $sugar_config['upload_maxsize']) {
                die($mod_strings['LBL_IMAGE_UPLOAD_FAIL'].$sugar_config['upload_maxsize']);
            } else {
                $prefix_image = $this->getGUID().'_';
                $this->product_image=$prefix_image.$_FILES['uploadimage']['name'];
                move_uploaded_file($_FILES['uploadimage']['tmp_name'], $sugar_config['upload_dir'].$prefix_image.$_FILES['uploadimage']['name']);

                $sourceImagePath = "upload/{$this->product_image}";

                // Kiểm tra kích thước của hình ảnh
                $fileSize = filesize($sourceImagePath);
                $maxFileSize = 1024 * 1024; // 1MB

                if ($fileSize > $maxFileSize) {
                    // Hình ảnh vượt quá 1MB, tiến hành giảm kích thước

                    // Tạo một ảnh mới từ hình ảnh gốc
                    $sourceImage = imagecreatefromjpeg($sourceImagePath);

                    // Lấy kích thước của ảnh gốc
                    $sourceWidth = imagesx($sourceImage);
                    $sourceHeight = imagesy($sourceImage);

                    // Thiết lập kích thước mới cho ảnh đích (ví dụ: giảm 50%)
                    $newWidth = $sourceWidth * 0.5;
                    $newHeight = $sourceHeight * 0.5;

                    // Tạo một ảnh mới với kích thước mới
                    $newImage = imagecreatetruecolor($newWidth, $newHeight);

                    // Thay đổi kích thước của ảnh gốc thành ảnh mới
                    imagecopyresampled($newImage, $sourceImage, 0, 0, 0, 0, $newWidth, $newHeight, $sourceWidth, $sourceHeight);

                    // Lưu ảnh mới với chất lượng tốt hơn, ghi đè lên tệp ảnh gốc
                    imagejpeg($newImage, $sourceImagePath, 100); // 70 là chất lượng hình ảnh (giữa 0 và 100)

                    // Giải phóng bộ nhớ được sử dụng
                    imagedestroy($sourceImage);
                    imagedestroy($newImage);
                }
            }
        }
         // Hình ảnh phụ:
            $maxImages = 20;
            $id_array = [];

            for ($i = 0; $i < $maxImages; $i++) {
                $inputName = 'extra_image' . $i;



                if (isset($_FILES['extra_images']['tmp_name'][$i]) && $_FILES['extra_images']['tmp_name'][$i] != "") {
                    if ($_FILES['extra_images']['size'][$i] > $sugar_config['upload_maxsize']) {
                        die($mod_strings['LBL_IMAGE_UPLOAD_FAIL'] . $sugar_config['upload_maxsize']);
                    } else {
                        // Generate GUID
                        $prefix_image = $this->getGUID() . '_';
                        
                        move_uploaded_file($_FILES['extra_images']['tmp_name'][$i], $sugar_config['upload_dir'] . $prefix_image . $_FILES['extra_images']['name'][$i]);
                        $id_array[] = $prefix_image . $_FILES['extra_images']['name'][$i];


                        $maxFileSize = 1024 * 1024; // 1MB

                        $sourceImagePath = "upload/{$id_array[$i]}";
                        $fileSize = filesize($sourceImagePath);

                        if ($fileSize > $maxFileSize) {
    
                        $sourceImage = imagecreatefromjpeg($sourceImagePath);
 
                        $sourceWidth = imagesx($sourceImage);
                        $sourceHeight = imagesy($sourceImage);

                        // Thiết lập kích thước mới cho ảnh đích (ví dụ: giảm 50%)
                        $newWidth = $sourceWidth * 0.5;
                        $newHeight = $sourceHeight * 0.5;

                        // Tạo một ảnh mới với kích thước mới
                        $newImage = imagecreatetruecolor($newWidth, $newHeight);

                        // Thay đổi kích thước của ảnh gốc thành ảnh mới
                        imagecopyresampled($newImage, $sourceImage, 0, 0, 0, 0, $newWidth, $newHeight, $sourceWidth, $sourceHeight);

                        // Lưu ảnh mới với chất lượng tốt hơn, ghi đè lên tệp ảnh gốc
                        imagejpeg($newImage, $sourceImagePath, 100); // 90 là chất lượng hình ảnh (giữa 0 và 100)

                        // Giải phóng bộ nhớ được sử dụng
                        imagedestroy($sourceImage);
                        imagedestroy($newImage);
                        }

                    }
                }
            }


            

        require_once('modules/AOS_Products_Quotes/AOS_Utils.php');
        require_once 'include/utils/get_voucher_code.php';

        if($this->part_number ==''){
            $code_date = date("d/m/Y");
            $this->part_number = get_voucher_code('aos_products', $code_date);
        }

        perform_aos_save($this);

        if($this->name =='' || $this->name == null){

        $this->name  = $this->part_number;
        }
        if($this->type_currency == 1){
            $this->vnd = $this->price;
            $this->usd = $this->price / $this->exchange_rate;
        }else{
            $this->usd = $this->price;
            $this->vnd = $this->price * $this->exchange_rate;
        }
        $this->status = 0;
        $return_id = parent::save($check_notify);
        require_once('modules/AOS_Size/AOS_Size.php');

        $productQuoteGroup = new AOS_Size();
        $productQuoteGroup->save_size($_POST, $this, 'size_');

        
        require_once('modules/Documents/DocumentSoap.php');
        $Notes = new DocumentSoap ();
        $Notes->save_images_photos($_POST, $this, $id_array);
        return $return_id;
    }

public function mark_deleted($id) //xử lý xóa 
{
    global $db;
    $sql = "SELECT id,product_image FROM aos_products WHERE id = '".$this->id."'";
    $result = $db->query($sql); // Đảm bảo $db được khởi tạo trước đó

    if ($result && $result->num_rows > 0) {
        $row = $result->fetch_assoc();
        $name_image = $row['product_image'];
        
        // Xóa hình ảnh từ thư mục "upload"
        $image_path = "upload/$name_image";
        if (file_exists($image_path)) {
            unlink($image_path);
        }
        $sql2 = "SELECT aos_products_documents_1documents_idb FROM `aos_products_documents_1_c` WHERE aos_products_documents_1aos_products_ida = '".$row['id']."'";

        $result2 = $db->query($sql2); // Đảm bảo $db được khởi tạo trước đó 

        while($row2 = $db->fetchByAssoc($result2)){ 
            $id_documents = $row2['aos_products_documents_1documents_idb'];

            $sql3 = "SELECT * FROM `documents` WHERE id = '".$id_documents."'";
            $result3 = $db->query($sql3);

            require_once('modules/Documents/Document.php');
            $row3 = $result3->fetch_assoc(); // Thêm dấu chấm phẩy vào cuối dòng

            $doc = new Document();
            $doc->mark_deleted($row3['id']);

            $directory = "upload/"; // Đường dẫn đến thư mục chứa hình ảnh
            $keyword = $row3['id'];

            $files = scandir($directory);
            foreach ($files as $file) {
                // Bỏ qua tên các tệp "." và ".."
                if ($file != "." && $file != "..") {
                    if (strpos($file, $keyword) !== false) {
                        unlink($directory . $file);
                    }
                }
            }

            $sql_update = "UPDATE `aos_products_documents_1_c` SET deleted = 1 WHERE aos_products_documents_1aos_products_ida = '".$row['id']."'";
            $db->query($sql_update); 
        }   
    }
    require_once('modules/AOS_Packing/AOS_Packing.php');
    $Packing = new AOS_Packing();

    $q = "SELECT `aos_products_aos_packing_1aos_packing_idb`as id_packing FROM `aos_products_aos_packing_1_c`  WHERE `aos_products_aos_packing_1aos_products_ida` = '".$id."'";
    $result_q = $db->query($q);
   while($row_q = $db->fetchByAssoc($result_q)){
    $id_packing = $row_q['id_packing'];



    $q2 = "SELECT id FROM aos_packing WHERE id  = '".$id_packing."'";
    $result_q2 = $db->query($q2);
    if($row_q2 = $result_q2->fetch_assoc()){
           $Packing->mark_deleted($row_q2['id']);
       }
    
    }
    $q3 = "UPDATE `aos_products_aos_packing_1_c` SET `deleted` = 1 WHERE `aos_products_aos_packing_1aos_products_ida` = '".$id."'";
    $result_q3 = $db->query($q3);
    parent::mark_deleted($id);
    }
}
