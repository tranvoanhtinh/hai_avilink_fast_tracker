<?php
    /**
     * Advanced OpenSales, Advanced, robust set of sales modules.
     * @package Advanced OpenSales for SugarCRM
     * @copyright SalesAgility Ltd http://www.salesagility.com
     *
     * This program is free software; you can redistribute it and/or modify
     * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
     * the Free Software Foundation; either version 3 of the License, or
     * (at your option) any later version.
     *
     * This program is distributed in the hope that it will be useful,
     * but WITHOUT ANY WARRANTY; without even the implied warranty of
     * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     * GNU General Public License for more details.
     *
     * You should have received a copy of the GNU AFFERO GENERAL PUBLIC LICENSE
     * along with this program; if not, see http://www.gnu.org/licenses
     * or write to the Free Software Foundation,Inc., 51 Franklin Street,
     * Fifth Floor, Boston, MA 02110-1301  USA
     *
     * @author SalesAgility <info@salesagility.com>
     */

    function display_size_lines($focus, $field, $value, $view)
    {
        global $sugar_config, $locale, $app_list_strings, $mod_strings;
     
         $sql = "SELECT aos_size.*
                FROM aos_size
                JOIN aos_products_aos_size_1_c ON aos_size.id = aos_products_aos_size_1_c.aos_products_aos_size_1aos_size_idb 
                JOIN aos_products ON aos_products_aos_size_1_c.aos_products_aos_size_1aos_products_ida = aos_products.id 
                WHERE aos_products.id = '".$focus->id."' AND aos_size.deleted = 0";


    
            $result = $focus->db->query($sql);
            if ($result){
            $count = mysqli_num_rows($result);
            } 
            if ($count ==0) {
                $count = 1;
            } 

        $html = '';

        if ($view == 'EditView') {
            
$html .= '<style>.alignedLabel { display: flex; align-items: center; justify-content: center; height: 100%; }</style>';

            $html .= '<script src="modules/AOS_Products/size_line_items.js"></script>';
            if (file_exists('custom/modules/AOS_Products/size_line_items.js')) {
                $html .= '<script src="custom/modules/AOS_Products/size_line_items.js"></script>';
            }
            $html .= '<script language="javascript">var sig_digits = '.$locale->getPrecision().';';
            $html .= 'var module_sugar_grp1 = "'.$focus->module_dir.'";';

            $html .= '</script>';

            $html .= "<table border='0' cellspacing='4' id='sizes'></table>";

            $html .= "<script>insertSize(1)</script>";
                        if($focus->id == null){
                    $html .= "<script>";
                    $html .= "    insertSizeLines('size_group0', '0', '5');"; // Chạy insertSizeLines trước
                    $html .= "</script>";
            }
                 $i = 0;

                    while ($row = $result->fetch_assoc()) {
                        $html .= "<script>";
                        $html .= "    insertSizeLines('size_group0', '0', '1');"; // Chạy insertSizeLines trước
                        $html .= "document.addEventListener('DOMContentLoaded', function() {";
                        $html .= "    document.getElementById('size_id' + {$i}).value = '{$row['id']}';";
                        $html .= "    document.getElementById('size_l_cm_name' + {$i}).value = '" . customFormat($row['size_l_cm']) . "';";
                        $html .= "    document.getElementById('size_w_cm_name' + {$i}).value = '" . customFormat($row['size_w_cm']) . "';";
                        $html .= "    document.getElementById('size_h_cm_name' + {$i}).value = '" . customFormat($row['size_h_cm']) . "';";
                        $html .= "    document.getElementById('size_w_h_cm_name' + {$i}).value = '" . customFormat($row['size_w_h_cm']) . "';";
                        $html .= "    document.getElementById('handle_name' + {$i}).value = '" . customFormat($row['handle']) . "';";
                        $html .= "    document.getElementById('bottom_length_name' + {$i}).value = '" . customFormat($row['bottom_length']) . "';";
                        $html .= "    document.getElementById('bottom_width_name' + {$i}).value = '" . customFormat($row['bottom_width']) . "';";
                        $html .= "});";
                        $html .= "</script>";
                        $i++;
                    }
                    if($focus->id != null){
                    $y  =  5 - $i;
                    $html .= "<script>";
                    $html .= "    insertSizeLines('size_group0', '0', '".$y."');"; // Chạy insertSizeLines trước
                    $html .= "</script>";
                    }


        }


else {
    if ($view =='DetailView') {
                $html .= '<span style="font-weight: bold; margin-left: -5px; margin-bottom: 30px">Kích thước:</span>';
                $html .= '<style>';
                $html .= '#size_table_detail th {';
                $html .= '    text-align: center;';
                $html .= '}';
                $html .= '</style>';
                $html .= '<table id="size_table_detail" style="width: 100%; background-color: #f5f5f5;">';
                $html .= '<tr>';

                $html .= '<th>'.$mod_strings['LBL_SIZE_L_CM'].'</th>';
                $html .= '<th>'.$mod_strings['LBL_SIZE_W_CM'].'</th>';
                $html .= '<th>'.$mod_strings['LBL_SIZE_H_CM'].'</th>';
                $html .= '<th>'.$mod_strings['LBL_HANDLE'].'</th>';
                $html .= '<th>'.$mod_strings['LBL_BOTTOM_LENGTH'].'</th>';
                $html .= '<th>'.$mod_strings['LBL_BOTTOM_WIDTH'].'</th>';

                $html .= '</tr>';

        $stt = 1;

            if ($result){
            while ($row = $result->fetch_assoc()) {
                $html .= '<tr>';
  
                $html .= '<td>' . customFormat($row['size_l_cm']) . '</td>';
                $html .= '<td>' . customFormat($row['size_w_cm']) . '</td>';
                $html .= '<td>' . customFormat($row['size_h_cm']) . '</td>';
                $html .= '<td>' . customFormat($row['handle']) . '</td>';
                $html .= '<td>' . customFormat($row['bottom_length']) . '</td>';
                $html .= '<td>' . customFormat($row['bottom_width']) . '</td>';

                $html .= '</tr>';
                $stt++;
        }
    }

        $html .= '</table>';

            }

        }
        return $html;
    }

function customFormat($number) {

    return number_format((float) $number, 1);
}
