<?php
$module_name = 'pr_PayrollFile';
$viewdefs [$module_name] = 
array (
  'QuickCreate' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'allowance1',
            'label' => 'LBL_ALLOWANCE1',
          ),
          1 => 
          array (
            'name' => 'allowance2',
            'label' => 'LBL_ALLOWANCE2',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'allowance3',
            'label' => 'LBL_ALLOWANCE3',
          ),
          1 => 
          array (
            'name' => 'allowance4',
            'label' => 'LBL_ALLOWANCE4',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'pr_payrollfile_cs_cususer_name',
            'label' => 'LBL_PR_PAYROLLFILE_CS_CUSUSER_FROM_CS_CUSUSER_TITLE',
          ),
          1 => 
          array (
            'name' => 'salary_amount_basic',
            'label' => 'LBL_SALARY_AMOUNT_BASIC',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'workday',
            'label' => 'LBL_WORKDAY',
          ),
          1 => 'assigned_user_name',
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'description',
            'comment' => 'Full text of the note',
            'label' => 'LBL_DESCRIPTION',
          ),
        ),
      ),
    ),
  ),
);
;
?>
