<?php
$module_name = 'pr_PayrollFile';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'ALLOWANCE1' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'label' => 'LBL_ALLOWANCE1',
    'width' => '10%',
  ),
  'ALLOWANCE2' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'label' => 'LBL_ALLOWANCE2',
    'width' => '10%',
  ),
  'ALLOWANCE3' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'label' => 'LBL_ALLOWANCE3',
    'width' => '10%',
  ),
  'ALLOWANCE4' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'label' => 'LBL_ALLOWANCE4',
    'width' => '10%',
  ),
  'SALARY_AMOUNT_BASIC' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'label' => 'LBL_SALARY_AMOUNT_BASIC',
    'width' => '10%',
  ),
  'WORKDAY' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'label' => 'LBL_WORKDAY',
    'width' => '10%',
  ),
  'PR_PAYROLLFILE_CS_CUSUSER_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_PR_PAYROLLFILE_CS_CUSUSER_FROM_CS_CUSUSER_TITLE',
    'id' => 'PR_PAYROLLFILE_CS_CUSUSERCS_CUSUSER_IDB',
    'width' => '10%',
    'default' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
  'CREATED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'default' => false,
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => false,
  ),
  'MODIFIED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_MODIFIED_NAME',
    'id' => 'MODIFIED_USER_ID',
    'width' => '10%',
    'default' => false,
  ),
  'DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => false,
  ),
);
;
?>
