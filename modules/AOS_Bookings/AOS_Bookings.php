<?php


class AOS_Bookings extends Basic
{
    public $new_schema = true;
    public $module_dir = 'AOS_Bookings';
    public $object_name = 'AOS_Bookings';
    public $table_name = 'aos_bookings';
    public $importable = false;

    public $id;
    public $name;
    public $date_entered;
    public $date_modified;
    public $modified_user_id;
    public $modified_by_name;
    public $created_by;
    public $created_by_name;
    public $description;
    public $deleted;
    public $created_by_link;
    public $modified_user_link;
    public $assigned_user_id;
    public $assigned_user_name;
    public $assigned_user_link;
    public $SecurityGroups;

    public function bean_implements($interface)
    {
        switch ($interface) {
            case 'ACL':
                return true;
        }
        return false;
    }

    public function save($check_notify = false)
    {
        global $db, $sugar_config;
        require_once 'include/TimeDate.php';
        $time = new TimeDate(); 
        $typetime = $time->get_time_format();
        $typedate = $time->get_date_format();
        $date2 = $this->date_of_service2; 
        $dateTime = DateTime::createFromFormat('d/m/Y H:i', $date2);

        $dateTime->modify('-7 hours');
        $formattedDate = $dateTime->format('Y-m-d H:i:s');

        $this->date_of_service = $formattedDate;

        $return_id = parent::save($check_notify);

        $sql = "SELECT aos_bookings_bill_billing_1bill_billing_idb FROM aos_bookings_bill_billing_1_c WHERE aos_bookings_bill_billing_1aos_bookings_ida = '".$this->id."' AND deleted = '0'";
        $result = $db->query($sql);

        while ($row = $db->fetchByAssoc($result)) {
            $billingId = $row['aos_bookings_bill_billing_1bill_billing_idb'];
            
            // Lấy thông tin billing
            $sqlBilling = "SELECT billing_status, type_currency, billingamount FROM bill_billing WHERE id = '$billingId' AND deleted = '0'";
            $billingResult = $db->query($sqlBilling);
            $billing = $db->fetchByAssoc($billingResult);
            
                if ($billing['type_currency'] == 1) {
                    $vnd = $billing['billingamount'];
                    $usd = 0;
                } else {
                    $usd = $billing['billingamount'];
                    $vnd = 0;
                }
                
                // Cập nhật thông tin billing
                $sqlUpdate = "UPDATE bill_billing SET vnd = '$vnd', usd = '$usd' WHERE id = '$billingId'";
                $db->query($sqlUpdate);
        }

        // Lấy account_id từ POST request
        $account_id = $this->accounts_aos_bookings_1accounts_ida;
        $product_id = $this->aos_products_aos_bookings_1aos_products_ida;

        $code = 'passenger_';
        $detail = array();
        $count_passenger = 0; // Khởi tạo biến đếm hành khách
        $passports_seen = array(); // Khởi tạo mảng để lưu trữ các passport đã xuất hiện
        if (isset($_POST[$code . 'passport']) && is_array($_POST[$code . 'passport'])) {
            $passports = $_POST[$code . 'passport'];
            foreach ($passports as $key => $passport) {
                if ($_POST[$code . 'services'] != '') {
                    $this->description = $_POST[$code . 'services'];
                }
                    if ($passport !== '' && !in_array($passport, $passports_seen)) {
                            $passports_seen[] = $passport; // Thêm passport vào mảng $passports_seen
                            $count_passenger++; // Tăng biến đếm hành khách

                    if ($_POST[$code . 'save'][$key] == '0') {
                        $detail= array(
                            'airport_from' => $_POST[$code . 'from'][$key],
                            'airport_to' =>   $_POST[$code . 'to'][$key],
                            'flight_name' =>  $_POST[$code . 'flight'][$key],
                            'date_std_sta' => $_POST[$code . 'std_sta'][$key],
                        );
                    }

                    if ($_POST[$code . 'save'][$key] == '1') {

                        $Bean = BeanFactory::getBean('AOS_Passengers');
                        $Bean->name = $_POST[$code . 'name'][$key];
                        $Bean->nationality = $_POST[$code . 'nationality'][$key];
                        $Bean->passport = $_POST[$code . 'passport'][$key];
                        $Bean->assigned_user_id = $this->assigned_user_id;

                        $Bean->photo2 = $_FILES[$code . 'photo']['name'][$key];
                        $id_passenger_new = $Bean->save();
                        $_POST[$code . 'id'][$key] = $id_passenger_new;
                        $this->save_account_passenger($id_passenger_new, $Bean, $account_id);

                        $detail = array(
                            'airport_from' => $_POST[$code . 'from'][$key],
                            'airport_to' =>   $_POST[$code . 'to'][$key],
                            'flight_name' =>  $_POST[$code . 'flight'][$key],
                            'date_std_sta' => $_POST[$code . 'std_sta'][$key],
                        );
                        if (isset($_FILES['passenger_photo']) && is_array($_FILES['passenger_photo']['name'])) {
                            $file_name = $_FILES['passenger_photo']['name'][$key];
                            $file_tmp_name = $_FILES['passenger_photo']['tmp_name'][$key];
                            $file_error = $_FILES['passenger_photo']['error'][$key];

                            if ($file_error === UPLOAD_ERR_OK) {
                                // Định nghĩa thư mục upload
                                $upload_dir = 'upload/';
                                
                                // Đảm bảo thư mục upload tồn tại
                                if (!is_dir($upload_dir)) {
                                    mkdir($upload_dir, 0777, true);
                                }

                                // Tạo tên file duy nhất dựa trên ID của passenger hoặc tên file gốc
                                $unique_file_name = $id_passenger_new . '_' . basename($file_name);

                                // Định nghĩa đường dẫn đầy đủ cho file upload
                                $upload_file = $upload_dir . $unique_file_name;

                                // Lưu file ảnh trực tiếp vào thư mục upload
                                if (move_uploaded_file($file_tmp_name, $upload_file)) {
                                    // Đã upload thành công, bây giờ tiến hành giảm kích thước ảnh nếu cần thiết

                                    $maxFileSize = 1024 * 1024; // 1MB

                                    // Kiểm tra kích thước file ảnh
                                    $fileSize = filesize($upload_file);

                                    if ($fileSize > $maxFileSize) {
                                        // Đọc hình ảnh gốc
                                        $sourceImage = imagecreatefromjpeg($upload_file);
                                        
                                        // Lấy kích thước ảnh gốc
                                        $sourceWidth = imagesx($sourceImage);
                                        $sourceHeight = imagesy($sourceImage);

                                        // Thiết lập kích thước mới cho ảnh (giảm kích thước)
                                        $newWidth = $sourceWidth * 0.5;
                                        $newHeight = $sourceHeight * 0.5;

                                        // Tạo một ảnh mới với kích thước mới
                                        $newImage = imagecreatetruecolor($newWidth, $newHeight);

                                        // Thay đổi kích thước của ảnh gốc thành ảnh mới
                                        imagecopyresampled($newImage, $sourceImage, 0, 0, 0, 0, $newWidth, $newHeight, $sourceWidth, $sourceHeight);

                                        // Lưu ảnh mới với chất lượng tốt hơn và ghi đè lên tệp ảnh gốc
                                        imagejpeg($newImage, $upload_file, 90); // 90 là chất lượng hình ảnh (giữa 0 và 100)

                                        // Giải phóng bộ nhớ được sử dụng
                                        imagedestroy($sourceImage);
                                        imagedestroy($newImage);
                                    }
                                }
                            }
                        }

                    }
                }
            }
        }
        $detail['count'] = $count_passenger; // Thêm số lượng hành khách vào mảng detail
        require_once 'modules/AOS_Booking_detail/AOS_Booking_detail.php';
        $booking_detail = new AOS_Booking_detail();
        $booking_detail->save_booking_detail($this, $_POST);


        $sql = "SELECT COUNT(DISTINCT spa_actionservice.id) AS total_count
        FROM spa_actionservice
        JOIN aos_bookings_spa_actionservice_1_c
            ON spa_actionservice.id = aos_bookings_spa_actionservice_1_c.aos_bookings_spa_actionservice_1spa_actionservice_idb
        JOIN aos_bookings
            ON aos_bookings.id = aos_bookings_spa_actionservice_1_c.aos_bookings_spa_actionservice_1aos_bookings_ida
        WHERE spa_actionservice.deleted = 0
          AND aos_bookings_spa_actionservice_1_c.deleted = 0
          AND aos_bookings.deleted = 0
          AND aos_bookings_spa_actionservice_1_c.aos_bookings_spa_actionservice_1aos_bookings_ida = '".$this->id."';
        ";
        $rs = $db->query($sql);
        $row = $db->fetchByAssoc($rs);
        $num_rows = $row['total_count'];

        if ($num_rows == 0) {
            $this->save_AgnetP_ActionService($account_id, $product_id, $this, $detail);
            $this->billing_receipts($this->id);
        }

        
        return $return_id;
    }


    public function save_account_passenger($passenger_id, $passengers, $account_id)
    {
        // Lấy đối tượng Contact từ database
        $passengers->retrieve($passenger_id);
        if ($passengers->load_relationship('accounts')) {
            $passengers->accounts->add($account_id);
        }
    }

    public function save_AgnetP_ActionService($account_id, $product_id, $booking, $detail)
    {
    global $db;
    $productBean = BeanFactory::getBean('AOS_Products', $product_id);

    $sql_1  = "SELECT * FROM aos_products WHERE type_service =  '2' AND deleted = '0' AND type_currency ='".$productBean->type_currency."'";
    $result_1 = $db->query($sql_1);
    $row_1 = $result_1->fetch_assoc();


    $sql_2  = "SELECT * FROM aos_products WHERE type_service =  '3' AND deleted = '0' AND type_currency ='".$productBean->type_currency."'";
    $result_2 = $db->query($sql_2);
    $row_2 = $result_2->fetch_assoc();

    $accountBean = BeanFactory::getBean('Accounts', $account_id);

    // Danh sách các dịch vụ cần lưu với các thông tin tương ứng
    $services = array();

        if ($booking->aos_products_aos_bookings_1aos_products_ida) {
            $services[] = array(
                'name' => $productBean->name,
                'service_code' => $productBean->part_number,
                'product_id' => $product_id,
                'type_service' => '1',
            );
        }
    $price_service = 0;
    $sql_3 = "SELECT * FROM aos_booking_detail WHERE booking_id ='".$booking->id."' AND deleted ='0'";
    $rs_3 = $db->query($sql_3);
    while($row_3 = $db->fetchByAssoc($rs_3)){
    foreach ($services as $service) {

        require_once 'modules/spa_ActionService/spa_ActionService.php';
        $action_sv = BeanFactory::getBean('spa_ActionService', $acction_id);
        $action_sv->name = $service['name'];
        $action_sv->aos_passengers_spa_actionservice_1aos_passengers_ida = $row_3['passenger_id'];
        $action_sv->type_service = '1';
        $action_sv->booking_date = $booking->booking_date;
        $action_sv->accounts_spa_actionservice_1_name = $booking->accounts_aos_bookings_1_name;
        $action_sv->accounts_spa_actionservice_1accounts_ida = $booking->accounts_aos_bookings_1accounts_ida;
        $action_sv->aos_products_spa_actionservice_1aos_products_ida = $service['product_id'];
        $action_sv->aos_products_spa_actionservice_1_name = $service['name'];
        $action_sv->flight_name = $booking->flight_name;
        $action_sv->status = 'Not Started';
        $action_sv->airport_from = $detail['airport_from'];
        $action_sv->airport_to = $detail['airport_to'];
        $action_sv->flight_name =  $detail['flight_name'];
        $date_std_sta = $detail['date_std_sta'] . ':00';
        $dateTime = DateTime::createFromFormat('d-m-Y H:i:s', $date_std_sta);
        if ($dateTime !== false) {
            $formatted_date = $dateTime->format('Y-m-d H:i:s');
            $action_sv->date_std_sta = $formatted_date;
        }
        $action_sv->description = $booking->description;
        $action_sv->assigned_user_id = $booking->assigned_user_id;
        $action_sv->mobileref = $accountBean->phone_alternate;
        $action_sv->product_id = $service['product_id'];
        
        if ($accountBean && $accountBean->load_relationship('accounts_aos_data_unitprice_1')) {
                $relatedBeans = $accountBean->accounts_aos_data_unitprice_1->getBeans();

            if($service['service_code'] == $productBean->part_number){
                $action_sv->exchange_rate = $booking->exchange_rate;
                $action_sv->type_currency = $productBean->type_currency;
                $action_sv->unit_price_input = $productBean->price;
            }
            foreach ($relatedBeans as $unitPriceBean) {
                if ($unitPriceBean->service_code == $service['service_code']) {
                    $action_sv->exchange_rate = $booking->exchange_rate;
                    $action_sv->type_currency = $unitPriceBean->type_currency;
                    $action_sv->unit_price_input = $unitPriceBean->price;

                    if($service['type_service'] == 1){
                           $price_service = $unitPriceBean->price;
                    }
                    if($service['type_service'] == 3){
                           $action_sv->unit_price_input =  $price_service;
                    }

                    break; // Không cần thiết phải thực hiện vòng lặp tiếp nếu đã tìm thấy dịch vụ
                }

            }

        }

            // Thực hiện phép toán
            if ($action_sv->type_currency == 1) {
                $action_sv->vnd = $action_sv->unit_price_input;
                $action_sv->usd = 0;
            } else {
                $action_sv->usd = $action_sv->unit_price_input;
                $action_sv->vnd = 0;
            }
            $action_sv->vnd_total = $action_sv->vnd * $detail['count'];
            $action_sv->usd_total = $action_sv->usd * $detail['count'];
            $action_sv->amountservice = $action_sv->unit_price_input;
            $action_sv->type_service = $service['type_service'];

            // Lưu dịch vụ
            $action_sv->save();

            // Liên kết dịch vụ với đặt phòng
            if ($action_sv->load_relationship('aos_bookings_spa_actionservice_1')) {
                $action_sv->aos_bookings_spa_actionservice_1->add($booking->id);
            }
        }
    }
}



    public function mark_deleted($id)
    {
        global $db;

        $sql = "SELECT id FROM aos_booking_detail WHERE deleted = '0' AND booking_id = '" . $id . "'";
        $result = $db->query($sql);

        // Lặp qua từng bản ghi và gắn nhãn xóa
        while ($row = $db->fetchByAssoc($result)) {
            require_once 'modules/AOS_Booking_detail/AOS_Booking_detail.php';
            $booking_detail = new AOS_Booking_detail();
            $booking_detail->mark_deleted($row['id']);
        }

        $sql2 = "SELECT aos_bookings_spa_actionservice_1spa_actionservice_idb FROM `aos_bookings_spa_actionservice_1_c` WHERE aos_bookings_spa_actionservice_1aos_bookings_ida ='".$id."'";
        $result2 = $db->query($sql2);
        while ($row2 = $db->fetchByAssoc($result2)) {
            $id_action = $row2['aos_bookings_spa_actionservice_1spa_actionservice_idb'];
            require_once 'modules/spa_ActionService/spa_ActionService.php';
            $spa_actionsv = new spa_ActionService();
            $spa_actionsv->mark_deleted($id_action);
        }
        parent::mark_deleted($id);
    }

      public function billing_receipts($booking_id){

        global $db;
        $booking = BeanFactory::getBean('AOS_Bookings', $booking_id);
        if(!$booking){
            return;
        }

        $exchangerate = $booking->exchange_rate;

        $sql = "SELECT b.*
                FROM aos_bookings_bill_billing_1_c abbb
                JOIN bill_billing b ON abbb.aos_bookings_bill_billing_1bill_billing_idb = b.id
                WHERE abbb.aos_bookings_bill_billing_1aos_bookings_ida = '".$booking_id."' 
                  AND b.check_type = 1
                  AND b.deleted = 0
                LIMIT 1";
        
        // Thực hiện truy vấn
        $result = $db->query($sql);
        if ($result) {
            $row = $result->fetch_assoc();
        } else {
            // Xử lý lỗi truy vấn nếu cần
            return;
        }

        $sql2 = "SELECT 
            spa.type_service,
            SUM(spa.vnd) AS total_vnd,
            SUM(spa.usd) AS total_usd
        FROM spa_actionservice spa
        JOIN aos_bookings_spa_actionservice_1_c link
        ON spa.id = link.aos_bookings_spa_actionservice_1spa_actionservice_idb
        WHERE link.aos_bookings_spa_actionservice_1aos_bookings_ida = '".$booking_id."' AND link.deleted = '0' 
        AND spa.status = 'Completed'
        GROUP BY spa.type_service";

        $result2 = $db->query($sql2);
        $collectionfee = 0;
        $servicefee = 0;

        if ($result2) {
            while ($row2 = $result2->fetch_assoc()) {

                if ($row2['type_service'] == 1) {
                    $servicefee = $row2['total_vnd'] + $row2['total_usd'] * $exchangerate;
                }

                if ($row2['type_service'] == 3) {
                    $collectionfee = $row2['total_vnd'] + $row2['total_usd'] * $exchangerate;
                }
            }
        }

        $totalrevenue = $servicefee - $collectionfee;


        if (isset($row['id'])) {
            $id_billing = $row['id'];
        }else{
            $billing = BeanFactory::newBean('bill_Billing');
            $billing->check_type = '1';
            $id_billing = $billing->save();
        }
          
            $billing = BeanFactory::getBean('bill_Billing', $id_billing);
            if (!$billing) {
                 $billing = BeanFactory::newBean('bill_Billing');
            }
    
                $billing->type = 'receipts';
                $dateString = $booking->date_of_service;
                $date = DateTime::createFromFormat('d/m/Y h:ia', $dateString);
                if ($date !== false && !array_sum($date::getLastErrors())) {
                    // Định dạng ngày thành d/m/Y
                    $formattedDate = $date->format('d/m/Y');
                    $billing->billingdate = $formattedDate;
                }



                require_once 'include/utils/get_voucher_code.php';

                if ($billing->name == '') {
                    $code_date = $billing->billingdate;

                    // Tạo đối tượng DateTime từ chuỗi ngày tháng
                    $date = DateTime::createFromFormat('Y-m-d', $code_date);

                    // Kiểm tra xem DateTime::createFromFormat có thành công không
                    if ($date !== false) {
                        // Chuyển đổi định dạng ngày tháng
                        $formatted_date = $date->format('d/m/Y');

                        // Gán mã voucher
                        $billing->name = get_voucher_code('bill_billing', $formatted_date, $billing->type);
                    } else {
                        // Xử lý lỗi khi không thể tạo đối tượng DateTime
                        // Ví dụ: Ghi log lỗi hoặc sử dụng ngày tháng mặc định
                        error_log("Invalid date format: $code_date");
                        $billing->name = get_voucher_code('bill_billing', '', $billing->type); // Hoặc gán giá trị khác nếu cần
                    }
                }

                $billing->paymentmethod = '7.CK BANK'; // Cập nhật đúng thuộc tính
                $billing->aos_bookings_bill_billing_1aos_bookings_ida = $booking_id;
                $billing->check_type = '1';
                if($billing->billing_status == ''){
                    $billing->billing_status = 'plan';
                }
                $billing->assigned_user_id = $booking->assigned_user_id;
                if($billing->type_currency == '' || $billing->type_currency == 1 ){
                    $billing->type_currency = 1;
                    $billing->billingamount = $totalrevenue;
                    $billing->vnd = $totalrevenue;
                    $billing->usd = 0;
                }
                if($billing->type_currency == 2){
                    $billing->billingamount = $servicefee/$exchangerate;
                    $billing->vnd = 0;
                    $billing->usd = $servicefee/$exchangerate;
                }
             
                $billing->save();
        
    }
}
