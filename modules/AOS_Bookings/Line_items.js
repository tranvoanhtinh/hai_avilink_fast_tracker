var divToRemove = document.querySelector('div.col-xs-12.col-sm-2.label[data-label="LBL_LINE_ITEMS"]');
if (divToRemove) {
    divToRemove.remove();
}
function search_data() {
    const inputs = document.querySelectorAll('.name-passenger');
    let focusedInputId = null;

    inputs.forEach(input => {
        input.addEventListener('focus', () => {
            let searchTab = document.getElementById('searchTab');
            if (!searchTab) {
                searchTab = document.createElement('div');
                searchTab.className = 'search-tab';
                searchTab.id = 'searchTab';
                searchTab.innerHTML = '<ul id="results"></ul>';
                input.closest('td').appendChild(searchTab);
            }
            searchTab.style.display = 'block';
            fetchPassengerData();
            focusedInputId = input.id; // Lưu ID của input đang focus
        });

        input.addEventListener('input', () => {
            const searchValue = normalizeString(input.value.trim()).toLowerCase(); // Chuẩn hóa và chuyển đổi sang chữ thường
            filterResults(searchValue); // Lọc kết quả dựa trên giá trị tìm kiếm
        });
        let timeoutId; // Biến để lưu trữ ID của setTimeout
        input.addEventListener('focus', () => {
            const searchValue = normalizeString(input.value.trim()).toLowerCase(); // Chuẩn hóa và chuyển đổi sang chữ thường
            
            if (searchValue !== '') { // Kiểm tra nếu giá trị không rỗng
                // Xóa timeout cũ trước khi tạo timeout mới
                if (timeoutId) {
                    clearTimeout(timeoutId);
                }
                timeoutId = setTimeout(() => {
                    filterResults(searchValue); // Lọc kết quả dựa trên giá trị tìm kiếm sau 1 giây
                }, 500); // Đợi 1000ms (1 giây) trước khi gọi hàm filterResults
            }
        });
        setTimeout(() => {
            if (timeoutId) {
                clearTimeout(timeoutId);
            }
        }, 4000);

    });

    // Hàm lọc kết quả
    function filterResults(searchValue) {
        const lis = Array.from(document.getElementById('results').getElementsByTagName('li'));
        let hasVisibleItem = false;

        lis.forEach(li => {
            const nameSpan = li.querySelector('.name');
            const passportSpan = li.querySelector('.passport');
            const nameText = normalizeString(nameSpan.textContent).toLowerCase(); // Chuẩn hóa và chuyển đổi sang chữ thường
            const passportText = normalizeString(passportSpan.textContent).toLowerCase(); // Chuẩn hóa và chuyển đổi sang chữ thường

            // Reset lại style màu nền trước khi kiểm tra
            nameSpan.style.backgroundColor = '';
            passportSpan.style.backgroundColor = '';

            // Đặt mặc định ẩn phần tử li
            li.style.display = 'none';

            // Kiểm tra xem có phù hợp với chuỗi tìm kiếm không và hiển thị/không hiển thị
            if (nameText.includes(searchValue)) {
                highlightText2(nameSpan, nameText, searchValue); // Đánh dấu văn bản tên phù hợp với màu nền
                li.style.display = 'block'; // Hiển thị phần tử li nếu tìm thấy chuỗi tìm kiếm
                hasVisibleItem = true; // Đánh dấu có hiển thị ít nhất một phần tử
            }

            if (passportText.includes(searchValue)) {
                highlightText2(passportSpan, passportText, searchValue); // Đánh dấu văn bản passport phù hợp với màu nền
                li.style.display = 'block'; // Hiển thị phần tử li nếu tìm thấy chuỗi tìm kiếm
                hasVisibleItem = true; // Đánh dấu có hiển thị ít nhất một phần tử
            }
        });
    }


    function highlightText2(element, text, searchValue) {
        const startIndex = text.indexOf(searchValue);
        if (startIndex !== -1) {
            const highlightedText = element.textContent.substring(startIndex, startIndex + searchValue.length);
            const beforeText = element.textContent.substring(0, startIndex);
            const afterText = element.textContent.substring(startIndex + searchValue.length);
            element.innerHTML = beforeText + '<span class="highlight">' + highlightedText + '</span>' + afterText;
        }
    }

    // Hàm chuẩn hóa chuỗi
    function normalizeString(str) {
        return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase().trim(); // Loại bỏ dấu và chuẩn hóa thành chữ thường
    }

    // Hàm điền dữ liệu hành khách vào kết quả
    function populateResults(data) {
        const results = document.getElementById('results');
        if (!results) {
            return; // Thoát hàm nếu không tìm thấy phần tử 'results'
        }
        results.innerHTML = '';

        data.forEach((passenger) => {
            const li = document.createElement('li');

            // Tạo phần tử span cho tên và passport
            const nameSpan = document.createElement('span');
            nameSpan.classList.add('name');
            nameSpan.textContent = passenger.name;

            const passportSpan = document.createElement('span');
            passportSpan.classList.add('passport');
            passportSpan.textContent = passenger.passport;

            // Thêm các phần tử span vào phần tử li
            li.appendChild(nameSpan);
            li.appendChild(document.createTextNode(' - ')); // Thêm dấu gạch ngang
            li.appendChild(passportSpan);

            li.style.textAlign = 'left'; // Căn lề văn bản sang trái

            // Xử lý sự kiện click cho mỗi item trong kết quả
            li.addEventListener('mousedown', () => {
                var name = passenger.name;
                var passport = passenger.passport;
                var nationality = passenger.nationality;
                var id = passenger.id;

                if (focusedInputId) {
                    const nameInput = document.getElementById(focusedInputId);

                    const match = focusedInputId.match(/\[(\d+)\]/);
                    const i = match[1];

                    const nametInput = document.getElementById('passenger_name[' + i + ']');
                    if (nametInput) {
                        nametInput.value = name;
                        nametInput.style.backgroundColor = 'rgb(216, 245, 238)';
                        nametInput.readOnly =false;
                    }
                    const idInput = document.getElementById('passenger_id[' + i + ']');
                    if (idInput) {
                        idInput.value = id;
                    }
                    const passportInput = document.getElementById('passenger_passport[' + i + ']');
                    if (passportInput) {
                        passportInput.value = passport;
                        passportInput.style.backgroundColor = 'rgb(216, 245, 238)';
                    }

                    const nationalityInput = document.getElementById('passenger_nationality[' + i + ']');
                    if (nationalityInput) {
                        nationalityInput.value = nationality;
                        nationalityInput.style.backgroundColor =  'rgb(216, 245, 238)';
                        nationalityInput.readOnly =false;
                    }
                    if(document.getElementById('passenger_save[' + i + ']')){
                        document.getElementById('passenger_save[' + i + ']').value = '0';
                        passportInput.style.borderColor = '';
                        nationalityInput.style.borderColor ='';
                        nametInput.style.borderColor = '';
                    }

                    check_booking_detail  = document.getElementById('passenger_id_detail[' + i + ']');
                    if (check_booking_detail !== ''){
                        var hiddenInput = document.createElement('input');
                        hiddenInput.type = 'hidden';
                        hiddenInput.id = 'passenger_deleted[' + i + ']';
                        hiddenInput.name = 'passenger_deleted[' + i + ']';
                        hiddenInput.value = check_booking_detail.value;
                        document.getElementById('check_save').appendChild(hiddenInput);

                    }
                }

                const searchTab = document.getElementById('searchTab');
                if (searchTab) {
                    searchTab.remove(); // Xóa tab tìm kiếm khỏi DOM
                }
            });

            // Thêm phần tử li vào danh sách kết quả
            results.appendChild(li);
        });
    }

    // Hàm gọi AJAX để lấy dữ liệu hành khách
    function fetchPassengerData() {
        $.ajax({
            type: "POST",
            url: "index.php?entryPoint=get_info_passenger",
            data: {
                status: 'get_passenger_passport',
            },
            success: function(data) {
                try {
                    const passengers = JSON.parse(data);
                    populateResults(passengers); // Gọi hàm populateResults với dữ liệu từ AJAX
                } catch (error) {
                   
                }
            },
            error: function(error) {
              
            }
        });
    }

    document.addEventListener('mousedown', (event) => {
        const searchTab = document.getElementById('searchTab');
        if (event.target.classList && !event.target.classList.contains('name-passenger')) {
            if(searchTab){
            searchTab.remove();
            }
        }
    }); 
}



var ln = 1;

function addline(count, data) {
    var service = '';
    var key = 'passenger_';
    var start = ln;
    var end = ln + count; // Add rows from ln to ln + count

    if (data) {
        var datas = JSON.parse(data);
        service = datas[0]['service'];
        var ln2 = 1; // Start line for hidden inputs
        datas.forEach(item => {
            var hiddenInput = document.createElement('input');
            hiddenInput.type = 'hidden';
            hiddenInput.id = key + 'save[' + ln2 + ']'; // Create id as required
            hiddenInput.name = key + 'save[' + ln2 + ']'; // Create name as required
            hiddenInput.value = 0;
            document.getElementById('check_save').appendChild(hiddenInput);
            ln2++;
        });
    }

    for (var i = start; i <= end; i++) {
        var dataRow = datas && datas[i - 1] ? datas[i - 1] : {};
        var date_std_sta_value = dataRow['date_std_sta'] || '';

        // Format date-time to 'd-m-Y H:i:s'
        var formattedDate = formatDateTime(date_std_sta_value);

        var newRow = '<tr class="line" id="' + key + 'tr[' + i + ']">' +
            '<td class="stt">' + i + '</td>' +
            '<td><input style="width: 97%" class="check-save name-passenger" oninput="get_info_passenger(this.value,' + i + ')" id="' + key + 'passport[' + i + ']" name="' + key + 'passport[' + i + ']" type="text" value="' + (dataRow['passport'] || '') + '"></td>' +
            '<td><input style="width: 97%" class="check-save" id="' + key + 'name[' + i + ']" name="' + key + 'name[' + i + ']" type="text" value="' + (dataRow['name'] || '') + '"></td>';

        if (dataRow['photo2']) {
            // Giả sử bạn có ID và photo2 từ dataRow, thay thế bằng giá trị thực tế của bạn
            var photoUrl = 'upload/' + dataRow['pid'] + '_' + dataRow['photo2'];
            newRow += '<td style="width: 72px"><img src="' + photoUrl + '" style="width: auto; height: 50px;" /></td>';
        } else {
            newRow += '<td style="width: 72px"><input style="width: 72px" type="file" onChange="handleIMG(this)" id="' + key + 'photo[' + i + ']" name="' + key + 'photo[' + i + ']" accept="image/*"></td>';
        }


        newRow += '<input style="width: 97%" class="passenger-id" id="' + key + 'id[' + i + ']" name="' + key + 'id[' + i + ']" type="hidden" value="' + (dataRow['passenger_id'] || '') + '">' +
            '<input id="' + key + 'id_detail[' + i + ']" name="' + key + 'id_detail[' + i + ']" type="hidden" value="' + (dataRow['id'] || '') + '">' +

            '<td><input style="width: 97%" class="check-save coppyall" oninput="UpperCase(this)" id="' + key + 'nationality[' + i + ']" name="' + key + 'nationality[' + i + ']" type="text" value="' + (dataRow['nationality'] || '') + '"></td>' +

            '<td><input style="width: 97%" class="coppyall passenger_flight" id="' + key + 'flight[' + i + ']" name="' + key + 'flight[' + i + ']" type="text" value="' + (dataRow['flight_name'] || '') + '"></td>' +
            '<td><input style="width: 97%" class="coppyall passenger_from" oninput="UpperCase(this)" onblur="UpperCase(this)" name="' + key + 'from[' + i + ']" id="' + key + 'from[' + i + ']" type="text" value="' + (dataRow['airport_from'] || '') + '" title=""></td>' +
            '<td><input style="width: 97%" class="coppyall passenger_to" oninput="UpperCase(this)" onblur="UpperCase(this)" name="' + key + 'to[' + i + ']" id="' + key + 'to[' + i + ']" type="text" value="' + (dataRow['airport_to'] || '') + '" title=""></td>' +

            '<td><input style="width: 97%" class="date-time coppyall passenger_std_sta" id="' + key + 'std_sta[' + i + ']" name="' + key + 'std_sta[' + i + ']" type="text" value="' + formattedDate + '"></td>' +
            '<td><input style="width: 97%" class="coppyall passenger_pnr" id="' + key + 'pnr[' + i + ']" name="' + key + 'pnr[' + i + ']" type="text" value="' + (dataRow['pnr'] || '') + '"></td>' +
            '<td><input style="width: 97%" id="' + key + 'deleted[' + i + ']" name="' + key + 'deleted[' + i + ']" type="button" value="X" onclick="deletedline(\'' + key + '\',' + i + ')"></td></tr>';

        var table = document.getElementById('table_addpassengers');
        var tbody = table.getElementsByTagName('tbody')[0]; // Get the first tbody in the table
        tbody.insertAdjacentHTML('beforeend', newRow); // Append the new row to the end of tbody
        set_date_time();
    }

    ln += count + 1; // Increment ln by count after adding new rows

    addServicesCell(service); // Call addServicesCell with an empty value if no data
    updateRowNumbers(); // Additional function to update row numbers
    search_data();
}


function UpperCase(input) {
    input.value = input.value.toUpperCase();
}

function formatDateTime(dateTimeStr) {
    if (!dateTimeStr) return ''; // Trả về chuỗi rỗng nếu không có giá trị
    var dateTime = new Date(Date.parse(dateTimeStr)); // Phân tích chuỗi thành đối tượng Date
    var day = ('0' + dateTime.getDate()).slice(-2); // Lấy ngày với định dạng 2 chữ số
    var month = ('0' + (dateTime.getMonth() + 1)).slice(-2); // Lấy tháng với định dạng 2 chữ số
    var year = dateTime.getFullYear();
    var hours = ('0' + dateTime.getHours()).slice(-2); // Lấy giờ với định dạng 2 chữ số
    var minutes = ('0' + dateTime.getMinutes()).slice(-2); // Lấy phút với định dạng 2 chữ số
    var seconds = ('0' + dateTime.getSeconds()).slice(-2); // Lấy giây với định dạng 2 chữ số
    return day + '-' + month + '-' + year + ' ' + hours + ':' + minutes + ':' + seconds;
}


function get_info_passenger(passport, ln) {
    var agent = document.getElementById('accounts_aos_bookings_1accounts_ida').value;
    var key = 'passenger_';
    $.ajax({
        type: "POST",
        url: "index.php?entryPoint=get_info_passenger",
        data: {
            passport: passport,
        },
        success: function(data) {
            var passportInput = document.getElementById(key + 'passport[' + ln + ']');
            var nationalityInput = document.getElementById(key + 'nationality[' + ln + ']');
            var nameInput = document.getElementById(key + 'name[' + ln + ']');
            var hiddenInputId = key + 'save[' + ln + ']';
            var hiddenInput = document.getElementById(hiddenInputId);

            if (data !== '-1') {
                var datas = JSON.parse(data);
                // Cập nhật giá trị của các input trong hàng tương ứng
                nameInput.value = datas.name;
                nationalityInput.value = datas.nationality;
                document.getElementById(key + 'id[' + ln + ']').value = datas.id;

                // Đặt màu nền cho các input
                passportInput.style.backgroundColor = '#d8f5ee';
                nationalityInput.style.backgroundColor = 'rgb(216, 245, 238)';
                nameInput.style.backgroundColor = '#d8f5ee';
                passportInput.style.borderColor = '';
                nationalityInput.style.borderColor = '';
                nameInput.style.borderColor = '';

                // Khóa các input name và nationality
                nameInput.readOnly = true;
                nationalityInput.setAttribute('data-original-value', nationalityInput.value);
                nationalityInput.classList.add('locked');

                if (hiddenInput) {
                    hiddenInput.value = '0';
                }
                var saveButtons = document.querySelectorAll('#SAVE');
                saveButtons.forEach(function(button) {
                    button.disabled = false;
                    button.style.setProperty('padding-top', '0px');
                    button.style.setProperty('padding-bottom', '0px');
                    button.style.setProperty('margin-top', '0px');
                });

                check_booking_detail = document.getElementById(key + 'id_detail[' + ln + ']');
                if (check_booking_detail !== '') {
                    var hiddenInput = document.createElement('input');
                    hiddenInput.type = 'hidden';
                    hiddenInput.id = key + 'deleted[' + ln + ']';
                    hiddenInput.name = key + 'deleted[' + ln + ']';
                    hiddenInput.value = check_booking_detail.value;
                    document.getElementById('check_save').appendChild(hiddenInput);
                }
            } else {
                document.getElementById(key + 'id[' + ln + ']').value = '';

                // Đặt màu nền cho các input
                passportInput.style.backgroundColor = 'rgb(255, 253, 91)';
                nationalityInput.style.backgroundColor = 'rgb(255, 253, 91)';
                nameInput.style.backgroundColor = 'rgb(255, 253, 91)';

                // Xóa giá trị của quốc tịch và tên
                nationalityInput.value = '';
                nameInput.value = '';

                var saveButtons = document.querySelectorAll('#SAVE');
                saveButtons.forEach(function(button) {
                    button.style.setProperty('padding-top', '5px');
                    button.style.setProperty('padding-bottom', '26px');
                    button.style.setProperty('margin-top', '-10px');
                    button.disabled = true; // Ví dụ: Vô hiệu hóa từng button
                });

                // Mở khóa các input name và nationality
                nameInput.readOnly = false;
                nationalityInput.classList.remove('locked');

                if (hiddenInput) {
                    hiddenInput.value = 1;
                } else {
                    hiddenInput = document.createElement('input');
                    hiddenInput.type = 'hidden';
                    hiddenInput.id = hiddenInputId;
                    hiddenInput.name = hiddenInputId;
                    hiddenInput.value = 1;
                    document.getElementById('check_save').appendChild(hiddenInput);
                }
            }
        },
        error: function(error) {
            // Xử lý lỗi nếu có
        },
    });
}

// Lắng nghe sự kiện thay đổi trên tất cả các thẻ select bị khóa
document.addEventListener('change', function(event) {
    if (event.target.classList.contains('locked')) {
        var originalValue = event.target.getAttribute('data-original-value');
        event.target.value = originalValue;
        alert('This field is locked and cannot be changed.');
    }
});

function deletedline(key, ln) {

    if(document.getElementById(key + 'id_detail[' + ln + ']').value !=''){
    var hiddenInput = document.createElement('input'); // Khai báo biến với từ khóa var hoặc let để tránh biến toàn cục
    hiddenInput.type = 'hidden';
    hiddenInput.id = key + 'deleted[' + ln + ']'; // Sử dụng tham số ln thay vì ln2
    hiddenInput.name = key + 'deleted[' + ln + ']'; // Sử dụng tham số ln thay vì ln2
    hiddenInput.value = document.getElementById(key + 'id_detail[' + ln + ']').value; // Sửa lỗi cú pháp
    document.getElementById('check_save').appendChild(hiddenInput);
    }
    var row = document.getElementById(key + 'tr[' + ln + ']');
    if (row) {
        var inputs = row.querySelectorAll('input');
        inputs.forEach(function(input) {
            input.setAttribute('disabled', 'true'); // Disable input elements
        });

        row.remove();
        updateRowNumbers(); // Update row numbers
    }
}


function updateRowNumbers() {
    var table = document.getElementById('table_addpassengers');
    var rows = table.querySelectorAll('tr.line'); // Lấy tất cả các hàng có class "line"
    var count = 1; // Bắt đầu đếm từ 1

    rows.forEach(function(row) {
        if (row.style.display !== 'none') {
            var cell = row.querySelector('.stt');
            if (cell) {
                cell.textContent = count++;
            }
        }
    });
    addServicesCell();
    count2 = count - 1;
    var services = document.getElementById("services");
    services.setAttribute("rows", count2 * 2); // Cập nhật số hàng của ô dịch vụ

    // Cập nhật thuộc tính rowspan của ô dịch vụ
    var servicesRowSpan = count2;
    var servicesCell = services.closest('td');
    if (servicesCell) {
        servicesCell.setAttribute('rowspan', servicesRowSpan);
    }
}



function addServicesCell(serviceValue) {
    var table = document.getElementById('table_addpassengers');
    var rows = table.querySelectorAll('tr.line'); // Lấy tất cả các hàng có class "line"
    var count = 0; // Biến đếm số lượng hàng hiển thị
    for (var i = 0; i < rows.length; i++) {
        if (rows[i].style.display !== 'none') { // Kiểm tra nếu style.display của hàng khác 'none'
            count++; // Tăng biến đếm lên 1
        }
    }
    if (document.getElementById('services')) {
        var services = document.getElementById("services");
        services.setAttribute("rows", count * 2); // Cập nhật số hàng của ô dịch vụ
        // Cập nhật thuộc tính rowspan của ô dịch vụ
        var servicesRowSpan = count;
        var servicesCell = services.closest('td');
        if (servicesCell) {
            servicesCell.setAttribute('rowspan', servicesRowSpan);
        }
    } else {
        var servicesCellHTML = '<td rowspan="' + count + '">' +
            '<textarea class ="services" id="services" name="services" rows="' + (count * 2) + '" style="width: 120px; height: auto;">' + (serviceValue || '') + '</textarea>' +
            '</td>';
        var row = table.querySelector('tbody tr.line:nth-child(2)');
        if (row) {
            var deleteButtonCell = row.querySelector('td:last-child');
            deleteButtonCell.parentNode.insertBefore(document.createElement('td'), deleteButtonCell);
            deleteButtonCell.previousSibling.outerHTML = servicesCellHTML;
        }
    }
}


function quick_coppy(id) {
    var isChecked = document.getElementById(id).checked;
    if (isChecked) {
        var table = document.getElementById('table_addpassengers');
        var rows = table.getElementsByTagName('tr');
        var firstIndex = -1;
        var lastIndex = -1;
        
        // Tìm số thứ tự đầu tiên và cuối cùng của các hàng có class 'line'
        for (var i = 0; i < rows.length; i++) {
            var row = rows[i];
            if (row.classList.contains('line')) {
                var rowId = row.id;
                var index = parseInt(rowId.split('[')[1].split(']')[0]);
                if (firstIndex === -1) {
                    firstIndex = index;
                }
                lastIndex = index;
            }
        }
        
        var firstValue = document.getElementById(id + '[' + firstIndex + ']').value;
        for (var j = firstIndex; j <= lastIndex; j++) {
            document.getElementById(id + '[' + j + ']').value = firstValue;
        }
    }
}


document.addEventListener('DOMContentLoaded', function() {
    set_date_time();
});

function set_date_time(){
    document.getElementById('date_of_service2').classList.add('date-time');
    flatpickr(".date-time", {
        enableTime: true,
        dateFormat: "d/m/Y H:i",
        time_24hr: true, 
        onChange: function(selectedDates, dateStr, instance) {
          
        }
    });

    var check_duplicate = 0;
    var duplicate = document.querySelector('input[name="duplicateSave"]');
    if (duplicate) {
        if (duplicate.value == 'true') {
            check_duplicate = 1;  // đang trùng lập
        }
    }

    var input_name = document.getElementById('name');
    if (input_name.value == '' || check_duplicate == 1) {
        auto_code();
    }

}

function auto_code() {
    // Logic của hàm auto_code
}

document.getElementById('date_of_service2').addEventListener('change', function() {
    var newValue = this.value;
    var dateInputs = document.querySelectorAll('input.date-time');
    dateInputs.forEach(function(input) {
        input.value = newValue;
    });
});

function checkSAVE() {
    var saveButtons = document.querySelectorAll('#SAVE');
    var allFilled = true; // Giả sử tất cả đều có giá trị

    // Lấy tất cả các hidden input trong div có id là "check_save" và có giá trị là '1'
    var hiddenInputs = document.querySelectorAll('#check_save input[type="hidden"][value="1"]');
    hiddenInputs.forEach(function(hiddenInput) {
        var index = hiddenInput.id.match(/\[(\d+)\]/)[1]; // Lấy chỉ số từ id của hidden input

        // Kiểm tra sự tồn tại và giá trị của các input passport, name, nationality tương ứng
        var passportInput = document.getElementById(`passenger_passport[${index}]`);
        var nameInput = document.getElementById(`passenger_name[${index}]`);
        var nationalityInput = document.getElementById(`passenger_nationality[${index}]`);

        // Kiểm tra giá trị của từng input nếu chúng tồn tại
        if (passportInput && nameInput && nationalityInput) {
            var passportValue = passportInput.value.trim();
            var nameValue = nameInput.value.trim();
            var nationalityValue = nationalityInput.value.trim();

            // Nếu có bất kỳ input nào thiếu giá trị, đặt allFilled thành false và thay đổi màu sắc
            if (passportValue === '' || nameValue === '' || nationalityValue === '') {
                allFilled = false;

                // Hiển thị input thiếu giá trị lên màu đỏ
                if (passportValue === '') {
                    passportInput.style.borderColor = 'red';
                } else {
                    passportInput.style.borderColor = '';
                }

                if (nameValue === '') {
                    nameInput.style.borderColor = 'red';
                } else {
                    nameInput.style.borderColor = '';
                }

                if (nationalityValue === '') {
                    nationalityInput.style.borderColor = 'red';
                } else {
                    nationalityInput.style.borderColor = '';
                }

            } else {
                // Đặt lại màu sắc cho input đã có giá trị
                passportInput.style.borderColor = '';
                nameInput.style.borderColor = '';
                nationalityInput.style.borderColor = '';

                var saveButtons = document.querySelectorAll('#SAVE');
                saveButtons.forEach(function(button) {
                    button.style.setProperty('padding-top', '0px');
                    button.style.setProperty('padding-bottom', '0px');
                    button.style.setProperty('margin-top', '0px');
                });


            }
        }
    });

    // Lặp qua mảng các phần tử nút "Lưu"
    saveButtons.forEach(function(button) {
        // Tùy thuộc vào giá trị của allFilled, vô hiệu hóa hoặc bật nút "Lưu"
        button.disabled = !allFilled;
    });

    if(allFilled == true){
        var saveButtons = document.querySelectorAll('#SAVE');
        saveButtons.forEach(function(button) {
            button.style.setProperty('padding-top', '0px');
            button.style.setProperty('padding-bottom', '0px');
            button.style.setProperty('margin-top', '0px');
        });
    }
}

// Gọi hàm checkSAVE mỗi 0.5 giây
setInterval(checkSAVE, 500);

function initializeDropdown(inputId, columns, entryPoint, module) {
    // Thay đổi id và name của input
    var agent = document.getElementById(inputId);
    if (agent) {
        agent.setAttribute('id', 'custom_' + inputId);
        agent.setAttribute('name', 'custom_' + inputId);

        var hiddenInput = document.createElement('input');
        hiddenInput.type = 'hidden';
        hiddenInput.id = inputId; // Sử dụng lại id ban đầu
        hiddenInput.name = inputId; // Sử dụng lại name ban đầu
        hiddenInput.value = document.getElementById('custom_'+inputId).value;
        agent.parentNode.insertBefore(hiddenInput, agent.nextSibling);
    }

    // Tạo ul element chứa danh sách
    var divElement = document.querySelector(`div.col-xs-12.col-sm-8.edit-view-field[type="relate"][field="${inputId}"]`);
    if (divElement) {
        var ulElement = document.createElement('ul');
        ulElement.id = `data_${module}_list`;
        ulElement.classList.add('dropdown-list');
        ulElement.style.display = 'none'; // Mặc định ẩn đi
        divElement.appendChild(ulElement);
    }

    // Fetch dữ liệu từ server
    function fetchAccountsData() {
        $.ajax({
            type: "POST",
            url: `index.php?entryPoint=${entryPoint}`,
            data: {
                rows: JSON.stringify(columns),
                module: module,
            },
            success: function(data) {
                var datas = JSON.parse(data);
                populateResults2(datas, module);
            },
            error: function(error) {
             
            }
        });
    }

    fetchAccountsData();

    // Populate dữ liệu vào ul
    function populateResults2(datas, module) {
        const ulElement = document.getElementById(`data_${module}_list`);
        if (!ulElement) {
            return;
        }
        ulElement.innerHTML = '';

        datas.forEach((item) => {
            const liElement = document.createElement('li');
            let content = `<span class="name">${item[columns[1]]}</span>`; // Use 'name' from columns array
            if (columns.length > 2 && item[columns[2]]) { // Check for 'account_code' from columns array if available
                content += ` - <span class="code">${item[columns[2]]}</span>`;
            }
            liElement.innerHTML = content;
            liElement.setAttribute('data-id', item[columns[0]]); // Use 'id' from columns array
            liElement.setAttribute('data-name', item[columns[1]]); // Use 'name' from columns array
            if (columns.length > 2 && item[columns[2]]) { // Check for 'account_code' from columns array if available
                liElement.setAttribute('data-account-code', item[columns[2]]);
            }
            liElement.classList.add('dropdown-item');

            liElement.addEventListener('click', () => {
                const inputName = document.getElementById('custom_' + inputId);
                const hiddeninputName = document.getElementById(inputId);

                if (inputName && hiddeninputName) {
                    inputName.value = item[columns[1]]; // Use 'name' from columns array
                    hiddeninputName.value = inputName.value;

                }
                var main_id = inputId.slice(0, -5);
                const inputIdField = document.getElementById(main_id + module + '_ida');
                if (inputIdField) {
                    inputIdField.value = item[columns[0]]; // Use 'id' from columns array
                }

                ulElement.style.display = 'none'; // Ẩn danh sách sau khi chọn
            });

            ulElement.appendChild(liElement);
        });
    }

    // Hiển thị danh sách khi focus vào input
    const inputSearch = document.getElementById('custom_' + inputId);
    if (inputSearch) {
        inputSearch.addEventListener('input', function() {
            if (this.value.trim() !== '') { // Kiểm tra nếu input đã có giá trị
                filterResults(normalizeString(this.value)); // Filter dữ liệu khi focus vào nếu đã có giá trị
            } else {
                if (ulElement) {
                    ulElement.style.display = 'block';
                }
            }
        });

        inputSearch.addEventListener('input', function() {
            const searchValue = normalizeString(this.value);
            filterResults(searchValue);
        });
    }

    // Ẩn danh sách khi nhấn ra ngoài
    document.addEventListener('click', function(event) {
        const isClickInside = inputSearch && inputSearch.contains(event.target) || (ulElement && ulElement.contains(event.target));
        if (!isClickInside && ulElement) {
            ulElement.style.display = 'none';
        }
    });

    function filterResults(searchValue) {
        const ulElement = document.getElementById(`data_${module}_list`);
        if (!ulElement) {
            return;
        }

        const lis = Array.from(ulElement.getElementsByTagName('li'));
        let hasVisibleItem = false;

        lis.forEach(li => {
            const name = normalizeString(li.getAttribute('data-name'));
            const accountCode = normalizeString(li.getAttribute('data-account-code'));

            if (name.includes(searchValue) || (accountCode && accountCode.includes(searchValue))) {
                li.style.display = 'block';
                hasVisibleItem = true;
                highlightText(li, li.getAttribute('data-name'), li.getAttribute('data-account-code'), searchValue); // Đánh dấu văn bản
            } else {
                li.style.display = 'none';
            }
        });

        if (!hasVisibleItem) {
            ulElement.style.display = 'none';
        } else {
            ulElement.style.display = 'block';
        }
    }


    function normalizeString(str) {
        return str ? str.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase().trim() : '';
    }

    // Hàm đánh dấu văn bản phù hợp với màu nền
    function highlightText(element, name, accountCode, searchValue) {
        const normalizedSearchValue = normalizeString(searchValue);
        const nameSpan = element.querySelector('.name');
        const accountCodeSpan = element.querySelector(`.code`);

        if (nameSpan) {
            if (name && normalizeString(name).includes(normalizedSearchValue)) {
                nameSpan.innerHTML = highlightSubstring(name, searchValue);
            } else {
                nameSpan.innerHTML = name; // Bỏ đánh dấu nếu không khớp
            }
        }

        if (accountCodeSpan) {
            if (accountCode && normalizeString(accountCode).includes(normalizedSearchValue)) {
                accountCodeSpan.innerHTML = highlightSubstring(accountCode, searchValue);
            } else {
                accountCodeSpan.innerHTML = accountCode; // Bỏ đánh dấu nếu không khớp
            }
        }
    }


    function highlightSubstring(text, searchValue) {
        const normalizedText = normalizeString(text);
        const normalizedSearchValue = normalizeString(searchValue);
        const startIndex = normalizedText.indexOf(normalizedSearchValue);

        if (startIndex !== -1) {
            const beforeText = text.substring(0, startIndex);
            const highlightedText = text.substring(startIndex, startIndex + searchValue.length);
            const afterText = text.substring(startIndex + searchValue.length);
            return beforeText + '<span class="highlight">' + highlightedText + '</span>' + afterText;
        }

        return text;
    }


    const customInput = document.getElementById('custom_'+inputId);
    const originalInput = document.getElementById(inputId);

    if (customInput && originalInput) {
        // Sự kiện khi người dùng nhập vào customInput
        customInput.addEventListener('input', function() {
            originalInput.value = this.value;
        });


    }

    const clearButton = document.getElementById('btn_clr_'+inputId);

    if (clearButton && customInput) {
        clearButton.addEventListener('click', function() {
            customInput.value = ''; // Xóa giá trị của input custom_accounts_aos_bookings_1_name
        });
    }


    var intervalId1;
    var field_name = 'custom_' + inputId;
    var field_id = inputId.slice(0, -5) + module + '_ida';
    var btn_choose = 'btn_' + inputId;
    var btn_choose_clear = 'btn_clr_' + inputId;
    var id2 = document.getElementById(field_id).value;


    var namex = [columns[1]];
    console.log(namex);

    var getElement = (id) => document.getElementById(id);

    // Sửa lỗi: Di chuyển dòng này xuống dưới để đảm bảo field_id đã được khởi tạo trước khi sử dụng
    var field_idvalue = getElement(field_id).value;

    const handleClick = () => {
        intervalId1 = setInterval(check_hidden_id, 500);
    };

    const handleFocus = () => {
        intervalId1 = setInterval(check_hidden_id, 500);
    };

    const handleBlur = () => {
        intervalId1 = setInterval(check_hidden_id, 500);
       
    };

    const check_hidden_id = () => {
        var id = getElement(field_id);
        if (id.value !== id2) {
            id2 = id.value;
            get_fieldname(namex ,id2); // Sửa lỗi: Đã định nghĩa hàm get_fieldname
            clearInterval(intervalId1);
        }
    };

    getElement(btn_choose).addEventListener("click", handleClick);
    getElement(field_name).addEventListener('focus', handleFocus);
    getElement(field_name).addEventListener('blur', handleBlur);

    function get_fieldname(columns,id) {

        $.ajax({
            type: "POST",
            url: `index.php?entryPoint=${entryPoint}`,
            data: {
                rows: JSON.stringify(columns),
                module: module,
                where_id : id,

        },
            success: function(data) {
                datas = JSON.parse(data);
                console.log(datas);
                document.getElementById('custom_'+inputId).value = datas[0][columns[0]];

            },
            error: function(error) {
                console.error(error);
            }
        });
    }


}//xong
// Gọi hàm với các tham số cần thiết
initializeDropdown('accounts_aos_bookings_1_name', ['id','name','account_code'], 'get_info_inputsearch', 'accounts');
initializeDropdown('aos_products_aos_bookings_1_name', ['id','name','part_number'], 'get_info_inputsearch', 'aos_products');
// initializeDropdown('contacts_aos_bookings_1_name', ['id','last_name'], 'get_info_inputsearch', 'contacts');

window.addEventListener('load', function() {
    var table = document.querySelector('#table_addpassengers');

    if (table && table.offsetWidth < 500) {
        var elements = table.querySelectorAll('tr, th, td');
        elements.forEach(function(element) {
            element.classList.add('small-font-size');
        });
    }
});


function handleIMG(input) {
    // Tạo một đối tượng FileReader để đọc dữ liệu hình ảnh
    const reader = new FileReader();

    // Khi FileReader đọc xong dữ liệu hình ảnh
    reader.onload = function(e) {
        // Tạo một phần tử img mới để hiển thị hình ảnh
        const img = document.createElement('img');
        img.src = e.target.result; // Gán URL của hình ảnh vào thẻ img
        img.style.height = '50px'; // Điều chỉnh kích thước hình ảnh nếu cần
        img.style.cursor = 'pointer'; // Con trỏ chuột dạng pointer khi hover vào hình ảnh

        // Thêm sự kiện double click vào thẻ img để ẩn hình và hiện input lại
        img.ondblclick = function() {
            img.remove(); // Xóa hình ảnh
            input.style.display = 'block'; // Hiển thị lại nút input file
            input.value = ''; // Xóa giá trị file đã chọn trước đó
        };

        // Ẩn nút input file đi và hiển thị hình ảnh
        input.style.display = 'none';
        input.parentNode.appendChild(img); // Thêm hình ảnh vào sau nút input file trong DOM
    };

    // Đọc dữ liệu hình ảnh dưới dạng Data URL
    if (input.files && input.files[0]) {
        reader.readAsDataURL(input.files[0]);
    }
}
