
<?php

/**
 * Advanced OpenSales, Advanced, robust set of sales modules.
 * @package Advanced OpenSales for SugarCRM
 * @copyright SalesAgility Ltd http://www.salesagility.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU AFFERO GENERAL PUBLIC LICENSE
 * along with this program; if not, see http://www.gnu.org/licenses
 * or write to the Free Software Foundation,Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA 02110-1301  USA
 *
 * @author SalesAgility <info@salesagility.com>
 */

function line_Items($focus, $field, $value, $view)
{
    global $sugar_config, $locale, $mod_strings, $db;
    require_once 'include/TimeDate.php';
    $time = new TimeDate(); 
    $typetime = $time->get_time_format();
    $typedate = $time->get_date_format();

    $html = '';
    $css_path = 'modules/AOS_Bookings/style.css';
    $js_path = 'modules/AOS_Bookings/Line_items.js';
    $html .= "<link rel='stylesheet' type='text/css' href='" . $css_path . "'>";
    $html .= "<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css'>";
    $html .= "<script src='modules/AOS_Bookings/flatpickr.js'></script>";
    if ($view == 'EditView') {
        $html .= '<div id="tab">
        <span id="copyBtn">Copy all</span>
        <input type="checkbox" id="checkbox"> <label for="checkbox"></label>
        </div>';

        $nationalityOptions = '';
        foreach ($GLOBALS['app_list_strings']['nationality_list'] as $value => $label) {
            $nationalityOptions .= '<option value="' . $value . '">' . $label . '</option>';
        }
        $html .= "<table id='table_addpassengers'>
            <tr class ='thline'>
                <th>STT<br><span class='english-text'>(No.)</span></th>
                <th style='min-width: 100px;  width: 120px;'>Passport</th>
                <th style='min-width: 180px'>Tên hành khách<br><span class='english-text'>(Passenger's name)</span></th>
                <th width: 40px;'>Photo</th>
                <th style='min-width: 40px; width: 100px;'>Quốc tịch<br><span class='english-text'>(Nationality)</span>
                <input type ='checkbox' onclick='quick_coppy(this.id)' name ='passenger_from' id ='passenger_nationality'>
                </th>
                <th style='min-width: 65px; width: 100px;'>Chuyến bay<br><span class='english-text'>(Flight)</span>
                <input type ='checkbox' onclick='quick_coppy(this.id)' name ='passenger_flight' id ='passenger_flight'>
                </th>
                <th style='min-width: 40px; width: 100px;'>Từ<br><span class='english-text'>(From)</span>
                    <input type ='checkbox' onclick='quick_coppy(this.id)' name ='passenger_from' id ='passenger_from'>
                </th>
                <th style='min-width: 40px; width: 100px;'>Đến<br><span class='english-text'>(To)</span>                    
                    <input type ='checkbox' onclick='quick_coppy(this.id)' name ='passenger_to' id ='passenger_to'>
                </th>
                <th style='min-width: 100px'>Ngày bay/đáp<br><span class='english-text'>(STD / STA)</span>                    
                    <input type ='checkbox' onclick='quick_coppy(this.id)' name ='passenger_std_sta' id ='passenger_std_sta'>
                </th>
                <th style='min-width: 100px; width: 200px'>Mã đặt chỗ<br><span class='english-text'>(PNR)</span>
                    <input type ='checkbox' onclick='quick_coppy(this.id)' name ='passenger_pnr' id ='passenger_pnr'>
                </th>
                <th>Yêu cầu phục vụ<br><span class='english-text'>(Request services)</span></th>
                <th></th>
            </tr>
        </table>";


        $html .= "<div style='width: 200px' class='button btn_addlins' id='addline' onclick='addline(4)'>Thêm hành khách</div>";
        $html .= "<div id='check_save'></div>";
        $html .= "<script>
            var nationalityOptions = `{$nationalityOptions}`;
        </script>";
        $html .= "<script src='" . $js_path . "'></script>";

        $sql = "SELECT a.id, a.name, a.airport_from, a.airport_to, a.flight_name, a.pnr, a.nationality, a.service, a.passenger_id, a.date_std_sta, a.passport, p.photo2 , p.id as pid
                FROM aos_booking_detail a
                JOIN aos_passengers p ON a.passenger_id = p.id
                WHERE a.deleted = '0' AND a.booking_id = '" . $focus->id . "'
                ORDER BY a.passenger_no ASC";

        $result = $db->query($sql);

        $datas = [];
        while ($row = $db->fetchByAssoc($result)) {

          

            $date_std_sta = $row['date_std_sta'];
            $dateTime = DateTime::createFromFormat('Y-m-d H:i:s', $date_std_sta);
            if ($dateTime !== false) {
                $dateTime->modify('+7 hours');
                $formatted_date = $dateTime->format('Y-m-d H:i:s');
                $row['date_std_sta'] = $formatted_date;
            } else {
                $row['date_std_sta'] = null;
            }
            $datas[] = $row;
        }

        if ($result) {
            $count = $result->num_rows;
            $jsonData = json_encode($datas, JSON_UNESCAPED_UNICODE);
            $jsonData = addslashes($jsonData); // Escape single quotes for JavaScript
            $html .= "<script>addline(" . ($count - 1) . ", '" . $jsonData . "');</script>";
        }

        if ($result->num_rows < 1) {
            $html .= "<script>addline(4);</script>";
        }

    }

    if ($view == 'DetailView') {
        global $sugar_config;
        $html .= "
        <table id='table_addpassengers'>
            <tr class ='thline'>
                <td>STT<br><span class='english-text'>(No.)</span></td>
                <td>Tên hành khách<br><span class='english-text'>(Passenger's name)</span></td>
                <td>Passport</td>
                <td>Quốc tịch<br><span class='english-text'>(Nationality)</span></td>
                <td>Chuyến bay<br><span class='english-text'>(Flight)</span></td>
                <td>Từ<br><span class='english-text'>(From)</span></td>
                <td>Đến<br><span class='english-text'>(To)</span></td>
                <td>Ngày bay/đáp<br><span class='english-text'>(STD / STA)</span></td>
                <td>Mã đặt chỗ<br><span class='english-text'>(PNR)</span></td>
                <td style='width: 250px'>Yêu cầu phục vụ<br><span class='english-text'>(Request services)</span></td>
                <td></td>
            </tr>";
        $sql = "SELECT * FROM aos_booking_detail WHERE deleted = '0' AND booking_id = '" . $focus->id . "' ORDER BY passenger_no ASC";
        $result = $db->query($sql);
        $count  =  $result->num_rows;
        $no = 1;
        while ($row = $db->fetchByAssoc($result)) {
            $html .= "<tr class='line2'>";
            $html .= "<td>" . $row['passenger_no'] . "</td>";
            $html .= "<td style='text-align: left; padding-left: 40px'>" . $row['name'] . "</td>";
            $html .= "<td>" . $row['passport'] . "</td>";
            $html .= "<td>" . $row['nationality']. "</td>";
            $html .= "<td>" . $row['flight_name'] . "</td>";
            $html .= "<td>" . $row['airport_from'] . "</td>";
            $html .= "<td>" . $row['airport_to'] . "</td>";
            $dateTime = DateTime::createFromFormat('Y-m-d H:i:s', $row['date_std_sta']);
            if ($dateTime !== false) {
                // Thêm 7 giờ
                $dateTime->modify('+7 hours');

                // Định dạng thành 'd-m-Y h:i:s A'
                $formatted_date = $dateTime->format($typedate . ' '.$typetime);

                // Thêm vào HTML
                $html .= "<td>" . $formatted_date . "</td>";
            } else {
                $html .= "<td></td>";
            }


            $html .= "<td>" . $row['pnr'] . "</td>";
            if ($no == 1) {
                $html .= "<td rowspan='" . $count . "'>" . $row['service'] . "</td>";
            }
            $html .= '</tr>';
            $no++;
        }

        $html .= "</table>";
    }

    return $html;
}

