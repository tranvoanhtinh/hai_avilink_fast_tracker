<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2018 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

require_once('include/upload_file.php');


require_once('include/upload_file.php');

class DocumentSoap
{
    public $upload_file;
    public function __construct()
    {
        $this->upload_file = new UploadFile('filename_file');
    }

    /**
     * @deprecated deprecated since version 7.6, PHP4 Style Constructors are deprecated and will be remove in 7.8, please update your code, use __construct instead
     */
    public function DocumentSoap()
    {
        $deprecatedMessage = 'PHP4 Style Constructors are deprecated and will be remove in 7.8, please update your code';
        if (isset($GLOBALS['log'])) {
            $GLOBALS['log']->deprecated($deprecatedMessage);
        } else {
            trigger_error($deprecatedMessage, E_USER_DEPRECATED);
        }
        self::__construct();
    }
    public function generateUUIDv4() {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0x0fff) | 0x4000,
            mt_rand(0, 0x3fff) | 0x8000,
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }
    public function save_images_photos ($post_data, $parent, $id){


    global $db;
    
    $c =  1;
    while (isset($post_data['deleted_'. +$c])) {
        $q3 = "UPDATE aos_products_documents_1_c SET deleted = 1 WHERE  aos_products_documents_1documents_idb = '{$post_data["deleted_$c"]}'";
        $result3 = $db->query($q3);
        if ($result3) {
            $q4 = "UPDATE documents SET deleted = 1 WHERE id = '{$post_data["deleted_$c"]}'";
            $result4 = $db->query($q4);
            if($result4){
            $q_1 = "SELECT * FROM documents WHERE id = '{$post_data["deleted_$c"]}'";
            $result_1 = $db->query($q_1);
            $row_1 = $result_1->fetch_assoc();

            $directory = "upload/"; // Đường dẫn đến thư mục chứa hình ảnh
            $keyword = $row_1['id'];

            $files = scandir($directory);
            foreach ($files as $file) {
                // Bỏ qua tên các tệp "." và ".."
                if ($file != "." && $file != "..") {
                    if (strpos($file, $keyword) !== false) {
                        unlink($directory . $file);
                    }
                }
            }
        }


        }


        $c++;
    }

    $count = count($id);

    for ($i = 0 ; $i <= $count; $i++) {
        $id_image = $id[$i];

        $name = $post_data['extra_images_'. +$i];
        $date_entered = $parent->date_modified;
        $date_modified = $parent->date_modified;
        $modified_user_id = $parent->modified_user_id;
        $created_by = $parent->created_by;
        $assigned_user_id = $parent->assigned_user_id;

             if (isset($post_data['extra_images_' . $i]) && $post_data['extra_images_' . $i] != '') {
            $name = $post_data['extra_images_' . $i];
            $q = "INSERT INTO documents 
                    (assigned_user_id, id, date_entered, date_modified, modified_user_id, created_by, document_name, deleted) 
                    VALUES 
                    ('$assigned_user_id', '$id_image', '$date_entered', '$date_modified', '$modified_user_id', '$created_by', 
                        '$name', 0)";

            $result = $db->query($q);

            if ($result) {
                $uuid = $this->generateUUIDv4();

                $q2 = "INSERT INTO  aos_products_documents_1_c (id, date_modified, deleted, aos_products_documents_1aos_products_ida, aos_products_documents_1documents_idb)
                        VALUES ('$uuid', '$date_entered', 0, '$parent->id', '$id_image')";

                $result2 = $db->query($q2);
            }

            if ($result2) {
                $q6 = "INSERT INTO documents_cstm (id_c, documenttype_c, subtypedocument_c) VALUES ('$id_image', 12, NULL);";
                $result6 = $db->query($q6);
            }
        }


    }
}


        


    public function saveFile($document, $portal = false)
    {
        global $sugar_config;

        $focus = new Document();



        if (!empty($document['id'])) {
            $focus->retrieve($document['id']);
            if (empty($focus->id)) {
                return '-1';
            }
        } else {
            return '-1';
        }

        if (!empty($document['file'])) {
            $decodedFile = base64_decode($document['file']);
            $this->upload_file->set_for_soap($document['filename'], $decodedFile);

            $ext_pos = strrpos($this->upload_file->stored_file_name, ".");
            $this->upload_file->file_ext = substr($this->upload_file->stored_file_name, $ext_pos + 1);
            if (in_array($this->upload_file->file_ext, $sugar_config['upload_badext'])) {
                $this->upload_file->stored_file_name .= ".txt";
                $this->upload_file->file_ext = "txt";
            }

            $revision = new DocumentRevision();
            $revision->filename = $this->upload_file->get_stored_file_name();
            $revision->file_mime_type = $this->upload_file->getMimeSoap($revision->filename);
            $revision->file_ext = $this->upload_file->file_ext;
            //$revision->document_name = ;
            $revision->revision = $document['revision'];
            $revision->document_id = $document['id'];
            $revision->save();

            $focus->document_revision_id = $revision->id;
            $focus->save();
            $return_id = $revision->id;
            $this->upload_file->final_move($revision->id);
        } else {
            return '-1';
        }
        return $return_id;
    }
}
