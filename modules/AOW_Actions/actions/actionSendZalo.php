<?php
session_start();
/**
 * Advanced OpenWorkflow, Automating SugarCRM.
 * @package Advanced OpenWorkflow for SugarCRM
 * @copyright SalesAgility Ltd http://www.salesagility.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU AFFERO GENERAL PUBLIC LICENSE
 * along with this program; if not, see http://www.gnu.org/licenses
 * or write to the Free Software Foundation,Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA 02110-1301  USA
 *
 * @author SalesAgility <info@salesagility.com>
 */


require_once __DIR__ . '/../../AOW_Actions/actions/actionBase.php';
require_once __DIR__ . '/../../AOW_WorkFlow/aow_utils.php';
class actionSendZalo extends actionBase
{
    private $emailableModules = array();

    /**
     *
     * @var int
     */
    protected $lastEmailsFailed;

    /**
     *
     * @var int
     */
    protected $lastEmailsSuccess;

    public function __construct($id = '')
    {
        parent::__construct($id);
        $this->clearLastEmailsStatus();
    }

    /**
     * @deprecated deprecated since version 7.6, PHP4 Style Constructors are deprecated and will be remove in 7.8, please update your code, use __construct instead
     */
    public function actionSendZalo($id = '')
    {
        $deprecatedMessage = 'PHP4 Style Constructors are deprecated and will be remove in 7.8, please update your code';
        if (isset($GLOBALS['log'])) {
            $GLOBALS['log']->deprecated($deprecatedMessage);
        } else {
            trigger_error($deprecatedMessage, E_USER_DEPRECATED);
        }
        self::__construct($id);
    }
	public function changeCurrency($number){
		$textEle=$number;
		$str='';
		$i=strlen($textEle)-3;
		$st;
		$j;
		$text_val;
		$cusText;
		$cusText='';
		$str='';
		for($u=strlen($textEle-1);$u>=0;$u--)
		{
			$str=$textEle[$u].$str;
			if($u == $i && $u > 0){
				$str=",".$str;	
				$i=$i-3;
			}
		}
		return $str;
		 
	}

    public function loadJS()
    {
        return array('modules/AOW_Actions/actions/actionSendZalo.js');
    }

    public function edit_display($line, SugarBean $bean = null, $params = array())
    {
        global $app_list_strings;
        $zalo_templates = get_bean_select_array(true, 'EmailTemplate', 'name', 'email_templates.type = "zalo"', 'name');

        if (!in_array($bean->module_dir, getEmailableModules())) {
            unset($app_list_strings['aow_email_type_list']['Record Email']);
        }
        $targetOptions = getRelatedEmailableFields($bean->module_dir);
        if (empty($targetOptions)) {
            unset($app_list_strings['aow_email_type_list']['Related Field']);
        }

        $html = '<input type="hidden" name="aow_email_type_list" id="aow_email_type_list" value="'.get_select_options_with_id($app_list_strings['aow_email_type_list'], '').'">
				  <input type="hidden" name="aow_email_to_list" id="aow_email_to_list" value="'.get_select_options_with_id($app_list_strings['aow_email_to_list'], '').'">';

        $checked = '';
        if (isset($params['individual_email']) && $params['individual_email']) {
            $checked = 'CHECKED';
        }

        $html .= "<table border='0' cellpadding='0' cellspacing='0' width='100%' data-workflow-action='send-email'>";
        $html .= "<tr>";
        $html .= '<td id="relate_label" scope="row" valign="top"><label>Gửi zalo sms:</label>';
        $html .= '</td>';
        $html .= "<td valign='top'>";
        $html .= "<input type='hidden' name='aow_actions_param[".$line."][individual_email]' value='0' >";
        $html .= "<input type='checkbox' id='aow_actions_param[".$line."][individual_email]' name='aow_actions_param[".$line."][individual_email]' value='1' $checked></td>";
        $html .= '</td>';

        if (!isset($params['email_template'])) {
            $params['email_template'] = '';
        }
        $hidden = "style='visibility: hidden;'";
        if ($params['email_template'] != '') {
            $hidden = "";
        }

        $html .= '<td id="name_label" scope="row" valign="top"><label>Zalo template:<span class="required">*</span></label></td>';
        $html .= "<td valign='top'>";
        $html .= "<select name='aow_actions_param[".$line."][email_template]' id='aow_actions_param_email_template".$line."' onchange='show_edit_template_link(this,".$line.");' >".get_select_options_with_id($zalo_templates, $params['email_template'])."</select>";

        $html .= "&nbsp;<a href='javascript:open_email_template_form(".$line.")' >".translate('LBL_CREATE_EMAIL_TEMPLATE', 'AOW_Actions')."</a>";
        $html .= "&nbsp;<span name='edit_template' id='aow_actions_edit_template_link".$line."' $hidden><a href='javascript:edit_email_template_form(".$line.")' >".translate('LBL_EDIT_EMAIL_TEMPLATE', 'AOW_Actions')."</a></span>";
        $html .= "</td>";
        $html .= "</tr>";
        $html .= "<tr>";
		
        $html .= '<td id="name_label" scope="row" valign="top"><label>Zalo:<span class="required">*</span></label></td>';
		
        $html .= '<td valign="top" scope="row">';

        $html .='<button type="button" onclick="add_ZaloLine('.$line.')"><img src="'.SugarThemeRegistry::current()->getImageURL('id-ff-add.png').'"></button>';
        $html .= '<table id="emailLine'.$line.'_table" width="100%" class="email-line"></table>';
        $html .= '</td>';
		
        $html .= "</tr>";
        $html .= "</table>";

        $html .= "<script id ='aow_script".$line."'>";

        //backward compatible
        if (isset($params['email_target_type']) && !is_array($params['email_target_type'])) {
            $email = '';
            switch ($params['email_target_type']) {
                case 'Email Address':
                    $email = $params['email'];
                    break;
                case 'Specify User':
                    $email = $params['email_user_id'];
                    break;
                case 'Related Field':
                    $email = $params['email_target'];
                    break;
            }
            $html .= "load_zaloline('".$line."','to','".$params['email_target_type']."','".$email."');";
        }
        //end backward compatible

        if (isset($params['email_target_type'])) {
            foreach ($params['email_target_type'] as $key => $field) {
                if (is_array($params['email'][$key])) {
                    $params['email'][$key] = json_encode($params['email'][$key]);
                }
                $html .= "load_zaloline('".$line."','".$params['email_to_type'][$key]."','".$params['email_target_type'][$key]."','".$params['email'][$key]."');";
            }
        }
        $html .= "</script>";

        return $html;
    }

    private function getEmailsFromParams(SugarBean $bean, $params)
    {
        $emails = array();
        //backward compatible
        if (isset($params['email_target_type']) && !is_array($params['email_target_type'])) {
            $email = '';
            switch ($params['email_target_type']) {
                case 'Email Address':
                    $params['email'] = array($params['email']);
                    break;
                case 'Specify User':
                    $params['email'] = array($params['email_user_id']);
                    break;
                case 'Related Field':
                    $params['email'] = array($params['email_target']);
                    break;
            }
            $params['email_target_type'] = array($params['email_target_type']);
            $params['email_to_type'] = array('to');
        }
        //end backward compatible
        if (isset($params['email_target_type'])) {
            foreach ($params['email_target_type'] as $key => $field) {
                switch ($field) {
                    case 'Email Address':
                        if (trim($params['email'][$key]) != '') {
                            $emails[$params['email_to_type'][$key]][] = $params['email'][$key];
                        }
                        break;
                    case 'Specify User':
                        $user = new User();
                        $user->retrieve($params['email'][$key]);
                        $user_email = $user->emailAddress->getPrimaryAddress($user);
                        if (trim($user_email) != '') {
                            $emails[$params['email_to_type'][$key]][] = $user_email;
                            $emails['template_override'][$user_email] = array('Users' => $user->id);
                        }

                        break;
                    case 'Users':
                        $users = array();
                        switch ($params['email'][$key][0]) {
                            case 'security_group':
                                if (file_exists('modules/SecurityGroups/SecurityGroup.php')) {
                                    require_once('modules/SecurityGroups/SecurityGroup.php');
                                    $security_group = new SecurityGroup();
                                    $security_group->retrieve($params['email'][$key][1]);
                                    $users = $security_group->get_linked_beans('users', 'User');
                                    $r_users = array();
                                    if ($params['email'][$key][2] != '') {
                                        require_once('modules/ACLRoles/ACLRole.php');
                                        $role = new ACLRole();
                                        $role->retrieve($params['email'][$key][2]);
                                        $role_users = $role->get_linked_beans('users', 'User');
                                        foreach ($role_users as $role_user) {
                                            $r_users[$role_user->id] = $role_user->name;
                                        }
                                    }
                                    foreach ($users as $user_id => $user) {
                                        if ($params['email'][$key][2] != '' && !isset($r_users[$user->id])) {
                                            unset($users[$user_id]);
                                        }
                                    }
                                    break;
                                }
                            //No Security Group module found - fall through.
                            // no break
                            case 'role':
                                require_once('modules/ACLRoles/ACLRole.php');
                                $role = new ACLRole();
                                $role->retrieve($params['email'][$key][2]);
                                $users = $role->get_linked_beans('users', 'User');
                                break;
                            case 'all':
                            default:
                                $db = DBManagerFactory::getInstance();
                                $sql = "SELECT id from users WHERE status='Active' AND portal_only=0 ";
                                $result = $db->query($sql);
                                while ($row = $db->fetchByAssoc($result)) {
                                    $user = new User();
                                    $user->retrieve($row['id']);
                                    $users[$user->id] = $user;
                                }
                                break;
                        }
                        foreach ($users as $user) {
                            $user_email = $user->emailAddress->getPrimaryAddress($user);
                            if (trim($user_email) != '') {
                                $emails[$params['email_to_type'][$key]][] = $user_email;
                                $emails['template_override'][$user_email] = array('Users' => $user->id);
                            }
                        }
                        break;
                    case 'Related Field':
                        $emailTarget = $params['email'][$key];
                        $relatedFields = array_merge($bean->get_related_fields(), $bean->get_linked_fields());
                        $field = $relatedFields[$emailTarget];
                        if ($field['type'] == 'relate') {
                            $linkedBeans = array();
                            $idName = $field['id_name'];
                            $id = $bean->$idName;
                            $linkedBeans[] = BeanFactory::getBean($field['module'], $id);
                        } else {
                            if ($field['type'] == 'link') {
                                $relField = $field['name'];
                                if (isset($field['module']) && $field['module'] != '') {
                                    $rel_module = $field['module'];
                                } else {
                                    if ($bean->load_relationship($relField)) {
                                        $rel_module = $bean->$relField->getRelatedModuleName();
                                    }
                                }
                                $linkedBeans = $bean->get_linked_beans($relField, $rel_module);
                            } else {
                                $linkedBeans = $bean->get_linked_beans($field['link'], $field['module']);
                            }
                        }
                        if ($linkedBeans) {
                            foreach ($linkedBeans as $linkedBean) {
                                if (!empty($linkedBean)) {
                                    $rel_email = $linkedBean->emailAddress->getPrimaryAddress($linkedBean);
                                    if (trim($rel_email) != '') {
                                        $emails[$params['email_to_type'][$key]][] = $rel_email;
                                        $emails['template_override'][$rel_email] = array($linkedBean->module_dir => $linkedBean->id);
                                    }
                                }
                            }
                        }
                        break;
                    case 'Record Email':
                        $recordEmail = $bean->emailAddress->getPrimaryAddress($bean);
                        if ($recordEmail == '' && isset($bean->email1)) {
                            $recordEmail = $bean->email1;
                        }
                        if (trim($recordEmail) != '') {
                            $emails[$params['email_to_type'][$key]][] = $recordEmail;
                        }
                        break;
                }
            }
        }
        return $emails;
    }

    /**
     * Return true on success otherwise false.
     * Use actionsendZalo::getLastEmailsSuccess() and actionsendZalo::getLastEmailsFailed()
     * methods to get last email sending status
     *
     * @param SugarBean $bean
     * @param array $params
     * @param bool $in_save
     * @return boolean
     */
    public function run_action(SugarBean $bean, $params = array(), $in_save = false)
    {
        include_once __DIR__ . '/../../EmailTemplates/EmailTemplate.php';

        //$this->clearLastEmailsStatus();
        $emailTemp = new EmailTemplate();
		// info email template
        $emailTemp->retrieve($params['email_template']);
		
        if ($emailTemp->id == '') {
            return false;
        }
        //$emails = $this->getEmailsFromParams($bean, $params);

       // if (!isset($emails['to']) || empty($emails['to'])) {
       //     return false;
        //}

        //$attachments = $this->getAttachments($emailTemp);

        $ret = true;
		/*
        if (isset($params['individual_email']) && $params['individual_email']) {
            foreach ($emails['to'] as $email_to) {
                $emailTemp = new EmailTemplate();
                $emailTemp->retrieve($params['email_template']);
                $template_override = isset($emails['template_override'][$email_to]) ? $emails['template_override'][$email_to] : array();
                $this->parse_template($bean, $emailTemp, $template_override);
				
				//send zalo
                if (!$this->sendZalo($email_to, $emailTemp->id, $emailTemp->body, $emailTemp->body, $bean, "", "", "")) {
                    $ret = false;
                    $this->lastEmailsFailed++;
                } else {
                    $this->lastEmailsSuccess++;
                }
            }
			*/
       // } else {
            //$this->parse_template($bean, $emailTemp);
           // if ($emailTemp->text_only == '1') {
           //     $email_body_html = $emailTemp->body;
          //  } else {
          //      $email_body_html = $emailTemp->body_html;
          //  }

            if (!$this->sendZalo($emailTemp->template_id_c, $emailTemp->body, $emailTemp->body,$bean)) {
                $ret = false;
                $this->lastEmailsFailed++;
            } else {
                $this->lastEmailsSuccess++;
            }
      //  }
        return $ret;
    }

    /**
     *  clear last email sending status
     */
    protected function clearLastEmailsStatus()
    {
        $this->lastEmailsFailed = 0;
        $this->lastEmailsSuccess = 0;
    }

    /**
     * failed emails count at last run_action
     * @return int
     */
    public function getLastEmailsFailed()
    {
        return $this->lastEmailsFailed;
    }

    /**
     * successfully sent emails count at last run_action
     * @return type
     */
    public function getLastEmailsSuccess()
    {
        return $this->lastEmailsSuccess;
    }

    public function parse_template(SugarBean $bean, &$template, $object_override = array())
    {
        global $sugar_config;

        require_once __DIR__ . '/templateParser.php';

        $object_arr[$bean->module_dir] = $bean->id;

        foreach ($bean->field_defs as $bean_arr) {
            if ($bean_arr['type'] == 'relate') {
                if (isset($bean_arr['module']) &&  $bean_arr['module'] != '' && isset($bean_arr['id_name']) &&  $bean_arr['id_name'] != '' && $bean_arr['module'] != 'EmailAddress') {
                    $idName = $bean_arr['id_name'];
                    if (isset($bean->field_defs[$idName]) && $bean->field_defs[$idName]['source'] != 'non-db') {
                        if (!isset($object_arr[$bean_arr['module']])) {
                            $object_arr[$bean_arr['module']] = $bean->$idName;
                        }
                    }
                }
            } else {
                if ($bean_arr['type'] == 'link') {
                    if (!isset($bean_arr['module']) || $bean_arr['module'] == '') {
                        $bean_arr['module'] = getRelatedModule($bean->module_dir, $bean_arr['name']);
                    }
                    if (isset($bean_arr['module']) &&  $bean_arr['module'] != ''&& !isset($object_arr[$bean_arr['module']])&& $bean_arr['module'] != 'EmailAddress') {
                        $linkedBeans = $bean->get_linked_beans($bean_arr['name'], $bean_arr['module'], array(), 0, 1);
                        if ($linkedBeans) {
                            $linkedBean = $linkedBeans[0];
                            if (!isset($object_arr[$linkedBean->module_dir])) {
                                $object_arr[$linkedBean->module_dir] = $linkedBean->id;
                            }
                        }
                    }
                }
            }
        }

        $object_arr['Users'] = is_a($bean, 'User') ? $bean->id : $bean->assigned_user_id;

        $object_arr = array_merge($object_arr, $object_override);

        $parsedSiteUrl = parse_url($sugar_config['site_url']);
        $host = $parsedSiteUrl['host'];
        if (!isset($parsedSiteUrl['port'])) {
            $parsedSiteUrl['port'] = 80;
        }

        $port		= ($parsedSiteUrl['port'] != 80) ? ":".$parsedSiteUrl['port'] : '';
        $path		= !empty($parsedSiteUrl['path']) ? $parsedSiteUrl['path'] : "";
        $cleanUrl	= "{$parsedSiteUrl['scheme']}://{$host}{$port}{$path}";

        $url =  $cleanUrl."/index.php?module={$bean->module_dir}&action=DetailView&record={$bean->id}";

        $template->subject = str_replace("\$contact_user", "\$user", $template->subject);
        $template->body_html = str_replace("\$contact_user", "\$user", $template->body_html);
        $template->body = str_replace("\$contact_user", "\$user", $template->body);
        $template->subject = aowTemplateParser::parse_template($template->subject, $object_arr);
        $template->body_html = aowTemplateParser::parse_template($template->body_html, $object_arr);
        $template->body_html = str_replace("\$url", $url, $template->body_html);
        $template->body_html = str_replace('$sugarurl', $sugar_config['site_url'], $template->body_html);
        $template->body = aowTemplateParser::parse_template($template->body, $object_arr);
        $template->body = str_replace("\$url", $url, $template->body);
        $template->body = str_replace('$sugarurl', $sugar_config['site_url'], $template->body);
    }

    public function getAttachments(EmailTemplate $template)
    {
        $attachments = array();
        if ($template->id != '') {
            $note_bean = new Note();
            $notes = $note_bean->get_full_list('', "parent_type = 'Emails' AND parent_id = '".$template->id."'");

            if ($notes != null) {
                foreach ($notes as $note) {
                    $attachments[] = $note;
                }
            }
        }
        return $attachments;
    }

    public function sendZalo($emailSubject, $emailBody, $altemailBody, SugarBean $relatedBean = null)
    {
		
		global $db;
		global $app_list_strings;
		global $sugar_config;
		$this->clearLastEmailsStatus();
		$arr = (array)($relatedBean);
		
		//
		//if($sugar_config["TYPE_OA"] == "VIHAT"){
					if(!empty($arr["billing_account_id"])){
						$q = "select phone_alternate from accounts  where accounts.deleted = '0' and accounts.id = '".$arr["billing_account_id"]."'";
						$res = $db->query($q);
						$rowMobile = $db->fetchByAssoc($res);
						$number = $rowMobile["phone_alternate"];
					}else{
						$number = $arr["phone_alternate"]?$arr["phone_alternate"]:$arr["phone_mobile"];
					}
					$emailBody = str_replace("$","",$emailBody);
					$array = explode(',', $emailBody);
					
					$array3 = array();
					$table='';
					$col = '';
					//lặp qua từng biến 
					
					if($sugar_config["type_oa"] == "VIHAT"){
						for($i = 0; $i<count($array); $i++){
						//lặp qua 1 biến ,lấy cột và bảng
						$array2= explode('_', $array[$i]);
						//$array2[0] = trim($array2[0]," ");
						$array2[0] = str_replace(array("\r", "\n"), '', $array2[0]);
							if($array2[0] == "aos"){
									$table = $array2[0]."_".$array2[1];
									$table = str_replace(array("\r", "\n"), '', $table);
									$table = str_replace(" ", '', $table);
									$col = $array2[2].($array2[3]?("_".$array2[3]):'').($array2[4]?("_".$array2[4]):'');
									$col = str_replace(array("\r", "\n"), '', $col);
									$col = str_replace(" ", '', $col);
							}else{
									$table = $array2[0]."s";
									$table = str_replace(array("\r", "\n"), '', $table);
									$table = str_replace(" ", '', $table);
									$col = $array2[1].($array2[2]?("_".$array2[2]):'').($array2[3]?("_".$array2[3]):'');
									$col = str_replace(array("\r", "\n"), '', $col);
									$col = str_replace(" ", '', $col);
									if($table == "leads" && $col == "name" ){
										$col == "last_name";
									}
							}
							
						
						switch($table){
							case "leads":
							$where = "phone_mobile";
							break;
							case "accounts":
							$where = "phone_alternate";
							break;
							case "contacts":
							$where = "phone_mobile";
							break;
							default:
							$where = "";
						}
						if($table == "leads" || $table == "accounts" || $table == "contacts"){
							$q= "select ".$col." from ".$table." where deleted = '0' and ".$where ." = '".$number."'";
							$r = $db->query($q);
							$row = $db->fetchByAssoc($r);
							if(!empty($row[$col])){
								$array3[$i] = $row[$col];
							}
						}else if($table == "aos_invoices" || $table == "opportunities" || $table == "aos_products"){
							if($table == "aos_invoices"){
								$q= "select ".$table.".".$col.",aos_invoices.date_entered  from ".$table." inner join accounts on aos_invoices.billing_account_id = accounts.id and accounts.deleted = '0' where aos_invoices.deleted = '0' and accounts.phone_alternate = '".$number."' order by aos_invoices.date_entered DESC limit 0,1";
								$r = $db->query($q);
								$row = $db->fetchByAssoc($r);
								if($col == "paymentmethod"){
									$row[$col] = $app_list_strings['paymentmethod_list'][$row[$col]];
								}
								$q1= "select ".$table.".".$col.",aos_invoices.date_modified from ".$table." inner join accounts on aos_invoices.billing_account_id = accounts.id and accounts.deleted = '0' where aos_invoices.deleted = '0' and accounts.phone_alternate = '".$number."' order by aos_invoices.date_modified DESC limit 0,1";
								$r1 = $db->query($q1);
								$row1 = $db->fetchByAssoc($r1);
								if($col == "paymentmethod"){
									$row1[$col] = $app_list_strings['paymentmethod_list'][$row1[$col]];
								}
								if(!empty($row1[$col]) && !empty($row[$col])){
									if($row["date_entered"] > $row1["date_modified"]){
										if(strpos($col, '_amount') != false){
											$array3[$i] = $this->changeCurrency((String)((int)$row[$col]));
										}else{
											$array3[$i] = $row[$col];
										}
									}else{
										if(strpos($col, '_amount') != false){
											$array3[$i] = $this->changeCurrency((String)((int)$row1[$col]));
										}else{
										  $array3[$i] = $row1[$col];
										}
										
									}
								}
							}else if($table == "aos_products"){
								$q= "select ".$table.".".$col." from ".$table." left join aos_products_quotes on  aos_products_quotes.product_id = aos_products.id and aos_products_quotes.deleted = '0'  left join aos_invoices on aos_products_quotes.parent_id = aos_invoices.id and aos_invoices.deleted = '0' left join accounts on aos_invoices.billing_account_id = accounts.id and accounts.deleted = '0' where aos_products.deleted = '0' and accounts.phone_alternate = '".$number."'";
								$r = $db->query($q);
								$row = $db->fetchByAssoc($r);
								if(!empty($row[$col])){
									$array3[$i] = $row[$col];
								}
							}else
							{
								$q= "select ".$table.".".$col." from ".$table." inner join accounts_opportunities on  opportunities.id = accounts_opportunities.opportunity_id and accounts_opportunities.deleted = '0' inner join accounts on accounts_opportunities.account_id = accounts.id and accounts.deleted = '0' where opportunities.deleted = '0' and accounts.phone_alternate = '".$number."'";
								$_SESSION["query"][]= $q;
								$r = $db->query($q);
								$row = $db->fetchByAssoc($r);
								if(!empty($row[$col])){
									$array3[$i] = $row[$col];
								}
							}
						}
					}
					$str = '["'; 
					for($i = 0; $i < count($array3); $i++){
						if($i < count($array3) - 1){
							$str .= $array3[$i].'","';
						}else{
							$str .= $array3[$i].'"]';
						}
					}
				$q = "select tttt_worldfonepbx.*,tttt_worldfonepbx_cstm.* from tttt_worldfonepbx inner join tttt_worldfonepbx_users_1_c on tttt_worldfonepbx_users_1_c.tttt_worldfonepbx_users_1tttt_worldfonepbx_ida = tttt_worldfonepbx.id and tttt_worldfonepbx_users_1_c.deleted = '0' left join tttt_worldfonepbx_cstm on tttt_worldfonepbx.id = tttt_worldfonepbx_cstm.id_c where tttt_worldfonepbx_users_1_c.tttt_worldfonepbx_users_1users_idb = '".$_SESSION['authenticated_user_id']."' and type_sys_app = '2'";
				$r = $db->query($q);
				$ro = $db->fetchByAssoc($r);
				if($ro["is_sendzalo"] == 1){
					$curl = curl_init();
					curl_setopt_array($curl, array(
					CURLOPT_URL => 'http://rest.esms.vn/MainService.svc/json/SendZaloMessage_V4_post_json/',
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'POST',
					CURLOPT_POSTFIELDS =>'{
						"ApiKey": "70A515BF4D74D9C68F785610D00A37",
						"SecretKey":"0F410DEDA519583232D255B314CF07",
						"Params":'.$str.',
						"Phone":"'.$number.'",
						"TempID": "'.$emailSubject.'",
						"OAID": "1311518628854618268",
						"campaignid": "test 1 ",
						"CallbackUrl": "https://en4ysz7m6tn8h.x.pipedream.net/"
					}',
					CURLOPT_HTTPHEADER => array(
					  'Content-Type: application/json',
				     'Cookie: ASP.NET_SessionId=rc04mnfdapu0hkj3qlsfdfeu'
			        ),
			     ));

			   $response = curl_exec($curl);
			  curl_close($curl);
			}
		}else{
			
			for($i = 0; $i<count($array); $i++){
						//lặp qua 1 biến ,lấy cột và bảng
						// $array11[0] -> tên khóa
						$array11 = explode('@', $array[$i]);
						$array11[0] = str_replace(array("\r", "\n"), '', $array11[0]);
						$array11[0] = str_replace(" ", '', $array11[0]);
						$array3[$i] .= '"'.$array11[0].'":';
						
						$array2= explode('_', $array11[1]);
						//$array2[0] = trim($array2[0]," ");
						$array2[0] = str_replace(array("\r", "\n"), '', $array2[0]);
							if($array2[0] == "aos"){
									$table = $array2[0]."_".$array2[1];
									$table = str_replace(array("\r", "\n"), '', $table);
									$table = str_replace(" ", '', $table);
									$col = $array2[2].($array2[3]?("_".$array2[3]):'').($array2[4]?("_".$array2[4]):'');
									$col = str_replace(array("\r", "\n"), '', $col);
									$col = str_replace(" ", '', $col);
							}else{
									$table = $array2[0]."s";
									$table = str_replace(array("\r", "\n"), '', $table);
									$table = str_replace(" ", '', $table);
									$col = $array2[1].($array2[2]?("_".$array2[2]):'').($array2[3]?("_".$array2[3]):'');
									$col = str_replace(array("\r", "\n"), '', $col);
									$col = str_replace(" ", '', $col);
									if($table == "leads" && $col == "name" ){
										$col = "last_name";
									}
							}
							
						
						switch($table){
							case "leads":
							$where = "leads.phone_mobile";
							break;
							case "accounts":
							$where = "accounts.phone_alternate";
							break;
							case "contacts":
							$where = "contacts.phone_mobile";
							break;
							default:
							$where = "";
						}
						if($table == "leads" || $table == "accounts" || $table == "contacts"){
							$q= "select ".$table.((strpos($col, '_c') != false)?"_cstm":"").".".$col." from ".$table."  inner join ".$table."_cstm on ".$table.".id = ".$table."_cstm.id_c where ".$table.".deleted = '0' and ".$where ." = '".$number."'";
							$r = $db->query($q);
							$row = $db->fetchByAssoc($r);
							if(!empty($row[$col])){
								if($col == "servicetype_c"){
									$row[$col] = $app_list_strings['servicetype_list'][$row[$col]];
								}
								$array3[$i] .= '"'.$row[$col].'"';
							}
							
						}else if($table == "aos_invoices" || $table == "opportunities" || $table == "aos_products"){
							if($table == "aos_invoices"){
								$q= "select ".$table.".".$col.",aos_invoices.date_entered  from ".$table." inner join accounts on aos_invoices.billing_account_id = accounts.id and accounts.deleted = '0' where aos_invoices.deleted = '0' and accounts.phone_alternate = '".$number."' order by aos_invoices.date_entered DESC limit 0,1";
								$r = $db->query($q);
								$row = $db->fetchByAssoc($r);
								if($col == "paymentmethod"){
									$row[$col] = $app_list_strings['paymentmethod_list'][$row[$col]];
								}
								$q1= "select ".$table.".".$col.",aos_invoices.date_modified from ".$table." inner join accounts on aos_invoices.billing_account_id = accounts.id and accounts.deleted = '0' where aos_invoices.deleted = '0' and accounts.phone_alternate = '".$number."' order by aos_invoices.date_modified DESC limit 0,1";
								$r1 = $db->query($q1);
								$row1 = $db->fetchByAssoc($r1);
								if($col == "paymentmethod"){
									$row1[$col] = $app_list_strings['paymentmethod_list'][$row1[$col]];
								}
								if(!empty($row1[$col]) && !empty($row[$col])){
									if($row["date_entered"] > $row1["date_modified"]){
										if(strpos($col, '_amount') != false){
											$array3[$i] .= '"'.$this->changeCurrency((String)((int)$row[$col])).'"';
										}else{
											$array3[$i] .= '"'.$row[$col].'"';
										}
									}else{
										if(strpos($col, '_amount') != false){
											$array3[$i] .= '"'.$this->changeCurrency((String)((int)$row1[$col])).'"';
										}else{
										  $array3[$i] .= '"'.$row1[$col].'"';
										}
										
									}
								}
							}else if($table == "aos_products"){
								$q= "select ".$table.".".$col." from ".$table." left join aos_products_quotes on  aos_products_quotes.product_id = aos_products.id and aos_products_quotes.deleted = '0'  left join aos_invoices on aos_products_quotes.parent_id = aos_invoices.id and aos_invoices.deleted = '0' left join accounts on aos_invoices.billing_account_id = accounts.id and accounts.deleted = '0' where aos_products.deleted = '0' and accounts.phone_alternate = '".$number."'";
								$r = $db->query($q);
								$row = $db->fetchByAssoc($r);
								if(!empty($row[$col])){
									$array3[$i] .= '"'.$row[$col].'"';
								}
							}else
							{
								$q= "select ".$table.".".$col." from ".$table." inner join accounts_opportunities on  opportunities.id = accounts_opportunities.opportunity_id and accounts_opportunities.deleted = '0' inner join accounts on accounts_opportunities.account_id = accounts.id and accounts.deleted = '0' where opportunities.deleted = '0' and accounts.phone_alternate = '".$number."'";
								$_SESSION["query"][]= $q;
								$r = $db->query($q);
								$row = $db->fetchByAssoc($r);
								if(!empty($row[$col])){
									$array3[$i] .= '"'.$row[$col].'"';
								}
							}
						}
						
					}
					$str = '{'; 
					for($i = 0; $i < count($array3); $i++){
						if($i < count($array3) - 1){
							$str .= $array3[$i].',';
						}else{
						$str .= $array3[$i].'}';
						}
						
					}
					
					
$q = "select tttt_worldfonepbx.*,tttt_worldfonepbx_cstm.* from tttt_worldfonepbx inner join tttt_worldfonepbx_users_1_c on tttt_worldfonepbx_users_1_c.tttt_worldfonepbx_users_1tttt_worldfonepbx_ida = tttt_worldfonepbx.id and tttt_worldfonepbx_users_1_c.deleted = '0' left join tttt_worldfonepbx_cstm on tttt_worldfonepbx.id = tttt_worldfonepbx_cstm.id_c where tttt_worldfonepbx_users_1_c.tttt_worldfonepbx_users_1users_idb = '".$_SESSION['authenticated_user_id']."' and type_sys_app = '2'";
			$r = $db->query($q);
			$ro = $db->fetchByAssoc($r);
			if($ro["is_sendzalo"] == 1){
				$que = "select tttt_worldfonepbx.*,tttt_worldfonepbx_cstm.* from tttt_worldfonepbx inner join tttt_worldfonepbx_users_1_c on tttt_worldfonepbx_users_1_c.tttt_worldfonepbx_users_1tttt_worldfonepbx_ida = tttt_worldfonepbx.id and tttt_worldfonepbx_users_1_c.deleted = '0' left join tttt_worldfonepbx_cstm on tttt_worldfonepbx.id = tttt_worldfonepbx_cstm.id_c where tttt_worldfonepbx_users_1_c.tttt_worldfonepbx_users_1users_idb = '".$_SESSION['authenticated_user_id']."' and type_sys_app = '2'";
				$ress = $db->query($que);
				$roww = $db->fetchByAssoc($ress);
				//if(!empty($roww["psw_callcenter"])){
				$api2 = "https://business.openapi.zalo.me/message/template";
				
				//if(!empty($_POST["phone"])){
					/*
					$pos = strpos($_POST["phone"],",");
					if($pos == false){
					$arrPhone = array();	
					$arrPhone[0] = $_POST["phone"];
					}else{
					$arrPhone = explode(",",$_POST["phone"]);
					}
					*/
					$headers = [
									//'access_token: eEENNGZOCHMfwPTT0Dyu7ANbaLK7oMbzm_ELRcQMHIc5zvmeNCr0ARgTYHzvsYC-gkdl8K3J9mQ3ei87HvaEGxZ4e7XBbNLwby-XIJ-FQ46R-Q9hKvTGJFkbfWbQamjFeTRNJd-U6agTxhLCP8rcSwVPg6S6WK5FrTorPn6s65hkWS95ICeF0g64qXLu_WmCaSg-336AHX7JnAfM1_DqVCQ1gZiVudagmB2m8o7JQWBVXw0H1znDKg-cY4rUnKLwll6RG7-UF43Q-EPV68uRLltzr5GwXdv9a0AVS0FFEXy',
									'Content-Type: application/json',
									 'access_token:'.$roww["psw_callcenter"]
								];

					//$q = "select template_id_c from email_templates where id = '".$_POST["id"]."' and deleted = '0'";
					//$r = $db->query($q);
					//$row = $db->fetchByAssoc($r);
					$ch2 = curl_init();
					
					
						//$data = '{"phone":"'.$number.'","template_id":"'.$row["template_id_c"].'","template_data":{"otp":"'.$_POST["message"].'"},"tracking_id":"12345"}';
						if($number[0] == "8" && $number[1] == "4"){
							$number = $number;
						}else{
							if($number[0] == "0"){
								$number = substr_replace($number, '84', 0, 1);
							}else{
								$number = $number;
							}
							
						}
						$data = '{
						"phone":"'.$number.'",
						"template_id":"'.$emailSubject.'",
						"template_data":'.
						$str.',
						"tracking_id":"tracking_id"
					}';
					
						curl_setopt($ch2, CURLOPT_HTTPHEADER, $headers);
						curl_setopt($ch2, CURLOPT_HEADER, false);
						curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);
						curl_setopt($ch2, CURLOPT_URL, $api2);
						curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
						curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, "POST");
						curl_setopt($ch2, CURLOPT_POSTFIELDS, $data);
						$response = curl_exec($ch2);
				}
			
		}
			return true;
			
    }
}
