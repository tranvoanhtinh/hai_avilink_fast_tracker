<?php


class AOS_Data_unitprice extends Basic
{
    public $new_schema = true;
    public $module_dir = 'AOS_Data_unitprice';
    public $object_name = 'AOS_Data_unitprice';
    public $table_name = 'aos_data_unitprice';
    public $importable = false;
    public $id;
    public $name;
    public $date_entered;
    public $date_modified;
    public $modified_user_id;
    public $modified_by_name;
    public $created_by;
    public $created_by_name;
    public $description;
    public $deleted;
    public $created_by_link;
    public $modified_user_link;
    public $assigned_user_id;
    public $assigned_user_name;
    public $assigned_user_link;
    public $SecurityGroups;
	
    public function bean_implements($interface)
    {
        switch($interface)
        {
            case 'ACL':
                return true;
        }

        return false;
    }

    public function save($check_notify = false)
    {

        if($this->type_currency == 1){
            $this->vnd = $this->price;
            $this->usd = 0;
        }else{
            $this->usd = $this->price;
            $this->vnd = 0;
        }
        $return_id = parent::save($check_notify);
        return $return_id;
    }

}
