<?php
$module_name='lm_LeaveManagerment';
$subpanel_layout = array (
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopCreateButton',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'popup_module' => 'lm_LeaveManagerment',
    ),
  ),
  'where' => '',
  'list_fields' => 
  array (
    'name' => 
    array (
      'vname' => 'LBL_NAME',
      'widget_class' => 'SubPanelDetailViewLink',
      'width' => '45%',
      'default' => true,
    ),
    'fromdate' => 
    array (
      'type' => 'date',
      'vname' => 'LBL_FROMDATE',
      'width' => '10%',
      'default' => true,
    ),
    'todate' => 
    array (
      'type' => 'date',
      'vname' => 'LBL_TODATE',
      'width' => '10%',
      'default' => true,
    ),
    'status' => 
    array (
      'type' => 'enum',
      'studio' => 'visible',
      'vname' => 'LBL_STATUS',
      'width' => '10%',
      'default' => true,
    ),
    'reason' => 
    array (
      'type' => 'enum',
      'studio' => 'visible',
      'vname' => 'LBL_REASON',
      'width' => '10%',
      'default' => true,
    ),
    'edit_button' => 
    array (
      'vname' => 'LBL_EDIT_BUTTON',
      'widget_class' => 'SubPanelEditButton',
      'module' => 'lm_LeaveManagerment',
      'width' => '4%',
      'default' => true,
    ),
    'remove_button' => 
    array (
      'vname' => 'LBL_REMOVE',
      'widget_class' => 'SubPanelRemoveButton',
      'module' => 'lm_LeaveManagerment',
      'width' => '5%',
      'default' => true,
    ),
  ),
);