<?php
$module_name = 'spa_ActionService';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'status' => 
      array (
        'type' => 'enum',
        'studio' => 'visible',
        'label' => 'LBL_STATUS',
        'width' => '10%',
        'default' => true,
        'name' => 'status',
      ),
      'current_user_only' => 
      array (
        'name' => 'current_user_only',
        'label' => 'LBL_CURRENT_USER_FILTER',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
      ),
    ),
    'advanced_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'status' => 
      array (
        'type' => 'enum',
        'studio' => 'visible',
        'label' => 'LBL_STATUS',
        'width' => '10%',
        'default' => true,
        'name' => 'status',
      ),
      'action_date' => 
      array (
        'type' => 'datetimecombo',
        'label' => 'LBL_ACTION_DATE',
        'width' => '10%',
        'default' => true,
        'name' => 'action_date',
      ),
      'spa_actionservice_cs_cususer_name' => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_SPA_ACTIONSERVICE_CS_CUSUSER_FROM_CS_CUSUSER_TITLE',
        'id' => 'SPA_ACTIONSERVICE_CS_CUSUSERCS_CUSUSER_IDB',
        'width' => '10%',
        'default' => true,
        'name' => 'spa_actionservice_cs_cususer_name',
      ),
    
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
;
?>
