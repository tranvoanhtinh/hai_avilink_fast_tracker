<?php
$popupMeta = array (
    'moduleMain' => 'spa_ActionService',
    'varName' => 'spa_ActionService',
    'orderBy' => 'spa_actionservice.name',
    'whereClauses' => array (
  'status' => 'spa_actionservice.status',
  'action_date' => 'spa_actionservice.action_date',
  'spa_actionservice_cs_cususer_name' => 'spa_actionservice.spa_actionservice_cs_cususer_name',
  'assigned_user_name' => 'spa_actionservice.assigned_user_name',
),
    'searchInputs' => array (
  3 => 'status',
  4 => 'action_date',
  5 => 'spa_actionservice_cs_cususer_name',
  6 => 'assigned_user_name',
),
    'searchdefs' => array (
  'status' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_STATUS',
    'width' => '10%',
    'name' => 'status',
  ),
  'action_date' => 
  array (
    'type' => 'datetimecombo',
    'label' => 'LBL_ACTION_DATE',
    'width' => '10%',
    'name' => 'action_date',
  ),
  'spa_actionservice_cs_cususer_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_SPA_ACTIONSERVICE_CS_CUSUSER_FROM_CS_CUSUSER_TITLE',
    'id' => 'SPA_ACTIONSERVICE_CS_CUSUSERCS_CUSUSER_IDB',
    'width' => '10%',
    'name' => 'spa_actionservice_cs_cususer_name',
  ),
  'assigned_user_name' => 
  array (
    'link' => true,
    'type' => 'relate',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'id' => 'ASSIGNED_USER_ID',
    'width' => '10%',
    'name' => 'assigned_user_name',
  ),
),
);
