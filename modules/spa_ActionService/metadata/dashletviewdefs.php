<?php
$dashletData['spa_ActionServiceDashlet']['searchFields'] = array (
  'status' => 
  array (
    'default' => '',
  ),
  'action_date' => 
  array (
    'default' => '',
  ),
  'spa_actionservice_aos_invoices_name' => 
  array (
    'default' => '',
  ),
  'spa_actionservice_cs_cususer_name' => 
  array (
    'default' => '',
  ),
);
$dashletData['spa_ActionServiceDashlet']['columns'] = array (
  'name' => 
  array (
    'width' => '40%',
    'label' => 'LBL_LIST_NAME',
    'link' => true,
    'default' => true,
    'name' => 'name',
  ),
  'status' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_STATUS',
    'width' => '10%',
    'default' => true,
  ),
  'action_date' => 
  array (
    'type' => 'datetimecombo',
    'label' => 'LBL_ACTION_DATE',
    'width' => '10%',
    'default' => true,
  ),
  'assigned_user_name' => 
  array (
    'width' => '8%',
    'label' => 'LBL_LIST_ASSIGNED_USER',
    'name' => 'assigned_user_name',
    'default' => true,
  ),
  'date_modified' => 
  array (
    'width' => '15%',
    'label' => 'LBL_DATE_MODIFIED',
    'name' => 'date_modified',
    'default' => false,
  ),
  'created_by' => 
  array (
    'width' => '8%',
    'label' => 'LBL_CREATED',
    'name' => 'created_by',
    'default' => false,
  ),
  'date_entered' => 
  array (
    'width' => '15%',
    'label' => 'LBL_DATE_ENTERED',
    'default' => false,
    'name' => 'date_entered',
  ),
);
