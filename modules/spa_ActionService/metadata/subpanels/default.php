<?php
$module_name='spa_ActionService';
$subpanel_layout = array (
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopCreateButton',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'popup_module' => 'spa_ActionService',
    ),
  ),
  'where' => '',
  'list_fields' => 
  array (
    'name' => 
    array (
      'vname' => 'LBL_NAME',
      'widget_class' => 'SubPanelDetailViewLink',
      'width' => '45%',
      'default' => true,
    ),
    'action_date' => 
    array (
      'type' => 'datetimecombo',
      'vname' => 'LBL_ACTION_DATE',
      'width' => '10%',
      'default' => true,
    ),
    'status' => 
    array (
      'type' => 'enum',
      'studio' => 'visible',
      'vname' => 'LBL_STATUS',
      'width' => '10%',
      'default' => true,
    ),
    'commission_action' => 
    array (
      'type' => 'decimal',
      'default' => true,
      'vname' => 'LBL_COMMISSION_ACTION',
      'width' => '10%',
    ),
    'edit_button' => 
    array (
      'vname' => 'LBL_EDIT_BUTTON',
      'widget_class' => 'SubPanelEditButton',
      'module' => 'spa_ActionService',
      'width' => '4%',
      'default' => true,
    ),
    'remove_button' => 
    array (
      'vname' => 'LBL_REMOVE',
      'widget_class' => 'SubPanelRemoveButton',
      'module' => 'spa_ActionService',
      'width' => '5%',
      'default' => true,
    ),
  ),
);