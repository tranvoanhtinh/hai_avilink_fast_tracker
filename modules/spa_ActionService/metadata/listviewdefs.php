<?php
$module_name = 'spa_ActionService';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'ACTION_DATE' => 
  array (
    'type' => 'datetimecombo',
    'label' => 'LBL_ACTION_DATE',
    'width' => '10%',
    'default' => true,
  ),
  'STATUS' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_STATUS',
    'width' => '10%',
    'default' => true,
  ),
  'COMMISSION_ACTION' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'label' => 'LBL_COMMISSION_ACTION',
    'width' => '10%',
  ),
  'SPA_ACTIONSERVICE_CS_CUSUSER_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_SPA_ACTIONSERVICE_CS_CUSUSER_FROM_CS_CUSUSER_TITLE',
    'id' => 'SPA_ACTIONSERVICE_CS_CUSUSERCS_CUSUSER_IDB',
    'width' => '10%',
    'default' => true,
  ),
  'SPA_ACTIONSERVICE_AOS_INVOICES_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_SPA_ACTIONSERVICE_AOS_INVOICES_FROM_AOS_INVOICES_TITLE',
    'id' => 'SPA_ACTIONSERVICE_AOS_INVOICESAOS_INVOICES_IDA',
    'width' => '10%',
    'default' => true,
  ),
   'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => false,
  ),
  'DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'CREATED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'default' => false,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => false,
  ),
  'MODIFIED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_MODIFIED_NAME',
    'id' => 'MODIFIED_USER_ID',
    'width' => '10%',
    'default' => false,
  ),
);
;
?>
