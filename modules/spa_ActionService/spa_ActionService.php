<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2018 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */


class spa_ActionService extends Basic
{
    public $new_schema = true;
    public $module_dir = 'spa_ActionService';
    public $object_name = 'spa_ActionService';
    public $table_name = 'spa_actionservice';
    public $importable = true;

    public $id;
    public $name;
    public $date_entered;
    public $date_modified;
    public $modified_user_id;
    public $modified_by_name;
    public $created_by;
    public $created_by_name;
    public $description;
    public $deleted;
    public $created_by_link;
    public $modified_user_link;
    public $assigned_user_id;
    public $assigned_user_name;
    public $assigned_user_link;
    public $SecurityGroups;
    public $status;
    public $commission_action;
    public $action_date;
	
    public function bean_implements($interface)
    {
        switch($interface)
        {
            case 'ACL':
                return true;
        }

        return false;
    }
   public function save($check_notify = false)
{
    global $db;
    if($this->$amountservice == ''){
            $this->amountservice = $this->unit_price_input;
    }
    $booking_id = $this->aos_bookings_spa_actionservice_1aos_bookings_ida;
    if($booking_id !=  ''){
        $booking =  BeanFactory::getBean('AOS_Bookings',  $booking_id);
        $this->exchange_rate = $booking->exchange_rate;
        if ($this->type_currency == 1) {
            $this->vnd = $this->amountservice;
            $this->usd = 0;
        } else {
            $this->vnd = 0;
            $this->usd = $this->amountservice;
        }
    }else {
        if ($this->type_currency == 1) {
            $this->vnd = $this->amountservice;
            $this->usd = 0;
        } else {
            $this->vnd = 0;
            $this->usd = $this->amountservice;

        }
    }
    $this->product_id = $this->aos_products_spa_actionservice_1aos_products_ida;

    $return_id = parent::save($check_notify);

    $this->billing_receipts($booking_id);

    $sql = "SELECT aos_bookings_spa_actionservice_1aos_bookings_ida FROM `aos_bookings_spa_actionservice_1_c` WHERE `aos_bookings_spa_actionservice_1spa_actionservice_idb` = '".$this->id."'";
    $result = $db->query($sql);

    if ($result) {
        $row = $result->fetch_assoc();
        if ($row) {
            $bookingId = $row['aos_bookings_spa_actionservice_1aos_bookings_ida'];

            $sqlCount = "SELECT COUNT(*) as count FROM `aos_booking_detail` WHERE booking_id = '".$bookingId."' AND deleted = '0'";
            $resultCount = $db->query($sqlCount);
            $countRow = $resultCount->fetch_assoc();
            if ($countRow && isset($countRow['count'])) {
                $count = $countRow['count'];
                $this->vnd_total = $this->vnd * $count;
                $this->usd_total = $this->usd * $count;
                $return_id = parent::save($check_notify);
            }
        }
    }

    return $return_id;
}

    public function mark_deleted($id)
    {

        parent::mark_deleted($id);
    }

    public function billing_receipts($booking_id){
        global $db;
        $booking = BeanFactory::getBean('AOS_Bookings', $booking_id);
        if(!$booking){
            return;
        }

        $exchangerate = $booking->exchange_rate;

        $sql = "SELECT b.*
                FROM aos_bookings_bill_billing_1_c abbb
                JOIN bill_billing b ON abbb.aos_bookings_bill_billing_1bill_billing_idb = b.id
                WHERE abbb.aos_bookings_bill_billing_1aos_bookings_ida = '".$booking_id."' 
                  AND b.check_type = 1
                  AND b.deleted = 0
                LIMIT 1";
        
        // Thực hiện truy vấn
        $result = $db->query($sql);
        if ($result) {
            $row = $result->fetch_assoc();
        } else {
            // Xử lý lỗi truy vấn nếu cần
            return;
        }

        $sql2 = "SELECT 
            spa.type_service,
            SUM(spa.vnd) AS total_vnd,
            SUM(spa.usd) AS total_usd
        FROM spa_actionservice spa
        JOIN aos_bookings_spa_actionservice_1_c link
        ON spa.id = link.aos_bookings_spa_actionservice_1spa_actionservice_idb
        WHERE link.aos_bookings_spa_actionservice_1aos_bookings_ida = '".$booking_id."' AND link.deleted = '0' 
        AND spa.status = 'Completed'
        GROUP BY spa.type_service";

        $result2 = $db->query($sql2);
        $collectionfee = 0;
        $servicefee = 0;

        if ($result2) {
            while ($row2 = $result2->fetch_assoc()) {

                if ($row2['type_service'] == 1) {
                    $servicefee = $row2['total_vnd'] + $row2['total_usd'] * $exchangerate;
                }

                if ($row2['type_service'] == 3) {
                    $collectionfee = $row2['total_vnd'] + $row2['total_usd'] * $exchangerate;
                }
            }
        }

        $totalrevenue = $servicefee - $collectionfee;


        if (isset($row['id'])) {
            $id_billing = $row['id'];
        }else{
            $billing = BeanFactory::newBean('bill_Billing');
            $billing->check_type = '1';
            $id_billing = $billing->save();
        }
          
            $billing = BeanFactory::getBean('bill_Billing', $id_billing);
            if (!$billing) {
                 $billing = BeanFactory::newBean('bill_Billing');
            }
    
                $billing->type = 'receipts';
                $dateString = $booking->date_of_service;
                $date = DateTime::createFromFormat('d/m/Y h:ia', $dateString);
                if ($date !== false && !array_sum($date::getLastErrors())) {
                    // Định dạng ngày thành d/m/Y
                    $formattedDate = $date->format('d/m/Y');
                    $billing->billingdate = $formattedDate;
                }
                $billing->paymentmethod = '7.CK BANK'; // Cập nhật đúng thuộc tính
                $billing->aos_bookings_bill_billing_1aos_bookings_ida = $booking_id;
                $billing->check_type = '1';
                if($billing->billing_status == ''){
                    $billing->billing_status = 'plan';
                }
                $billing->assigned_user_id = $booking->assigned_user_id;
                if($billing->type_currency == '' || $billing->type_currency == 1 ){
                    $billing->type_currency = 1;
                    $billing->billingamount = $totalrevenue;
                    $billing->vnd = $totalrevenue;
                    $billing->usd = 0;
                }
                if($billing->type_currency == 2){
                    $billing->billingamount = $servicefee/$exchangerate;
                    $billing->vnd = 0;
                    $billing->usd = $servicefee/$exchangerate;
                }
             
                $billing->save();
        
    }
}