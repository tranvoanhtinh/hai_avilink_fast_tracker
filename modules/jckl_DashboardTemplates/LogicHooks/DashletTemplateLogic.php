<?php
    
    class DashletTemplateLogic
    {

        public function checkUserChange($bean, $event, $arguments)
        {

            if ($bean->fetched_row['assigned_user_id'] != $bean->assigned_user_id) {

                $bean->updateEncodedData($bean->assigned_user_id);
            }

        }
    }