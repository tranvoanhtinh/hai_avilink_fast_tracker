<?php

    class jckl_DashboardTemplatesDeploymentUtilities
    {

        public function getDeployType($type)
        {

            switch ($type) {
                case 'role':
                    $list = $this->getRoles();
                    break;
                case 'group':
                    $list = $this->getGroups();
                    break;
                case 'users':
                    $list = $this->getAllUser();
                    break;
            }


            return $list;
        }


       
        public function getRoles()
        {

            global $db;

            $sql = "SELECT ar.id, ar.name 
                    FROM acl_roles ar
                    WHERE ar.deleted = 0
                    AND ar.id IN (
                      SELECT aru.role_id FROM acl_roles_users aru 
                      WHERE aru.deleted = 0
                      )";

            $GLOBALS['log']->debug('Dashlet Deploy Get Roles Query: ' . $sql);

            $result = $db->query($sql);

            $select_options = array();

                while ($row = $db->fetchByAssoc($result)) {
                    $select_options[$row['id']] = $row['name'];
                }

            return $select_options;

        }

       
        public function getGroups()
        {

            global $db;

            $sql = "SELECT sg.id, sg.name
                        FROM securitygroups sg
                        WHERE sg.id IN 
                          (SELECT su.securitygroup_id FROM securitygroups_users su WHERE su.deleted = 0)";

            $GLOBALS['log']->debug('Dashlet Deploy Get Roles Query: ' . $sql);
            $result = $db->query($sql);

            $select_options = array();

                while ($row = $db->fetchByAssoc($result)) {

                    $select_options[$row['id']] = $row['name'];

                }

            return $select_options;

        }

         public function getAllUser () {

             global $db;

            $sql = "SELECT user_name, id
                    FROM  users 
                    WHERE  deleted = 0";

            $results = $db->query($sql);

            $users = array();

            while ($row = $db->fetchByAssoc($results)) {

                $users[$row['id']] = $row['user_name'];

            }

            return $users;
        }

        public function getUsers()
        {

            require_once('modules/Users/User.php');

            $users = User::getAllUsers();

            return $users;
        }


       
        public function getDeploymentOptions()
        {
            global $app_list_strings;
            return $app_list_strings['jckl_deploy_options'];

        }



    }