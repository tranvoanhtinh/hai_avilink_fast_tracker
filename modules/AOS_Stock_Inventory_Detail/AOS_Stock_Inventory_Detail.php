<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2018 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */
require_once('modules/AOS_Stock_Inventory_Detail/AOS_Stock_Inventory_Detail_sugar.php');
class AOS_Stock_Inventory_Detail extends AOS_Stock_Inventory_Detail_sugar
{

    public function __construct()
        {
            parent::__construct();
        }


    public function AOS_Stock_Inventory_Detail()
    {
        $deprecatedMessage = 'PHP4 Style Constructors are deprecated and will be remove in 7.8, please update your code';
        if (isset($GLOBALS['log'])) {
            $GLOBALS['log']->deprecated($deprecatedMessage);
        } else {
            trigger_error($deprecatedMessage, E_USER_DEPRECATED);
        }
        self::__construct();
    }



        public function save_SID($post_data, $parent, $parent2, $key, $type, $type_voucher) {


            $line_count = isset($post_data[$key . 'name']) ? count($post_data[$key . 'name']) : 0;
            $j = 0;
            for ($i = 0; $i < $line_count; ++$i) {
                if (isset($post_data[$key . 'deleted'][$i]) && $post_data[$key . 'deleted'][$i] == 1) {
                    $parent2->mark_deleted($post_data[$key . 'id'][$i]);
                } else {
                    if (!isset($post_data[$key . 'id'][$i])) {
                        LoggerManager::getLogger()->warn('Post date has no key id');
                        $postDataKeyIdI = null;
                    } else {
                        $postDataKeyIdI = $post_data[$key . 'id'][$i];
                    }
                    $SID = BeanFactory::newBean('AOS_Stock_Inventory_Detail');
                    foreach ($parent2->field_defs as $field_def) {
                        $field_name = $field_def['name'];

                        if(isset($type)) {
                            if($field_name == 'name') {
                                $SID->name = $post_data[$key . $field_name][$i];
                            }
                            if($field_name == 'part_number') {
                                $SID->maincode = $post_data[$key . $field_name][$i];
                            }
                            if($field_name == 'product_qty') {
                                $sl = 0;
                                $SID->quantity = $post_data[$key . $field_name][$i];
                                $sl = $post_data[$key . $field_name][$i];
                            }
                             if ($field_name == 'product_id') {
                                $where = "aos_products.id = '".$post_data[$key . $field_name][$i]."'";
                                $productsBeans = BeanFactory::getBean('AOS_Products')->get_full_list('', $where);
                                if (!empty($productsBeans)) {
                                    $product = $productsBeans[0]; // Chọn bản ghi đầu tiên từ mảng
                                    $SID->assigned_user_id = $product->assigned_user_id; // Truy cập thuộc tính assigned_user_id của bản ghi đã chọn
                                }
                            }


                            if($field_name == 'product_list_price') {
                                $SID->price = $post_data[$key . $field_name][$i];
                              
                            $price = (float) str_replace(',', '', $post_data[$key . $field_name][$i]);
                            $total_price = $sl * $price;
                            $total_price = number_format($total_price, 2, '.', ',');
                            $SID->total_price = $total_price;

                            }

                        
                            

                            if($field_name == 'product_id') {
                                $SID->product_id = $post_data[$key . $field_name][$i];
                            }
                        }
                    }

                    $dateParts = explode('-', $parent->voucherdate);
                    if($parent->module_dir == 'AOS_Invoices'){
                         $dateParts = explode('-', $parent->invoice_date);
                    }
                    $year = $dateParts[0]; // Lấy năm
                    $month = $dateParts[1]; // Lấy tháng
                    $day = $dateParts[2];
                    $SID->take_from = $parent->take_from;
                    // Cập nhật các trường khác của $SID
                    $currentDate = date('Y-m-d');
                    $SID->day=$day;
                    $SID->month=$month;
                    $SID->year=$year;

                    if($parent->module_dir == 'AOS_Invoices'){
                         $SID->voucherdate = $parent->invoice_date;
                    }else{
                        $SID->voucherdate = $parent->voucherdate;
                    }
                    $SID->in_out = $type;
                    $SID->parent_id = $parent->id;
                    $SID->type_voucher = $type_voucher;
                    $SID->voucherno = $parent->name;
                    $SID->save();

                }
            }
        }

    public function save($check_notify = false)
    {
        perform_aos_save($this);
        return parent::save($check_notify);
    }

}



