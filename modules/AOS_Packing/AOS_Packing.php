<?php
/**
 * Products, Quotations & Invoices modules.
 * Extensions to SugarCRM
 * @package Advanced OpenSales for SugarCRM
 * @subpackage Products
 * @copyright SalesAgility Ltd http://www.salesagility.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU AFFERO GENERAL PUBLIC LICENSE
 * along with this program; if not, see http://www.gnu.org/licenses
 * or write to the Free Software Foundation,Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA 02110-1301  USA
 *
 * @author SalesAgility Ltd <support@salesagility.com>
 */

/**
 * THIS CLASS IS FOR DEVELOPERS TO MAKE CUSTOMIZATIONS IN
 */
require_once('modules/AOS_Packing/AOS_Packing_sugar.php');
class AOS_Packing extends AOS_Packing_sugar
{
    public function __construct()
    {
        parent::__construct();
    }

    public function AOS_Packing()
    {
        $deprecatedMessage = 'PHP4 Style Constructors are deprecated and will be remove in 7.8, please update your code';
        if (isset($GLOBALS['log'])) {
            $GLOBALS['log']->deprecated($deprecatedMessage);
        } else {
            trigger_error($deprecatedMessage, E_USER_DEPRECATED);
        }
        self::__construct();
    }

    public function save($check_notify = false)
    {
        global $db;

        $return_id = parent::save($check_notify);
        return $return_id;
    }

    public  function getGUID()
    {
        if (function_exists('com_create_guid')) {
            $uuid = com_create_guid();
        } else {
            mt_srand((double)microtime()*10000); // optional for PHP 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45); // "-"
            $uuid = substr($charid, 0, 8) . $hyphen
                . substr($charid, 8, 4) . $hyphen
                . substr($charid, 12, 4) . $hyphen
                . substr($charid, 16, 4) . $hyphen
                . substr($charid, 20, 12);
        }
        return strtolower($uuid);
    }



    public function save_line($post_data, $parent) {
    global $db;


    // Đếm số lượng phần tử trong mảng $post_data['packing_delete']
    $count = count($post_data['packing_delete']);

    // Duyệt qua mảng $post_data['packing_delete'] để xử lý từng bản ghi
    for ($i = 0; $i < $count; $i++) {
        $id = $post_data['packing_id'][$i];
        // Kiểm tra xem bản ghi có bị xóa không và có dữ liệu không
        if (isset($post_data['packing_delete'][$i]) && $post_data['packing_delete'][$i]) {
            $delete_line = "DELETE FROM `aos_packing` WHERE id ='".$id."'";
            $db->query($delete_line);
            continue; // Bỏ qua bản ghi bị xóa hoặc không có dữ liệu
        }




        $AOS_Packing = BeanFactory::getBean('AOS_Packing', $id);
        $type == 'update';
        if (!$AOS_Packing) {
            $AOS_Packing = BeanFactory::newBean('AOS_Packing');
            $type ='insert';
        }

        $AOS_Packing->packing_l_cm = $post_data['packing_l_cm_name'][$i];
        $AOS_Packing->packing_w_cm = $post_data['packing_w_cm_name'][$i];
        $AOS_Packing->packing_h_cm = $post_data['packing_h_cm_name'][$i];
        $AOS_Packing->packing_l_inch = $post_data['packing_l_inch_name'][$i];
        $AOS_Packing->packing_w_inch = $post_data['packing_w_inch_name'][$i];
        $AOS_Packing->packing_h_inch = $post_data['packing_h_inch_name'][$i];
        $AOS_Packing->cbm = $post_data['cbm_name'][$i];
        $AOS_Packing->sets_ctn = $post_data['sets_ctn_name'][$i];
        $AOS_Packing->fob_packing = $post_data['fob_packing'][$i];
        $AOS_Packing->load_packing = $post_data['load_packing'][$i];
        $AOS_Packing->note_packing = $post_data['note_packing'][$i];

        // Lưu bean AOS_Packing

        if (
            !empty($AOS_Packing->packing_l_cm) ||
            !empty($AOS_Packing->packing_w_cm) ||
            !empty($AOS_Packing->packing_h_cm) ||
            !empty($AOS_Packing->packing_l_inch) ||
            !empty($AOS_Packing->packing_w_inch) ||
            !empty($AOS_Packing->packing_h_inch) ||
            !empty($AOS_Packing->cbm) ||
            !empty($AOS_Packing->sets_ctn) ||
            !empty($AOS_Packing->fob_packing) ||
            !empty($AOS_Packing->load_packing) ||
            !empty($AOS_Packing->note_packing)
        ) {
            // Lưu bean AOS_Packing nếu có ít nhất một trường không rỗng
            $AOS_Packing->save();
        
            if($type =='insert'){

            $relation_ship = "INSERT INTO `aos_products_aos_packing_1_c`(`id`, `date_modified`, `deleted`, `aos_products_aos_packing_1aos_products_ida`, `aos_products_aos_packing_1aos_packing_idb`) VALUES ('".$this->getGUID()."','".$this->date_modified."','0','".$parent->id."','".$AOS_Packing->save()."')";
                $db->query($relation_ship);

            }
        }
    }
}

    public function mark_deleted($id)
    {
        global $db;
        parent::mark_deleted($id);
    }
}

