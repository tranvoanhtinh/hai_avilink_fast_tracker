var lineno;
var prodln = 0;
var servln = 0;
var groupn = 0;
var group_ids = {};

/**
 * Load Line Items
 */

function insertLineItems(product, group) {
  var type = "product_";
  var ln = 0;
  var current_group = "lineItems";
  var gid = product.group_id;

  if (typeof group_ids[gid] === "undefined") {
    current_group = insertGroup();
    group_ids[gid] = current_group;
    for (var g in group) {
      if (document.getElementById("group" + current_group + g) !== null) {
        document.getElementById("group" + current_group + g).value = group[g];
      }
    }
  } else {
    current_group = group_ids[gid];
  }
//a
  if (product.product_id != "0" && product.product_id !== "") {
    ln = insertProductLine("product_group" + current_group, current_group);
    type = "product_";
    //----------------
    var product_id = product.product_id;
    if (product_id) {
      var tableContainer = document.getElementById("table_size_" + ln);
      if (tableContainer) {
        display_size(product_id, function (tableHtml) {
          // Xử lý kết quả ở đây
          tableContainer.innerHTML = tableHtml;
        });
      }
    }
    if (product_id) {
      var tableContainer2 = document.getElementById("table_packing_" + ln);
      if (tableContainer) {
        display_packing(product_id, function (tableHtml) {
          // Xử lý kết quả ở đây
          tableContainer2.innerHTML = tableHtml;
        });
      }
    }

  
    
     // display_photo(product_id);
    
    //------------------------
  }

  for (var p in product) {
    if (document.getElementById(type + p + ln) !== null) {
      if (
        product[p] !== "" &&
        isNumeric(product[p]) &&
        p != "vat" &&
        p != "product_id" &&
        p != "name" &&
        p != "part_number"
      ) {
        document.getElementById(type + p + ln).value = format2Number(
          product[p]
        );
      } else {
        document.getElementById(type + p + ln).value = product[p];
      }
    }
  }

  calculateLine(ln, type);
}

/**
 * Insert product line
 */

function insertProductLine(tableid, groupid) {
  if (!enable_groups) {
    tableid = "product_group0";
  }

  if (document.getElementById(tableid + "_head") !== null) {
    document.getElementById(tableid + "_head").style.display = "";
  }

  /**
   * Insert product line
   */

  function select_user() {
    var currentURL = window.location.href;
    var index = currentURL.indexOf("/index.php");
    var desiredPart = currentURL.slice(0, index);
    var newURL = desiredPart + "/index.php?entryPoint=user_active";

    var xhr = new XMLHttpRequest();
    xhr.open("POST", newURL, false); // Thực hiện yêu cầu đồng bộ (không khuyến khích)
    xhr.send();

    if (xhr.readyState == 4 && xhr.status == 200) {
      // Parse JSON response
      var users = JSON.parse(xhr.responseText);

      // Process the array of users
      var html = '<option selected value="-1">--Không--</option>';

      for (var i = 0; i < users.length; i++) {
        html +=
          '<option value="' + users[i].id + '">' + users[i].name + "</option>";
      }

      // Trả về chuỗi HTML
      return html;
    }
  }

  // Gọi hàm select_user
  var html = select_user();

  var vat_hidden = document.getElementById("vathidden").value;
  var discount_hidden = document.getElementById("discounthidden").value;
  var parkhidden = document.getElementById("parkhidden").value;

  sqs_objects["product_name[" + prodln + "]"] = {
  "form": "EditView",
  "method": "query",
  "modules": ["AOS_Products"],
  "group": "or",
  "field_list": ["name", "id", "part_number", "cost", "price", "description", "currency_id"],
  "populate_list": ["product_name[" + prodln + "]", "product_product_id[" + prodln + "]", "product_part_number[" + prodln + "]", "product_product_cost_price[" + prodln + "]", "product_product_list_price[" + prodln + "]", "product_item_description[" + prodln + "]", "product_currency[" + prodln + "]"],
  "required_list": ["product_id[" + prodln + "]"],
  "conditions": [{
  "name": "name",
  "op": "like_custom",
  "end": "%",
  "value": ""
  }],
  "order": "name",
  "limit": "30",
  "post_onblur_function": "formatListPrice(" + prodln + ");",
  "no_match_text": "No Match"
  };
  sqs_objects["product_part_number[" + prodln + "]"] = {
  "form": "EditView",
  "method": "query",
  "modules": ["AOS_Products"],
  "group": "or",
  "field_list": ["part_number", "name", "id","cost", "price","description","currency_id"],
  "populate_list": ["product_part_number[" + prodln + "]", "product_name[" + prodln + "]", "product_product_id[" + prodln + "]",  "product_product_cost_price[" + prodln + "]", "product_product_list_price[" + prodln + "]", "product_item_description[" + prodln + "]", "product_currency[" + prodln + "]"],
  "required_list": ["product_id[" + prodln + "]"],
  "conditions": [{
  "name": "part_number",
  "op": "like_custom",
  "end": "%",
  "value": ""
  }],
  "order": "name",
  "limit": "30",
  "post_onblur_function": "formatListPrice(" + prodln + ");",
  "no_match_text": "No Match"
  };

  tablebody = document.createElement("tbody");
  tablebody.id = "product_body" + prodln;
  document.getElementById(tableid).appendChild(tablebody);

  //--------------------------------------dòng 1----------------------
  var x = tablebody.insertRow(-1);
  x.id = "product_line" + prodln;

  var a = x.insertCell(0);
  a.innerHTML =
    "<input type='text'  style='width: 97%;' name='product_product_qty[" +
    prodln +
    "]' id='product_product_qty" +
    prodln +
    "'  value='' title='' tabindex='116' onblur='Quantity_format2Number(" +
    prodln +
    ");calculateLine(" +
    prodln +
    ",\"product_\");' class='product_qty'>";

  var b1 = x.insertCell(1);
  b1.innerHTML =
    "<input class='sqsEnabled product_part_number' autocomplete='off' type='text' style='width:  95%;' name='product_part_number[" +
    prodln +
    "]' id='product_part_number" +
    prodln +
    "' maxlength='50' value='' title='' tabindex='116' value=''>";

  var b = x.insertCell(2);
  b.innerHTML =
    "<input class='sqsEnabled product_name' autocomplete='off' type='text' style='width:  160%;' name='product_name[" +
    prodln +
    "]' id='product_name" +
    prodln +
    "' maxlength='50' value='' title='' tabindex='116' value=''><input type='hidden' name='product_product_id[" +
    prodln +
    "]' id='product_product_id" +
    prodln +
    "'  maxlength='50' value=''> ";



var b2 = x.insertCell(3);
b2.innerHTML =
  "<button title='" +
  SUGAR.language.get("app_strings", "LBL_SELECT_BUTTON_TITLE") +
  "' accessKey='" +
  SUGAR.language.get("app_strings", "LBL_SELECT_BUTTON_KEY") +
  "' type='button' tabindex='116' class='button product_part_number_button' value='" +
  SUGAR.language.get("app_strings", "LBL_SELECT_BUTTON_LABEL") +
  "' name='btn1' onclick='openProductPopup(" +
  prodln +
  ");' style='margin-left: 100px;'><span class='suitepicon suitepicon-action-select'></span></button>";

  var d = x.insertCell(4);
  d.innerHTML =
    "<input type='text' style='width: 97%;' name='product_sets_ctn[" +
    prodln +
    "]' id='product_sets_ctn" +
    prodln +
    "'  maxlength='50' value='' title='' tabindex='116' onblur='calculateLine(" +
    prodln +
    ",\"product_\");' onblur='calculateLine(" +
    prodln +
    ",\"product_\");' class='product_discount_text'><input type='hidden' name='product_set_ctns_amount[" +
    prodln +
    "]' id='product_set_ctns_amount" +
    prodln +
    "' value=''  />";

  var f = x.insertCell(5);
  f.innerHTML =
    "<input type='text'  style='width: 97%;'  name='product_total_ctns[" +
    prodln +
    "]' id='product_total_ctns" +
    prodln +
    "' maxlength='250' value='' title='' tabindex='116' readonly='readonly' class='product_vat_amt_text'>";
  // if (typeof currencyFields !== 'undefined'){
  //   currencyFields.push("product_vat_amt" + prodln);
  // }
  var k = x.insertCell(6);
  k.innerHTML =
    "<input type='text' style='width: 97%;' name='product_cbm[" +
    prodln +
    "]' id='product_cbm" +
    prodln +
    "' maxlength='50' value='' title='' tabindex='116' onblur='calculateLine(" +
    prodln +
    ",\"product_\");' class='product_list_price'><input type='hidden' name='product_product_cost_price[" +
    prodln +
    "]' id='product_product_cost_price" +
    prodln +
    "' value=''  />";

  var l = x.insertCell(7);
  l.innerHTML =
    "<input type='text' style='width: 97%;' name='product_total_cbm[" +
    prodln +
    "]' id='product_total_cbm" +
    prodln +
    "' maxlength='50' value='' title='' tabindex='116' onblur='calculateLine(" +
    prodln +
    ",\"product_\");' class='product_list_price'><input type='hidden' name='product_product_cost_price[" +
    prodln +
    "]' id='product_product_cost_price" +
    prodln +
    "' value=''  />";

  var c = x.insertCell(8);
  c.innerHTML =
    "<input type='text' style='width: 97%;'  name='product_product_list_price[" +
    prodln +
    "]' id='product_product_list_price" +
    prodln +
    "' maxlength='50' value='' title='' tabindex='116' onblur='calculateLine(" +
    prodln +
    ",\"product_\");' class='product_list_price'><input type='hidden' name='product_product_cost_price[" +
    prodln +
    "]' id='product_product_cost_price" +
    prodln +
    "' value=''  />";

  if (typeof currencyFields !== "undefined") {
    currencyFields.push("product_product_list_price" + prodln);
    currencyFields.push("product_product_cost_price" + prodln);
  }


  var h = x.insertCell(9);
  h.innerHTML =
    "<input type='hidden' style='width: 95%;'  name='product_currency[" +
    prodln +
    "]' id='product_currency" +
    prodln +
    "' value=''><input type='hidden' name='product_deleted[" +
    prodln +
    "]' id='product_deleted" +
    prodln +
    "' value='0'><input type='hidden' name='product_id[" +
    prodln +
    "]' id='product_id" +
    prodln +
    "' value=''><button type='button' id='product_delete_line" +
    prodln +
    "' class='button product_delete_line' value='" +
    SUGAR.language.get(module_sugar_grp1, "LBL_REMOVE_PRODUCT_LINE") +
    "' tabindex='116' onclick='markLineDeleted(" +
    prodln +
    ',"product_")\'><span class="suitepicon suitepicon-action-clear"></span></button><br>';

  enableQS(true);

  ///------------------------------------------- dòng 2 --------------------------------
  //QSFieldsArray["EditView_product_name"+prodln].forceSelection = true;

  var p = tablebody.insertRow(-1);
  p.id = "product_tax_line" + prodln;

  var b1 = p.insertCell(0);
  b1.innerHTML +=
    "<br><select tabindex='116'  style='width: 97%' name='product_vat[" +
    prodln +
    "]' id='product_vat" +
    prodln +
    "' onchange='calculateLine(" +
    prodln +
    ",\"product_\");' class='product_vat_amt_select'>" +
    vat_hidden +
    "</select>";
  b1.innerHTML +=
    "<input type='hidden' name='product_percent_vat[" +
    prodln +
    "]' id='product_percent_vat" +
    prodln +
    "' value=''  />";

  var b = p.insertCell(1);
  b.innerHTML =
    "<span style='vertical-align: top;' class='product_description_label'>" +
    SUGAR.language.get(module_sugar_grp1, "LBL_VAT_AMT") +
    "</span>";
  b.innerHTML +=
    "<input type='text' style='width: 97%' name='product_vat_amt[" +
    prodln +
    "]' id='product_vat_amt" +
    prodln +
    "' maxlength='250' value='' title='' tabindex='116' readonly='readonly' class='product_vat_amt_text'>";

  var a1 = p.insertCell(2);
  a1.innerHTML +=
    "<br><select tabindex='116' name='product_discount[" +
    prodln +
    "]' id='product_discount" +
    prodln +
    "' onchange='calculateLine(" +
    prodln +
    ", \"product_\");' class='product_discount_amount_select'>" +
    discount_hidden +
    "</select><input type='hidden' name='product_type_discount[" +
    prodln +
    "]' id ='product_type_discount" +
    prodln +
    "' value= '' />";

  var a = p.insertCell(3);
  a.innerHTML =
    "<span style='vertical-align: top;' class='product_description_label'>" +
    SUGAR.language.get(module_sugar_grp1, "LBL_DISCOUNT_AMT") +
    "</span>";
  a.innerHTML +=
    "<input type='text' style='width: 97%' name='product_product_discount[" +
    prodln +
    "]' id='product_product_discount" +
    prodln +
    "'  maxlength='50' value='' title='' tabindex='116' onblur='calculateLine(" +
    prodln +
    ",\"product_\");' onblur='calculateLine(" +
    prodln +
    ",\"product_\");' class='product_discount_text'><input type='hidden' name='product_product_discount_amount[" +
    prodln +
    "]' id='product_product_discount_amount" +
    prodln +
    "' value=''  />";

  var c = p.insertCell(4);
  c.innerHTML =
    "<span style='vertical-align: top;' class='product_description_label'>" +
    SUGAR.language.get(module_sugar_grp1, "LBL_TYPE") +
    "</span>";
  c.innerHTML +=
    "<select tabindex='116' style='width: 97%' name='product_percent_commsvc[" +
    prodln +
    "]' id='product_percent_commsvc" +
    prodln +
    "' onchange='calculateLine(" +
    prodln +
    ",\"product_\");' class=''>" +
    parkhidden +
    "</select>";

  var c1 = p.insertCell(5);
  c1.innerHTML =
    "<span style='vertical-align: top;' class='product_description_label'>" +
    SUGAR.language.get(module_sugar_grp1, "LBL_COMMISONSERVICE") +
    "</span>";
  c1.innerHTML +=
    "<input type='text' style='width: 97%'name='product_commisonservice[" +
    prodln +
    "]' id='product_commisonservice" +
    prodln +
    "'  maxlength='50' value='' title='' tabindex='116' onblur='calculateLine(" +
    prodln +
    ",\"product_\");' onblur='calculateLine(" +
    prodln +
    ",\"product_\");' class='product_commisonservice_text'><input type='hidden' name='product_product_commisonservice[" +
    prodln +
    "]' id='product_product_commisonservice" +
    prodln +
    "' value=''  />";

  var c2 = p.insertCell(6);

  c2.innerHTML +=
    "<br><select tabindex='116' style='width: 97%' name='product_select_user[" +
    prodln +
    "]' id='product_select_user" +
    prodln +
    "' onchange='calculateLine(" +
    prodln +
    ', "product_"); updateHiddenInput(' +
    prodln +
    ", \"product_\");' class=''>" +
    html +
    "</select><input type='hidden' name='product_user_commsvc[" +
    prodln +
    "]' id='product_user_commsvc" +
    prodln +
    "' value=''  />";

  var e = p.insertCell(7);
  e.innerHTML =
    "<span style='vertical-align: top;' class='product_description_label'>" +
    SUGAR.language.get(module_sugar_grp1, "LBL_UNIT_PRICE") +
    "</span>";
  e.innerHTML +=
    "<input type='text' style='width: 97%;'  name='product_product_unit_price[" +
    prodln +
    "]' id='product_product_unit_price" +
    prodln +
    "' maxlength='50' value='' title='' tabindex='116' readonly='readonly' onblur='calculateLine(" +
    prodln +
    ",\"product_\");' onblur='calculateLine(" +
    prodln +
    ",\"product_\");' class='product_unit_price'>";

  if (typeof currencyFields !== "undefined") {
    currencyFields.push("product_product_unit_price" + prodln);
  }
  var g = p.insertCell(8);
  g.innerHTML =
    "<span style='vertical-align: top;' class='product_description_label'>" +
    SUGAR.language.get(module_sugar_grp1, "LBL_TOTAL_PRICE") +
    "</span>";
  g.innerHTML +=
    "<input type='text' style='width: 97%;'  name='product_product_total_price[" +
    prodln +
    "]' id='product_product_total_price" +
    prodln +
    "' maxlength='50' value='' title='' tabindex='116' readonly='readonly' class='product_total_price'><input type='hidden' name='product_group_number[" +
    prodln +
    "]' id='product_group_number" +
    prodln +
    "' value='" +
    groupid +
    "'>";

  ///--------------------------------------------dòng 3 --------------------------------
  var y = tablebody.insertRow(-1);
  y.id = "product_note_line" + prodln;

  var v = y.insertCell(0);
  v.colSpan = "2";
  v.setAttribute("valign", "top"); // Thêm dòng này để căn chỉnh lên đầu trang
  v.innerHTML =
    "<div style='width: 230px' id='main_image" +
    prodln +
    "'></div>";

  var h1 = y.insertCell(1);
  h1.colSpan = "2";
  h1.style.color = "rgb(68,68,68)";
  h1.setAttribute("valign", "top"); // Thêm dòng này để căn chỉnh lên đầu trang

  h1.innerHTML =
    "<div style='width: 98%;' id='table_size_" +
    prodln +
    "'></div>";

  var t = y.insertCell(2);
  t.colSpan = "2";
  t.style.color = "rgb(68,68,68)";
  t.setAttribute("valign", "top"); // Thêm dòng này để căn chỉnh lên đầu trang

  t.innerHTML =
    "<div style='width: 98%;' id='table_packing_" +
    prodln +
    "'></div>";

  var i = y.insertCell(3);
  i.colSpan = "3";
  i.style.color = "rgb(68,68,68)";
  i.setAttribute("valign", "top"); // Thêm dòng này để căn chỉnh lên đầu trang

  i.innerHTML =
    "<span style='vertical-align: top;' class='product_description_label'>" +
    SUGAR.language.get(module_sugar_grp1, "LBL_PRODUCT_NOTE") +
    "</span>";
  i.innerHTML +=
    "<textarea tabindex='116' style ='height: 94px; width: 98%' name='product_description[" +
    prodln +
    "]' id='product_description" +
    prodln +
    "'class='product_description'></textarea>";

  var g = tablebody.insertRow(-1);
  g.id = "br_line" + prodln;
  var a = g.insertCell(0);
  a.innerHTML = "&nbsp";

  addToValidate(
    "EditView",
    "product_product_id" + prodln,
    "id",
    true,
    "Please choose a product"
  );

  addAlignedLabels(prodln, "product");

  prodln++;

  return prodln - 1;
}

var addAlignedLabels = function (ln, type) {
  if (typeof type == "undefined") {
    type = "product";
  }
  if (type != "product") {
    console.error('type could be "product"');
  }
  var labels = [];
  $("tr#" + type + "_head td").each(function (i, e) {
    if (type == "product" && $(e).attr("colspan") > 1) {
      for (var i = 0; i < parseInt($(e).attr("colspan")); i++) {
        if (i == 0) {
          labels.push($(e).html());
        } else {
          labels.push("");
        }
      }
    } else {
      labels.push($(e).html());
    }
  });
  $("tr#" + type + "_line" + ln + " td").each(function (i, e) {
    $(e).prepend('<span class="alignedLabel">' + labels[i] + "</span>");
  });
};

function updateHiddenInput(prodln) {
  // Lấy giá trị của option được chọn
  var user_id = document.getElementById("product_select_user" + prodln).value;

  // Gán giá trị vào đối tượng input ẩn
  document.getElementById("product_user_commsvc" + prodln).value = user_id;
}

/**
 * Open product popup
 */

function openProductPopup(ln) {
  lineno = ln;
  var popupRequestData = {
    call_back_function: "setProductReturn",
    form_name: "EditView",
    field_to_name_array: {
      id: "product_product_id" + ln,
      name: "product_name" + ln,
      product_image: "product_product_image" + ln,

      part_number: "product_part_number" + ln,
      cost: "product_product_cost_price" + ln,
      price: "product_product_list_price" + ln,
      currency_id: "product_currency" + ln,
      sets_ctn: "product_sets_ctn" + ln,
      cbm: "product_cbm" + ln,
    },
  };

  open_popup("AOS_Products", 800, 850, "", true, true, popupRequestData);
}
function setProductReturn(popupReplyData) {
  set_return(popupReplyData);

  var tableContainer = document.getElementById("table_size_" + lineno);
  var tableContainer2 = document.getElementById("table_packing_" + lineno);

  var id_product =
    popupReplyData.name_to_value_array["product_product_id" + lineno];

  // Gọi display_size với một hàm callback
  display_size(id_product, function (tableHtml) {
    // Xử lý kết quả ở đây
    tableContainer.innerHTML = tableHtml;
  });

  display_packing(id_product, function (tableHtml) {
    // Xử lý kết quả ở đây
    tableContainer2.innerHTML = tableHtml;
  });

  var image = popupReplyData.name_to_value_array["product_product_image" + lineno];
  var imageContainer = document.getElementById("main_image" + lineno);


  // if (image !== '') {
  //     var lbl_img = document.getElementById("lbl_img" + lineno);
  //     if (lbl_img) lbl_img.remove();

  //     var img = document.getElementById("img" + lineno);
  //     if (img) img.remove();
  //     var imgElementp = document.createElement("p");
  //     imgElementp.textContent = SUGAR.language.get(module_sugar_grp1, "LBL_IMAGE_PRODUCT");
  //     imgElementp.id = "lbl_img" + lineno;

  //     var imgElement = document.createElement("img");
  //     imgElement.id = "img" + lineno;
  //     imgElement.src = "upload/" + image;
  //     imgElement.style.width = "97%";

  //     imageContainer.appendChild(imgElementp);
  //     imageContainer.appendChild(imgElement);
  // } else {
  //     var lbl_img = document.getElementById("lbl_img" + lineno);
  //     if (lbl_img) lbl_img.remove();

  //     var img = document.getElementById("img" + lineno);
  //     if (img) img.remove();
  // }


  formatListPrice(lineno);
  calculateTotal2(2, "");
}
document.addEventListener("DOMContentLoaded", function () {
  var stopLoop = false;

  // Lấy giá trị từ trường input
  for (var i = 0; i <= 100; i++) {
    if (stopLoop) {
      break;
    }

    (function (index) {
      var productIdElement = document.getElementById(
        "product_product_id" + index
      );

      if (productIdElement !== null) {
        var productId = productIdElement.value;

        display_packing(productId, function (tableHtml) {
          // Xử lý kết quả ở đây (bạn cần có một biến tableContainer)
          var tableContainer = document.getElementById(
            "table_packing_" + index
          );
          tableContainer.innerHTML = tableHtml;
        });
        display_size(productId, function (tableHtml) {
          // Xử lý kết quả ở đây (bạn cần có một biến tableContainer)
          var tableContainer = document.getElementById("table_size_" + index);
          tableContainer.innerHTML = tableHtml;
        });
        display_photo(productId, i);
      }
    })(i);
  }
});
function display_photo(id, i) {
  var currentURL = window.location.href;
  var index = currentURL.indexOf("/index.php");
  var desiredPart = currentURL.slice(0, index);
  var newURL = desiredPart + "/index.php?entryPoint=get_image_product";

  var xhr = new XMLHttpRequest();

  // Set up the request
  xhr.open("POST", newURL, true);
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

  // Set the callback function to handle the response
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4) {
      if (xhr.status === 200) {
        // Handle the response data here
        var responseData = xhr.responseText;

        var imgElementp = document.createElement("p");
        imgElementp.textContent = SUGAR.language.get(module_sugar_grp1, "LBL_IMAGE_PRODUCT");
        imgElementp.id = "lbl_img" + i;

        // Create img element
        var imgElement = document.createElement("img");
        imgElement.id = "img" + i;



        // Set the src attribute
        var imageName = responseData.replace(/["']/g, "");


        imgElement.src = "upload/" + imageName;
        imgElement.style.width = "98%";

        // Get the main_image div and append the image element to it
        var mainImageDiv = document.getElementById("main_image" + i);
        mainImageDiv.innerHTML = ""; // Clear existing content
        mainImageDiv.appendChild(imgElementp);
        mainImageDiv.appendChild(imgElement);
        if (imageName == -1) {
          console.log("allo");
            var existingImgP = document.getElementById("lbl_img" + i);
            if (existingImgP) {
                existingImgP.remove();
            }

            var existingImg = document.getElementById("img" + i);
            if (existingImg) {
                existingImg.remove();
            }
        }

      
    }
    }
  };

  // Send the request with the provided ID
  xhr.send("id=" + id);
}
//sản phẩm
function display_size(id, callback) {
  var currentURL = window.location.href;
  var index = currentURL.indexOf("/index.php");
  var desiredPart = currentURL.slice(0, index);
  var newURL = desiredPart + "/index.php?entryPoint=Aos_pos_form_sizes";

  var id = id;

  var xhr = new XMLHttpRequest();

  // Set up the request
  xhr.open("POST", newURL, true);
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

  // Set the callback function to handle the response
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4) {
      if (xhr.status === 200) {
        // Handle the response data here
        var sizes = JSON.parse(xhr.responseText);

        var length = sizes.length > 3 ? 4 : sizes.length;
         var  tableHtml ='';
        if (length > 0) {
          // Create the HTML table
          tableHtml +=
            '<p style="font-size: 13px">' + SUGAR.language.get(module_sugar_grp1, "LBL_SIZE_PRODUCTS") + '</p> <table width="100%" style="border-collapse: collapse; border: 1px solid #ffffff;">';
          tableHtml += "<tr>";
          tableHtml +=
            "<th style='border: 1px solid #ffffff; font-size: 12px; background-color: rgba(118, 168, 162, 0.21); text-align: center;'>" +
            SUGAR.language.get(module_sugar_grp1, "LBL_PACKING_L_CM") +
            "</th>";
          tableHtml +=
            "<th style='border: 1px solid #ffffff; font-size: 12px; background-color: rgba(118, 168, 162, 0.21); text-align: center;'>" +
            SUGAR.language.get(module_sugar_grp1, "LBL_PACKING_W_CM") +
            "</th>";
          tableHtml +=
            "<th style='border: 1px solid #ffffff; font-size: 12px; background-color: rgba(118, 168, 162, 0.21); text-align: center;'>" +
            SUGAR.language.get(module_sugar_grp1, "LBL_PACKING_H_CM") +
            "</th>";

          tableHtml += "</tr>";
        
        // Add data rows to the HTML table
        for (var i = 0; i < length; i++) {
          var sizeL = sizes[i].size_l;
          var sizeW = sizes[i].size_w;
          var sizeH = sizes[i].size_h;

          tableHtml += "<tr>";
          tableHtml +=
            '<td style="border: 1px solid #ffffff; font-size: 12px; background-color: rgba(118, 168, 162, 0.21); text-align: center;">' +
            sizeL +
            "</td>";
          tableHtml +=
            '<td style="border: 1px solid #ffffff; font-size: 12px; background-color: rgba(118, 168, 162, 0.21); text-align: center;">' +
            sizeW +
            "</td>";
          tableHtml +=
            '<td style="border: 1px solid #ffffff; font-size: 12px; background-color: rgba(118, 168, 162, 0.21); text-align: center;">' +
            sizeH +
            "</td>";
          tableHtml += "</tr>";
        }

        tableHtml += "</table>";
        }else {
          tableHtml = '';
        }
        // Call the callback function with the tableHtml
        callback(tableHtml);
      }
    }
  };

  // Prepare and send the request body
  var requestBody = "id=" + encodeURIComponent(id);
  xhr.send(requestBody);
}
//đống gói
function display_packing(data, callback) {
  var currentURL = window.location.href;
  var index = currentURL.indexOf("/index.php");
  var desiredPart = currentURL.slice(0, index);
  var newURL = desiredPart + "/index.php?entryPoint=Aos_pos_packing_size";

  var id = data;

  var xhr = new XMLHttpRequest();

  // Set up the request
  xhr.open("POST", newURL, true);
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

  // Set the callback function to handle the response
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4) {
      if (xhr.status === 200) {
        // Handle the response data here
        console.log(xhr.responseText);

        var packings = JSON.parse(xhr.responseText);

        var length = packings.length > 3 ? 4 : packings.length;
        var tableHtml = '';
        if (length > 0) {
          // Create the HTML table
         tableHtml  +=
          '<p style="font-size: 13px">' + SUGAR.language.get(module_sugar_grp1, "LBL_PACKING_SIZE") + '</p><table width="100%" style="border-collapse: collapse; border: 1px solid #ffffff;">';

          tableHtml += "<tr>";
          tableHtml +=
            "<th style='border: 1px solid #ffffff; font-size: 12px; background-color: rgba(118, 168, 162, 0.21); text-align: center;'>" +
            SUGAR.language.get(module_sugar_grp1, "LBL_PACKING_L_CM") +
            "</th>";
          tableHtml +=
            "<th style='border: 1px solid #ffffff; font-size: 12px; background-color: rgba(118, 168, 162, 0.21); text-align: center;'>" +
            SUGAR.language.get(module_sugar_grp1, "LBL_PACKING_W_CM") +
            "</th>";
          tableHtml +=
            "<th style='border: 1px solid #ffffff; font-size: 12px; background-color: rgba(118, 168, 162, 0.21); text-align: center;'>" +
            SUGAR.language.get(module_sugar_grp1, "LBL_PACKING_H_CM") +
            "</th>";

          tableHtml += "</tr>";
        
        // Add data rows to the HTML table
        for (var i = 0; i < length; i++) {
          var packingL = packings[i].packing_l;
          var packingW = packings[i].packing_w;
          var packingH = packings[i].packing_h;

          tableHtml += "<tr>";
          tableHtml +=
            '<td style="border: 1px solid #ffffff; font-size: 12px; background-color: rgba(118, 168, 162, 0.21); text-align: center;">' +
            packingL +
            "</td>";
          tableHtml +=
            '<td style="border: 1px solid #ffffff; font-size: 12px; background-color: rgba(118, 168, 162, 0.21); text-align: center;">' +
            packingW +
            "</td>";
          tableHtml +=
            '<td style="border: 1px solid #ffffff; font-size: 12px; background-color: rgba(118, 168, 162, 0.21); text-align: center;">' +
            packingH +
            "</td>";
          tableHtml += "</tr>";
        }

        tableHtml += "</table>";
      }else {
        tableHtml = '';
      }
      
        callback(tableHtml);
      }
    }
  };

  // Prepare and send the request body
  var requestBody = "id=" + encodeURIComponent(id);
  xhr.send(requestBody);
}

function insertProductHeader(tableid) {
  tablehead = document.createElement("thead");
  tablehead.id = tableid + "_head";
  tablehead.style.display = "none";
  document.getElementById(tableid).appendChild(tablehead);

  var x = tablehead.insertRow(-1);
  x.id = "product_head";

  var a = x.insertCell(0);
  a.style.color = "rgb(68,68,68)";
  a.innerHTML = SUGAR.language.get(module_sugar_grp1, "LBL_PRODUCT_QUANITY");

  var b = x.insertCell(1);
  b.style.color = "rgb(68,68,68)";
  b.innerHTML = SUGAR.language.get(module_sugar_grp1, "LBL_PART_NUMBER");

  var b1 = x.insertCell(2);
  b1.colSpan = "2";
  b1.style.color = "rgb(68,68,68)";
  b1.innerHTML = SUGAR.language.get(module_sugar_grp1, "LBL_PRODUCT_NAME");

  var d = x.insertCell(3);
  d.style.color = "rgb(68,68,68)";
  d.innerHTML = SUGAR.language.get(module_sugar_grp1, "LBL_SETS_CTN");

  var f = x.insertCell(4);
  f.style.color = "rgb(68,68,68)";
  f.innerHTML = SUGAR.language.get(module_sugar_grp1, "LBL_TOTAL_CTNS");

  var k = x.insertCell(5);
  k.style.color = "rgb(68,68,68)";
  k.innerHTML = SUGAR.language.get(module_sugar_grp1, "LBL_CBM");

  var l = x.insertCell(6);
  l.style.color = "rgb(68,68,68)";
  l.innerHTML = SUGAR.language.get(module_sugar_grp1, "LBL_TOTAL_CBM");

  var c = x.insertCell(7);
  c.style.color = "rgb(68,68,68)";
  c.innerHTML = SUGAR.language.get(module_sugar_grp1, "LBL_LIST_PRICE");

  var h = x.insertCell(8);
  h.style.color = "rgb(68,68,68)";
  h.innerHTML = "&nbsp;";
}

function insertGroup() {
  if (!enable_groups && groupn > 0) {
    return;
  }
  var tableBody = document.createElement("tr");
  tableBody.id = "group_body" + groupn;
  tableBody.className = "group_body";
  document.getElementById("lineItems").appendChild(tableBody);

  var a = tableBody.insertCell(0);
  a.colSpan = "100";
  var table = document.createElement("table");
  table.id = "group" + groupn;
  table.style.width = "150%";
  table.className = "group";

  table.style.whiteSpace = "nowrap";
  table.style.marginLeft = "-210px";
  // Adjusting the top margin of the table to 40px
  table.style.marginTop = "40px";

  a.appendChild(table);

  tableheader = document.createElement("thead");
  table.appendChild(tableheader);
  var header_row = tableheader.insertRow(-1);

  if (enable_groups) {
    var header_cell = header_row.insertCell(0);
    header_cell.scope = "row";
    header_cell.colSpan = "8";
    header_cell.innerHTML =
      SUGAR.language.get(module_sugar_grp1, "LBL_GROUP_NAME") +
      ":&nbsp;&nbsp;<input name='group_name[]' id='" +
      table.id +
      "name' maxlength='255'  title='' tabindex='120' type='text' class='group_name'><input type='hidden' name='group_id[]' id='" +
      table.id +
      "id' value=''><input type='hidden' name='group_group_number[]' id='" +
      table.id +
      "group_number' value='" +
      groupn +
      "'>";

    var header_cell_del = header_row.insertCell(1);
    header_cell_del.scope = "row";
    header_cell_del.colSpan = "2";
    header_cell_del.innerHTML =
      "<span title='" +
      SUGAR.language.get(module_sugar_grp1, "LBL_DELETE_GROUP") +
      "' style='float: right;'><a style='cursor: pointer;' id='deleteGroup' tabindex='116' onclick='markGroupDeleted(" +
      groupn +
      ")' class='delete_group'><span class=\"suitepicon suitepicon-action-clear\"></span></a></span><input type='hidden' name='group_deleted[]' id='" +
      table.id +
      "deleted' value='0'>";
  }

  var productTableHeader = document.createElement("thead");
  table.appendChild(productTableHeader);
  var productHeader_row = productTableHeader.insertRow(-1);
  var productHeader_cell = productHeader_row.insertCell(0);
  productHeader_cell.colSpan = "100";
  var productTable = document.createElement("table");
  productTable.id = "product_group" + groupn;
  productTable.className = "product_group";
  productHeader_cell.appendChild(productTable);

  insertProductHeader(productTable.id);

  tablefooter = document.createElement("tfoot");
  table.appendChild(tablefooter);
  var footer_row = tablefooter.insertRow(-1);
  var footer_cell = footer_row.insertCell(0);

  footer_cell.scope = "row";
  footer_cell.colSpan = "20";
  footer_cell.innerHTML =
    "<input type='button' tabindex='116' class='button add_product_line' value='" +
    SUGAR.language.get(module_sugar_grp1, "LBL_ADD_PRODUCT_LINE") +
    "' id='" +
    productTable.id +
    "addProductLine' onclick='insertProductLine(\"" +
    productTable.id +
    '","' +
    groupn +
    "\")' />";

  if (enable_groups) {
    footer_cell.innerHTML +=
      "<span class='totals'><label>" +
      SUGAR.language.get(module_sugar_grp1, "LBL_TOTAL_AMT") +
      ":</label><input name='group_lp_total_amt[]' id='" +
      table.id +
      "lp_total_amt' class='group_lp_total_amt' maxlength='26' value='' title='' tabindex='120' type='text' readonly></span>";

    var footer_row2 = tablefooter.insertRow(-1);
    var footer_cell2 = footer_row2.insertCell(0);
    footer_cell2.scope = "row";
    footer_cell2.colSpan = "20";
    footer_cell2.innerHTML =
      "<span class='totals'><label>" +
      SUGAR.language.get(module_sugar_grp1, "LBL_TOTAL_CTNS") +
      ":</label><input name='group_total_ctns[]' id='" +
      table.id +
      "total_ctns' class='group_total_ctns' maxlength='26' value='' title='' tabindex='120' type='text' readonly></label>";

    var footer_row3 = tablefooter.insertRow(-1);
    var footer_cell3 = footer_row3.insertCell(0);
    footer_cell3.scope = "row";
    footer_cell3.colSpan = "20";
    footer_cell3.innerHTML =
      "<span class='totals'><label>" +
      SUGAR.language.get(module_sugar_grp1, "LBL_TOTAL_CBM") +
      ":</label><input name='group_total_cbm[]' id='" +
      table.id +
      "lp_total_cbm' class='group_total_cbm'  maxlength='26' value='' title='' tabindex='120' type='text' readonly></span>";

    var footer_row4 = tablefooter.insertRow(-1);
    var footer_cell4 = footer_row4.insertCell(0);
    footer_cell4.scope = "row";
    footer_cell4.colSpan = "20";
    footer_cell4.innerHTML =
      "<span class='totals'><label>" +
      SUGAR.language.get(module_sugar_grp1, "LBL_DISCOUNT_AMOUNT") +
      ":</label><input name='group_lp_discount_amount[]' id='" +
      table.id +
      "lp_discount_amount' class='group_lp_discount_amount' maxlength='26' value='' title='' tabindex='120' type='text' readonly></label>";

    var footer_row5 = tablefooter.insertRow(-1);
    var footer_cell5 = footer_row5.insertCell(0);
    footer_cell5.scope = "row";
    footer_cell5.colSpan = "20";
    footer_cell5.innerHTML =
      "<span class='totals'><label>" +
      SUGAR.language.get(module_sugar_grp1, "LBL_SUBTOTAL_AMOUNT") +
      ":</label><input name='group_lp_subtotal_amount[]' id='" +
      table.id +
      "lp_subtotal_amount' class='group_lp_subtotal_amount'  maxlength='26' value='' title='' tabindex='120' type='text' readonly></span>";

    var footer_row6 = tablefooter.insertRow(-1);
    var footer_cell6 = footer_row6.insertCell(0);
    footer_cell6.scope = "row";
    footer_cell6.colSpan = "20";
    footer_cell6.innerHTML =
      "<span class='totals'><label>" +
      SUGAR.language.get(module_sugar_grp1, "LBL_TAX_AMOUNT") +
      ":</label><input name='group_lp_tax_amount[]' id='" +
      table.id +
      "lp_tax_amount' class='group_lp_tax_amount' maxlength='26' value='' title='' tabindex='120' type='text' readonly></span>";

    if (document.getElementById("subtotal_tax_amount") !== null) {
      var footer_row7 = tablefooter.insertRow(-1);
      var footer_cell7 = footer_row8.insertCell(0);
      footer_cell7.scope = "row";
      footer_cell7.colSpan = "20";
      footer_cell7.innerHTML =
        "<span class='totals'><label>" +
        SUGAR.language.get(module_sugar_grp1, "LBL_SUBTOTAL_TAX_AMOUNT") +
        ":</label><input name='group_lp_subtotal_tax_amount[]' id='" +
        table.id +
        "lp_subtotal_tax_amount' class='group_lp_subtotal_tax_amount' maxlength='26' value='' title='' tabindex='120' type='text' readonly></span>";

      if (typeof currencyFields !== "undefined") {
        currencyFields.push("" + table.id + "subtotal_tax_amount");
      }
    }
    var footer_row8 = tablefooter.insertRow(-1);
    var footer_cell8 = footer_row8.insertCell(0);
    footer_cell8.scope = "row";
    footer_cell8.colSpan = "20";
    footer_cell8.innerHTML =
      "<span class='totals'><label>" +
      SUGAR.language.get(module_sugar_grp1, "LBL_GROUP_TOTAL") +
      ":</label><input name='group_lp_total_amount[]' id='" +
      table.id +
      "lp_total_amount' class='lp_group_total_amount'  maxlength='26' value='' title='' tabindex='120' type='text' readonly></span>";

    if (typeof currencyFields !== "undefined") {
      currencyFields.push("" + table.id + "lp_total_amt");
      currencyFields.push("" + table.id + "lp_discount_amount");
      currencyFields.push("" + table.id + "lp_subtotal_amount");
      currencyFields.push("" + table.id + "lp_tax_amount");
      currencyFields.push("" + table.id + "lp_total_amount");
    }
  }

  groupn++;
  return groupn - 1;
}

/**
 * Mark Group Deleted
 */

function markGroupDeleted(gn) {
  document.getElementById("group_body" + gn).style.display = "none";

  var rows = document
    .getElementById("group_body" + gn)
    .getElementsByTagName("tbody");

  for (x = 0; x < rows.length; x++) {
    var input = rows[x].getElementsByTagName("button");
    for (y = 0; y < input.length; y++) {
      if (input[y].id.indexOf("delete_line") != -1) {
        input[y].click();
      }
    }
  }
}

/**
 * Mark line deleted
 */

function markLineDeleted(ln, key) {
  // collapse line; update deleted value
  if (confirm("Bạn có chắc chắn muốn xòa sản phẩm này không?")) {
    $.ajax({
      url: "index.php?entryPoint=deleteProduct",
      type: "POST",
      data: {
        id: $("#product_id" + ln).val(),
      },
      success: function (get_body) {
        document.getElementById(key + "body" + ln).style.display = "none";
        document.getElementById(key + "deleted" + ln).value = "1";
        document.getElementById(key + "delete_line" + ln).onclick = "";
        var groupid =
          "group" + document.getElementById(key + "group_number" + ln).value;

        if (checkValidate("EditView", key + "product_id" + ln)) {
          removeFromValidate("EditView", key + "product_id" + ln);
        }

        calculateTotal(groupid);
        calculateTotal();
        calculateTotal2(2, "");
      },
      error: function (error) {
        alert("không thể xóa sản phẩm");
      },
    });
  }
}

function calculateLine(ln, key) {
  var required = "product_list_price";

  if (document.getElementById(key + required + ln) === null) {
    required = "product_unit_price";
  }

  if (
    document.getElementById(key + "name" + ln) == null ||
    document.getElementById(key + "name" + ln) == undefined ||
    document.getElementById(key + required + ln).value === ""
  ) {
    return;
  }

  if (
    key === "product_" &&
    document.getElementById(key + "product_qty" + ln) !== null &&
    document.getElementById(key + "product_qty" + ln).value === ""
  ) {
    document.getElementById(key + "product_qty" + ln).value = 1;
  }

  var productUnitPrice = unformat2Number(
    document.getElementById(key + "product_unit_price" + ln).value
  );

  if (
    document.getElementById(key + "product_list_price" + ln) !== null &&
    document.getElementById(key + "product_discount" + ln) !== null &&
    document.getElementById(key + "discount" + ln) !== null
  ) {
    var listPrice = get_value(key + "product_list_price" + ln);
    var discount = get_value(key + "product_discount" + ln);
    var dis = document.getElementById(key + "discount" + ln).value;

    document.getElementById(key + "type_discount" + ln).value = dis;

    if (dis == "Amount") {
      if (discount > listPrice) {
        document.getElementById(key + "product_discount" + ln).value =
          listPrice;
        discount = listPrice;
      }
      productUnitPrice = listPrice - discount;
      document.getElementById(key + "product_unit_price" + ln).value =
        format2Number(listPrice - discount);
    } else if (dis == "Percentage") {
      if (discount > 100) {
        document.getElementById(key + "product_discount" + ln).value = 100;
        discount = 100;
      }

      discount = (discount / 100) * listPrice;
      productUnitPrice = listPrice - discount;
      document.getElementById(key + "product_unit_price" + ln).value =
        format2Number(listPrice - discount);
    } else {
      document.getElementById(key + "product_unit_price" + ln).value =
        document.getElementById(key + "product_list_price" + ln).value;
      document.getElementById(key + "product_discount" + ln).value = "";
      discount = 0;
    }
    document.getElementById(key + "product_list_price" + ln).value =
      format2Number(listPrice);
    document.getElementById(key + "product_discount" + ln).value =
      format2Number(
        unformat2Number(
          document.getElementById(key + "product_discount" + ln).value
        )
      );
    document.getElementById(key + "product_discount_amount" + ln).value =
      format2Number(-discount, 6);
  }

  var productQty = 1;
  if (document.getElementById(key + "product_qty" + ln) !== null) {
    productQty = unformat2Number(
      document.getElementById(key + "product_qty" + ln).value
    );
    Quantity_format2Number(ln);
  }

  var vat = unformatNumber(
    document.getElementById(key + "vat" + ln).value,
    ",",
    "."
  );

  var rose = unformatNumber(
    document.getElementById(key + "percent_commsvc" + ln).value,
    ",",
    "."
  );
  sets = document.getElementById(key + "sets_ctn" + ln).value;

  sets = sets === "" ? 0 : sets;

  var sets = unformatNumber(sets);
  var cbm = unformatNumber(document.getElementById(key + "cbm" + ln).value);

  var productTotalPrice = productQty * productUnitPrice;
  if (sets === 0) {
    var Total_CTNs = 0;
  } else {
    var Total_CTNs = productQty / sets;
  }
  var Total_CBM = Total_CTNs * cbm;

  var totalvat = (productTotalPrice * vat) / 100;
  var total_rose = (productTotalPrice * rose) / 100;

  if (total_tax) {
    productTotalPrice = productTotalPrice + totalvat;
  }

  document.getElementById(key + "percent_vat" + ln).value = vat;
  document.getElementById(key + "total_cbm" + ln).value = format2Number(
    Total_CBM,
    4
  );
  document.getElementById(key + "total_ctns" + ln).value = format2Number(
    Total_CTNs,
    4
  );
  // document.getElementById(key + 'user_commsvc' + ln).value = id_user;  //----------
  document.getElementById(key + "vat_amt" + ln).value = format2Number(totalvat);
  document.getElementById(key + "commisonservice" + ln).value =
    format2Number(total_rose);
  document.getElementById(key + "product_unit_price" + ln).value =
    format2Number(productUnitPrice);
  document.getElementById(key + "product_total_price" + ln).value =
    format2Number(productTotalPrice);
  var groupid = 0;
  if (enable_groups) {
    groupid = document.getElementById(key + "group_number" + ln).value;
  }
  groupid = "group" + groupid;

  calculateTotal(groupid, ln);
  calculateTotal();

  calculateTotal2(2, "");

}

function calculateAllLines() {
  $(".product_group").each(function (productGroupkey, productGroupValue) {
    $(productGroupValue)
      .find("tbody")
      .each(function (productKey, productValue) {
        calculateLine(productKey, "product_");
      });
  });
}

/**
 * Calculate totals
 */
function calculateTotal(key, ln) {
  if (typeof key === "undefined") {
    key = "lineItems";
  }
  var row = document.getElementById(key).getElementsByTagName("tbody");
  if (key == "lineItems") key = "";
  var length = row.length;
  var head = {};
  var tot_amt = 0;
  var subtotal = 0;
  var dis_tot = 0;
  var tax = 0;
  var totalQty = 0;
  var sets_ctn = 0;
  var totalCBM = 0;
  var rose = 0;

  for (var i = 0; i < length; i++) {
    var qty = 1;
    var list = null;
    var unit = 0;
    var deleted = 0;
    var dis_amt = 0;
    var sets = 0;
    var cbm = 0;
    var product_vat_amt = 0;

    var input = row[i].getElementsByTagName("input");
    for (var j = 0; j < input.length; j++) {
      if (input[j].id.indexOf("product_qty") != -1) {
        qty = unformat2Number(input[j].value);
      }
      if (input[j].id.indexOf("product_list_price") != -1) {
        list = unformat2Number(input[j].value);
      }
      if (input[j].id.indexOf("product_unit_price") != -1) {
        unit = unformat2Number(input[j].value);
      }
      if (input[j].id.indexOf("product_discount_amount") != -1) {
        dis_amt = unformat2Number(input[j].value);
      }
      if (input[j].id.indexOf("vat_amt") != -1) {
        product_vat_amt = unformat2Number(input[j].value);
      }
      if (input[j].id.indexOf("deleted") != -1) {
        deleted = input[j].value;
      }

      if (input[j].id.indexOf("product_sets_ctn") != -1) {
        sets = input[j].value;
      }
      if (input[j].id.indexOf("product_cbm") != -1) {
        cbm = input[j].value;
      }
    }

    if (deleted != 1 && key !== "") {
      head[row[i].parentNode.id] = 1;
    } else if (key !== "" && head[row[i].parentNode.id] != 1) {
      head[row[i].parentNode.id] = 0;
    }

    if (qty !== 0 && list !== null && deleted != 1) {
      tot_amt += list * qty;
    } else if (qty !== 0 && unit !== 0 && deleted != 1) {
      tot_amt += unit * qty;
    }

    if (dis_amt !== 0 && deleted != 1) {
      dis_tot += dis_amt * qty;
    }
    if (product_vat_amt !== 0 && deleted != 1) {
      tax += product_vat_amt;
    }
    if (sets != 0 && deleted != 1 && sets !== "") {
      sets_ctn += qty / sets;
      set_ctns = qty / sets;

      if (cbm !== 0) {
        totalCBM += set_ctns * cbm;
      }
    }
  }

  for (var h in head) {
    if (head[h] != 1 && document.getElementById(h + "_head") !== null) {
      document.getElementById(h + "_head").style.display = "none";
    }
  }

  subtotal = tot_amt + dis_tot;

  set_value(key + "lp_total_amt", tot_amt);
  set_value(key + "lp_subtotal_amount", subtotal);
  set_value(key + "lp_discount_amount", dis_tot);
  set_value2(key + "total_ctns", sets_ctn.toFixed(4));
  set_value2(key + "lp_total_cbm", totalCBM.toFixed(4));

  var shipping = get_value(key + "lp_shipping_amount");
  var shippingtax = get_value(key + "shipping_tax");
  var shippingtax_amt = shipping * (shippingtax / 100);

  set_value(key + "shipping_tax_amt", shippingtax_amt);

  tax += shippingtax_amt;

  set_value(key + "lp_tax_amount", tax);
  set_value(key + "lp_subtotal_tax_amount", subtotal + tax);
  set_value(key + "lp_total_amount", subtotal + tax + shipping);
}

function set_value(id, value) {
  if (document.getElementById(id) !== null) {
    document.getElementById(id).value = format2Number(value);
  }
}

function get_value(id) {
  if (document.getElementById(id) !== null) {
    return unformat2Number(document.getElementById(id).value);
  }
  return 0;
}

function unformat2Number(num) {
  return unformatNumber(num, num_grp_sep, dec_sep);
}

function format2Number(str, sig) {
  if (typeof sig === "undefined") {
    sig = sig_digits;
  }
  num = Number(str);
  if (sig == 2) {
    str = formatCurrency(num);
  } else {
    str = num.toFixed(sig);
  }

  str = str.split(/,/).join("{,}").split(/\./).join("{.}");
  str = str.split("{,}").join(num_grp_sep).split("{.}").join(dec_sep);

  return str;
}

function formatCurrency(strValue) {
  strValue = strValue.toString().replace(/\$|\,/g, "");
  dblValue = parseFloat(strValue);

  blnSign = dblValue == (dblValue = Math.abs(dblValue));
  dblValue = Math.floor(dblValue * 100 + 0.50000000001);
  intCents = dblValue % 100;
  strCents = intCents.toString();
  dblValue = Math.floor(dblValue / 100).toString();
  if (intCents < 10) strCents = "0" + strCents;
  for (var i = 0; i < Math.floor((dblValue.length - (1 + i)) / 3); i++)
    dblValue =
      dblValue.substring(0, dblValue.length - (4 * i + 3)) +
      "," +
      dblValue.substring(dblValue.length - (4 * i + 3));
  return (blnSign ? "" : "-") + dblValue + "." + strCents;
}

function Quantity_format2Number(ln) {
  var str = "";
  var qty = unformat2Number(
    document.getElementById("product_product_qty" + ln).value
  );
  if (qty === null) {
    qty = 1;
  }

  if (qty === 0) {
    str = "0";
  } else {
    str = format2Number(qty);
    if (sig_digits) {
      str = str.replace(/0*$/, "");
      str = str.replace(dec_sep, "~");
      str = str.replace(/~$/, "");
      str = str.replace("~", dec_sep);
    }
  }

  document.getElementById("product_product_qty" + ln).value = str;
}

function formatNumber(n, num_grp_sep, dec_sep, round, precision) {
  if (typeof num_grp_sep == "undefined" || typeof dec_sep == "undefined") {
    return n;
  }
  if (n === 0) n = "0";

  n = n ? n.toString() : "";
  if (n.split) {
    n = n.split(".");
  } else {
    return n;
  }
  if (n.length > 2) {
    return n.join(".");
  }
  if (typeof round != "undefined") {
    if (round > 0 && n.length > 1) {
      n[1] = parseFloat("0." + n[1]);
      n[1] = Math.round(n[1] * Math.pow(10, round)) / Math.pow(10, round);
      if (n[1].toString().includes(".")) {
        n[1] = n[1].toString().split(".")[1];
      } else {
        n[0] = (parseInt(n[0]) + n[1]).toString();
        n[1] = "";
      }
    }
    if (round <= 0) {
      n[0] =
        Math.round(parseInt(n[0], 10) * Math.pow(10, round)) /
        Math.pow(10, round);
      n[1] = "";
    }
  }
  if (typeof precision != "undefined" && precision >= 0) {
    if (n.length > 1 && typeof n[1] != "undefined") {
      n[1] = n[1].substring(0, precision);
    } else {
      n[1] = "";
    }
    if (n[1].length < precision) {
      for (var wp = n[1].length; wp < precision; wp++) {
        n[1] += "0";
      }
    }
  }
  regex = /(\d+)(\d{3})/;
  while (num_grp_sep !== "" && regex.test(n[0])) {
    n[0] = n[0].toString().replace(regex, "$1" + num_grp_sep + "$2");
  }
  return n[0] + (n.length > 1 && n[1] !== "" ? dec_sep + n[1] : "");
}

function check_form(formname) {
  calculateAllLines();
  if (
    typeof siw != "undefined" &&
    siw &&
    typeof siw.selectingSomething != "undefined" &&
    siw.selectingSomething
  )
    return false;
  return validate_form(formname, "");
}



function formatListPrice(ln) {
  calculateLine(ln, "product_");
  var product_id = document.getElementById("product_product_id"+ln).value;

  if (product_id) {
      var tableContainer = document.getElementById("table_size_" + ln);
      if (tableContainer) {
        display_size(product_id, function (tableHtml) {
          // Xử lý kết quả ở đây
          tableContainer.innerHTML = tableHtml;
        });
      }
    }
    if (product_id) {
      var tableContainer2 = document.getElementById("table_packing_" + ln);
      if (tableContainer2){
        display_packing(product_id, function (tableHtml) {
          // Xử lý kết quả ở đây
          tableContainer2.innerHTML = tableHtml;
        });
      }
    }

    if(product_id) {
      display_photo(product_id, ln);
    }




  }
