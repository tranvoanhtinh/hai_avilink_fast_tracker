<?php
/**
 * Advanced OpenSales, Advanced, robust set of sales modules.
 * @package Advanced OpenSales for SugarCRM
 * @copyright SalesAgility Ltd http://www.salesagility.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU AFFERO GENERAL PUBLIC LICENSE
 * along with this program; if not, see http://www.gnu.org/licenses
 * or write to the Free Software Foundation,Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA 02110-1301  USA
 *
 * @author SalesAgility <info@salesagility.com>
 */

function pos_line_items($focus, $field, $value, $view)
{

    global $sugar_config, $locale, $app_list_strings, $mod_strings;

    $enable_groups = (int)$sugar_config['aos']['lineItems']['enableGroups'];
    $total_tax = (int)$sugar_config['aos']['lineItems']['totalTax'];

    $html = '';

    if ($view == 'EditView') {
       
        $html .= '<script src="modules/AOS_pos/pos_line_items.js"></script>';
        if (file_exists('custom/modules/AOS_pos/pos_line_items.js')) {
            $html .= '<script src="custom/modules/AOS_pos/pos_line_items.js"></script>';
        }

        $html .= '<script language="javascript">var sig_digits = '.$locale->getPrecision().';';
        $html .= 'var module_sugar_grp1 = "'.$focus->module_dir.'";';
        $html .= 'var enable_groups = '.$enable_groups.';';
        $html .= 'var total_tax = '.$total_tax.';';
        $html .= '</script>';


        $html .= "<table border='0' cellspacing='4' id='lineItems'></table>";

        if ($enable_groups) {
            $html .= "<div style='padding-top: 10px; padding-bottom:10px;'>";
            $html .= "<input type=\"button\" tabindex=\"116\" class=\"button\" value=\"".$mod_strings['LBL_ADD_GROUP']."\" id=\"addGroup\" onclick=\"insertGroup(0)\" />";
            $html .= "</div>";
        }

        $html .= '<input type="hidden" name="vathidden" id="vathidden" value="'.get_select_options_with_id($app_list_strings['vat_list'], '').'">
                  <input type="hidden" name="discounthidden" id="discounthidden" value="'.get_select_options_with_id($app_list_strings['discount_list'], '').'"><input type="hidden" name="parkhidden" id="parkhidden" value="'.get_select_options_with_id($app_list_strings['parkservice_list'], '').'">';




        $html .= '<script>
            function handle_Jion_Data(data) {   
                var datas = JSON.parse(data);     
                var o = 0;         
                datas.forEach(function(item) {
                    console.log("aaaaaaaaaaaa");
                    lineItem = item.line_item;
                    groupItem = item.group_item;
                    insertLineItems(lineItem, groupItem);
 
                    o++;
                });
            }
        </script>';
$html .= '<script>
    var responseData; // Biến global để lưu trữ dữ liệu từ AJAX
    var ajaxProcessing = false; // Biến để theo dõi trạng thái xử lý của request AJAX
    var previous_in_id = ""; // Biến lưu trữ giá trị trước đó của inventory_in_id
    var previous_out_id = ""; // Biến lưu trữ giá trị trước đó của inventory_out_id
    var previous_invoice_id ="";

    function product_line_inventory(id, type) {
        var current_id = document.getElementById(id).value;

        if (type && current_id !== ((type === 1) ? previous_in_id : previous_out_id) && !ajaxProcessing) {
            ajaxProcessing = true; // Đánh dấu rằng request AJAX đang được xử lý

            $.ajax({
                type: "POST",
                url: "index.php?entryPoint=display_pos_itmes",
                data: {
                    type: type,
                    previous_id: current_id,
                },
                success: function(data) {
                    handle_Jion_Data(data);
                    ajaxProcessing = false; // Đánh dấu rằng request AJAX đã hoàn thành
                },
                error: function(error) {
                    // Handle errors if needed
                    console.error("Error:", error);
                    ajaxProcessing = false; // Đánh dấu rằng request AJAX đã hoàn thành
                }
            });
        } 
    }

    function handle_viewedit_coppy(id, type) {
        product_line_inventory(id, type);
    }

    setInterval(function() {

        var aos_inventoryinput_id = document.getElementById("aos_inventoryinput_id");
        if (aos_inventoryinput_id){
            var inventory_in_value = aos_inventoryinput_id.value;
            if (inventory_in_value !== previous_in_id) {

                handle_viewedit_coppy("aos_inventoryinput_id", 1);
                previous_in_id = inventory_in_value;
            }
        }

        var aos_inventoryoutput_id = document.getElementById("aos_inventoryoutput_id");
        if (aos_inventoryoutput_id){
            var inventory_out_value = aos_inventoryoutput_id.value;
            if (inventory_out_value !== previous_out_id) {

                handle_viewedit_coppy("aos_inventoryoutput_id", 2);
                previous_out_id = inventory_out_value;
            }
        }

        var aos_invoice_id = document.getElementById("aos_invoice_id");
        if (aos_invoice_id){
            var invoice_value = aos_invoice_id.value;
            if (invoice_value !== previous_invoice_id) {
                handle_viewedit_coppy("aos_invoice_id", 3); 
                previous_invoice_id = invoice_value; 
            }
        }
    }, 500);';
$html.='</script>';  

        if ($focus->id != '') {
            require_once('modules/AOS_Products_Pos/AOS_Products_Pos.php');
            require_once('modules/AOS_Pos_Line_Items_Detail/AOS_Pos_Line_Items_Detail.php');

            $sql = "SELECT pg.id, pg.group_id 
            FROM aos_products_pos pg 
            LEFT JOIN aos_pos_line_items_detail lig ON pg.group_id = lig.id 
            WHERE pg.parent_type = '".$focus->object_name."' 
            AND pg.parent_id = '".$focus->id."' 
            AND pg.deleted = 0 
            ORDER BY lig.number ASC, pg.number ASC";

            $result = $focus->db->query($sql);
            $html .= "<script>
                if(typeof sqs_objects == 'undefined'){var sqs_objects = new Array;}
                </script>";

            while ($row = $focus->db->fetchByAssoc($result)) {
                $line_item = new AOS_Products_Pos();
                $line_item->retrieve($row['id'], false);
                $line_item = json_encode($line_item->toArray());

                $group_item = 'null';
                if ($row['group_id'] != null) {
                    $group_item = new AOS_Pos_Line_Items_Detail();
                    $group_item->retrieve($row['group_id'], false);
                    $group_item = json_encode($group_item->toArray());
                }
                $html .= "<script>
                        insertLineItems(" . $line_item . "," . $group_item . ");
                    </script>";
            }
        }
       
        if (!$enable_groups) {
            $html .= '<script>insertGroup();</script>';
        }
    }else {
        if ($view == 'DetailView') {

            $html .= '<style>

    .style_th {
        background-color: rgba(118, 168, 162, 0.21);
         border: 1px solid #ffffff; /* Đường viền màu trắng, 1 pixel mỏng */
    }
    .style_td {
      border: 1px solid #ffffff; /* Đường viền màu trắng, 1 pixel mỏng */
    }

    .style_table {
      position: relative;
      left: -24%;
      top: 40px;
      width: 125%;
      font-size: 12px;
      font-weight: bold;
      /* Điền màu nền mong muốn vào đây */
      background-color: #f5f5f5;
}

</style>';

            $params = array('currency_id' => $focus->currency_id);

          $sql = "SELECT pg.id, pg.group_id 
            FROM aos_products_pos pg 
            LEFT JOIN aos_pos_line_items_detail lig ON pg.group_id = lig.id 
            WHERE pg.parent_type = '".$focus->object_name."' 
            AND pg.parent_id = '".$focus->id."' 
            AND pg.deleted = 0 
            ORDER BY lig.number ASC, pg.number ASC";

            $result = $focus->db->query($sql);
            $sep = get_number_seperators();

            $html .= "<table class='style_table' border='0' width='100%' cellpadding='0' cellspacing='0'>";

            $i = 0;
            $productCount = 0;
            $serviceCount = 0;
            $group_id = '';
            $groupStart = '';
            $groupEnd = '';
            $product = '';
 
            while ($row = $focus->db->fetchByAssoc($result)) {
                $line_item = new AOS_Products_Pos();
                $line_item->retrieve($row['id']);


                if ($enable_groups && ($group_id != $row['group_id'] || $i == 0)) {
                    $html .= $groupStart.$product.$groupEnd;
                    if ($i != 0) {
                        $html .= "<tr><td colspan='9' nowrap='nowrap'><br></td></tr>";
                    }
                    $groupStart = '';
                    $groupEnd = '';
                    $product = '';
                    $i = 1;
                    $productCount = 0;
             
                    $group_id = $row['group_id'];

                    $group_item = new AOS_Pos_Line_Items_Detail();
                    $group_item->retrieve($row['group_id']);

                    //col - group name
                    $groupStart .= "<tr>";
                    $groupStart .= "<td class='tabDetailViewDL' style='text-align: left;padding:2px;' scope='row'>&nbsp;</td>";
                    $groupStart .= "<td class='tabDetailViewDL' colspan='2'style='text-align: left;padding:2px;' scope='row'>".$mod_strings['LBL_GROUP_NAME'].":</td>";
                    $groupStart .= "<td class='tabDetailViewDL' colspan='16' style='text-align: left;padding:2px;'>".$group_item->name."</td>";
                    $groupStart .= "</tr>";

                    $groupEnd = "<tr><td colspan='9' nowrap='nowrap'><br></td></tr>";

                    //
                    $groupEnd .= "<tr>";

        

                    $groupEnd .= "<td class='tabDetailViewDL' colspan='17' style='text-align: right;padding:2px;' scope='row'>".$mod_strings['LBL_TOTAL_AMT'].":&nbsp;&nbsp;</td>";
                    $groupEnd .= "<td class='tabDetailViewDL' style='text-align: right;padding:2px;'>".currency_format_number($group_item->total_amt, $params)."</td>";
                    $groupEnd .= "</tr>";
//
                    $groupEnd .= "<tr>";
                    $groupEnd .= "<td class='tabDetailViewDL' colspan='17' style='text-align: right;padding:2px;' scope='row'>".$mod_strings['LBL_TOTAL_CTNS'].":&nbsp;&nbsp;</td>";
                    $groupEnd .= "<td class='tabDetailViewDL' style='text-align: right;padding:2px;'>".currency_format_number($group_item->total_ctns, $params)."</td>";
                    $groupEnd .= "</tr>";
                    $groupEnd .= "<tr>";
                    $groupEnd .= "<td class='tabDetailViewDL' colspan='17' style='text-align: right;padding:2px;' scope='row'>".$mod_strings['LBL_TOTAL_CBM'].":&nbsp;&nbsp;</td>";
                    $groupEnd .= "<td class='tabDetailViewDL' style='text-align: right;padding:2px;'>".currency_format_number($group_item->total_cbm, $params)."</td>";
                    $groupEnd .= "</tr>";
//

                    $groupEnd .= "<tr>";
                    $groupEnd .= "<td class='tabDetailViewDL' colspan='17' style='text-align: right;padding:2px;' scope='row'>".$mod_strings['LBL_DISCOUNT_AMOUNT'].":&nbsp;&nbsp;</td>";
                    $groupEnd .= "<td class='tabDetailViewDL' style='text-align: right;padding:2px;'>".currency_format_number($group_item->discount_amount, $params)."</td>";
                    $groupEnd .= "</tr>";


                    $groupEnd .= "<tr>";
                    $groupEnd .= "<td class='tabDetailViewDL' colspan='17' style='text-align: right;padding:2px;' scope='row'>".$mod_strings['LBL_SUBTOTAL_AMOUNT'].":&nbsp;&nbsp;</td>";
                    $groupEnd .= "<td class='tabDetailViewDL' style='text-align: right;padding:2px;'>".currency_format_number($group_item->subtotal_amount, $params)."</td>";
                    $groupEnd .= "</tr>";
                    $groupEnd .= "<tr>";
                    $groupEnd .= "<td class='tabDetailViewDL' colspan='17' style='text-align: right;padding:2px;' scope='row'>".$mod_strings['LBL_TAX_AMOUNT'].":&nbsp;&nbsp;</td>";
                    $groupEnd .= "<td class='tabDetailViewDL' style='text-align: right;padding:2px;'>".currency_format_number($group_item->tax_amount, $params)."</td>";
                    $groupEnd .= "</tr>";
                    $groupEnd .= "<tr>";
                    $groupEnd .= "<td class='tabDetailViewDL' colspan='17' style='text-align: right;padding:2px;' scope='row'>".$mod_strings['LBL_GRAND_TOTAL'].":&nbsp;&nbsp;</td>";
                    $groupEnd .= "<td class='tabDetailViewDL' style='text-align: right;padding:2px;'>".currency_format_number($group_item->total_amount, $params)."</td>";

                    $groupEnd .= "</tr>";
                               $product .= "<tr>";
                        
                    $product .= "<td width='3%' class='tabDetailViewDL  style_th' style='text-align: left;padding:2px;' scope='row'>".$mod_strings['LBL_ASC']."</td>";



                    $product .= "<td width='6%' class='tabDetailViewDL style_th' style='text-align: left;padding:2px;' scope='row'>".$mod_strings['LBL_IMAGE_PRODUCT']."</td>";    
                    $product .= "<td width='3%' class='tabDetailViewDL style_th' style='text-align: left;padding:2px;' scope='row'>".$mod_strings['LBL_PRODUCT_QUANITY']."</td>";
                    $product .= "<td width='6%' class='tabDetailViewDL style_th' style='text-align: left;padding:2px;' scope='row'>".$mod_strings['LBL_PRODUCT_NAME']."</td>"; 
                                



                    $product .= "<td width='6%' class='tabDetailViewDL style_th' style='text-align: left;padding:2px;' scope='row'>".$mod_strings['LBL_PART_NUMBER']."</td>";
                    $product .= "<td width='4%' class='tabDetailViewDL style_th' style='text-align: left;padding:2px;' scope='row'>".$mod_strings['LBL_SETS_CTN']."</td>";
                    $product .= "<td width='6%' class='tabDetailViewDL style_th' style='text-align: left;padding:2px;' scope='row'>".$mod_strings['LBL_CBM']."</td>";
                    $product .= "<td width='6%' class='tabDetailViewDL style_th' style='text-align: left;padding:2px;' scope='row'>".$mod_strings['LBL_TOTAL_CTNS']."</td>";
                    $product .= "<td width='6%' class='tabDetailViewDL style_th' style='text-align: left;padding:2px;' scope='row'>".$mod_strings['LBL_TOTAL_CBM']."</td>";
                    $product .= "<td width='6%' class='tabDetailViewDL style_th' style='text-align: left;padding:2px;' scope='row'>".$mod_strings['LBL_LIST_PRICE']."</td>";  

                    $product .= "<td width='12%' colspan='2' class='tabDetailViewDL style_th' style='text-align: center; padding: 8px; border: 1px solid white;' scope='row'>".$mod_strings['LBL_VAT_AMT']."</td>";
                    $product .= "<td width='18%' colspan='3' class='tabDetailViewDL style_th' style='text-align: center; padding: 8px; border: 1px solid white;' scope='row'>".$mod_strings['LBL_COMMISONSERVICE']."</td>";


                    $product .= "<td width='6%' class='tabDetailViewDL style_th' style='text-align: left;padding:2px;' scope='row'>".$mod_strings['LBL_DISCOUNT_AMT']."</td>";
                    $product .= "<td width='6%' class='tabDetailViewDL style_th' style='text-align: left;padding:2px;' scope='row'>".$mod_strings['LBL_SERVICE_PRICE']."</td>";
                    $product .= "<td width='6%' class='tabDetailViewDL style_th' style='text-align: left;padding:2px;' scope='row'>".$mod_strings['LBL_TOTAL_PRICE']."</td>";         
                    $product .= "</tr>";
                }
                if ($line_item->product_id != '0' && $line_item->product_id != null) {
                $product_note = wordwrap($line_item->description, 40, "<br />\n");

                        
         
                    
 
                    $product .= "<tr>";
                            $product .= "<td class='tabDetailViewDL style_td' style='text-align: left;padding:2px;' scope='row'>".++$productCount."</td>";

                        if ($line_item->product_id) {
                            $id_product = $line_item->product_id;
                            $sql1 = "SELECT * FROM `aos_products` WHERE id = '".$id_product."'";
                            $result1 = $focus->db->query($sql1);

                            while ($row = $focus->db->fetchByAssoc($result1)) {
                                if($row['product_image'] !=''){
                                $product .= "<td  class='tabDetailViewDL style_td' style='text-align: left;padding:2px;' scope='row'><img width='99%'src='upload/".$row['product_image']."' alt='Product Image'></td>";

                                }else{
                                $product .= "<td></td>";   
                                }
                            }
                        }
            
                    $product .= "<td class='tabDetailViewDF style_td' style='padding:2px;'>".stripDecimalPointsAndTrailingZeroes_pos(format_number($line_item->product_qty), $sep[1])."</td>";
                    $product .= "<td class='tabDetailViewDF style_td' style='padding:2px;'><a href='index.php?module=AOS_Products&action=DetailView&record=".$line_item->product_id."' class='tabDetailViewDFLink'>".$line_item->name."</a></td>";

                    $product .= "<td class='tabDetailViewDF style_td' style='text-align: right; padding:2px;'>".$line_item->part_number."</td>";
                    $product .= "<td class='tabDetailViewDF style_td' style='text-align: right; padding:2px;'>".$line_item->sets_ctn."</td>";
                    $product .= "<td class='tabDetailViewDF style_td' style='text-align: right; padding:2px;'>".$line_item->cbm."</td>";
                    $product .= "<td class='tabDetailViewDF style_td' style='text-align: right; padding:2px;'>".$line_item->total_ctns."</td>";
                    $product .= "<td class='tabDetailViewDF style_td' style='text-align: right; padding:2px;'>".$line_item->total_cbm."</td>";
                    $product .= "<td class='tabDetailViewDF style_td' style='text-align: right; padding:2px;'>".currency_format_number($line_item->product_list_price, $params)."</td>";
                    $product .= "<td class='tabDetailViewDF style_td' style='text-align: right; padding:2px; '>".$line_item->percent_vat."%</td>";
                    $product .= "<td class='tabDetailViewDF style_td' style='text-align: right; padding:2px;'>".currency_format_number($line_item->vat_amt,$params)."</td>";
                    $product .= "<td class='tabDetailViewDF style_td' style='text-align: right; padding:2px;'>".$line_item->percent_commsvc."%</td>";
                    $product .= "<td class='tabDetailViewDF style_td' style='text-align: right; padding:2px;'>".currency_format_number($line_item->commisonservice,$params)."</td>";
                    if ($line_item->user_commsvc) {
                        $id_user = $line_item->user_commsvc;
                        $sql2 = "SELECT * FROM `cs_cususer` WHERE id = '".$id_user."'";
                        $result2 = $focus->db->query($sql2);

                        while ($row = $focus->db->fetchByAssoc($result2)) {
                            $product .= "<td class='tabDetailViewDF style_td' style='text-align: right; padding:2px;'>".$row['name']."</td>";
                        }
                    } else {
                         $product .= "<td class='tabDetailViewDF style_td' style='text-align: right; padding:2px;'>&nbsp &nbsp</td>";
                    }
                  
                   if ($line_item->type_discount =='Percentage') {
                    $product .= "<td class='tabDetailViewDF style_td' style='text-align: right; padding:2px;'>".rtrim(rtrim(format_number($line_item->product_discount), '0'), $sep[1])."%</td>";

                  
                    } else {
                        $product .= "<td class='tabDetailViewDF style_td' style='text-align: right; padding:2px;'>".currency_format_number($line_item->product_discount,$params)."</td>";

                    }

 

                    $product .= "<td class='tabDetailViewDF style_td' style='text-align: right; padding:2px;'>".currency_format_number($line_item->product_unit_price,$params)."</td>";

                    $product .= "<td class='tabDetailViewDF style_td' style='text-align: right; padding:2px;'>".currency_format_number($line_item->product_total_price,$params)."</td>";
            
                    $product .= "</tr>";

        

                } 
            }
    
            $html .= $groupStart.$product.$groupEnd;
            $html .= "</table>";
            $html .= '<br><br>';
        }
    }
    return $html;
}




function stripDecimalPointsAndTrailingZeroes_pos($inputString, $decimalSeparator)
{
    return preg_replace('/'.preg_quote($decimalSeparator).'[0]+$/', '', $inputString);
}


function pos_display_shipping_vat($focus, $field, $value, $view)
{
    if ($view == 'EditView') {
        global $app_list_strings;

        if ($value != '') {
            $value = format_number($value);
        }

        $html = "<input id='shipping_tax_amt' type='text' tabindex='0' title='' value='".$value."' maxlength='26,6' size='22' name='shipping_tax_amt' onblur='calculateTotal(\"lineItems\");'>";
        $html .= "<select name='shipping_tax' id='shipping_tax' onchange='calculateTotal(\"lineItems\");' >".get_select_options_with_id($app_list_strings['vat_list'], (isset($focus->shipping_tax) ? $focus->shipping_tax : ''))."</select>";

        return $html;
    }
    return format_number($value);
}

