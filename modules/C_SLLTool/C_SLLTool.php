<?php
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.

 * SuiteCRM is an extension to SugarCRM Community Edition developed by Salesagility Ltd.
 * Copyright (C) 2011 - 2014 Salesagility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 ********************************************************************************/

/**
 * THIS CLASS IS FOR DEVELOPERS TO MAKE CUSTOMIZATIONS IN
 */
require_once('modules/C_SLLTool/C_SLLTool_sugar.php');
class C_SLLTool extends C_SLLTool_sugar {
	function __construct(){
		parent::__construct();
	}

    /*
     * This function using to get all dropdown by key
     */
    function getDropdown($key, $lang = '', $to_lang = '') {
        $dropdowns = $this->getAppListStrings($lang);
        $new_dropdowns = array();
        if(!empty($to_lang)) {
            $new_dropdowns = return_app_list_strings_language($to_lang);
        }
        $new_dropdown = array();
        if(!empty($new_dropdowns)) {
            $new_dropdown = $new_dropdowns[$key];
        }
        $dropdown = $dropdowns[$key];
        foreach($dropdown as $dropdown_key => $name) {
            $dropdown[$dropdown_key] = array(
                'cur_lang' => $name,
                'new_lang' => isset($new_dropdown) ? $new_dropdown[$dropdown_key] : ''
            );
        }
        return $dropdown;
    }
    /*
     * This function using to get all app list string
     */
    function getAppListStrings($lang = '') {
        global $sugar_config;
        if(empty($lang)) {
            global $current_language;
            $lang = (!empty($current_language)) ? $current_language : $sugar_config['default_language'];
        }
        $dropdowns = $this->return_app_list_strings_language($lang);
        if(empty($dropdowns)) {
            $dropdowns = $this->return_app_list_strings_language($sugar_config['default_language']);
        }
        foreach($dropdowns as $key => $dropdown) {
            if(!is_array($dropdown)) {
                unset($dropdowns[$key]);
            }
        }
        return $dropdowns;
    }
    function getAppStrings($lang = '', $to_lang = '') {
        global $sugar_config;
        if(empty($lang)) {
            global $current_language;
            $lang = (!empty($current_language)) ? $current_language : $sugar_config['default_language'];
        }
        $app_languages = $this->return_application_language($lang);
        if(empty($app_languages)) {
            $app_languages = $this->return_application_language($sugar_config['default_language']);
        }
        $new_app_languages = array();
        if(!empty($to_lang)) {
            $new_app_languages = $this->return_application_language($to_lang);
        }
        foreach($app_languages as $key => $label) {
            $app_languages[$key] = array(
                'cur_lang' => $label,
                'new_lang' => isset($new_app_languages) ? $new_app_languages[$key] : ''
            );
        }
        return $app_languages;
    }

    function getModStrings($module = 'Home', $lang = '', $to_lang = '') {
        global $sugar_config;
        if(empty($module)) {
            $module = 'Home';
        }
        if(empty($lang)) {
            global $current_language;
            $lang = (!empty($current_language)) ? $current_language : $sugar_config['default_language'];
        }
        $languages = return_module_language($lang, $module, true);
        if(empty($languages)) {
            $languages = return_module_language($sugar_config['default_language'], $module, true);
        }
        $new_languages = array();
        if(!empty($to_lang)) {
            $new_languages = return_module_language($to_lang, $module, true);
        }
        foreach($languages as $key => $label) {
            if(empty($label) || preg_match('/^LBL_/', $label, $matches))
            {
                unset($languages[$key]);
                continue;
            }
            $languages[$key] = array(
                'cur_lang' => strip_tags($label),
                'new_lang' => isset($new_languages) ? strip_tags($new_languages[$key]) : ''
            );
        }
        return $languages;
    }

    function getModuleList() {
        global $app_list_strings;
        require_once("modules/MySettings/TabController.php");
        $controller = new TabController();
        $tabs = $controller->get_tabs_system();
        $modules = array();
        foreach($tabs[0] as $key => $label) {
            $modules[$key] = $app_list_strings["moduleList"][$key];
        }
        return $modules;
    }

    function saveAppStrings($params) {
        require_once ('modules/Administration/Common.php');
        global $sugar_config;
        $app_strings = array();
        foreach ($params as $key => $value) {
            if (preg_match('/^label_/', $key) && strcmp($value, 'no_change') != 0) {
                $app_strings [ strtoupper(substr($key, 6)) ] = SugarCleaner::cleanHtml(from_html($value), false);
            }
        }
        $to_lang = (!empty($params['to_language'])) ? $params['to_language'] : $sugar_config['default_language'];
        $to_lang .= ".ZTranslatedByTool";
        $dir = "custom/Extension/application/Ext/Language";

        $contents = return_custom_app_list_strings_file_contents($to_lang);

        //write to contents
        $contents = str_replace("?>", '', $contents);
        if(empty($contents))$contents = "<?php";

        foreach($app_strings as $name=>$value){
            $contents = replace_or_add_app_string($name, $value, $contents);
        }
        if(!is_dir($dir))
        {
            $continue = mkdir_recursive($dir);
        }
        save_custom_app_list_strings_contents($contents, $to_lang, $dir);
        sugar_cache_reset();
        clearAllJsAndJsLangFilesWithoutOutput();
    }

    function saveAppListStrings($params) {
        require_once ('modules/Administration/Common.php');
        global $sugar_config;
        $dropdown = array();
        foreach ($params[$_REQUEST['dropdown_translation']] as $key => $value) {
            if (strcmp($value, 'no_change') != 0) {
                $dropdown [trim($key)] = SugarCleaner::cleanHtml(from_html($value), false);
            }
        }
        $to_lang = (!empty($params['to_language'])) ? $params['to_language'] : $sugar_config['default_language'];
        $to_lang .= ".ZTranslatedByTool";
        $dir = "custom/Extension/application/Ext/Language";

        $contents = return_custom_app_list_strings_file_contents($to_lang);

        //write to contents
        $contents = str_replace("?>", '', $contents);
        if(empty($contents))$contents = "<?php";

        // get the contents of the custom app list strings file
        $contents = replace_or_add_dropdown_type($params['dropdown_translation'],
            $dropdown, $contents);
        if(!is_dir($dir))
        {
            $continue = mkdir_recursive($dir);
        }
        save_custom_app_list_strings_contents($contents, $to_lang, $dir);
        sugar_cache_reset();
        clearAllJsAndJsLangFilesWithoutOutput();
    }

    function removeDirectory($path)
    {
        $files = glob($path . '/*');
        foreach ($files as $file) {
            is_dir($file) ? $this->removeDirectory($file) : unlink($file);
        }
        rmdir($path);
    }

    function return_application_language($language)
    {
        global $app_strings, $sugar_config, $app_list_strings;

        $cache_key = 'app_strings.' . $language;

        $temp_app_strings = $app_strings;
        $default_language = isset($sugar_config['default_language']) ? $sugar_config['default_language'] : null;

        $langs = array();
        if ($language != 'en_us') {
            $langs[] = 'en_us';
        }
        if ($default_language != 'en_us' && $language != $default_language) {
            $langs[] = $default_language;
        }

        $langs[] = $language;

        $app_strings_array = array();

        foreach ($langs as $lang) {
            $app_strings = array();
            if (file_exists("include/language/$lang.lang.php")) {
                include "include/language/$lang.lang.php";
                $GLOBALS['log']->info("Found language file: $lang.lang.php");
            }
            if (file_exists("include/language/$lang.lang.override.php")) {
                include "include/language/$lang.lang.override.php";
                $GLOBALS['log']->info("Found override language file: $lang.lang.override.php");
            }
            if (file_exists("include/language/$lang.lang.php.override")) {
                include "include/language/$lang.lang.php.override";
                $GLOBALS['log']->info("Found override language file: $lang.lang.php.override");
            }
            if (file_exists("custom/application/Ext/Language/$lang.lang.ext.php")) {
                include "custom/application/Ext/Language/$lang.lang.ext.php";
                $GLOBALS['log']->info("Found extended language file: $lang.lang.ext.php");
            }
            if (file_exists("custom/include/language/$lang.lang.php")) {
                include "custom/include/language/$lang.lang.php";
                $GLOBALS['log']->info("Found custom language file: $lang.lang.php");
            }
            $app_strings_array[] = $app_strings;
        }

        $app_strings = array();
        foreach ($app_strings_array as $app_strings_item) {
            $app_strings = sugarLangArrayMerge($app_strings, $app_strings_item);
        }

        if (!isset($app_strings)) {
            $GLOBALS['log']->fatal('Unable to load the application language strings');

            return;
        }

        // If we are in debug mode for translating, turn on the prefix now!
        if (!empty($sugar_config['translation_string_prefix'])) {
            foreach ($app_strings as $entry_key => $entry_value) {
                $app_strings[$entry_key] = $language . ' ' . $entry_value;
            }
        }
        if (isset($_SESSION['show_deleted'])) {
            $app_strings['LBL_DELETE_BUTTON'] = $app_strings['LBL_UNDELETE_BUTTON'];
            $app_strings['LBL_DELETE_BUTTON_LABEL'] = $app_strings['LBL_UNDELETE_BUTTON_LABEL'];
            $app_strings['LBL_DELETE_BUTTON_TITLE'] = $app_strings['LBL_UNDELETE_BUTTON_TITLE'];
            $app_strings['LBL_DELETE'] = $app_strings['LBL_UNDELETE'];
        }

        $app_strings['LBL_ALT_HOT_KEY'] = get_alt_hot_key();

        $return_value = $app_strings;
        $app_strings = $temp_app_strings;

        sugar_cache_put($cache_key, $return_value);

        return $return_value;
    }

    function return_app_list_strings_language($language)
    {
        global $app_list_strings;
        global $sugar_config;
		$cache_key = 'app_strings.' . $language;


        $default_language = isset($sugar_config['default_language']) ? $sugar_config['default_language'] : 'en_us';
        $temp_app_list_strings = $app_list_strings;

        $langs = array();
        if ($language != 'en_us') {
            $langs[] = 'en_us';
        }
        if ($default_language != 'en_us' && $language != $default_language) {
            $langs[] = $default_language;
        }
        $langs[] = $language;

        $app_list_strings_array = array();

        foreach ($langs as $lang) {
            $app_list_strings = array();
            if (file_exists("include/language/$lang.lang.php")) {
                include "include/language/$lang.lang.php";
                $GLOBALS['log']->info("Found language file: $lang.lang.php");
            }
            if (file_exists("include/language/$lang.lang.override.php")) {
                include "include/language/$lang.lang.override.php";
                $GLOBALS['log']->info("Found override language file: $lang.lang.override.php");
            }
            if (file_exists("include/language/$lang.lang.php.override")) {
                include "include/language/$lang.lang.php.override";
                $GLOBALS['log']->info("Found override language file: $lang.lang.php.override");
            }

            $app_list_strings_array[] = $app_list_strings;
        }

        $app_list_strings = array();
        foreach ($app_list_strings_array as $app_list_strings_item) {
            $app_list_strings = sugarLangArrayMerge($app_list_strings, $app_list_strings_item);
        }

        foreach ($langs as $lang) {
            if (file_exists("custom/application/Ext/Language/$lang.lang.ext.php")) {
                $app_list_strings = _mergeCustomAppListStrings("custom/application/Ext/Language/$lang.lang.ext.php", $app_list_strings);
                $GLOBALS['log']->info("Found extended language file: $lang.lang.ext.php");
            }
            if (file_exists("custom/include/language/$lang.lang.php")) {
                include "custom/include/language/$lang.lang.php";
                $GLOBALS['log']->info("Found custom language file: $lang.lang.php");
            }
        }

        if (!isset($app_list_strings)) {
            $GLOBALS['log']->fatal("Unable to load the application language file for the selected language ($language) or the default language ($default_language) or the en_us language");

            return;
        }

        $return_value = $app_list_strings;
        $app_list_strings = $temp_app_list_strings;

        sugar_cache_put($cache_key, $return_value);

        return $return_value;
    }


}
?>