SUGAR.util.doWhen(function () {
    return $('#tbl_translate').length;
},
function () {
    // Remove onclick
    $('.buttons').find('#SAVE').removeAttr('onclick');
    $('.buttons').find('#SAVE').attr('onclick', 'saveLabels();');
    // Remove label
    //$('#translation_layout_label').remove();
});

SUGAR.util.doWhen(function () {
    return $('#type_translation').length;
},
function () {
    showHideModule();
    showHideDropdown();
});

function setLabelForKey(ele) {
$(ele).closest('td').find('input[type=hidden]').val($(ele).val());
}

function saveLabels() {
var _form = document.getElementById('EditView');
_form.action.value = 'saveLabels';
if (check_form('EditView'))SUGAR.ajaxUI.submitForm(_form);
return false;
}

function getLablesByModule() {
var module_name = $('#module_translation').val();
var to_lang = $('#to_language').val();
if (module_name != '' && module_name != 'undefined') {
    SUGAR.ajaxUI.showLoadingPanel();
    $.ajax({
        url: "index.php?module=C_SLLTool&action=getLabelsModule&sugar_body_only=1",
        type: "POST",
        data: {"module_translation": module_name, "to_lang": to_lang},
        success: function (response) {
            var $response = $(response);
            var data = $response.filter("#work_area_translation").html();
            $("#work_area_translation").html(data);
            SUGAR.ajaxUI.hideLoadingPanel();
        }
    });
}
}

function getAppLanguages() {
var to_lang = $('#to_language').val();
if (to_lang != '' && to_lang != 'undefined') {
    SUGAR.ajaxUI.showLoadingPanel();
    $.ajax({
        url: "index.php?module=C_SLLTool&action=getAppLanguages&sugar_body_only=1",
        type: "POST",
        data: {"to_lang": to_lang},
        success: function (response) {
            var $response = $(response);
            var data = $response.filter("#work_area_translation").html();
            $("#work_area_translation").html(data);
            SUGAR.ajaxUI.hideLoadingPanel();
        }
    });
}
}

function getDropDown() {
var to_lang = $('#to_language').val();
var dropdown_translation = $('#dropdown_translation').val();
if (to_lang != '' && to_lang != 'undefined') {
    SUGAR.ajaxUI.showLoadingPanel();
    $.ajax({
        url: "index.php?module=C_SLLTool&action=getDropdown&sugar_body_only=1",
        type: "POST",
        data: {"to_lang": to_lang, "dropdown_translation": dropdown_translation, "type_translation": $("#type_translation").val()},
        success: function (response) {
            var $response = $(response);
            var data = $response.filter("#work_area_translation").html();
            $("#work_area_translation").html(data);
            SUGAR.ajaxUI.hideLoadingPanel();
        }
    });
}
}

function triggerChangeType() {
showHideModule();
showHideDropdown();
var type_translation = $('#type_translation').val();

var new_lang_title = SUGAR.language.get('C_SLLTool', 'LBL_NEW_LANGUAGE');

new_lang_title += ' (' +  $('#to_language > option:selected').text() + ')';

$('#new_lang_title').text(new_lang_title);
// Mod strings
if(type_translation == 'module') {
    getLablesByModule();
}
// App strings
else if(type_translation == 'application') {
    getAppLanguages();
}
// App list strings
else {
    getDropDown();
}
}

function showHideModule() {
if ($('#type_translation').val() == 'module') {
    $('#module_translation').closest('tr').show();
}
else {
    $('#module_translation').closest('tr').hide();
}
}

function showHideDropdown() {
if ($('#type_translation').val() == 'dropdown') {
    $('#dropdown_translation').closest('tr').show();
}
else {
    $('#dropdown_translation').closest('tr').hide();
}
}

function addLanguage(ele) {
var eles = $(ele).nextAll();
$(ele).hide();
for(var i = 0; i < eles.length; i++) {
    $(eles[i]).show();
}
}

function saveNewLanguage() {
var lang_key = $('#lang_key').val();
var lang_name = $('#lang_name').val();
if (lang_key != '' && lang_name != '') {
    SUGAR.ajaxUI.showLoadingPanel();
    $.ajax({
        url: "index.php?module=C_SLLTool&action=addLanguage&sugar_body_only=1",
        type: "POST",
        data: {"lang_key": lang_key, "lang_name": lang_name},
        success: function (response) {
            $("#to_language").html(response);
            var eles = $('#btn_add_lang').nextAll();
            $('#btn_add_lang').show();
            for(var i = 0; i < eles.length; i++) {
                $(eles[i]).hide();
            }
            SUGAR.ajaxUI.hideLoadingPanel();
        }
    });
}
else {
    alert(SUGAR.language.get('C_SLLTool', 'LBL_PLEASE_ENTER_KEY_AND_NAME'));
}
}

