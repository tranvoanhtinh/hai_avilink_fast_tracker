<?PHP
/******************************************
 * SuiteCRM Translations
 * @URL: https://crowdin.com/project/suitecrmtrans
 * @author SuiteCRM Community via Crowdin
 ******************************************/
$manifest = array( 
	'name' => 'Vietnamese (Viet Nam)',
	'description' => 'Build by MTS',
	'type' => 'langpack',
	'is_uninstallable' => 'Yes',
	'acceptable_sugar_versions' =>
		  array (),
	'acceptable_sugar_flavors' =>
		  array('CE'),
	'author' => 'SuiteCRM Community',
	'version' => '1.0.0',
	'published_date' => '2018-07-20',
      );
$installdefs = array(
	'id'=> 'vi_VN',
	'copy' => array(
	array('from'=> '<basepath>/include/language','to'=>'include'),
	array('from'=> '<basepath>/modules','to'=>'modules'),
   )
 );
?>