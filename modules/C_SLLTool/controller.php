<?php
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * SuiteCRM is an extension to SugarCRM Community Edition developed by Salesagility Ltd.
 * Copyright (C) 2011 - 2014 Salesagility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 ********************************************************************************/

require_once('modules/ModuleBuilder/MB/ModuleBuilder.php');
require_once('modules/ModuleBuilder/parsers/ParserFactory.php');
require_once('modules/ModuleBuilder/Module/StudioModuleFactory.php');
require_once 'modules/ModuleBuilder/parsers/constants.php';
require_once 'ModuleInstall/ModuleInstaller.php';

class C_SLLToolController extends SugarController
{
    protected $action_remap = array('index' => 'EditView', 'listview' => 'EditView');

    function action_saveLabels()
    {
        $sll_tool = new C_SLLTool();
        if ($_REQUEST['type_translation'] == 'module') {
            require_once 'modules/ModuleBuilder/parsers/parser.label.php';
            $_REQUEST['view_module'] = $_REQUEST['module_translation'];
            $parser = new ParserLabel ($_REQUEST['view_module'], null);
            $parser->handleSave($_REQUEST, $_REQUEST ['to_language']);
            $from = "custom/modules/{$_REQUEST['view_module']}/language/{$_REQUEST ['to_language']}.lang.php";
            $to = "custom/Extension/modules/{$_REQUEST['view_module']}/Ext/Language/{$_REQUEST ['to_language']}.ZTranslatedByTool.php";
            mkdir_recursive(dirname($to));
            copy($from, $to);
            if(file_exists("custom/modules/{$_REQUEST['view_module']}/Ext/Language/{$_REQUEST ['to_language']}.lang.ext.php")) {
                @unlink("custom/modules/{$_REQUEST['view_module']}/Ext/Language/{$_REQUEST ['to_language']}.lang.ext.php");
            }
        } else if ($_REQUEST['type_translation'] == 'application') {
            $sll_tool->saveAppStrings($_REQUEST);
        } else {
            $sll_tool->saveAppListStrings($_REQUEST);
        }
        global $sugar_config;
        $mi = new ModuleInstaller();
        $mi->rebuild_languages($sugar_config['languages']);
        SugarApplication::redirect("index.php?module=C_SLLTool&action=EditView&module_translation=" . $_REQUEST['module_translation'] . "&type_translation=" . $_REQUEST['type_translation'] . "&to_language=" . $_REQUEST['to_language'] . "&dropdown_translation=" . $_REQUEST['dropdown_translation']);
    }

    function action_getLabelsModule()
    {
        $ss = new Sugar_Smarty();
        $sll_tool = new C_SLLTool();
        $ss->assign('LANGUAGES', $sll_tool->getModStrings($_POST['module_translation'], null, $_POST['to_lang']));
        echo $ss->fetch('modules/C_SLLTool/tpls/translation_layout.tpl');
    }

    function action_getAppLanguages()
    {
        $ss = new Sugar_Smarty();
        $sll_tool = new C_SLLTool();
        $ss->assign('LANGUAGES', $sll_tool->getAppStrings(null, $_POST['to_lang']));
        echo $ss->fetch('modules/C_SLLTool/tpls/translation_layout.tpl');
    }

    function action_addLanguage()
    {
        global $sugar_config;
        $cf = new Configurator();
        $cf->config['languages'][$_POST['lang_key']] = $_POST['lang_name'];
        $cf->saveConfig();
        $ss = new Sugar_Smarty();
        echo get_select_options($sugar_config['languages'], $_POST['lang_key']);
    }

    function action_getDropdown()
    {
        $ss = new Sugar_Smarty();
        $sll_tool = new C_SLLTool();
        $ss->assign('LANGUAGES', $sll_tool->getDropdown($_POST['dropdown_translation'], null, $_POST['to_lang']));
        if($_POST['type_translation'] == 'dropdown') {
            $ss->assign('DROPDOWN_NAME', $_POST['dropdown_translation']);
        }
        echo $ss->fetch('modules/C_SLLTool/tpls/translation_layout.tpl');
    }

    function action_exportLanguage()
    {
        if(isset($_POST['submit'])) {

            $lang = isset($_POST['language']) ? $_POST['language'] : 'en_us';
            require_once('modules/Administration/Common.php');
            global $app_list_strings;
            $export_path = 'modules/C_SLLTool/langpack';
            $this->bean->removeDirectory($export_path);
            $module_export_path = "{$export_path}/modules";
            $app_export_path = $export_path;
            foreach ($app_list_strings['moduleList'] as $module_dir => $label) {
                $languages = return_module_language($lang, $module_dir, true);
                if (empty($languages)) continue;
                mkdir_recursive($module_export_path . "/" . $module_dir . "/language");
                $lang_path = $module_export_path . "/{$module_dir}/language/{$lang}.lang.php";
                write_array_to_file('mod_strings', $languages, $lang_path, "w");
            }
            // Get app list string


            $app_list_strings_lang = return_app_list_strings_language($lang);
            $app_list_strings_lang_content = "\$app_list_strings = " . var_export_helper( $app_list_strings_lang ) . ";";

            $app_strings_lang = return_application_language($lang);
            $app_strings_lang_content = "\$app_strings = " . var_export_helper( $app_strings_lang ) . ";";



            //write to contents
            $contents =   "<?php\n" .
                '// created: ' . date('Y-m-d H:i:s') . "\n//======== app_list_strings ========//\n";

            $contents .= $app_list_strings_lang_content . "//======== app_strings ========//\n" . $app_strings_lang_content;

            if (!is_dir("{$app_export_path}/language")) {
                $continue = mkdir_recursive("{$app_export_path}/include/language");
            }
            save_custom_app_list_strings_contents($contents, $lang, "{$app_export_path}/include/language");
            $this->build_zip($lang);
        }
        $this->view = 'exportLanguage';
    }

    function build_zip($lang = 'en_us')
    {
        echo "\n Building zip file...";
        $base_path = 'modules/C_SLLTool';
        $time = time();
        $file_name = $lang . "_" . $time;
        $zipFile = $base_path . '/' . $file_name . '.zip';
        $lang_pack_path = $base_path . '/langpack';
        if (!file_exists($lang_pack_path . '/manifest.php')) {
            copy($base_path . '/manifest.php', $lang_pack_path . '/manifest.php');
        }
        $zip = new ZipArchive();
        $zip->open($zipFile, ZipArchive::CREATE);

        $files = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($lang_pack_path),
            RecursiveIteratorIterator::LEAVES_ONLY
        );

        foreach ($files as $name => $file) {
            // Skip directories (they would be added automatically)
            if (!$file->isDir()) {
                // Get real and relative path for current file
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen(__DIR__) + 1);

                // Add current file to archive
                $zip->addFile($filePath, $relativePath);
            }
        }
        $zip->close();
        $this->downLoad($zipFile, $file_name);
    }

    function downLoad($download_location, $file_name) {
        $mime_type = "application/zip";
        header("Pragma: public");
        header("Cache-Control: maxage=1, post-check=0, pre-check=0");
        header('Content-type: ' . $mime_type);
        header("Content-Disposition: attachment; filename=\"" . $file_name . "\";");
        // disable content type sniffing in MSIE
        header("X-Content-Type-Options: nosniff");
        header("Content-Length: " . filesize($download_location));
        header('Expires: ' . gmdate('D, d M Y H:i:s \G\M\T', time() + 2592000));
        set_time_limit(0);

        readfile($download_location);
    }

}

?>
