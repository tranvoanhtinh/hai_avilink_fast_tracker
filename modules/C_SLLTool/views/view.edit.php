<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');



class C_SLLToolViewEdit extends ViewEdit {

    function __construct(){
        parent::__construct();
    }


    function display() {
        global $sugar_config;
        $sll_tool = new C_SLLTool();
        $selected_module = $_REQUEST['module_translation'];
        $selected_type = $_REQUEST['type_translation'];
        $selected_lang = isset($_REQUEST['to_language']) ? $_REQUEST['to_language'] : $GLOBALS['current_language'];
        $selected_dropdown = $_REQUEST['dropdown_translation'];

        $languages = $sll_tool->getModStrings($selected_module, $GLOBALS['current_language'], $selected_lang);
        if($selected_type == "application") {
            $languages = $sll_tool->getAppStrings($GLOBALS['current_language'], $selected_lang);
        }
        if($selected_type == "dropdown") {
            $languages = $sll_tool->getDropdown($selected_dropdown, $GLOBALS['current_language'], $selected_lang);
            $this->ss->assign('DROPDOWN_NAME', $selected_dropdown);
        }
        $dropdowns = $sll_tool->getAppListStrings($GLOBALS['current_language']);
        $dropdowns = array_keys($dropdowns);
        $set_label_dropdowns = array();
        foreach($dropdowns as $dropdown) {
            $set_label_dropdowns[$dropdown] = $dropdown;
        }
        $this->ss->assign('DROPDOWNS', get_select_options($set_label_dropdowns, $selected_dropdown));
        $this->ss->assign('MODULE_LIST', get_select_options($sll_tool->getModuleList(), $selected_module));
        $this->ss->assign('TYPE_OPTIONS', get_select_options(array(
            'module' => 'Module',
            'application' => 'Application',
            'dropdown' => 'Dropdown'
        ), $selected_type));
        $this->ss->assign('TO_LANGUAGES' , get_select_options($sugar_config['languages'], $selected_lang));
        $this->ss->assign('CURR_LANG' , $sugar_config['languages'][$GLOBALS['current_language']]);
        $this->ss->assign('TO_LANG' , $sugar_config['languages'][$selected_lang]);
        $this->ss->assign('LANGUAGES', $languages);
        $this->ss->assign('VERSION', time());
        parent::display();
    }
}
?>
