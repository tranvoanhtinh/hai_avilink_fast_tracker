<?php

class ViewexportLanguage extends SugarView {

    function display()
    {
        parent::display();
        global $sugar_config;
        $selected_lang = isset($GLOBALS['current_language']) ? $GLOBALS['current_language'] : 'en_us';
        $this->ss->assign('LANGUAGES' , get_select_options($sugar_config['languages'], $selected_lang));
        echo $this->ss->display('modules/C_SLLTool/tpls/export.tpl');
    }
}