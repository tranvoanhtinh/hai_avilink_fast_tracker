<style>
    {literal}
        #tbl_translate td, #tbl_translate th, #work_area_translation td, #work_area_translation th{
            border: 1px solid !important;
            padding: 10px 5px 10px 5px !important;
        }
        #tbl_translate input, #tbl_translate select, #work_area_translation input, #work_area_translation select {
            border-radius: unset !important;
            border: 1px solid !important;
    #work_area_translation::-webkit-scrollbar-track
    {
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
        border-radius: 5px;
        background-color: #F5F5F5;
    }

    #work_area_translation::-webkit-scrollbar {
        width: 0px;
        background-color: #F5F5F5;
    }

    #work_area_translation::-webkit-scrollbar-thumb
    {
        border-radius: 5px;
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
        background-color: #555;
    }
    {/literal}
</style>
<table style="width: 100%" id="tbl_translate">
    <tbody>
    <tr>
        <th style="text-align: left;width: 30%;">
            {$MOD.LBL_TYPE}
        </th>
        <td align="left">
            <select id="type_translation" name="type_translation" onchange="triggerChangeType()">
                {$TYPE_OPTIONS}
            </select>
        </td>
    </tr>
    <tr>
        <th style="text-align: left">{$MOD.LBL_MODULE}</th>
        <td align="left">
            <select id="module_translation" name="module_translation" onchange="getLablesByModule();">
                {$MODULE_LIST}
            </select>
        </td>
    </tr>
    <tr>
        <th style="text-align: left">{$MOD.LBL_DROPDOWN}</th>
        <td align="left">
            <select id="dropdown_translation" name="dropdown_translation" onchange="getDropDown();">
                {$DROPDOWNS}
            </select>
        </td>
    </tr>
    <tr>
        <th style="text-align: left">
            {$MOD.LBL_TO_LANGUAGE}
        </th>
        <td align="left">
            <select id="to_language" name="to_language" onchange="triggerChangeType();">
                {$TO_LANGUAGES}
            </select>
            <input type="button" id="btn_add_lang" onclick="addLanguage(this);" value="{$MOD.LBL_ADD}">
            <label style="display: none"><input type="text" id="lang_key" placeholder="{$MOD.LBL_ENTER_KEY}" name="lang_key"></label>
            <label style="display: none"><input type="text" id="lang_name" placeholder="{$MOD.LBL_ENTER_NAME}" name="lang_name"></label>
            <input style="display: none" type="button" id="btn_save_lang" onclick="saveNewLanguage();" value="{$MOD.LBL_SAVE}">
        </td>
    </tr>
    <tr>
        <th style="text-align: right;">{$MOD.LBL_CURRENT_LANGUAGE} ({$CURR_LANG})</th>
        <th colspan="2" id="new_lang_title">{$MOD.LBL_NEW_LANGUAGE} ({$TO_LANG})</th>
    </tr>
    </tbody>
</table>
<div style="overflow-y: scroll; height: 800px; margin-top: 15px;" id="work_area_translation">
    <table style="width: 100%;">
        {foreach from=$LANGUAGES key=KEY item=LABEL}
            {if $KEY}
                <tr>
                    <th align="right" width="30%">
                        {$LABEL.cur_lang}
                    </th>
                    <td align="left" width="70%">
                        {if $DROPDOWN_NAME }
                            <input type="hidden" id="label_{$KEY}" name="{$DROPDOWN_NAME}[{$KEY}]" value="{if $LABEL.new_lang}{$LABEL.new_lang}{else}{$LABEL.cur_lang}{/if}">
                        {else}
                            <input type="hidden" id="label_{$KEY}" name="label_{$KEY}" value="{if $LABEL.new_lang}{$LABEL.new_lang}{else}{$LABEL.cur_lang}{/if}">
                        {/if}
                        <input type="text" style="width: 100%" onblur="setLabelForKey(this);" value="{$LABEL.new_lang}">
                    </td>
                </tr>
            {/if}
        {/foreach}
    </table>
</div>
<script src="modules/C_SLLTool/js/EditView.js?v={$VERSION}"></script>