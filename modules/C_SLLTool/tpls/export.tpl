<style>
    {literal}
        #tbl_translate td, #tbl_translate th, #work_area_translation td, #work_area_translation th{
            border: 1px solid !important;
            padding: 10px 5px 10px 5px !important;
        }
        #tbl_translate input, #tbl_translate select, #work_area_translation input, #work_area_translation select {
            border-radius: unset !important;
            border: 1px solid !important;
    #work_area_translation::-webkit-scrollbar-track
    {
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
        border-radius: 5px;
        background-color: #F5F5F5;
    }

    #work_area_translation::-webkit-scrollbar {
        width: 0px;
        background-color: #F5F5F5;
    }

    #work_area_translation::-webkit-scrollbar-thumb
    {
        border-radius: 5px;
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
        background-color: #555;
    }
    {/literal}
</style>
<form name="frmExportLanguage" action="index.php" method="post">
    <input type="hidden" name="action" value="exportLanguage">
    <input type="hidden" name="module" value="C_SLLTool">
    <table style="width: 100%" id="tbl_translate">
        <tbody>
        <tr>
            <th style="text-align: left">
                {$MOD.LBL_LANGUAGE}
            </th>
            <td align="left">
                <select id="language" name="language">
                    {$LANGUAGES}
                </select>
            </td>
        </tr>
        <tr>
            <th style="text-align: right;"></th>
            <th colspan="2" id="new_lang_title"><input type="submit" class="button" name="submit" value="Export"></th>
        </tr>
        </tbody>
    </table>
</form>