<?php
$popupMeta = array (
    'moduleMain' => 'tttt_WorldfonePBX',
    'varName' => 'tttt_WorldfonePBX',
    'orderBy' => 'tttt_worldfonepbx.name',
    'whereClauses' => array (
  'name' => 'tttt_worldfonepbx.name',
),
    'searchInputs' => array (
  0 => 'tttt_worldfonepbx_number',
  1 => 'name',
  2 => 'priority',
  3 => 'status',
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => true,
    'name' => 'description',
  ),
  'PHONE_WORK' => 
  array (
    'type' => 'phone',
    'label' => 'LBL_PHONE_WORK',
    'width' => '10%',
    'default' => true,
    'name' => 'phone_work',
  ),
  'CALLUUID' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_CALLUUID',
    'width' => '10%',
    'default' => true,
    'name' => 'calluuid',
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
    'name' => 'assigned_user_name',
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
    'name' => 'date_entered',
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => true,
    'name' => 'date_modified',
  ),
),
);
