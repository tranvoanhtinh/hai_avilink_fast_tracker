<?php
$module_name = 'tttt_History_calls';
$listViewDefs [$module_name] = 
array (
  'WORKSTATUS' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_WORKSTATUS',
    'width' => '10%',
    'default' => true,
  ),
  'STARTTIME' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_STARTTIME',
    'width' => '10%',
    'default' => true,
  ),
  'USEREXTENSION' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_USEREXTENSION',
    'width' => '10%',
    'default' => true,
  ),
  'BILLDURATION' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_BILLDURATION',
    'width' => '10%',
    'default' => true,
  ),
  'FILE_GHIAM' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_FILE_GHIAM',
    'width' => '10%',
    'default' => true,
  ),
  'CALLSTATUS' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_CALLSTATUS',
    'width' => '10%',
    'default' => true,
  ),
  'DIRECTION' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_DIRECTION',
    'width' => '10%',
    'default' => true,
  ),
  'CALLUUID' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_CALLUUID',
    'width' => '10%',
    'default' => true,
  ),
  'CUSTOMERNUMBER' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_CUSTOMERNUMBER',
    'width' => '10%',
    'default' => true,
  ),
);
;
?>
