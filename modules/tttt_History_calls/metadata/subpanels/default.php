<?php
$module_name='tttt_History_calls';
$subpanel_layout = array (
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopCreateButton',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'popup_module' => 'tttt_WorldfonePBX',
    ),
  ),
  'where' => '',
  'list_fields' => 
  array (
    'workstatus' => 
    array (
      'type' => 'varchar',
      'vname' => 'LBL_WORKSTATUS',
      'width' => '10%',
      'default' => true,
    ),
    'starttime' => 
    array (
      'type' => 'varchar',
      'vname' => 'LBL_STARTTIME',
      'width' => '10%',
      'default' => true,
    ),
    'billduration' => 
    array (
      'type' => 'varchar',
      'vname' => 'LBL_BILLDURATION',
      'width' => '10%',
      'default' => true,
    ),
    'userextension' => 
    array (
      'type' => 'varchar',
      'vname' => 'LBL_USEREXTENSION',
      'width' => '10%',
      'default' => true,
    ),
    'calluuid' => 
    array (
      'type' => 'varchar',
      'vname' => 'LBL_CALLUUID',
      'width' => '10%',
      'default' => true,
    ),
    'callnote' => 
    array (
      'type' => 'varchar',
      'vname' => 'LBL_CALLNOTE',
      'width' => '10%',
      'default' => true,
    ),
  ),
);