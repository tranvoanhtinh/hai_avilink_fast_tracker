<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2018 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */
$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Assigned User Id',
  'LBL_ASSIGNED_TO_NAME' => 'Assigned to',
  'LBL_SECURITYGROUPS' => 'Security Groups',
  'LBL_SECURITYGROUPS_SUBPANEL_TITLE' => 'Security Groups',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Date Created',
  'LBL_DATE_MODIFIED' => 'Date Modified',
  'LBL_MODIFIED' => 'Modified By',
  'LBL_MODIFIED_NAME' => 'Modified By Name',
  'LBL_CREATED' => 'Created By',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_DELETED' => 'Deleted',
  'LBL_NAME' => 'Name',
  'LBL_CREATED_USER' => 'Created by User',
  'LBL_MODIFIED_USER' => 'Modified by User',
  'LBL_LIST_NAME' => 'Name',
  'LBL_EDIT_BUTTON' => 'Edit',
  'LBL_REMOVE' => 'Remove',
  'LBL_ASCENDING' => 'Ascending',
  'LBL_DESCENDING' => 'Descending',
  'LBL_OPT_IN' => 'Opt In',
  'LBL_OPT_IN_PENDING_EMAIL_NOT_SENT' => 'Pending Confirm opt in, Confirm opt in not sent',
  'LBL_OPT_IN_PENDING_EMAIL_SENT' => 'Pending Confirm opt in, Confirm opt in sent',
  'LBL_OPT_IN_CONFIRMED' => 'Opted in',
  'LBL_LIST_FORM_TITLE' => 'History calls List',
  'LBL_MODULE_NAME' => 'History calls',
  'LBL_MODULE_TITLE' => 'History calls',
  'LBL_HOMEPAGE_TITLE' => 'My History calls',
  'LNK_NEW_RECORD' => 'Create History calls',
  'LNK_LIST' => 'View History calls',
  'LNK_IMPORT_TTTT_WORLDFONEPBX' => 'Import WorldfonePBX',
  'LBL_SEARCH_FORM_TITLE' => 'Search History calls',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
  'LBL_TTTT_WORLDFONEPBX_SUBPANEL_TITLE' => 'WorldfonePBX',
  'LBL_NEW_FORM_TITLE' => 'New History calls',
  'LBL_CALLUUID' => 'calluuid',
  'LBL_EDITVIEW_PANEL1' => 'field',
  'LBL_PHONE_WORK' => 'phone work',
  'LBL_QUICKCREATE_PANEL1' => 'New Panel 1',
  'LBL_EDITVIEW_PANEL2' => 'field',
  'LBL_EDITVIEW_PANEL3' => 'New Panel 3',
  'LNK_IMPORT_TTTT_WORLDFONEPBX1' => 'Import History calls',
  'LBL_TTTT_WORLDFONEPBX1_SUBPANEL_TITLE' => 'History calls',
  'LNK_IMPORT_TTTT_HISTORY_CALLS' => 'Import History calls',
  'LBL_TTTT_HISTORY_CALLS_SUBPANEL_TITLE' => 'History calls',
  'LBL_DIRECTION' => 'direction',
  'LBL_CALLSTATUS' => 'callstatus',
  'LBL_WORKSTATUS' => 'workstatus',
  'LBL_STARTTIME' => 'starttime',
  'LBL_ANSWERTIME' => 'answertime',
  'LBL_ENDTIME' => 'endtime',
  'LBL_TOTALDURATION' => 'totalduration',
  'LBL_BILLDURATION' => 'billduration',
  'LBL_CALLUUID2' => 'calluuid2',
  'LBL_USEREXTENSION' => 'userextension',
  'LBL_CUSTOMERNAME' => 'customername',
  'LBL_CUSTOMERNUMBER' => 'customernumber',
  'LBL_CUSTOMERTYPE' => 'customertype',
  'LBL_CUSTOMERCODE' => 'customercode',
  'LBL_CALLTYPE' => 'calltype',
  'LBL_DISPOSITION' => 'disposition',
  'LBL_ACTIVITYDISPOSITION' => 'activitydisposition',
  'LBL_CALLNOTE' => 'callnote',
  'LBL_CAUSETXT' => 'causetxt',
  'LBL_FILE_GHIAM' => 'Nghe Ghi Âm',
  'LBL_LEADS_ID_LEAD_ID' => 'leads id (related Lead ID)',
  'LBL_LEADS_ID' => 'leads id',
);