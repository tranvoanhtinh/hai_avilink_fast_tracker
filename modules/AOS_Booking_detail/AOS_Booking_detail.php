<?php

/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2018 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */


class AOS_Booking_detail extends Basic
{
    public $new_schema = true;
    public $module_dir = 'AOS_Booking_detail';
    public $object_name = 'AOS_Booking_detail';
    public $table_name = 'aos_booking_detail';
    public $importable = false;

    public $id;
    public $name;
    public $date_entered;
    public $date_modified;
    public $modified_user_id;
    public $modified_by_name;
    public $created_by;
    public $created_by_name;
    public $description;
    public $deleted;
    public $created_by_link;
    public $modified_user_link;
    public $assigned_user_id;
    public $assigned_user_name;
    public $assigned_user_link;
    public $SecurityGroups;

    public function bean_implements($interface)
    {
        switch ($interface) {
            case 'ACL':
                return true;
        }

        return false;
    }


    public function save_booking_detail($booking, $post)
    {
        global $db, $sugar_config;
        require_once 'include/TimeDate.php';
        $time = new TimeDate(); 
        $typetime = $time->get_time_format();
        $typedate = $time->get_date_format();
        $code = 'passenger_';
        $no = 1;
        $deleteds = $post[$code . 'deleted'];
        foreach ($deleteds as $key => $value) {
            if ($value !== '') {
                $this->mark_deleted($post[$code . 'deleted'][$key]);
            }
        }
        if (isset($post[$code . 'passport']) && is_array($post[$code . 'passport'])) {
            $passports = $post[$code . 'passport'];
            foreach ($passports as $key => $value) {
                if ($value !== '') {

                    // Sanitize the passenger_id to prevent SQL injection
                    $passenger_id = $db->quote($post[$code . 'id'][$key]);

                    // Check if there is already a record for the given passenger_id
                    $sql = "SELECT id FROM aos_booking_detail WHERE booking_id = '".$booking->id."' AND deleted = '0' AND passenger_id = '$passenger_id' LIMIT 1";
                    $result = $db->query($sql);
                    $row = $db->fetchByAssoc($result);

                    if ($row) {
                        // Record exists, load the existing bean
                        $bean = BeanFactory::getBean('AOS_Booking_detail', $row['id']);
                    } else {
                        // No matching record, create a new bean
                        $bean = BeanFactory::newBean('AOS_Booking_detail');
                    }
                    // Set bean properties
                    $bean->invoice_no = $booking->invoice_no;
                    $bean->passport = $post[$code . 'passport'][$key];
                    $bean->name = $post[$code . 'name'][$key];
                    $bean->passenger_id = $post[$code . 'id'][$key];
                    $bean->booking_id = $booking->id; // Assuming you want to link this detail to the booking
                    $bean->assigned_user_id  = $booking->assigned_user_id;
                    $bean->booking_date = $booking->booking_date;
                    $bean->airport_from = $post[$code . 'from'][$key];
                    $bean->nationality = $post[$code . 'nationality'][$key];
                    $bean->airport_to = $post[$code . 'to'][$key];
                    $bean->flight_name = $post[$code . 'flight'][$key];
                    $bean->pnr = $post[$code . 'pnr'][$key];
                    $bean->service = $post['services'];
                    $date_std_sta = $post[$code . 'std_sta'][$key]; // Đảm bảo chuỗi ngày giờ có dạng 'YYYY-MM-DD HH:MM:SS'
                    $dateTime = DateTime::createFromFormat('d/m/Y H:i', $date_std_sta);


                    if ($dateTime !== false) {
                        // Add 7 hours
                        $dateTime->modify('-7 hours');

                        // Format to 'Y-m-d H:i:s'
                        $formatted_date = $dateTime->format('Y-m-d H:i:s');

                        // Assign to $bean->date_std_sta
                        $bean->date_std_sta = $formatted_date;
                    }

                    $bean->passenger_no = $no;
                    // Save the bean
                    $bean->save();
                    $no++;

                    // Additional logic for cases where $post[$code . 'save'][$key] is not '0' can be added here
                }
            }
        }
    }
    public function mark_deleted($id)
    {
        parent::mark_deleted($id);
    }
}
