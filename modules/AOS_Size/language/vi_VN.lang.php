<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2018 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */
$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Tên người dùng được chỉ định',
  'LBL_ASSIGNED_TO_NAME' => 'Chỉ định cho',
  'LBL_SECURITYGROUPS' => 'Nhóm bảo mật',
  'LBL_SECURITYGROUPS_SUBPANEL_TITLE' => 'Nhóm bảo mật',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Ngày tạo',
  'LBL_DATE_MODIFIED' => 'Ngày chỉnh sửa',
  'LBL_MODIFIED' => 'Được sửa bởi',
  'LBL_MODIFIED_NAME' => 'Chỉnh sửa bởi Tên',
  'LBL_CREATED' => 'Khởi tạo bởi',
  'LBL_DESCRIPTION' => 'Mô tả',
  'LBL_DELETED' => 'Đã xóa',
  'LBL_NAME' => 'Tên',
  'LBL_CREATED_USER' => 'Tạo bởi người dùng',
  'LBL_MODIFIED_USER' => 'Sửa bởi người dùng',
  'LBL_LIST_NAME' => 'Tên',
  'LBL_EDIT_BUTTON' => 'Sửa',
  'LBL_REMOVE' => 'Xóa bỏ',
  'LBL_ASCENDING' => 'tăng dần',
  'LBL_DESCENDING' => 'Giảm dần',
  'LBL_OPT_IN' => 'Chọn tham gia',
  'LBL_OPT_IN_PENDING_EMAIL_NOT_SENT' => 'Đang chờ xác nhận chọn tham gia, xác nhận chọn tham gia không được gửi',
  'LBL_OPT_IN_PENDING_EMAIL_SENT' => 'Đang chờ xác nhận chọn tham gia, xác nhận chọn tham gia đã được gửi',
  'LBL_OPT_IN_CONFIRMED' => 'Đã chọn tham gia',
  'LBL_LIST_FORM_TITLE' => 'SIZE Danh sách',
  'LBL_MODULE_NAME' => 'SIZE',
  'LBL_MODULE_TITLE' => 'SIZE',
  'LBL_HOMEPAGE_TITLE' => 'Của tôi SIZE',
  'LNK_NEW_RECORD' => 'Tạo SIZE',
  'LNK_LIST' => 'Xem SIZE',
  'LNK_IMPORT_AOS_SIZE' => 'Nhập SIZE',
  'LBL_SEARCH_FORM_TITLE' => 'Tìm kiếm SIZE',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Xem lịch sử',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Hoạt động',
  'LBL_AOS_SIZE_SUBPANEL_TITLE' => 'SIZE',
  'LBL_NEW_FORM_TITLE' => 'Mới SIZE',
  'LBL_SIZE_L_CM' => 'size l cm',
  'LBL_SIZE_W_CM' => 'size w cm',
  'LBL_SIZE_H_CM' => 'size h cm',
  'LBL_SIZE_L_INCH' => 'size l inch',
  'LBL_SIZE_W_INCH' => 'size w inch',
  'LBL_SIZE_H_INCH' => 'size h inch',
  'LBL_SIZE_W_H_CM' => 'size w/h cm',
  'LBL_SIZE_W_H_INCH' => 'size w/h inch',
  'LBL_EDITVIEW_PANEL1' => 'Size',
  'LBL_EDITVIEW_PANEL2' => 'Bảng điều khiển mới 2',
  'LBL_DETAILVIEW_PANEL1' => 'Size',

);