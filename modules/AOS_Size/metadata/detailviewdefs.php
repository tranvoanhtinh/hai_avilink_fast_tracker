<?php
$module_name = 'AOS_Size';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_DETAILVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 'assigned_user_name',
        ),
        1 => 
        array (
          0 => 'date_entered',
          1 => 'date_modified',
        ),
      ),
      'lbl_detailview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'size_l_cm',
            'label' => 'LBL_SIZE_L_CM',
          ),
          1 => 
          array (
            'name' => 'size_l_inch',
            'label' => 'LBL_SIZE_L_INCH',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'size_w_cm',
            'label' => 'LBL_SIZE_W_CM',
          ),
          1 => 
          array (
            'name' => 'size_w_inch',
            'label' => 'LBL_SIZE_W_INCH',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'size_h_cm',
            'label' => 'LBL_SIZE_H_CM',
          ),
          1 => 
          array (
            'name' => 'size_h_inch',
            'label' => 'LBL_SIZE_H_INCH',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'size_w_h_cm',
            'label' => 'LBL_SIZE_W_H_CM',
          ),
          1 => 
          array (
            'name' => 'size_w_h_inch',
            'label' => 'LBL_SIZE_W_H_INCH',
          ),
        ),
        4 => 
        array (
          0 => 'description',
          1 => '',
        ),
      ),
    ),
  ),
);
;
?>
