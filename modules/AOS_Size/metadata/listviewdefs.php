<?php
$module_name = 'AOS_Size';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'SIZE_L_CM' => 
  array (
    'type' => 'decimal',
    'label' => 'LBL_SIZE_L_CM',
    'width' => '10%',
    'default' => true,
  ),
  'SIZE_W_CM' => 
  array (
    'type' => 'decimal',
    'label' => 'LBL_SIZE_W_CM',
    'width' => '10%',
    'default' => true,
  ),
  'SIZE_H_CM' => 
  array (
    'type' => 'decimal',
    'label' => 'LBL_SIZE_H_CM',
    'width' => '10%',
    'default' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
);
;
?>
