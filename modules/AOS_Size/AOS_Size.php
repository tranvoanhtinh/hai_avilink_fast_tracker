<?php
require_once('modules/AOS_Size/AOS_Size_sugar.php');

class AOS_Size extends AOS_Size_sugar
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @deprecated deprecated since version 7.6, PHP4 Style Constructors are deprecated and will be remove in 7.8, please update your code, use __construct instead
     */
    public function AOS_Size()
    {
        $deprecatedMessage = 'PHP4 Style Constructors are deprecated and will be remove in 7.8, please update your code';
        if (isset($GLOBALS['log'])) {
            $GLOBALS['log']->deprecated($deprecatedMessage);
        } else {
            trigger_error($deprecatedMessage, E_USER_DEPRECATED);
        }
        self::__construct();
    }

    public function generateUUIDv4()
    {
        $data = random_bytes(16);

        // Set the version (4) and variant bits (0100)
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80);

        // Convert to hexadecimal format
        $uuid = vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));

        return $uuid;
    }

    public function save_size($post_data, $parent, $key)
    {   
      
        $count = count($post_data[$key . 'delete']);

        global $db;
        $inch = 2.54;
    

        for ($i = 0; $i < $count; $i++) {


          if (isset($post_data[$key . 'delete'][$i]) && $post_data[$key . 'delete'][$i] == 1 && $post_data[$key . 'l_cm_name'][$i] != '') {

                

                    $q3 = "UPDATE aos_products_aos_size_1_c SET deleted = 1 WHERE   aos_products_aos_size_1aos_size_idb  = '{$post_data[$key . 'id'][$i]}'";
                    $result3 = $db->query($q3);

                    if ($result3) {
                        $q4 = "UPDATE aos_size SET deleted = 1 WHERE id = '{$post_data[$key . 'id'][$i]}'";
                        $result4 = $db->query($q4);
                    }
                
            }

            if (isset($post_data[$key . 'delete'][$i]) && $post_data[$key . 'delete'][$i] == 0 && $post_data[$key . 'l_cm_name'][$i] != '') {
                $sizeLcm = $post_data[$key . 'l_cm_name'][$i];
                $sizeWcm = $post_data[$key . 'w_cm_name'][$i];
                $sizeHcm = $post_data[$key . 'h_cm_name'][$i];

                $sizeLinch = $post_data[$key . 'l_cm_name'][$i] / $inch;
                $sizeWinch = $post_data[$key . 'w_cm_name'][$i] / $inch;
                $sizeHinch = $post_data[$key . 'h_cm_name'][$i] / $inch;

                $name = $sizeLcm . 'x' . $sizeWcm . 'x' . $sizeHcm;
                $id = $post_data[$key . 'id'][$i];

                $date_entered = $parent->date_modified;
                $date_modified = $parent->date_modified;
                $modified_user_id = $parent->modified_user_id;
                $created_by = $parent->created_by;
                $assigned_user_id = $parent->assigned_user_id;
                $handle = $post_data['handle_name'][$i];
                $bottom_length = $post_data['bottom_length_name'][$i];
                $bottom_width = $post_data['bottom_width_name'][$i];

                $q = "INSERT INTO aos_size (id, name, date_entered, date_modified, modified_user_id, created_by, description, deleted, assigned_user_id, size_l_cm, size_w_cm, size_h_cm, size_l_inch, size_w_inch, size_h_inch, size_w_h_cm, size_w_h_inch, handle, bottom_length, bottom_width)
                    VALUES ('$id', '$name', '$date_entered', '$date_modified', '$modified_user_id', '$created_by', '', 0, '$assigned_user_id', '$sizeLcm', '$sizeWcm', '$sizeHcm', '$sizeLinch', '$sizeWinch', '$sizeHinch', '', '', '$handle', '$bottom_length', '$bottom_width')";

                $result = $db->query($q);

                if ($result) {
                    $uuid = $this->generateUUIDv4(); // Use $this to call the method

                    $q2 = "INSERT INTO aos_products_aos_size_1_c (id, date_modified, deleted, aos_products_aos_size_1aos_products_ida, aos_products_aos_size_1aos_size_idb)
                        VALUES ('$uuid', '$date_entered', 0, '$parent->id', '$id')";

                    $result2 = $db->query($q2);
                }else {
                    $q3 = "UPDATE aos_size 
                      SET name = '$name', 
                          date_entered = '$date_entered', 
                          date_modified = '$date_modified', 
                          modified_user_id = '$modified_user_id', 
                          created_by = '$created_by', 
                          assigned_user_id = '$assigned_user_id', 
                          size_l_cm = '$sizeLcm', 
                          size_w_cm = '$sizeWcm', 
                          size_h_cm = '$sizeHcm', 
                          size_l_inch = '$sizeLinch', 
                          size_w_inch = '$sizeWinch', 
                          size_h_inch = '$sizeHinch', 
                          handle = '$handle', 
                          bottom_length = '$bottom_length', 
                          bottom_width = '$bottom_width' 
                      WHERE id = '$id'";

                    $result3 = $db->query($q3);

                }
            }

                
        }
    }
}
?>
