<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class DupDetectorController extends SugarController
{
    /**
     * Action Field Config
     */
    protected function action_fieldconfig()
    {
        $this->view = 'fieldconfig';
    }
}
